#! /bin/bash

cd docker

help() {
  echo "Shell Script wrapping Docker, inspired by: http://www.techinsight.io/review/adobe-experience-manager/incontainer-testing-for-aem-with-docker/"
  echo "No help here yet, try reading the shell script for options... SOZ!"
}

create-image() {
    echo "Creating Image"
    git lfs fetch
    git lfs checkout
    docker build --tag="aem/author:5.6.1" .
}

commit() {
  VERSION=$(docker ps --filter "name=aem-author" --format "{{.ID}}")

  echo "Committing: $VERSION"
  docker commit $VERSION aem/author:5.6.1
}

run-image() {
  echo "Running docker container"
  docker run --name=aem-author -ti -d -p 4502:4502 aem/author:5.6.1
}

if [[ $1 =~ ^(help|create-image|commit|run-image)$ ]]; then
  "$@"
else
  help
  exit 1
fi
