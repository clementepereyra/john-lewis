DEV_AEM_URL="http://dev-cms-author.onejl.uk"
GROUP="com.jl.aem"
SITES_DIST_NAME="onejl-sites"
CONTENT_DIST_NAME="onejl-sites-content"

if [ -z "$SITES_TAG" ]; then
  SITES_TAG=$(curl -su admin:admin $DEV_AEM_URL/crx/packmgr/service.jsp?cmd=ls | grep -Eo "$SITES_DIST_NAME-[0-9]+.zip" | sort | tail -n1 | grep -Eo "[0-9]+")
fi

SITES_DIST_FILENAME="$SITES_DIST_NAME-$SITES_TAG.zip"
CONTENT_DIST_FILENAME=$(curl -su admin:admin $DEV_AEM_URL/crx/packmgr/service.jsp?cmd=ls | grep -Eo "$CONTENT_DIST_NAME-$SITES_TAG-[0-9]+.zip")

echo "Using sites bundle $SITES_DIST_FILENAME"
echo "Using content bundle $CONTENT_DIST_FILENAME"

curl -u admin:admin -o /tmp/$SITES_DIST_FILENAME $DEV_AEM_URL/etc/packages/$GROUP/$SITES_DIST_FILENAME
curl -u admin:admin -o /tmp/$CONTENT_DIST_FILENAME $DEV_AEM_URL/etc/packages/$GROUP/$CONTENT_DIST_FILENAME
curl -u admin:admin -F file=@"/tmp/$CONTENT_DIST_FILENAME" -F force=true -F install=true http://localhost:4505/crx/packmgr/service.jsp
curl -u admin:admin -F file=@"/tmp/$SITES_DIST_FILENAME" -F force=true -F install=true http://localhost:4505/crx/packmgr/service.jsp
curl -d "scriptPath=/etc/groovyconsole/scripts/onejl/releaseMigration.groovy&data=%7B%22dryRun%22%3Afalse%7D" -X POST -u admin:admin http://localhost:4505/bin/groovyconsole/post.json
