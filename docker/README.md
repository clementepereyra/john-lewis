# Local Docker environment

For acceptance testing use the `up.sh` script in `aem-and-browse`

For local Browse app development use the `up.sh` in `aem-only` and point your local browse to http://localhost:4502 

## Setup

### Docker
Install [Docker](https://www.docker.com/)

If running on OSX configure Docker to have at least 4gb ram

### GCloud registry for Browse images
GCloud registry setup is needed to pull Browse images

Download and install the [latest SDK](https://cloud.google.com/sdk/downloads)

Run this to authenticate (this command opens a browser for login, use yourname@johnlewis.co.uk credentials)
```bash
gcloud auth configure-docker 
```

If this fails because of Python issues you can try installing Python 2.x manually and setting the CLOUDSDK_PYTHON environment variable
```bash
export CLOUDSDK_PYTHON=/home/foo/python27/python.exe 
```

### Gitlab registry for AEM images
Gitlab registry setup is needed to pull AEM images

[Create an access token](https://jl.githost.io/profile/personal_access_tokens) with read_registry permission

Run this to authenticate (use yourname@johnlewis.co.uk and your access token as password)
```bash
docker login jl-docker.githost.io
```

## Useful URLS
 * [Browse](http://localhost:8080)
 * [AEM Author](http://localhost:4502)
 * [AEM Dispatcher](http://localhost:4505)
 * [AEM Groovy Console](http://localhost:4505/etc/groovyconsole.html)
 * [AEM infrastructure config](http://localhost:4505/system/console/configMgr/com.jl.aem.preview.HtmlPreviewServiceImpl)
 * [CRX/DE](http://localhost:4505/crx/de)

## Starting containers

### AEM 6.3 and latest Browse
```bash
./up.sh
```

### AEM 6.3 and a specific tag of Browse
```bash
BROWSE_TAG=2000 ./up.sh
```

## Deploying sites and content using deploy-sites.sh

This requires the containers to be up and for the sites packages to be available on dev [AEM](http://dev-cms-author.onejl.uk)
There is a [build job](https://build.onejl.uk/go/tab/pipeline/history/content-automated-tests) that builds and tests the the sites packages, and uploads the packages to dev when successful 

### Deploy latest with content and migration scripts
```bash
./deploy-sites.sh
```

### Deploy a specific tag with content and migration scripts
```bash
SITES_TAG=77 ./deploy-sites.sh
```

### Manually deploying a sites/content package

Use [CRX package manager](http://localhost:4505/crx/packmgr/index.jsp) to upload and install the packages

### Manually running migration scripts
If you manually installed a content package you will need to run migration scripts

Open releaseMigration.groovy in [AEM Groovy Console](http://localhost:4505/etc/groovyconsole.html)

Change dryRun to true

Run script

## Known issues

### Cant start containers because container name already exists
This is an issue when switching between the aem-browse-docker and aem-only-docker configs

Sometimes when the docker-compose config changes or the dir name changes you need to delete the old containers before running `up.sh`

```bash
docker rm browse-app
docker rm browse-bff
docker rm aem-author
docker rm aem-dispatcher
```
