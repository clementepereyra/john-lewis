if [ -z "$BROWSE_TAG" ]; then
  BROWSE_TAG=`gcloud container images list-tags eu.gcr.io/johnlewis-web/browse-app --limit 1 | awk 'NR > 1 {print $2}'`
fi

docker pull eu.gcr.io/johnlewis-web/browse-app:${BROWSE_TAG}
docker pull eu.gcr.io/johnlewis-web/browse-bff:${BROWSE_TAG}

if [ $(uname) == "Linux" ]
then
    cp docker-compose.linux.yml docker-compose.override.yml
fi

BROWSE_TAG=$BROWSE_TAG docker-compose up
