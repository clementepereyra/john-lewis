package com.jl.aem.common;

public interface JLConstants {

    String BASE_AEM_PATH = "/content/1jl/en/content/browse";

    String STUB_FOR_1JL_MAPPER_RESOURCE_TYPE = "onejl/components/stubFor1jlMapper";

    String CANONICAL_URL = "dynamicPageCanonicalUrl";
}
