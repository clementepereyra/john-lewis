package com.jl.aem.common.util;

import com.day.cq.commons.jcr.JcrConstants;
import com.jl.aem.common.JLConstants;
import org.apache.commons.lang3.StringUtils;

public class JLPathUtil {

    private JLPathUtil(){}

    public static String get1JLPath(String aemPath) {
        if (aemPath == null)
            return StringUtils.EMPTY;
        return aemPath.replace(JLConstants.BASE_AEM_PATH, StringUtils.EMPTY)
                .replace("/" + JcrConstants.JCR_CONTENT, StringUtils.EMPTY);
    }
}
