package com.jl.aem.common;

public interface AEMConstants {

    String ECMA_DATE_FORMAT = "EEE MMM dd yyyy HH:mm:ss 'GMT'Z";

}
