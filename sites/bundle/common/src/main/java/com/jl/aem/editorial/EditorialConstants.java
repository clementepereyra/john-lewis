package com.jl.aem.editorial;

import com.jl.aem.common.JLConstants;

public interface EditorialConstants {

    String HUB_PAGE_RESOURCE_TYPE = "onejl/components/pages/articlehubpage";

    String HUB_PAGE_RESOURCE_TYPE_WITH_PREFIX = "/apps/onejl/components/pages/articlehubpage";

    String ARTICLE_PAGE_RESOURCE_TYPE = "onejl/components/pages/articlepage";

    String ARTICLE_PAGE_RESOURCE_TYPE_WITH_PREFIX = "/apps/onejl/components/pages/articlepage";

    String ARTICLE_TILE_RESOURCE_TYPE = "onejl/components/editorial/tile";

    String ARTICLE_HERO_RESOURCE_TYPE = "onejl/components/editorial/heroTile";

    String EDITORIAL_ROOT_PATH = JLConstants.BASE_AEM_PATH + "/content";

    String ARTICLE_REPLICATION_DATE_PROPERTY_NAME = "articleReplicationDate";

    String REWRITE_ARTICLE_PUBLISH_DATE = "rewriteArticlePublishDate";
}
