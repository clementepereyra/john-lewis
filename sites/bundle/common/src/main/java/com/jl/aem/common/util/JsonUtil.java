package com.jl.aem.common.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.sling.JsonObjectCreator;

import java.io.IOException;

public class JsonUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    public static JsonNode fromResource(Resource resource) throws IOException, JSONException {
        StringBuilderWriter writer = new StringBuilderWriter();
        JsonObjectCreator.create(resource, -1).write(writer);
        return mapper.readTree(writer.getBuilder().toString());
    }

    private JsonUtil(){}

}
