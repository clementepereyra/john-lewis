package com.jl.aem.common.util;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;

import static com.jl.aem.common.AEMConstants.ECMA_DATE_FORMAT;

public class DateFormatUtil {

    private static final Locale DATE_FORMAT_LOCALE = Locale.US;

    public static final String DATE_FORMAT_LIST_VIEW = "dd MMM yyyy HH:mm:ss";

    public static String format(Calendar date) {
        return Optional.ofNullable(date)
                .map(DateFormatUtil::formatUnsafe)
                .orElse(StringUtils.EMPTY);
    }

    public static String formatArticlePublishDate(Long timeInMillis) {
        DateFormat formatter = new SimpleDateFormat(DATE_FORMAT_LIST_VIEW, DATE_FORMAT_LOCALE);
//        formatter.setTimeZone(TimeZone.getDefault());
        if (timeInMillis == null) {
            return StringUtils.EMPTY;
        }
        return formatter.format(timeInMillis);
    }

    private static String formatUnsafe(Calendar date) {
        DateFormat formatter = new SimpleDateFormat(ECMA_DATE_FORMAT, DATE_FORMAT_LOCALE);
//        formatter.setTimeZone(date.getTimeZone());
        return formatter.format(date.getTime());
    }

}
