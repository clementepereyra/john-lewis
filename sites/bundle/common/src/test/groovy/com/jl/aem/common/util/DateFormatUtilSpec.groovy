package com.jl.aem.common.util

import spock.lang.Specification

class DateFormatUtilSpec extends Specification {


    def "should format nonnull calendar correctly"(){
        when:
            def calendar = GregorianCalendar.getInstance()
            calendar.setTimeInMillis(1525363943000)
        then:
            def formatted = DateFormatUtil.format(calendar)
    }

    def "should format null calendar correctly"(){
        when:
            def calendar = null
        then:
            DateFormatUtil.format(calendar) == ""
    }

    def "should return empty string if article publish time is null"(){
        when:
            Long time = null
        then:
            DateFormatUtil.formatArticlePublishDate(time) == ""
    }

    def "should format format article publish date correctly"(){
        when:
            Long time = 1525363943000
            TimeZone.setDefault(TimeZone.getTimeZone("Europe/London"))
        then:
            DateFormatUtil.formatArticlePublishDate(time) == "03 May 2018 17:12:23"
    }

}
