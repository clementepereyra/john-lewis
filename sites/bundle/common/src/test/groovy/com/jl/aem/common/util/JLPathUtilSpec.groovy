package com.jl.aem.common.util

import spock.lang.Specification

class JLPathUtilSpec extends Specification {

    def 'should remove aem prefix form url' () {
        expect:
            JLPathUtil.get1JLPath("/content/1jl/en/content/browse/content/article") == "/content/article"
    }

    def 'should remove jcr:content form url' () {
        expect:
            JLPathUtil.get1JLPath("/content/article/jcr:content") == "/content/article"
    }

    def 'should return empty stirng when parameter is empty' () {
        expect:
            JLPathUtil.get1JLPath("") == ""
    }

    def 'should return empty stirng when parameter is null' () {
        expect:
            JLPathUtil.get1JLPath(null) == ""
    }
}
