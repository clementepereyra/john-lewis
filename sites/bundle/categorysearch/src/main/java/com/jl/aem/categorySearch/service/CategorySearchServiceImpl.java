package com.jl.aem.categorysearch.service;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.jcr.JsonItemWriter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component(service = CategorySearchService.class, immediate = true)
public class CategorySearchServiceImpl implements CategorySearchService {

    static final String CATEGORY_PATH = "/content/1jl/en/content/browse/category/";

    static String s = "";
    private static final Logger LOG = LoggerFactory.getLogger(CategorySearchServiceImpl.class);

    static final String SEARCH_QUERY =
            "SELECT * FROM [cq:Page] WHERE NAME() = '%s' AND ISDESCENDANTNODE('" + CATEGORY_PATH + "')";

    private final Pattern categoryIdPattern = Pattern.compile(
        "/content/1jl/en/content/browse/category/(\\d+)(/jcr:content|/jcr%3Acontent|/_jcr_content|)\\.infinity\\.json");

    private JsonItemWriter jsonWriter;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Activate
    public void activate() {
        jsonWriter = new JsonItemWriter(Collections.emptySet());
    }

    @Override
    public Resource searchCategoryPage(String categoryId) {
        return searchCategoryPage(categoryId, getThreadResourceResolver());
    }

    @Override
    public String getCategoryPageJson(String requestPath) {
        try {
            Matcher matcher = categoryIdPattern.matcher(requestPath);
            if (matcher.find()) {
                String categoryId = matcher.group(1);
                Resource pageResource = searchCategoryPage(categoryId, getThreadResourceResolver());
                if (!matcher.group(2).isEmpty() && pageResource != null) {
                    pageResource = pageResource.getChild("jcr:content");
                }

                return convertResourceToJson(pageResource);
            }
        } catch (Throwable ex) {
            LOG.error("An exception while processing request to {} : {}", requestPath, ex);
        }
        return StringUtils.EMPTY;
    }

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "resolver factory shouldn't be null")
    private ResourceResolver getThreadResourceResolver() {
        return resourceResolverFactory.getThreadResourceResolver();
    }

    private Resource searchCategoryPage(String categoryId, ResourceResolver resourceResolver) {
        String searchQuery = createSearchQuery(categoryId);
        Iterator<Resource> resources = resourceResolver.findResources(searchQuery, Query.JCR_SQL2);
        return getResource(resources);
    }

    private String createSearchQuery(String categoryId) {
        return String.format(SEARCH_QUERY, categoryId);
    }

    private Resource getResource(Iterator<Resource> resources) {
        return resources.hasNext() ? resources.next() : null;
    }

    private String convertResourceToJson(Resource resource) {
        if (Objects.isNull(resource)) {
            return null;
        }
        try {
            StringWriter stringWriter = new StringWriter();
            jsonWriter.dump(resource.adaptTo(Node.class), stringWriter, -1);
            return stringWriter.toString();
        } catch (RepositoryException | JSONException e) {
            LOG.error("Could not create JSON", e);
        }
        return null;
    }
}
