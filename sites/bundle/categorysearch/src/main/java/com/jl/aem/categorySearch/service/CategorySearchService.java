package com.jl.aem.categorysearch.service;

import org.apache.sling.api.resource.Resource;

public interface CategorySearchService {

    Resource searchCategoryPage(String categoryId);

    String getCategoryPageJson(String requestPath);
}
