package com.jl.aem.categorysearch.service

import com.google.common.collect.Lists
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ResourceResolverFactory
import org.apache.sling.testing.mock.jcr.MockJcr
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.apache.sling.testing.mock.sling.loader.ContentLoader
import spock.lang.Specification

import javax.jcr.Node
import javax.jcr.Session
import javax.jcr.query.Query

import static com.jl.aem.categorysearch.service.CategorySearchServiceImpl.CATEGORY_PATH
import static com.jl.aem.categorysearch.service.CategorySearchServiceImpl.SEARCH_QUERY

class CategorySearchServiceImplSpec extends Specification {

    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)
    def service = new CategorySearchServiceImpl()
    ResourceResolver resourceResolver

    def setup(){
        aemContext.load().json("/categoryPages.json", "/content/1jl/en/content/browse/")
        resourceResolver = aemContext.resourceResolver()
        aemContext.registerInjectActivateService(service)
        service.resourceResolverFactory = Mock(ResourceResolverFactory.class)
        service.resourceResolverFactory.getThreadResourceResolver() >> aemContext.resourceResolver()
    }

    def "should be correctly configured after activate method"(){
        expect:
            def serviceFromContext = aemContext.getService(CategorySearchService)
            service == serviceFromContext
    }

    def "should return correct page resource if page is a direct child of categories root page"(){
        when:
            def categoryId = "5000010"
            mockQuery(categoryId,
                    Lists.asList(resourceResolver.getResource(CATEGORY_PATH + "/" + categoryId).adaptTo(Node.class)))
            def resource = service.searchCategoryPage("5000010").getChild("jcr:content")
        then:
            resource != null
            resource.getValueMap().get("jcr:title", String.class) == "Baby & Child Area"

    }

    def "should return correct page resource if page is not a direct child of categories root page"(){
        when:
            def categoryId = "50000296"
            mockQuery(categoryId,
                    Lists.asList(resourceResolver.getResource(CATEGORY_PATH + "/5000010/50000296").adaptTo(Node.class)))
            def resource = service.searchCategoryPage("50000296").getChild("jcr:content")
        then:
            resource != null
            resource.getValueMap().get("jcr:title", String.class) == "Beauty Area"
    }

    def "should return null if page does not exist"(){
        when:
            def categoryId = "12345678"
            mockQuery(categoryId, Lists.newArrayList())
            def resource = service.searchCategoryPage(categoryId)
        then:
            resource == null
    }

    def "should return correct json on request without jcr:content"(){
        when:
            def categoryId = "5000010"
            mockQuery(categoryId,
                    Lists.asList(resourceResolver.getResource(CATEGORY_PATH + "/" + categoryId).adaptTo(Node.class)))
            def output = service.getCategoryPageJson("/content/1jl/en/content/browse/category/5000010.infinity.json")
        then:
            output == ContentLoader.getResourceAsStream("/jsonOutputWithoutJcrContent.json").text
    }

    def "should return correct json on request with jcr:content"(){
        when:
            def categoryId = "5000010"
            mockQuery(categoryId,
                    Lists.asList(resourceResolver.getResource(CATEGORY_PATH + "/" + categoryId).adaptTo(Node.class)))
            def output = service.getCategoryPageJson("/content/1jl/en/content/browse/category/5000010/jcr:content.infinity.json")
        then:
            output == ContentLoader.getResourceAsStream("/jsonOutputWithJcrContent.json").text
    }

    private void mockQuery(String categoryId, List<Node> result) {
        def statement = String.format(SEARCH_QUERY, categoryId)
        MockJcr.setQueryResult(resourceResolver.adaptTo(Session.class),
                statement,
                Query.JCR_SQL2,
                result)
    }


}
