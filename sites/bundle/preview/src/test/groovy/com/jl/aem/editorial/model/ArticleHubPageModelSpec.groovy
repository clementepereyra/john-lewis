package com.jl.aem.editorial.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.google.common.collect.Lists
import com.jl.aem.editorial.service.impl.HubPageSearchService
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.junit.Rule
import spock.lang.Specification

import static com.jl.aem.editorial.EditorialConstants.EDITORIAL_ROOT_PATH

class ArticleHubPageModelSpec extends Specification {

    ArticleHubPageModel articlehubPageModel

    @Rule
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)

    ResourceResolver resourceResolver
    def hubPageService
    def mapper = new ObjectMapper()

    def setup(){
        aemContext.load().json('/articlePages.json', EDITORIAL_ROOT_PATH)
        hubPageService = Mock(HubPageSearchService.class)
        aemContext.registerService(HubPageSearchService.class, hubPageService)
        aemContext.addModelsForPackage(ArticleHubPageModel.class.getPackage().getName())

        resourceResolver = aemContext.resourceResolver()
    }

    def 'json should contain AEM mandatory properties'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList().iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.has("jcr:title")
            actual.has("jcr:primaryType")
            actual.has("sling:resourceType")
            actual.has("pageTitle")
    }

    def 'canonical url should be present in response' () {
        when:
        def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
        def resourceWithNoCanonicalUrl = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub_2/jcr:content")
        hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList().iterator()
        hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
        def model = resource.adaptTo(ArticleHubPageModel.class)
        def modelWithNoCanonicalUrl = resourceWithNoCanonicalUrl.adaptTo(ArticleHubPageModel.class)
        then:
        def actual = mapper.valueToTree(model)
        def actualWithGeneratedCanonicalUrl = mapper.valueToTree(modelWithNoCanonicalUrl)
        actual.dynamicPageCanonicalUrl.textValue() == "/content/test_hub"
        actualWithGeneratedCanonicalUrl.dynamicPageCanonicalUrl.textValue() == "/content/test_hub_2"
    }

    def 'json should not contain articles if hub page service returns empty result'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList().iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.has(ArticleHubPageModel.ARTICLE_HERO) == false
            actual.has(ArticleHubPageModel.ARTICLES) == false
    }

    def 'should keep the order of the pages returned from the service'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_3/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLE_HERO).articleHero.path.textValue() == "/content/test_hub/article_1"
            actual.get(ArticleHubPageModel.ARTICLES).article_0.path.textValue() == "/content/test_hub/article_3"
            actual.get(ArticleHubPageModel.ARTICLES).article_1.path.textValue() == "/content/test_hub/article_2"
            actual.get(ArticleHubPageModel.ARTICLES).elements().toList().findAll({it.class == ObjectNode}).size() == 2
    }

    def 'should return correct json if there is only 1 underlying article page'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLE_HERO).articleHero.path.textValue() == "/content/test_hub/article_1"
            actual.has(ArticleHubPageModel.ARTICLES) == false
    }

    def 'should return sell if present in article page properties'() {
        given:
            def subtitle;
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_3/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLE_HERO).articleHero.subtitle.textValue() == "sell text"
            actual.get(ArticleHubPageModel.ARTICLES).article_0.subtitle.textValue() == "sell text2"
            actual.get(ArticleHubPageModel.ARTICLES).article_1.subtitle.textValue() == "article3 heading subtitle"
    }

    def "should single article contain responsive parameters"() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLES).article_0.has("responsive")
    }

    def "should single article has configs for all the devices"() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.has("small")
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.has("medium")
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.has("default")
    }

    def "should single article has correct configs for all the devices"() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content")
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.small.width.textValue() == "6"
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.medium.width.textValue() == "6"
            actual.get(ArticleHubPageModel.ARTICLES).article_0.responsive.default.width.textValue() == "4"
    }

    def "should single article has no responsive in case if there is no responsive config in parent page"() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content")

            resourceResolver.delete(resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content/articleTileResponsive"))
            def model = resource.adaptTo(ArticleHubPageModel.class)
        then:
            def actual = mapper.valueToTree(model)
            actual.get(ArticleHubPageModel.ARTICLES).article_0.has("responsive") == false
    }

}
