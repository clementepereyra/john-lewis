package com.jl.aem.preview

import org.apache.sling.api.SlingHttpServletRequest
import org.apache.sling.api.resource.Resource
import spock.lang.Specification

class PagePreviewModelSpec extends Specification {

    def resource = Mock(Resource)
    def request = Mock(SlingHttpServletRequest)
    def previewService = Mock(HtmlPreviewService)
    def model = new PagePreviewModel(previewService: previewService, request: request, resource: resource)

    def "should return page parts"() {
        given:
            previewService.getHtmlPreview(request, resource, null) >>
                    """<html>
                      |  <head>
                      |    <!--[if gt IE 9]><!-->
                      |      <link rel="stylesheet" href="/assets/css/styles.css" />
                      |    <!--<![endif]-->
                      |    <!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/legacy-ie.css" /><![endif]-->
                      |    <script type="text/javascript" purpose="analytics">
                      |        /* beautify ignore:start */var jlData = {"page":{"locale":{"country":"uk","currency":"gbp","language":"en-gb"},"pageInfo":{"breadCrumb":[{"clickable":1,"value":""}],"pageCanonicalURL":"https://www.johnlewis.com/http:/www.johnlewis.com","pageName":"jl:","pageType":"content","site":"m.johnlewis.com"},"videos":[]}};/* beautify ignore:end */
                      |    </script>
                      |    <script type="text/json" id="jsonPageData">{"platform":"unknown"}</script>
                      |    <script type="text/json" id="richRelevanceData">{"apiKey":"0000000000000000","baseUrl":"//integration.richrelevance.com/rrserver/","placementTypes":""}</script>
                      |    <script type="text/javascript" src="//media.richrelevance.com/rrserver/js/1.2/p13n.js"></script>
                      |    <script type="text/javascript" src="https://nexus.ensighten.com/johnlewis/onejldev/Bootstrap.js"></script>
                      |    <script type="text/javascript" data-redirect-international-visitors="true" src="//static.goqubit.com/smartserve-4146.js" async defer></script>
                      |  </head>
                      |  <body>
                      |    <header class="header-site-wide"><header>
                      |      header content
                      |    </header></header>
                      |    <div class="cms-content-main-switcher">
                      |      <div class="cms-content-main-holder">
                      |        <main class="cms-content-main">
                      |          <div class="cms-content-main__resp-wrapper"><cms-stub/></div>
                      |        </main>
                      |      </div>
                      |    </div>
                      |    <footer class="footer-site-wide"><footer>
                      |      footer content
                      |    </footer></footer>
                      |    <script src="//johnlewis.scene7.com/s7viewers/html5/js/VideoViewer.js" type="text/javascript"></script>
                      |    <script type="text/javascript" src="/assets/js/app.bundle.js"></script>
                      |    <script type="text/javascript" src="/assets/js/qv.bundle.js"></script>
                      |  </body>
                      |</html>""".stripMargin()
        when:
            model.init()
        then:
            model.cssLinks == """<link rel="stylesheet" href="/assets/css/styles.css" />"""
            model.header == """<header class="header-site-wide"><header>\n      header content\n    </header></header>"""
            model.footer == """<footer class="footer-site-wide"><footer>\n      footer content\n    </footer></footer>"""
            model.javascriptAfterFooter == """<script src="//johnlewis.scene7.com/s7viewers/html5/js/VideoViewer.js" type="text/javascript"></script>\n<script type="text/javascript" src="/assets/js/app.bundle.js"></script>\n<script type="text/javascript" src="/assets/js/qv.bundle.js"></script>"""
    }

    def "should return page parts without main cms stub"() {
        given:
            previewService.getHtmlPreview(request, resource, null) >>
                    """<html>
                      |  <head>
                      |    <!--[if gt IE 9]><!-->
                      |      <link rel="stylesheet" href="/assets/css/styles.css" />
                      |    <!--<![endif]-->
                      |    <!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/legacy-ie.css" /><![endif]-->
                      |    <script type="text/javascript" purpose="analytics">
                      |        /* beautify ignore:start */var jlData = {"page":{"locale":{"country":"uk","currency":"gbp","language":"en-gb"},"pageInfo":{"breadCrumb":[{"clickable":1,"value":""}],"pageCanonicalURL":"https://www.johnlewis.com/http:/www.johnlewis.com","pageName":"jl:","pageType":"content","site":"m.johnlewis.com"},"videos":[]}};/* beautify ignore:end */
                      |    </script>
                      |    <script type="text/json" id="jsonPageData">{"platform":"unknown"}</script>
                      |    <script type="text/json" id="richRelevanceData">{"apiKey":"0000000000000000","baseUrl":"//integration.richrelevance.com/rrserver/","placementTypes":""}</script>
                      |    <script type="text/javascript" src="//media.richrelevance.com/rrserver/js/1.2/p13n.js"></script>
                      |    <script type="text/javascript" src="https://nexus.ensighten.com/johnlewis/onejldev/Bootstrap.js"></script>
                      |    <script type="text/javascript" data-redirect-international-visitors="true" src="//static.goqubit.com/smartserve-4146.js" async defer></script>
                      |  </head>
                      |  <body>
                      |    <header class="header-site-wide"><header>
                      |      header content
                      |    </header></header>
                      |    <div class="cms-content-main-switcher">
                      |      <div class="cms-content-main-holder">
                      |        <main class="cms-content-main">
                      |          <div class="cms-content-main__resp-wrapper"></div>
                      |        </main>
                      |      </div>
                      |    </div>
                      |    <footer class="footer-site-wide"><footer>
                      |      footer content
                      |    </footer></footer>
                      |    <script src="//johnlewis.scene7.com/s7viewers/html5/js/VideoViewer.js" type="text/javascript"></script>
                      |    <script type="text/javascript" src="/assets/js/app.bundle.js"></script>
                      |    <script type="text/javascript" src="/assets/js/qv.bundle.js"></script>
                      |  </body>
                      |</html>""".stripMargin()
        when:
            model.init()
        then:
            model.cssLinks == """<link rel="stylesheet" href="/assets/css/styles.css" />"""
            model.header == """<header class="header-site-wide"><header>\n      header content\n    </header></header>"""
            model.footer == """<footer class="footer-site-wide"><footer>\n      footer content\n    </footer></footer>"""
            model.javascriptAfterFooter == """<script src="//johnlewis.scene7.com/s7viewers/html5/js/VideoViewer.js" type="text/javascript"></script>\n<script type="text/javascript" src="/assets/js/app.bundle.js"></script>\n<script type="text/javascript" src="/assets/js/qv.bundle.js"></script>"""
    }

    def "should return default page parts when 1JL server is not responding"() {
        given:
            previewService.getHtmlPreview(request, resource, null) >> "1JL server is not responding"
        when:
            model.init()
        then:
            model.cssLinks == ""
            model.header == "<header></header>"
            model.footer == "<footer></footer>"
            model.javascriptAfterFooter == ""
    }
}
