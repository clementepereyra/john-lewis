package com.jl.aem.editorial.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.collect.Lists
import com.jl.aem.editorial.service.impl.HubPageSearchService
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.apache.sling.testing.mock.sling.loader.ContentLoader
import org.junit.Rule
import spock.lang.Specification

import static com.jl.aem.editorial.EditorialConstants.EDITORIAL_ROOT_PATH

class ArticlePageModelSpec extends Specification{

    ArticlePageModel articlePageModel

    @Rule
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)

    ResourceResolver resourceResolver
    HubPageSearchService hubPageService
    def mapper = new ObjectMapper()

    def setup(){
        aemContext.addModelsForClasses(ArticlePageModel.class)
        aemContext.load().json('/articlePages.json', EDITORIAL_ROOT_PATH)
        hubPageService = Mock(HubPageSearchService.class)
        aemContext.registerService(HubPageSearchService.class, hubPageService)
        aemContext.addModelsForPackage(ArticlePageModel.class.getPackage().getName())

        resourceResolver = aemContext.resourceResolver()
    }

    def 'should return correct json when service returned correct response'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            def expected = mapper.readTree(ContentLoader.getResourceAsStream("/articlePageModel/contentPageModelResponse.json"))
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.get("articleHeader") == expected.get("articleHeader")
            json.get("articleBody") == expected.get("articleBody")
    }

    def 'should return correct json with no related articles when structure there is no parent hub'(){
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.has("relatedArticles") == false
    }

    def 'should return no next article when current page is the only article under hubPage'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.relatedArticles.relatedArticles.container.has("nextArticle") == false
    }

    def 'should return the first article in related articles if current page is the last'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.relatedArticles.relatedArticles.container.nextArticle.container.tile.path.textValue() == "/content/test_hub/article_1"
    }

    def 'should return all the fields for next article in JSON if all the information is present in article heading'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            def expected = mapper.readTree(ContentLoader.getResourceAsStream("/articlePageModel/nextPageTileWithAllFields.json"))
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.relatedArticles.relatedArticles.container.nextArticle.container.tile == expected.tile
            json.relatedArticles.relatedArticles.container.nextArticle.container.buttontertiary.cmsLinkButtonHref == expected.tile.path
    }

    def 'should return link to hub page in JSON if when hub page exists'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content"),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.relatedArticles.relatedArticles.container.hubPageButton.cmsLinkButtonHref.textValue() == "/content/test_hub"
    }

    def 'should return next article form articles under current parent'() {
        given:
            Resource res = null

        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_not_hub/article_4/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        try {
            model.getRelatedArticles()
        } catch(Exception e) {}

        then:
            1 * hubPageService.getSubArticlesSortedByReplicationDate(_) >> {arguments -> res=arguments[0]}
            res.path == EDITORIAL_ROOT_PATH + "/test_not_hub"
    }

    def 'should fill hub page button href when there is no next article'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.relatedArticles.relatedArticles.container.hubPageButton.cmsLinkButtonHref.textValue() == "/content/test_hub"
    }

    def 'should not throw an exception when there is no relatedArticles node'() {
        when:
            def resource = resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            hubPageService.getSubArticlesSortedByReplicationDate(_) >> Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content")
            ).iterator()
            hubPageService.getClosestHubPage(_) >> resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
            resourceResolver.delete(resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/jcr:content/relatedArticles"))
            def model = resource.adaptTo(ArticlePageModel.class)
        then:
            def json = mapper.valueToTree(model)
            json.has("relatedArticles") == false
            json.has("articleBody") == true
    }

}
