package com.jl.aem.preview

import groovy.json.JsonSlurper
import org.apache.sling.testing.mock.sling.junit.SlingContext
import org.junit.Rule
import spock.lang.Specification

import static org.apache.sling.testing.mock.sling.ResourceResolverType.JCR_MOCK

class JsonConverterSpec extends Specification {

    def jsonConverter = new JsonConverter()

    @Rule
    SlingContext context = new SlingContext(JCR_MOCK)

    def setup() {
        context.load().json("/testpage.json", "/testpage")
    }

    def "container should ignore nested layout container"() {
        given:
        def resource = context.resourceResolver().getResource("/testpage/jcr:content/par/container")

        when:
        def json = new JsonSlurper().parseText(jsonConverter.toJson(resource))

        then:
        json.'sling:resourceType' == "onejl/components/container"
        json.isFullWidth == "true"
        !json.container
    }

    def "container.someComponent should include nested properties"() {
        given:
        def resource = context.resourceResolver().getResource("/testpage/jcr:content/par/container/container/someComponent")

        when:
        def json = new JsonSlurper().parseText(jsonConverter.toJson(resource))

        then:
        json.'sling:resourceType' == "onejl/components/someComponent"
        json.someProperty == "someValue"
        json.moreProperties.anotherProperty == "anotherValue"
    }

    def "non container component should include nested components"() {
        given:
        def resource = context.resourceResolver().getResource("/testpage/jcr:content/par/nonContainer")

        when:
        def json = new JsonSlurper().parseText(jsonConverter.toJson(resource))

        then:
        json.'sling:resourceType' == "onejl/components/nonContainer"
        json.someProperty == "someValue"
        json.nonContainer.someComponent.'sling:resourceType' == "onejl/components/someComponent"
        json.nonContainer.someComponent.someProperty == "someValue"
        json.nonContainer.someComponent.moreProperties.anotherProperty == "anotherValue"
    }
}
