package com.jl.aem.editorial.model.workflow

import com.adobe.granite.ui.components.rendercondition.RenderCondition
import com.adobe.granite.workflow.exec.WorkItem
import com.adobe.granite.workflow.exec.WorkflowData
import com.day.cq.commons.jcr.JcrConstants
import com.google.common.base.Function
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.api.SlingHttpServletRequest
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.junit.Rule
import spock.lang.Specification

import javax.annotation.Nullable

class ArticlePageWorkflowDialogConditionModelSpec extends Specification {

    public static final String WF_INSTANCES = '/etc/workflow/instances/server0/2018-03-29'

    ArticlePageWorkflowDialogConditionModel model

    @Rule
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)

    ResourceResolver resourceResolver
    SlingHttpServletRequest request
    WorkItem workItem = Mock(WorkItem)
    def setup(){
        aemContext.load().json('/workflow/wfinstance.json',
                WF_INSTANCES)
        aemContext.load().json('/workflow/wfmodel.json',
                "/etc/workflow/models/onejl/john-lewis-approval-workflow")
        aemContext.load().json('/workflow/pages.json',
                '/content/1jl/en/content/browse/banners/brands')
        aemContext.addModelsForPackage('com.jl.aem.editorial.model.workflow')
        resourceResolver = aemContext.resourceResolver()
        request = aemContext.request()
    }

    def "should return true if workflow node has link to article page which is already published"(){
        setup:
            Resource resource = aemContext.resourceResolver().getResource(WF_INSTANCES
                    + "/john-lewis-approval-workflow_8/workItems/node1_etc_workflow_instances_server0_2018-03-29_john-lewis-approval-workflow_8")
            request.getParameterMap().put("item", resource.getPath())
            mockWorkflowItem('/content/1jl/en/content/browse/banners/brands/alessi-bnr-250118')
        when:
            model = request.adaptTo(ArticlePageWorkflowDialogConditionModel)
        then:
            model.showCheckbox()
        expect:
            request.getAttribute(RenderCondition.class.getName()).check() == true
    }

    def "should return false if workflow node has link to not article page"(){
        setup:
            Resource resource = aemContext.resourceResolver().getResource(WF_INSTANCES
                    + "/john-lewis-approval-workflow_8/workItems/node1_etc_workflow_instances_server0_2018-03-29_john-lewis-approval-workflow_8")
            request.getParameterMap().put("item", resource.getPath())
            mockWorkflowItem('/content/1jl/en/content/browse/banners/brands/all-saints-bnr-250118')
        when:
            model = request.adaptTo(ArticlePageWorkflowDialogConditionModel)
        then:
            model.showCheckbox()
        expect:
            request.getAttribute(RenderCondition.class.getName()).check() == false
    }

    def "should return false if workflow node has link to article page which is not activated yet"(){
        setup:
            Resource resource = aemContext.resourceResolver().getResource(WF_INSTANCES
                    + "/john-lewis-approval-workflow_8/workItems/node1_etc_workflow_instances_server0_2018-03-29_john-lewis-approval-workflow_8")
            request.getParameterMap().put("item", resource.getPath())
            mockWorkflowItem('/content/1jl/en/content/browse/banners/brands/alessi-bnr')
        when:
            model = request.adaptTo(ArticlePageWorkflowDialogConditionModel)
        then:
            model.showCheckbox()
        expect:
            request.getAttribute(RenderCondition.class.getName()).check() == false
    }

    def mockWorkflowItem(String pathToPage){
        aemContext.registerAdapter(Resource.class, WorkItem.class, new Function<Resource, WorkItem>() {
            @Override
            WorkItem apply(@Nullable Resource resource) {
                if(resource.getValueMap().get(JcrConstants.JCR_PRIMARYTYPE) == "cq:WorkItem") {
                    return workItem
                }
                return null
            }
        })
        WorkflowData workflowData = Mock(WorkflowData)
        workItem.getWorkflowData() >> workflowData
        workflowData.getPayload() >> pathToPage
    }

}
