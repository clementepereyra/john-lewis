package com.jl.aem.preview

import com.github.tomakehurst.wiremock.client.BasicCredentials
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.sling.api.SlingHttpServletRequest
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ValueMap
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.Supplier

import static com.github.tomakehurst.wiremock.client.WireMock.*

class HtmlPreviewServiceImplSpec extends Specification {

    def valueMap = Mock(ValueMap)
    def jsonConverter = Mock(JsonConverter)
    def request = Mock(SlingHttpServletRequest)
    def config = Mock(InfrastructureConfigurator)
    def resource = [getValueMap: { valueMap }, getPath: { "path" }] as Resource

    def previewService = new HtmlPreviewServiceImpl(jsonConverter: jsonConverter)

    @Rule
    WireMockRule wireMockRule = new WireMockRule(8081)

    def setup() {
        config.oneJLHost >> "http://localhost:8081"
        config.projectBHost >> "http://localhost:8081/b"
    }

    def cleanup() {
        previewService.deactivate()
    }

    @Unroll
    def "should get preview from #expectedEndpoint with innerContainers: #innerContainers"() {
        given:
        previewService.activate(config)
        request.parameterMap >> params
        valueMap.get("jcr:primaryType") >> jcrPrimaryType
        jsonConverter.toJson(resource) >> '{"foo":"bar"}'
        givenThat(post(anyUrl()).willReturn(aResponse().withStatus(200).withBody(htmlFromOneJL)))

        when:
        def htmlPreview = previewService.getHtmlPreview(request, resource, innerContainers)

        then:
        htmlPreview == expectedHtmlPreview
        verify(postRequestedFor(urlPathEqualTo(expectedEndpoint)).withRequestBody(equalTo("component=%7B%22foo%22%3A%22bar%22%7D")))

        where:
        params  | jcrPrimaryType    | innerContainers | htmlFromOneJL                       | expectedEndpoint     | expectedHtmlPreview
        [:]     | "nt:unstructured" | null            | "<div></div>"                       | "/renderComponent"   | "<div></div>"
        [:]     | "nt:unstructured" | "/1"            | "<div><cms-stub/></div>"            | "/renderComponent"   | '<div><!--#include virtual="path/1.html?forceeditcontext=true" --></div>'
        [:]     | "nt:unstructured" | "/1,/2"         | "<div><cms-stub/><cms-stub/></div>" | "/renderComponent"   | '<div><!--#include virtual="path/1.html?forceeditcontext=true" --><!--#include virtual="path/2.html?forceeditcontext=true" --></div>'
        [:]     | "cq:PageContent"  | null            | "<div></div>"                       | "/renderPage"        | "<div></div>"
        [:]     | "cq:PageContent"  | "/1"            | "<div><cms-stub/></div>"            | "/renderPage"        | '<div><!--#include virtual="path/1.html?forceeditcontext=true" --></div>'
        [:]     | "cq:PageContent"  | "/1,/2"         | "<div><cms-stub/><cms-stub/></div>" | "/renderPage"        | '<div><!--#include virtual="path/1.html?forceeditcontext=true" --><!--#include virtual="path/2.html?forceeditcontext=true" --></div>'
        [b: ""] | "nt:unstructured" | null            | "<div></div>"                       | "/b/renderComponent" | "<div></div>"
        [b: ""] | "nt:unstructured" | "/1"            | "<div><cms-stub/></div>"            | "/b/renderComponent" | '<div><!--#include virtual="path/1.html?forceeditcontext=true&b" --></div>'
        [b: ""] | "nt:unstructured" | "/1,/2"         | "<div><cms-stub/><cms-stub/></div>" | "/b/renderComponent" | '<div><!--#include virtual="path/1.html?forceeditcontext=true&b" --><!--#include virtual="path/2.html?forceeditcontext=true&b" --></div>'
        [b: ""] | "cq:PageContent"  | null            | "<div></div>"                       | "/b/renderPage"      | "<div></div>"
        [b: ""] | "cq:PageContent"  | "/1"            | "<div><cms-stub/></div>"            | "/b/renderPage"      | '<div><!--#include virtual="path/1.html?forceeditcontext=true&b" --></div>'
        [b: ""] | "cq:PageContent"  | "/1,/2"         | "<div><cms-stub/><cms-stub/></div>" | "/b/renderPage"      | '<div><!--#include virtual="path/1.html?forceeditcontext=true&b" --><!--#include virtual="path/2.html?forceeditcontext=true&b" --></div>'
    }

    @Unroll
    def "should get preview using basic auth"() {
        given:
        config.oneJLUsername >> oneJLUsername
        config.oneJLPassword >> oneJLPassword
        config.projectBUsername >> projectBUsername
        config.projectBPassword >> projectBPassword
        previewService.activate(config)

        request.parameterMap >> params
        jsonConverter.toJson(resource) >> '{"foo":"bar"}'
        givenThat(post(anyUrl()).willReturn(aResponse().withStatus(200).withBody("<div/>")))

        when:
        def htmlPreview = previewService.getHtmlPreview(request, resource, null)

        then:
        htmlPreview == "<div/>"
        verify(expectedOneJlRequest.withRequestBody(equalTo("component=%7B%22foo%22%3A%22bar%22%7D")))

        where:
        params  | oneJLUsername   | oneJLPassword   | projectBUsername   | projectBPassword   | expectedOneJlRequest
        [:]     | null            | null            | null               | null               | postRequestedFor(urlPathEqualTo("/renderComponent")).withoutHeader("Authorization")
        [:]     | "oneJLUsername" | null            | null               | null               | postRequestedFor(urlPathEqualTo("/renderComponent")).withoutHeader("Authorization")
        [:]     | "oneJLUsername" | "oneJLPassword" | null               | null               | postRequestedFor(urlPathEqualTo("/renderComponent")).withBasicAuth(new BasicCredentials("oneJLUsername", "oneJLPassword"))
        [b: ""] | null            | null            | null               | null               | postRequestedFor(urlPathEqualTo("/b/renderComponent")).withoutHeader("Authorization")
        [b: ""] | null            | null            | "projectBUsername" | null               | postRequestedFor(urlPathEqualTo("/b/renderComponent")).withoutHeader("Authorization")
        [b: ""] | null            | null            | "projectBUsername" | "projectBPassword" | postRequestedFor(urlPathEqualTo("/b/renderComponent")).withBasicAuth(new BasicCredentials("projectBUsername", "projectBPassword"))
    }

    @Unroll
    def "should get article preview from #expectedEndpoint with credentials #expectedCredentials"() {
        given:
        config.oneJLUsername >> oneJLUsername
        config.oneJLPassword >> oneJLPassword
        config.projectBUsername >> projectBUsername
        config.projectBPassword >> projectBPassword
        previewService.activate(config)

        request.parameterMap >> params
        jsonConverter.toJson(resource) >> '{"foo":"bar"}'
        givenThat(post(anyUrl()).willReturn(aResponse().withStatus(200).withBody("<div/>")))

        when:
        def htmlPreview = previewService.getHtmlPreview(request, '{"foo":"bar"}')

        then:
        htmlPreview == "<div/>"
        verify(expectedOneJlRequest.withRequestBody(equalTo("component=%7B%22foo%22%3A%22bar%22%7D")))

        where:
        params  | oneJLUsername   | oneJLPassword   | projectBUsername   | projectBPassword   | expectedOneJlRequest
        [:]     | null            | null            | null               | null               | postRequestedFor(urlPathEqualTo("/renderPage")).withoutHeader("Authorization")
        [:]     | "oneJLUsername" | null            | null               | null               | postRequestedFor(urlPathEqualTo("/renderPage")).withoutHeader("Authorization")
        [:]     | "oneJLUsername" | "oneJLPassword" | null               | null               | postRequestedFor(urlPathEqualTo("/renderPage")).withBasicAuth(new BasicCredentials("oneJLUsername", "oneJLPassword"))
        [b: ""] | null            | null            | null               | null               | postRequestedFor(urlPathEqualTo("/b/renderPage")).withoutHeader("Authorization")
        [b: ""] | null            | null            | "projectBUsername" | null               | postRequestedFor(urlPathEqualTo("/b/renderPage")).withoutHeader("Authorization")
        [b: ""] | null            | null            | "projectBUsername" | "projectBPassword" | postRequestedFor(urlPathEqualTo("/b/renderPage")).withBasicAuth(new BasicCredentials("projectBUsername", "projectBPassword"))
    }

    def "activate should throw exception when host urls URL are malformed"() {
        given:
        def config = Mock(InfrastructureConfigurator)
        config.oneJLHost >> oneJLHost
        config.projectBHost >> projectBHost

        when:
        previewService.activate(config)

        then:
        thrown(MalformedURLException)

        where:
        oneJLHost          | projectBHost
        "malformed"        | null
        "malformed"        | "http://localhost"
        null               | "malformed"
        "http://localhost" | "malformed"
        "malformed"        | "malformed"
    }

    def "preview should return error message when jsonConverter throws exception"() {
        given:
        request.parameterMap >> [:]
        jsonConverter.toJson(resource) >> { throw new RuntimeException("message") }
        previewService = new HtmlPreviewServiceImpl(config: config, jsonConverter: jsonConverter)

        when:
        def htmlPreview = previewService.getHtmlPreview(request, resource, null)

        then:
        htmlPreview == "Error getting preview json: message"
    }

    def "preview should return error message when httpclient throws exception"() {
        given:
        request.parameterMap >> [:]
        def httpClient = Mock(CloseableHttpClient)
        httpClient.execute(_, _) >> { throw new RuntimeException("message") }
        previewService = new HtmlPreviewServiceImpl(httpClient: httpClient, config: config, jsonConverter: jsonConverter, httpContextSupplier: Mock(Supplier))

        when:
        def htmlPreview = previewService.getHtmlPreview(request, resource, null)

        then:
        htmlPreview == "Error getting preview html: message"
    }

    def "deactivate should close httpclient"() {
        given:
        def httpClient = Mock(CloseableHttpClient)
        previewService = new HtmlPreviewServiceImpl(httpClient: httpClient)

        when:
        previewService.deactivate()

        then:
        1 * httpClient.close()
    }

    def "deactivate should swallow httpclient.close() errors"() {
        given:
        def httpClient = Mock(CloseableHttpClient)
        previewService = new HtmlPreviewServiceImpl(httpClient: httpClient)

        when:
        previewService.deactivate()

        then:
        1 * httpClient.close() >> { throw new IOException() }
    }
}
