package com.jl.aem.editorial.model.sites.listview.infoprovider

import org.apache.sling.api.SlingHttpServletRequest
import org.junit.Before
import spock.lang.Specification

import static com.jl.aem.editorial.model.sites.listview.infoprovider.ArticlePublishDateListViewColumnModel.SITES_LIST_VIEW_INFO_RENDER_VALUE

class ArticlePublishDateListViewColumnModelTest extends Specification {

    def request = Mock(SlingHttpServletRequest)
    def model = new ArticlePublishDateListViewColumnModel()

    @Before
    def init(){
        model.request = request
    }

    def "should return empty string time in millis if there is no attribute in request"() {
        when:
            request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE) >> null
        then:
            model.getTimeInMillis() == ""
    }

    def "should return null time in millis if there is incorrect attribute in request"() {
        when:
            request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE) >> "incorrect info"
        then:
            model.getTimeInMillis() == ""
    }

    def "should return null time formatted if there is incorrect attribute in request"() {
        when:
            request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE) >> "incorrect info"
        then:
            model.getTimeFormatted() == ""
    }

    def "should return correct time in millis if there is correct attribute in request"() {
        when:
            request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE) >> "1525363943000"
        then:
            model.getTimeInMillis() == "1525363943000"
    }

    def "should return correct time formatted if there is correct attribute in request"() {
        when:
            request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE) >> "1525363943000"
            TimeZone.setDefault(TimeZone.getTimeZone("Europe/London"))
        then:
            model.getTimeFormatted() == "03 May 2018 17:12:23"
    }
}
