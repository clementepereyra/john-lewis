package com.jl.aem.editorial.service.impl

import com.google.common.collect.Lists
import com.jl.aem.common.JLConstants
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.api.resource.Resource
import org.apache.sling.testing.mock.jcr.MockJcr
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.junit.Rule
import spock.lang.Specification

import javax.jcr.Node
import javax.jcr.Session
import javax.jcr.query.Query

import static com.jl.aem.editorial.EditorialConstants.ARTICLE_REPLICATION_DATE_PROPERTY_NAME
import static com.jl.aem.editorial.EditorialConstants.EDITORIAL_ROOT_PATH

class HubPageSearchServiceSpec extends Specification {

    @Rule
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)
    def service = new HubPageSearchService()
    def resourceResolver


    Comparator<Resource> articleReplicationDateCopmarator = { res1, res2 ->
        return res2.getValueMap().get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class)
                .compareTo(res1.getValueMap().get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class))
    }

    void setup() {
        resourceResolver = aemContext.resourceResolver()
        aemContext.load().json('/articlePages.json', EDITORIAL_ROOT_PATH)
        aemContext.registerInjectActivateService(service)
    }

    def "should register inject activate service correctly"(){
        expect:
            aemContext.getService(HubPageSearchService.class) != null
            aemContext.getService(HubPageSearchService.class) == service
    }

    def "should return closest hub page"(){
        given:
            def articlePage = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content")
            def expectedHub = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
        expect:
            expectedHub.getPath() == service.getClosestHubPage(articlePage).getPath()
    }

    def "should return closest hub page if there are no jcr:content under some of parent pages"(){
        given:
            def articlePage = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH + "/test_hub/test_sub_hub_no_jcr_content/article/jcr:content")
            def expectedHub = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH + "/test_hub/jcr:content")
        expect:
            expectedHub.getPath() == service.getClosestHubPage(articlePage).getPath()
    }

    def "should return null if there is no parent hub page recursively"(){
        given:
            aemContext = new AemContext(ResourceResolverType.JCR_MOCK)
            aemContext.load().json('/noArticlePages.json', JLConstants.BASE_AEM_PATH)
            def notArticlePage = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/page1/page2/jcr:content")
        expect:
            service.getClosestHubPage(notArticlePage) == null
    }

    def "should find all the articles under the hub sorted by replication date"(){
        given:
            def hubPage = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH + "/test_hub")
            def queryString = String.format(HubPageSearchService.SORTED_BY_DATE_ARTICLES_QUERY, hubPage.getPath())
            List<Node> queryResult = Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content").adaptTo(Node.class),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content").adaptTo(Node.class)
            )
            MockJcr.setQueryResult(resourceResolver.adaptTo(Session.class), queryString, Query.JCR_SQL2, queryResult)
        when:
            def articles = service.getSubArticlesSortedByReplicationDate(hubPage)
            def articlesList = Lists.newArrayList(articles)
        then:
            articlesList.size() == queryResult.size()
            articlesList.toSorted(articleReplicationDateCopmarator) == articlesList
    }

    def "should find all the articles under the parent hub sorted by replication date"(){
        given:
            def hubPage = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH)
            def queryString = String.format(HubPageSearchService.SORTED_BY_DATE_ARTICLES_QUERY, hubPage.getPath())
            List<Node> queryResult = Lists.newArrayList(
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_1/jcr:content").adaptTo(Node.class),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_2/jcr:content").adaptTo(Node.class),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub/article_3/jcr:content").adaptTo(Node.class),
                    resourceResolver.getResource(EDITORIAL_ROOT_PATH + "/test_hub_2/article_3/jcr:content").adaptTo(Node.class)
            )
            MockJcr.setQueryResult(resourceResolver.adaptTo(Session.class), queryString, Query.JCR_SQL2, queryResult)
        when:
            def articles = service.getSubArticlesSortedByReplicationDate(hubPage)
            def articlesList = Lists.newArrayList(articles)
        then:
            articlesList.size() == queryResult.size()
            articlesList.toSorted(articleReplicationDateCopmarator) == articlesList
    }

    def "should return empty list if there is no editorial pages"(){
        given:
            def hubPage = aemContext.resourceResolver().getResource(EDITORIAL_ROOT_PATH)
            def queryString = String.format(HubPageSearchService.SORTED_BY_DATE_ARTICLES_QUERY, hubPage.getPath())
            List<Node> queryResult = Lists.newArrayList()
            MockJcr.setQueryResult(resourceResolver.adaptTo(Session.class), queryString, Query.JCR_SQL2, queryResult)
        when:
            def articles = service.getSubArticlesSortedByReplicationDate(hubPage)
            def articlesList = Lists.newArrayList(articles)
        then:
            articlesList.size() == queryResult.size()
    }

}
