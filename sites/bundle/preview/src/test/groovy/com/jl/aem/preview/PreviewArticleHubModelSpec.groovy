package com.jl.aem.preview

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.jl.aem.editorial.model.ArticleHubPageModel
import org.apache.sling.api.SlingHttpServletRequest
import org.apache.sling.api.resource.Resource
import spock.lang.Specification

class PreviewArticleHubModelSpec extends Specification {

    PreviewArticleHubModel model = new PreviewArticleHubModel()
    ArticleHubPageModel articleHubModel = Mock(ArticleHubPageModel)
    JsonNode hubPageJson = Mock(JsonNode)

    def setup() {
        model.resource = Mock(Resource)
        model.request = Mock(SlingHttpServletRequest)
        model.previewService = Mock(HtmlPreviewService)
        model.mapper = Mock(ObjectMapper)


    }

    def "should return informative message when cannot adapt to model" () {
        when:
        model.resource.adaptTo(ArticleHubPageModel.class) >> null
        def response = model.getPageHtml()

        then:
        1 * model.resource.adaptTo(ArticleHubPageModel.class)
        response == "there is a problem in content structure of this page"
    }

    def "should return html based on the json form adapted page resource" () {
        when:
        model.resource.adaptTo(ArticleHubPageModel.class) >> articleHubModel
        model.mapper.convertValue(articleHubModel, JsonNode.class) >> hubPageJson
        hubPageJson.toString() >> "json: {}"
        model.previewService.getHtmlPreview(model.request, hubPageJson.toString()) >> "<html></html>"

        def response = model.getPageHtml()

        then:
        response == "<html></html>"
    }
}
