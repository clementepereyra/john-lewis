package com.jl.aem.editorial.model;

import com.adobe.aemds.guide.utils.JcrResourceConstants;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jl.aem.common.JLConstants;
import com.jl.aem.editorial.service.impl.HubPageSearchService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

import static com.day.cq.commons.jcr.JcrConstants.*;

@Model(adaptables = Resource.class)
public abstract class BaseEditorialPageModel {

    final ObjectMapper mapper = new ObjectMapper();
    @Self
    protected Resource resource;

    @ValueMapValue(name = JCR_TITLE)
    @JsonProperty(value = JCR_TITLE, access = JsonProperty.Access.READ_ONLY)
    protected String title;

    @JsonProperty(value = JCR_PRIMARYTYPE, access = JsonProperty.Access.READ_ONLY)
    protected String primaryType() {
        return JcrResourceConstants.CQ_PAGE_CONTENT;
    }

    @ValueMapValue(name = JCR_DESCRIPTION)
    @JsonProperty(value = JCR_DESCRIPTION, access = JsonProperty.Access.READ_ONLY)
    protected String description;

    @JsonProperty(value = JLConstants.CANONICAL_URL, access = JsonProperty.Access.READ_ONLY)
    protected String getDynamicPageCanonicalUrl() {
        Object canonicalUrl = resource.getValueMap().get(JLConstants.CANONICAL_URL);
        return canonicalUrl != null ? canonicalUrl.toString() : generateCanonicalUrl();
    }

    @ValueMapValue(name = "pageTitle")
    @JsonProperty(value = "pageTitle", access = JsonProperty.Access.READ_ONLY)
    protected String pageTitle;

    @OSGiService
    HubPageSearchService hubPageSearchService;

    @PostConstruct
    protected void init() {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    protected abstract String resourceType();

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE", justification = "parent could not be null")
    private String generateCanonicalUrl() {
        return resource.getParent().getPath().replace(JLConstants.BASE_AEM_PATH, "");
    }
}
