package com.jl.aem.editorial.model;

import com.adobe.dam.print.ids.StringConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.jl.aem.common.JLConstants;
import com.jl.aem.common.util.JLPathUtil;
import com.jl.aem.common.util.JsonUtil;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import java.io.IOException;

import static com.day.cq.commons.jcr.JcrConstants.JCR_PRIMARYTYPE;
import static com.day.cq.commons.jcr.JcrConstants.NT_UNSTRUCTURED;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleTileModel {

    public static final String PATH_TO_ARTICLE_RESPONSIVE_CONFIG =
            JLConstants.BASE_AEM_PATH + "/content/jcr:content/articleTileResponsive";
    @Self
    private Resource resource;

    private Resource headingResource;

    private String resourceType;

    @PostConstruct
    private void init() {
        headingResource = resource.getChild("articleHeader/articleHeading");
    }

    @JsonProperty(value = JCR_PRIMARYTYPE)
    public String primaryType() {
        return NT_UNSTRUCTURED;
    }

    @JsonProperty(value = "path")
    public String getPath() {
        return JLPathUtil.get1JLPath(resource.getPath());
    }

    @JsonProperty(value = StringConstants.SLING_RESOURCE_TYPE)
    public String getResourceType() {
        return resourceType;
    }

    ArticleTileModel setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    @JsonProperty(value = "title")
    public String getTitle() {
        return headingResource.getValueMap().get("title", StringUtils.EMPTY);
    }

    @JsonProperty(value = "subtitle")
    public String getSubtitle() {
        String sell = getPageProperty("sell");
        return sell.isEmpty() ? headingResource.getValueMap().get("subtitle", StringUtils.EMPTY) : sell;
    }

    @JsonProperty(value = "imageAlt")
    public String imageAlt() {
        return headingResource.getValueMap().get("imageAlt", StringUtils.EMPTY);
    }

    @JsonProperty(value = "imageSrc")
    public String getImageSrc() {
        return headingResource.getValueMap().get("imageSrc", StringUtils.EMPTY);
    }

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE", justification = "getParent shouldn't return null")
    private String getPageProperty(String propertyName) {
        ValueMap articleProperties = headingResource.getParent().getParent().getValueMap();
        if (articleProperties.containsKey(propertyName))
            return articleProperties.get(propertyName).toString();
        return StringUtils.EMPTY;
    }

    @JsonProperty(value = "responsive")
    public JsonNode getResponsive() throws IOException, JSONException {
        Resource editorialParent = resource.getResourceResolver().getResource(PATH_TO_ARTICLE_RESPONSIVE_CONFIG);
        return editorialParent == null ? null : JsonUtil.fromResource(editorialParent);
    }
}
