package com.jl.aem.editorial.service.impl;

import com.day.crx.JcrConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;

import javax.jcr.query.Query;
import java.util.Iterator;

import static com.jl.aem.editorial.EditorialConstants.ARTICLE_PAGE_RESOURCE_TYPE;
import static com.jl.aem.editorial.EditorialConstants.HUB_PAGE_RESOURCE_TYPE;

@Component(service = HubPageSearchService.class, immediate = true)
public class HubPageSearchService {

    public static final String SORTED_BY_DATE_ARTICLES_QUERY =
            "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([%s]) " +
                    "AND NOT ISSAMENODE([%<s/jcr:content])" +
                    "AND s.[sling:resourceType] = \'" +
                    ARTICLE_PAGE_RESOURCE_TYPE +
                    "\' ORDER BY s.[articleReplicationDate] DESC";

    public Resource getClosestHubPage(Resource articlePage) {
        Resource possibleHub = articlePage.getParent();
        Resource possibleHubContent = null;

        while (possibleHub != null) {
            possibleHubContent = possibleHub.getChild(JcrConstants.JCR_CONTENT);
            if (possibleHubContent != null
                    && StringUtils.contains(possibleHubContent.getResourceType(), HUB_PAGE_RESOURCE_TYPE)) {
                return possibleHubContent;
            }
            possibleHub = possibleHub.getParent();
        }
        return null;
    }

    public Iterator<Resource> getSubArticlesSortedByReplicationDate(Resource hubPage) {
        String query = String.format(SORTED_BY_DATE_ARTICLES_QUERY, hubPage.getPath());
        return hubPage
                .getResourceResolver()
                .findResources(query, Query.JCR_SQL2);

    }

}
