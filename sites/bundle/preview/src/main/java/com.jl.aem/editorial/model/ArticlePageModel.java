package com.jl.aem.editorial.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import com.jl.aem.common.util.DateFormatUtil;
import com.jl.aem.common.util.JLPathUtil;
import com.jl.aem.editorial.EditorialConstants;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static com.adobe.dam.print.ids.StringConstants.SLING_RESOURCE_TYPE;
import static com.jl.aem.common.util.JsonUtil.fromResource;
import static com.jl.aem.editorial.EditorialConstants.*;

@Model(resourceType = {ARTICLE_PAGE_RESOURCE_TYPE, ARTICLE_PAGE_RESOURCE_TYPE_WITH_PREFIX},
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = "json", selector = "infinity", options = {
        @ExporterOption(name = "SerializationFeature.FAIL_ON_EMPTY_BEANS", value = "false")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticlePageModel extends BaseEditorialPageModel {

    @ValueMapValue(name = ARTICLE_REPLICATION_DATE_PROPERTY_NAME)
    private Calendar articleReplicationDate;

    private Resource hubPage;

    @PostConstruct
    protected void init() {
        hubPage = getHubPage();
    }

    @JsonProperty(value = SLING_RESOURCE_TYPE)
    public String resourceType() {
        return ARTICLE_PAGE_RESOURCE_TYPE;
    }

    @JsonProperty(value = ARTICLE_REPLICATION_DATE_PROPERTY_NAME)
    public String getLastReplicated() {
        return DateFormatUtil.format(articleReplicationDate);
    }

    private ArticleTileModel adaptResourceToArticleTile(Resource resource) {
        return Optional
                .ofNullable(resource)
                .map(page -> page.adaptTo(ArticleTileModel.class))
                .orElse(null);
    }

    @JsonProperty(value = "articleBody")
    public JsonNode articleBody() throws JSONException, IOException {
        return fromResource(resource.getChild("articleBody"));
    }

    @JsonProperty(value = "articleHeader")
    public JsonNode articleHeader() throws JSONException, IOException {
        return fromResource(resource.getChild("articleHeader"));
    }

    @JsonProperty(value = "relatedArticles")
    public JsonNode relatedArticles() throws JSONException, IOException {
        if (hubPage == null) {
            return null;
        }
        return getRelatedArticles();
    }

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE", justification = "resource could not be null")
    protected JsonNode getRelatedArticles() throws IOException, JSONException {
        ArticleTileModel nextArticleInfo = getNextArticleTile(resource.getParent());
        Resource relatedArticlesRes = resource.getResourceResolver().getResource(
                EditorialConstants.EDITORIAL_ROOT_PATH + "/jcr:content/relatedArticles");
        if (relatedArticlesRes == null) {
            return null;
        }
        JsonNode relatedArticles = fromResource(relatedArticlesRes);
        if (relatedArticles.isObject()) {
            addHubPageData(relatedArticles);
            if (nextArticleInfo == null) {
                removeNextArticleFromJson(relatedArticles);
            } else {
                addDataToRelatedArticles(relatedArticles, nextArticleInfo);
            }
        }
        return relatedArticles;
    }

    private void addDataToRelatedArticles(JsonNode relatedArticles, ArticleTileModel nextArticleInfo)
            throws IOException, JSONException {
        ObjectNode nextArticleTile = getNextArticleTile(relatedArticles);
        ObjectNode nextArticleButton = getNextArticleButton(relatedArticles);
        nextArticleButton.put("cmsLinkButtonHref", JLPathUtil.get1JLPath(nextArticleInfo.getPath()));
        nextArticleTile.put("path", JLPathUtil.get1JLPath(nextArticleInfo.getPath()));
        nextArticleTile.put("title", nextArticleInfo.getTitle());
        nextArticleTile.put("subtitle", nextArticleInfo.getSubtitle());
        nextArticleTile.put("imageSrc", nextArticleInfo.getImageSrc());

    }

    private void addHubPageData(JsonNode relatedArticles) {
        ObjectNode hubLinkButton = getHubLinkButton(relatedArticles);
        hubLinkButton.put("cmsLinkButtonHref", JLPathUtil.get1JLPath(hubPage.getPath()));
    }

    private ObjectNode getHubLinkButton(JsonNode relatedArticles) {
        return (ObjectNode) relatedArticles.get("relatedArticles").get("container").get("hubPageButton");
    }

    private Resource getHubPage() {
        return hubPageSearchService.getClosestHubPage(resource);
    }

    private ArticleTileModel getNextArticleTile(Resource hubPage) {
        List<Resource> resources = getSortedSubArticles(hubPage);
        if (resources.size() <= 1) {
            return null;
        }
        return getNextArticle(resources);
    }

    private ObjectNode getNextArticleTile(JsonNode relatedArticles) {
        return (ObjectNode) relatedArticles.get("relatedArticles")
                .get("container").get("nextArticle").get("container").get("tile");
    }

    private ArticleTileModel getNextArticle(List<Resource> resources) {
        for (int i = 0; i < resources.size() - 1; i++) {
            if (resource.getPath().equals(resources.get(i).getPath())) {
                return adaptResourceToArticleTile(resources.get(i + 1));
            }
        }
        return adaptResourceToArticleTile(resources.get(0));
    }

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "if service is null - nothing should work")
    private ArrayList<Resource> getSortedSubArticles(Resource hubPage) {
        return Lists.newArrayList(hubPageSearchService
                .getSubArticlesSortedByReplicationDate(hubPage.getParent()));
    }

    private void removeNextArticleFromJson(JsonNode relatedArticles) {
        ((ObjectNode) relatedArticles.get("relatedArticles").get("container")).remove("nextArticle");
    }

    private ObjectNode getNextArticleButton(JsonNode relatedArticles) {
        return (ObjectNode) relatedArticles.get("relatedArticles")
                .get("container").get("nextArticle").get("container").get("buttontertiary");
    }
}
