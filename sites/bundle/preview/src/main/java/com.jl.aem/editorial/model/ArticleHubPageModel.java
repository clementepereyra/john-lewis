package com.jl.aem.editorial.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jl.aem.common.JLConstants;
import com.jl.aem.editorial.EditorialConstants;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

import java.util.*;

import static com.adobe.dam.print.ids.StringConstants.SLING_RESOURCE_TYPE;
import static com.day.cq.commons.jcr.JcrConstants.JCR_PRIMARYTYPE;
import static com.day.cq.commons.jcr.JcrConstants.NT_UNSTRUCTURED;
import static com.day.cq.replication.ReplicationStatus.NODE_PROPERTY_LAST_REPLICATED;
import static com.jl.aem.editorial.EditorialConstants.HUB_PAGE_RESOURCE_TYPE;
import static com.jl.aem.editorial.EditorialConstants.HUB_PAGE_RESOURCE_TYPE_WITH_PREFIX;


@Model(resourceType = {HUB_PAGE_RESOURCE_TYPE, HUB_PAGE_RESOURCE_TYPE_WITH_PREFIX},
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = "json", selector = "infinity", options = {
        @ExporterOption(name = "SerializationFeature.FAIL_ON_EMPTY_BEANS", value = "false")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleHubPageModel extends BaseEditorialPageModel {

    public static final String ARTICLE_HERO = "articleHero";
    public static final String ARTICLES = "articles";

    private Resource heroArticleResource;

    private List<Resource> articles;

    @ValueMapValue(name = NODE_PROPERTY_LAST_REPLICATED)
    private Calendar lastReplicated;

    @PostConstruct
    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE", justification = "resource could not be null")
    protected void init() {
        articles = new ArrayList<>();
        Iterator<Resource> articleResourceIterator = hubPageSearchService
                .getSubArticlesSortedByReplicationDate(resource.getParent());
        if (articleResourceIterator.hasNext()) {
            heroArticleResource = articleResourceIterator.next();
        }
        while (articleResourceIterator.hasNext()) {
            articles.add(articleResourceIterator.next());
        }
    }

    @JsonProperty(value = ARTICLE_HERO)
    private JsonNode getHeroArticle() {
        ArticleTileModel articleTileModel = Optional.ofNullable(heroArticleResource)
                .map(res -> res.adaptTo(ArticleTileModel.class))
                .orElse(null);
        if (articleTileModel == null) {
            return null;
        }
        return setupHeroArticleJson(articleTileModel);
    }

    private ObjectNode setupHeroArticleJson(ArticleTileModel articleTileModel) {
        articleTileModel.setResourceType(EditorialConstants.ARTICLE_HERO_RESOURCE_TYPE);
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put(SLING_RESOURCE_TYPE, JLConstants.STUB_FOR_1JL_MAPPER_RESOURCE_TYPE);
        objectNode.put(JCR_PRIMARYTYPE, NT_UNSTRUCTURED);
        objectNode.set(ARTICLE_HERO, mapper.valueToTree(articleTileModel));
        return objectNode;
    }

    @JsonProperty(value = ARTICLES)
    private JsonNode getArticles() {
        LinkedHashMap<String, ArticleTileModel> result = new LinkedHashMap<>();
        if (articles.isEmpty()) {
            return null;
        }
        for (int index = 0; index < articles.size(); index++) {
            ArticleTileModel articleTileModel = articles.get(index).adaptTo(ArticleTileModel.class);
            if (articleTileModel == null) {
                continue;
            }
            articleTileModel.setResourceType(EditorialConstants.ARTICLE_TILE_RESOURCE_TYPE);
            result.put("article_" + index, articleTileModel);
        }
        ObjectNode objectNode = mapper.valueToTree(result);
        objectNode.put(SLING_RESOURCE_TYPE, JLConstants.STUB_FOR_1JL_MAPPER_RESOURCE_TYPE);
        objectNode.put(JCR_PRIMARYTYPE, NT_UNSTRUCTURED);
        return objectNode;
    }

    @JsonProperty(value = SLING_RESOURCE_TYPE)
    public String resourceType() {
        return HUB_PAGE_RESOURCE_TYPE;
    }

}
