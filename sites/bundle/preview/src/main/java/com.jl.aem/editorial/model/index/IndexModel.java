package com.jl.aem.editorial.model.index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.jl.aem.common.JLConstants;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.jcr.query.Query;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.day.cq.commons.jcr.JcrConstants.JCR_DESCRIPTION;
import static com.day.cq.commons.jcr.JcrConstants.JCR_TITLE;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IndexModel {

    final Map<String, List<String>> textProps = new HashMap<>();

    private static final String FIND_COMPONENTS_QUERY =
            "SELECT * FROM [nt:unstructured] AS s " +
                    "WHERE ISDESCENDANTNODE([%s]) " +
                    "AND s.[sling:resourceType] IS NOT NULL";

    @Self
    protected Resource resource;

    @PostConstruct
    private void init() {
        textProps.put("onejl/components/accordionItem",
                Lists.newArrayList("dynamicAccordionDescription", "dynamicAccordionTitle"));
        textProps.put("onejl/components/dosAndDonts", Lists.newArrayList("cmsDosItems", "cmsDontsItema"));
        textProps.put("onejl/components/youtubeVideo", Lists.newArrayList("videoTitle", "videoDescription"));
        textProps.put("onejl/components/scene7Video", Lists.newArrayList("videoTitle", "videoDescription"));
        textProps.put("onejl/components/title", Lists.newArrayList("title"));
        textProps.put("onejl/components/copy", Lists.newArrayList("text"));
        textProps.put("onejl/components/editorial/copy", Lists.newArrayList("text"));
    }

    @JsonProperty("url")
    public String getURL() {
        return resource.getPath()
                .replace(JLConstants.BASE_AEM_PATH, "")
                .replace("/jcr:content", "");
    }

    @JsonProperty("jcr:title")
    public String getJcrTitle() {
        return resource.getValueMap().get(JCR_TITLE, "").trim();
    }

    @JsonProperty("jcr:description")
    public String getJcrDescription() {
        return resource.getValueMap().get(JCR_DESCRIPTION, "").replaceAll("[\\r\\n]", "").trim();
    }

    @JsonProperty("pageTitle")
    public String getPageTitle() {
        return resource.getValueMap().get("pageTitle", "").trim();
    }

    @JsonProperty("sell")
    public String getSell() {
        return resource.getValueMap().get("sell", "").trim();
    }

    @JsonProperty("subtitle")
    public String getSubtitle() {
        return Optional.ofNullable(resource.getChild("articleHeader/articleHeading"))
                .map(Resource::getValueMap)
                .map(valueMap -> valueMap.get("subtitle", ""))
                .orElse("").trim();
    }

    @JsonProperty("title")
    public String getTitle() {
        return Optional.ofNullable(resource.getChild("articleHeader/articleHeading"))
                .map(Resource::getValueMap)
                .map(valueMap -> valueMap.get("title", ""))
                .orElse("").trim();
    }

    @JsonProperty("content")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<String> getContent() {
        return Lists.newArrayList(resource.getChildren())
                .stream()
                .map(this::getTextProps)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    List<String> getTextProps(Resource containerComponent) {
        if (containerComponent == null || !isContainer(containerComponent)) {
            return Lists.newArrayList();
        }
        String query = String.format(FIND_COMPONENTS_QUERY, containerComponent.getPath());
        Iterator<Resource> resources = containerComponent.getResourceResolver()
                .findResources(query, Query.JCR_SQL2);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(resources, Spliterator.ORDERED), false)
                .map(this.mapper())
                .flatMap(List::stream)
                .map(String::trim)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    private Function<Resource, List<String>> mapper() {
        return new ComponentToTextPropertiesMapper();
    }

    private List<String> propsNames(Resource component) {
        List<String> textPropsNames = textProps.get(component.getResourceType());
        if (isEmpty(textPropsNames)) {
            return Collections.emptyList();
        }
        return textPropsNames;
    }

    class ComponentToTextPropertiesMapper implements Function<Resource, List<String>> {
        @Override
        public List<String> apply(Resource component) {
            List<String> textPropsNames = propsNames(component);
            return textPropsNames.stream()
                    .map(propName -> component.getValueMap().get(propName))
                    .map(this::propertyToList)
                    .flatMap(List::stream)
                    .map(s -> s.replaceAll("<.*?>", "").replaceAll("[\\r\\n]", " "))
                    .map(StringEscapeUtils::unescapeHtml4)
                    .collect(Collectors.toList());
        }

        private List<String> propertyToList(Object prop) {
            if (prop instanceof String[]) {
                return Arrays.asList((String[]) prop);
            }
            return Lists.newArrayList(prop.toString());
        }
    }

    @JsonIgnore
    private boolean isContainer(Resource component) {
        return component.isResourceType("wcm/foundation/components/responsivegrid")
                || component.isResourceType("foundation/components/parsys");
    }

}
