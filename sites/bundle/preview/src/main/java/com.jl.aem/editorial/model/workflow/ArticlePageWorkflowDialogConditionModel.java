package com.jl.aem.editorial.model.workflow;

import com.adobe.dam.print.ids.StringConstants;
import com.adobe.granite.ui.components.rendercondition.RenderCondition;
import com.adobe.granite.ui.components.rendercondition.SimpleRenderCondition;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationStatus;
import com.jl.aem.editorial.EditorialConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.inject.Inject;
import java.util.Optional;

@Model(adaptables = SlingHttpServletRequest.class)
public class ArticlePageWorkflowDialogConditionModel {

    @Inject
    private SlingHttpServletRequest request;

    @SlingObject
    private ResourceResolver resourceResolver;

    public void showCheckbox() {
        Resource workItemResource = resourceResolver.getResource(request.getParameter("item"));
        Resource pageContentResource = pageContentResource(workItemResource);
        request.setAttribute(RenderCondition.class.getName(), new SimpleRenderCondition(getVote(pageContentResource)));
    }

    private Resource pageContentResource(Resource workItemResource) {
        String pathToPage = Optional.ofNullable(workItemResource)
                .map(resource -> resource.adaptTo(WorkItem.class))
                .map(WorkItem::getWorkflowData)
                .map(WorkflowData::getPayload)
                .map(Object::toString)
                .orElse(StringUtils.EMPTY);
        return Optional.ofNullable(resourceResolver.getResource(pathToPage))
                .map(resource -> resource.getChild(JcrConstants.JCR_CONTENT))
                .orElse(null);
    }

    private boolean getVote(Resource pageContentResource) {
        return (pageContentResource != null)
                && isArticlePageResourceType(pageContentResource)
                && hasLastReplicated(pageContentResource);
    }

    private boolean isArticlePageResourceType(Resource pageContentResource) {
        return StringUtils.contains(
                pageContentResource.getValueMap().get(StringConstants.SLING_RESOURCE_TYPE, ""),
                EditorialConstants.ARTICLE_PAGE_RESOURCE_TYPE);
    }

    private boolean hasLastReplicated(Resource pageContentResource) {
        return pageContentResource.getValueMap().containsKey(ReplicationStatus.NODE_PROPERTY_LAST_REPLICATED);
    }

}
