package com.jl.aem.editorial.model.sites.listview.infoprovider;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageInfoProvider;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.Optional;

import static com.jl.aem.editorial.EditorialConstants.ARTICLE_REPLICATION_DATE_PROPERTY_NAME;
import static com.jl.aem.editorial.model.sites.listview.infoprovider.ArticlePublishDatePageInfoProvider.PROVIDER_TYPE;

@Component(service = PageInfoProvider.class,
        property = {"pageInfoProviderType=sites.listView.info.provider." + PROVIDER_TYPE})
public class ArticlePublishDatePageInfoProvider implements PageInfoProvider {

    public static final String PROVIDER_TYPE = "article";

    public void updatePageInfo(SlingHttpServletRequest request, JSONObject info, Resource resource)
            throws JSONException {
        Page page = resource.adaptTo(Page.class);
        JSONObject myProjectInfo = new JSONObject();
        Long date = Optional.ofNullable(page)
                .map(Page::getContentResource)
                .map(Resource::getValueMap)
                .map(valueMap -> valueMap.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class))
                .map(Date::getTime)
                .orElse(null);
        myProjectInfo.put(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, date);

        info.put(PROVIDER_TYPE, myProjectInfo);
    }
}
