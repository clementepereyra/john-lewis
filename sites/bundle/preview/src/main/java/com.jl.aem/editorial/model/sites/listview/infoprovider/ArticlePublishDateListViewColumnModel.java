package com.jl.aem.editorial.model.sites.listview.infoprovider;

import com.jl.aem.common.util.DateFormatUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import java.util.Optional;

@Model(adaptables = SlingHttpServletRequest.class)
public class ArticlePublishDateListViewColumnModel {

    public static final String SITES_LIST_VIEW_INFO_RENDER_VALUE = "sites.listView.info.render.value";

    @Self
    SlingHttpServletRequest request;

    public String getTimeInMillis() {
        return Optional.ofNullable(request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE))
                .map(Object::toString)
                .filter(StringUtils::isNumeric)
                .orElse("");
    }

    public String getTimeFormatted() {
        return Optional.ofNullable(request.getAttribute(SITES_LIST_VIEW_INFO_RENDER_VALUE))
                .map(Object::toString)
                .filter(StringUtils::isNumeric)
                .map(Long::parseLong)
                .map(DateFormatUtil::formatArticlePublishDate)
                .orElse("");
    }
}
