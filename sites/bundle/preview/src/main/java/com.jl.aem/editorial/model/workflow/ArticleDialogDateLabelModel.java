package com.jl.aem.editorial.model.workflow;


import com.adobe.granite.workflow.exec.WorkItem;
import com.day.cq.commons.jcr.JcrConstants;
import com.jl.aem.common.util.DateFormatUtil;
import com.jl.aem.editorial.EditorialConstants;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import java.util.Calendar;
import java.util.Optional;

@Model(adaptables = SlingHttpServletRequest.class)
public class ArticleDialogDateLabelModel {

    @Self
    private SlingHttpServletRequest request;

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "workflowResource is adaptable to workitem")
    public String getDate() {
        String itemPath = request.getParameter("item");
        Resource workflowResource = request.getResourceResolver().getResource(itemPath);
        String payloadPath = workflowResource.adaptTo(WorkItem.class).getWorkflowData().getPayload().toString();
        Resource pageResource = request.getResourceResolver().getResource(payloadPath);
        return Optional.ofNullable(pageResource)
                .map(resource -> resource.getChild(JcrConstants.JCR_CONTENT))
                .map(Resource::getValueMap)
                .map(valueMap -> valueMap.get(EditorialConstants.ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Calendar.class))
                .map(Calendar::getTimeInMillis)
                .map(DateFormatUtil::formatArticlePublishDate)
                .orElse(StringUtils.EMPTY);
    }

}
