package com.jl.aem.preview.apps.carousel;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class CarouselModel {

    private static final Logger LOG = LoggerFactory.getLogger(CarouselModel.class);

    @Inject
    List<ImageModel> images;

    public List<ImageModel> getImages() {
        return images;
    }

}
