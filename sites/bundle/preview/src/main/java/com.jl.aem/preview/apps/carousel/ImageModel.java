package com.jl.aem.preview.apps.carousel;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class ImageModel {

    @Inject
    AppsImageModel apps;

    @Inject
    String cmsImageSrc;

    public AppsImageModel getApps() {
        return apps;
    }

    public String getCmsImageSrc() {
        return cmsImageSrc;
    }
}
