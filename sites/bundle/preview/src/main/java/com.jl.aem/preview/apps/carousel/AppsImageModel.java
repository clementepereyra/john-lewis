package com.jl.aem.preview.apps.carousel;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AppsImageModel {

    @Inject
    String campaignID;

    @Inject
    String facetID;

    @Inject
    String iPhoneCarouselImage;

    @Inject
    String iPadCarouselImage;

    @Inject
    String term;

    @Inject
    String type;

    public String getCampaignID() {
        return campaignID;
    }

    public String getFacetID() {
        return facetID;
    }

    public String getIPhoneCarouselImage() {
        return iPhoneCarouselImage;
    }

    public String getIPadCarouselImage() {
        return iPadCarouselImage;
    }

    public String getTerm() {
        return term;
    }

    public String getType() {
        return type;
    }
}
