package com.jl.aem.preview;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jl.aem.editorial.model.ArticleHubPageModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;


@Model(adaptables = SlingHttpServletRequest.class)
public class PreviewArticleHubModel {

    @Inject
    private HtmlPreviewService previewService;

    ObjectMapper mapper = new ObjectMapper();

    @Inject
    protected Resource resource;

    @Inject
    protected SlingHttpServletRequest request;

    public String getPageHtml() {
        ArticleHubPageModel hubPage = resource.adaptTo(ArticleHubPageModel.class);

        if (hubPage == null) {
            return "there is a problem in content structure of this page";
        }
        JsonNode hubPageJson = mapper.convertValue(hubPage, JsonNode.class);

        return previewService.getHtmlPreview(request, hubPageJson.toString());
    }
}

