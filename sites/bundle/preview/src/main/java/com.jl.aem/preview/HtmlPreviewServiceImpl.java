package com.jl.aem.preview;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.http.auth.AuthScope.ANY_REALM;
import static org.apache.http.client.config.AuthSchemes.BASIC;

@Component(service = HtmlPreviewService.class)
@Designate(ocd = InfrastructureConfigurator.class)
public class HtmlPreviewServiceImpl implements HtmlPreviewService {

    private static final Logger LOG = LoggerFactory.getLogger(HtmlPreviewServiceImpl.class);

    private static final String RENDER_PAGE_SERVICE_URL = "/renderPage";
    private static final String RENDER_COMPONENT_SERVICE_URL = "/renderComponent";
    private static final String STUB_TAG = "<cms-stub/>";
    private static final String SEPARATOR = ",";
    private static final String COMPONENT_PARAM = "component";

    private static final int CONNECTION_TIMEOUT = 5000;
    private static final int MAX_TOTAL_REQUESTS = 200000;
    private static final int MAX_REQUESTS_PER_ROUTE = 20000;
    private static final String SLING_DYNAMIC_INCLUDE_TEMPLATE = "<!--#include virtual=\"%s%s.html?forceeditcontext=true\" -->";
    private static final String SLING_DYNAMIC_INCLUDE_TEMPLATE_B = "<!--#include virtual=\"%s%s.html?forceeditcontext=true&b\" -->";

    private InfrastructureConfigurator config;

    private CloseableHttpClient httpClient;
    private Supplier<HttpClientContext> httpContextSupplier;
    private JsonConverter jsonConverter = new JsonConverter();

    @Activate
    public void activate(InfrastructureConfigurator config) throws MalformedURLException {
        this.config = config;
        this.httpClient = httpClient();
        this.httpContextSupplier = new HttpClientContextSupplier(config);
    }

    @Deactivate
    public void deactivate() {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (Exception exception) {
                LOG.warn("Could not close http client: ", exception);
            }
        }
    }

    public String getHtmlPreview(SlingHttpServletRequest request, Resource resource, String innerContainers) {
        try {
            boolean projectB = isProjectB(request);
            String serviceUrl = isPage(resource) ? RENDER_PAGE_SERVICE_URL : RENDER_COMPONENT_SERVICE_URL;
            String oneJlEndpoint = oneJlBaseUrl(projectB) + serviceUrl;
            return getHtmlPreview(oneJlEndpoint, jsonConverter.toJson(resource), resource.getPath(), innerContainers, projectB);
        } catch (Exception exception) {
            String message = "Error getting preview json: " + exception.getMessage();
            LOG.error(message);
            return message;
        }
    }

    public String getHtmlPreview(SlingHttpServletRequest request, String json) {
        boolean projectB = isProjectB(request);
        String oneJlEndpoint = oneJlBaseUrl(projectB) + RENDER_PAGE_SERVICE_URL;
        return getHtmlPreview(oneJlEndpoint, json, "", null, projectB);
    }

    private String getHtmlPreview(String oneJlEndpoint, String json, String resourcePath, String innerContainers, boolean projectB) {
        HttpResponse response = null;

        try {
            HttpPost post = httpPost(oneJlEndpoint, json);
            response = httpClient.execute(post, httpContextSupplier.get());
            return parseResponse(response, resourcePath, innerContainers, projectB);
        } catch (Exception exception) {
            String message = "Error getting preview html: " + exception.getMessage();
            LOG.error(message);
            return message;
        } finally {
            quietlyConsumeAndClose(response);
        }
    }

    private String parseResponse(HttpResponse response, String path, String innerContainers, boolean projectB) throws IOException {
        String responseStr = IOUtils.toString(response.getEntity().getContent(), UTF_8);
        return replaceStubTagsWithSDIs(responseStr, path, innerContainers, projectB);
    }

    private HttpPost httpPost(String oneJlEndpoint, String json) throws UnsupportedEncodingException {
        HttpPost post = new HttpPost(oneJlEndpoint);
        List<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair(COMPONENT_PARAM, json));
        post.setEntity(new UrlEncodedFormEntity(postParameters, UTF_8));
        return post;
    }

    private boolean isPage(Resource resource) {
        return "cq:PageContent".equals(resource.getValueMap().get("jcr:primaryType"));
    }

    private String replaceStubTagsWithSDIs(String responseStr, String path, String innerContainers, boolean projectB) {
        if (innerContainers != null) {
            for (String innerContainer : innerContainers.split(SEPARATOR)) {
                String slingDynamicIncludeTemplate = projectB ? SLING_DYNAMIC_INCLUDE_TEMPLATE_B : SLING_DYNAMIC_INCLUDE_TEMPLATE;
                String replacement = String.format(slingDynamicIncludeTemplate, path, innerContainer);
                responseStr = responseStr.replaceFirst(STUB_TAG, replacement);
            }
        }
        return responseStr;
    }

    private String oneJlBaseUrl(boolean projectB) {
        return projectB ? config.getProjectBHost() : config.getOneJLHost();
    }

    private boolean isProjectB(SlingHttpServletRequest request) {
        return request.getParameterMap().containsKey("b");
    }

    private void quietlyConsumeAndClose(HttpResponse response) {
        if (response != null) {
            try {
                EntityUtils.consume(response.getEntity());
            } catch (IOException ignore) {
            }
        }
    }

    private static CloseableHttpClient httpClient() {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(CONNECTION_TIMEOUT)
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(MAX_TOTAL_REQUESTS);
        connectionManager.setDefaultMaxPerRoute(MAX_REQUESTS_PER_ROUTE);

        return HttpClients.custom()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(defaultRequestConfig)
                .build();
    }

    private static class HttpClientContextSupplier implements Supplier<HttpClientContext> {

        private final AuthCache authCache = new BasicAuthCache();
        private final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        private HttpClientContextSupplier(InfrastructureConfigurator config) throws MalformedURLException {
            configureHost(config.getOneJLHost(), config.getOneJLUsername(), config.getOneJLPassword());
            configureHost(config.getProjectBHost(), config.getProjectBUsername(), config.getProjectBPassword());
        }

        @Override
        public HttpClientContext get() {
            HttpClientContext context = HttpClientContext.create();
            context.setCredentialsProvider(credentialsProvider);
            context.setAuthCache(authCache);
            return context;
        }

        private void configureHost(String baseUrl, String username, String password) throws MalformedURLException {
            URL url = isBlank(baseUrl) ? null : new URL(baseUrl);
            if (url == null || isBlank(username) || isBlank(password)) {
                return;
            }

            int port = url.getPort() == -1 ? url.getDefaultPort() : url.getPort();
            authCache.put(new HttpHost(url.getHost(), port, url.getProtocol()), new BasicScheme());
            credentialsProvider.setCredentials(
                    new AuthScope(url.getHost(), port, ANY_REALM, BASIC),
                    new UsernamePasswordCredentials(username, password)
            );
        }
    }
}
