package com.jl.aem.preview;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import static org.osgi.service.metatype.annotations.AttributeType.PASSWORD;

@ObjectClassDefinition(name = "Infrastructure Configuration", description = "Service Configuration")
public @interface InfrastructureConfigurator {

    @AttributeDefinition(name = "OneJL Host", description = "Host of OneJL server for instance http://localhost:8080")
    String getOneJLHost();

    @AttributeDefinition(
            name = "OneJL Username",
            description = "Optional username for OneJL",
            required = false
    )
    String getOneJLUsername();

    @AttributeDefinition(
            name = "OneJL Password",
            description = "Optional password for OneJL. Encrypt plain text using system/console/crypto and paste here",
            required = false,
            type = PASSWORD
    )
    String getOneJLPassword();

    @AttributeDefinition(
            name = "Project B Host",
            description = "Optional host of Project B OneJL server for instance https://testb.onejl.uk",
            required = false
    )
    String getProjectBHost();

    @AttributeDefinition(
            name = "Project B Username",
            description = "Optional username for OneJL",
            required = false
    )
    String getProjectBUsername();

    @AttributeDefinition(
            name = "Project B Password",
            description = "Optional password for OneJL. Encrypt plain text using system/console/crypto and paste here",
            required = false,
            type = PASSWORD
    )
    String getProjectBPassword();
}
