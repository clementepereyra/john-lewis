package com.jl.aem.preview;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;


@Model(adaptables = SlingHttpServletRequest.class)
public class PreviewModel {

    @Inject
    private HtmlPreviewService previewService;

    @Inject
    @Optional
    protected String innerContainers;

    @Inject
    protected Resource resource;

    @Inject
    protected SlingHttpServletRequest request;

    public String getComponentHtml() {
        return previewService.getHtmlPreview(request, resource, innerContainers);
    }
}

