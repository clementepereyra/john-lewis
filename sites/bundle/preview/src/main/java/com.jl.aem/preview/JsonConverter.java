package com.jl.aem.preview;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.sling.JsonObjectCreator;

import javax.jcr.RepositoryException;
import java.util.Iterator;

class JsonConverter {

    String toJson(Resource resource) throws JSONException, RepositoryException {
        JSONObject jsonObject = JsonObjectCreator.create(resource, -1);
        removeNestedComponents(jsonObject);
        return jsonObject.toString();

    }

    private void removeNestedComponents(JSONObject jsonObject) {
        Iterator<String> keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            JSONObject property = jsonObject.optJSONObject(key);
            if (property != null) {
                if (property.has("sling:resourceType")) {
                    keys.remove();
                }
            }
        }
    }
}
