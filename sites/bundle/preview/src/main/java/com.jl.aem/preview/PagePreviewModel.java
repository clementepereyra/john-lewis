package com.jl.aem.preview;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.*;

@Model(adaptables = SlingHttpServletRequest.class)
public class PagePreviewModel {

    private static final Pattern JAVASCRIPT_PATTERN = Pattern.compile(
            "(?<javascript><script\\s+[^>]*type=\"text/javascript\".+?</script>)",
            CASE_INSENSITIVE + MULTILINE + DOTALL);
    private static final Pattern CSS_PATTERN = Pattern.compile(
            "(?<css><link\\s+[^>]*rel=\"stylesheet\".*?>)",
            CASE_INSENSITIVE + MULTILINE + DOTALL);
    private static final Pattern PAGE_PATTERN = Pattern.compile(
            "(?<beforeHeader>.*?)(?<header><header.*</header>).*?(?<footer><footer.*</footer>)(?<afterFooter>.*)",
            CASE_INSENSITIVE + MULTILINE + DOTALL);

    @Inject
    private HtmlPreviewService previewService;

    @Inject
    protected Resource resource;

    @Inject
    protected SlingHttpServletRequest request;

    private String cssLinks = "";
    private String header = "<header></header>";
    private String footer = "<footer></footer>";
    private String javascriptAfterFooter = "";

    @PostConstruct
    protected void init() {
        String htmlPreview = previewService.getHtmlPreview(request, resource, null);
        Matcher pageMatcher = PAGE_PATTERN.matcher(htmlPreview);
        if (pageMatcher.matches()) {
            header = pageMatcher.group("header").trim();
            footer = pageMatcher.group("footer").trim();
            cssLinks = findCssLinks(pageMatcher.group("beforeHeader")).trim();
            javascriptAfterFooter = findJavascript(pageMatcher.group("afterFooter")).trim();
        }
    }

    public String getCssLinks() {
        return cssLinks;
    }

    public String getHeader() {
        return header;
    }

    public String getFooter() {
        return footer;
    }

    public String getJavascriptAfterFooter() {
        return javascriptAfterFooter;
    }

    private String findCssLinks(String html) {
        StringBuilder cssLinks = new StringBuilder();
        Matcher matcher = CSS_PATTERN.matcher(html);
        while (matcher.find()) {
            String cssLink = matcher.group("css");
            if (!cssLink.contains("legacy-ie.css")) {
                cssLinks.append(cssLink).append("\n");
            }
        }
        return cssLinks.toString();
    }

    private String findJavascript(String html) {
        StringBuilder javascriptLinks = new StringBuilder();
        Matcher matcher = JAVASCRIPT_PATTERN.matcher(html);
        while (matcher.find()) {
            String javascriptLink = matcher.group("javascript");
            javascriptLinks.append(javascriptLink).append("\n");
        }
        return javascriptLinks.toString();
    }
}
