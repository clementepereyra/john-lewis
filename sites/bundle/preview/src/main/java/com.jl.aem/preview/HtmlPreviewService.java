package com.jl.aem.preview;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

public interface HtmlPreviewService {

    String getHtmlPreview(SlingHttpServletRequest request, Resource resource, String innerContainers);

    String getHtmlPreview(SlingHttpServletRequest request, String json);
}
