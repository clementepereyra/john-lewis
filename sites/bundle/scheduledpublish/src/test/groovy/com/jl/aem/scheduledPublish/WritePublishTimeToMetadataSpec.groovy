package com.jl.aem.scheduledpublish

import com.adobe.granite.workflow.WorkflowSession
import com.adobe.granite.workflow.exec.WorkItem
import com.adobe.granite.workflow.exec.WorkflowData
import com.adobe.granite.workflow.metadata.MetaDataMap
import com.jl.aem.common.JLConstants
import io.wcm.testing.mock.aem.junit.AemContext
import org.apache.sling.testing.mock.sling.ResourceResolverType
import org.junit.Rule
import spock.lang.Specification

import javax.jcr.Session

import static com.jl.aem.editorial.EditorialConstants.ARTICLE_REPLICATION_DATE_PROPERTY_NAME
import static com.jl.aem.editorial.EditorialConstants.REWRITE_ARTICLE_PUBLISH_DATE
import static com.jl.aem.scheduledpublish.WritePublishTimeToMetadata.ABSOLUTE_TIME

class WritePublishTimeToMetadataSpec extends Specification {

    @Rule
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK)

    WorkItem workItem = Mock(WorkItem)
    WorkflowSession workflowSession = Mock(WorkflowSession)
    MetaDataMap metaDataMap = Mock(MetaDataMap)


    WritePublishTimeToMetadata process = new WritePublishTimeToMetadata()

    Session session

    def setup(){
        aemContext.load().json("/payloadPages.json", JLConstants.BASE_AEM_PATH)
        session = aemContext.resourceResolver().adaptTo(Session)
        setupWFSession()
    }

    def 'should put absoluteTime to workflow metadata'(){
        setup:
            def expectedTimeMillis = 1517495528000
            setupWorkItem('oh-buggies-travel')
        when:
            process.execute(workItem, workflowSession, metaDataMap)
        then:
            1 * metaDataMap.put(ABSOLUTE_TIME, _) >> {
                args -> assert args[1] == expectedTimeMillis
            }
    }

    def 'should not throw an exception when content structure is corrupted'(){
        when:
            setupWorkItem('5000010-oh')
        then:
            process.execute(workItem, workflowSession, metaDataMap)
    }

    def 'should not throw exception when cannot adapt wfsession to session'(){
        setup:
            setupWorkItem('oh-buggies-travel')
        when:
            workflowSession.adaptTo(Session) >> null
        then:
            process.execute(workItem, workflowSession, metaDataMap)
    }

    def 'should add custom property for an article page when it is published for the first time'(){
        setup:
            setupWorkItem('article1')
        when:
            process.execute(workItem, workflowSession, metaDataMap)
        then:
            def valueMap = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/article1/jcr:content").getValueMap()
            valueMap.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME) != null
            valueMap.get(REWRITE_ARTICLE_PUBLISH_DATE) == null
    }

    def 'should rewrite custom property for an article page when node has rewrite property set to true'(){
        setup:
            setupWorkItem('article2')
            def valueMapBefore = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/article2/jcr:content").getValueMap()
            def previousDate = valueMapBefore.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class).getTime()
            def rewrite = valueMapBefore.get(REWRITE_ARTICLE_PUBLISH_DATE)
        when:
            process.execute(workItem, workflowSession, metaDataMap)
        then:
            def valueMapAfter = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/article2/jcr:content").getValueMap()
            rewrite == true
            valueMapAfter.get(REWRITE_ARTICLE_PUBLISH_DATE) == null
            valueMapAfter.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME) != null
            valueMapAfter.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class).getTime() != previousDate
    }

    def 'should not rewrite custom property for an article page when node has rewrite property set to false'(){
        setup:
            setupWorkItem('article3')
            def valueMapBefore = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/article3/jcr:content").getValueMap()
            def previousDate = valueMapBefore.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class).getTime()
            def rewrite = valueMapBefore.get(REWRITE_ARTICLE_PUBLISH_DATE)
        when:
            process.execute(workItem, workflowSession, metaDataMap)
        then:
            def valueMapAfter = aemContext.resourceResolver().getResource(JLConstants.BASE_AEM_PATH + "/article3/jcr:content").getValueMap()
            rewrite == false
            valueMapAfter.get(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, Date.class).getTime() == previousDate
    }

    def setupWFSession(){
        workflowSession.adaptTo(Session) >> session
    }

    def setupWorkItem(String pathToPage){
        WorkflowData workflowData = Mock(WorkflowData)
        def payload = JLConstants.BASE_AEM_PATH + "/" + pathToPage
        workItem.getWorkflowData() >> workflowData
        workflowData.getPayload() >> payload
        workflowData.getMetaDataMap() >> metaDataMap
    }
}
