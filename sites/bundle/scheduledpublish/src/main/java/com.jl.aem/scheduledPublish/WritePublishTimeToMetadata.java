package com.jl.aem.scheduledpublish;

import com.adobe.dam.print.ids.StringConstants;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.jl.aem.common.util.DateFormatUtil;
import com.jl.aem.editorial.EditorialConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.jl.aem.editorial.EditorialConstants.ARTICLE_REPLICATION_DATE_PROPERTY_NAME;
import static com.jl.aem.editorial.EditorialConstants.REWRITE_ARTICLE_PUBLISH_DATE;

@Component(service = WorkflowProcess.class, immediate = true, property = {
        "process.label = Write publish time from payload to workflowMetadata",
        Constants.SERVICE_PID + "=WritePublishTimeToMetadata"
})
public class WritePublishTimeToMetadata implements WorkflowProcess {

    private static final Logger LOG = LoggerFactory.getLogger(WritePublishTimeToMetadata.class);
    private static final String ABSOLUTE_TIME = "absoluteTime";

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException {
        String payloadPath = workItem.getWorkflowData().getPayload().toString();
        LOG.info("WritePublishTimeToMetadata step for " + payloadPath);
        Session jcrSession = workflowSession.adaptTo(Session.class);
        if (jcrSession == null) {
            return;
        }
        try {
            Node payloadNode = jcrSession.getNode(payloadPath + "/jcr:content");
            Calendar absoluteTimeCalendar = getPropertyIfNotEmpty(payloadNode, ABSOLUTE_TIME);
            setupArticlePublishDate(payloadNode, absoluteTimeCalendar);
            workItem.getWorkflowData().getMetaDataMap().put(ABSOLUTE_TIME, absoluteTimeCalendar.getTimeInMillis());
            removeProperty(payloadNode, ABSOLUTE_TIME);
            removeProperty(payloadNode, REWRITE_ARTICLE_PUBLISH_DATE);
            jcrSession.save();
        } catch (RepositoryException e) {
            LOG.error("Error while getting payload node or getting date from it: ", e);
        }
    }

    private void setupArticlePublishDate(Node payloadNode, Calendar calendar) throws RepositoryException {
        if (isEditorialArticlePage(payloadNode)
                && shouldRewriteArticlePublishDate(payloadNode)) {
            LOG.info("Setting up " + ARTICLE_REPLICATION_DATE_PROPERTY_NAME + " for page " + payloadNode.getPath()
                    + " : " + DateFormatUtil.format(calendar));
            payloadNode.setProperty(ARTICLE_REPLICATION_DATE_PROPERTY_NAME, calendar);
        }
    }

    private boolean shouldRewriteArticlePublishDate(Node payloadNode) throws RepositoryException {
        return !payloadNode.hasProperty(REWRITE_ARTICLE_PUBLISH_DATE)
                || payloadNode.getProperty(REWRITE_ARTICLE_PUBLISH_DATE).getBoolean();
    }

    private boolean isEditorialArticlePage(Node payloadNode) throws RepositoryException {
        return payloadNode.hasProperty(StringConstants.SLING_RESOURCE_TYPE)
                && payloadNode.getProperty(StringConstants.SLING_RESOURCE_TYPE)
                .getString().contains(EditorialConstants.ARTICLE_PAGE_RESOURCE_TYPE);
    }

    private Calendar getPropertyIfNotEmpty(Node node, String propertyName) throws RepositoryException {
        if (node.hasProperty(propertyName))
            return node.getProperty(propertyName).getDate();
        else
            return GregorianCalendar.getInstance();
    }

    private void removeProperty(Node node, String propertyName) throws RepositoryException {
        if (node.hasProperty(propertyName))
            node.getProperty(propertyName).remove();
    }
}

