//download csv files and store them in a folder
def csvFolder = new File('/home/dev/Downloads/tmp');
def regex = ~/([a-z-]+)-c([0-9]+).csv/;

def rootPage = getResource('/content/1jl/en/content/browse/category');

def createDeptsNodes(node, contents) {
    for(int i = 0; i < contents.size(); i++){
        def department = contents[i].replace("\"","").split(",");
        def deptNode = node.addNode("department_" + i, "nt:unstructured");
        deptNode.setProperty('sling:resourceType', '1jl/components/department');
        deptNode.setProperty('dynamicDepartmentUrl', department[0]);
        deptNode.setProperty('dynamicDepartmentName', department[1]);
        deptNode.setProperty('dynamicDepartmentImageSrc', department[2]);
    }
}

csvFolder.listFiles().each({
    def name = it.getName();
    def catNum = (name =~ regex)[0][2];
    def catName = (name =~ regex)[0][1].toUpperCase();
    def contents = it.text.split("\n");

    def page = pageManager.create(
        rootPage.path,
        catNum,
        "/apps/onejl/templates/areapage",
        catName
    );
    def deptsRes = page.getContentResource().getChild('dynamicAreaPageContent/departments/departments');
    this.&createDeptsNodes(deptsRes.adaptTo(Node.class), contents);
    println "Created category page #" + catNum + ", name = " catName;
    session.save();
})