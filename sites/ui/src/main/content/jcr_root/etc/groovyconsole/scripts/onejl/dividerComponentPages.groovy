import javax.jcr.query.Query;

def result = new HashSet<String>();

session.workspace.queryManager
        .createQuery('SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/1jl/en]) AND s.[sling:resourceType] = "onejl/components/divider"',
        Query.JCR_SQL2)
        .execute()
        .nodes
        .each {
    def dividerComponentRes = getResource(it.path);
    def parentContainerRes = dividerComponentRes.parent;
    def parentResourceType = parentContainerRes.valueMap.get('sling:resourceType', '');
    def page = getPage(parentContainerRes.parent.parent.path);
    if ((parentResourceType == 'wcm/foundation/components/parsys'
            || parentResourceType == 'wcm/foundation/components/responsivegrid')
            && parentContainerRes.listChildren().next().path == dividerComponentRes.path
            && page != null) {
        result.add(page.path);
    }
}
result.sort().each { println it }