import groovy.transform.Field

@Field dryRun = binding.variables.data?.containsKey("dryRun") ? data.dryRun : true

if (dryRun) println "Running script in dry run mode, no changes will be saved"

@Field index = 0
@Field bunchSaveSize = 2000
@Field JL_ROOT_PATH = "/content/1jl/en"

updateComponent(JL_ROOT_PATH, "1jl/components/department", this.&updateDepartment)
updateComponent(JL_ROOT_PATH + "/content/browse", "1jl/components/dosAndDonts", this.&updateDosAndDonts)
updateComponent(JL_ROOT_PATH, "onejl/components/accordion", this.&migrateAccordions)
updateComponent(JL_ROOT_PATH + "/content/browse/category", "onejl/components/pages/areapage", this.&updateSEOBannersForAreaPage)
updateResourceType(JL_ROOT_PATH, "onejl/components/pageHeading","onejl/components/authorInformation")

if (!dryRun) session.save()

//===============================================

def printComponentPath(node) {
    println(node.getPath())
}

//=================-Utils-================================

def removeNode(node) {
    def pathToComponent = node.getPath()
    session.removeItem(pathToComponent)
    println("Removed component - " + pathToComponent)
}

def renameProperty(node, oldProperty, newProperty, bunchSave = null) {
    if (node.hasProperty(oldProperty) && !node.hasProperty(newProperty)) {
        println(index + "renaming property for node - " + node.getPath())
        node.setProperty(newProperty, node.getProperty(oldProperty).getString())
        node.getProperty(oldProperty).remove()

        if (bunchSave != null) {
            index++
        }
    }
}

def updatePropertyValue(node, propertyName, oldNewMap) {
    if (node.hasProperty(propertyName)) {
        def newPropValue = oldNewMap.get(node.getProperty(propertyName).getString())
        node.setProperty(propertyName, newPropValue ?: "")
    }
}

def renameNode(parentNode, oldName, newName) {
    if (parentNode.hasNode(oldName) && !parentNode.hasNode(newName)) {
        println(parentNode.path)
        println("---renaming node from '" + oldName + "' to '" + newName)
        session.move(parentNode.getPath() + "/" + oldName, parentNode.getPath() + "/" + newName)
    }
}

def copyArticleReplicationDate(node) {
    if (node.hasProperty("cq:lastReplicated") && !node.hasProperty("articleReplicationDate")) {
        println("---copying articleReplicationDate property for " + node.path)
        node.setProperty("articleReplicationDate", node.getProperty("cq:lastReplicated").getDate())
    }
}


def updateDosAndDonts(node) {
    node.setProperty("sling:resourceType", "onejl/components/dosAndDonts")
    node.setProperty("dosTitle", "Do's")
    node.setProperty("dontsTitle", "Don'ts")
    def dos = []
    if (node.hasNode("dosAndDonts/dos/dosAndDontsContainer")) {
        node.getNode("dosAndDonts/dos/dosAndDontsContainer").getNodes().each({
            if (it.hasProperty("text")) {
                dos.add(it.getProperty("text").string)
            }
        })
    }
    node.setProperty("cmsDosItems", dos as String[])
    def donts = []
    if (node.hasNode("dosAndDonts/donts/dosAndDontsContainer")) {
        node.getNode("dosAndDonts/donts/dosAndDontsContainer").getNodes().each({
            if (it.hasProperty("text")) {
                donts.add(it.getProperty("text").string)
            }
        })
    }
    node.setProperty("cmsDontsItems", donts as String[])
    session.removeItem(node.path + "/dosAndDonts")
    println("Updated " + node.path + " to fit new dosAndDonts component structure")
}

def removeComponent(resourceType) {
    println("Removing all the components of resourceType " + resourceType)
    getNodes(JL_ROOT_PATH, resourceType).each({
        removeNode(it)
    })
}

def updateDepartment(node) {
    if (node.hasProperty('dynamicDepartmentImageSrc')
            && node.getProperty('dynamicDepartmentImageSrc').getString().trim() == '') {
        node.getProperty('dynamicDepartmentImageSrc').remove()
        println("Removed dynamicDepartmentImageSrc for " + node.getPath())
    }
}

def migrateAccordions(node) {
    println "Migrating accordion content for " + node.path
    def container = node.getParent().addNode(node.name + "_migrated")
    container.setProperty("sling:resourceType", "onejl/components/container")
    container.parent.orderBefore(container.getName(), node.getName())
    def containerNode = container.addNode("container")
    containerNode.setProperty("sling:resourceType", "wcm/foundation/components/responsivegrid")
    if (node.hasNode("cq:responsive")) {
        session.move(node.path + "/cq:responsive", container.path + "/cq:responsive")
    }
    node.getNode("accordion").getNodes().each({
        def newAccordion = containerNode.addNode(it.name, "nt:unstructured")
        newAccordion.setProperty("sling:resourceType", "onejl/components/accordion2")
        def newAccordionContainer = newAccordion.addNode("container", "nt:unstructured")
        newAccordionContainer.setProperty("sling:resourceType", "wcm/foundation/components/responsivegrid")
        if (it.hasProperty("dynamicAccordionTitle")) {
            newAccordion.setProperty("title", it.getProperty("dynamicAccordionTitle").value)
            newAccordion.setProperty("showTopBorder", "true")
        }
        if (it.hasProperty("dynamicAccordionDescription")) {
            def copy = newAccordionContainer.addNode(it.getName() + "_copy", "nt:unstructured")
            copy.setProperty("sling:resourceType", "onejl/components/copy")
            copy.setProperty("text", it.getProperty("dynamicAccordionDescription").value)
        }
    })
    if (dryRun) session.removeItem(containerNode.path)
    session.removeItem(node.path)
}

def updateSEOBannersForAreaPage(areapage) {
    def queryManager = session.workspace.queryManager
    def statement = 'SELECT * FROM [nt:unstructured] WHERE ISDESCENDANTNODE("' + areapage.path + '/dynamicAreaPageContent' + '") AND [sling:resourceType] = "onejl/components/seoBanner"'
    def result = queryManager.createQuery(statement, "JCR-SQL2").execute().getNodes()

    if(!areapage.hasNode("dynamicAreaPageContentBottom")) {
        def bottomContainer = areapage.addNode("dynamicAreaPageContentBottom", 'nt:unstructured')
        bottomContainer.setProperty("sling:resourceType", "wcm/foundation/components/responsivegrid")
    }

    if (result.hasNext()) {
        def seoBanner = result.next()
        def bottomContainer = areapage.getNode("dynamicAreaPageContentBottom")
        if (!dryRun) {
            println 'moving SEO Banner for the page ' + areapage.path + ' to bottom container'
            session.move(seoBanner.path, bottomContainer.path + "/seoBanner")
            if (seoBanner.hasNode('cq:responsive')) {
                session.removeItem(seoBanner.path + "/cq:responsive")
            }
            def responsive = seoBanner.addNode('cq:responsive', 'nt:unstructured')
            def defaultResp = responsive.addNode('default', 'nt:unstructured')
            def medium = responsive.addNode('medium', 'nt:unstructured')
            defaultResp.setProperty('offset', '2')
            defaultResp.setProperty('width', '10')
            medium.setProperty('offset', '3')
            medium.setProperty('width', '9')
        }
        if (dryRun) {
            println 'Found SEO banner for page ' + areapage.path + '. It will be moved to bottom section when dryRun is off'
        }
    }
    if (result.hasNext()) {
        println 'WARNING!\nThere is an area category page \n' + areapage.path + '\nwhich has more than one SEO banner in it. This won`t be moved anywhere'
    }
}

//=================================================


def getNodes(path, resourceType) {
    def queryManager = session.workspace.queryManager
    def statement = 'SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE("' + path + '") AND [sling:resourceType] = "' + resourceType + '"'

    println "Executing query for " + resourceType + " components in " + path + "..."

    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()

    println "Query returned " + result.nodes.size() + " nodes..."

    return result
}

def updateComponent(path, resourceType, update = null) {
    def result = this.&getNodes(path, resourceType)

    if (update != null) {
        result.nodes.each { node ->
            if (index > bunchSaveSize)
                return
            update(node)
        }
    }
}

def updateResourceType(path, resourceType, newResourceType) {
    def result = this.&getNodes(path, resourceType)

    result.nodes.each { node ->
        if (index > bunchSaveSize)
            return
        node.setProperty("sling:resourceType", newResourceType)
        println("Set new sling:resourceType \'" + newResourceType + "\' for node " + node.path)
    }
}
