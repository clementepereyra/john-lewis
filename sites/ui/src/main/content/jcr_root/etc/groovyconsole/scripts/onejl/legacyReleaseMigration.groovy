import groovy.transform.Field

@Field dryRun = true
if (dryRun) println "Running script in dry run mode, no changes will be saved"

@Field index = 0
@Field bunchSaveSize = 2000

updateComponent("/content/1jl/en/content/browse","onejl/components/image", this.&renameImageProperties)
updateComponent("/content/1jl/en/content/browse","onejl/components/editorial/image", this.&renameImageProperties)
updateComponent("/content/1jl/en/content/browse","onejl/components/pageHeading", this.&renamePageHeadingProperties)
updateComponent("/content/1jl/en/content/browse","onejl/components/carousel/image",this.&renameCarouselImageProperties)
updateComponent("/content/1jl/en/content/browse","onejl/components/carouselbanner/image",this.&renameCarouselImageProperties)
updateComponent("/content/1jl/en/content/browse","onejl/components/pages/shoppage", this.&addAllowedTemplates)

updateComponent("/content/1jl/en/content/browse/content", "onejl/components/articlepage", this.&copyRelatedArticlesSection)
updateComponent(JL_ROOT_PATH, "onejl/components/articlepage", this.&copyArticleReplicationDate)
updateComponent(JL_ROOT_PATH, "/apps/onejl/components/articlepage", this.&copyArticleReplicationDate)
updateResourceType(JL_ROOT_PATH, "onejl/components/areapage", "onejl/components/pages/areapage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/areapage", "onejl/components/pages/areapage")
updateResourceType(JL_ROOT_PATH, "onejl/components/articlehubpage", "onejl/components/pages/articlehubpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/articlehubpage", "onejl/components/pages/artclehubpage")
updateResourceType(JL_ROOT_PATH, "onejl/components/articlepage", "onejl/components/pages/articlepage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/articlepage", "onejl/components/pages/articlepage")
updateResourceType(JL_ROOT_PATH, "onejl/components/bannerpage", "onejl/components/pages/bannerpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/bannerpage", "onejl/components/pages/bannerpage")
updateResourceType(JL_ROOT_PATH, "onejl/components/categorypage", "onejl/components/pages/categorypage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/categorypage", "onejl/components/pages/categorypage")
updateResourceType(JL_ROOT_PATH, "onejl/components/contentpage", "onejl/components/pages/contentpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/contentpage", "onejl/components/pages/contentpage")
updateResourceType(JL_ROOT_PATH, "onejl/components/hybridbannerpage", "onejl/components/pages/hybridbannerpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/hybridbannerpage", "onejl/components/pages/hybridbannerpage")
updateResourceType(JL_ROOT_PATH, "onejl/components/navigationpage", "onejl/components/pages/navigationpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/navigationpage", "onejl/components/pages/navigationpage")
updateResourceType(JL_ROOT_PATH, "onejl/components/shoppage", "onejl/components/pages/shoppage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/shoppage", "onejl/components/pages/shoppage")
updateResourceType(JL_ROOT_PATH, "onejl/components/voucherpage", "onejl/components/pages/voucherpage")
updateResourceType(JL_ROOT_PATH, "/apps/onejl/components/voucherpage", "onejl/components/pages/voucherpage")

updateComponent(JL_ROOT_PATH + "/content/browse/content", "onejl/components/pages/articlepage", this.&removeRelatedArticlesSection)
//-----------------------------Release 14.06.2018------------------------
updateComponent(JL_ROOT_PATH, "1jl/components/tableOfContents", this.&removeNode)
updateComponent(JL_ROOT_PATH, "1jl/components/quickLinks", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/iceberg", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/pageHeading", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/articleHeading", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/carousel", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/carouselbanner", this.&printComponentPath)
//-----------------------------------------------------------------------

if (!dryRun) session.save()

//===============================================


def printComponentPath(node) {
    println(node.getPath())
}


def removeNode(node) {
    def pathToComponent = node.getPath()
    session.removeItem(pathToComponent)
    println("Removed component - " + pathToComponent)
}

def removeRelatedArticlesSection(node){
    if (node.hasNode("relatedArticles")) {
        session.removeItem(node.getPath() + "/relatedArticles")
        println("Removed related articles section for page - " + node.getPath())
    }
}

def renameImageProperties(node) {

    renameProperty(node, "dynamicPromoImageAlt", "imageAlt")
    renameProperty(node, "dynamicPromoHref", "imageHref")
    renameProperty(node, "dynamicImageBannerisNewWindow", "imageLinkOpenNewWindow")
    renameProperty(node, "dynamicImageWithMarginBottom", "imageWithMarginBottom")

    renameProperty(node, "cmsImageCaption", "imageCaption")
    renameProperty(node, "cmsImageCaptionType", "imageCaptionType")
    renameProperty(node, "cmsImageCaptionAlignment", "imageCaptionAlignment")
    renameProperty(node, "cmsImageCaptionBGColour", "imageCaptionBGColour")
    renameProperty(node, "cmsImageCaptionTextColour", "imageCaptionTextColour")
    renameProperty(node, "dynamicPromoImageSrc", "imageSrc", true)
}

def renamePageHeadingProperties(node) {
    renameProperty(node, "cmsImageHref", "imageHref")
    renameProperty(node, "cmsImageAlt", "imageAlt")
    renameProperty(node, "cmsImageLinkOpenNewWindow", "imageLinkOpenNewWindow")
    renameProperty(node, "cmsImageSrc", "imageSrc")

    renameProperty(node, "cmsImageCaption", "imageCaption")
    renameProperty(node, "cmsImageCaptionType", "imageCaptionType")
    renameProperty(node, "cmsImageCaptionAlignment", "imageCaptionAlignment")
    renameProperty(node, "cmsImageCaptionBGColour", "imageCaptionBGColour")
    renameProperty(node, "cmsImageCaptionTextColour", "imageCaptionTextColour")
}

def renameCarouselImageProperties(node) {
    renameProperty(node, "cmsImageSrc", "imageSrc")
    renameProperty(node, "cmsImageAlt", "imageAlt")
}

def addAllowedTemplates(node) {
    node.setProperty("cq:allowedTemplates", "/apps/onejl/templates/contentpage")
    println("added allowedTemplates to" + node.getPath())
    def childPages = node.getParent().getNodes()
    while (childPages.hasNext()) {
        convertPageToContentPageType(childPages.nextNode(), "onejl/components/contentpage" )
    }
}

def convertPageToContentPageType(node, value) {
    if (!node.hasNode("jcr:content")) return
    def jcrContentNode = node.getNode("jcr:content")
    jcrContentNode.setProperty("sling:resourceType", value)
    renameNode(jcrContentNode, "shopPageContent", "par")
    println("renamed resourceType for - " + node.getPath())
}


def copyRelatedArticlesSection(node){
    if (!node.hasNode("relatedArticles")) {
        session.workspace.copy("/apps/onejl/templates/articlepage/jcr:content/relatedArticles", node.getPath() + "/relatedArticles")
        println("copied related articles structure to page - " + node.getPath())
    }
}

//=================-Utils-================================

def renameProperty(node, oldProperty, newProperty, bunchSave = null) {
    if (node.hasProperty(oldProperty) && !node.hasProperty(newProperty)) {
        println(index + "renaming property for node - " + node.getPath())
        node.setProperty(newProperty, node.getProperty(oldProperty).getString())
        node.getProperty(oldProperty).remove()

        if (bunchSave != null ) {
            index++
        }
    }
}

def updatePropertyValue(node, propertyName, oldNewMap) {
    if(node.hasProperty(propertyName)){
        def newPropValue = oldNewMap.get(node.getProperty(propertyName).getString())
        node.setProperty(propertyName, newPropValue?:"")
    }
}

def renameNode(parentNode, oldName, newName) {
    if (parentNode.hasNode(oldName) && !parentNode.hasNode(newName)) {
        println(parentNode.path)
        println("---renaming node from '" + oldName +"' to '" + newName)
        session.move(parentNode.getPath() + "/" + oldName, parentNode.getPath() + "/" + newName)
    }
}

//=================================================


def getNodes(path, resourceType){
    def queryManager = session.workspace.queryManager;
    def statement = 'SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE("' + path + '") AND [sling:resourceType] = "'+ resourceType + '"'

    println "Executing query for " + resourceType + " components in " + path + "..."

    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()

    println "Query returned " + result.nodes.size() + " nodes..."

    return result
}

def updateComponent(path, resourceType, update = null) {
    def result = this.&getNodes(path, resourceType)

    if (update != null) {
        result.nodes.each { node ->
            if (index > bunchSaveSize)
                return
            update(node)
        }
    }
}
