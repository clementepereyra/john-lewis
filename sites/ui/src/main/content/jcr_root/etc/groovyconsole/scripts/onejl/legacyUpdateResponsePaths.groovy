def dryRun = true
if (dryRun) println "Running script in dry run mode, no changes will be saved"

updateResourceType("/content/1jl", "1jl/components/copy", "onejl/components/copy")
updateResourceType("/content/1jl", "1jl/components/imageBanner", "onejl/components/image")
updateResourceType("/content/1jl", "1jl/components/buttonLink", "onejl/components/buttonLink")
updateResourceType("/content/1jl", "1jl/components/hero", "onejl/components/hero")
updateResourceType("/content/1jl", "1jl/components/accordionItem", "onejl/components/accordionItem")
updateResourceType("/content/1jl", "1jl/components/title", "onejl/components/title")
updateResourceType("/content/1jl", "1jl/components/divider", "onejl/components/divider", this.&updateDividerProperties)
updateResourceType("/content/1jl", "1jl/components/youtubeViewer", "onejl/components/youtubeViewer")
updateResourceType("/content/1jl", "1jl/components/s7Viewer", "onejl/components/scene7Video")
updateResourceType("/content/1jl", "onejl/components/youtubeViewer", "onejl/components/youtubeVideo")
updateResourceType("/content/1jl", "onejl/components/youtuViewer", "onejl/components/youtubeVideo")
updateResourceType("/content/1jl", "1jl/components/container", "onejl/components/container")
updateResourceType("/content/1jl", "1jl/components/card", "onejl/components/card")
updateResourceType("/content/1jl", "1jl/components/accordion", "onejl/components/accordion")
updateResourceType("/content/1jl", "1jl/components/accordionItem", "onejl/components/accordionItem")
updateResourceType("/content/1jl", "1jl/components/iceberg", "onejl/components/iceberg")
updateResourceType("/content/1jl", "1jl/components/icebergDefaultContent", "onejl/components/icebergDefaultContent")
updateResourceType("/content/1jl", "1jl/components/icebergHiddenContent", "onejl/components/icebergHiddenContent")
updateResourceType("/content/1jl", "onejl/components/icebergDefaultContent", "onejl/components/iceberg/icebergDefaultContent", this.&updateIcebergProperties)
updateResourceType("/content/1jl", "onejl/components/icebergHiddenContent", "onejl/components/iceberg/icebergHiddenContent")
updateResourceType("/content/1jl", "foundation/components/parsys", "wcm/foundation/components/responsivegrid")
updateResourceType("/content/1jl", "onejl/components/hero", "onejl/components/pageHeading", this.&updateHeroProperties)
updateResourceType("/content/1jl", "1jl/components/areapage", "onejl/components/areapage")
updateResourceType("/content/1jl", "1jl/components/bannerpage", "onejl/components/bannerpage")
updateResourceType("/content/1jl", "1jl/components/buyingGuidesPage", "onejl/components/buyingGuidesPage")
updateResourceType("/content/1jl", "1jl/components/categorypage", "onejl/components/categorypage")
updateResourceType("/content/1jl", "1jl/components/contentpage", "onejl/components/contentpage")
updateResourceType("/content/1jl", "1jl/components/hybridbannerpage", "onejl/components/hybridbannerpage")
updateResourceType("/content/1jl", "1jl/components/voucherpage", "onejl/components/voucherpage")
updateResourceType("/content/1jl", "1jl/components/basePage", "onejl/components/basePage")
updateCqTemplate("/content/1jl", "1jl/templates/areapage", "onejl/templates/areapage")
updateCqTemplate("/content/1jl", "1jl/templates/bannerpage", "onejl/templates/bannerpage")
updateCqTemplate("/content/1jl", "1jl/templates/buyingGuidesPage", "onejl/templates/buyingGuidesPage")
updateCqTemplate("/content/1jl", "1jl/templates/categorypage", "onejl/templates/categorypage")
updateCqTemplate("/content/1jl", "1jl/templates/contentpage", "onejl/templates/contentpage")
updateCqTemplate("/content/1jl", "1jl/templates/hybridbannerpage", "onejl/templates/hybridbannerpage")
updateCqTemplate("/content/1jl", "1jl/templates/voucherpage", "onejl/templates/voucherpage")
updateResourceType("/content/1jl", "onejl/components/pageHeading", "onejl/components/pageHeading", this.&updatePageHeadingProperties)
updateResourceType("/content/1jl", "1jl/components/accordion2", "onejl/components/accordion2")
updateResourceType("/content/1jl", "onejl/components/title", "onejl/components/title", this.&updateTitleProperties)
updateResourceType("/content/1jl", "onejl/components/container", "onejl/components/container", this.&updateContainerProperties)
updateResourceType("/content/1jl/en/content/browse/navigation", "onejl/components/contentpage", "/apps/onejl/components/navigationpage")
updateResourceType("/content/1jl/en/content/browse/navigation", "/apps/onejl/components/contentpage", "/apps/onejl/components/navigationpage")
updateResourceType("/content/1jl/en/content/browse", "onejl/components/carousel", "onejl/components/carousel", this.&updateCarouselProperties)
updateResourceType("/content/1jl/en/content/browse", "onejl/components/carouselbanner", "onejl/components/carouselbanner", this.&updateCarouselBannerProperties)
updateResourceType("/content/1jl", "onejl/components/divider", "onejl/components/divider", this.&updateDividerProps)
updateResourceType("/content/1jl", "onejl/components/title", "onejl/components/title", this.&updateTitleHeadingStyle)
updateResourceType("/content/1jl/en/content/browse/our-shops", "onejl/components/contentpage", "onejl/components/pages/shoppage", this.&updateShopPageStructure)
updateResourceType("/content/1jl/en/content/browse/our-shops", "/apps/onejl/components/contentpage", "onejl/components/pages/shoppage", this.&updateShopPageStructure)
updateResourceType("/content/1jl/en/content/browse/content", "/apps/onejl/components/articlepage", "onejl/components/articlepage", this.&updateArticlePageStructure)
updateResourceType("/content/1jl/en/content/browse/content", "onejl/components/articlepage", "onejl/components/articlepage", this.&updateArticlePageStructure)
updatePageTitles()

if (!dryRun) save()

def oldValue
def newValue

def updateCqTemplate(path, old, neW){
    def result = this.&getNodes(path, old, neW, "cq:template")

    result.nodes.each(this.&updateNodeByTemplate)
}

def getNodes(path, old, neW, property){
    oldValue = old
    newValue = neW
    def queryManager = session.workspace.queryManager;
    def statement = "select * from [nt:base] where [jcr:path] like '" + path + "%' and [" + property + "] like '%" + old + "'";

    println "Executing query for " + old + " components in " + path + "..."

    def result = queryManager.createQuery(statement, "JCR-SQL2").execute();

    println "Query returned " + result.nodes.size() + " nodes..."

    return result
}

def updateNodeByTemplate(node) {
    this.&updateNodeByProperty(node, "cq:template")
}

def updateNodeByProperty(node, property){
    def oldProperty = node.getProperty(property).getString()
    def newProperty = oldProperty.replace(oldValue, newValue)
    if (!node.isCheckedOut()) {
        println("node not checked out" + node.getPath());
        //checkout versionable node
        node.getSession().getNode(node.getPath().substring(0, node.getPath().lastIndexOf("jcr:content")+11)).checkout();
    }
    node.setProperty(property, newProperty)
    println " Resource type was updated for node " + node.getPath() + " to value " + node.getProperty(property).getString()
}

def updateResourceType(path, old, neW, update = null) {
    def result = this.&getNodes(path, old, neW, "sling:resourceType")

    if (update != null) {
        result.nodes.each(update)
    }

    result.nodes.each(this.&updateNodeByResourceType)
}

def updateNodeByResourceType(node) {
    this.&updateNodeByProperty(node, "sling:resourceType")
}

def updateDividerProperties(node) {
    if (node.hasProperty("dividerType")) {
        def type = node.getProperty("dividerType").getString()
        type = type.replace("dynamic", "cms")
        node.setProperty("dividerType", type)
    }
}

def updateIcebergProperties(node) {
    if (!node.getParent().hasProperty("sling:resourceType")) {
        node.getParent().setProperty("sling:resourceType", "onejl/components/innerContainer")
    }
}

def updateHeroProperties(node){
    if(node.hasProperty("dynamicPromoImageSrc")){
        def imageSrc = node.getProperty("dynamicPromoImageSrc").getString()

        def pageHeadingImage = node.getOrAddNode("pageHeadingImage")
        pageHeadingImage.setProperty("cmsImageSrc",imageSrc)

        def imageAlt = node.getProperty("dynamicPromoImageAlt").getString()
        pageHeadingImage.setProperty("cmsImageAlt",imageAlt)
    }
}

def updatePageHeadingProperties(node){

    if(node.hasNode("pageHeadingImage")){
        def pageHeadingImage = node.getNode("pageHeadingImage")
        def properties = pageHeadingImage.getProperties()
        for(property in properties){
            if(property.getName() == "jcr:primaryType"){
                continue
            }
            node.setProperty(property.getName(),property.getString())
        }
        node.removeNode("pageHeadingImage")
    }

}

def updateTitleProperties(node){

    if(node.hasProperty("headingLevel")){
        def headingLevel = node.getProperty("headingLevel").getString()
        headingLevel = headingLevel.replace("dynamicH","h")
        node.setProperty("headingLevel",headingLevel)
    }

}

def updateContainerProperties(node){
    updateFlexPropertyForContainer(node,"isFlexItemsStretch","stretch")
    updateFlexPropertyForContainer(node,"isFlexItemsCentre","centre")
}

def updateFlexPropertyForContainer(node,property, value){
    if(node.hasProperty(property)){
        node.setProperty("flexAlignment",value)
    }
}

def updateCarouselProperties(node){
    if(node.hasNode("images")){
        session.removeItem(node.getPath() + "/images")
    }
}

def updateCarouselBannerProperties(node){
    if(node.hasNode("images")){
        session.removeItem(node.getPath() + "/images")
    }
}

def updateDividerProps(node){
    def oldNewMap = [cmsDepartmentWhitespaceSmall: "solid",
                     cmsDepartmentWhitespaceLarge:"whitespace-xsmall",
                     cmsDepartmentSeparator:"whitespace-small",
                     cmsDepartmentSeparatorChunky:"whitespace-medium",
                     cmsDepartmentWhitespaceXSmall:"whitespace-large",
                     cmsDepartmentWhitespaceXXSmall:"whitespace-xlarge",
                     cmsDepartmentWhitespaceXLarge:"dotted",
                     cmsDepartmentSeparatorDotted:"chunky"]
    updatePropertyValue(node, 'dividerType', oldNewMap)
}

def updateTitleHeadingStyle (node) {
    def oldNewMap = ["2em" : "h1",
                     "1.5em" : "h2",
                     "1.17em" : "h3",
                     "1em" : "h4",
                     ".83em" : "h5",
                     ".67em" : "h6"]
    updatePropertyValue(node, 'headingStyle', oldNewMap)
}

def updatePropertyValue(node, propertyName, oldNewMap) {
    if(node.hasProperty(propertyName)){
        def newPropValue = oldNewMap.get(node.getProperty(propertyName).getString())
        node.setProperty(propertyName, newPropValue?:"")
    }
}

def updateShopPageStructure(parentNode) {
    renameNode(parentNode, "par", "shopPageContent")
    articleBody
}

def renameNode(parentNode, oldName, newName) {
    if (parentNode.hasNode(oldName) && !parentNode.hasNode(newName)) {
        println(parentNode.path)
        println("---renaming node from '" + oldName +"' to '" + newName)
        session.move(parentNode.getPath() + "/" + oldName, parentNode.getPath() + "/" + newName)
    }
}

def updateArticlePageStructure(jcrContentNode) {
    if (jcrContentNode.hasNode("articleBody")) {
        def articleBody = jcrContentNode.getNode("articleBody")
        articleBody.setProperty("sling:resourceType", "foundation/components/parsys")
    }
}

def updatePageTitles() {
    def rootPath = "/content/1jl/en/content/browse";
    def queryManager = session.workspace.queryManager;
    def query = "SELECT * FROM [cq:Page] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])";
    def queryResult = queryManager.createQuery(query, "JCR-SQL2").execute()
    queryResult.nodes.each({
        if (it.hasNode("jcr:content")) {
            renameProperty(it.getNode("jcr:content"), "navTitle", "pageTitle")
        }

    })
}
