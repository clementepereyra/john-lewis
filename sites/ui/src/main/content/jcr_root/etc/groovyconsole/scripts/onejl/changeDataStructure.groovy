import groovy.transform.Field

@Field dryRun = binding.variables.data?.containsKey("dryRun") ? data.dryRun : true

if (dryRun) println "Running script in dry run mode, no changes will be saved"

@Field index = 0
@Field bunchSaveSize = 2000
@Field JL_ROOT_PATH = "/content/1jl/en"

updateComponent(JL_ROOT_PATH, "1jl/components/tableOfContents", this.&removeNode)
updateComponent(JL_ROOT_PATH + "/content/browse", "1jl/components/dosAndDonts", this.&updateDosAndDonts)
updateComponent(JL_ROOT_PATH, "1jl/components/quickLinks", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/iceberg", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/pageHeading", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/articleHeading", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/carousel", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "onejl/components/carouselbanner", this.&printComponentPath)
updateComponent(JL_ROOT_PATH, "1jl/components/department", this.&updateDepartment)


if (!dryRun) session.save()

//===============================================

def printComponentPath(node) {
    println(node.getPath())
}

//=================-Utils-================================

def removeNode(node) {
    def pathToComponent = node.getPath();
    session.removeItem(pathToComponent)
    println("Removed component - " + pathToComponent)
}

def renameProperty(node, oldProperty, newProperty, bunchSave = null) {
    if (node.hasProperty(oldProperty) && !node.hasProperty(newProperty)) {
        println(index + "renaming property for node - " + node.getPath())
        node.setProperty(newProperty, node.getProperty(oldProperty).getString())
        node.getProperty(oldProperty).remove()

        if (bunchSave != null ) {
            index++
        }
    }
}

def updatePropertyValue(node, propertyName, oldNewMap) {
    if(node.hasProperty(propertyName)){
        def newPropValue = oldNewMap.get(node.getProperty(propertyName).getString())
        node.setProperty(propertyName, newPropValue?:"")
    }
}

def renameNode(parentNode, oldName, newName) {
    if (parentNode.hasNode(oldName) && !parentNode.hasNode(newName)) {
        println(parentNode.path)
        println("---renaming node from '" + oldName +"' to '" + newName)
        session.move(parentNode.getPath() + "/" + oldName, parentNode.getPath() + "/" + newName)
    }
}

def copyArticleReplicationDate(node) {
    if (node.hasProperty("cq:lastReplicated") && !node.hasProperty("articleReplicationDate")){
        println("---copying articleReplicationDate property for " + node.path)
        node.setProperty("articleReplicationDate", node.getProperty("cq:lastReplicated").getDate())
    }
}


def updateDosAndDonts(node) {
    node.setProperty("sling:resourceType", "onejl/components/dosAndDonts")
    node.setProperty("dosTitle", "Do's")
    node.setProperty("dontsTitle", "Don'ts")
    def dos = []
    if (node.hasNode("dosAndDonts/dos/dosAndDontsContainer")){
        node.getNode("dosAndDonts/dos/dosAndDontsContainer").getNodes().each({
            if(it.hasProperty("text")){
                dos.add(it.getProperty("text").string)
            }
        })
    }
    node.setProperty("cmsDosItems", dos as String[])
    def donts = []
    if (node.hasNode("dosAndDonts/donts/dosAndDontsContainer")){
        node.getNode("dosAndDonts/donts/dosAndDontsContainer").getNodes().each({
            if(it.hasProperty("text")){
                donts.add(it.getProperty("text").string)
            }
        })
    }
    node.setProperty("cmsDontsItems", donts as String[])
    session.removeItem(node.path + "/dosAndDonts")
    println("Updated " + node.path + " to fit new dosAndDonts component structure")
}

def updateDepartment(node){
    if (node.hasProperty('dynamicDepartmentImageSrc')
            && node.getProperty('dynamicDepartmentImageSrc').getString().trim() == ''){
        node.getProperty('dynamicDepartmentImageSrc').remove()
        println ("Removed dynamicDepartmentImageSrc for " + node.getPath())
    }
}

//=================================================


def getNodes(path, resourceType){
    def queryManager = session.workspace.queryManager
    def statement = 'SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE("' + path + '") AND [sling:resourceType] = "'+ resourceType + '"'

    println "Executing query for " + resourceType + " components in " + path + "..."

    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()

    println "Query returned " + result.nodes.size() + " nodes..."

    return result
}

def updateComponent(path, resourceType, update = null) {
    def result = this.&getNodes(path, resourceType)

    if (update != null) {
        result.nodes.each { node ->
            if (index > bunchSaveSize)
                return
            update(node)
        }
    }
}

def updateResourceType(path, resourceType, newResourceType) {
    def result = this.&getNodes(path, resourceType)

    result.nodes.each { node ->
        if (index > bunchSaveSize)
            return
        node.setProperty("sling:resourceType", newResourceType)
        println ("Set new sling:resourceType \'" + newResourceType + "\' for node " + path)
    }
}
