import javax.jcr.Node

void printLinkTsv(String slingResourceType, String urlProperty, String descriptionProperty, String imageProperty = "") {
    def queryManager = session.workspace.queryManager
    def statement = """SELECT component.*
                      |FROM [cq:PageContent] AS page
                      |INNER JOIN [nt:unstructured] AS component
                      |ON ISDESCENDANTNODE(component,page)
                      |WHERE ISDESCENDANTNODE(page, '/content/1jl/en/content/browse') 
                      |AND page.[cq:lastReplicationAction] = 'Activate'
                      |AND page.[deleted] IS NULL
                      |AND component.[sling:resourceType] = "$slingResourceType"
                      |AND component.[$urlProperty] IS NOT NULL""".stripMargin().stripIndent()

    queryManager.createQuery(statement, "JCR-SQL2").execute().nodes.each { node ->
        println "$node.path\t${propertyValue(node, urlProperty)}\t${propertyValue(node, descriptionProperty)}\t${propertyValue(node, imageProperty)}"
    }
}

void printMarkupLinkTsv(String slingResourceType, String markupProperty) {
    def queryManager = session.workspace.queryManager
    def statement = """SELECT component.*
                      |FROM [cq:PageContent] AS page
                      |INNER JOIN [nt:unstructured] AS component
                      |ON ISDESCENDANTNODE(component,page)
                      |WHERE ISDESCENDANTNODE(page, '/content/1jl/en/content/browse') 
                      |AND page.[cq:lastReplicationAction] = 'Activate'
                      |AND page.[deleted] IS NULL
                      |AND component.[sling:resourceType] = "$slingResourceType"
                      |AND component.[$markupProperty] IS NOT NULL
                      |AND component.[$markupProperty] LIKE '%<a %'""".stripMargin().stripIndent()

    queryManager.createQuery(statement, "JCR-SQL2").execute().nodes.each { node ->
        def matcher = propertyValue(node, markupProperty) =~ /(?is)<a.*?href="(?<linkHref>.*?)".*?>(?<linkText>.*?)<\/a>/
        while (matcher.find()) {
            println "$node.path\t${matcher.group("linkHref")}\t${matcher.group("linkText")}\t"
        }
    }
}

String propertyValue(Node node, String property) {
    def propertyParts = property.split("/")
    def nodeWithProperty = node
    propertyParts.init().each { nodeWithProperty = nodeWithProperty.getNode(it) }
    def propertyValue = nodeWithProperty.hasProperty(propertyParts.last()) ? nodeWithProperty.getProperty(propertyParts.last()).getString() : ""
    return propertyValue.trim().replaceAll('[\t\r\n]', '')
}

printLinkTsv("onejl/components/buttonLink", "link", "title")
printLinkTsv("onejl/components/buttonPrimary", "cmsLinkButtonHref", "cmsLinkButtonVisibleText")
printLinkTsv("onejl/components/buttonSecondary", "cmsLinkButtonHref", "cmsLinkButtonVisibleText")
printLinkTsv("onejl/components/buttonTertiary", "cmsLinkButtonHref", "cmsLinkButtonVisibleText")
printLinkTsv("onejl/components/carousel/image", "cmsCarouselLinkUrl", "cmsCarouselButtonVisibleText", "imageSrc")
printLinkTsv("onejl/components/carousel/image", "firstLinkHref", "firstLinkVisibleText", "imageSrc")
printLinkTsv("onejl/components/carousel/image", "secondLinkHref", "secondLinkVisibleText", "imageSrc")
printLinkTsv("onejl/components/carouselbanner/image", "cmsCarouselLinkUrl", "cmsCarouselButtonVisibleText", "imageSrc")
printLinkTsv("onejl/components/carouselbanner/image", "firstLinkHref", "firstLinkVisibleText", "imageSrc")
printLinkTsv("onejl/components/carouselbanner/image", "secondLinkHref", "secondLinkVisibleText", "imageSrc")
printLinkTsv("onejl/components/hero", "dynamicPromoHref", "dynamicPromoTitle", "dynamicPromoImageSrc")
printLinkTsv("onejl/components/homepageHero", "href", "callToActionCopy", "imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration1/href", "inspiration1/copy", "inspiration1/imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration2/href", "inspiration2/copy", "inspiration2/imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration3/href", "inspiration3/copy", "inspiration3/imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration4/href", "inspiration4/copy", "inspiration4/imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration5/href", "inspiration5/copy", "inspiration5/imageSrcMobile")
printLinkTsv("onejl/components/homepageInspiration", "inspiration6/href", "inspiration6/copy", "inspiration6/imageSrcMobile")
printLinkTsv("onejl/components/homepagePromotionsDouble", "promo1/href", "promo1/callToActionCopy", "promo1/imageSrcMobile")
printLinkTsv("onejl/components/homepagePromotionsDouble", "promo2/href", "promo2/callToActionCopy", "promo2/imageSrcMobile")
printLinkTsv("onejl/components/homepagePromotionsTreble", "promo1/href", "promo1/callToActionCopy", "promo1/imageSrcMobile")
printLinkTsv("onejl/components/homepagePromotionsTreble", "promo2/href", "promo2/callToActionCopy", "promo2/imageSrcMobile")
printLinkTsv("onejl/components/homepagePromotionsTreble", "promo3/href", "promo3/callToActionCopy", "promo3/imageSrcMobile")
printLinkTsv("onejl/components/homepageTidbits", "tidbit1/href", "tidbit1/copy")
printLinkTsv("onejl/components/homepageTidbits", "tidbit2/href", "tidbit2/copy")
printLinkTsv("onejl/components/homepageTidbits", "tidbit3/href", "tidbit3/copy")
printLinkTsv("onejl/components/homepageTidbits", "tidbit4/href", "tidbit4/copy")
printLinkTsv("onejl/components/image", "imageHref", "imageAlt", "imageSrc")
printLinkTsv("onejl/components/pageHeading", "imageHref", "imageAlt", "imageSrc")
printLinkTsv("onejl/components/supplierBanner/supplierBannerLink", "url", "label")
printMarkupLinkTsv("onejl/components/copy", "text")
printMarkupLinkTsv("onejl/components/table", "tableData")
printMarkupLinkTsv("onejl/components/seoBanner", "cmsRichContent")
printMarkupLinkTsv("onejl/components/accordionItem", "dynamicAccordionDescription")
