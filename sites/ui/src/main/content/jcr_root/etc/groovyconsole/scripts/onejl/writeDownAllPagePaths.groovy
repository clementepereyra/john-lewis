import org.apache.jackrabbit.commons.JcrUtils

def dryrun = true;

if (dryrun) {
    println "DRY RUN MODE IS ON, changes will NOT be saved\n"
}

writeToFileAllPagePaths('/content/1jl/en/', "/var/onejl/allPages", dryrun)

def writeToFileAllPagePaths(rootPath, nodeToWritePath, dryrun) {

    def nodeToWrite = JcrUtils.getOrCreateByPath(nodeToWritePath, 'nt:unstructured', session)

    String pagePaths = getPagePaths(rootPath);

    nodeToWrite.setProperty("jcr:data", convertToBinary(pagePaths));

    if (!dryrun) {
        session.save();
    }

    "Success"
}

def getPagePaths(String rootPath) {
    StringBuilder pagePaths = new StringBuilder("");

    getPage(rootPath).recurse { page ->
        pagePaths.append(page.path + ",");
        println 'path:' + page.path
    }
    return pagePaths.toString();
}

def convertToBinary(String pagePaths) {
    InputStream is = new ByteArrayInputStream(pagePaths.getBytes());

    ValueFactory factory = session.getValueFactory();

    return factory.createBinary(is);
}


