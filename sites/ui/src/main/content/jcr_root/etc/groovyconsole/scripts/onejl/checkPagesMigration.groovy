import org.apache.jackrabbit.commons.JcrUtils
import org.apache.commons.io.IOUtils;


checkMigration("/var/onejl/allPages")


def checkMigration(migratedPagesArchivePath) {

    InputStream is = JcrUtils.readFile(getNode(migratedPagesArchivePath))

    String[] pagePaths = IOUtils.toString(is, "UTF-8").split(",");

    println 'all pages are migrated except: '

    pagePaths.each {
        if (null == getPage(it))
            println '-' + it;
    }

    println '-----'

    "Success"
}
