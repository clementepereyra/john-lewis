import javax.jcr.query.Query;
import groovy.transform.Field;

def result = new LinkedHashMap<String, Set<String>>();
def rootPath = "/content/1jl/en/content/browse";
def query = "SELECT * FROM [cq:Page] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])";
def componentsQuery = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([%s])";
def queryResult = resourceResolver.findResources(query, Query.JCR_SQL2);
queryResult.each({
    if(it.getPath().endsWith('-test')){
        return
    }
    def componentResourceTypes = new HashSet<String>();
    def componentNodes = resourceResolver.findResources(String.format(componentsQuery, it.getPath()), Query.JCR_SQL2);
    componentNodes.each({
        def resourceType = it.getValueMap().get('sling:resourceType', String.class);
        if(!wasteResourceType(resourceType)){
            componentResourceTypes.add(resourceType);
        }
    })
    result.put(it.path, componentResourceTypes)
})
@Field
def pageResourceTypes = ['/apps/onejl/components/voucherpage',
                         '/apps/onejl/components/areapage',
                         '/apps/onejl/components/bannerpage',
                         '/apps/onejl/components/contentpage',
                         '/apps/onejl/components/categorypage',
                         '/apps/onejl/components/categotysearchpage',
                         '/apps/onejl/components/navigationpage',
                         '/apps/onejl/components/hybridbannerpage',
                         'onejl/components/voucherpage',
                         'onejl/components/areapage',
                         'onejl/components/bannerpage',
                         'onejl/components/contentpage',
                         'onejl/components/categorypage',
                         'onejl/components/categotysearchpage',
                         'onejl/components/navigationpage',
                         'onejl/components/hybridbannerpage'];
@Field
def ootbResourceTypes = ['wcm/foundation/components/responsivegrid',
                         'foundation/components/parsys'];

def wasteResourceType(resourceType){
    return pageResourceTypes.contains(resourceType) || ootbResourceTypes.contains(resourceType) || resourceType == null;
}
result.keySet().each({
    println it;
    def components = result.get(it);
    if (components.isEmpty()) {
        println "\t Empty page"
    } else {
        components.each({
            println "\t" + it
        })
    }
})

println "============================="

result = new TreeMap<String, Integer>();
componentsQuery = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])";
def components = resourceResolver.findResources(componentsQuery, Query.JCR_SQL2);
components.each({
    if(it.getPath().contains('-test/jcr:content')){
        return
    }
    def resourceType = it.valueMap.get("sling:resourceType", "empty");
    def freq = result.getOrDefault(resourceType, 0);
    result.put(resourceType, freq + 1);
})

result.entrySet().each({
    println it.key + " is used " + it.value + " times";
})