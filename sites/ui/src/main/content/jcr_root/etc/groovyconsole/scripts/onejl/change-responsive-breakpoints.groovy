import groovy.transform.Field
import javax.jcr.query.Query

@Field dryRun = true
if (dryRun) println "Running script in dry run mode, no changes will be saved"

@Field index = 0

def rootPath = "/content/1jl/en/content/browse"
def query = "SELECT * FROM [cq:Page] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])"
def queryResult = resourceResolver.findResources(query, Query.JCR_SQL2)

queryResult.each({
    def jcrContent = it.getChild("jcr:content")
    if (jcrContent != null) {
        findAndUpdateBreakpoints(jcrContent)
    }
})

def findAndUpdateBreakpoints(resource) {
    def cqResponsive = resource.getChild("cq:responsive")
    if (index > 500) {
        if (!dryRun) save()
        index = 0
        sleep(2000)
    }

    if (cqResponsive != null) {
        index++
        updateResponsiveBreakpoints(cqResponsive.adaptTo(Node.class))
    }
    if (resource.hasChildren())
        resource.getChildren().each(this.&findAndUpdateBreakpoints)
}

def updateResponsiveBreakpoints(node) {
    updateResponsiveBreakpoint(node, "mobile", "small")
    updateResponsiveBreakpoint(node, "tablet", "medium")
}

def updateResponsiveBreakpoint(node, oldName, newName) {
    if (node.hasNode(oldName) && !node.hasNode(newName)) {
        println(node.path)
        if (!node.isCheckedOut()) {
            println("node not checked out" + node.getPath());
            //checkout versionable node
            node.getSession().getNode(node.getPath().substring(0, node.getPath().lastIndexOf("jcr:content")+11)).checkout();
        }
        println("---renaming breakpoint from '" + oldName +"' to '" + newName)
        session.move(node.getPath() + "/" + oldName, node.getPath() + "/" + newName)
    }
}

if (!dryRun) save()
