def dryRun = true
if (dryRun) println "Running script in dry run mode, no changes will be saved"


def queryManager = session.workspace.queryManager;

def statement = "SELECT * FROM [nt:unstructured] AS node WHERE ISDESCENDANTNODE(node, '/content/1jl/en/content/browse') AND NAME() = 'cq:responsive'";

def result = queryManager.createQuery(statement, "JCR-SQL2").execute();

println "Query returned " + result.nodes.size() + " nodes..."

result.nodes.each(this.&updateNodeByResourceType)

def updateNodeByResourceType(node) {
    println(node.path)
    if (node.hasNode("desktop") && !node.hasNode("default")) {
        println("---------------" + node.path)
        session.move(node.getPath() + "/desktop", node.getPath() + "/" + "default");
    }

}

if (!dryRun) save()
