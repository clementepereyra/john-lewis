def queryManager = session.workspace.queryManager
def statement = """SELECT *
                  |FROM [cq:Page]
                  |WHERE [jcr:path] LIKE '/content/1jl/en/content/browse/%'
                  |AND [jcr:path] NOT LIKE '%archive%'
                  |AND [jcr:content/sling:resourceType] LIKE '%jl/components/pages/%page%'
                  |AND [jcr:content/sling:resourceType] NOT LIKE '%jl/components/pages/categorypage'
                  |AND [jcr:content/deleted] IS NULL""".stripMargin().stripIndent()
def result = queryManager.createQuery(statement, "JCR-SQL2").execute()
result.nodes.each {
    println it.path
}
