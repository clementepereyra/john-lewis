import groovy.transform.Field
import com.jl.aem.editorial.model.index.IndexModel
import org.apache.sling.models.export.spi.ModelExporter
import org.apache.sling.api.resource.Resource
import org.apache.jackrabbit.commons.JcrUtils

@Field CONTENT_TO_INDEX = [
        "/content/1jl/en/content/browse/content" : "onejl/components/pages/articlepage",
        "/content/1jl/en/content/browse/our-shops" : "onejl/components/pages/shoppage",
        "/content/1jl/en/content/browse/buying-guides" : "onejl/components/pages/contentpage",
        "/content/1jl/en/content/browse/our-services" : "onejl/components/pages/contentpage",
        "/content/1jl/en/content/browse/customer-services" : "onejl/components/pages/contentpage"
]

@Field serializationOptions = ["SerializationFeature.FAIL_ON_EMPTY_BEANS" : "false"]

this.&getIndexedData()

def getIndexedData(){
    def exporter = getService("org.apache.sling.models.export.spi.ModelExporter")

    def models = []
    CONTENT_TO_INDEX.each { path, resourceType ->
        def pageNodes = getNodes(path, resourceType)

        models.addAll(pageNodes.collect{
            def model = it.adaptTo(IndexModel.class)
            return model
        })
    }

    def res =  JsonOutput.prettyPrint(JsonOutput.toJson(models))
    saveIndexToJcrFile(res)
}

def saveIndexToJcrFile(index){
    Binary binary = session.getValueFactory().createBinary( new ByteArrayInputStream( index.getBytes( 'UTF-8' ) ))
    JcrUtils.getOrCreateByPath("/content/1jl/index.json", "nt:file", session)
    JcrUtils.getOrCreateByPath("/content/1jl/index.json/jcr:content", "nt:resource", session)

    session.getNode("/content/1jl/index.json/jcr:content").setProperty("jcr:data", binary)
    session.save()

    println("Saved intex to /content/1jl/index.json")
}

def getNodes(path, resourceType){
    def queryManager = session.workspace.queryManager
    def statement = 'SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE("' + path + '") AND [sling:resourceType] = "'+ resourceType + '"'

    println "Executing query = " + statement + "..."
    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()

    println "Query returned " + result.nodes.size() + " nodes..."

    return result.getNodes().collect{
        resourceResolver.getResource(it.getPath())
    }
}