import javax.jcr.Node
import javax.jcr.query.QueryManager
import java.time.Instant
import java.util.function.Function

def dryRun = binding.variables.data?.containsKey("dryRun") ? data.dryRun : true

updateNode("/content/1jl/en/content/browse/banners/test/jcr:content/par", removeNode("carouselbanner"))
updateNode("/content/1jl/en/content/browse/seobanners/electricals", removeNode("9600620013"))
updateNode("/content/1jl/en/content/browse/category/10139010570/10139010570/jcr:content/dynamicAreaPageContent/container/container/overlaycontainer/innerContainer", addBackgroundContentNode())
updateNode("/content/1jl/en/content/browse/category/10139281177/jcr:content/dynamicAreaPageContent/container/container/overlaycontainer/innerContainer", addBackgroundContentNode())
updateNode("/content/1jl/en/content/browse/category/9780211219/jcr:content/dynamicAreaPageContent/container_1406558076/container/overlaycontainer_141/innerContainer", addBackgroundContentNode())
updateNode("/content/1jl/en/content/browse/category/9780211219/97802112195/jcr:content/dynamicAreaPageContent/overlaycontainer_141_770058065/innerContainer", addBackgroundContentNode())
updateNode("/content/1jl/en/content/browse/category/50000296/500002969/jcr:content", removeProperties("rewriteArticlePublishDate"))
updateNode("/content/1jl/en/content/browse/our-services/john-lewis-fitted-kitchens/special-buy-fitted-kitchens/jcr:content", removeProperties("absoluteTime", "rewriteArticlePublishDate"))
updateComponent("onejl/components/authorInformation", removeProperties("imageSrc", "imageAlt"))

removeEmptyNodes()

if (!dryRun) {
    session.save()
}

def removeEmptyNodes() {
    [
            "/content/1jl/en/content/browse/category/10106220020/101062200202/jcr:content/dynamicAreaPageContent/container_968440322/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/10139301370/jcr:content/dynamicAreaPageContent/container_968440322/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/500001/jcr:content/dynamicAreaPageContent/container_copy_72077_766029182",
            "/content/1jl/en/content/browse/category/500001/jcr:content/dynamicAreaPageContent/container_copy_72077",
            "/content/1jl/en/content/browse/category/500001/50000114/jcr:content/dynamicAreaPageContent/container_copy_72077_766029182",
            "/content/1jl/en/content/browse/category/500001/50000114/jcr:content/dynamicAreaPageContent/container_copy_72077",
            "/content/1jl/en/content/browse/category/50000296/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/category/50000296/5000029610/jcr:content/dynamicAreaPageContent/container_1901060325_823241210/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/5000029610/jcr:content/dynamicAreaPageContent/container_1901060325_823241210/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/5000029610/jcr:content/dynamicAreaPageContent/container_1901060325_823241210/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_887696475/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_1834389270/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_1834389270/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/5000029612/jcr:content/dynamicAreaPageContent/container_1901060325_1834389270/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/category/50000296/500002969/jcr:content/dynamicAreaPageContent/container_1901060325_1055664655/container/image_1177994528_cop_881560674",
            "/content/1jl/en/content/browse/category/50000296/500002969/jcr:content/dynamicAreaPageContent/container_1901060325_1055664655/container/image_1177994528_cop",
            "/content/1jl/en/content/browse/category/50000296/500002969/jcr:content/dynamicAreaPageContent/container_1901060325_1055664655/container/image_1177994528_cop_1660913718",
            "/content/1jl/en/content/browse/customer-services/contact-us/jcr:content/par/accordion2_copy_copy_1432884350/container/container_1327831306/container/copy_copy_copy_copy_",
            "/content/1jl/en/content/browse/customer-services/contact-us/jcr:content/par/accordion2_copy_copy_1301683091/container/container_1327831306/container/container_copy_copy__413999242/container/copy_copy_copy_copy_1489834665",
            "/content/1jl/en/content/browse/customer-services/contact-us/jcr:content/par/accordion2_copy_copy_1301683091/container/container_1327831306/container/container_copy_copy__413999242/container/copy_copy_copy_copy",
            "/content/1jl/en/content/browse/homepage/jcr:content/par/container_100808159_/container/overlaycontainer/innerContainer/foregroundContent/container/container/container/divider_copy_1779761"
    ].forEach { removeEmptyNode(it) }
}

def removeEmptyNode(String nodePath) {
    def queryManager = session.workspace.queryManager as QueryManager
    def statement = "select * from [nt:unstructured] where [jcr:path] = '$nodePath'"
    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()
    result.nodes.each { Node node ->
        def propertyNames = node.properties.collect { it.name }
        propertyNames.remove("jcr:primaryType")

        def nodeNames = node.nodes.collect { it.name }
        nodeNames.remove("cq:responsive")

        if (propertyNames.empty && nodeNames.empty) {
            println "Removing empty node $node.path"
            node.remove()
        } else {
            println "WARN Skipping non empty node $node.path"
        }
    }
}

def updateComponent(String slingResourceType, Function<Node, Boolean> update) {
    def queryManager = session.workspace.queryManager as QueryManager
    def result = queryManager.createQuery("SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE('/content/1jl') AND [sling:resourceType]='$slingResourceType'", "JCR-SQL2").execute()
    def updated = result.nodes.collect { Node node -> update.apply(node) }
    def numUpdatedNodes = updated.count { it }
    println "Updated $numUpdatedNodes/${updated.size()} $slingResourceType"
}

def updateNode(String path, Function<Node, Boolean> update) {
    def queryManager = session.workspace.queryManager as QueryManager
    def result = queryManager.createQuery("SELECT * FROM [nt:base] WHERE [jcr:path] = '$path'", "JCR-SQL2").execute()
    def numUpdatedNodes = result.nodes.collect { Node node -> update.apply(node) }.count { it }
    println "Updated $numUpdatedNodes nodes at $path"
}

def removeMisplacedContainerContent() {
    def queryManager = session.workspace.queryManager as QueryManager
    def statement = "select * from [nt:unstructured] where [jcr:path] like '/content/1jl%' and [sling:resourceType] = 'onejl/components/container'"
    def result = queryManager.createQuery(statement, "JCR-SQL2").execute()

    result.nodes.each { Node container ->
        try {
            container.nodes.findAll { Node containerChild ->
                containerChild.hasProperty("sling:resourceType") && !["wcm/foundation/components/responsivegrid", "foundation/components/parsys"].contains(containerChild.getProperty("sling:resourceType").string)
            }.each { Node containerChild ->
                println "Removing misplaced container content type ${containerChild.getProperty("sling:resourceType").string} at $containerChild.path"
                containerChild.remove()
            }
        } catch (InvalidItemStateException e) {
            println "Already removed $container.path"
        }
    }
}

Function<Node, Boolean> removeProperties(String... propertyNames) {
    return { Node node ->
        def propertiesToRemove = propertyNames.findAll { node.hasProperty(it) }
        propertiesToRemove.each { node.getProperty(it).remove() }
        return !propertiesToRemove.empty
    }
}

Function<Node, Boolean> replaceProperty(String oldName, String oldValue, String newName, String newValue) {
    return { Node node ->
        if (node.hasProperty(oldName) && node.getProperty(oldName).string == oldValue) {
            node.getProperty(oldName).remove()
            if (!node.hasProperty(newName)) {
                node.setProperty(newName, newValue)
            }
            return true
        }

        return false
    }
}

Function<Node, Boolean> ifNotModifiedSince(String date) {
    return { Node node ->
        if (node.hasProperty("jcr:lastModified") && !node.getProperty("jcr:lastModified").date.toInstant().isAfter(Instant.parse(date))) {
            return true
        }

        println "WARN $node.path modified after $date"
        return false
    }
}

Function<Node, Boolean> addBackgroundContentNode() {
    return { Node node ->
        if (!node.hasNode("backgroundContent")) {
            def backgroundContent = node.addNode("backgroundContent", "nt:unstructured")
            backgroundContent.setProperty("sling:resourceType", "onejl/components/container")
            def container = backgroundContent.addNode("container", "nt:unstructured")
            container.setProperty("sling:resourceType", "wcm/foundation/components/responsivegrid")
            return true
        }

        return false
    }
}

Function<Node, Boolean> addLayoutContainer(String containerName) {
    return { Node node ->
        if (!node.hasNode(containerName)) {
            def layoutContainer = node.addNode(containerName, "nt:unstructured")
            layoutContainer.setProperty("sling:resourceType", "wcm/foundation/components/responsivegrid")
            return true
        }

        return false
    }
}

Function<Node, Boolean> removeNode(String nodeToRemove, Function<Node, Boolean> predicate = { true }) {
    return { Node node ->
        if (node.hasNode(nodeToRemove) && predicate.apply(node.getNode(nodeToRemove))) {
            node.getNode(nodeToRemove).remove()
            return true
        }

        return false
    }
}
