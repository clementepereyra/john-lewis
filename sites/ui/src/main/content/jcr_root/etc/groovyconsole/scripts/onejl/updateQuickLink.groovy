def dryRun = true

if (dryRun) println "Running script in dry run mode, no changes will be saved"

def queryManager = session.workspace.queryManager;
def statement = "select * from [nt:base] where [sling:resourceType] like '%quickLinks/quickLink'";

println "Executing query for quick link components..."

def result = queryManager.createQuery(statement, "JCR-SQL2").execute();

println "Query returned " + result.nodes.size() + " nodes..."
println "Checking nodes for wrong links..."

result.nodes.each { node ->
    updateLink(node)
}

if (!dryRun) save()

def updateLink(node) {
    if(node.hasProperty("link") && wrongLink(node.getProperty("link")) ) {
        node.setProperty("link", "/" + node.getProperty("link").value)
        println "\tUpdated link " + node.getProperty("link").value + " in " + node.path
    }
}

def wrongLink(link) {
    !( link.value.toString().startsWith("#") || link.value.toString().startsWith("/") )
}
