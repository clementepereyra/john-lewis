(function(){

    document.addEventListener('coral-component:attached', (e) => {
        const chevronUp = 'chevronUp';
        const chevronDown = 'chevronDown';

        const toggleItems = (event) => {
            if (event.target.tagName === 'CORAL-ICON'
                        && (event.target.classList.contains("collapse-multifield-item-button"))) {
                const isHidden = event.target.getAttribute('icon') === chevronDown;
                const itemsToHide = [].slice.apply(event.target.parentNode.parentNode.getElementsByClassName('coral-Form-fieldwrapper')).slice(1);
                itemsToHide.forEach(item => item.style.display = isHidden ? 'block' : 'none');
                event.target.setAttribute('icon', isHidden ? chevronUp : chevronDown);
            }
        }

        if (e.target.tagName === 'CORAL-DIALOG' && (e.target.getElementsByTagName('coral-multifield').length)) {
            e.target.getElementsByTagName('coral-multifield')[0].removeEventListener('click', toggleItems);
            e.target.getElementsByTagName('coral-multifield')[0].addEventListener('click', toggleItems);
        }
    });
})();