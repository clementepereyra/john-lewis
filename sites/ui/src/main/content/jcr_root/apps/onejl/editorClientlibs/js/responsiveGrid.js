(function() {
  var frame, oldWidth, newWidth;
  function step(){
    frame = document.getElementById('ContentFrame');
    newWidth = frame.offsetWidth;
    if (newWidth) {
      if(!oldWidth) {
        oldWidth = newWidth;
      } else if (oldWidth != newWidth) {
        if(oldWidth < 768 && newWidth >= 768) {
          $('#OverlayWrapper').addClass('jl-tablet-grid');
        } else {
          $('#OverlayWrapper').removeClass('jl-tablet-grid');
        }
        oldWidth = newWidth;
      }
    }
    window.requestAnimationFrame(step);
  }

  window.requestAnimationFrame(step);
  
})();


