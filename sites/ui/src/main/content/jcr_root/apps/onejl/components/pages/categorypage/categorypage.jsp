<%@page session="false" contentType="text/html; charset=utf-8" %>
<%@include file="/libs/foundation/global.jsp"%>

<html>
    <cq:include script="head.jsp"/>
    <cq:include script="body.jsp"/>

    <h2>Top content</h2>
    <div class="jl-wrapper" style="padding: 16px;">
		<cq:include path="dynamicCategoryTop" resourceType="foundation/components/parsys" />
    </div>


    <h2>Bottom Content</h2>
    <div class="jl-wrapper" style="padding: 16px;">
		<cq:include path="dynamicCategoryBottom" resourceType="foundation/components/parsys" />
    </div>
</html>
