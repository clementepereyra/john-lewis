$(document).on("cq-editor-loaded", () => {
    $(document).on("cq-layer-activated", function (event) {
      const items = [].slice.apply(document.getElementsByTagName('iframe')[0].contentWindow.document.querySelectorAll('.responsivegrid, .aem-GridColumn, .cq-Editable-dom'));
	  items.forEach((item) => {
          if (event.layer === 'Preview') {
		  	item.classList.add('previewMode');

          	addOrRemoveWrapperClass(item,false);

          } else {
            item.classList.remove('previewMode');

            addOrRemoveWrapperClass(item,true);
          }
      });
    });
})

function addOrRemoveWrapperClass(item,addOrRemove){
	$(item.getElementsByClassName("cms-wrapper")).each(function(){
        $(this).toggleClass("cms-wrapper-edit",addOrRemove);
    });
}