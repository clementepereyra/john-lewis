$(document).on("cq-editor-loaded", () => {
  $(document).on("cq-layer-activated", function (event) {
    if (event.layer !== 'Preview') {
      const accordions = document.getElementsByTagName('iframe')[0].contentWindow.document.querySelectorAll('.accordion-control.is-contracted');
      accordions.forEach(item => {
        $(item).click();
      });
    }
  });
})