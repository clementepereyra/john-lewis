
(function(document, $) {
    "use strict";

    // when dialog gets injected
    $(document).on("foundation-contentloaded", function(e) {
        // if there is already an inital value make sure the according target element becomes visible
        $(".cq-dialog-checkbox-enabledisable").each( function() {
            enableDisable($(this));
        });

    });

    $(document).on("change", ".cq-dialog-checkbox-enabledisable", function(e) {
        enableDisable($(this));
    });

    function enableDisable(el){

        var target = el.data("cqDialogCheckboxEnabledisableTarget");

        var checked = el.prop('checked');

        var value = checked ? el.val() : '';

        $(target).disable();

        $(target).filter("[data-enabledisabletargetvalue='" + value + "']").enable();

   }

   $.prototype.enable = function () {
       $.each(this, function (index, el) {
           if (el.hasAttribute('data-init')) {
               if (el.getAttribute('data-init') == 'pathbrowser'){
                   iteratePathBrowserDescendants(el, false);
               };
           } else {
               el.disabled = false;
           }
       });
   }

   $.prototype.disable = function () {
       $.each(this, function (index, el) {
           if (el.hasAttribute('data-init')) {
               if (el.getAttribute('data-init') == 'pathbrowser'){
                   iteratePathBrowserDescendants(el, true);
               };
           } else {
               el.disabled = true;
           }
       });
   }

})(document,Granite.$);