<%@include file="/libs/foundation/global.jsp"%>

<h2>Departments Widget</h2>

<div class="jl-wrapper" style="padding: 16px;">
	<%String removeBorders = properties.get("dynamicDepartmentsNoBorders", String.class);%>
    <section style="margin-left: -8px; width: calc(100% + 8px);">
    	<h3>Remove borders <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= removeBorders %>" /></p>
	</section>

	<cq:include path ="departments" resourceType ="foundation/components/parsys" />
</div>
