<%@include file="/libs/foundation/global.jsp"%>

<%@ page import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.foundation.Image,
                    com.day.cq.wcm.api.components.DropTarget,
                    com.day.cq.wcm.api.components.EditConfig,
                    com.day.cq.wcm.commons.WCMUtils,
					org.apache.jackrabbit.commons.JcrUtils"
                        %>

<h2>USP Promo Tile</h2>
<div class="jl-wrapper">
    <%String dynamicUspPromoTileText = properties.get("dynamicUspPromoTileText", String.class);%>
    <section>
    	<h3>USP title <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicUspPromoTileText %>" /></p>
	</section>

    <%String dynamicUspPromoTileCta = properties.get("dynamicUspPromoTileCta", String.class);%>
    <section>
    	<h3>Call to action text <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicUspPromoTileCta %>" /></p>
	</section>

    <%String dynamicUspPromoTileLink = properties.get("dynamicUspPromoTileLink", String.class);%>
    <section>
    	<h3>Link URL <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicUspPromoTileLink %>" /></p>
	</section>

    <%String dynamicUspPromoTileIcon = properties.get("dynamicUspPromoTileIcon", String.class);%>
    <section>
    	<h3>Icon <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicUspPromoTileIcon %>" /></p>
	</section>
</div>