<%@include file="/libs/foundation/global.jsp"%>

<%@ page import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.foundation.Image,
                    com.day.cq.wcm.api.components.DropTarget,
                    com.day.cq.wcm.api.components.EditConfig,
                    com.day.cq.wcm.commons.WCMUtils,
					org.apache.jackrabbit.commons.JcrUtils"
                        %>

<h2>Department item</h2>
<div class="jl-wrapper">
    <%String title = properties.get("dynamicDepartmentName", String.class);%>
    <section>
    	<h3>Department name <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= title %>" /></p>
	</section>

    <%String href = properties.get("dynamicDepartmentUrl", String.class);%>
    <section>
    	<h3>Link URL <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= href %>" /></p>
	</section>

    <%String imgUrl = properties.get("dynamicDepartmentImageSrc", String.class);%>
    <section>
    	<h3>Image URL <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= imgUrl %>" /></p>
	</section>
</div>
