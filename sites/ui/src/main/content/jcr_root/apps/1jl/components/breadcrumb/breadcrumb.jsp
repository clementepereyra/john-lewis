<%@include file="/libs/foundation/global.jsp"%>

<%@ page import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.foundation.Image,
                    com.day.cq.wcm.api.components.DropTarget,
                    com.day.cq.wcm.api.components.EditConfig,
                    com.day.cq.wcm.commons.WCMUtils,
					org.apache.jackrabbit.commons.JcrUtils"
                        %>

<h2>Breadcrumb Widget</h2>
<div class="jl-wrapper">
    <%String dynamicBreadcrumbText = properties.get("dynamicBreadcrumbText", String.class);%>
    <section>
    	<h3>Breadcrumb text <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicBreadcrumbText %>" /></p>
	</section>

    <%String dynamicBreadcrumbHref = properties.get("dynamicBreadcrumbHref", String.class);%>
    <section>
    	<h3>Link URL <span class="highlight">*</span>: </h3>
    	<p class="jl-value"><cq:text property="jcr:text" value="<%= dynamicBreadcrumbHref %>" /></p>
	</section>
</div>
