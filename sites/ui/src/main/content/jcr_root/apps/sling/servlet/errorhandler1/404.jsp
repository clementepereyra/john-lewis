<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Generic 404 error handler

  Important note:  
  Since Sling uses the user from the request (depending on the authentication
  handler but typically HTTP basic auth) to login to the repository and JCR/CRX
  will simply say "resource not found" if the user does not have a right to
  access a certain node, everything ends up in this 404 handler, both access
  denied ("401", eg. for non-logged in, anonymous users) and really-not-existing
  scenarios ("404", eg. logged in, but does not exist in repository).

--%><%
%><%@ page session="false" %><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %><%
%><sling:defineObjects /><%
%><%@ page import="
    org.apache.sling.engine.auth.Authenticator,
    org.apache.sling.engine.auth.NoAuthenticationHandlerException,
    com.day.cq.wcm.api.WCMMode,
    com.jl.aem.categorysearch.service.CategorySearchService" %><%!

    private boolean isAnonymousUser(HttpServletRequest request) {
        return request.getAuthType() == null
            || request.getRemoteUser() == null;
    }

    private boolean isBrowserRequest(HttpServletRequest request) {
        // check if user agent contains "Mozilla" or "Opera"
        final String userAgent = request.getHeader("User-Agent");
        return userAgent != null
            && (userAgent.indexOf("Mozilla") > -1
                || userAgent.indexOf("Opera") > -1);
    }

%><%

    boolean isAuthor = WCMMode.fromRequest(request) != WCMMode.DISABLED
            && !sling.getService(SlingSettingsService.class).getRunModes().contains("publish");

    // decide whether to redirect to the (wcm) login page, or to send a plain 404
    if (isAuthor
            && isAnonymousUser(request)
            && isBrowserRequest(request)) {
        
        Authenticator auth = sling.getService(Authenticator.class);
        if (auth != null) {
            try {
                auth.login(request, response);
                
                // login has been requested, nothing more to do
                return;
            } catch (NoAuthenticationHandlerException nahe) {
                bindings.getLog().warn("Cannot login: No Authentication Handler is willing to authenticate");
            }
        } else {
            bindings.getLog().warn("Cannot login: Missing Authenticator service");
        }

    }

    // get here if authentication should not take place or if
    // no Authenticator service is available or if no
    // AuthenticationHandler is willing to authenticate
    // So we fall back to plain old 404/NOT FOUND    
%>

<%

    //===============================================================================================================
    // OC-682 add these changes to redirect to category search servlet if category page was not found


    String path = request.getRequestURI();
    if(path.startsWith("/content/1jl/en/content/browse/category/")){
        String jsonResult = sling.getService(CategorySearchService.class).getCategoryPageJson(request.getPathInfo());
        if(jsonResult != null){
            out.println(jsonResult);
			out.flush();
			out.close();
        }
    }


	//================================================================================================================

%><%@include file="default.jsp"%>
