# one-jl-content
This repository contains the AEM templates, content and configuration for OneJL. It also provides a simple way (using docker) to create a local AEM instance for testing purposes.

There is lots of documentation in Confluence under [RESP:Content Management](https://www.jlpit.com/wiki/display/RESP/Content+Management) about how to create pages and use AEM in conjunction with the 1JL app and Barlow.

## Templates & Content
Run `./gradlew clean build` to build the project
Run `./gradlew contentDeploy` or `./gradlew` to deploy project to local environment with default instance setting.
Add project property 'env' to specify instnces configuration you want to use for deployment
e.g. './gradlew -PenvName=test'.

Note that by default deploy of content module is turned off, 
to deploy it to local machines use parameter includeContent like this: `./gradlew -PincludeContent`

 
## Bundles
Bundles will be added in the future and will be deployed by `./gradlew bundleDeploy` command


## Docker
To create a local instance of AEM you can simply run:

``` 
./aem.sh create-image
./aem.sh run-image
```

from the repository root. This takes some time as AEM installs for the first time. For further information, including the *prerequisites* for running a local instance, see the [Docker readme](docker/README.md)

## JSON schemas for OneJL components
The generateSchemas gradle task will generate JSON schemas for all OneJL components in the site/ui/src/main/content/jcr_root/apps/onejl/components dir
