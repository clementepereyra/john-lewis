package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;

import com.jl.aem.dam.pim.ProductService;

import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/DamImportProductsFromCsvAsset.json",
    "sling.servlet.methods=POST"
  }
)
public class DamImportProductsFromCsvAssetServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ProductService productService;

    ResourceResolver resourceResolver = null;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        
        String path = request.getParameter("import_path");
        String output = productService.importProducts(path);
        response.getWriter().write(output);
        response.setContentType("text/plain");
        
    }
    
}