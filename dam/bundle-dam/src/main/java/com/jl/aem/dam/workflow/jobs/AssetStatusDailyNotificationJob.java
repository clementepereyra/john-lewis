package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.AssetStatusDailyNotificationService;

/**
 * Schedulable job to purge dead references of assets from sku objects.
 * 
 * @author Bruno Szumpich
 *
 */
@Component(immediate = true, name = "Daily required action assets notification job",service= {Runnable.class},
    property ={"scheduler.expression=0 30 9 * * ? "})
public class AssetStatusDailyNotificationJob implements Runnable {

    private static final Logger logger = LoggerFactory
            .getLogger(AssetStatusDailyNotificationJob.class);

    @Reference
    private AssetStatusDailyNotificationService assetStatusDailyNotificationService;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance) {
            logger.trace("Running asset status daily notification job");
            assetStatusDailyNotificationService.execute();
            logger.trace("Asset status daily notification completed");
        }
    }
}