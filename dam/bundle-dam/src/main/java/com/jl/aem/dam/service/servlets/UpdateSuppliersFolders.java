package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.rmi.ServerException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.opencsv.CSVWriter;

@Component(service = {Servlet.class},
        property = {"sling.servlet.paths=/bin/jlp/public/updateSuppliersFolders.json",
                "sling.servlet.methods=GET"})
public class UpdateSuppliersFolders extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;
    private static final Logger logger = LoggerFactory.getLogger(UpdateSuppliersFolders.class);

    @Reference
    private JLPWorkflowSettings workflowSettings;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            if("removeSupplierEmailFolders".equals(request.getParameter("action"))) {
                removeSupplierEmailFolders(response, resourceResolver);
            } else if("setupBrandFolders".equals(request.getParameter("action"))) {
               setupBrandFolders(response, resourceResolver);
            } else if("cleanBrandFolders".equals(request.getParameter("action"))) {
                cleanBrandFolders(response, resourceResolver);
            } else if("logProducts".equals(request.getParameter("action"))) {
                logProducts(response, resourceResolver);
            } else {
                response.getWriter().println("Wrong Request: Specify action.");
            }
        } catch (Exception e) {
            logger.error("Can not migrate suppliers folders: ", e);
        } finally {
            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }


    }
    
    private void cleanBrandFolders(SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        try {

            Resource suppplierFolderResource =
                    resourceResolver.getResource(JLPConstants.SUPPLIERS_ROOT_FOLDER);
            Node supplierFolder = suppplierFolderResource.adaptTo(Node.class);
            NodeIterator brandFolderIt = supplierFolder.getNodes();
            while (brandFolderIt.hasNext()) {
                Node brandFolder = brandFolderIt.nextNode();
                if (!brandFolder.getName().equals("jcr:content")) {
                    brandFolder.remove();
                    resourceResolver.commit();
                }
            }
            resourceResolver.commit();

        } catch (Exception e) {
            logger.error("Can not setup brands", e);
        }
    }

    private void setupBrandFolders(SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        try {
            Resource resource = resourceResolver.getResource(JLPConstants.PRODUCTS_ROOT_FOLDER);
            Node node = resource.adaptTo(Node.class);
            NodeIterator directorateIt = node.getNodes();
            List<String> brandList = new LinkedList<>();
            while (directorateIt.hasNext()) {

                Node directorate = directorateIt.nextNode();
                NodeIterator yearIt = directorate.getNodes();
                while (yearIt.hasNext()) {
                    Node year = yearIt.nextNode();
                    NodeIterator brandsIt = year.getNodes();
                    while (brandsIt.hasNext()) {
                        Node brand = brandsIt.nextNode();
                        NodeIterator productIt = brand.getNodes();
                        while (productIt.hasNext()) {
                            Node product = productIt.nextNode();
                            logger.debug("Product Code: " + product.getName() + " - " + " Product Path: " + product.getPath());
                            if (product.hasProperty(ProductConstants.BRAND_NAME)) {
                                String brandName = product.getProperty(ProductConstants.BRAND_NAME)
                                        .getString();
                                if (!brandList.contains(brandName)) {
                                    brandList.add(brandName);
                                }
                            }
                        }
                    }
                }
            }
            Resource suppplierFolderResource =
                    resourceResolver.getResource(JLPConstants.SUPPLIERS_ROOT_FOLDER);
            Node supplierFolder = suppplierFolderResource.adaptTo(Node.class);
            NodeIterator brandFolderIt = supplierFolder.getNodes();
            while (brandFolderIt.hasNext()) {
                Node brandFolder = brandFolderIt.nextNode();
                if (!brandFolder.getName().equals("jcr:content")) {
                    brandFolder.remove();
                }
            }
            resourceResolver.commit();
            Collections.sort(brandList);
            for (String brandName : brandList) {
                getOrCreateBrandFolder(supplierFolder, brandName, resourceResolver);
                
                
                response.getWriter().print("Created folder for " + brandName + "\n");
            }
            resourceResolver.commit();
        } catch (Exception e) {
            logger.error("Can not setup brands", e);
        }
    }
    
    private void logProducts(SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        CSVWriter writer = null;
        try {
            response.setContentType("text/csv");

            // Make sure to show the download dialog
            response.setHeader("Content-disposition","attachment; filename=export.csv");

            OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), 
                    StandardCharsets.UTF_8);
            writer = new CSVWriter(osw);
            String[] entries = { "Product Code", "Product Path", "Is Deleted", "Modified" };
            writer.writeNext(entries);
            Resource resource = resourceResolver.getResource(JLPConstants.PRODUCTS_ROOT_FOLDER);
            Node node = resource.adaptTo(Node.class);
            NodeIterator directorateIt = node.getNodes();
            while (directorateIt.hasNext()) {

                Node directorate = directorateIt.nextNode();
                NodeIterator yearIt = directorate.getNodes();
                while (yearIt.hasNext()) {
                    Node year = yearIt.nextNode();
                    NodeIterator brandsIt = year.getNodes();
                    while (brandsIt.hasNext()) {
                        Node brand = brandsIt.nextNode();
                        NodeIterator productIt = brand.getNodes();
                        while (productIt.hasNext()) {
                            Node product = productIt.nextNode();
                            logger.debug("Product Code: " + product.getName() + " - " + " Product Path: " + product.getPath());
                            if (product.hasProperty(ProductConstants.BRAND_NAME)) {
                                String path = product.getPath();
                                String code = product.getName();
                                String modified = product.getProperty("jcr:lastModified").getString();
                                String isDeleted = "N";
                                if (product.hasProperty("deleted")) {
                                    isDeleted = product.getProperty("deleted").getString();
                                }
 
                                
                                entries = new String[]{ code, path, isDeleted, modified }; 
                                writer.writeNext(entries);
                                 
                                
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Can not setup brands", e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    logger.error("Can't close file", e);
                }
            }
            try {
                response.getWriter().flush();
            } catch (Exception e) {
                logger.error("Can't close file", e);
            }
        }
    }
    private void getOrCreateBrandFolder(Node supplierFolder, String brandName, ResourceResolver resourceResolver) {
        try {
            Node brandedFolder = null;
            if (supplierFolder.hasNode(JLWorkflowsUtils.encodePath(brandName))) {
                brandedFolder = supplierFolder.getNode(JLWorkflowsUtils.encodePath(brandName)); 
            } else {
                brandedFolder = supplierFolder.addNode(JLWorkflowsUtils.encodePath(brandName), "sling:Folder");
                Node jcrContent = brandedFolder.addNode("jcr:content", "nt:unstructured");
                jcrContent.setProperty("jcr:title", brandName);
                resourceResolver.commit();
                Thread.sleep(workflowSettings.getCommitTimeout());
            }
            createFolderIfNotExist(brandedFolder, "product", "Product Image");
            createFolderIfNotExist(brandedFolder, "video", "Video");
            createFolderIfNotExist(brandedFolder, "pdf", "PDFs");
            createFolderIfNotExist(brandedFolder, "logo", "Logo");
            createFolderIfNotExist(brandedFolder, "rejected", "Rejected");
            resourceResolver.commit();
            Thread.sleep(workflowSettings.getCommitTimeout());
        } catch (Exception e) {
            logger.error("Can not create folder for supplier", e);
        }
   }
    
    private Node createFolderIfNotExist(Node parent, String nodeName, String title) throws RepositoryException {
        if (parent.hasNode(nodeName)) {
            return parent.getNode(nodeName);
        }
        Node newFolder = parent.addNode(nodeName, "sling:Folder");
        Node jcrContent = newFolder.addNode("jcr:content", "nt:unstructured");
        jcrContent.setProperty("jcr:title", title);
        return newFolder;
    }
    


    private void removeSupplierEmailFolders(SlingHttpServletResponse response,
            ResourceResolver resourceResolver)
            throws RepositoryException, IOException, VersionException, LockException,
            ConstraintViolationException, AccessDeniedException, PersistenceException {
        Resource suppliersFolder =
                resourceResolver.getResource(JLPConstants.SUPPLIERS_ROOT_FOLDER);
        Node suppliersNode = suppliersFolder.adaptTo(Node.class);
        NodeIterator it = suppliersNode.getNodes();
        while (it.hasNext()) {
            Node supplierEmailFolder = it.nextNode();
            if ("jcr:content".equals(supplierEmailFolder.getName())) {
                continue;
            }
            NodeIterator supplierBrandsIt = supplierEmailFolder.getNodes();
            while (supplierBrandsIt.hasNext()) {
                Node brandFolder = supplierBrandsIt.nextNode();
                if ("jcr:content".equals(brandFolder.getName())) {
                    continue;
                }
                try {
                    response.getWriter().print("Moving: " + brandFolder.getPath() + " --> " + suppliersFolder.getPath());
                    resourceResolver.move(brandFolder.getPath(), suppliersFolder.getPath());
                    response.getWriter().print(" - OK\n");
                } catch (ItemExistsException e) {
                    response.getWriter().print(" - OK (Already Exist)\n");
                } catch (Exception e) {
                    response.getWriter().print(" - FAIL: " + e.getCause() + "\n");
                }
            }
            supplierEmailFolder.remove();
        }
        resourceResolver.commit();
    }

}
