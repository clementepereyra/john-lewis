package com.jl.aem.dam.workflow.step;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.Workspace;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;

@Component(immediate = true, service = WorkflowProcess.class,
        property = {"process.label=Process Request Asset 3rd Party Retouch"})
public class BriefRequestAssetThirdPartyRetouchWorkflowProcess
        extends AbstractBriefAssetRetouchedWorkflowProcess implements WorkflowProcess {

    private static final Logger log =
            LoggerFactory.getLogger(BriefRequestAssetThirdPartyRetouchWorkflowProcess.class);
    @Reference
    private JLPWorkflowSettings workflowSettings;
    @Reference
    ResourceResolverFactory resolverFactory;

    @Override
    public void execute(WorkItem item, WorkflowSession wfSession, MetaDataMap metadata)
            throws WorkflowException {
        try {

            Session session = wfSession.getSession();
            ResourceResolver resourceResolver = getResourceResolver(resolverFactory, session);
            
            Node projectNode = getProjectFromPayload(item, session, resolverFactory);
            String thirdPartyFolderPath = item.getMetaDataMap().get("comment", String.class);
            JcrUtils.getOrCreateByPath(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH, SLING_FOLDER,
                    session);
            session.save();

            StringBuilder targetBasePath =
                    new StringBuilder(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH).append(SLASH);

            String shareFolderBaseName = null;
            
            String agentBriefFolder = null;
            String agent = null;
            if (StringUtils.isNotBlank(thirdPartyFolderPath)) {
                agentBriefFolder = thirdPartyFolderPath.trim();
                agent = thirdPartyFolderPath.substring(0, thirdPartyFolderPath.indexOf(SLASH));
            } else {
                agentBriefFolder = JLPConstants.BORN_NAME;
                agent = JLPConstants.BORN_NAME;
            }
            
            targetBasePath.append(agentBriefFolder);
            Node targetBaseNode = JcrUtils.getOrCreateByPath(targetBasePath.toString(), SLING_ORDERED_FOLDER, session);
            String targetFolderFormattedTitle = null;
            if (!targetBaseNode.hasNode("jcr:content")) {
                Node targetBaseJCRContentNode = targetBaseNode.addNode("jcr:content", "nt:unstructured");
                targetFolderFormattedTitle = formatTargetFolderTitle(targetBaseNode.getName());
                targetBaseJCRContentNode.setProperty("jcr:title", targetFolderFormattedTitle);
            } else {
                targetFolderFormattedTitle = targetBaseNode.getNode("jcr:content").getProperty("jcr:title").getString();
            }
            session.save();

            targetBasePath.append("/");
            if (agentBriefFolder.indexOf(SLASH) >= 0) { //Comes from autocomplete
                shareFolderBaseName = agentBriefFolder.substring(agentBriefFolder.lastIndexOf(SLASH)+1);
            } else { // Comes from inbox
                shareFolderBaseName = getShareFolderName(projectNode);
                targetBasePath.append(shareFolderBaseName).append(SLASH);
            }
            
            String pendingRetouchTargetFolderPath = targetBasePath.toString() + shareFolderBaseName + "_" + JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER;
            String retouchedTargetFolderPath = targetBasePath.toString() + shareFolderBaseName + "_" + JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER;
            Node pendingRetouchNode = JcrUtils.getOrCreateByPath(pendingRetouchTargetFolderPath, SLING_ORDERED_FOLDER, session);
            Node retouchedNode = JcrUtils.getOrCreateByPath(retouchedTargetFolderPath, SLING_ORDERED_FOLDER, session);

            addJCRTitle(pendingRetouchNode, targetFolderFormattedTitle, "Pending Retouch");
            addJCRTitle(retouchedNode, targetFolderFormattedTitle, "Retouched");
            session.save();

            String workflowTitle = item.getWorkflow().getWorkflowData().getMetaDataMap()
                    .get("workflowTitle", String.class);
            

            String productCode = workflowTitle.split("-")[0];
            String altCode = workflowTitle.split("-")[1];

            Node jcrContent = projectNode.getNode("jcr:content");
            Node damFolderNode = resourceResolver.getResource(
                    jcrContent.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            Node productNode = damFolderNode.hasNode(productCode)?damFolderNode.getNode(productCode):null;
            if (productNode == null) {
                NodeIterator productNodes = damFolderNode.getNodes();
                while (productNodes.hasNext()) {
                    Node candidateNode = productNodes.nextNode();
                    if (candidateNode.hasProperty("ean")) {
                        if (productCode.equals(candidateNode.getProperty("ean").getString())) {
                            productCode = candidateNode.getName();
                            break;
                        }
                    }
                }
            }

            if (projectNode != null && StringUtils.isNotBlank(altCode)
                    && StringUtils.isNotBlank(productCode)) {
                copyPendingRetouchAsset(resourceResolver, session, projectNode, productCode,
                        altCode, pendingRetouchTargetFolderPath, retouchedTargetFolderPath, agent);
            }


        } catch (Exception e) {
            log.error("Error processing Request Asset 3rd Party Retouch: {}", e);
        }
    }
    
    private void addJCRTitle(Node targetNode, String targetFolderFormattedTitle,
            String suffix) {
        try {
            if (!targetNode.hasNode("jcr:content")) {
                Node targeContentNode = targetNode.addNode("jcr:content", "nt:unstructured");
                targeContentNode.setProperty("jcr:title", targetFolderFormattedTitle + " " + suffix);
            }
        } catch (Exception e) {
            log.error("Can't create jcr content folder", e);
        }
    }

    SimpleDateFormat nodeNameSDF = new SimpleDateFormat("yyyyMMdd_HHmmss");
    SimpleDateFormat nodeTitleSDF = new SimpleDateFormat("dd.MM.yy HH:mm");

    private String formatTargetFolderTitle(String name) {
        try {
            Date date = nodeNameSDF.parse(name.substring(name.lastIndexOf("-")+1));
            
            return name.substring(0, name.lastIndexOf("-")) + " " +nodeTitleSDF.format(date);
        } catch (Exception e) {
            log.error("Error formatting folder", e);
            return name;
        }
    }

    private String getShareFolderName(Node projectNode) throws RepositoryException {
        String projectName = projectNode.getName();
        
        return projectName + "-" + nodeNameSDF.format(new Date());
    }

    private String copyPendingRetouchAsset(ResourceResolver resourceResolver, Session session,
            Node projectNode, String productCode, String altCode, String pendingRetouchNodePath,
            String retouchedTargetFolderPath, String agent)
            throws PathNotFoundException, RepositoryException, ValueFormatException {
        Node projectContentNode = projectNode.getNode(JCR_CONTENT);

        if (projectContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
            String damFolderPath =
                    projectContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();

            StringBuilder assetPathSB = new StringBuilder(damFolderPath).append(SLASH)
                    .append(productCode).append(SLASH).append(altCode);

            Resource altRes = resourceResolver.getResource(assetPathSB.toString());
            if (altRes != null) {
                Node altNode = altRes.adaptTo(Node.class);
                if (altNode.hasNode("Composites")) {
                    copyComposites(session, pendingRetouchNodePath, altNode);
                }
                NodeIterator nodeIt = altNode.getNodes();
                while (nodeIt.hasNext()) {
                    Node assetNode = (Node) nodeIt.next();
                    if (assetNode.getPrimaryNodeType().getName().equals(DAM_ASSET)) {
                        String targetAssetPath = new StringBuilder(pendingRetouchNodePath)
                                .append(SLASH).append(assetNode.getName()).toString();
                        Workspace workspace = session.getWorkspace();
                        workspace.copy(assetNode.getPath(), targetAssetPath);
                        session.save();
                        

                        Node assetContentNode = assetNode.getNode(JCR_CONTENT);
                        if (assetContentNode != null) {
                            assetContentNode.setProperty(JLPConstants.THIRD_PARTY_ASSET_PATH,
                                    targetAssetPath.replace(
                                            JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER,
                                            JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER));
                            assetContentNode.getNode("metadata").setProperty(JLPConstants.RETOUCH_MODE, "thirdparty");
                            altNode.setProperty(JLPConstants.RETOUCH_MODE, "thirdparty");
                            session.save();
                        }
                        
                        Resource targetResource = resourceResolver.getResource(targetAssetPath);
                        if (targetResource != null) {
                            Node targetNode = targetResource.adaptTo(Node.class);
                            Node targetMetadataNode = targetNode.getNode("jcr:content/metadata");
                            targetMetadataNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.READY_TO_RETOUCH.toString());
                            targetMetadataNode.setProperty(JLPConstants.OUTSOURCED_TO, agent);
                            session.save();
                        }
                        return targetAssetPath;
                    }
                }
            }
        }
        return null;
    }

    private void copyComposites(Session session, String pendingRetouchNodePath, Node altNode)
            throws ConstraintViolationException, VersionException, AccessDeniedException,
            PathNotFoundException, ItemExistsException, LockException, RepositoryException,
            ReferentialIntegrityException, InvalidItemStateException, NoSuchNodeTypeException {

        Node compositesFolderNode = altNode.getNode("Composites");
        NodeIterator compositeIt = compositesFolderNode.getNodes();
        int counter = 1;
        String itemName = null;
        Node itemNode = altNode.getParent();
        if (itemNode.hasProperty("webSku")) {
            itemName = JLWorkflowsUtils.addLeftZeros(itemNode.getProperty("webSku").getString());
        } else if (itemNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)) {
            itemName = itemNode.getProperty(JLPLifeStyleConstants.NAMING_CONVENTION).getString();
        } else {
            itemName = JLWorkflowsUtils.addLeftZeros(itemNode.getName());
        }
        while (compositeIt.hasNext()) {
            Node assetComp = compositeIt.nextNode();
            if (assetComp.getPrimaryNodeType().getName().equals(DAM_ASSET)) {
                String assetCompName = assetComp.getName();
                String altNodeName = altNode.getName();
                String baseAssetName = altNodeName.equals("main")?"":altNodeName;
                String extension = assetCompName.substring(assetCompName.lastIndexOf("."));
                String targetPath = pendingRetouchNodePath + SLASH + itemName + baseAssetName + "_comp" + counter + extension;
                Workspace workspace = session.getWorkspace();
                workspace.copy(assetComp.getPath(), targetPath);
                session.save();
                counter++;
            }
        }
        
    }


}
