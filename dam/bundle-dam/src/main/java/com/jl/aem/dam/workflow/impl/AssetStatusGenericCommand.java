package com.jl.aem.dam.workflow.impl;



import org.osgi.service.component.annotations.Component;

import com.jl.aem.dam.workflow.AssetStatusCommand;

@Component(immediate=true, service=AssetStatusGenericCommand.class)
public class AssetStatusGenericCommand implements AssetStatusCommand {

    @Override
    public void execute(final String assetPath, final String userID) {
        // Do nothing
    }
}
