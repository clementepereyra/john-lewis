package com.jl.aem.dam.pim.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.pim.ProductService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/create_product",
    "sling.servlet.methods=POST"
  }
)
public class CreateProductServlet extends SlingAllMethodsServlet{

    /**
     * 
     */
    private static final long serialVersionUID = 1946503157051545195L;
    
    private static final Logger logger = LoggerFactory.getLogger(CreateProductServlet.class);


    @Reference
    private ProductService productService;
    
    private static Gson gson = new Gson();

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        long start = System.currentTimeMillis();
        response.setContentType("application/json");
        Product product = getProductFromRequest(request);
        long parseObject = System.currentTimeMillis();
        if (product != null) {
            String prodCode = product.getProdCode();
            if (StringUtils.isBlank(product.getProdCode())) {
                CreateProductResponse resp = new CreateProductResponse(400, "Product Code can not be empty",prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                
                return;
            }
            if (StringUtils.isBlank(product.getDirectorate())) {
                CreateProductResponse resp = new CreateProductResponse(400, "Directorate can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getYear())) {
                CreateProductResponse resp = new CreateProductResponse(400, "Year can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getSeasonCode())) {
                CreateProductResponse resp = new CreateProductResponse(400, "Season Code can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getBrandName())) {
                CreateProductResponse resp = new CreateProductResponse(400, "Brand Name can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            long validateObject = System.currentTimeMillis();
           try {
                if (!productService.exist(product.getProdCode())) {
                    long queryExistent = System.currentTimeMillis();
                    productService.addProduct(product);
                    long addObject = System.currentTimeMillis();
                    StringBuilder builder = new StringBuilder("Insert product: \n");
                    builder.append("Parse object: " + (parseObject - start) +  " ms\n");
                    builder.append("Validate object: " + (validateObject - parseObject) +  " ms\n");
                    builder.append("Query Existent: " + (queryExistent - validateObject) +  " ms\n");
                    builder.append("Add object: " + (addObject - queryExistent) +  " ms\n");
                    builder.append("Total Insert: " + (addObject - start) +  " ms\n");
                    if (logger.isDebugEnabled()) {
                        logger.debug(builder.toString());
                    }
                    CreateProductResponse resp = new CreateProductResponse(200, "", prodCode);
                    response.getWriter().write(gson.toJson(resp));
                    return;
                } else {
                    CreateProductResponse resp = new CreateProductResponse(401, "Product already exist", prodCode);
                    response.getWriter().write(gson.toJson(resp));
                    response.setStatus(401);
                    return;
                }
            } catch (Exception e) {
                CreateProductResponse resp = new CreateProductResponse(500, "Internal error while creating product", prodCode);
                response.setStatus(500);
                response.getWriter().write(gson.toJson(resp)); 
            }
        } else {
            CreateProductResponse resp = new CreateProductResponse(400, "Can not parse json object from request body - malformed json body", "");
            response.setStatus(400);
            response.getWriter().write(gson.toJson(resp));
        }
        
    }
    
    private Product getProductFromRequest(SlingHttpServletRequest request) {
        try {
            BufferedReader reader = request.getReader();
            Gson gson = new Gson();
            Product product = gson.fromJson(reader, Product.class);
            return product;
        } catch (Exception e) {
            logger.error("Can not parse product from request body - aborting", e);
            return null;
        }
        
    }

    public static class CreateProductResponse {
        private int status;
        private String statusDescription;
        private String error;
        private String prod_code;
        
        public CreateProductResponse(int status, String error, String productCode) {
            this.status = status;
            statusDescription = "ERROR";
            if (status < 400) {
                statusDescription = "SUCCESS";
            }
            this.prod_code = productCode;
            this.error = error;
        }
        
        public String getProdCode() {
            return prod_code;
        }
        
        public int getStatus() {
            return status;
        }
        
        public String getError() {
            return error;
        }
        
        public String getStatusDescription() {
            return statusDescription;
        }
    }
    
}
