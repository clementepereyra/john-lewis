package com.jl.aem.dam.briefs.workflow;

import javax.jcr.Node;

public class ProjectWorkflowInfo {

    private final String workflowTitle;
    private final String workflowNodeName;
    private final String workflowModel;
    private final Node projectContentNode;
    private final String userId;
    
    
    public ProjectWorkflowInfo(final String workflowTitle, final String workflowNodeName, final String workflowModel,
            final Node projectContentNode, final String userId) {
        this.workflowTitle = workflowTitle;
        this.workflowNodeName = workflowNodeName;
        this.workflowModel = workflowModel;
        this.projectContentNode = projectContentNode;
        this.userId = userId;
    }
    
    public String getWorkflowTitle() {
        return workflowTitle;
    }
    public String getWorkflowNodeName() {
        return workflowNodeName;
    }
    public String getWorkflowModel() {
        return workflowModel;
    }
    public Node getProjectContentNode() {
        return projectContentNode;
    }
    public String getUserId() {
        return userId;
    }
    
}
