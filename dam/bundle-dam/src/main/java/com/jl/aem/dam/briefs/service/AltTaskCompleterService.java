package com.jl.aem.dam.briefs.service;

import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;

public interface AltTaskCompleterService {

    boolean canComplete(String assetPath, AutocompleteStatus newStatus);
    void complete(String assetPath, AutocompleteStatus newStatus, String comments);
    
}
