package com.jl.aem.dam.reports.models.results;

public class JLSNIItem {

    private String briefPath;
    private String briefName;
    private String startShootDate;
    private String endShootDate;
    private String dueDate;
    private String countAssets;
    private String countProducts;
    public String getBriefPath() {
        return briefPath;
    }
    public void setBriefPath(String briefPath) {
        this.briefPath = briefPath;
    }
    public String getBriefName() {
        return briefName;
    }
    public void setBriefName(String briefName) {
        this.briefName = briefName;
    }
    public String getStartShootDate() {
        return startShootDate;
    }
    public void setStartShootDate(String startShootDate) {
        this.startShootDate = startShootDate;
    }
    public String getEndShootDate() {
        return endShootDate;
    }
    public void setEndShootDate(String endShootDate) {
        this.endShootDate = endShootDate;
    }
    public String getDueDate() {
        return dueDate;
    }
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    public String getCountAssets() {
        return countAssets;
    }
    public void setCountAssets(String countAssets) {
        this.countAssets = countAssets;
    }
    public String getCountProducts() {
        return countProducts;
    }
    public void setCountProducts(String countProducts) {
        this.countProducts = countProducts;
    }
    
    
}
