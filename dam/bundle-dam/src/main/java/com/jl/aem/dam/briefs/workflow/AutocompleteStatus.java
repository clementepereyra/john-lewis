package com.jl.aem.dam.briefs.workflow;

public enum AutocompleteStatus {

    INTERNAL_RETOUCH, EXTERNAL_RETOUCH, RESHOOT, ALT_NOT_NEEDED, EXTERNAL_RETOUCH_DONE, COLOUR_MATCH_NOT_NEEDED;
}
