package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.DateRangePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.NextToExpireNotificationService;

@Component(immediate = true, name = "JLP DAM Next to Expire Notification Service",
        service = NextToExpireNotificationService.class)
public class NextToExpireNotificationServiceImpl implements NextToExpireNotificationService {

    private static final Logger logger =
            LoggerFactory.getLogger(NextToExpireNotificationServiceImpl.class);

    private static final String NOTIFICATION_ITEM_ROOT =
            "/etc/jl-admin/roll-out/rights-expiring-notification/jcr:content/list/item";

    private static final String DISABLE_JOB = "disableJob";
    private static final boolean DEFAULT_DISABLE_JOB = false;

    private static final String EMAIL = "emails";

    private static final String DATE_RATIO = "ratio";
    private static final int DEFAULT_RATIO = 14;

    private static final String OBSERVED_PATH = "observedPath";
    private static final String DEFAULT_DAM_PATH = "/content/dam";

    private static final String LOGO_URL = "logoUrl";
    private static final String DEFAULT_LOGO_URL =
            "https://www.johnlewis.com/assets/header/john-lewis-logo.gif";

    private static final String LOGO_ALT = "logoAlt";
    private static final String DEFAULT_LOGO_ALT = "John Lewis";

    private static final String LOGO_HEIGHT = "logoHeight";
    private static final int DEFAULT_LOGO_HEIGHT = 30;

    private static final String SIGNATURE = "signature";
    private static final String DEFAULT_SIGNATURE = "John Lewis";

    private static String emailTemplate =
            "/etc/notification/email/html/jlp/nextToExpireNotificationTemplate.txt";

    private static final String OPEN_TR = "<tr>";
    private static final String CLOSE_TR = "</tr>";

    private static final String OPEN_TD = "<td>";
    private static final String CLOSE_TD = "</td>";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Override
    public void execute() {
        logger.debug("JLP DAM Next to Expire Notification Service");

        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }

        try {
            boolean disableNotification =
                    getDisableNotification(resourceResolver, NOTIFICATION_ITEM_ROOT);
            logger.debug("Disable Notification {}", disableNotification);

            if (!disableNotification) {
                List<String> emailAddress = getEmailList(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Number of emails address to notifiy {}", emailAddress.size());
                int dateRatio = getDateRatio(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Date ratio {}", dateRatio);
                String observedPath = getObservedPath(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Observed Path {}", observedPath);

                String logoUrl = getLogoUrl(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Logo Url {}", logoUrl);
                String logoAltText = getLogoAltText(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Logo Alt Text {}", logoAltText);
                int logoHeight = getLogoHeight(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Logo Height {}", logoHeight);
                String signature = getSignature(resourceResolver, NOTIFICATION_ITEM_ROOT);
                logger.debug("Email Signature {}", signature);

                SearchResult result =
                        searchAssets(resourceResolver, session, observedPath, dateRatio);
                logger.debug("Number of assets next to expire {}", result.getTotalMatches());

                if (result.getTotalMatches() > 0) {
                    List<Hit> hits = result.getHits();
                    sendNotificationEmail(resourceResolver, session, dateRatio, emailAddress, hits,
                            logoUrl, logoAltText, logoHeight, signature);
                }
            }
        } catch (Exception e) {
            logger.error("Error while trying of notificate Assets next to expire: {}", e);
        }
    }

    private void sendNotificationEmail(ResourceResolver resourceResolver, Session session,
            int dateRatio, List<String> emailAddress, List<Hit> hits, String logoUrl,
            String logoAltText, int logoHeight, String signature) {
        logger.debug("Sending Notification Email");
        try {
            if (!emailAddress.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (Hit hit : hits) {
                    Node node = hit.getNode();
                    if (node.hasNode("jcr:content/metadata")) {
                        Node metadata = node.getNode("jcr:content/metadata");
                        sb.append(OPEN_TR);
                        sb.append(OPEN_TD).append(node.getPath()).append(CLOSE_TD);

                        if (metadata.hasProperty(JLPConstants.JLP_DIRECTORATE)) {
                            sb.append(OPEN_TD)
                                    .append(StringUtils.capitalize(
                                            metadata.getProperty(JLPConstants.JLP_DIRECTORATE)
                                                    .getString().replace("jlp:directorate/", "")))
                                    .append(CLOSE_TD);
                        } else {
                            sb.append(OPEN_TD).append(CLOSE_TD);
                        }

                        if (metadata.hasProperty(JLPConstants.JLP_BRAND_NAME)) {
                            sb.append(OPEN_TD)
                                    .append(StringUtils.capitalize(metadata
                                            .getProperty(JLPConstants.JLP_BRAND_NAME).getString()))
                                    .append(CLOSE_TD);
                        } else {
                            sb.append(OPEN_TD).append(CLOSE_TD);
                        }

                        if (metadata.hasProperty("prism:expirationDate")) {
                            sb.append(OPEN_TD)
                                    .append(StringUtils.capitalize(metadata
                                            .getProperty("prism:expirationDate").getString()))
                                    .append(CLOSE_TD);
                        } else {
                            sb.append(OPEN_TD).append(CLOSE_TD);
                        }
                        sb.append(CLOSE_TR);
                    }
                }
                String pathList = sb.toString();
                // Getting the Email template.
                Resource templateRsrc = resourceResolver.getResource(emailTemplate);

                MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
                MessageGateway<HtmlEmail> messageGateway =
                        messageGatewayService.getGateway(HtmlEmail.class);

                // Creating the Email.
                HtmlEmail email = new HtmlEmail();
                Map<String, String> emailProperties = new HashMap<String, String>();

                emailProperties.put("dateRatio", Integer.toString(dateRatio));
                emailProperties.put("logoUrl", logoUrl);
                emailProperties.put("logoAlt", logoAltText);
                emailProperties.put("logoHeight", Integer.toString(logoHeight));
                emailProperties.put("pathList", pathList);
                emailProperties.put("signature", signature);
                email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties),
                        HtmlEmail.class);

                email.setTo(getEmailRecipients(emailAddress));

                messageGateway.send(email);
                logger.debug("Notification Emails Sent");
            }
        } catch (Exception e) {
            logger.error("Fatal error while sending email: ", e);
        }

    }

    private List<InternetAddress> getEmailRecipients(List<String> recipients) {

        ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
        for (String recipient : recipients) {
            try {
                emailRecipients.add(new InternetAddress(recipient));
            } catch (AddressException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return emailRecipients;
    }

    private List<String> getEmailList(ResourceResolver resourceResolver,
            String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        List<String> result = new ArrayList<>();
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(EMAIL)) {
                Property property = node.getProperty(EMAIL);
                if (property.isMultiple()) {
                    Value[] values = node.getProperty(EMAIL).getValues();
                    for (Value value : values) {
                        result.add(value.getString());
                    }
                } else {
                    result.add(node.getProperty(EMAIL).getString());
                }
            }
        }
        return result;
    }

    private boolean getDisableNotification(ResourceResolver resourceResolver,
            String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(DISABLE_JOB)) {
                return Boolean.parseBoolean(node.getProperty(DISABLE_JOB).getString());
            }
        }
        return DEFAULT_DISABLE_JOB;
    }


    private int getDateRatio(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(DATE_RATIO)) {
                String result = node.getProperty(DATE_RATIO).getString();
                try {
                    return Integer.parseInt(result);
                } catch (NumberFormatException e) {
                    logger.debug("Cant get date ratio, returning the default one: {} days",
                            DEFAULT_RATIO);
                    return DEFAULT_RATIO;
                }
            }
        }
        return DEFAULT_RATIO;
    }

    private String getObservedPath(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(OBSERVED_PATH)) {
                return node.getProperty(OBSERVED_PATH).getString();
            }
        }
        return DEFAULT_DAM_PATH;
    }

    private String getLogoUrl(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_URL)) {
                return node.getProperty(LOGO_URL).getString();
            }
        }
        return DEFAULT_LOGO_URL;
    }

    private String getLogoAltText(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_ALT)) {
                return node.getProperty(LOGO_ALT).getString();
            }
        }
        return DEFAULT_LOGO_ALT;
    }

    private int getLogoHeight(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_HEIGHT)) {
                String result = node.getProperty(LOGO_HEIGHT).getString();
                try {
                    return Integer.parseInt(result);
                } catch (NumberFormatException e) {
                    logger.debug("Cant get logo height, returning the default one: {}",
                            DEFAULT_LOGO_HEIGHT);
                    return DEFAULT_LOGO_HEIGHT;
                }
            }
        }
        return DEFAULT_LOGO_HEIGHT;
    }

    private String getSignature(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(SIGNATURE)) {
                return node.getProperty(SIGNATURE).getString();
            }
        }
        return DEFAULT_SIGNATURE;
    }

    private SearchResult searchAssets(ResourceResolver resourceResolver, Session session,
            String observedPath, int dateRatio) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", observedPath);

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "dam:Asset");

        Predicate datePredicate = new Predicate("daterange", "daterange");
        datePredicate.set(DateRangePredicateEvaluator.PROPERTY,
                "jcr:content/metadata/prism:expirationDate");
        long expirationTime = dateRatio * 24 * 60 * 60 * 1000;
        datePredicate.set(DateRangePredicateEvaluator.UPPER_BOUND,
                "" + (System.currentTimeMillis() + expirationTime));
        datePredicate.set(DateRangePredicateEvaluator.UPPER_OPERATION, "<=");

        Predicate datePredicate2 = new Predicate("daterange2", "daterange");
        datePredicate2.set(DateRangePredicateEvaluator.PROPERTY,
                "jcr:content/metadata/prism:expirationDate");
        datePredicate2.set(DateRangePredicateEvaluator.LOWER_BOUND,
                "" + (System.currentTimeMillis()));
        datePredicate2.set(DateRangePredicateEvaluator.LOWER_OPERATION, ">=");

        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.add(datePredicate);
        predicates.add(datePredicate2);
        predicates.setAllRequired(true);

        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(predicates, session);

        query.setHitsPerPage(0);

        return query.getResult();
    }

}
