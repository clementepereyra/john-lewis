package com.jl.aem.dam.service.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = {Servlet.class}, property = {
        "sling.servlet.paths=/bin/jlp/public/showForceCompleteButton", "sling.servlet.methods=GET"})
public class ShowForceCompleteButton extends SlingAllMethodsServlet {

    private static final long serialVersionUID = -1079425426078116490L;

    @Reference
    private BriefsService briefService;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {

        String projectPath = request.getParameter("project_path");
        boolean showForceCompleteButton = briefService.showForceCompleteButton(projectPath);

        response.getWriter().write("{\"result\":\"" + showForceCompleteButton + "\"}");
        response.setContentType("application/json");
    }



}
