package com.jl.aem.dam.reports.models.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.reports.api.ResultsPage;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.reports.models.results.OverDueProjectDetailsItem;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

public class JLOverDueProjectDetailsHelper implements JLReportExecutorHelper {
    
    private static final Logger log = LoggerFactory.getLogger(JLOverDueProjectDetailsHelper.class);
    
    private Map<String, String> parameters  = new HashMap<String, String>();
    
    private static final String START_DATE_LOWER_BOUND_PARAM = "startDateLowerBound";
    private static final String START_DATE_UPPER_BOUND_PARAM = "startDateUpperBound";
    private static final String DUE_DATE_LOWER_BOUND_PARAM = "dueDateLowerBound";
    private static final String DUE_DATE_UPPER_BOUND_PARAM = "dueDateUpperBound";
    
    @Override
    public ResultsPage fetchResults(SlingHttpServletRequest request) {
    	
        List<Object> results = new ArrayList<>();
        try {
            prepareParameters(request);
            
            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            List<Node> briefs = new ArrayList<Node>();
            
            StringBuilder queryString = new StringBuilder("SELECT s.* "
                    + " FROM [nt:unstructured] AS s "
                    + " WHERE ISDESCENDANTNODE(s, [/content/projects]) "
                    + " AND s.[sling:resourceType]='cq/gui/components/projects/admin/card/projectcard'"
                    + " AND s.[jcr:content/JLPStatus] = 'ACTIVE'"
                    ); 
            if (parameters.get(START_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(START_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(START_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(START_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]<=" + upperBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(DUE_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(DUE_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]<=" + upperBound.getTime());
                }
            }
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery(queryString.toString(), javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            Date currentDate = new Date();
            while (iterator.hasNext()) {
                Node brief = iterator.nextNode();
                Date actualDueDate = null;
                Date productReDueDate = getBriefDate(brief, "project.redueDate");
                if(productReDueDate == null) {
                	actualDueDate  = getBriefDate(brief, "project.dueDate");
                }else actualDueDate = productReDueDate;
                if (actualDueDate != null  && actualDueDate.getTime() <= currentDate.getTime()) {
                    briefs.add(brief);
                }
                    
            }
            
            for (Node brief : briefs) { 
            	OverDueProjectDetailsItem item = new OverDueProjectDetailsItem();
            	Date shootStartDate = getBriefDate(brief, "project.shootstartDate");
            	if(null != shootStartDate) {
            		item.setStartShootDate(shootStartDate.toString());
            	}
            	Date shootEndDate = getBriefDate(brief, "project.shootendDate");
            	if(null != shootEndDate) {
            		item.setEndShootDate(shootEndDate.toString());
            	}
                item.setBriefType(getBriefType(brief));
                Date actualDueDate = null;
                Date productReDueDate = getBriefDate(brief, "project.redueDate");
                if(productReDueDate == null) {
                	actualDueDate  = getBriefDate(brief, "project.dueDate");
                }else actualDueDate = productReDueDate;
                
                item.setActualDueDate(actualDueDate.toString());
                if(null != actualDueDate) {
                	BriefSummary prodSummary = getBriefSummary(brief, resourceResolver);
                	if((currentDate.getTime() > actualDueDate.getTime())) {
                		 item.setCountAssets(prodSummary.getCountAssets());                		
                	}
                }
                
               
                item.setProjectName(getProjectName(brief));                
                results.add(item);
            }            
        } catch (Exception e) {
        	log.error("Can't create OverDue report", e);
        }
        ResultsPage page = new ResultsPage(results, results.size(), 0);
        return page;

    }

    private BriefSummary getBriefSummary(Node brief, ResourceResolver resourceResolver) {
        BriefSummary result = new BriefSummary();    
        Map<BriefAssetStatus, Integer> statusMap = new HashMap<>();
        statusMap.put(BriefAssetStatus.ASSET_DELIVERED, 0);
        statusMap.put(BriefAssetStatus.ASSET_REQUESTED, 0);
        statusMap.put(BriefAssetStatus.ASSET_RETOUCHED, 0);
        statusMap.put(BriefAssetStatus.ASSET_UPLOADED, 0);
        statusMap.put(BriefAssetStatus.MARK_UP_NEEDED, 0);
        statusMap.put(BriefAssetStatus.RE_SHOOT_NEEDED, 0);
        statusMap.put(BriefAssetStatus.ALT_NOT_NEEDED , 0);
        statusMap.put(BriefAssetStatus.READY_TO_RETOUCH, 0);
        List<Node> products = getItemsInBrief(brief, resourceResolver);
        for (Node prodNode : products) {            
            try {
                NodeIterator it = prodNode.getNodes();
                while (it.hasNext()) {
                    Node assetNode = it.nextNode();
                    if (assetNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                        BriefAssetStatus status = BriefAssetStatus.valueOf(assetNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                        statusMap.put(status, statusMap.get(status)+1);
                    }
                }
            } catch (Exception e) {
                log.error("Can't get assets summary for product.", e);
            }
        }
        int undeliveredCount = statusMap.get(BriefAssetStatus.ASSET_RETOUCHED) + statusMap.get(BriefAssetStatus.MARK_UP_NEEDED) + statusMap.get(BriefAssetStatus.RE_SHOOT_NEEDED)+
               statusMap.get(BriefAssetStatus.ASSET_UPLOADED) + statusMap.get(BriefAssetStatus.ASSET_REQUESTED);
        result.setCountAssets(Integer.toString(undeliveredCount));       

        return result;
    }    

    private String getProjectName(Node brief) {
        String projectName = "N/A";
        try {
            Node jcrContent = brief.getNode("jcr:content");
            projectName = jcrContent.getProperty("jcr:title").getString();
        } catch (Exception e) {
            log.error("Can't get project name", e);
        }
        return projectName;
    }

    @Override
    public String getDetails() {
        return "<dl>" + "Inflight Content Report Executor" + "</dl>";
    }

    @Override
    public String getParameters() {
        return "";
    }

    
    private void prepareParameters(SlingHttpServletRequest request) {
        parameters = new HashMap<String, String>();
        @SuppressWarnings("unchecked")
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
          String key = paramNames.nextElement();
          parameters.put(key, StringEscapeUtils.escapeSql(request.getParameter(key)));
        }
    }
    
    
    
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Date parseDate(String stringDate) {
        Date date = null;
        String sanitized = stringDate;
        if (stringDate.length() >= 10) {
            sanitized = stringDate.substring(0, 10);
            try { 
                date = sdf.parse(sanitized);
            } catch (Exception e) {
                log.error("Error parsing date", e);
            }
        }
        return date;
    }
    
    private Date getBriefDate(Node brief, String string) {
        Node jcrContent;
        try {
            jcrContent = brief.getNode("jcr:content");
            if (jcrContent.hasProperty(string) && jcrContent.getProperty(string).getLength() > 0) {
                return jcrContent.getProperty(string).getDate().getTime();
            }
        } catch (Exception e) {
            log.error("Can't parse date", e);
        }
        return null;
    }

    

    private String getBriefType(Node brief) {
        try {
            Node projectContentNode = brief.getNode("jcr:content");
            if (BriefUtils.isCutoutBrief(projectContentNode)) {
                return "cutout";
            } else if (BriefUtils.isLifestyleBrief(projectContentNode)) {
                return "lifestyle";
            } else if (BriefUtils.isRetouchBrief(projectContentNode)) {
                return "retouch";
            }
        } catch (Exception e) {
            log.error("Can't get brief type for report", e);
        }
        return null;
    }
    
    private List<Node> getItemsInBrief(Node brief, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<>();
        try {
            Node damNode = resourceResolver.getResource(brief.getNode("jcr:content").getProperty("damFolderPath").getString()).adaptTo(Node.class);
            NodeIterator it = damNode.getNodes();
            while (it.hasNext()) {
                Node productNode = it.nextNode();
                if (productNode.hasProperty("stockNumber")
                      || productNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)
                        ) {
                    results.add(productNode);
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return results;
    }

    
    
    private static class BriefSummary {        
        private String countAssets;       
        public String getCountAssets() {
			return countAssets;
		}

		public void setCountAssets(String countAssets) {
			this.countAssets = countAssets;
		}
        
        
    }
}
