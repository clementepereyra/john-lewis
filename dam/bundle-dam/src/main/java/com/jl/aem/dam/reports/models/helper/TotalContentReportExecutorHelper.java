package com.jl.aem.dam.reports.models.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.reports.api.ResultsPage;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.reports.models.JLContentReportExecutor;
import com.jl.aem.dam.reports.models.results.JLContentItem;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

public class TotalContentReportExecutorHelper implements JLReportExecutorHelper {
    
private static final Logger log = LoggerFactory.getLogger(TotalContentReportExecutorHelper.class);
    
    private Map<String, String> parameters  = new HashMap<String, String>();
    
    private static final String START_DATE_LOWER_BOUND_PARAM = "startDateLowerBound";
    private static final String START_DATE_UPPER_BOUND_PARAM = "startDateUpperBound";
    private static final String DUE_DATE_LOWER_BOUND_PARAM = "dueDateLowerBound";
    private static final String DUE_DATE_UPPER_BOUND_PARAM = "dueDateUpperBound";
    
    @Override
    public ResultsPage fetchResults(SlingHttpServletRequest request) {
        List<Object> results = new ArrayList<>();
        try {
            prepareParameters(request);
            
            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            Map<String, List<Node>> briefs = new HashMap<>();
            briefs.put("cutout", new ArrayList<>());
            briefs.put("lifestyle", new ArrayList<>());
            briefs.put("retouch", new ArrayList<>());
            
            StringBuilder queryString = new StringBuilder("SELECT s.* "
                    + " FROM [nt:unstructured] AS s "
                    + " WHERE ISDESCENDANTNODE(s, [/content/projects]) "
                    + " AND s.[sling:resourceType]='cq/gui/components/projects/admin/card/projectcard'"
                    + " AND s.[jcr:content/JLPStatus] = 'COMPLETE'"
                    ); 
            if (parameters.get(START_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(START_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(START_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(START_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]<=" + upperBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(DUE_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(DUE_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]<=" + upperBound.getTime());
                }
            }
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery(queryString.toString(), javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            while (iterator.hasNext()) {
                Node brief = iterator.nextNode();
                String type = getBriefType(brief);
                if (type != null) {
                    briefs.get(type).add(brief);
                    
                }
            }
            
    
            
            JLContentItem item = new JLContentItem();
            item.setCutout(briefs.get("cutout").size());
            item.setItemType("Briefs");
            item.setLifestyle(briefs.get("lifestyle").size());
            item.setRetouch(briefs.get("retouch").size());
            item.setTotal(item.getCutout()+item.getLifestyle()+item.getRetouch());
            results.add(item);
            
            List<Node> cutoutProducts = getItemsInBriefs(briefs.get("cutout"), resourceResolver);
            List<Node> lifestyleProducts = getItemsInBriefs(briefs.get("lifestyle"), resourceResolver);
            List<Node> retouchProducts = getItemsInBriefs(briefs.get("retouch"), resourceResolver);

            item = new JLContentItem();
            item.setCutout(cutoutProducts.size());
            item.setItemType("Products");
            item.setLifestyle(lifestyleProducts.size());
            item.setRetouch(retouchProducts.size());
            item.setTotal(item.getCutout()+item.getLifestyle()+item.getRetouch());
            results.add(item);
            
            List<Node> cutoutAssets = getAssetsInProductOrLook(cutoutProducts, resourceResolver);
            List<Node> lifestyleAssets = getAssetsInProductOrLook(lifestyleProducts, resourceResolver);
            List<Node> retouchAssets = getAssetsInProductOrLook(retouchProducts, resourceResolver);

            
            item = new JLContentItem();
            item.setCutout(cutoutAssets.size());
            item.setItemType("Assets");
            item.setLifestyle(lifestyleAssets.size());
            item.setRetouch(retouchAssets.size());
            item.setTotal(item.getCutout()+item.getLifestyle()+item.getRetouch());
            results.add(item);
            
            List<Node> cutoutOverdue = getOverdueBriefs(briefs.get("cutout"), resourceResolver);
            List<Node> lifestyleOverdue = getOverdueBriefs(briefs.get("lifestyle"), resourceResolver);
            List<Node> retouchOverdue = getOverdueBriefs(briefs.get("retouch"), resourceResolver);
            
            item = new JLContentItem();
            item.setCutout(cutoutOverdue.size());
            item.setItemType("Overdue");
            item.setLifestyle(lifestyleOverdue.size());
            item.setRetouch(retouchOverdue.size());
            item.setTotal(item.getCutout()+item.getLifestyle()+item.getRetouch());
            results.add(item);
        } catch (Exception e) {
            log.error("Can't create inflight report", e);
        }
        ResultsPage page = new ResultsPage(results, results.size(), 0);
        return page;

    }

    private final static SimpleDateFormat sdfCompletedOn = new SimpleDateFormat("MM/dd/yyyy");

    private List<Node> getOverdueBriefs(List<Node> list, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<Node>();
        try {
            for (Node briefNode : list) {
               Node jcrContentNode = briefNode.getNode("jcr:content");
               if (jcrContentNode.hasProperty("JLPCompletedOn")) {
                   String dateString = jcrContentNode.getProperty("JLPCompletedOn").getString();
                   Date completedOn = sdfCompletedOn.parse(dateString);
                   Date deliveryDate = jcrContentNode.getProperty("project.dueDate").getDate().getTime();
                   if (completedOn.getTime()>deliveryDate.getTime()) {
                       results.add(briefNode);
                   }
               }
            }
        } catch (Exception e) {
            log.error("Can't retrieve overdue reports", e);
        }
        return results;
    }

    @Override
    public String getDetails() {
        return "<dl>" + "Total Content Report Executor" + "</dl>";
    }

    @Override
    public String getParameters() {
        return "";
    }

    
    private void prepareParameters(SlingHttpServletRequest request) {
        parameters = new HashMap<String, String>();
        @SuppressWarnings("unchecked")
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
          String key = paramNames.nextElement();
          parameters.put(key, StringEscapeUtils.escapeSql(request.getParameter(key)));
        }
    }
    
    
    
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Date parseDate(String stringDate) {
        Date date = null;
        String sanitized = stringDate;
        if (stringDate.length() >= 10) {
            sanitized = stringDate.substring(0, 10);
            try { 
                date = sdf.parse(sanitized);
            } catch (Exception e) {
                log.error("Error parsing date", e);
            }
        }
        return date;
    }
    

    private List<Node> getAssetsInProductOrLook(List<Node> nodes,
            ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<Node>();
        for (Node itemNode : nodes) {
            results.addAll(getAssetInProductOrLook(itemNode, resourceResolver));
        }
        return results;
    }

    private List<Node> getItemsInBriefs(List<Node> list, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<>();
        try {
            for (Node n : list) {
                List<Node> itemsInBrief = getItemsInBrief(n, resourceResolver);
                for (Node item : itemsInBrief) {
                    if (!isProductNodeInList(item, results)) {
                        results.add(item);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can't add product to report results", e);
        }
        return results;
    }

    private String getBriefType(Node brief) {
        try {
            Node projectContentNode = brief.getNode("jcr:content");
            if (BriefUtils.isCutoutBrief(projectContentNode)) {
                return "cutout";
            } else if (BriefUtils.isLifestyleBrief(projectContentNode)) {
                return "lifestyle";
            } else if (BriefUtils.isRetouchBrief(projectContentNode)) {
                return "retouch";
            }
        } catch (Exception e) {
            log.error("Can't get brief type for report", e);
        }
        return null;
    }
    
    private List<Node> getItemsInBrief(Node brief, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<>();
        try {
            Node damNode = resourceResolver.getResource(brief.getNode("jcr:content").getProperty("damFolderPath").getString()).adaptTo(Node.class);
            NodeIterator it = damNode.getNodes();
            while (it.hasNext()) {
                Node productNode = it.nextNode();
                if ((productNode.hasProperty("stockNumber")
                      || productNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION))
                        && !productNode.hasProperty("dam:JLPDelivered")) {
                    results.add(productNode);
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return results;
    }

    private boolean isProductNodeInList(Node productNode, List<Node> list) throws RepositoryException {
        for (Node n : list) {
            if (n.getName().equals(productNode.getName())) {
                return true;
            }
        }
        return false;
    }
    
    private List<Node> getAssetInProductOrLook(Node itemNode, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<>();
        try {
            NodeIterator it = itemNode.getNodes();
            while (it.hasNext()) {
                Node assetNode = it.nextNode();
                if (assetNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                    results.add(assetNode);
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return results;
    }
}
