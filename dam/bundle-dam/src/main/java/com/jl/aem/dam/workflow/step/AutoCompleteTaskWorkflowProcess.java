package com.jl.aem.dam.workflow.step;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.exec.Route;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;

/**
 * 
 * This process autocomplete the single asset workflow. 
 * Since it is implemented using goto steps, always has only one route, the one that point to "step to go to".
 * 
 * @author hgercek
 *
 */
@Component(immediate = true, service = WorkflowProcess.class,
        property = {"process.label=Auto Complete Task Workflow Process"})
public class AutoCompleteTaskWorkflowProcess extends AbstractDAMWorkflowProcess {


    private static final Logger log =
            LoggerFactory.getLogger(AutoCompleteTaskWorkflowProcess.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private JLPWorkflowSettings settings;

    private static final String PROVIDE_SHOOT_PATH = "/jcr:content/work/provide-shoot-";

    @Override
    public void execute(WorkItem item, WorkflowSession wfSession, MetaDataMap args)
            throws WorkflowException {
        try {
            log.debug("Auto Complete Task Workflow Process Running");

            Node assetNode = getAssetNodeFromPayload(item, wfSession.getSession());

            Session session = wfSession.getSession();
            ResourceResolver resourceResolver = getResourceResolver(session);
            String projectPath = BriefUtils.getProjectPath(assetNode);
            Node projectContentNode = resourceResolver.getResource(projectPath).adaptTo(Node.class).getNode("jcr:content");
            String targetFolder =
                    item.getWorkflowData().getMetaDataMap().get("targetFolder").toString();
            String targetFileName =
                    item.getWorkflowData().getMetaDataMap().get("targetFileName").toString();
            String targetFullPath =
                    targetFolder + (targetFolder.endsWith("/") ? "" : "/") + targetFileName;
            BriefAssetStatus currentPlaceHolderStatus = BriefAssetStatus.valueOf(assetNode.getParent().getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
            if ((resourceResolver.getResource(targetFullPath) == null &&
                   ( currentPlaceHolderStatus == BriefAssetStatus.ASSET_REQUESTED || currentPlaceHolderStatus == BriefAssetStatus.RE_SHOOT_NEEDED))
                    || (targetFileName.equals(assetNode.getName()) && !BriefUtils.isLifestyleBrief(projectContentNode))) {
                autoCompleteStep(assetNode, resourceResolver);
            } else if ((resourceResolver.getResource(targetFullPath) == null && currentPlaceHolderStatus != BriefAssetStatus.ASSET_REQUESTED)
                    || targetFileName.equals(assetNode.getName()) && BriefUtils.isLifestyleBrief(projectContentNode)) {
                assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS,currentPlaceHolderStatus.toString());
                resourceResolver.commit();
            }

        } catch (Exception e) {
            log.error("Error auto completing task for asset "
                    + item.getWorkflowData().getPayload().toString(), e);
        }
    }

    @Override
    protected ResourceResolverFactory getResolverFactory() {
        return resolverFactory;
    }

    private void autoCompleteStep(final Node assetNode, ResourceResolver resourceResolver) {
        try {
            log.debug("Autocompleating workflow for asset {}", assetNode.getPath());
            com.adobe.granite.workflow.WorkflowSession graniteWfSession =
                    resourceResolver.adaptTo(com.adobe.granite.workflow.WorkflowSession.class);

            String assetFolderName = null;
            String productCode = null;
            String eanCode = null;


            BriefAssetStatus assetStatus = null;            
            boolean jlRetoucher = false;

            Node assetFolderNode = assetNode.getParent();
            if (assetFolderNode != null) {
                assetFolderName = assetFolderNode.getName();
                Node productNode = assetFolderNode.getParent();
                if (productNode != null) {
                    productCode = productNode.getName();
                    eanCode = productNode.hasProperty("ean")?productNode.getProperty("ean").getString():"-";
                }
                if (assetFolderNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                    assetStatus = BriefAssetStatus.valueOf(assetFolderNode
                            .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                }
                
                if (assetFolderNode.hasProperty(JLPConstants.JL_RETOUCHER)) {
                    jlRetoucher = true;
                }

            }

            if (assetStatus != null && ((assetStatus == BriefAssetStatus.RE_SHOOT_NEEDED
                    && settings.getEnableAutoCompletePhotographersTask())
                    || (assetStatus == BriefAssetStatus.ASSET_REQUESTED
                            && settings.getEnableAutoCompletePhotographersTask())
                    || (assetStatus == BriefAssetStatus.ASSET_UPLOADED && jlRetoucher
                            && settings.getEnableAutoCompleteReTouchersTask()))) {

                String projectPath = BriefUtils.getProjectPath(assetNode);
                String projectWorkPath = null;

                Node projectWorkNode = null;
                if (productCode != null && assetFolderName != null) {
                    projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                            .append(productCode).append("-").append(assetFolderName).toString();
                    Resource projectWorkResurce = resourceResolver.getResource(projectWorkPath);

                    if (projectWorkResurce != null) {
                        projectWorkNode = projectWorkResurce.adaptTo(Node.class);
                    } else {
                        projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                                .append(eanCode).append("-").append(assetFolderName).toString();
                        projectWorkResurce = resourceResolver.getResource(projectWorkPath);
                        if (projectWorkResurce != null) {
                            projectWorkNode = projectWorkResurce.adaptTo(Node.class);
                        }
                    }
                }

                if (graniteWfSession != null && projectWorkNode != null) {
                    String workflowId = null;
                    if (projectWorkNode.hasProperty("workflow.id")) {
                        workflowId = projectWorkNode.getProperty("workflow.id").getString();
                    }

                    if (workflowId != null) {
                        Node workItemNode = resourceResolver.getResource(workflowId + "/workItems")
                                .adaptTo(Node.class);
                        NodeIterator workItemChildIterator = workItemNode.getNodes();
                        if (workItemChildIterator.hasNext()) {
                            Node workItemInstanceNode = (Node) workItemChildIterator.next();
                            com.adobe.granite.workflow.exec.WorkItem workItem =
                                    graniteWfSession.getWorkItem(workItemInstanceNode.getPath());

                            Node metaDataWorkItemNode = resourceResolver
                                    .getResource(workItemInstanceNode.getPath() + "/metaData")
                                    .adaptTo(Node.class);

                            String taskId = null;
                            if (metaDataWorkItemNode.hasProperty("taskId")) {
                                log.debug("Set as compleate taskId {} ", taskId);
                                taskId = metaDataWorkItemNode.getProperty("taskId").getString();
                                Node taskNode =
                                        resourceResolver.getResource(taskId).adaptTo(Node.class);
                                taskNode.setProperty("status", "COMPLETE");
                                resourceResolver.commit();
                            }

                            log.debug("workItem id = ", workItem.getId());
                            List<Route> routes = graniteWfSession.getRoutes(workItem, false);
                            log.debug("Number of routes for the workItem = {}", routes.size());
                            // It is a linear workflow model.
                            Route route = routes.get(0);
                            if (route != null) {
                                log.debug("routing: {}",route.getName());
                                graniteWfSession.complete(workItem, routes.get(0));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error trying to programatically advance on jl-cutout-singleassetworkflow {}",
                    e);
        } catch (Throwable e) {
            log.error("Error trying to programatically advance on jl-cutout-singleassetworkflow {}",
                    e);
        }

    }

   

}
