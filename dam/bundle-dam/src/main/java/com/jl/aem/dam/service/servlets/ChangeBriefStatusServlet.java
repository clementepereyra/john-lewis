package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/changeProjectStatus",
        "sling.servlet.methods=POST"})
public class ChangeBriefStatusServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String projectPath = request.getParameter("project_path");
        String status = request.getParameter("status");
        String comments = request.getParameter("comments");
        String userID = request.getResourceResolver().adaptTo(Session.class).getUserID();
        
        String result = "ERROR";
        if (briefService.isAValidForceCompleteState(projectPath)) {
            result = briefService.forceCompleteProject(projectPath, status, comments, userID);
        }
        
        response.getWriter().write("{\"result\":\"" + result+ "\"}");
        response.setContentType("application/json");
    }
    

}
