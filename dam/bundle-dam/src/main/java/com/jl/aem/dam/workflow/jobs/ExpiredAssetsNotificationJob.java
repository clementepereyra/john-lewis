package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.ExpiredAssetNotificationService;

/**
 * Schedulable job to run the report of expired assets.
 * 
 * @author fersoube
 *
 */
@Component(immediate = true, name = "Expired Assets Notification Job",service= {Runnable.class},
property ={"scheduler.expression=0 0 9 * * ? "})
public class ExpiredAssetsNotificationJob implements Runnable {

	private static final Logger logger = LoggerFactory
			.getLogger(ExpiredAssetsNotificationJob.class);

	@Reference
	private ExpiredAssetNotificationService expiredAssetsService;

	@Reference
	private SlingSettingsService slingSettingsService;
	

	public void run() {
		boolean isAuthorInstance = slingSettingsService.getRunModes()
				.contains("author");
		if (isAuthorInstance) {
			logger.trace("Running Expired Asset Notification job");
			expiredAssetsService.execute();
			logger.trace("Unchanged Expired Notification job completed");
		}
	}
}
