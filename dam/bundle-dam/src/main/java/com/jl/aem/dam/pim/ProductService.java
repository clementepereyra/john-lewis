package com.jl.aem.dam.pim;

import java.util.Date;
import java.util.List;

import javax.jcr.Node;


public interface ProductService {

     void addProduct(final Product product);
     void updateProduct(final Product product);
     void deleteProduct(final Product product);
     List<CompletedImage> getProductCompletedImages(final Date date);
     boolean exist(String prodCode);
     String importProducts(String path);
     String getAssetPathByProduct(Product product);
     public void sentProductEmptyFieldsEmail(String prodCode,String fieldName);
     Product getProduct(String prodCode);
}
