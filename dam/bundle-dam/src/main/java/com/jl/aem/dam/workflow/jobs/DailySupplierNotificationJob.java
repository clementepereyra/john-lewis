package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.DailySupplierNotificationService;

/**
 * Schedulable job to sent a daily notification to suppliers that have to upload images
 * 
 * @author Bruno Szumpich
 *
 */
@Component(immediate = true, name = "JLP Daily supliers notification job",service= {Runnable.class},
    property ={"scheduler.expression=0 0 7 * * ? "})
public class DailySupplierNotificationJob implements Runnable {

    private static final Logger logger = LoggerFactory
            .getLogger(DailySupplierNotificationJob.class);

    @Reference
    private DailySupplierNotificationService service;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance) {
            logger.trace("Running suppliers daily notification job");
            service.execute();
            logger.trace("Asset status daily notification completed");
        }
    }
}