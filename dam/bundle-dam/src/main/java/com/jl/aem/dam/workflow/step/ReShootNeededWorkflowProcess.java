package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Process ReShoot needed on Cutout Brief"})
public class ReShootNeededWorkflowProcess extends AbstractProjectWorkflowProcess implements WorkflowProcess {
    
    
    private static final Logger log = LoggerFactory.getLogger(SetupAssetShootsWorkflowProcess.class);
    
    @Reference 
    ResourceResolverFactory resolverFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap metadata)
            throws WorkflowException {
        try { 
            Node project = getProjectFromPayload(item, session.getSession(), resolverFactory);
            String workflowTitle = item.getWorkflow().getWorkflowData().getMetaDataMap().get("workflowTitle", String.class);
            String productCode = workflowTitle.split("-")[0];
            String altCode = workflowTitle.split("-")[1];
            briefsService.setReshootNeeded(project.getPath(), productCode, altCode);
        } catch (Exception e) {
            log.error("Can not set alt no needed", e);
        }
    }
    

}
