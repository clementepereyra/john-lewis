package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.version.VersionManager;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.jl.aem.dam.briefs.service.impl.BriefsServiceImpl;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/closeRetouch.json",
        "sling.servlet.methods=POST"})
public class CloseRetouchServlet  extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    private static final String TEMP_PATH = "/content/dam/projects/temp/retouch";
    
    private static final Logger logger = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        for(String path : items) {
            closeRetouch(path, resourceResolver);
        }
        response.getWriter().write("{\"result\":\"OK\"}");
        response.setContentType("application/json");
    }

    private void closeRetouch(String path, ResourceResolver resourceResolver) {
        try {
            String targetPath = path.replace(TEMP_PATH, JLPConstants.JOHNLEWIS_SCENE7_FOLDER);
         //   String targetFolder = targetPath.substring(0, targetPath.lastIndexOf("/"));
         //   String targetFileName = targetPath.substring(targetPath.lastIndexOf("/") + 1);
            Session session = resourceResolver.adaptTo(Session.class);


            Node targetNode = session.getNode(targetPath);
            Node targetJCRContent = targetNode.getNode("jcr:content/renditions");
            targetJCRContent.remove();

            Node sourceNode = resourceResolver.getResource(path).adaptTo(Node.class);
            session.move(sourceNode.getPath() + "/jcr:content/renditions",
                    targetNode.getPath() + "/jcr:content/renditions");
            session.save();
            if (targetNode.canAddMixin("mix:versionable")) {
                targetNode.addMixin("mix:versionable");
                session.save();
            }
            VersionManager versionManager = session.getWorkspace().getVersionManager();
            versionManager.checkpoint(targetPath);
            sourceNode.remove();
            session.save();
            
            Asset finalAsset = resourceResolver.getResource(targetPath).adaptTo(Asset.class);
            
            Rendition flatten = finalAsset.getRendition(JLPConstants.RENDITION_FLATTEN);
            Rendition layered = finalAsset.getOriginal();
            finalAsset.addRendition(JLPConstants.RENDITION_LAYERED, layered.getStream(), layered.getMimeType());
            finalAsset.addRendition(JLPConstants.RENDITION_ORIGINAL, flatten.getStream(), flatten.getMimeType());
            resourceResolver.commit();
            
            Map<String, Object> metaData = new HashMap<>();
            workflowInvoker.startWorkflow(JLPConstants.SCENE7_WORKFLOW, targetPath, metaData);
            
        } catch (Exception e) {
            logger.error("Can not reopen asset: " + path, e);
        }
    }
    
    
}
