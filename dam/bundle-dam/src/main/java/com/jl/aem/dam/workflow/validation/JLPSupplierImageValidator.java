package com.jl.aem.dam.workflow.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.step.helper.ProductAssociatorHelper;

@Component(immediate=true, service = JLPSupplierImageValidator.class)
@Designate(ocd = JLPSupplierImageValidatorConfiguration.class)
public class JLPSupplierImageValidator extends JLPCommonValidator  implements JLPAssetValidator{

    private static final Logger logger = LoggerFactory.getLogger(JLPSupplierImageValidator.class);
    
    private int minSize;
    private int maxSize;
    private List<String> extensions;

    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    
    
    @Override
    public boolean canValidate(String assetPath) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                if (!JLWorkflowsUtils.isImage(assetNode)) {
                    return false;
                }
                Session session = resourceResolver.adaptTo(Session.class);
                Map<String, String>  productInfo = ProductAssociatorHelper.getProductFromAsset(assetNode, resourceResolver, session);
                String productPath = productInfo.get("productPath");
                if (productPath != null) {
                   return true;
                }
            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return false;
    }
    
    @Override
    public String validateAsset(String assetPath) {
        logger.info("JLPSupplierImageValidator executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user", e);
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                long assetHeight = getAssetHeight(metadataNode);
                long assetWidth = getAssetWidth(metadataNode);

                if (assetHeight > maxSize || assetWidth > maxSize) {
                    return "ERROR: Asset is larger than " + maxSize + " px";
                }
                if (assetHeight < minSize || assetWidth < minSize) {
                    return "ERROR: Asset size is below " + minSize + " px";
                }
                String assetExtension = getAssetExtension(assetNode);
                if (!extensions.contains(assetExtension)) {
                    return "ERROR: Asset extension is not allowed";
                }
            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return JLPAssetValidator.SUCCESS_RESPONSE;
    }
    
    
    @Activate
    protected void activate(final JLPSupplierImageValidatorConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPSupplierImageValidatorConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPSupplierImageValidatorConfiguration config) {
    }

    private synchronized void resetService(final JLPSupplierImageValidatorConfiguration config) {
        maxSize = config.getMaxSize();
        minSize = config.getMinSize();
        extensions = Arrays.asList(config.getExtensions());
    }
}
