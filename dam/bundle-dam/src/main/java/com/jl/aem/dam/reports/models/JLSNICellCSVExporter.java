package com.jl.aem.dam.reports.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.adobe.acs.commons.reports.api.ReportCellCSVExporter;
import com.jl.aem.dam.reports.models.results.JLSNIItem;


@Model(adaptables = Resource.class)
public class JLSNICellCSVExporter implements ReportCellCSVExporter {

    @Inject
    private String property;

    @Inject
    @Optional
    private String format;

    @Override
    public String getValue(Object result) {
      JLSNIItem resource = (JLSNIItem) result;
      JLSNICellValue val = new JLSNICellValue(resource, property);
      return val.getValue();
    }
}
