package com.jl.aem.dam.briefs.workflow.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowService;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefChangeProcessor;
import com.jl.aem.dam.briefs.workflow.ProjectWorkflowInfo;
import com.jl.aem.dam.workflow.JLPConstants;
@Component(service=BriefChangeProcessor.class, immediate=true)
public class BriefChangeProcessorImpl implements BriefChangeProcessor {
    
    private static final Logger log = LoggerFactory.getLogger(BriefChangeProcessorImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;
    @Reference
    private WorkflowService workflowService;
    
    @Override
    public void process(String projectContentPath, String userId) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource contentResource = resourceResolver.getResource(projectContentPath);
            Node contentNode = contentResource.adaptTo(Node.class);
            Boolean started = false;
            if (contentNode.hasProperty("startNotified")) {
                started = contentNode.getProperty("startNotified").getBoolean();
            }
            if (contentNode.hasProperty("project.dueDate")) {
                Date deliveryDateSet = contentNode.getProperty("project.dueDate").getDate().getTime();
                if (!started && deliveryDateSet != null && !BriefUtils.isRetouchBrief(contentNode)) {
                    ProjectWorkflowInfo workflowInfo = getProjectWorkflowInfo(userId, contentNode);
                    ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService, resourceResolver);
                    contentNode.setProperty("startNotified", true);
                    resourceResolver.commit();
                }
            }
        } catch (Exception e) {
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private ProjectWorkflowInfo getProjectWorkflowInfo(String userId, Node contentNode) throws RepositoryException {
        boolean isCutoutWorkflow = BriefUtils.isCutoutBrief(contentNode);
        boolean isLifestyleWorkflow = BriefUtils.isLifestyleBrief(contentNode);
        if (isCutoutWorkflow) {
            String workflowTitle = "CutOut Brief Workflow";
            String workflowNodeName = "cutout-brief-workflow";
            String workflowModel = JLPConstants.CUTOUT_WORKFLOW_ID;
            ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName, workflowModel, contentNode, userId);
            return workflowInfo;
        } else if (isLifestyleWorkflow) {
            String workflowTitle = "Lifestyle Brief Workflow";
            String workflowNodeName = "lifestyle-brief-workflow";
            String workflowModel = JLPConstants.LIFESTYLE_WORKFLOW_ID;
            ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName, workflowModel, contentNode, userId);
            return workflowInfo;
        } else {
            String workflowTitle = "Retouch Brief Workflow";
            String workflowNodeName = "retouch-brief-workflow";
            String workflowModel = JLPConstants.RETOUCH_WORKFLOW_ID;
            ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName, workflowModel, contentNode, userId);
            return workflowInfo;
        }
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
    
    
    
}
