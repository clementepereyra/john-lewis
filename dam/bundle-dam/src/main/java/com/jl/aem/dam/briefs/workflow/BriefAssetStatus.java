package com.jl.aem.dam.briefs.workflow;

public enum BriefAssetStatus {

    ASSET_REQUESTED ("Pending Shot"),
    MARK_UP_NEEDED ("Mark Up Needed"),
    ASSET_UPLOADED ("Pending Retouch"),
    ASSET_RETOUCHED ("Asset Retouched"),
    ALT_NOT_NEEDED ("ALT Not Needed"),
    RE_SHOOT_NEEDED ("Pending Re-Shoot"),
    READY_TO_RETOUCH ("Ready To Retouch"),
    ASSET_DELIVERED ("Asset Delivered"), 
    PENDING_COLOUR_MATCH("Pending Colour Match");
    private final String displayLabel;
    
    private BriefAssetStatus(final String theDisplayLabel) {
        this.displayLabel = theDisplayLabel;
    }
    
    public String getDisplayLabel() {
        return displayLabel;
    }
}
