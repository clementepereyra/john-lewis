package com.jl.aem.dam.reports.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.reports.api.ReportException;
import com.adobe.acs.commons.reports.api.ReportExecutor;
import com.adobe.acs.commons.reports.api.ResultsPage;
import com.jl.aem.dam.reports.models.helper.JLReportExecutorHelper;
import com.jl.aem.dam.reports.models.helper.JLReportExecutoryHelperFactory;

@Model(adaptables = SlingHttpServletRequest.class)
public class JLContentReportExecutor implements ReportExecutor {
    
    private SlingHttpServletRequest request;
    private JLReportConfig config;
    
    private JLReportExecutoryHelperFactory reportHelperFactory = new JLReportExecutoryHelperFactory();

    private static final Logger log = LoggerFactory.getLogger(JLContentReportExecutor.class);

    public JLContentReportExecutor(SlingHttpServletRequest request) {
        this.request = request;
    }

    @Override
    public ResultsPage getAllResults() throws ReportException {
        JLReportExecutorHelper helper = reportHelperFactory.getReportExecutorHelper(config.getReportType());
        return helper.fetchResults(request);
    }

    @Override
    public String getDetails() throws ReportException {
        JLReportExecutorHelper helper = reportHelperFactory.getReportExecutorHelper(config.getReportType());
        return helper.getDetails();
    }

    @Override
    public String getParameters() throws ReportException {
        JLReportExecutorHelper helper = reportHelperFactory.getReportExecutorHelper(config.getReportType());
        return helper.getParameters();
    }

    @Override
    public ResultsPage getResults() throws ReportException {
        return getAllResults();
    }

    @Override
    public void setConfiguration(Resource arg0) {
        this.config = arg0.adaptTo(JLReportConfig.class);
    }

    @Override
    public void setPage(int arg0) {
        // TODO Auto-generated method stub
        
    }
    
    
}
