package com.jl.aem.dam.briefs.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.AltTaskCompleterService;
import com.jl.aem.dam.briefs.service.ProductTaskCompleterService;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=ProductTaskCompleterService.class)
public class ProductTaskCompleterServiceImpl implements ProductTaskCompleterService {
    
    private static final Logger log = LoggerFactory.getLogger(ProductTaskCompleterServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private AltTaskCompleterService altCompleter;

    @Override
    public boolean canComplete(String resourcePath, AutocompleteStatus newStatus) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource productResource = resourceResolver.getResource(resourcePath);
            if (productResource != null) {
                Node productNode = productResource.adaptTo(Node.class);
                NodeIterator prodIterator = productNode.getNodes();
                while (prodIterator.hasNext()) {
                    Node altNode = prodIterator.nextNode();
                    if (altNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0
                            && 
                              ( altNode.getName().toLowerCase().startsWith("alt")
                                      || altNode.getName().toLowerCase().equals("main"))
                              && isAltNeeded(altNode)) {
                        if (!altCompleter.canComplete(altNode.getPath(), newStatus)) {
                            return false;
                        }
                    }
                }
                return true;                
            }
            
        } catch (Exception e) {
            log.error("Error checking alt current status", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return false;
    }
    
    @Override
    public Collection<? extends String> getAssetsToRetouch(String path,
            AutocompleteStatus newStatus) {
        ResourceResolver resourceResolver = getResourceResolver();
        List<String> results = new ArrayList<String>();
        try {
            Resource productResource = resourceResolver.getResource(path);
            if (productResource != null) {
                Node productNode = productResource.adaptTo(Node.class);
                NodeIterator prodIterator = productNode.getNodes();
                while (prodIterator.hasNext()) {
                    Node altNode = prodIterator.nextNode();
                    if (altNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0
                            && 
                              ( altNode.getName().toLowerCase().startsWith("alt")
                                      || altNode.getName().toLowerCase().equals("main"))
                              && isAltNeeded(altNode)) {
                        if (altCompleter.canComplete(altNode.getPath(), AutocompleteStatus.EXTERNAL_RETOUCH)) {
                            results.add(altNode.getPath());
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            log.error("Error checking alt current status", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return results;
    }

    private boolean isAltNeeded(Node altNode) throws Exception {
        if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
            return !BriefAssetStatus.ALT_NOT_NEEDED.equals(BriefAssetStatus.valueOf(altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString()));
        }
        return false;
    }

    @Override
    public void complete(String resourcePath, AutocompleteStatus newStatus, String comments) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource productResource = resourceResolver.getResource(resourcePath);
            if (productResource != null) {
                Node productNode = productResource.adaptTo(Node.class);
                NodeIterator prodIterator = productNode.getNodes();
                while (prodIterator.hasNext()) {
                    Node altNode = prodIterator.nextNode();
                    if (altNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0
                            && 
                              ( altNode.getName().toLowerCase().startsWith("alt")
                                      || altNode.getName().toLowerCase().equals("main"))) {
                        altCompleter.complete(altNode.getPath(), newStatus, comments);
                    }
                }
            }
            
        } catch (Exception e) {
            log.error("Error completing product", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }

}
