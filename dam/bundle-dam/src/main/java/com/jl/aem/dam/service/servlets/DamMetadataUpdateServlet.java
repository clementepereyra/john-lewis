package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/DamMetadataUpdateAsset",
    "sling.servlet.methods=POST"
  }
)
public class DamMetadataUpdateServlet extends SlingAllMethodsServlet {
    private static final Logger log = LoggerFactory.getLogger(DamMetadataUpdateServlet.class);

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private SlingRepository repository;


    public void bindRepository(SlingRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        try {
            // Get the submitted form data
            String assigned = request.getParameter("assigned");
            String comment = request.getParameter("comment");
            String metadataPropStatus = request.getParameter("metadata_prop_status");
            String metadataPropAssigned = request.getParameter("metadata_prop_assigned");
            String metadataPropComment = request.getParameter("metadata_prop_comment");
            
            String approved = request.getParameter("approved");
            String assetStatus = request.getParameter("status");


            RequestParameter assetPaths[] = request.getRequestParameters("path");
            List<Asset> assetList = new ArrayList<Asset>();

            if (assetPaths != null) {
                ResourceResolver resResolver = request.getResourceResolver();
                for (RequestParameter path : assetPaths) {
                    String filename = path.toString();
                    Resource resource = resResolver.getResource(filename);

                    if (null != resource) {
                        Asset asset = resource.adaptTo(Asset.class);
                        if (null != asset)
                            assetList.add(asset);
                    }
                }
                if (approved!=null){
                    smartUpdateMetadataProp(assetList, metadataPropStatus, approved, metadataPropAssigned, assigned, metadataPropComment, comment);
                } else if (assetStatus != null){
                    updateMetadataProp(assetList, metadataPropStatus, assetStatus, metadataPropAssigned, assigned, metadataPropComment, comment);

                }

            }

            // Return to caller
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void updateMetadataProp(List<Asset> assetList, String statusPropertyName, String statusPropertyValue,
            String assignedPropertyName, String assignedPropertyValue, String commentPropertyName, String commentPropertyValue) throws PersistenceException {

        // just change asset metadata
        for (Asset asset : assetList) {
            Resource metadataResource = asset.getChild("jcr:content/metadata");
            ModifiableValueMap map = metadataResource.adaptTo(ModifiableValueMap.class);

            if (assignedPropertyValue != null && !assignedPropertyValue.trim().isEmpty()
                    && assignedPropertyValue.trim().length() > 1)
                map.put(assignedPropertyName, assignedPropertyValue);

            if (statusPropertyValue != null && !statusPropertyValue.trim().isEmpty()
                    && statusPropertyValue.trim().length() > 1)
                map.put(statusPropertyName, statusPropertyValue);

            if (commentPropertyValue != null && !commentPropertyValue.trim().isEmpty()
                    && commentPropertyValue.trim().length() > 1)
                map.put(commentPropertyName, commentPropertyValue);

            asset.getResourceResolver().commit();
        }
    }
    
    private void smartUpdateMetadataProp(List<Asset> assetList, String statusPropertyName, String approved,
            String assignedPropertyName, String assignedPropertyValue, String commentPropertyName, String commentPropertyValue) throws PersistenceException {

        // just change asset metadata
        for (Asset asset : assetList) {
            Resource metadataResource = asset.getChild("jcr:content/metadata");
            ModifiableValueMap map = metadataResource.adaptTo(ModifiableValueMap.class);

            if (assignedPropertyValue != null && !assignedPropertyValue.trim().isEmpty()
                    && assignedPropertyValue.trim().length() > 1)
                map.put(assignedPropertyName, assignedPropertyValue);
            try {
                if (approved.trim().equalsIgnoreCase("true")) {
                    String prevStatus = map.get(statusPropertyName).toString();
                    String newStatus = getNextStatus(prevStatus, asset.getResourceResolver().getResource(asset.getPath()).adaptTo(Node.class));
                    map.put(statusPropertyName, newStatus);
                } else {
                    String prevStatus = map.get(statusPropertyName).toString();
                    String newStatus = getPrevStatus(prevStatus, asset.getResourceResolver().getResource(asset.getPath()).adaptTo(Node.class));
                    map.put(statusPropertyName, newStatus);
                }
            } catch (Exception e) {
                log.error("Error updating asset status", e);
            }

            if (commentPropertyValue != null && !commentPropertyValue.trim().isEmpty()
                    && commentPropertyValue.trim().length() > 1)
                map.put(commentPropertyName, commentPropertyValue);

            asset.getResourceResolver().commit();
        }
    }

    private String getPrevStatus(String prevStatus, Node assetNode) throws RepositoryException {
        AssetStatus status =  AssetStatus.valueOf(prevStatus);
        if (status == AssetStatus.ASSET_NEW) { return AssetStatus.ASSET_RE_REQUESTED.toString();}
        else if (status == AssetStatus.ASSET_PENDING_IC) { return AssetStatus.ASSET_RE_REQUESTED.toString();}
        else if (status == AssetStatus.ASSET_AVAILABLE_FOR_ARTWORKING) { return AssetStatus.ASSET_PENDING_IC.toString();}
        else if (status == AssetStatus.ASSET_PENDING_ARTWORKING) { return AssetStatus.ASSET_AVAILABLE_FOR_ARTWORKING.toString();}
        else if (status == AssetStatus.FINAL_ASSET_APPROVED) { return AssetStatus.FINAL_ASSET_REJECTED.toString();}
        else if (status == AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL) {
            if (JLWorkflowsUtils.isVideo(assetNode) || JLWorkflowsUtils.isManual(assetNode) || JLWorkflowsUtils.isLogo(assetNode)) {
                return AssetStatus.ASSET_PENDING_IC.toString();
            } else {
                return AssetStatus.FINAL_ASSET_REJECTED.toString();
            }
        }
        return prevStatus;
    }

    private String getNextStatus(String prevStatus, Node assetNode) throws RepositoryException {
        AssetStatus status =  AssetStatus.valueOf(prevStatus);
        if (status == AssetStatus.ASSET_NEW) { return AssetStatus.ASSET_PENDING_IC.toString();}
        else if (status == AssetStatus.ASSET_AVAILABLE_FOR_ARTWORKING) { return AssetStatus.ASSET_PENDING_ARTWORKING.toString();}
        else if (status == AssetStatus.ASSET_PENDING_ARTWORKING) { return AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL.toString();}
        else if (status == AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL) { return AssetStatus.FINAL_ASSET_APPROVED.toString();}
        else if (status == AssetStatus.FINAL_ASSET_APPROVED) { return AssetStatus.FINAL_ASSET_APPROVED.toString();}
        else if (status == AssetStatus.ASSET_PENDING_IC) {
            if (JLWorkflowsUtils.isVideo(assetNode) || JLWorkflowsUtils.isManual(assetNode) || JLWorkflowsUtils.isLogo(assetNode)) {
                return AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL.toString();
            } else {
                return AssetStatus.ASSET_AVAILABLE_FOR_ARTWORKING.toString();
            }
        }
        return prevStatus;
    }

}