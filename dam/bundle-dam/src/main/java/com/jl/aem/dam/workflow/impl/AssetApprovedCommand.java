package com.jl.aem.dam.workflow.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.jl.aem.dam.workflow.ProductStatus;

@Component(immediate=true, service=AssetApprovedCommand.class)
public class AssetApprovedCommand implements AssetStatusCommand {
    
    private static final Logger logger = LoggerFactory.getLogger(AssetApprovedCommand.class);

    @Reference
    private Replicator replicator;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    
    private static final String SCENE7_WORKFLOW = "/etc/workflow/models/scene7/jcr:content/model";
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Override
    public void execute(String assetPath, final String userID) {
 
        logger.info("AssetApprovedCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            String productPath = assetNode.getProperty("jcr:content/metadata/cq:productReference").getString();
            Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            if (productNode.hasProperty("wsmItemId") && productNode.getProperty("wsmItemId").getString().trim() != "") {
                logger.info("Asset replicate: " + assetPath);
                Calendar cal = Calendar.getInstance();
                String month = new SimpleDateFormat("MMM").format(cal.getTime());
                String year = new SimpleDateFormat("YYYY").format(cal.getTime());
                String targetPath = JLPConstants.JOHNLEWIS_SCENE7_FOLDER + "/" +
                        year + "/" + month + "/" +
                        productNode.getProperty(ProductConstants.DIRECTORATE).getString() + "/" +
                        productNode.getProperty(ProductConstants.BUYING_OFFICE).getString();
                
                if (JLWorkflowsUtils.isMainIMage(assetNode)) {
                    productNode.setProperty("statusId", ProductStatus.IMAGE_COMPLETED.toString());
                    session.save();
                }

                
                Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                
                UserManager userMgr = resourceResolver.adaptTo(UserManager.class);
                Authorizable auth = userMgr.getAuthorizable(userID);
                javax.jcr.Value[] values = auth.getProperty("./profile/mainframe_id");
                String user_id = userID;
                if (values != null && values[0] != null && values[0].getString().trim() != "") {
                    user_id = values[0].getString();
                }
                
                metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_TIME, Calendar.getInstance());
                metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_USER, user_id);
                
                resourceResolver.commit();
                
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("targetFolder", targetPath );
                metaData.put("targetFileName", assetNode.getName());
                // we tell to publish to scene7 once moving is done
                metaData.put("nextWorkflow", SCENE7_WORKFLOW);
                workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);

            } else if (JLWorkflowsUtils.isLogo(assetNode)){
                String targetFolder = JLPConstants.JOHNLEWIS_SCENE7_FOLDER + "/Logos";
                String targetFileName = assetNode.getName(); 
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("targetFolder", targetFolder );
                metaData.put("targetFileName", targetFileName);
                // we tell to publish to scene7 once moving is done
                metaData.put("nextWorkflow", SCENE7_WORKFLOW);
                workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
            }
           
            
        } catch (Exception e){
            logger.error("Can not publish new asset: " + assetPath, e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

}
