package com.jl.aem.dam.briefs.service;

import java.util.List;

public class CutoutBrief {
    
    private List<CutoutBriefRow> rows;
    private boolean valid = true;
    
    public static class CutoutBriefRow { 
        private String stockNumber;
        private String styleGuideProductType;
        private String imageRequirements;
        private String clientStylingNotes;
        private String category;
        private String range;
        private String IDS;
        public String getStockNumber() {
            return stockNumber;
        }
        public void setStockNumber(String stockNumber) {
            this.stockNumber = stockNumber;
        }
        public String getStyleGuideProductType() {
            return styleGuideProductType;
        }
        public void setStyleGuideProductType(String styleGuideProductType) {
            this.styleGuideProductType = styleGuideProductType;
        }
        public String getImageRequirements() {
            return imageRequirements;
        }
        public void setImageRequirements(String imageRequirements) {
            this.imageRequirements = imageRequirements;
        }
        public String getClientStylingNotes() {
            return clientStylingNotes;
        }
        public void setClientStylingNotes(String clientStylingNotes) {
            this.clientStylingNotes = clientStylingNotes;
        }
        public String getCategory() {
            return category;
        }
        public void setCategory(String category) {
            this.category = category;
        }
        public String getRange() {
            return range;
        }
        public void setRange(String range) {
            this.range = range;
        }
        public String getIDS() {
            return IDS;
        }
        public void setIDS(String iDS) {
            IDS = iDS;
        }
        
        
    }

    public void setNotValid() {
        valid = false;
    }
    
    public boolean isValid() {
        return valid;
    }

    public List<CutoutBriefRow> getRows() {
        return rows;
    }



    public void setRows(List<CutoutBriefRow> rows) {
        this.rows = rows;
    }
}

