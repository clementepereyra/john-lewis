package com.jl.aem.dam.workflow;

public interface NextToExpireNotificationService {
    void execute();
}
