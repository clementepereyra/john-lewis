package com.jl.aem.dam.workflow.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.step.helper.ProductAssociatorHelper;

@Component(immediate=true, service = JLPSquareValidator.class)
@Designate(ocd = JLPSquareValidatorConfiguration.class)
public class JLPSquareValidator extends JLPCommonValidator  implements JLPAssetValidator {

    private static final Logger logger = LoggerFactory.getLogger(JLPSquareValidator.class);
    
    private int minSize;
    private int maxSize;
    private int minBitsPerPixel;
    private List<String> extensions;
    private List<String> productCategories;
    private String productField;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    
    
    @Override
    public boolean canValidate(String assetPath) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                if (!JLWorkflowsUtils.isImage(assetNode)) {
                    return false;
                }
                Session session = resourceResolver.adaptTo(Session.class);
                Map<String, String>  productInfo = ProductAssociatorHelper.getProductFromAsset(assetNode, resourceResolver, session);
                String productPath = productInfo.get("productPath");
                if (productPath != null) {
                    Resource productResource = resourceResolver.getResource(productPath);
                    Node productNode = productResource.adaptTo(Node.class);
                    String prodCategory = productNode.getProperty(productField).getString().toLowerCase().trim();
                    if (productCategories.contains(prodCategory)) {
                        return true;
                    }
                }
            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return false;
    }
    
    @Override
    public String validateAsset(String assetPath) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user", e);
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                long assetHeight = getAssetHeight(metadataNode);
                long assetWidth = getAssetWidth(metadataNode);
                if (assetHeight != assetWidth) {
                    return "ERROR: Asset aspect ratio is not square";
                }
                if (assetHeight > maxSize) {
                    return "ERROR: Asset is larger than " + maxSize + " px";
                }
                if (assetHeight < minSize) {
                    return "ERROR: Asset height is below " + minSize + " px";
                }
                String assetExtension = getAssetExtension(assetNode);
                if (!extensions.contains(assetExtension)) {
                    return "ERROR: Asset extension is not allowed";
                }
                long bitsPerPixel = getBitsPerPixel(metadataNode);
                if (bitsPerPixel < minBitsPerPixel) {
                    return "ERROR: Bits per pixel is below " + minBitsPerPixel + " bits";
                }
            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return JLPAssetValidator.SUCCESS_RESPONSE;
    }

    private long getBitsPerPixel(Node metadataNode) {
        try {
            return metadataNode.getProperty("dam:Bitsperpixel").getLong();
        } catch (RepositoryException e) {
            throw new RuntimeException("Can not get bits", e);
        }
    }

    
    @Activate
    protected void activate(final JLPSquareValidatorConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPSquareValidatorConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPSquareValidatorConfiguration config) {
    }

    private synchronized void resetService(final JLPSquareValidatorConfiguration config) {
        maxSize = config.getMaxSize();
        minSize = config.getMinSize();
        minBitsPerPixel = config.getMinBitsPerPixel();
        productCategories =  Arrays.asList(config.getProductCategories());
        extensions = Arrays.asList(config.getExtensions());
        productField = config.getProductField();
    }
}
