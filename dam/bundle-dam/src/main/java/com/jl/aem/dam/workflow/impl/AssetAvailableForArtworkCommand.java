package com.jl.aem.dam.workflow.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGatewayService;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.service.integration.ApiConnector;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.WorkflowBusinessSettings;

@Component(immediate=true, service=AssetAvailableForArtworkCommand.class)
public class AssetAvailableForArtworkCommand implements AssetStatusCommand {

    private static final Logger logger = LoggerFactory.getLogger(AssetAvailableForArtworkCommand.class);


    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private ApiConnector apiConnector;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private JLPWorkflowSettings settings;
    
    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Reference
    private WorkflowBusinessSettings businessSettings;
    
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    
    @Override
    public void execute(String assetPath, final String userID) {
        logger.info("AssetReadyForArtworkCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            updateAssetStatus(assetNode, resourceResolver, "Image Available", userID);
            resourceResolver.commit();
            String targetFolder = JLPConstants.ARTWORKERS_AVAILABLE;
            String productPath = assetNode.getNode("jcr:content/metadata").getProperty("cq:productReference").getString();
            Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            Product product = JLWorkflowsUtils.populateProductFromNode(productNode);
            if (businessSettings.is34Ratio(product)) {
                targetFolder += "/3_4";
            } else {
                targetFolder += "/square";
            }
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder );
            metaData.put("targetFileName", assetNode.getName());
            
            workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
        } catch (Exception e) {
            logger.error("Can not move asset to Artworkers. Asset: " + assetPath, e);
        } finally{
            if(resourceResolver != null && resourceResolver.isLive()){
                resourceResolver.close();
             }
        }
    }


    private void updateAssetStatus(Node assetNode, ResourceResolver resourceResolver, String status, String userID) {
        try {
            if (assetNode.getNode("jcr:content/metadata").hasProperty("cq:productReference")) {
                String productPath = assetNode.getProperty("jcr:content/metadata/cq:productReference").getString();
                Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
                if(JLWorkflowsUtils.isMainIMage(assetNode)) {
                    productNode.setProperty("statusId", status);
                }
            }
            
            UserManager userMgr = resourceResolver.adaptTo(UserManager.class);
            Authorizable auth = userMgr.getAuthorizable(userID);
            javax.jcr.Value[] values = auth.getProperty("./profile/mainframe_id");
            String user_id = userID;
            if (values != null && values[0] != null && values[0].getString().trim() != "") {
                user_id = values[0].getString();
            }
            
            Node metadata = assetNode.getNode("jcr:content/metadata");
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_TIME, Calendar.getInstance());
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_USER, user_id);
            if(assetNode.getPath().contains(JLPConstants.IC_REVIEW_PATH)){
                metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_IC_APPROVED_TIME, Calendar.getInstance());
            }
        } catch (Exception e) {
            logger.error("Can not update product status",e);
        }
        
    }
    
    
}
