package com.jl.aem.dam.archive.service.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Archiving Configuration", description = "Service Configuration")
public @interface JLPArchivingConfiguration {
    
    @AttributeDefinition(name ="Enable Archiving", description="Check to enable automatic archiving", defaultValue="false")
    boolean getArchiveEnabled() default false;
    
    @AttributeDefinition(name ="Path to JSON Key", description="Path to file withing crx repo containing GCloud json file, must inlude jcr:content at the end", defaultValue="/etc/jl-admin/gcloud/jl-dcm-archive-dev-664e9455da40.json/jcr:content", type = AttributeType.STRING)
    String getPathToJsonKey() default "/etc/jl-admin/gcloud/jl-dcm-archive-dev-664e9455da40.json/jcr:content" ;
    
    @AttributeDefinition(name ="GCloud Project ID", description="ProjectID to use in GCloud", defaultValue="jl-dcm-archive-dev", type = AttributeType.STRING)
    String getProjectID() default "jl-dcm-archive-dev" ;
    
    @AttributeDefinition(name ="GCloud Bucket Name", description="Name of bucket to use in GCloud", defaultValue="dcm-archive-dev", type = AttributeType.STRING)
    String getBucketName() default "dcm-archive-dev" ;
    
    @AttributeDefinition(name ="Layered Files Max Living time (In days)", description="Time in days before a layered file will be archived in GCloud", defaultValue="30", type = AttributeType.INTEGER)
    int getLayeredTiffLivingTimeInDays() default 30 ;
    
    @AttributeDefinition(name ="Flatten Files Max Living time (In days)", description="Time in days before a flatten file will be removed in AEM", defaultValue="30", type = AttributeType.INTEGER)
    int getFlattenTiffLivingTimeInDays() default 30;
    
    @AttributeDefinition(name ="Keep Asset Metadata And Thumbnails When Deleting Flatten", description="Check to allow keeping the assets metadata and thumbnails when deleting old flatten assets. Uncheck to completely delete the assets and all of its renditions.", defaultValue="true")
    boolean getKeepAssetMetadataAndThumbnails() default true;

    @AttributeDefinition(name ="Third Party Asset Max Living time (In hours)", description="Time in hours before a third party asset file will be removed in AEM", defaultValue="36", type = AttributeType.INTEGER)
    int getThirdPartyAssetLivingTimeInHours() default 36;
    
}
