package com.jl.aem.dam.archive.service.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.archive.service.ArchiveService;
import com.jl.aem.dam.archive.service.JLPArchivingSettings;

@Component(immediate = true, name = "Archive layered assets job",service= {Runnable.class},
    property ={"scheduler.expression=0 0 8 * * ? "})
public class ArchiveLayeredAssetsJob implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ArchiveLayeredAssetsJob.class);

    @Reference
    private ArchiveService service;
    
    @Reference
    private JLPArchivingSettings archivingSettings;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance && archivingSettings.getArchiveEnabled()) {
            logger.trace("Running archive layered assets job");
            service.archiveLayeredAssets();
            logger.trace("Archive layered assets job completed");
        }
    }
}