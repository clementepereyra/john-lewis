package com.jl.aem.dam.pim.commerce.integration;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.common.AbstractJcrProduct;

public class JLPProductImpl extends AbstractJcrProduct {

	private static final Logger logger = LoggerFactory
			.getLogger(JLPProductImpl.class);

	public static final String PN_IDENTIFIER = "identifier";
	public static final String PN_PRICE = "price";

	private String sku = "Unknown";

	public JLPProductImpl(final Resource resource) {
		super(resource);
		Node node = resource.adaptTo(Node.class);
		try {
			sku = node.getProperty(PN_IDENTIFIER).getString();
		} catch (Exception e) {
			logger.error("Can not create Product", e);
		}
	}

	public String getSKU() {
		return sku;
	}
}