package com.jl.aem.dam.workflow;

public interface DailySupplierNotificationService {

    void execute();
}