package com.jl.aem.dam.service.integration.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.service.integration.DashboardService;
import com.jl.aem.dam.service.integration.PieChartView;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;

@Component(immediate = true, service = DashboardService.class)
public class DashboardServiceImpl implements DashboardService {
    
    private static final Logger logger = LoggerFactory.getLogger(DashboardServiceImpl.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    @Reference
    private JLPWorkflowSettings settings;
    
    @Override
    public PieChartView getCurrentStatus() {
        ResourceResolver resourceResolver = getResourceResolver();
        Map<AssetStatus, Long> counters = new HashMap<AssetStatus, Long>();
        for (AssetStatus status : AssetStatus.values()) {
            if (status!= AssetStatus.FINAL_ASSET_APPROVED) {
                counters.put(status, 0L);
            }
        }
        if (resourceResolver != null ) {
            Session session = resourceResolver.adaptTo(Session.class);
            SearchResult result = searchCreativeAssets(resourceResolver, session);
            
            if (result.getTotalMatches() > 0) {
                 for (Hit hit : result.getHits()) {
                     try {
                         Node n = hit.getNode();
                         Node metadataNode = n.getNode("jcr:content/metadata");
                         String status = metadataNode.getProperty(JLPConstants.JLP_STATUS).getString();
                         counters.put(AssetStatus.valueOf(status), counters.get(AssetStatus.valueOf(status)) +1);
                     } catch (Exception e) {
                         logger.error("Error accessing hit result: " , e);
                     }
                 }
            }
            
        }
        List<String> x = new ArrayList<>();
        List<Long> y = new ArrayList<>();
        List<String> series = new ArrayList<>();
        for (Entry<AssetStatus, Long> entry : counters.entrySet()) {
            x.add(entry.getKey().getDisplayLabel());
            y.add(entry.getValue());
            series.add("Assets");
        }
        return new PieChartView(x, y, series);
    }
    
    @Override
    public PieChartView getBriefStatus(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);            
            String assetPath = getAssetPath(projectNode);
            Map<BriefAssetStatus, Long> counters = new HashMap<>();
            Resource projectContentrRes =
                    resourceResolver.getResource(projectPath).getChild(JcrConstants.JCR_CONTENT);
            if (projectContentrRes != null) {
                Node projectContentNode = projectContentrRes.adaptTo(Node.class);
                for (BriefAssetStatus status : BriefAssetStatus.values()) {
                    if (status != BriefAssetStatus.ALT_NOT_NEEDED 
                        && status != BriefAssetStatus.READY_TO_RETOUCH
                        && !(BriefUtils.isCutoutBrief(projectContentNode)
                                    && status == BriefAssetStatus.MARK_UP_NEEDED)
                        && !(BriefUtils.isRetouchBrief(projectContentNode)
                                    && (status == BriefAssetStatus.RE_SHOOT_NEEDED
                                       || status == BriefAssetStatus.MARK_UP_NEEDED
                                       || status == BriefAssetStatus.ASSET_UPLOADED))) {
                        counters.put(status, 0L);
                    }
                }
            }
            if (resourceResolver != null ) {
                Session session = resourceResolver.adaptTo(Session.class);
                SearchResult result = searchBriefAssets(assetPath, resourceResolver, session);
                
                if (result.getTotalMatches() > 0) {
                     for (Hit hit : result.getHits()) {
                         try {
                             Node n = hit.getNode();
                             String status = n.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString();
                             counters.put(BriefAssetStatus.valueOf(status), counters.get(BriefAssetStatus.valueOf(status)) +1);
                         } catch (Exception e) {
                             logger.error("Error accessing hit result: " , e);
                         }
                     }
                }
                
            }
            List<String> x = new ArrayList<>();
            List<Long> y = new ArrayList<>();
            List<String> series = new ArrayList<>();
            for (Entry<BriefAssetStatus, Long> entry : counters.entrySet()) {
                x.add(entry.getKey().getDisplayLabel());
                y.add(entry.getValue());
                series.add("Assets");
            }
            return new PieChartView(x, y, series);
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return null;
    }
    
    private SearchResult searchBriefAssets(String assetPath, ResourceResolver resourceResolver,
            Session session) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", assetPath);

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "sling:Folder");
 
        int i = 1;
        Predicate statusPredicate = new Predicate("status", "property");
        statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, JLPConstants.JLP_BRIEF_ASSET_STATUS);
        for (BriefAssetStatus status :  BriefAssetStatus.values()) {
            if (status != BriefAssetStatus.ALT_NOT_NEEDED) {
                statusPredicate.set( i + "_" + JcrPropertyPredicateEvaluator.VALUE, status.toString());
                i++;
            }
        }
        statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);
        predicates.add(statusPredicate);
       
        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.setAllRequired(true);
                
        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);
        
        return query.getResult();
    }

    private SearchResult searchCreativeAssets(ResourceResolver resourceResolver, Session session) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", settings.getObservedPath());

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "dam:Asset");
 
        int i = 1;
        Predicate statusPredicate = new Predicate("status", "property");
        statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_STATUS);
        for (AssetStatus status :  AssetStatus.values()) {
            if (status != AssetStatus.FINAL_ASSET_APPROVED) {
                statusPredicate.set( i + "_" + JcrPropertyPredicateEvaluator.VALUE, status.toString());
                i++;
            }
        }
        statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);
        predicates.add(statusPredicate);
       
        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.setAllRequired(true);
                
        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);
        
        return query.getResult();
    }
    
    
    @Override
    public long getAssetRequestedCount(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {        
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            String assetPath = getAssetPath(projectNode);
        
            PredicateGroup rootPredicate = new PredicateGroup();
    
            Predicate pathPredicate = new Predicate("path", "path");
            pathPredicate.set("path", assetPath);
        
            Predicate typePredicate = new Predicate("type", "type");
            typePredicate.set("type", "sling:Folder");
     
            int i = 1;
            Predicate statusPredicate = new Predicate("status", "property");
            statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, JLPConstants.JLP_BRIEF_ASSET_STATUS);
            for (BriefAssetStatus status :  BriefAssetStatus.values()) {
                if (status != BriefAssetStatus.ALT_NOT_NEEDED) {
                    statusPredicate.set( i + "_" + JcrPropertyPredicateEvaluator.VALUE, status.toString());
                    i++;
                }
            }
            
            rootPredicate.add(pathPredicate);
            rootPredicate.add(typePredicate);
            rootPredicate.add(statusPredicate);
    
            rootPredicate.setAllRequired(true);
        
            Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
            query.setHitsPerPage(1);
            SearchResult result = query.getResult();
            return result.getTotalMatches();    
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return 0;
    }
    
    @Override
    public long getAssetUploadedCount(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            String assetPath = getAssetPath(projectNode);
            PredicateGroup rootPredicate = new PredicateGroup();
    
            Predicate pathPredicate = new Predicate("path", "path");
            pathPredicate.set("path", assetPath);
        
            Predicate typePredicate = new Predicate("type", "type");
            typePredicate.set("type", "dam:Asset");
            
            Predicate statusPredicate = new Predicate("status", "property");
            statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_BRIEF_ASSET_STATUS);
            statusPredicate.set("1_" + JcrPropertyPredicateEvaluator.VALUE, BriefAssetStatus.ASSET_UPLOADED.toString());
            statusPredicate.set("2_" + JcrPropertyPredicateEvaluator.VALUE, BriefAssetStatus.ASSET_RETOUCHED.toString());
            statusPredicate.set("3_" + JcrPropertyPredicateEvaluator.VALUE, BriefAssetStatus.MARK_UP_NEEDED.toString());
            statusPredicate.set("4_" + JcrPropertyPredicateEvaluator.VALUE, BriefAssetStatus.ASSET_DELIVERED.toString());
            statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);
            
            rootPredicate.add(pathPredicate);
            rootPredicate.add(typePredicate);
            rootPredicate.add(statusPredicate);

            rootPredicate.setAllRequired(true);
        
            Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
            query.setHitsPerPage(1);
            SearchResult result = query.getResult();
            return result.getTotalMatches();
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return 0;
    }
    
    @Override
    public List<String> getProductsRequested(String projectPath) {
        List<String> results = new ArrayList<>();
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            String assetPath = getAssetPath(projectNode);
            PredicateGroup rootPredicate = new PredicateGroup();
    
            Predicate pathPredicate = new Predicate("path", "path");
            pathPredicate.set("path", assetPath);
        
            Predicate typePredicate = new Predicate("type", "type");
            typePredicate.set("type", "sling:Folder");
            
            Predicate prodPredicate = new Predicate("property","property");
            prodPredicate.set("property", "productPath");
            prodPredicate.set("operation", "exists");
            
            Predicate lookPredicate = new Predicate("property","property");
            lookPredicate.set("property", "dam:JLPProducts");
            lookPredicate.set("operation", "exists");
            
            PredicateGroup itemPredicate = new PredicateGroup();
            itemPredicate.setAllRequired(false);
            itemPredicate.add(prodPredicate);
            itemPredicate.add(lookPredicate);
            
            
            rootPredicate.add(pathPredicate);
            rootPredicate.add(typePredicate);
            rootPredicate.add(itemPredicate);
    
            rootPredicate.setAllRequired(true);
        
            Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
            query.setHitsPerPage(0);
            SearchResult result = query.getResult();
            List<Hit> hits = result.getHits();
            for(Hit hit : hits) {
                Node hitNode = hit.getNode();
                if (hitNode.hasProperty("productPath")) {
                    results.add(hitNode.getProperty("productPath").getString());
                } else if (hitNode.hasProperty("dam:JLPProducts")){
                    Value[] values = hitNode.getProperty("dam:JLPProducts").getValues();
                    for (Value value : values) {
                        results.add(value.getString());
                    }
                    
                }
            }
                
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return results;
    }
       
    @Override
    public List<String> getProductsWithSamples(String projectPath) {
        List<String> productsRequested = getProductsRequested(projectPath);
        ResourceResolver resourceResolver = getResourceResolver();
        List<String> results = new ArrayList<>();
        try {
            for (String prodPath : productsRequested) {
                if (hasReceivedSamples(prodPath, resourceResolver)) {
                    results.add(prodPath);
                }
            }
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return results;
    }

    private boolean hasReceivedSamples(String prodPath, ResourceResolver resourceResolver) throws RepositoryException {
        Node prodNode = resourceResolver.getResource(prodPath).adaptTo(Node.class);
        if (prodNode.hasProperty("quantityReceived")) {
            long quantity = prodNode.getProperty("quantityReceived").getLong();
            if (quantity>0) {
                return true;
            }
        }
        return false;
    }
    
    private List<String> getReconcileProductsRequested(String projectPath) {
        List<String> results = new ArrayList<>();
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            String assetPath = getAssetPath(projectNode);
            PredicateGroup rootPredicate = new PredicateGroup();
    
            Predicate pathPredicate = new Predicate("path", "path");
            pathPredicate.set("path", assetPath);
        
            Predicate typePredicate = new Predicate("type", "type");
            typePredicate.set("type", "sling:Folder");
            
            Predicate altPredicate = new Predicate("property","property");
            altPredicate.set("property", JLPConstants.JLP_RECONCILE_PRODUCT);
            altPredicate.set("operation", "exists");
            
            rootPredicate.add(pathPredicate);
            rootPredicate.add(typePredicate);
            rootPredicate.add(altPredicate);
    
            rootPredicate.setAllRequired(true);
        
            Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
            query.setHitsPerPage(0);
            SearchResult result = query.getResult();
            List<Hit> hits = result.getHits();
            for(Hit hit : hits) {
               results.add(hit.getNode().getPath());
            }
                
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return results;
    }
    
    private List<String> getReconcileProductsWithSamples(String projectPath) {
        List<String> productsRequested = getReconcileProductsRequested(projectPath);
        ResourceResolver resourceResolver = getResourceResolver();
        List<String> results = new ArrayList<>();
        try {
            for (String prodPath : productsRequested) {
                if (hasReceivedSamples(prodPath, resourceResolver)) {
                    results.add(prodPath);
                }
            }
        } catch (Exception e) {
            logger.error("Error getting count", e);
        } finally {
            if (resourceResolver!= null) {
                resourceResolver.close();
            }
        }
        return results;
    }
    
    @Override
    public int getAllProductsRequestedCount(String projectPath) {
        return getReconcileProductsRequested(projectPath).size()
                + getProductsRequested(projectPath).size();
    }

    @Override
    public int getAllProductsWithSamplesCount(String projectPath) {
        return getReconcileProductsWithSamples(projectPath).size()
                + getProductsWithSamples(projectPath).size();
    }

    private String getAssetPath(Node projectNode) throws RepositoryException {
        Node jcrContentNode = projectNode.getNode("jcr:content");
        String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
        return damFolderPath;
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            logger.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }

        return resourceResolver;
    }

}
