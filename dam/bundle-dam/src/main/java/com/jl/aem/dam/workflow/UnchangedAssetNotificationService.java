package com.jl.aem.dam.workflow;

public interface UnchangedAssetNotificationService {

    void execute();
}
