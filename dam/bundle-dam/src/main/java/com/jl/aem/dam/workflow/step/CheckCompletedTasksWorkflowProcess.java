package com.jl.aem.dam.workflow.step;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Check if all shoot tasks are completed and trigger release"})
public class CheckCompletedTasksWorkflowProcess extends AbstractProjectWorkflowProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(SetupAssetShootsWorkflowProcess.class);
    
    @Reference 
    ResourceResolverFactory resolverFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap metadata)
            throws WorkflowException {
        try { 
            Node project = getProjectFromPayload(item, session.getSession(), resolverFactory);
            List<String> tasks = getPendingTaskForProject(project);
            if (tasks.size() <= 0
                    && !briefsService.hasPendingReconcileProducts(project.getPath())) {
                briefsService.triggerReleaseWorkflow(project.getPath());
            }
        } catch (Exception e) {
            log.error("Can not set alt no needed", e);
        }
    }

    private List<String> getPendingTaskForProject(final Node project) throws RepositoryException {
        List<String> results = new ArrayList<>();
        Node tasksNode = project.getNode("jcr:content/tasks");
        NodeIterator tasksIterator = tasksNode.getNodes();
        while (tasksIterator.hasNext()) {
            Node tasksGroupNode = tasksIterator.nextNode();
            NodeIterator taskItems = tasksGroupNode.getNodes();
            while (taskItems.hasNext()) {
                Node taskItem = taskItems.nextNode();
                if (taskItem.getProperty("wfModelId").getString().indexOf("singleassetworkflow") >0 
                        && "ACTIVE".equalsIgnoreCase(taskItem.getProperty("status").getString())) {
                    results.add(taskItem.getProperty("wfInstanceId").getString());
                    
                }
            }
        }
        return results;
    }
    

}
