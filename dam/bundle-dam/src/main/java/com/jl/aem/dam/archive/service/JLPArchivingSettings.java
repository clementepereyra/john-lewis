package com.jl.aem.dam.archive.service;

public interface JLPArchivingSettings {

    String getPathToJsonKey();
    String getProjectID();
    String getBucketName();
    int getLayeredTiffLivingTimeInDays();
    int getFlattenTiffLivingTimeInDays();
    boolean getArchiveEnabled();
    boolean getKeepAssetMetadataAndThumbnails();
    int getThirdPartyAssetLivingTimeInHours();
    
}
