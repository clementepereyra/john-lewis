package com.jl.aem.dam.reports.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.jl.aem.dam.reports.models.results.JLContentItem;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class JLContentCellValue {
    
    @ValueMapValue
    private String property;

    @RequestAttribute
    private JLContentItem result;

    private String value;
    
    public JLContentCellValue() {
    }
    
    public JLContentCellValue(JLContentItem resource, String property) {
       this.result = resource;
       this.property= property;
       init();
    }

    public String getValue() {
        return value;
    }
    
    @PostConstruct
    private void init() {
        if (result != null && property != null) {
           if ("itemType".equals(property)) {
               value = result.getItemType();
           } else if ("cutout".equals(property)) {
               value = "" + result.getCutout();
           } else if ("lifestyle".equals(property)) {
               value = "" + result.getLifestyle();
           } else if ("retouch".equals(property)) {
               value = "" + result.getRetouch();
           } else if ("total".equals(property)) {
               value = "" + result.getTotal();
           }
        }
    }
}
