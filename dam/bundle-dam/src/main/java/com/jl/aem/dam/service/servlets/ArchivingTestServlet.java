package com.jl.aem.dam.service.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.archive.service.ArchiveService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/ArchiveTestServlet",
    "sling.servlet.methods=GET"
  }
)
public class ArchivingTestServlet extends SlingSafeMethodsServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 5584606190073457984L;
    @Reference
    private ArchiveService archiveService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
       
        String assetPath = request.getParameter("assetPath");
        String type = request.getParameter("type");
        if ("flatten".equals(type)) {
            
                archiveService.removeFlattenAssets();
                response.getWriter().write("OK");
            
        } else if ("layered".equals(type)) {
            if (assetPath != null) {
                String result = archiveService.archiveAsset(assetPath);
                response.getWriter().write(result);
            } else {
                archiveService.archiveLayeredAssets();
                response.getWriter().write("OK");
            }
        }
        response.setContentType("text/plain");
    }
}
