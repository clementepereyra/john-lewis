package com.jl.aem.dam.workflow.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.DateRangePredicateEvaluator;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.UnchangedAssetNotificationService;


@Component(immediate=true, name="JLP DAM Unchanged Asset Notification Service", service=UnchangedAssetNotificationService.class)
public class UnchangedAssetNotificationServiceImpl implements UnchangedAssetNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(UnchangedAssetNotificationServiceImpl.class);


    @Reference
    private JLPWorkflowSettings workflowSettings;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Override
    public void execute() {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
                logger.error("Can not login system user");
                throw new RuntimeException(e);
        }
        try {
            SearchResult result = searchAssets(resourceResolver, session);
            if (result.getTotalMatches() > 0) {
                
                String csvFileName = "unchanged_assets_"+ System.currentTimeMillis() + ".csv";
                File tempFile = null;
                InputStream tempFiles = null;
                FileOutputStream tempout = null;
                tempFile = File.createTempFile(csvFileName, null);
                tempout = new FileOutputStream(tempFile);
                final String commaSeparator = ",";
                tempout.write("AssetPath,Status,LastModified,CurrentOwner\n".getBytes());   

                List<Hit> hits = result.getHits();
                
                for (Hit hit : hits) {
                    try {
                        Node node = hit.getNode();
                        
                        StringBuilder csvLine = new StringBuilder();
                        csvLine.append(node.getPath());
                        csvLine.append(commaSeparator);
                        csvLine.append(node.getNode("jcr:content/metadata")
                                .getProperty(JLPConstants.JLP_STATUS).getString());
                        csvLine.append(commaSeparator);
                        csvLine.append(node.getNode("jcr:content")
                                .getProperty(JLPConstants.JCR_LAST_MODIFIED).getString());
                        csvLine.append(commaSeparator);
                        csvLine.append(node.getNode("jcr:content/metadata")
                                .getProperty(JLPConstants.JLP_ASIGNEE).getString());
                        
                        csvLine.append("\n");
                        tempout.write(csvLine.toString().getBytes());
                        
                    } catch (RepositoryException e) {
                        logger.error("Can not process asset.", e);
                    }
                }
                
                tempout.flush();
                tempFiles = new FileInputStream(tempFile);

                if(workflowSettings.getEnableEmailNotification()) {
                    // Getting the Email template.
                    String emailTemplate = "/etc/notification/email/html/jlp/unchangedAssetsTemplate.txt";
                    Resource templateRsrc = resourceResolver.getResource(emailTemplate);
    
                    MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
    
                    try {
                        MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                        // Creating the Email.
                        HtmlEmail email = new HtmlEmail();
                        Map<String, String> emailProperties = new HashMap<String,String>();
                        emailProperties.put("days", "" + workflowSettings.getUnchangedDaysLimit());
                        emailProperties.put("logoUrl", workflowSettings.getEmailLogoUrl());
                        emailProperties.put("logoAlt", workflowSettings.getEmailLogoAlt());
                        emailProperties.put("logoHeight", Integer.toString(workflowSettings.getEmailLogoHeight()));
                        emailProperties.put("signature", workflowSettings.getEmailSignature());
                        email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                        ByteArrayDataSource fileDS = new ByteArrayDataSource(tempFiles, "text/csv");
                        email.attach(fileDS, "text/csv", "This is your attached file.");
                        email.setTo(getEmailRecipients(workflowSettings.getUnchangedAssetRecipientList()));
                        messageGateway.send(email);
                    } catch (Exception e) {
                        logger.error("Fatal error while sending email: ", e);
                }
             }
                tempout.close();
                tempFiles.close();

            }
        } catch (Exception e) {
            logger.error("Fatal error while creating unchanged assets: ", e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        } 
    }
    
    
    private SearchResult searchAssets(ResourceResolver resourceResolver, Session session) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", workflowSettings.getObservedPath());

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "dam:Asset");

        Predicate datePredicate = new Predicate("daterange", "daterange");
        datePredicate.set(DateRangePredicateEvaluator.PROPERTY, "jcr:content/jcr:lastModified");
        long expirationTime = workflowSettings.getUnchangedDaysLimit() * 24 * 60 * 60 * 1000;
        datePredicate.set(DateRangePredicateEvaluator.UPPER_BOUND, "" + (System.currentTimeMillis() - expirationTime));
        datePredicate.set(DateRangePredicateEvaluator.UPPER_OPERATION, "<=");
        
        Predicate propertyPredicate = new Predicate("property", "property");
        propertyPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_STATUS);
        propertyPredicate.set(JcrPropertyPredicateEvaluator.VALUE, AssetStatus.FINAL_ASSET_APPROVED.toString());
        propertyPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_UNEQUALS);
        
        Predicate propertyPredicate2 = new Predicate("property2", "property");
        propertyPredicate2.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_STATUS);
        propertyPredicate2.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EXISTS);
        
        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.add(datePredicate);
        predicates.add(propertyPredicate);
        predicates.add(propertyPredicate2);
        predicates.setAllRequired(true);
                
        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);
        
        return query.getResult();
    }

    public List<InternetAddress> getEmailRecipients(String[] recipients) {

        ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
        for (String recipient : recipients) {
            try {
                emailRecipients.add(new InternetAddress(recipient));
            } catch (AddressException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return emailRecipients;
    }

    
}
