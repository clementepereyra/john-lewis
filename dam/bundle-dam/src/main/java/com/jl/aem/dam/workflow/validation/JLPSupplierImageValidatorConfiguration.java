package com.jl.aem.dam.workflow.validation;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Supplier Product Asset Validator Configuration", description = "Service Configuration")
public @interface JLPSupplierImageValidatorConfiguration {

    @AttributeDefinition(name ="Max Size", description="Max valid dimension size allowed in its larger dimension", defaultValue="3000")
    public int getMaxSize() default 3000;
    @AttributeDefinition(name ="Min Size", description="Minimum valid dimension size allowed in its larger dimension", defaultValue="800")
    public int getMinSize() default 800;
    @AttributeDefinition(name ="Extensions allowed", description="List of extensions allowed (lower case, no dot)", defaultValue={"tif", "tiff", "psd", "jpeg", "jpg", "png", "gif", "bmp"})
    public String[] getExtensions() default {"tif", "tiff", "psd", "jpeg", "jpg", "png", "gif", "bmp"};

}
