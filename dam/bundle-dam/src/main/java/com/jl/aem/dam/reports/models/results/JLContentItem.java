package com.jl.aem.dam.reports.models.results;

public class JLContentItem {

    private String itemType;
    private int total;
    private int cutout;
    private int lifestyle;
    private int retouch;
    public String getItemType() {
        return itemType;
    }
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    public int getCutout() {
        return cutout;
    }
    public void setCutout(int cutout) {
        this.cutout = cutout;
    }
    public int getLifestyle() {
        return lifestyle;
    }
    public void setLifestyle(int lifestyle) {
        this.lifestyle = lifestyle;
    }
    public int getRetouch() {
        return retouch;
    }
    public void setRetouch(int retouch) {
        this.retouch = retouch;
    }
    
    
}
