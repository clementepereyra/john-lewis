package com.jl.aem.dam.workflow.step.helper;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;

public class JLProductAssociator {
    
    private static final Logger log = LoggerFactory.getLogger(JLProductAssociator.class);

    public void associateAssetWithProduct(Node asNode, ResourceResolver resourceResolver, Session session) throws RepositoryException {
        
        Map<String, String> productInfo = ProductAssociatorHelper.getProductFromAsset(asNode, resourceResolver, session);
       
        associateAssetWithProduct(asNode, resourceResolver, session, productInfo);

    }
    
    public void associateAssetWithProduct(Node asNode, ResourceResolver resourceResolver, Session session, Map<String, String> productInfo) throws RepositoryException {
               
        String productNodePath = productInfo.get("productPath");
        
        if (productNodePath == null) {
            try {
                // it can be a brand logo which is not having a prod number in it
                if (ProductAssociatorHelper.isBrandLogo(asNode.getPath())) {
                    String imageCat = ProductAssociatorHelper.IMAGE_CAT_ROOT + "brl";                 
                    //Image Category assignation
                    setImageCategoryToAssetNode(asNode, imageCat);
                }
            } catch (Exception e) {
                log.error("Can not set asset status.", e);
            }
            return;
        }
        Node productNode = resourceResolver.getResource(productNodePath).adaptTo(Node.class);
        String imageCat = productInfo.get("imageCat");

        //Image Category assignation
        setImageCategoryToAssetNode(asNode, imageCat);
        
        JLWorkflowsUtils.setAssetPropertiesFromProduct(asNode, productNode, resourceResolver);
        session.save();
        Node assets = null;
        try {
            assets = JcrUtils.getNodeIfExists(productNode, "assets");
            
            if (assets == null) {
                log.info("Create product assets folder product: '" + productNode.getPath() + "'");
                assets = productNode.addNode("assets", "nt:unstructured");
            } 
            
            if (!existAsset(assets)) {
                log.info("Associating asset " + asNode.getPath() + " to product " + productNode.getPath());
                Node assetNode = assets.addNode("asset" + UUID.randomUUID(), "nt:unstructured");
                assetNode.setProperty("fileReference", asNode.getPath());
                assetNode.setProperty("sling:resourceType", "commerce/components/product/image");
                assetNode.setProperty("jcr:lastModified", Calendar.getInstance());
                assetNode.setProperty("jcr:lastModifiedBy", "JLP-service");
                assetNode.setProperty("assetCategory", imageCat);
            }
            session.save();
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Can not create or obtain assets node", e);
        }

    }
    
    
    public void associateAssetWithProducts(List<String> productsPath, Node asNode, Node lookNode,
            String imageCat, ResourceResolver resourceResolver) throws RepositoryException {
        try {
            // Image Category assignation
            setImageCategoryToAssetNode(asNode, imageCat);
            JLWorkflowsUtils.setAssetPropertiesFromLookNode(asNode, lookNode, productsPath,
                    resourceResolver);

            for (String productNodePath : productsPath) {
                Node productNode =
                        resourceResolver.getResource(productNodePath).adaptTo(Node.class);
                if (productNode != null) {
                    Node assets = JcrUtils.getNodeIfExists(productNode, "assets");

                    if (assets == null) {
                        log.info("Create product assets folder product: '" + productNode.getPath()
                                + "'");
                        assets = productNode.addNode("assets", "nt:unstructured");
                    }

                    if (!existAsset(assets)) {
                        log.info("Associating asset " + asNode.getPath() + " to product "
                                + productNode.getPath());
                        Node assetNode =
                                assets.addNode("asset" + UUID.randomUUID(), "nt:unstructured");
                        assetNode.setProperty("fileReference", asNode.getPath());
                        assetNode.setProperty("sling:resourceType",
                                "commerce/components/product/image");
                        assetNode.setProperty("jcr:lastModified", Calendar.getInstance());
                        assetNode.setProperty("jcr:lastModifiedBy", "JLP-service");
                        assetNode.setProperty("assetCategory", imageCat);
                    }
                }
                resourceResolver.commit();
            }
        } catch (Exception e) {
            log.error("Can not create or obtain assets node", e);
        }
    }

    private boolean existAsset(Node assets) {
        try {
            NodeIterator it = assets.getNodes();
            boolean exists = false;
            while (it.hasNext() && !exists) {
                Node assetNode = it.nextNode();
                if (assetNode.hasProperty("fileReference")) {
                    String path = assetNode.getProperty("fileReference").getString();
                    if (path.equals(assetNode.getPath())) {
                        return true;
                    }
                }
            }
        }
        catch (RepositoryException e){
            log.error("Can not finish exist asset correctly", e);
        }
        return false;
    }
    
    
    private void setImageCategoryToAssetNode(Node asNode, String imageCat) throws RepositoryException {
        Node metadataNode = JcrUtils.getNodeIfExists(asNode, "jcr:content/metadata");
        if (metadataNode != null) {
            metadataNode.setProperty(JLPConstants.JLP_IMAGE_CATEGORY, imageCat);
        }
    }

    
}
