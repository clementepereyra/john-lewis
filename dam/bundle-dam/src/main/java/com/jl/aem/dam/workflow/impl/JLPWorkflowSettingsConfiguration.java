package com.jl.aem.dam.workflow.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Workflow Settings Configuration", description = "Service Configuration")
public @interface JLPWorkflowSettingsConfiguration {
    

    @AttributeDefinition(name ="Reviewer Admin", description="Username to assign to assets when they are changed to 'Pending Review'", defaultValue="admin")
    String getReviewAdmin() default "admin";
    
    @AttributeDefinition(name ="Observed Paths", description="Paths to be observerd by JLP DAM workflows'", defaultValue="/content/dam")
    String getObservedPath() default "/content/dam";
    
    @AttributeDefinition(name ="Email Logo URL", description="Logo to be used in imails", defaultValue="https://www.johnlewis.com/assets/header/john-lewis-logo.gif", type = AttributeType.STRING)
    String getEmailLogoUrl() default "https://www.johnlewis.com/assets/header/john-lewis-logo.gif" ;
    
    @AttributeDefinition(name ="Email Logo text", description="Image Logo alt text to be used in the imagwe logo in emails ", defaultValue="John Lewis", type = AttributeType.STRING)
    String getEmailSignature() default "John Lewis" ;
    
    @AttributeDefinition(name ="Email Signature", description="Email signarute", defaultValue="John Lewis", type = AttributeType.STRING)
    String getEmailLogoAlt() default "John Lewis" ;
    
    @AttributeDefinition(name ="Email Logo height", description="Height of logo image to be used in imails", defaultValue="30", type = AttributeType.INTEGER)
    int getEmailLogoHeight() default 30 ;
    
    @AttributeDefinition(name ="Unchanged Limit", description="Max amount of days that an asset can be unchanged before being notified", defaultValue="7", type = AttributeType.INTEGER)
    int getUnchangedLimit() default 7 ;
    
    @AttributeDefinition(name="Unchanged Asset Report Recipient List", description="The list of email addresses that need notification about unchanged assets.", defaultValue={"shane.chapman@johnlewis.co.uk"})
    String[] getUnchangedAssetRecipientList() default {"shane.chapman@johnlewis.co.uk"};
    
    @AttributeDefinition(name="Invalid Products Recipient List", description="The list of email addresses that need notification about invalid products.", defaultValue={"sumit.goswami@johnlewis.co.uk"})
    String[] getInvalidProductRecipientList() default {"sumit.goswami@johnlewis.co.uk"};
    
    @AttributeDefinition(name="Artworkers Recipient List", description="The list of email addresses that need notification about assets ready for artworkers.", defaultValue={"shane.chapman@johnlewis.co.uk"})
    String[] getArtworkersRecipientList() default {"shane.chapman@johnlewis.co.uk"};
    
    @AttributeDefinition(name="Asset Status Report Recipient List", description="The list of email addresses that need notification when an asset needs an action.", defaultValue={"shane.chapman@johnlewis.co.uk"})
    String[] getAssetStatusRecipientList()default {"shane.chapman@johnlewis.co.uk"};
        
    @AttributeDefinition(name="Monitored Status List", description="The list of status that needs an action.", defaultValue={"ASSET_SUBMITTED_FOR_FINAL_APPROVAL", "FINAL_ASSET_REJECTED" , "ASSET_PENDING_ARTWORKING", "ASSET_AVAILABLE_FOR_ARTWORKING", "ASSET_PENDING_IC", "ASSET_ARTWORKING_REQUESTED" })
    String[] getMonitoredStatus() default {"ASSET_SUBMITTED_FOR_FINAL_APPROVAL", "FINAL_ASSET_REJECTED" , "ASSET_PENDING_ARTWORKING", "ASSET_AVAILABLE_FOR_ARTWORKING", "ASSET_PENDING_IC", "ASSET_ARTWORKING_REQUESTED" } ;
    
    @AttributeDefinition(name ="Born Url", description="Born Api for artworkers - Url", defaultValue="http://transfer.cito.borngroup.com/api/v1/files/new")
    String getArtworkersBornUrl() default "http://transfer.cito.borngroup.com/api/v1/files/new";
    
    @AttributeDefinition(name ="Born Secure Token", description="Born Api for artworkers - Secure token", defaultValue="CWzWWBAKFGsjQjHcTM8CBvXLP3rhXjnjsxhfRyqZRPxb3Wk3ZC")
    String getArtworkersBornSecureToken() default "CWzWWBAKFGsjQjHcTM8CBvXLP3rhXjnjsxhfRyqZRPxb3Wk3ZC";
    
    @AttributeDefinition(name ="Born Artworkers Job Type Id", description="Born Artworkers job type id", defaultValue="13")
    String getArtworkersBornJobTypeId() default "13";
    
    @AttributeDefinition(name ="Born Artworkers Business Area Id", description="Born Artworkers Business Area Id", defaultValue="41")
    String getArtworkersBornBusinessAreaId() default "41";
    
    @AttributeDefinition(name="Born Recipient List", description="The list of email addresses that need notification about a born call failed.", defaultValue={"support.transfer@borngroup.com"})
    String[] getArtworkersBornRecipientList() default {"support.transfer@borngroup.com"};
    
    @AttributeDefinition(name ="Enable email notifications", description="Set it as false to disable all email notfications", defaultValue="false")
    boolean getEnableEmailNotification() default false;

    @AttributeDefinition(name ="Expiration Report Days", description="The number of days pending to expire for an asset to be notified in expiration report", defaultValue="14")
    int getExpirationDaysWarning() default 14;

    @AttributeDefinition(name ="Expired Assets Recipient List", description="List of recipients for expired assets", defaultValue={"shane.chapman@johnlewis.co.uk"})
    String[] getExpiredAssetsRecipientList() default {"shane.chapman@johnlewis.co.uk"};
    
    @AttributeDefinition(name ="Enable auto complete Photographers task", description="Set it as true to enable auto complete Photographers task", defaultValue="true")
    boolean getEnableAutoCompletePhotographersTask() default true;
    
    @AttributeDefinition(name ="Enable auto complete Re-touchers task", description="Set it as false to disable auto complete Re-touchers task", defaultValue="true")
    boolean getEnableAutoCompleteReTouchersTask() default true;
    
    @AttributeDefinition(name ="Commit Timeout", description="Amount of milliseconds to wait after commiting a new folder or asset in a CC folder", defaultValue="3000")
    int getCommitTimeout() default 3000;
}
