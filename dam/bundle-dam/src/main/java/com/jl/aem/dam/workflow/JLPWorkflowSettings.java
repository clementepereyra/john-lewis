package com.jl.aem.dam.workflow;

public interface JLPWorkflowSettings {

    String getReviewAdmin();
    String getEmailLogoUrl();
    String getEmailLogoAlt();
    String getEmailSignature();
    int getEmailLogoHeight();
    String[] getUnchangedAssetRecipientList();
    String[] getAssetStatusRecipientList();
    String[] getInvalidProductRecipientList();
    String[] getArtworkersBornRecipientList();
    String[] getArtworkersRecipientList();
    String[] getMonitoredStatus();
    String getObservedPath();
    int getUnchangedDaysLimit();
    String getArtworkersBornUrl();
    String getArtworkersBornSecureToken();
    String getArtworkersBornJobTypeId();
    String getArtworkersBornBusinessAreaId();
    boolean getEnableEmailNotification();
    int getExpirationDaysWarning();
    String[] getExpiredAssetRecipientList();
    boolean getEnableAutoCompletePhotographersTask();
    boolean getEnableAutoCompleteReTouchersTask();
    int getCommitTimeout();
}
