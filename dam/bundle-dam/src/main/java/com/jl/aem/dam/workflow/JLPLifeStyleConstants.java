package com.jl.aem.dam.workflow;

public interface JLPLifeStyleConstants {

    public static final String NAME = "dam:JLPLifestyleName";
    public static final String LOOK_SHOOT_NAME = "dam:JLPLookName";
    public static final String NAMING_CONVENTION = "dam:JLPNamingConvention";
    public static final String SHOOT_DESCRIPTION = "dam:JLPShotDescription";
    public static final String BUYING_OFFICE = "dam:JLPBuyingOffice";
    public static final String CLIENT_STYLING_NOTES = "dam:JLPClientStylingNotes";
    public static final String RANGE = "dam:JLPRange";
    public static final String IDS = "dam:JLPIDS";
    public static final String JLP_PRODUCTS = "dam:JLPProducts";
    public static final String DIRECTORATE = "dam:JLPDirectorate";

}
