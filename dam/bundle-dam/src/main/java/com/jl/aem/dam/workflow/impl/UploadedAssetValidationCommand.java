package com.jl.aem.dam.workflow.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGatewayService;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.step.helper.ProductAssociatorHelper;
import com.jl.aem.dam.workflow.validation.JLPAssetValidator;
import com.jl.aem.dam.workflow.validation.JLPAssetValidatorFactory;


@Component(immediate=true, service=UploadedAssetValidationCommand.class)
public class UploadedAssetValidationCommand implements AssetStatusCommand {

    private static final Logger logger = LoggerFactory.getLogger(UploadedAssetValidationCommand.class);
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Reference
    private JLPWorkflowSettings workflowSettings;
    
    @Reference
    private JLPAssetValidatorFactory validatorFactory;
    
    @Override
    public void execute(String assetPath, final String userID) {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Resource resource = resourceResolver.getResource(assetPath);
            Node assetNode = resource.adaptTo(Node.class);
            session = resourceResolver.adaptTo(Session.class);
            Map<String, String> productInfo = isValidName(assetNode, resourceResolver, session);
            Node metadataNode = assetNode.getNode("jcr:content/metadata");           
            if (productInfo != null && (productInfo.get("productPath") != null || "true".equals(productInfo.get("isLogo")))) {
                String validPath = productInfo.get("productPath");
                String validationResult = isValidFormat(assetNode.getPath());
                if ("SUCCESS".equals(validationResult)) {
                    metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ASSET_PENDING_IC.toString());
                    metadataNode.setProperty(JLPConstants.JLP_ASIGNEE, workflowSettings.getReviewAdmin());
                    //updateAssetStatus(assetNode, resourceResolver, "Image Available");
                } else {
                    metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ASSET_RE_REQUESTED.toString());
                    metadataNode.setProperty(JLPConstants.ASSET_REJECTED_AUTO, true);
                    metadataNode.setProperty(JLPConstants.JLP_COMMENT, validationResult);
                    if (validPath != null) {
                        Node productNode = resourceResolver.getResource(validPath).adaptTo(Node.class);
                        
                        String validationCountPropName = JLPConstants.ASSET_VALIDATION_FAILS;  
                        
                        int valilationFailsCount = 1;
                        String imageCat = productInfo.get("imageCat").replace("jlp:image-category/alt/", "_").replace("jlp:image-category/", "_");
                        
                        if(assetNode.getPath().contains(JLPConstants.SUPPLIERS_ROOT_FOLDER)) {
                            
                            if(productNode.hasProperty(JLPConstants.ASSET_VALIDATION_FAILS + imageCat)){
                                valilationFailsCount = Integer.parseInt(productNode.getProperty(JLPConstants.ASSET_VALIDATION_FAILS + imageCat).getString()) + 1;
                            }
                            
                            productNode.setProperty(validationCountPropName + imageCat ,valilationFailsCount);
                        }
                    }
                   
                    
                    //updateAssetStatus(assetNode, resourceResolver, "Image Re-Requested");
                }
            } else {
                metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ASSET_RE_REQUESTED.toString());
                metadataNode.setProperty(JLPConstants.ASSET_REJECTED_AUTO, true);
                metadataNode.setProperty(JLPConstants.JLP_COMMENT, "Asset name is not valid: can not be associated to a existent product or brand logo");
                //updateAssetStatus(assetNode, resourceResolver, "Image Re-Requested");
            }
            session.save();
            resourceResolver.commit();
        } catch (Exception e){
            logger.error("Can not validate new asset: " + assetPath, e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }


    
    private String isValidFormat(String assetPath) {
        JLPAssetValidator validator = validatorFactory.getAssetValidator(assetPath);
        if (validator != null) {
            return validator.validateAsset(assetPath);
        } 
        return "Asset not suppported by JL framework";
    }
    
    private Map<String, String> isValidName(final Node asNode, final ResourceResolver resourceResolver, final Session session) throws RepositoryException {
        Map<String, String> productInfo = ProductAssociatorHelper.getProductFromAsset(asNode, resourceResolver, session);
        if (productInfo.get("productCode") != null && productInfo.get("productPath") != null) {
            return productInfo;
        } else if (ProductAssociatorHelper.isBrandLogo(asNode.getPath()) ) {
            productInfo = new HashMap<String, String>();
            productInfo.put("isLogo", "true");
            return productInfo;
        }
        return null;
            
    }
    

}
