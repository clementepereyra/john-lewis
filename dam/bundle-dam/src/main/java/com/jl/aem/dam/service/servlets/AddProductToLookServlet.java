package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/AddProductToLook",
        "sling.servlet.methods=POST"})
public class AddProductToLookServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        response.setContentType("application/json");

        String currentFolderPath = request.getParameter("project_path");
        String productCode = request.getParameter("product_code");
        RequestParameter assetPaths[] = request.getRequestParameters("path");
        
        ResourceResolver resourceResolver = request.getResourceResolver();
        Node currentFolder = resourceResolver.getResource(currentFolderPath).adaptTo(Node.class);
        String projectPath = null;
        try {
            if (currentFolder.hasProperty("projectPath")) {
                Property projectPathProperty =  currentFolder.getProperty("projectPath");
                if (projectPathProperty.isMultiple()) {
                    Value[] values =  projectPathProperty.getValues();
                    projectPath = values[0].getString();
                } else {
                    projectPath = projectPathProperty.getString();
                }
            }
        } catch (Exception e) {
            response.getWriter().write("{\"result\":\"ERROR - Internal error reading project info\"}");
            return;
        }
        if (assetPaths == null || assetPaths.length == 0) {
            response.getWriter().write("{\"result\":\"ERROR - No folder was selected\"}");
            return;
        } else if (productCode == null || productCode.trim().isEmpty()) {
            response.getWriter().write("{\"result\":\"ERROR - No product was selected\"}");
            return;
        } else if (projectPath == null || projectPath.trim().isEmpty()) {
            response.getWriter().write("{\"result\":\"ERROR - Current folder is not a from lifestyle project\"}");
            return;
        }
        for (RequestParameter path : assetPaths) {  
            String lookName = path.getString().replace(currentFolderPath, "").replace("/","");
            String result = briefService.addProductToLifestyleLook(projectPath, lookName, productCode);
            if (!"SUCCESS".equals(result)) {
                response.getWriter().write("{\"result\":\""+result+"\"}");
                return;
            }
        }
       
        response.getWriter().write("{\"result\":\"SUCCESS\"}");
                
    }
    

}
