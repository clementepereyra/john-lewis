
package com.jl.aem.dam.workflow;

public interface JLPConstants {

    public static final String JCR_LAST_MODIFIED = "jcr:lastModified";
    public static final String JLP_LAST_ASIGNEE ="dam:lastJLPAssigned";
    public static final String JLP_LAST_STATUS = "dam:lastJLPStatus";
    public static final String JLP_ASIGNEE =  "dam:JLPAssigned";
    public static final String JLP_STATUS = "dam:JLPStatus";
    public static final String JLP_EXPIRATION_DATE = "prism:expirationDate";
    public static final String JLP_CHANNEL_COVERAGE = "dam:JLPChannelCoverage";
    public static final String JLP_PRODUCT_CODES = "dam:JLPProductCodes";
    public static final String JLP_PRODUCT_TITLE = "dam:JLPProductTitle";
    public static final String JLP_DIRECTORATE = "dam:JLPDirectorate";
    public static final String JLP_BUYING_GROUP = "dam:JLPBuyingGroup";
    public static final String JLP_SEASON = "dam:JLPSeason";
    public static final String JLP_HALF = "dam:JLPHalf";
    public static final String JLP_YEAR = "dam:JLPYear";
    public static final String JLP_ECAMPAIGN = "dam:JLPCampaign";
    public static final String JLP_SOURCE = "dam:JLPSource";
    public static final String JLP_SUPPLIER_CONTACT = "dam:supplierContactDetail";
    public static final String JLP_PRODUCT_TYPE = "dam:JLPProductType";
    public static final String JLP_ROOM = "dam:JLPRoom";
    public static final String JLP_STOCK_LEVEL = "dam:JLPStockLevel";
    public static final String JLP_CUTOUT_VS_CREATIVE = "dam:JLPCutoutVSCreative";
    public static final String JLP_COMMENT = "dam:JLPComment";
    public static final String JLP_IMAGE_CATEGORY = "dam:JLPImageCategory";
    public static final String JLP_ITEM_ID = "dam:JLPItemId";
    public static final String JLP_MODEL_NO = "dam:ModelNumber";
    public static final String JLP_PROD_DELETED = "dam:ProductDeleted";
    public static final String JLP_BRIEF_END_USE = "endUse";
    public static final String JLP_PROJECT_PATH = "projectPath";
    public static final String JLP_DAM_END_USE = "dam:JLPBriefEndUse";
    
    public static final String ASSET_REJECTED_AUTO = "rejectedAuto";
    public static final String ASSET_VALIDATION_FAILS = "assetValidationFailsCount";
    public static final String ASSET_ART_VALIDATION_FAILS = "artworkingValidationFailsCount";

    public String PRODUCTS_ROOT_FOLDER = "/etc/commerce/products/onejl";

    public static final String ASSET_STATUS_CHANGE_AVAILABLE_TIME = "dateAvailable";
    public static final String ASSET_STATUS_CHANGE_COMPLETED_TIME = "dateCompleted";
    public static final String ASSET_STATUS_CHANGE_AVAILABLE_USER = "userAvailable";
    public static final String ASSET_STATUS_CHANGE_COMPLETED_USER = "userCompleted";
    public static final String ASSET_STATUS_CHANGE_IC_APPROVED_TIME = "dateICApproved";
    public static final String ASSET_STATUS_CHANGE_PROVIDED_BY_ARTWORKER = "dateProvidedByArtworker";
    public static final String ASSET_STATUS_CHANGE_SUPPLIER_UPLOADED = "dateSupplierUploaded";
    public static final String ASSET_STATUS_CHANGE_SUPPLIER_REQUESTED = "dateSupplierRequested";
    public static final String ASSET_STATUS_CHANGE_REJECTED_TIME = "dateRejected";

    public static final String IMAGE_COMPLETE_LAST_QUERY_UPDATE = "lastQueryUpdate";
    public static final String CREATIVE_ASSETS_ROOT_FOLDER = "/content/dam/Creative";
    
    public static final String SUPPLIERS_ROOT_FOLDER = CREATIVE_ASSETS_ROOT_FOLDER + "/Suppliers";
    
    public static final String ARTWORKERS_ROOT_FOLDER = CREATIVE_ASSETS_ROOT_FOLDER + "/Artworkers";
    public static final String ARTWORKERS_PENDING_RETOUCH = ARTWORKERS_ROOT_FOLDER + "/PendingRetouch";
    public static final String ARTWORKERS_RETOUCHED = ARTWORKERS_ROOT_FOLDER + "/Retouched";  
    
    public static final String JOHNLEWIS_ROOT_FOLDER = CREATIVE_ASSETS_ROOT_FOLDER + "/JohnLewis";
    public static final String JOHNLEWIS_REVIEW_FOLDER = JOHNLEWIS_ROOT_FOLDER + "/Review";
    public static final String JOHNLEWIS_SCENE7_FOLDER = JOHNLEWIS_ROOT_FOLDER + "/Scene7";
    public static final String JOHNLEWIS_ARCHIVE_FOLDER = JOHNLEWIS_ROOT_FOLDER + "/Archive";
    public static final String IC_REVIEW_PATH = CREATIVE_ASSETS_ROOT_FOLDER + "/ImageCoordinator";
    public static final String JLP_SUPPLIER_ID = "dam:supplierID";
    public static final String JLP_BRAND_NAME = "dam:brandName";
    public static final String ARTWORKERS_AVAILABLE = ARTWORKERS_ROOT_FOLDER + "/Available";
    public static final String JLP_BUYING_OFFICE = "dam:JLPBuyingOffice";
    public static final String JLP_WSM_TITLE = "dam:wsmTitle";
    public static final String JLP_EAN = "dam:eanTradedCode";
    public static final String JLP_COLOUR = "dam:colour";
    public static final String JLP_COLOUR_GROUP = "dam:colourGroup";
    public static final String JLP_LAUNCH_DATE = "dam:launchDate";
    public static final String PROJECTS_DAM_PATH = "/content/dam/projects";
    public static final String PROJECTS_ROOT_FOLDER = "/content/projects";
    public static final String CSV_IMPORT_PATH = PROJECTS_DAM_PATH + "/temp";
    public static final String JLP_BRIEF_ASSET_STATUS = "dam:JLPBriefAssetStatus";
    public static final String JLP_BRIEF_DELIVERY_DATE = "dam:JLPBriefDueDate";
    public static final String JLP_BRIEF_NAME = "dam:JLPBriefName";
    public static final String DAM_JLP_PRODUCTS = "dam:JLPProducts";

    public static final String PROVIDE_SHOOT_CUTOUT_WORKFLOW_NAME = "/etc/workflow/models/jl-cutout-singleassetworkflow/jcr:content/model";
    public static final String PROVIDE_SHOOT_LIFESTYLE_WORKFLOW_NAME = "/etc/workflow/models/jl-lifestyle-singleassetworkflow/jcr:content/model";
    public static final String PROVIDE_SHOOT_RETOUCH_WORKFLOW_NAME = "/etc/workflow/models/jl-retouch-singleassetworkflow/jcr:content/model";
    public static final String SCENE7_WORKFLOW = "/etc/workflow/models/scene7/jcr:content/model";
    public static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";
    public static final String MOVE_ASSET_AND_AUTOCOMPLETE_WORKFLOW = "/etc/workflow/models/dam/move-asset-and-autocompletetask/jcr:content/model";
    public static final String IMAGE_CAT_ROOT = "jlp:image-category/";
    public static final String CUTOUT_COMPLETION_WORKFLOW_MODEL = "/etc/workflow/models/jl-cutout-workflowcompletion/jcr:content/model";
    public static final String LIFESTYLE_COMPLETION_WORKFLOW_MODEL = "/etc/workflow/models/jl-lifestyle-workflowcompletion/jcr:content/model";
    public static final String RETOUCH_COMPLETION_WORKFLOW_MODEL = "/etc/workflow/models/jl-retouch-workflowcompletion/jcr:content/model";

    public final static String CUTOUT_WORKFLOW_ID = "/etc/workflow/models/jl-cutout-project/jcr:content/model";
    public final static String LIFESTYLE_WORKFLOW_ID = "/etc/workflow/models/jl-lifestyle-project/jcr:content/model";
    public final static String RETOUCH_WORKFLOW_ID = "/etc/workflow/models/jl-retouch-project/jcr:content/model";

    public static final String COMPOSITE_FOLDER = "Composites";
    public static final String PROJECT_STATUS = "JLPStatus";
    public static final String PROJECT_COMMENTS = "JLPComments";
    public static final String PROJECT_COMPLETE_USER = "JLPCompletedBy";
    public static final String PROJECT_COMPLETE_DATE = "JLPCompletedOn";
    public static final String DAM_FOLER_PATH ="damFolderPath";
    public static final String WORKFLOW_MODEL_ID = "wfModelId";
    public static final String WORKFLOW_INSTACE_ID = "wfInstanceId";
    public static final String WORKFLOW_STATUS = "status";
    public static final String ACTIVE_WORKFLOW_STATUS = "ACTIVE";
    public static final String COMPLETE_WORKFLOW_STATUS = "COMPLETE";

    public static final String RELEASE_ONLINE_VALUE = "Product Launch/Online : Cropped tif";
    public static final String RELEASE_MULTI_USE_ONLINE_VALUE = "Multi Use (Including Online)";
    
    public static final String REFERENCE_FOLDER = "Reference";
    
    public static final String JLP_RECONCILE_PRODUCT = "JLPReconcileProduct";

    
    public static final String RENDITION_ORIGINAL = "original";
    public static final String RENDITION_LAYERED = "original-layered";
    public static final String RENDITION_FLATTEN = "original-flatten.tif";
    
    
    public static final String THIRD_PARTIES_RETOUCHERS_DAM_PATH = "/content/dam/3rdparties";
    public static final String THIRD_PARTY_PENDING_RETOUCH_FOLDER = "pending-retouch";
    public static final String THIRD_PARTY_RETOUCHED_FOLDER = "retouched";
    public static final String BORN_NAME = "born";
    public static final String THIRD_PARTY_ASSET_PATH = "thirdPartyAssetPath";
    public static final String JL_RETOUCHER = "JLRetoucher";
    public static final String OUTSOURCED_TO = "dam:JLPOutsourcedTo";
    public static final String RETOUCH_MODE = "dam:JLPRetouchMode";
    
}
