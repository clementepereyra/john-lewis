package com.jl.aem.dam.briefs.service;

import java.util.List;

public class RetouchBrief {
  
    private List<RetouchBriefRow> rows;
    private boolean valid = true;
    
    public static class RetouchBriefRow { 
        private String stockNumber;
        private String comments;
        private String prodType;
        
        public String getStockNumber() {
            return stockNumber;
        }
        public void setStockNumber(String stockNumber) {
            this.stockNumber = stockNumber;
        }
        
        public void setComments(String comments) {
            this.comments = comments;
        }
        public String getComments() {
            return comments;
        }
        public String getProdType() {
            return prodType;
        }
        public void setProdType(String prodType) {
            this.prodType = prodType;
        }
        
    }

    public void setNotValid() {
        valid = false;
    }
    
    public boolean isValid() {
        return valid;
    }

    public List<RetouchBriefRow> getRows() {
        return rows;
    }



    public void setRows(List<RetouchBriefRow> rows) {
        this.rows = rows;
    }
}

