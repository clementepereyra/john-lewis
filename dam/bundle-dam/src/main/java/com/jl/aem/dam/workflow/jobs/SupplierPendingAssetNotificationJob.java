package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.SupplierPendingAssetsNotificationService;

/**
 * Schedulable job to sent a daily notification to suppliers that have to upload images
 * 
 * @author Bruno Szumpich
 *
 */
@Component(immediate = true, name = "JLP Daily supliers pending asset notification job",service= {Runnable.class},
    property ={"scheduler.expression=0 30 7 * * ? "})
public class SupplierPendingAssetNotificationJob implements Runnable {

    private static final Logger logger = LoggerFactory
            .getLogger(SupplierPendingAssetNotificationJob.class);

    @Reference
    private SupplierPendingAssetsNotificationService service;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance) {
            logger.trace("Running suppliers daily pending assets notification job");
            service.execute();
            logger.trace("Asset status daily pending assets notification completed");
        }
    }
}