package com.jl.aem.dam.reports.models.helper;

public class JLReportExecutoryHelperFactory {
	
	    public JLReportExecutorHelper getReportExecutorHelper(String type) {
        if ("inflightContent".equals(type)) {
            return new InflightContentReportExecutorHelper();
        } else if ("totalContent".equals(type)) {
            return new TotalContentReportExecutorHelper();
        } else if ("sniReport".equals(type)) {
            return new SNIReportExecutorHelper();
        } else if ("inflightProjectsDetail".equals(type)) {
            return new InflightProjectsDetailReportExecutorHelper();
        } else if ("futureContent".equals(type)) {
            return new FutureContentReportExecutorHelper();
        }
        else if ("overDueReports".equals(type)) {
        	return new JLOverDueProjectDetailsHelper();
        }
        return null;
    }
}
