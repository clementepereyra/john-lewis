package com.jl.aem.dam.briefs.workflow.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.Workflow;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.model.WorkflowModel;
import com.jl.aem.dam.briefs.workflow.ProjectWorkflowInfo;

public class ProjectWorkflowHelper {
    
    static final Logger log = LoggerFactory.getLogger(ProjectWorkflowHelper.class);


    public static void startProjectWorkflow(ProjectWorkflowInfo workflowInfo, WorkflowService workflowService, ResourceResolver resourceResolver) {
        try {
            Node contentNode = workflowInfo.getProjectContentNode();
            if (!contentNode.hasNode("work")) {
                contentNode.setProperty("tasks.folder", "tasks");
                contentNode.addNode("tasks", "nt:unstructured");
            }
            contentNode.setProperty("work.folder", "work");
            resourceResolver.commit();
            Session session = resourceResolver.adaptTo(Session.class);
            
            String workflowTitle = workflowInfo.getWorkflowTitle();
            String workflowNodeName = workflowInfo.getWorkflowNodeName();
            
            Node workNode = null;
            if (contentNode.hasNode("work")) {
                workNode = contentNode.getNode("work");
            } else {
                workNode = contentNode.addNode("work", "nt:unstructured");
            }
            if (workNode.hasNode(workflowNodeName)) {
                workNode.getNode(workflowNodeName).remove();
            }
            
            ProjectRoleHelper roleHelper = new ProjectRoleHelper(contentNode.getParent());
            
            String workflowContent =contentNode.getParent().getPath();
            Map<String, Object> metadata = new HashMap<>();
            
            metadata.put("_charset_","utf-8");
            metadata.put("currentJobs","");
            metadata.put("modelId", workflowInfo.getWorkflowModel());
            metadata.put("modelId@Delete","");
            metadata.put("project", workflowContent);
            metadata.put("project.group.activitycoordinator", roleHelper.getActivityCoordinator());
            metadata.put("project.group.stockcoodinator", roleHelper.getStockCoordinator());
            
            metadata.put("project.group.shootproducer", roleHelper.getShootProducer());
            metadata.put("project.group.retoucher", roleHelper.getRetoucher());
            metadata.put("project.group.owner", roleHelper.getOwner());
            metadata.put("project.group.photographers", roleHelper.getPhotographer());
            metadata.put("project.group.externalretoucher", roleHelper.getExternalRetoucher());
            metadata.put("project.group.postproductioncoordinator", roleHelper.getPostProductionCoordinator());
            metadata.put("project.group.artdirector", roleHelper.getArtDirector());
            metadata.put("project.path",workflowContent);
            metadata.put("project.workLinkPath", workflowContent + "/jcr:content/work/" + workflowNodeName);
            metadata.put("startComment","");
            metadata.put("wizard","/libs/cq/core/content/projects/workflowwizards/photoshoot.html");
            metadata.put("workflowTitle", workflowTitle);
    
            // Create a workflow session
            WorkflowSession wfSession = workflowService.getWorkflowSession(session);
    
            // Get the workflow model
            WorkflowModel wfModel = wfSession.getModel(workflowInfo.getWorkflowModel());
    
            // Get the workflow data
            // The first param in the newWorkflowData method is the payloadType.
            // Just a fancy name to let it know what type of workflow it is
            // working with.
            WorkflowData wfData = wfSession.newWorkflowData("JCR_PATH", workflowContent);
    
            // Run the Workflow.
            Workflow workflow = wfSession.startWorkflow(wfModel, wfData, metadata);
            
            Node workItemNode = workNode.addNode(workflowNodeName, "nt:unstructured");
            workItemNode.setProperty("model.id", workflowInfo.getWorkflowModel());
            workItemNode.setProperty("model.tags", "misc");
            workItemNode.setProperty("workflow.id", workflow.getId());
                        
            Node workflowNode = resourceResolver.getResource(workflow.getId()).adaptTo(Node.class);
            workflowNode.setProperty("initiator", workflowInfo.getUserId());

            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Error starting workflow." , e);
        }
    }
  
}
