package com.jl.aem.dam.briefs.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.exec.Route;
import com.jl.aem.dam.briefs.service.AssetTaskCompleterService;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=AssetTaskCompleterService.class)
public class AssetTaskCompleterServiceImpl implements AssetTaskCompleterService {

    private static final Logger log = LoggerFactory.getLogger(AssetTaskCompleterServiceImpl.class);
    
    private Map<AutocompleteStatus, StatusCompleter> completers;

    @Reference
    private ResourceResolverFactory resolverFactory;
    

    
    @Override
    public boolean canComplete(final String assetPath, final AutocompleteStatus newStatus) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource assetResource = resourceResolver.getResource(assetPath);
            if (assetResource != null) {
                Node assetNode = assetResource.adaptTo(Node.class);
                StatusCompleter completer = completers.get(newStatus);
                return completer.canComplete(assetNode);
            }
            
        } catch (Exception e) {
            log.error("Error checking asset current status", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return false;
    }
    
    @Override
    public void complete(String assetPath, AutocompleteStatus newStatus, String comments) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource assetResource = resourceResolver.getResource(assetPath);
            if (assetResource != null) {
                Node assetNode = assetResource.adaptTo(Node.class);
                StatusCompleter completer = completers.get(newStatus);
                completer.complete(assetNode, resourceResolver, comments);
            }
            
        } catch (Exception e) {
            log.error("Error checking asset current status", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        
        
    }
    
    @Activate
    private void activate () {
        completers = new HashMap<>();
        completers.put(AutocompleteStatus.EXTERNAL_RETOUCH, new ExternalRetouchedCompleter());
        completers.put(AutocompleteStatus.INTERNAL_RETOUCH, new InternalRetouchedCompleter());
        completers.put(AutocompleteStatus.RESHOOT, new ReshootCompleter());
        completers.put(AutocompleteStatus.ALT_NOT_NEEDED, new AltNotNeededCompleter());
        completers.put(AutocompleteStatus.EXTERNAL_RETOUCH_DONE, new ExternalRetouchDoneCompleter());
        completers.put(AutocompleteStatus.COLOUR_MATCH_NOT_NEEDED, new ColourMatchDoneCompleter());
    }
    
    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
    
    
    private static abstract class StatusCompleter {
        
        private static final String PROVIDE_SHOOT_PATH = "/jcr:content/work/provide-shoot-";
       
        public boolean canComplete(Node assetNode) throws ValueFormatException, PathNotFoundException, RepositoryException {
            Node metadata = assetNode.getNode("jcr:content/metadata");
            if (metadata.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                if (metadata.hasProperty(JLPConstants.RETOUCH_MODE)) {
                    return false;
                }
                BriefAssetStatus currentStatus = BriefAssetStatus.valueOf(metadata.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                return currentStatus.equals(BriefAssetStatus.ASSET_UPLOADED);
            }
            return false;
        }

        public abstract boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments);
        
        protected void autoCompleteCurrentAssetWorkflow(final Node assetNode, ResourceResolver resourceResolver, String action, String comments) throws Exception {
            

                String projectPath = BriefUtils.getProjectPath(assetNode);
                String projectWorkPath = null;
                String productCode = null;
                String assetFolderName = null;
                String eanCode = null;
               
                
                Node assetFolderNode = assetNode.getParent();
                if (assetFolderNode != null) {
                    assetFolderName = assetFolderNode.getName();
                    Node productNode = assetFolderNode.getParent();
                    if (productNode != null) {
                        productCode = productNode.getName();
                        eanCode = productNode.hasProperty("ean")?productNode.getProperty("ean").getString():"-";
                    }
                }

                Node projectWorkNode = null;
                if (productCode != null && assetFolderName != null) {
                    projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                            .append(productCode).append("-").append(assetFolderName).toString();
                    Resource projectWorkResurce = resourceResolver.getResource(projectWorkPath);

                    if (projectWorkResurce != null) {
                        projectWorkNode = projectWorkResurce.adaptTo(Node.class);
                    } else {
                        projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                                .append(eanCode).append("-").append(assetFolderName).toString();
                        projectWorkResurce = resourceResolver.getResource(projectWorkPath);
                        if (projectWorkResurce != null) {
                            projectWorkNode = projectWorkResurce.adaptTo(Node.class);
                        }
                    }
                }
                
                com.adobe.granite.workflow.WorkflowSession graniteWfSession =
                        resourceResolver.adaptTo(com.adobe.granite.workflow.WorkflowSession.class);

                if (graniteWfSession != null && projectWorkNode != null) {
                    String workflowId = null;
                    if (projectWorkNode.hasProperty("workflow.id")) {
                        workflowId = projectWorkNode.getProperty("workflow.id").getString();
                    }

                    if (workflowId != null) {
                        
                        Node mainWorkflowNode = resourceResolver.getResource(workflowId).adaptTo(Node.class);
                        Node workItemNode = mainWorkflowNode.getNode("workItems");
                        Node workflowMetadataNode = mainWorkflowNode.getNode("data/metaData");
                        NodeIterator workItemChildIterator = workItemNode.getNodes();
                        if (workItemChildIterator.hasNext()) {
                            Node workItemInstanceNode = (Node) workItemChildIterator.next();
                            com.adobe.granite.workflow.exec.WorkItem workItem =
                                    graniteWfSession.getWorkItem(workItemInstanceNode.getPath());

                            Node metaDataWorkItemNode = resourceResolver
                                    .getResource(workItemInstanceNode.getPath() + "/metaData")
                                    .adaptTo(Node.class);

                            String taskId = null;
                            if (metaDataWorkItemNode.hasProperty("taskId")) {
                                log.debug("Set as compleate taskId {} ", taskId);
                                taskId = metaDataWorkItemNode.getProperty("taskId").getString();
                                Node taskNode =
                                        resourceResolver.getResource(taskId).adaptTo(Node.class);
                                taskNode.setProperty("status", "COMPLETE");
                                resourceResolver.commit();
                            }
                            
                            if (workflowMetadataNode != null) {
                                //workflowMetadataNode.setProperty("lastTaskAction", action);
                                workItem.getWorkflow().getWorkflowData().getMetaDataMap().put("lastTaskAction", action);
                                workItem.getMetaDataMap().put("comment", comments);
                                resourceResolver.commit();
                            }
                            
                            
                            log.debug("workItem id = ", workItem.getId());
                            List<Route> routes = graniteWfSession.getRoutes(workItem, false);
                            log.debug("Number of routes for the workItem = {}", routes.size());
                            // It is a linear workflow model.
                            Route route = routes.get(0);
                            if (route != null) {
                                log.debug("routing: {}",route.getName());
                                graniteWfSession.complete(workItem, routes.get(0));
                            }
                        }
                    }
                }
            }
    }
    
    private static class ExternalRetouchedCompleter extends StatusCompleter {

        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            try {
                super.autoCompleteCurrentAssetWorkflow(assetNode, resourceResolver, "Request 3rd Party Retouch", comments);
                return true;
            } catch (Exception e) {
                log.error("Error autocompleting asset workflow", e);
            }
            return false;
        }
    }
    
    private static class InternalRetouchedCompleter extends StatusCompleter {

        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            try {
                super.autoCompleteCurrentAssetWorkflow(assetNode, resourceResolver, "Send to Retouch", comments);
                return true;
            } catch (Exception e) {
                log.error("Error autocompleting asset workflow", e);
            }
            return false;
        }
    }
    
    private static class ReshootCompleter extends StatusCompleter {

        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            try {
                super.autoCompleteCurrentAssetWorkflow(assetNode, resourceResolver, "Request Re-Shoot", comments);
                return true;
            } catch (Exception e) {
                log.error("Error autocompleting asset workflow", e);
            }
            return false;
        }
    }
    
    private static class AltNotNeededCompleter extends StatusCompleter {
        
        public boolean canComplete(Node assetNode) throws ValueFormatException, PathNotFoundException, RepositoryException {
            return false;
        }
        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            
            return false;
        }
    }
    
    private static class ExternalRetouchDoneCompleter extends StatusCompleter {
        
        public boolean canComplete(Node assetNode) throws ValueFormatException, PathNotFoundException, RepositoryException {
            return false;
        }
        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            try {
                super.autoCompleteCurrentAssetWorkflow(assetNode, resourceResolver, "Send to Post Production", comments);
                return true;
            } catch (Exception e) {
                log.error("Error autocompleting asset workflow", e);
            }
            return false;
        }
    }
    
private static class ColourMatchDoneCompleter extends StatusCompleter {
        
        public boolean canComplete(Node assetNode) throws ValueFormatException, PathNotFoundException, RepositoryException {
            Node metadata = assetNode.getNode("jcr:content/metadata");
            if (metadata.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                BriefAssetStatus currentStatus = BriefAssetStatus.valueOf(metadata.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                return currentStatus.equals(BriefAssetStatus.PENDING_COLOUR_MATCH);
            }
            return false;
        }
        @Override
        public boolean complete(Node assetNode, ResourceResolver resourceResolver, String comments) {
            try {
                Node metadata = assetNode.getNode("jcr:content/metadata");
                metadata.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_RETOUCHED.toString());
                assetNode.getParent().setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_RETOUCHED.toString());
                resourceResolver.commit();
                return true;
            } catch (Exception e) {
                log.error("Error autocompleting asset workflow", e);
            }
            return false;
        }
    }
}
