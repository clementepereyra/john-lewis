package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowService;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.briefs.workflow.ProjectWorkflowInfo;
import com.jl.aem.dam.briefs.workflow.impl.ProjectWorkflowHelper;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/AddNewAlt",
    "sling.servlet.methods=POST"
  }
)
public class AddNewAltServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 6090044399598818194L;

	private static final Logger log = LoggerFactory.getLogger(AddNewAltServlet.class);
	
    @Reference
    private WorkflowService workflowService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        try {
            // Get the submitted form data
            int altNumber = Integer.parseInt(request.getParameter("new_alt_number"));
            RequestParameter assetPaths[] = request.getRequestParameters("path");
            if (assetPaths != null && altNumber != 0) {
                for (RequestParameter path : assetPaths) {
              	   	//search for alts - get the highest number from results - add new alts
                    Resource resource = resourceResolver.getResource(path.toString());
                    if (null != resource) {
                    	int highestAltNumber=1;
                    	for(Resource child : resource.getChildren()){                    		
                    		if (child.getName().startsWith("alt")) {
								int currentAltNumber = Integer.parseInt(child.getName().substring(3));
								if (currentAltNumber > highestAltNumber) highestAltNumber = currentAltNumber;
							}
                    	}                    	
                    	for (int i = 1; i <= altNumber; i++) {
                    		Node newAlt = JcrUtils.getOrAddNode(resource.adaptTo(Node.class), ("alt"+(highestAltNumber+i)), "sling:Folder");
                    		newAlt.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
                    		newAlt.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
                    		newAlt.addNode("jcr:content", "nt:unstructured");
                    	    startShootTaskIfNeeded(newAlt, resourceResolver);
                    	}
                		resource.getResourceResolver().commit();
                    }
                }
            }
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in Add New Alts Servlet " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void startShootTaskIfNeeded(Node altNode, ResourceResolver resourceResolver) throws RepositoryException {
        
        Node projectContentNode = getProjectContentNode(altNode, resourceResolver);
        if (projectContentNode != null) {
            if (projectContentNode.hasProperty("project.shootstartDate") && projectContentNode.hasProperty("project.shootendDate")) {
                if (datesConfirmed(projectContentNode)) {
                    String altId = altNode.getName();
                    String baseShootId = altNode.getParent().getName();
                    String workflowTitle = baseShootId + "-" + altId;
                    String workflowNodeName = "provide-shoot-"+ baseShootId + "-" + altId;
                    
                    ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName,
                            BriefUtils.getSingleAssetWorkflowModel(projectContentNode), projectContentNode, "admin");
                    ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService, resourceResolver);
                }
            }
        } 
    }
    

    private boolean datesConfirmed(Node projectContentNode) throws RepositoryException {
            
            if (projectContentNode.hasNode("work")) {
                Node workNode = projectContentNode.getNode("work");
                NodeIterator it = workNode.getNodes();
                int i = 0;
                while (it.hasNext()) {
                    it.nextNode();
                    i++;
                    if (i>2) {
                        return true;
                    }
                }
            }
            return false;
    }

    private Node getProjectContentNode(Node altNode, ResourceResolver resourceResolver) throws RepositoryException {
        Node productNode = altNode.getParent();
        Node damFolderNode = productNode.getParent();
        if (damFolderNode.hasProperty("projectPath")) {
            Property projectPathProperty =  damFolderNode.getProperty("projectPath");
            String projectPath = "";
            if (projectPathProperty.isMultiple()) {
                Value[] values =  projectPathProperty.getValues();
                projectPath = values[0].getString();
            } else {
                projectPath = projectPathProperty.getString();
            }
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContent = projectNode.getNode("jcr:content");
            return jcrContent;
        }
        return null;
    }
}