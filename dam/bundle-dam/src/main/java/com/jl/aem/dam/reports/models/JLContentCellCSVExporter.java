package com.jl.aem.dam.reports.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.adobe.acs.commons.reports.api.ReportCellCSVExporter;
import com.jl.aem.dam.reports.models.results.JLContentItem;

@Model(adaptables = Resource.class)
public class JLContentCellCSVExporter implements ReportCellCSVExporter {

    @Inject
    private String property;

    @Inject
    @Optional
    private String format;

    @Override
    public String getValue(Object result) {
      JLContentItem resource = (JLContentItem) result;
      JLContentCellValue val = new JLContentCellValue(resource, property);
      return val.getValue();
    }
    
    
}
