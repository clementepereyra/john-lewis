package com.jl.aem.dam.workflow.validation;

import org.osgi.service.component.annotations.Component;

@Component(immediate=true, service = JLPManualValidator.class)
public class JLPManualValidator extends JLPCommonValidator implements JLPAssetValidator {

    @Override
    public boolean canValidate(String assetPath) {
        if (assetPath.toLowerCase().endsWith("mnl1.pdf")) {
            return true;
        }
        return false;
    }
    
    @Override
    public String validateAsset(String assetPath) {
        if (canValidate(assetPath)) {
            return JLPAssetValidator.SUCCESS_RESPONSE;
        }
        return "Not valid format";
    }
}
