package com.jl.aem.dam.workflow;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.jl.aem.dam.pim.Product;

public class JLWorkflowsUtils {

    private static final Logger logger = LoggerFactory.getLogger(JLWorkflowsUtils.class);

    private static final int ASSET_NEME_LONG = 9;

    public static String formatSupplierID(final String supplierContactDetail) {
        return supplierContactDetail.trim().replace(".", "-").toLowerCase();
    }

    public static String getSupplierIDForNode(final Node node) {
        Node tmpNode = node;
        try {
            while (!JLPConstants.CREATIVE_ASSETS_ROOT_FOLDER.equals(tmpNode.getPath())) {
                if (tmpNode.hasProperty("dam:ccTeamMembers")) {
                    Property property = tmpNode.getProperty("dam:ccTeamMembers");
                    String ccTeamMember = null;
                    if (property.isMultiple()) {
                        ccTeamMember = property.getValues()[0].getString();
                    } else {
                        ccTeamMember = property.getValues()[0].getString();
                    }
                    if (ccTeamMember.indexOf(":") >= 0) {
                        return ccTeamMember.substring(0, ccTeamMember.indexOf(":"));
                    } 
                    return ccTeamMember;
                }
                tmpNode = tmpNode.getParent();
            }
        } catch (RepositoryException e) {
            logger.error("Can not get supplierID");
            return null;
        }
        return null;
    }

    public static boolean isMainIMage(Node assetNode) throws RepositoryException {
        if (assetNode.getNode("jcr:content/metadata")
                .hasProperty(JLPConstants.JLP_IMAGE_CATEGORY)) {
            String imageCat = assetNode.getNode("jcr:content/metadata")
                    .getProperty(JLPConstants.JLP_IMAGE_CATEGORY).getString();
            if (imageCat.indexOf("/main") > 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isLogo(Node assetNode) throws RepositoryException {
        if (assetNode.getNode("jcr:content/metadata")
                .hasProperty(JLPConstants.JLP_IMAGE_CATEGORY)) {
            String imageCat = assetNode.getNode("jcr:content/metadata")
                    .getProperty(JLPConstants.JLP_IMAGE_CATEGORY).getString();
            if (imageCat.indexOf("/logo") > 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isImage(Node assetNode) throws RepositoryException {
        Node metadataNode = assetNode.getNode("jcr:content/metadata");
        String dcFormat = metadataNode.getProperty("dc:format").getString();
        return dcFormat.startsWith("image");
    }

    public static String encodePath(final String string) {
        if (string != null) {
            
            String sanitized = Normalizer.normalize(string, Form.NFD).trim().toLowerCase().replace(" ", "_").replace("'", "").replace("/", "_")
                    .replace("&", "_").replace("?","_").replace(",", "_").replace("*", "").replace(":","_");
            return Text.escapeIllegalJcrChars(sanitized);
        }
        return "N_A";
    }

    public static String addLeftZeros(final String itemId) {
        String tmpItemID = itemId;
        while (tmpItemID.length() < ASSET_NEME_LONG) {
            tmpItemID = "0" + tmpItemID;
        }
        return tmpItemID;
    }

    public static boolean isVideo(Node assetNode) throws RepositoryException {
        Node metadataNode = assetNode.getNode("jcr:content/metadata");
        String dcFormat = metadataNode.getProperty("dc:format").getString();
        return dcFormat.startsWith("video");
    }
    
    public static boolean isManual(final Node assetNode) throws RepositoryException {
        Node metadataNode = assetNode.getNode("jcr:content/metadata");
        String dcFormat = metadataNode.getProperty("dc:format").getString();
        return dcFormat.equalsIgnoreCase("application/pdf");
    }
    
    public static boolean is34Ratio(Node assetNode, ResourceResolver resourceResolver) throws RepositoryException {
        String productPath = assetNode.getNode("jcr:content/metadata").getProperty("cq:productReference").getString();
        Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
        Product product = populateProductFromNode(productNode);
        if (product.getProdTypeName().toLowerCase().contains("women's") || product.getProdTypeName().toLowerCase().contains("men's")) {
            return true;
        }
        return false;
    }
    
    
    public static Product populateProductFromNode(final Node productNode) {
        try {
           Product p = new Product();
           p.setProdCode(getPropertySafely(productNode, ProductConstants.PRODUCT_CODE));
           p.setBrandName(getPropertySafely(productNode, ProductConstants.BRAND_NAME));
           p.setColour(getPropertySafely(productNode, ProductConstants.COLOUR));
           p.setColourCode(getPropertySafely(productNode, ProductConstants.COLOUR_CODE));
           p.setColourGroup(getPropertySafely(productNode, ProductConstants.COLOUR_GROUP));
           p.setConfirmedDate(getPropertySafely(productNode, ProductConstants.CONFIRMED_DATE));
           p.setImageSource(getPropertySafely(productNode, ProductConstants.IMAGE_SOURCE));
           p.setModelNo(getPropertySafely(productNode, ProductConstants.MODEL_NO));
           p.setOnlineStatus(getPropertySafely(productNode, ProductConstants.ONLINE_STATUS));
           p.setOptionDesc(getPropertySafely(productNode, ProductConstants.OPTION_DESC));
           p.setPosDesc(getPropertySafely(productNode, ProductConstants.POS_DESC));
           p.setProdTypeName(getPropertySafely(productNode, ProductConstants.PROD_TYPE_NAME));
           p.setResearchDone(getPropertySafely(productNode, ProductConstants.RESEARCH_DONE));
           p.setSampleSource(getPropertySafely(productNode, ProductConstants.SAMPLE_SOURCE));
           p.setSelDesc(getPropertySafely(productNode, ProductConstants.SEL_DESC));
           p.setStatusId(getPropertySafely(productNode, ProductConstants.STATUS_ID));
           p.setSupplierContactDetail(getPropertySafely(productNode, ProductConstants.SUPPLIER_CONTACT_DETAIL));
           p.setTradedCode(getPropertySafely(productNode, ProductConstants.TRADED_CODE));
           if(productNode.hasProperty(ProductConstants.WSM_ITEM_ID)){
               p.setWsmItemId(productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString());
           }
           p.setUserIdUpd(getPropertySafely(productNode, ProductConstants.USER_ID_UPD));
           p.setWsmTitle(getPropertySafely(productNode, ProductConstants.WSM_TITLE));
           p.setDirectorate(getPropertySafely(productNode, ProductConstants.DIRECTORATE));
           p.setSeasonCode(getPropertySafely(productNode, ProductConstants.SEASON_CODE));
           p.setYear(getPropertySafely(productNode, ProductConstants.YEAR));
           p.setBuyingOffice(getPropertySafely(productNode, ProductConstants.BUYING_OFFICE));
           p.setLaunchDate(getPropertySafely(productNode, ProductConstants.LAUNCH_DATE));
           if(productNode.hasProperty(ProductConstants.STOCK_LEVEL)) {
               p.setDeleted(productNode.getProperty(ProductConstants.DELETED).getString());
           }
           if(productNode.hasProperty(ProductConstants.STOCK_LEVEL)) {
               p.setStockLevel(productNode.getProperty(ProductConstants.STOCK_LEVEL).getString());
           }
           p.setProductPath(productNode.getPath());
           List<String> briefs = new ArrayList<String>();
           if(productNode.hasProperty(ProductConstants.BRIEFS)) {
               Property briefsProperty = productNode.getProperty(ProductConstants.BRIEFS);
               if (briefsProperty.isMultiple()) {
                   for (Value value : briefsProperty.getValues()) {
                       briefs.add(value.getString());
                   }
               } else {
                   briefs.add(briefsProperty.getString());
               }
           }
           p.setBriefs(briefs);
           return p;
        }
        catch (Exception e) {
            logger.error("Error creating product object", e);  
        }
        return null;
    }
    
    private static String getPropertySafely(Node productNode, String property) {
        try {
            if (productNode.hasProperty(property)) {
                return productNode.getProperty(property).getString();
            }
        } catch (RepositoryException e) {
            logger.error("Can not retrieve property" + property);
            return "";
        }
        return "";
    }

    public static String getTimeDifference(Long dateFromMilliseconds, Long dateToMilliseconds) {
        String difference = "";
        Long zero = new Long(0);
        if(zero.compareTo(dateFromMilliseconds) != 0 && zero.compareTo(dateToMilliseconds) != 0) {
            Long diff = dateToMilliseconds - dateFromMilliseconds;
           Long days =  TimeUnit.MILLISECONDS.toDays(diff);
           diff = diff - 86400000 * days;
           Long hours = TimeUnit.MILLISECONDS.toHours(diff);
           if( zero.compareTo(days) == -1){
            difference = days + " days " + hours + " hours";
           } else {
               if(zero.compareTo(hours) == -1){
                   difference = hours + " hours " ;
               }
               diff = diff - 3600000 * hours;
               Long mins = TimeUnit.MILLISECONDS.toMinutes(diff);
               difference += mins + " mins" ;
           }
        }
        return difference;
    }
    
    public static void setAssetPropertiesFromProduct(Node asNode,Node productNode, ResourceResolver resourceResolver) {
        try {
            Node metadataNode = JcrUtils.getNodeIfExists(asNode, "jcr:content/metadata");

            metadataNode.setProperty("cq:productReference", productNode.getPath());
            metadataNode.setProperty(JLPConstants.JLP_PRODUCT_CODES,  productNode.getProperty("identifier").getString());
            metadataNode.setProperty(JLPConstants.JLP_PRODUCT_TITLE,  productNode.getProperty("wsmTitle").getString());
            metadataNode.setProperty(JLPConstants.JLP_DIRECTORATE,  getTagValue(productNode.getProperty("directorate").getString().replace("Directorate","").replace("directorate","").trim(), "directorate", resourceResolver));            metadataNode.setProperty(JLPConstants.JLP_SEASON, getTagValue(productNode.getProperty("seasonCode").getString(), "season", resourceResolver));
            metadataNode.setProperty(JLPConstants.JLP_YEAR,  productNode.getProperty("year").getString());
            metadataNode.setProperty(JLPConstants.JLP_SOURCE,  productNode.getProperty("imageSource").getString());
            metadataNode.setProperty(JLPConstants.JLP_SUPPLIER_CONTACT,  productNode.getProperty("supplierContactDetail").getString());
            metadataNode.setProperty(JLPConstants.JLP_BRAND_NAME, productNode.getProperty("brandName").getString());
            metadataNode.setProperty(JLPConstants.JLP_BUYING_OFFICE, getNestedTagValue(productNode.getProperty("buyingOffice").getString(), "buying-office", resourceResolver));
            if(productNode.hasProperty(ProductConstants.WSM_TITLE)) {
                metadataNode.setProperty(JLPConstants.JLP_WSM_TITLE, productNode.getProperty(ProductConstants.WSM_TITLE).getString());
            }
            if(productNode.hasProperty(ProductConstants.TRADED_CODE)) {
                metadataNode.setProperty(JLPConstants.JLP_EAN, productNode.getProperty(ProductConstants.TRADED_CODE).getString());
            }
            if(productNode.hasProperty(ProductConstants.COLOUR)) {
                metadataNode.setProperty(JLPConstants.JLP_COLOUR, productNode.getProperty(ProductConstants.COLOUR).getString());
            }
            if(productNode.hasProperty(ProductConstants.COLOUR_GROUP)) {
                metadataNode.setProperty(JLPConstants.JLP_COLOUR_GROUP, productNode.getProperty(ProductConstants.COLOUR_GROUP).getString());
            }
            if(productNode.hasProperty(ProductConstants.LAUNCH_DATE)) {
                metadataNode.setProperty(JLPConstants.JLP_LAUNCH_DATE, productNode.getProperty(ProductConstants.LAUNCH_DATE).getString());
            }
            if(productNode.hasProperty(ProductConstants.IMAGE_REQUESTED_DATE)) {
                metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_SUPPLIER_REQUESTED, productNode.getProperty(ProductConstants.IMAGE_REQUESTED_DATE).getString());
            }
            metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_SUPPLIER_UPLOADED, Calendar.getInstance());
            if(productNode.hasProperty("prodTypeName")){
                metadataNode.setProperty(JLPConstants.JLP_PRODUCT_TYPE,  productNode.getProperty("prodTypeName").getString());
            }
            if(productNode.hasProperty(ProductConstants.MODEL_NO)){
                metadataNode.setProperty(JLPConstants.JLP_MODEL_NO, productNode.getProperty(ProductConstants.MODEL_NO).getString());
            }
            if(productNode.hasProperty(ProductConstants.STOCK_LEVEL)){
                metadataNode.setProperty(JLPConstants.JLP_STOCK_LEVEL,  productNode.getProperty(ProductConstants.STOCK_LEVEL).getString());
            }
            if(productNode.hasProperty(ProductConstants.DELETED)){
                metadataNode.setProperty(JLPConstants.JLP_PROD_DELETED,  productNode.getProperty(ProductConstants.DELETED).getString());
            }
            
            if(metadataNode.hasProperty(JLPConstants.JLP_IMAGE_CATEGORY)){
                String imageCat = metadataNode.getProperty(JLPConstants.JLP_IMAGE_CATEGORY).getString().replace("jlp:image-category/alt/", "_").replace("jlp:image-category/","_");
                if(productNode.hasProperty(JLPConstants.ASSET_VALIDATION_FAILS + imageCat)){
                    metadataNode.setProperty(JLPConstants.ASSET_VALIDATION_FAILS, productNode.getProperty(JLPConstants.ASSET_VALIDATION_FAILS + imageCat).getLong());
                }
            }
            //if (StringUtils.isNotBlank(productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString())) {
            //    String itemId = productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString();
            //    itemId = addLeftZeros(itemId);
            //    metadataNode.setProperty(JLPConstants.JLP_ITEM_ID, itemId);
            //}
        } catch (Exception e) {
            logger.error("Can not set properties from product.", e);
        }
    }
    
    public static void setAssetPropertiesFromLookNode(Node asNode, Node lookNode,
            List<String> productsPath, ResourceResolver resourceResolver) {
        try {
            Node metadataNode = JcrUtils.getNodeIfExists(asNode, "jcr:content/metadata");

            if (metadataNode != null) {
                if (lookNode.hasProperty(JLPLifeStyleConstants.NAME)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.NAME,
                            lookNode.getProperty(JLPLifeStyleConstants.NAME).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME, lookNode
                            .getProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.NAMING_CONVENTION, lookNode
                            .getProperty(JLPLifeStyleConstants.NAMING_CONVENTION).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.SHOOT_DESCRIPTION)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.SHOOT_DESCRIPTION, lookNode
                            .getProperty(JLPLifeStyleConstants.SHOOT_DESCRIPTION).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.BUYING_OFFICE)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.BUYING_OFFICE,
                            lookNode.getProperty(JLPLifeStyleConstants.BUYING_OFFICE).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.CLIENT_STYLING_NOTES)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.CLIENT_STYLING_NOTES, lookNode
                            .getProperty(JLPLifeStyleConstants.CLIENT_STYLING_NOTES).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.RANGE)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.RANGE,
                            lookNode.getProperty(JLPLifeStyleConstants.RANGE).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.IDS)) {
                    metadataNode.setProperty(JLPLifeStyleConstants.IDS,
                            lookNode.getProperty(JLPLifeStyleConstants.IDS).getString());
                }
                if (lookNode.hasProperty(JLPLifeStyleConstants.JLP_PRODUCTS)) {
                    String[] products = productsPath.toArray(new String[productsPath.size()]);
                    metadataNode.setProperty(JLPLifeStyleConstants.JLP_PRODUCTS, products);
                }

                resourceResolver.commit();
            }

        } catch (Exception e) {
            logger.error("Can not set properties from Look Node.", e);
        }
    }

    public static String getNestedTagValue(final String value, final String tagGroup, final ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource("/etc/tags/jlp/" + tagGroup );
        Iterator<Resource> it = resource.listChildren();
        while (it.hasNext()) {
            Resource childResource = it.next();
            Iterator<Resource> childIt = childResource.listChildren();
            while (childIt.hasNext()) {
                Resource nestedChildResource = childIt.next();
                Tag childTag = nestedChildResource.adaptTo(Tag.class);
                if (childTag.getTitle().equalsIgnoreCase(value)) {
                    return childTag.getTagID();
                }
            }
        }
        return value;
    }

    public static String getBuyingGrupByOffice(final String value, final ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource("/etc/tags/jlp/buying-office");
        Iterator<Resource> it = resource.listChildren();
        while (it.hasNext()) {
            Resource childResource = it.next();
            Iterator<Resource> childIt = childResource.listChildren();
            Tag tag = childResource.adaptTo(Tag.class);
            while (childIt.hasNext()) {
                Resource nestedChildResource = childIt.next();
                Tag childTag = nestedChildResource.adaptTo(Tag.class);
                if (childTag.getTitle().equalsIgnoreCase(value)) {
                    return tag.getTitle();
                }
            }
        }
        return "";
    }
    
    public static String getTagValue(final String value, final String tagGroup, final ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource("/etc/tags/jlp/" + tagGroup );
        Iterator<Resource> it = resource.listChildren();
        while (it.hasNext()) {
            Resource childResource = it.next();
            Tag childTag = childResource.adaptTo(Tag.class);
            if (childTag.getTitle().equalsIgnoreCase(value)) {
                return childTag.getTagID();
            }
        }
        return value;
    }

    public static boolean isFolder(Node baseShootNode) throws RepositoryException {
        return baseShootNode.getPrimaryNodeType().getName().toLowerCase().contains("folder");
    }

}
