package com.jl.aem.dam.workflow;

public enum AssetStatus {

    ASSET_NEW ("New Asset"),
    ASSET_RE_REQUESTED ("Asset Re-Requested"),
    ASSET_PENDING_IC ("Asset Pending Image Coordinator Review"),
    ASSET_REJECTED_SIPE ("Asset Rejected SIPE"),
    ASSET_AVAILABLE_FOR_ARTWORKING ("Asset Available For Artworking"),
    ASSET_PENDING_ARTWORKING ("Asset Pending Artworking"),
    ASSET_ARTWORKING_REQUESTED ("Asset Artworking Requested"),
    ASSET_SUBMITTED_FOR_FINAL_APPROVAL ("Submitted for Final Approval"),
    FINAL_ASSET_APPROVED ("Final Asset Approved"),
    FINAL_ASSET_REJECTED ("Final Asset Rejected"),
    ARTWORK_REJECTED("Artwork Rejected");

    private final String displayLabel;
    
    private AssetStatus(final String theDisplayLabel) {
        this.displayLabel = theDisplayLabel;
    }
    
    public String getDisplayLabel() {
        return displayLabel;
    }
}
