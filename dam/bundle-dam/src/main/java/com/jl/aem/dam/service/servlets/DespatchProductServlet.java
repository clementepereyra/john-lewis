package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/DespatchProduct",
    "sling.servlet.methods=POST"
  }
)
public class DespatchProductServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 4904472777745815851L;

	private static final Logger log = LoggerFactory.getLogger(DespatchProductServlet.class);

    @Reference
    private SlingRepository repository;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private QueryBuilder builder;


    public void bindRepository(SlingRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        try {
            String productPath = request.getRequestParameter("path").toString();            
            
            Resource productResource = resourceResolver.getResource(productPath);
            Node productNode = productResource.adaptTo(Node.class);
            

            long currentQuantity = productNode.hasProperty("quantityReceived")?productNode.getProperty("quantityReceived").getLong():0l;
            String quantity = request.getRequestParameter("quantity").toString();
            
            Node historyNode = productNode.hasNode("quantityHistory")?productNode.getNode("quantityHistory"):productNode.addNode("quantityHistory", "nt:unstructured");
            
            Node newEntry = historyNode.addNode("despatch"+ System.currentTimeMillis());

            newEntry.setProperty("quantityReceived", "-" + quantity);
            newEntry.setProperty("type", "despatch"); 
            newEntry.setProperty("user", session.getUserID());
            Calendar cal = Calendar.getInstance();
            newEntry.setProperty("date", cal);
            UserManager userManager = productResource.getResourceResolver().adaptTo(UserManager.class);
            User user = (User) userManager.getAuthorizable(session.getUserID());
            String lastName = user.getProperty("./profile/familyName")!=null?user.getProperty("./profile/familyName")[0].getString():"";
            String firstName = user.getProperty("./profile/givenName")!=null?user.getProperty("./profile/givenName")[0].getString():"";        
            newEntry.setProperty("userName", firstName + " " + lastName);
            
            productNode.setProperty("quantityReceived", currentQuantity - Long.parseLong(quantity));
            productResource.getResourceResolver().commit();
                
            
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in DespatchProductServlet ", e);
            
        }
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        try {
            String productPath = request.getRequestParameter("path").toString();            
            String[] paths = productPath.split("/");
            String productStockNumber = paths[paths.length-1];
            
            Resource productResource = resourceResolver.getResource(productPath);
            Node productNode = productResource.adaptTo(Node.class);
            
            List<String> briefList = new ArrayList<String>();
            if (productStockNumber != null) {
                try {                	  
                	QueryManager queryManager = session.getWorkspace().getQueryManager();
                    javax.jcr.query.Query query = queryManager.createQuery("SELECT brief.* " + 
                    		"FROM [sling:Folder] AS brief " + 
                    		"INNER JOIN [sling:Folder] AS alt ON ISCHILDNODE(alt,brief) " + 
                    		"WHERE ISDESCENDANTNODE(brief, '/content/dam/projects') " + 
                    		"AND brief.[stockNumber] = '" + productStockNumber + 
                    		"' AND alt.[dam:JLPBriefAssetStatus] = 'ASSET_REQUESTED'", javax.jcr.query.Query.JCR_SQL2);
                    QueryResult result = query.execute();
                    if (result.getRows().getSize() != 0) {
                    	if(result.getRows().hasNext()){
                    		briefList.add(result.getRows().nextRow().getPath().split("/")[4]);
                     	}
                    }
                    
                } catch (Exception e) {
                  log.error("Error despatching the product", e);
                }
           }
            if(!briefList.isEmpty()){
            	for (String brief : briefList) {
            		response.getWriter().write(brief + "\n");
				}                
                response.getWriter().flush();
            } else {
                long currentQuantity = productNode.hasProperty("quantityReceived")?productNode.getProperty("quantityReceived").getLong():0l;
                String qtyParam = request.getParameter("quantity");
                long quantity = 0;
                if (qtyParam != null) {
                    quantity = Long.parseLong(qtyParam);
                }
                if (currentQuantity <= 0) {
                    response.getWriter().write("Quantity is 0");
                } else if (quantity > currentQuantity) {
                    response.getWriter().write("Current quantity is " + currentQuantity);
                }
            }
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in DespatchProductServlet ", e);
        }
    }    
}