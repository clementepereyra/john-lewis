package com.jl.aem.dam.pim.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.pim.ProductService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/update_product",
    "sling.servlet.methods=POST"
  }
)
public class UpdateProductServlet extends SlingAllMethodsServlet{

    /**
     * 
     */
    private static final long serialVersionUID = 1946503157051545195L;
    private static final Logger logger = LoggerFactory.getLogger(UpdateProductServlet.class);

    @Reference
    private ProductService productService;
    
    private static Gson gson = new Gson();

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Product product = getProductFromRequest(request);
        if (product != null) {
            String prodCode = product.getProdCode();
            if (StringUtils.isBlank(product.getProdCode())) {
                UpdateProductResponse resp = new UpdateProductResponse(400, "Product Code can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getDirectorate())) {
                UpdateProductResponse resp = new UpdateProductResponse(400, "Directorate can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getYear())) {
                UpdateProductResponse resp = new UpdateProductResponse(400, "Year can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getSeasonCode())) {
                UpdateProductResponse resp = new UpdateProductResponse(400, "Season Code can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
            if (StringUtils.isBlank(product.getBrandName())) {
                UpdateProductResponse resp = new UpdateProductResponse(400, "Brand Name can not be empty", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(400);
                return;
            }
           
            try {
                if (productService.exist(product.getProdCode())) {
                    productService.updateProduct(product);
                    UpdateProductResponse resp = new UpdateProductResponse(200, "", prodCode);
                    response.getWriter().write(gson.toJson(resp));
                    return;
                } else {
                    UpdateProductResponse resp = new UpdateProductResponse(404, "Product does not exist", prodCode);
                    response.getWriter().write(gson.toJson(resp));
                    response.setStatus(404);
                    return;
                }
            } catch (Exception e) {
                UpdateProductResponse resp = new UpdateProductResponse(500, "Internal error updating product", prodCode);
                response.getWriter().write(gson.toJson(resp));
                response.setStatus(500);
                return;
            }

            
        } else {
            UpdateProductResponse resp = new UpdateProductResponse(400, "Can not parse json object from request body - malformed json body", "");
            response.setStatus(400);
            response.getWriter().write(gson.toJson(resp));
        }
    }
    
    private Product getProductFromRequest(SlingHttpServletRequest request) {
        try {
            BufferedReader reader = request.getReader();
            Gson gson = new Gson();
            Product product = gson.fromJson(reader, Product.class);
            return product;
        } catch (Exception e) {
            logger.error("Can not parse product from request body - aborting", e);
            return null;
        }
    }

    public static class UpdateProductResponse {
        private int status;
        private String statusDescription;
        private String error;
        private String prod_code;
        
        public UpdateProductResponse(int status, String error, String prodCode) {
            this.status = status;
            statusDescription = "ERROR";
            if (status < 400) {
                statusDescription = "SUCCESS";
            }
            this.prod_code = prodCode;
            this.error = error;
        }
        
        public int getStatus() {
            return status;
        }
        
        public String getError() {
            return error;
        }
        
        public String getProdCode() {
            return prod_code;
        }
        
        public String getStatusDescription() {
            return statusDescription;
        }
        
    }
    
}
