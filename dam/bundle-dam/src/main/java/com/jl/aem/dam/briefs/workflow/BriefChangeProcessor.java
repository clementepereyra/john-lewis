package com.jl.aem.dam.briefs.workflow;

public interface BriefChangeProcessor {

    void process(String projectContentPath, String userId);
}
