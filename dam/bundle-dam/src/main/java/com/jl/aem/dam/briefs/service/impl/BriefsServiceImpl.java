package com.jl.aem.dam.briefs.service.impl;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.security.AccessControlManager;
import javax.jcr.security.AccessControlPolicy;
import javax.jcr.security.AccessControlPolicyIterator;
import javax.jcr.security.Privilege;
import javax.jcr.version.VersionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.JackrabbitAccessControlList;
import org.apache.jackrabbit.api.security.JackrabbitAccessControlManager;
import org.apache.jackrabbit.api.security.principal.PrincipalManager;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.Workflow;
import com.day.cq.audit.AuditLog;
import com.day.cq.audit.AuditLogEntry;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.workflow.WorkflowService;
import com.jl.aem.dam.briefs.service.AssetTaskCompleterService;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.CSVBriefHelper;
import com.jl.aem.dam.briefs.service.CutoutBrief;
import com.jl.aem.dam.briefs.service.CutoutBrief.CutoutBriefRow;
import com.jl.aem.dam.briefs.service.GuideStyleUtils;
import com.jl.aem.dam.briefs.service.LifestyleBrief;
import com.jl.aem.dam.briefs.service.LifestyleBrief.LifestyleBriefRow;
import com.jl.aem.dam.briefs.service.RetouchBrief;
import com.jl.aem.dam.briefs.service.RetouchBrief.RetouchBriefRow;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.briefs.workflow.ProjectWorkflowInfo;
import com.jl.aem.dam.briefs.workflow.impl.ProjectRoleHelper;
import com.jl.aem.dam.briefs.workflow.impl.ProjectWorkflowHelper;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.pim.ProductService;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.jl.aem.dam.workflow.ProductStatus;
import com.jl.aem.dam.workflow.step.helper.JLProductAssociator;

@Component(immediate=true, service=BriefsService.class)
public class BriefsServiceImpl implements BriefsService {


    private static final Logger log = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private ProductService productService;

    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private WorkflowService workflowService;
    
    @Reference
    private JLPWorkflowSettings settings;
    
    @Reference
    private AuditLog auditLog;

    private static String ASSET_AUDITLOG_PATH = "com/day/cq/dam";
    
    private static final Pattern IMAGE_CAT_PATTERN = Pattern.compile("(main|alt\\d*)");
    
    private static final String RESHOOT_PREFIX = "need_reshoot";
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";
    
    @Override
    public void importCutoutBriefCSV(final String projectPath, final String filename) {
       
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            Node collectionsFolderNode = resourceResolver.getResource("/content/dam/collections").adaptTo(Node.class);
            Node dashboardNode = jcrContentNode.getNode("dashboard");
            Node gadgetsNode = dashboardNode.getNode("gadgets");
            
            String csvPath = JLPConstants.CSV_IMPORT_PATH + "/" + filename;
            Resource csvResource = resourceResolver.getResource(csvPath);
            Asset csvAsset = csvResource.adaptTo(Asset.class);
            
            Session session = resourceResolver.adaptTo(Session.class);
            JackrabbitSession jrSession = (JackrabbitSession) session;
            PrincipalManager principalManager = jrSession.getPrincipalManager();
            JackrabbitAccessControlManager acm = (JackrabbitAccessControlManager) session.getAccessControlManager();
            
            CutoutBrief brief = CSVBriefHelper.parseCutoutBriefCSV(csvAsset);
            
            if (brief.isValid()) {
                for (CutoutBriefRow row : brief.getRows()) {
                    createProductFolderInDAM(projectDamFolderNode, row, projectPath, resourceResolver);
                    Node collectionNode = createProductCollectionNode(row.getStockNumber(), projectNode.getName(), damFolderPath, collectionsFolderNode);
                    createCollectionTile(collectionNode, gadgetsNode);
                    setACLsToCollection(collectionNode, projectNode, acm, principalManager);
                }
                projectDamFolderNode.addNode(JLPConstants.REFERENCE_FOLDER, "sling:Folder");
                Node briefCollectionNode = createBriefCollectionNode(projectNode.getName(), jcrContentNode.getProperty("jcr:title").getString(), damFolderPath, collectionsFolderNode);
                createCollectionTile(briefCollectionNode, gadgetsNode);
                setACLsToCollection(briefCollectionNode, projectNode, acm, principalManager);
                resourceResolver.commit();
            } else {
                jcrContentNode.setProperty("project.csverror", Boolean.TRUE);
                resourceResolver.commit();
            }
            archiveCSVFile(csvResource, damFolderPath);
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not parse csv", e);
            try {
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                Node jcrContentNode = projectNode.getNode("jcr:content");
                jcrContentNode.setProperty("project.csverror", Boolean.TRUE);
                resourceResolver.commit();
            } catch (Exception e1) {
                log.error("Fatal Error - Can not store csv error status on brief", e);
            }
            throw new RuntimeException("CSV not properly formed", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    @Override
    public void importLifestyleBriefCSV(final String projectPath, final String filename) {
        
       
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            
            Node collectionsFolderNode = resourceResolver.getResource("/content/dam/collections").adaptTo(Node.class);
            Node dashboardNode = jcrContentNode.getNode("dashboard");
            Node gadgetsNode = dashboardNode.getNode("gadgets");
           
            String csvPath = JLPConstants.CSV_IMPORT_PATH + "/" + filename;
            Resource csvResource = resourceResolver.getResource(csvPath);
            Asset csvAsset = csvResource.adaptTo(Asset.class);
            
            Session session = resourceResolver.adaptTo(Session.class);
            JackrabbitSession jrSession = (JackrabbitSession) session;
            PrincipalManager principalManager = jrSession.getPrincipalManager();
            JackrabbitAccessControlManager acm = (JackrabbitAccessControlManager) session.getAccessControlManager();
            
            LifestyleBrief brief = CSVBriefHelper.parseLifestyleBriefCSV(csvAsset);
            projectDamFolderNode.addNode(JLPConstants.REFERENCE_FOLDER, "sling:Folder");

            for (LifestyleBriefRow row : brief.getRows()) {
                createLooksFolderInDAM(projectDamFolderNode, row, projectPath, resourceResolver);
                
                Node collectionNode = createProductCollectionNode(row.getNamingConvention(), projectNode.getName(), damFolderPath, collectionsFolderNode);
                createCollectionTile(collectionNode, gadgetsNode);
                setACLsToCollection(collectionNode, projectNode, acm, principalManager);
            }
            
            Node briefCollectionNode = createBriefCollectionNode(projectNode.getName(), jcrContentNode.getProperty("jcr:title").getString(), damFolderPath, collectionsFolderNode);
            createCollectionTile(briefCollectionNode, gadgetsNode);
            setACLsToCollection(briefCollectionNode, projectNode, acm, principalManager);
            
            resourceResolver.commit();
            archiveCSVFile(csvResource, damFolderPath);
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not parse csv", e);
            try {
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                Node jcrContentNode = projectNode.getNode("jcr:content");
                jcrContentNode.setProperty("project.csverror", Boolean.TRUE);
                resourceResolver.commit();
            } catch (Exception e1) {
                log.error("Fatal Error - Can not store csv error status on brief", e);
            }
            throw new RuntimeException("CSV not properly formed", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public void importRetouchBriefCSV(final String projectPath, final String filename) {
       
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            Node collectionsFolderNode = resourceResolver.getResource("/content/dam/collections").adaptTo(Node.class);
            Node dashboardNode = jcrContentNode.getNode("dashboard");
            Node gadgetsNode = dashboardNode.getNode("gadgets");
            
            String csvPath = JLPConstants.CSV_IMPORT_PATH + "/" + filename;
            Resource csvResource = resourceResolver.getResource(csvPath);
            Asset csvAsset = csvResource.adaptTo(Asset.class);
            
            Session session = resourceResolver.adaptTo(Session.class);
            JackrabbitSession jrSession = (JackrabbitSession) session;
            PrincipalManager principalManager = jrSession.getPrincipalManager();
            JackrabbitAccessControlManager acm = (JackrabbitAccessControlManager) session.getAccessControlManager();
            
            RetouchBrief brief = CSVBriefHelper.parseRetouchBriefCSV(csvAsset);
            
            if (brief.isValid()) {
                for (RetouchBriefRow row : brief.getRows()) {
                    createProductFolderInDAMForRetouch(projectDamFolderNode, row, projectPath, resourceResolver);
                    Node collectionNode = createProductCollectionNode(row.getStockNumber(), projectNode.getName(), damFolderPath, collectionsFolderNode);
                    createCollectionTile(collectionNode, gadgetsNode);
                    setACLsToCollection(collectionNode, projectNode, acm, principalManager);
                }
                projectDamFolderNode.addNode(JLPConstants.REFERENCE_FOLDER, "sling:Folder");
                Node briefCollectionNode = createBriefCollectionNode(projectNode.getName(), jcrContentNode.getProperty("jcr:title").getString(), damFolderPath, collectionsFolderNode);
                createCollectionTile(briefCollectionNode, gadgetsNode);
                setACLsToCollection(briefCollectionNode, projectNode, acm, principalManager);
                resourceResolver.commit();
            } else {
                jcrContentNode.setProperty("project.csverror", Boolean.TRUE);
                resourceResolver.commit();
            }
            archiveCSVFile(csvResource, damFolderPath);
            resourceResolver.commit();
            String workflowTitle = "Retouch Brief Workflow";
            String workflowNodeName = "retouch-brief-workflow";
            String workflowModel = JLPConstants.RETOUCH_WORKFLOW_ID;
            ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName, workflowModel, jcrContentNode, session.getUserID());
            ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService, resourceResolver);
            jcrContentNode.setProperty("startNotified", true);
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not parse csv", e);
            try {
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                Node jcrContentNode = projectNode.getNode("jcr:content");
                jcrContentNode.setProperty("project.csverror", Boolean.TRUE);
                resourceResolver.commit();
            } catch (Exception e1) {
                log.error("Fatal Error - Can not store csv error status on brief", e);
            }
            throw new RuntimeException("CSV not properly formed", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
 
    private void setProductProperties(final Node asNode, ResourceResolver resourceResolver) {
        try {
            Node altNode = asNode.getParent();
            Node prodNode = altNode.getParent();
            Node projectFolderNode = prodNode.getParent();

                    
            String projectPath = null;
            if (projectFolderNode.hasProperty(JLPConstants.JLP_PROJECT_PATH)) {
                Property projectPathProperty =
                        projectFolderNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
                if (projectPathProperty.isMultiple()) {
                    Value[] values = projectPathProperty.getValues();
                    projectPath = values[0].getString();
                } else {
                    projectPath = projectPathProperty.getString();
                }

                if (projectPath != null) {
                    Node projectContentNode = resourceResolver
                            .getResource(projectPath + "/jcr:content").adaptTo(Node.class);

                    String endUse = null;
                    if (projectContentNode.hasProperty(JLPConstants.JLP_BRIEF_END_USE)) {
                        endUse = projectContentNode.getProperty(JLPConstants.JLP_BRIEF_END_USE)
                                .getString();
                        Node assetMetadataNode = asNode.getNode("jcr:content/metadata");

                        if (endUse != null && assetMetadataNode != null) {
                            assetMetadataNode.setProperty(JLPConstants.JLP_DAM_END_USE, endUse);
                            resourceResolver.commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Setting Asset properties from project", e);
        }
    }

    @Override
    public void ingestCutoutAsset(final String assetPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            if (assetPath.indexOf("/" + JLPConstants.COMPOSITE_FOLDER + "/") > 0 
                    || (assetPath.indexOf("/" + JLPConstants.REFERENCE_FOLDER + "/") > 0)) {
                return;
            }
            Node asNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            String productCode = getProductCode(asNode);
            String altCode = getAltCode(asNode);
            Product product = productService.getProduct(productCode);
            if (product != null) {
                String productPath = product.getProductPath();
                Map<String, String> productInfo = new HashMap<>();
                productInfo.put("productCode", productCode);
                productInfo.put("productPath", productPath);
                productInfo.put("imageCat", getImageCategory(altCode));
                JLProductAssociator associator = new JLProductAssociator();
                associator.associateAssetWithProduct(asNode, resourceResolver, session, productInfo);                
            }
            setProductProperties(asNode, resourceResolver); 
            resourceResolver.commit();
            
            if (isColourPendingAsset(asNode.getParent())) {
                processNewStatus(BriefUtils.getProjectPath(asNode), productCode,
                        altCode, BriefAssetStatus.ASSET_RETOUCHED);
            } else {
            
                if ("main".equals(altCode)) {
                    altCode = "";
                }
                String targetFolder = asNode.getParent().getPath();
                String extension = asNode.getName().substring(asNode.getName().lastIndexOf("."));
                String targetFileName = JLWorkflowsUtils.addLeftZeros((product != null && !StringUtils.isBlank(product.getWsmItemId())) ? product.getWsmItemId() : productCode) + altCode + extension; 
                
                
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("targetFolder", targetFolder );
                metaData.put("targetFileName", targetFileName);
                metaData.put("conflictResolution", "keepboth");
                workflowInvoker.startWorkflow(JLPConstants.MOVE_ASSET_AND_AUTOCOMPLETE_WORKFLOW, asNode.getPath(), metaData);
            }

        } catch (Exception e) {
            log.error("Error ingesting cutout asset", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
        
    }
    
    private boolean isColourPendingAsset(Node altNode) throws Exception {
        if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
            String statusStr = altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString();
            BriefAssetStatus status = BriefAssetStatus.valueOf(statusStr);
            if (status == BriefAssetStatus.PENDING_COLOUR_MATCH) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setupShootTasks(final String projectPath) {
        ResourceResolver resourceResolver =  getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContent = projectNode.getNode("jcr:content");
            Node damFolderNode = resourceResolver.getResource(
                    jcrContent.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            NodeIterator rowsIterator = damFolderNode.getNodes(); 
            while (rowsIterator.hasNext()) {
                Node baseShootNode = rowsIterator.nextNode();
                if (JLWorkflowsUtils.isFolder(baseShootNode)
                        && !"jcr:content".equals(baseShootNode.getName())
                        && !JLPConstants.REFERENCE_FOLDER.equals(baseShootNode.getName())) {
                    setupProductTasks(resourceResolver, jcrContent, baseShootNode);
                }
            }
        } catch (Exception e) {
            log.error("Can not start shoot tasks", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private void setupProductTasks(ResourceResolver resourceResolver, Node jcrContent,
            Node productNode) throws RepositoryException {
        String baseShootId = productNode.getName();
        NodeIterator altIterator = productNode.getNodes();
        while (altIterator.hasNext()) {
            Node altNode = altIterator.nextNode();
            if (JLWorkflowsUtils.isFolder(productNode)
                    && !"jcr:content".equals(altNode.getName())) {
                startWorkflowForAltNode(resourceResolver, jcrContent, baseShootId, altNode.getName());
            }
        }
    }

    @Override
    public void startWorkflowForAltNodeInProject(String projectPath, String itemId, String altName) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContent = projectNode.getNode("jcr:content");
            startWorkflowForAltNode(resourceResolver, jcrContent, itemId, altName);
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Error ingesting cutout asset", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
        
    }
    
    private void startWorkflowForAltNode(ResourceResolver resourceResolver, Node jcrProjectContent,
            String baseShootId, String altName) throws RepositoryException {
        String workflowTitle = baseShootId + "-" + altName;
        String workflowNodeName = "provide-shoot-"+ baseShootId + "-" + altName;
        ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName,
                BriefUtils.getSingleAssetWorkflowModel(jcrProjectContent), jcrProjectContent, "admin");
        ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService, resourceResolver);
    }

    @Override
    public void processNewStatus(String projectPath, String productCode, String altCode,
            BriefAssetStatus assetStatus) {        
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContent = projectNode.getNode("jcr:content");
            Property briefname = null;
            if (jcrContent.hasProperty("jcr:title")) {
                briefname = jcrContent.getProperty("jcr:title");
            }
            Property deliveryDate = null;
            if (jcrContent.hasProperty("project.dueDate")) {
                deliveryDate = jcrContent.getProperty("project.dueDate");
            }    
            Node damFolderNode = resourceResolver.getResource(
                    jcrContent.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            Node productNode = damFolderNode.hasNode(productCode)?damFolderNode.getNode(productCode):null;
            if (productNode == null) {
                NodeIterator productNodes = damFolderNode.getNodes();
                while (productNodes.hasNext()) {
                    Node candidateNode = productNodes.nextNode();
                    if (candidateNode.hasProperty("ean")) {
                        if (productCode.equals(candidateNode.getProperty("ean").getString())) {
                            productNode = candidateNode;
                            break;
                        }
                    }
                }
            }
            Node altNode = productNode.getNode(altCode);
            altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, assetStatus.toString());   
            NodeIterator children = altNode.getNodes();
            while (children.hasNext()) {
                Node assetNode = children.nextNode();
                if (assetNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                    Node metdata = assetNode.getNode("jcr:content/metadata");
                    if (!isReShootAsset(assetNode)) {
                        metdata.setProperty(
                                JLPConstants.JLP_BRIEF_ASSET_STATUS, assetStatus.toString());
                        if (deliveryDate != null) {
                            metdata.setProperty(JLPConstants.JLP_BRIEF_DELIVERY_DATE, deliveryDate.getValue());
                        }
                        if (briefname != null) {
                            metdata.setProperty(JLPConstants.JLP_BRIEF_NAME, briefname.getValue());
                        }
                        if (assetStatus == BriefAssetStatus.ASSET_RETOUCHED && BriefUtils.isCutoutBrief(jcrContent)) {
                            setProductAvailable(assetNode, resourceResolver);
                        }
                    }
                }
            }          
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Error ingesting cutout asset", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
        
    }
    
    private void setProductAvailable(Node assetNode, ResourceResolver resourceResolver) {
        try {
            Node metadata = assetNode.getNode("jcr:content/metadata");
            if (JLWorkflowsUtils.isMainIMage(assetNode) && metadata.hasProperty("cq:productReference")) {
                String productPath = metadata.getProperty("cq:productReference").getString();
                Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
                productNode.setProperty("statusId", "Image Available");
            }  
            
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_TIME, Calendar.getInstance());
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_USER, "system");
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Can not update product status",e);
        }
    }

    @Override
    public void setReshootNeeded(String projectPath, String productCode, String altCode) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContent = projectNode.getNode("jcr:content");
            Node damFolderNode = resourceResolver.getResource(
                    jcrContent.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            Node productNode = damFolderNode.hasNode(productCode)?damFolderNode.getNode(productCode):null;
            if (productNode == null) {
                NodeIterator productNodes = damFolderNode.getNodes();
                while (productNodes.hasNext()) {
                    Node candidateNode = productNodes.nextNode();
                    if (candidateNode.hasProperty("ean")) {
                        if (productCode.equals(candidateNode.getProperty("ean").getString())) {
                            productNode = candidateNode;
                            break;
                        }
                    }
                }
            }
            
            Node altNode = productNode.getNode(altCode);
            if (altNode.hasProperty(JLPConstants.RETOUCH_MODE)) {
                altNode.getProperty(JLPConstants.RETOUCH_MODE).remove();
            }
            altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.RE_SHOOT_NEEDED.toString());
            NodeIterator children = altNode.getNodes();
            while(children.hasNext()) {
                Node assetNode = children.nextNode();
                if (assetNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                    if (!isReShootAsset(assetNode)) {
                        assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.RE_SHOOT_NEEDED.toString());
                        resourceResolver.commit();
                        renameAssetForReshoot(assetNode);
                    }
                }
            }
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Error ingesting cutout asset", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
    }
    
    private void renameAssetForReshoot(Node assetNode) throws RepositoryException {
        String targetFolder = assetNode.getParent().getPath();
        String targetFileName = RESHOOT_PREFIX + assetNode.getName(); 
        Map<String, Object> metaData = new HashMap<>();
        metaData.put("targetFolder", targetFolder );
        metaData.put("targetFileName", targetFileName);
        metaData.put("conflictResolution", "keepboth");

        workflowInvoker.startWorkflow(JLPConstants.MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
    }

    private boolean isReShootAsset(Node assetNode) throws RepositoryException {
       return assetNode.getName().indexOf(RESHOOT_PREFIX) >= 0;
    }

    private String getImageCategory(final String altCode) {
        StringBuilder imageCategory = new StringBuilder(JLPConstants.IMAGE_CAT_ROOT);
        if (altCode.contains("alt")) {
           imageCategory.append("alt/").append(altCode);
        } else if (altCode.contains("mnl")) {
            imageCategory.append("mnl");
        } else if (altCode.contains("vid")) {
            imageCategory.append("vid/").append(altCode);
        } else {
            imageCategory.append("main/");
        }
        return imageCategory.toString();
    }

    private String getAltCode(final Node asNode) throws RepositoryException {
        boolean  limit = false;
        Node temp = asNode.getParent();
        while(!limit) {
            String path = temp.getName().toLowerCase();
            Matcher matcher = IMAGE_CAT_PATTERN.matcher(path);
            if (matcher.matches()) {
                String altCode = matcher.group(1);
                return altCode;
            } else {
                temp = temp.getParent();
                if (temp == null) {
                    limit = true;
                }
            }
        }
        
        return "main"; //default to main if parent is not found
    }

    private String getProductCode(final Node asNode) throws RepositoryException {
        return getLookNodeFolder(asNode).getName();
    }
    
    private Node getLookNodeFolder(final Node asNode) throws RepositoryException {
        boolean  limit = false;
        Node temp = asNode.getParent();
        while(!limit) {
            String path = temp.getName().toLowerCase();
            Matcher matcher = IMAGE_CAT_PATTERN.matcher(path);
            if (matcher.matches()) {
                Node productFolder = temp.getParent();
                return productFolder;
            } else {
                temp = temp.getParent();
                if (temp == null) {
                    limit = true;
                }
            }
        }  
        return asNode; //default to asset node name.
    }

    @Override
    public void ingestLifestyleAsset(String assetPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        if (assetPath.indexOf("/" + JLPConstants.COMPOSITE_FOLDER + "/") > 0) {
            return;
        }
        try {
            Node asNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            String altCode = getAltCode(asNode);
            Node lookNode = getLookNodeFolder(asNode);
            String namingConvention = null;
            
            if (lookNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)) {
                namingConvention = lookNode.getProperty(JLPLifeStyleConstants.NAMING_CONVENTION).getString();
            }

            List<String> productsPath = new ArrayList<String>();
            if (lookNode != null && lookNode.hasProperty(JLPLifeStyleConstants.JLP_PRODUCTS)) {
                Property productsProperty =
                        lookNode.getProperty(JLPLifeStyleConstants.JLP_PRODUCTS);
                if (productsProperty.isMultiple()) {
                    for (Value value : productsProperty.getValues()) {
                        productsPath.add(value.getString());
                    }
                } else {
                    productsPath.add(productsProperty.getString());
                }
            }

            if (productsPath.size() > 0) {
                JLProductAssociator associator = new JLProductAssociator();
                associator.associateAssetWithProducts(productsPath, asNode, lookNode,
                        getImageCategory(altCode), resourceResolver);
                setProductProperties(asNode, resourceResolver);
            }
            String targetFolder = asNode.getParent().getPath();
            String extension = asNode.getName().substring(asNode.getName().lastIndexOf("."));
            String targetFileName = (namingConvention != null ? namingConvention
                    : StringUtils.EMPTY) + altCode + extension;    
            
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder);
            metaData.put("targetFileName", targetFileName);
            metaData.put("conflictResolution", "keepboth");
            workflowInvoker.startWorkflow(JLPConstants.MOVE_ASSET_AND_AUTOCOMPLETE_WORKFLOW, asNode.getPath(), metaData);
        
        } catch (Exception e) {
            log.error("Error ingesting LifeStyle asset", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }

    }

    private Node createProductFolderInDAM(final Node projectDamFolderNode, final CutoutBriefRow row,
            final String projectPath, ResourceResolver resourceResolver) throws RepositoryException {
        
        Product product = productService.getProduct(row.getStockNumber());
        String prodCode = product!=null?product.getProdCode():row.getStockNumber();
        if (!projectDamFolderNode.hasNode(prodCode)) {
            Node prodNode = projectDamFolderNode.addNode(prodCode, "sling:Folder");
            prodNode.addNode("jcr:content", "nt:unstructured");
            prodNode.setProperty("stockNumber", prodCode);
            prodNode.setProperty("category", row.getCategory());
            prodNode.setProperty("clientStylingNotes", row.getClientStylingNotes());
            prodNode.setProperty("ids", row.getIDS());
            prodNode.setProperty("imageRequirements", row.getImageRequirements());
            prodNode.setProperty("productType", row.getStyleGuideProductType());
            prodNode.setProperty("range", row.getRange());
            String productProdTypeName = null;
            if (product!=null) {
                productProdTypeName = product.getProdTypeName();
                populateProductData(projectPath, prodNode, product);
                storeBriefIntoProductInfo(projectPath, resourceResolver, product);
            }
            Node altNode = prodNode.addNode("main", "sling:Folder");
            altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
            altNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
            altNode.addNode("jcr:content", "nt:unstructured");
            int alts = GuideStyleUtils.getNumberOfAlts(productProdTypeName, resourceResolver) + 1;
            for (int j = 1; j < alts ; j++) {
                altNode = prodNode.addNode("alt"+j, "sling:Folder");
                altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
                altNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
                altNode.addNode("jcr:content", "nt:unstructured");
            }
            
            return prodNode;
        }
        return projectDamFolderNode.getNode(prodCode);
    }
    
    private Node createProductFolderInDAMForRetouch(final Node projectDamFolderNode, final RetouchBriefRow row,
            final String projectPath, ResourceResolver resourceResolver) throws RepositoryException, PersistenceException {
        String prodCode = row.getStockNumber();
        if (!projectDamFolderNode.hasNode(prodCode)) {
            Node prodNode = projectDamFolderNode.addNode(prodCode, "sling:Folder");
            prodNode.setProperty("stockNumber", prodCode);
            prodNode.setProperty("clientStylingNotes", row.getComments());
            prodNode.setProperty("productType", row.getProdType());
            Product product = productService.getProduct(prodCode);
            String productProdTypeName = null;
            if (product!=null) {
                productProdTypeName = product.getProdTypeName();

            }
            populateProductData(projectPath, prodNode, product);
           // Map<String, Node> currentAssets = getAssetsForRetouch(row.getStockNumber(), resourceResolver);
            
            Node altNode = prodNode.addNode("main", "sling:Folder");
            altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
            altNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
            altNode.addNode("jcr:content", "nt:unstructured");

//            Node assetNode = currentAssets.get("main");
//            if (assetNode != null) {
//                BriefUtils.reopenAssetForRetouch(assetNode.getPath(), altNode.getPath(), resourceResolver);
//            }
            
            int alts = GuideStyleUtils.getNumberOfAlts(productProdTypeName, resourceResolver) + 1;
            for (int j = 1; j < alts ; j++) {
                String altName = "alt" + j;                
                altNode = prodNode.addNode(altName, "sling:Folder");
                altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
                altNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
                altNode.addNode("jcr:content", "nt:unstructured");
//                assetNode = currentAssets.get(altName);
//                if (assetNode != null) {
//                    BriefUtils.reopenAssetForRetouch(assetNode.getPath(), altNode.getPath(), resourceResolver);
//                }
            }
            
            return prodNode;
        }
        return projectDamFolderNode.getNode(prodCode);
    }

    private void populateProductData(final String projectPath, Node prodNode, Product product) throws ValueFormatException, VersionException,
            LockException, ConstraintViolationException, RepositoryException {
        if (product != null) {
            prodNode.setProperty("productName", product.getWsmTitle());
            prodNode.setProperty("webSku", product.getWsmItemId());
            prodNode.setProperty("description", product.getSelDesc());
            prodNode.setProperty("ean", product.getTradedCode());
            prodNode.setProperty("status", "unavailable");
            prodNode.setProperty("supplier", product.getSupplierContactDetail());
            prodNode.setProperty("colour", product.getColour());
            //itemNode.setProperty("size", product.get)
            prodNode.setProperty("brand", product.getBrandName());
            prodNode.setProperty("productPath", product.getProductPath());
            prodNode.setProperty("buyingOffice", product.getBuyingOffice());
            //itemNode.setProperty("dissection", arg1);
            //itemNode.setProperty("material", arg1);
        } else {
            prodNode.setProperty(JLPConstants.JLP_RECONCILE_PRODUCT, "true");
        }
    }
    
    
//  Used to be for populating existing assets for retouch.
//    private Map<String, Node> getAssetsForRetouch(String stockNumber, ResourceResolver resourceResolver) throws RepositoryException{
//        Map<String, Node> assetMap = new HashMap<>();
//        Session session = resourceResolver.adaptTo(Session.class);
//        QueryManager queryManager = session.getWorkspace().getQueryManager();
//        javax.jcr.query.Query query = queryManager.createQuery("SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE(s,'" + JLPConstants.JOHNLEWIS_SCENE7_FOLDER + "') AND s.[jcr:content/metadata/dam:JLPProductCodes] = '"+stockNumber+"'", javax.jcr.query.Query.JCR_SQL2);
//        QueryResult queryResult = query.execute();
//        NodeIterator results = queryResult.getNodes();
//        while (results.hasNext()) {
//            Node hit = results.nextNode();
//            String hitName = hit.getName();
//            if (hitName.indexOf("alt")>=0) {
//                String alt = hitName.substring(hitName.indexOf("alt"), hitName.lastIndexOf("."));
//                assetMap.put(alt, hit);
//            } else {
//                assetMap.put("main", hit);
//            }
//        }
//        return assetMap;
//    }

    private Node createLooksFolderInDAM(final Node lookDamFolderNode, final LifestyleBriefRow row  ,
            final String projectPath, ResourceResolver resourceResolver) throws RepositoryException {
    	
    	String lookName = row.getNamingConvention();
    	
    	if(!lookDamFolderNode.hasNode(lookName)){
    		Node lookNode = lookDamFolderNode.addNode(lookName,"sling:Folder");
            lookNode.addNode("jcr:content", "nt:unstructured");
    		lookNode.setProperty(JLPLifeStyleConstants.NAME, row.getLifestyleName());
    		lookNode.setProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME, row.getShootname());
    		lookNode.setProperty(JLPLifeStyleConstants.NAMING_CONVENTION, row.getNamingConvention());
    		lookNode.setProperty(JLPLifeStyleConstants.SHOOT_DESCRIPTION, row.getShotDescription());
    		lookNode.setProperty(JLPLifeStyleConstants.BUYING_OFFICE, row.getBuyingOffice());
    		String buyingGroup = JLWorkflowsUtils.getBuyingGrupByOffice(row.getBuyingOffice(), resourceResolver);
    		lookNode.setProperty(JLPLifeStyleConstants.DIRECTORATE, getDirectorate(buyingGroup, resourceResolver));
    		lookNode.setProperty(JLPLifeStyleConstants.CLIENT_STYLING_NOTES, row.getClientNotes());
    		lookNode.setProperty(JLPLifeStyleConstants.RANGE, row.getRange());
    		lookNode.setProperty(JLPLifeStyleConstants.IDS, row.getIDS());
    		
    		List<String> productPaths = new ArrayList<>();
    		
    		for (String productCode : row.getProducts()) {
    		    Product product = productService.getProduct(productCode);
                if(product != null) {
                	productPaths.add(product.getProductPath());
                	storeBriefIntoProductInfo(projectPath, resourceResolver, product);
                }
    		}
    		lookNode.setProperty(JLPConstants.DAM_JLP_PRODUCTS, productPaths.toArray(new String[productPaths.size()]));
            Node altNode;
            int totalAlts = 4;
            String numberOfAlts = row.getAltNumber();
            if(numberOfAlts != null && !numberOfAlts.isEmpty()) {
            	totalAlts = Integer.parseInt(numberOfAlts);
            }
            for (int j = 0; j<totalAlts; j++) {
                altNode = lookNode.addNode("alt"+ (10 + j), "sling:Folder");
                altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
                altNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
                altNode.addNode("jcr:content", "nt:unstructured");
            }
            return lookNode;
    	}
        return null;
    }

    private void storeBriefIntoProductInfo(final String projectPath,
            ResourceResolver resourceResolver, Product product) {
        if(!product.getBriefs().contains(projectPath)) {
        	product.getBriefs().add(projectPath);
        	try {
            	Resource productResource = resourceResolver.getResource(product.getProductPath());
            	if (productResource != null) {
            	    Node productNode = productResource.adaptTo(Node.class);
                    String[] briefs = product.getBriefs().toArray(new String[product.getBriefs().size()]);
                    productNode.setProperty(ProductConstants.BRIEFS,  briefs);
                    resourceResolver.commit();
            	}
        	} catch (Exception e) {
        	    log.error("Could not store brief info on product node", e);
        	}
        }
    }
    


    private String getDirectorate(String buyingGroup, ResourceResolver resourceResolver) {
        String directorate = "Fashion";
        if (buyingGroup !=  null) {
            if (buyingGroup.equalsIgnoreCase("Communication-Tech") 
                    || buyingGroup.equalsIgnoreCase("Electricals")
                    || buyingGroup.equalsIgnoreCase("Beauty Wbeing&Lsure"))  {
                directorate = "EHT";
            } else if (buyingGroup.equalsIgnoreCase("Gifts Cook & Dine") 
                    || buyingGroup.equalsIgnoreCase("Furniture & Flooring")
                    || buyingGroup.equalsIgnoreCase("Textiles & Home Accs")) {
                directorate = "Home";
            }
        }
        return directorate; 
    }

    private Node createProductCollectionNode(final String prodCodeOrLookName, final String briefName, final String damFolderPath, final Node collectionsFolderNode) {
        try {
            Node collectionsProjectNode = JcrUtils.getOrAddNode(collectionsFolderNode, "projects", JcrConstants.NT_UNSTRUCTURED);
            Node briefCollectionNode = JcrUtils.getOrAddNode(collectionsProjectNode, briefName, JcrConstants.NT_UNSTRUCTURED);
            Node collectionNode = JcrUtils.getOrAddNode(briefCollectionNode, prodCodeOrLookName, JcrConstants.NT_UNSTRUCTURED);
            
            String querytemplate = "path={path}\n" 
                    + "type=dam:Asset\n" 
                    + "property=jcr:content/metadata/" + JLPConstants.JLP_BRIEF_ASSET_STATUS + "\n"
                    + "property.operation=exists\n"                    
                    + "p.guessTotal=true\n" 
                    + "savedSearches@Delete=\n" 
                    + "mainasset=true\n" 
                    + "createsmartcollection=true\n" 
                    + "location=asset\n";
            
            collectionNode.setProperty("dam:query", querytemplate.replace("{path}", damFolderPath+"/"+prodCodeOrLookName));
            collectionNode.setProperty("jcr:created", Calendar.getInstance());
            collectionNode.setProperty("jcr:createdBy", collectionsFolderNode.getSession().getUserID());
            collectionNode.setProperty("jcr:title", "Assets for " + prodCodeOrLookName);
            collectionNode.setProperty("sling:resourceSuperType", "sling/collection");
            collectionNode.setProperty("sling:resourceType", "dam/smartcollection");
            return collectionNode;
        } catch (Exception e) {
            log.error("Error creating collection for product", e);
        }
        return null;
    }
    
    private Node createBriefCollectionNode(final String briefName, final String briefTitle, final String damFolderPath, final Node collectionsFolderNode) {
        try {
            Node collectionsProjectNode = JcrUtils.getOrAddNode(collectionsFolderNode, "projects", JcrConstants.NT_UNSTRUCTURED);
            Node briefCollectionNode = JcrUtils.getOrAddNode(collectionsProjectNode, briefName, JcrConstants.NT_UNSTRUCTURED);
            Node collectionNode = JcrUtils.getOrAddNode(briefCollectionNode, briefName, JcrConstants.NT_UNSTRUCTURED);
            
            String querytemplate = "path={path}\n" 
                    + "type=dam:Asset\n" 
                    + "property=jcr:content/metadata/" + JLPConstants.JLP_BRIEF_ASSET_STATUS + "\n"
                    + "property.operation=exists\n"
                    + "p.guessTotal=true\n" 
                    + "savedSearches@Delete=\n" 
                    + "mainasset=true\n" 
                    + "createsmartcollection=true\n" 
                    + "location=asset\n"
                    + "orderby=@jcr:content/metadata/"+ JLPConstants.JLP_PRODUCT_CODES;
            
            collectionNode.setProperty("dam:query", querytemplate.replace("{path}", damFolderPath));
            collectionNode.setProperty("jcr:created", Calendar.getInstance());
            collectionNode.setProperty("jcr:createdBy", collectionsFolderNode.getSession().getUserID());
            collectionNode.setProperty("jcr:title", "Assets for " + briefTitle);
            collectionNode.setProperty("sling:resourceSuperType", "sling/collection");
            collectionNode.setProperty("sling:resourceType", "dam/smartcollection");
            return collectionNode;
        } catch (Exception e) {
            log.error("Error creating collection for product", e);
        }
        return null;
    }
    
    private void createCollectionTile(final Node collectionNode, final Node gadgetsNode) {
        try {
            Node tile = gadgetsNode.addNode("collection_" + collectionNode.getName(), JcrConstants.NT_UNSTRUCTURED);
            tile.setProperty("cardWeight", "90");
            tile.setProperty("collectionPath", collectionNode.getPath());
            tile.setProperty("jcr:description", "Asset Collection Tile");
            tile.setProperty("jcr:lastModified", collectionNode.getSession().getUserID());
            tile.setProperty("jcr:lastModifiedBy", Calendar.getInstance());
            tile.setProperty("jcr:title", "Asset Collection");
            tile.setProperty("sling:resourceType", "cq/gui/components/projects/admin/pod/collectionspod");
        } catch (Exception e) {
            log.error("Error creating tile for collection", e);
        }
        
    }
    
    private void setACLsToCollection(final Node collectionNode, final Node projectNode,
            final JackrabbitAccessControlManager acm, final PrincipalManager principalManager) {
        try {
            String targetPath = collectionNode.getPath();
            JackrabbitAccessControlList targetAcl = getAcl(acm, targetPath);
            
            ProjectRoleHelper projectRoleHelper = new ProjectRoleHelper(projectNode);
            
            Privilege[] privileges = new
                    Privilege[]{acm.privilegeFromName(Privilege.JCR_ALL)};
            targetAcl.addEntry(principalManager.getPrincipal("contributor"), privileges, false, null);
    
            Principal principal = principalManager.getPrincipal(projectRoleHelper.getActivityCoordinator());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getExternalRetoucher());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getOwner());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getPhotographer());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getPostProductionCoordinator());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getRetoucher());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getShootProducer());
            targetAcl.addEntry(principal, privileges, true, null);
            principal = principalManager.getPrincipal(projectRoleHelper.getStockCoordinator());
            targetAcl.addEntry(principal, privileges, true, null);
    
    
            acm.setPolicy(targetPath, targetAcl);
        } catch (Exception e) {
            log.error("Can't set collection ACLs", e);
        }
    }

    JackrabbitAccessControlList getAcl(final AccessControlManager acm, final String path) {
        try {
            AccessControlPolicyIterator app = acm.getApplicablePolicies(path);
            while (app.hasNext()) {
                AccessControlPolicy pol = app.nextAccessControlPolicy();
                if (pol instanceof JackrabbitAccessControlList) {
                    return (JackrabbitAccessControlList) pol;
                }
            }
            // no found...check if present
            for (AccessControlPolicy pol : acm.getPolicies(path)) {
                if (pol instanceof JackrabbitAccessControlList) {
                    return (JackrabbitAccessControlList) pol;
                }
            }
        } catch (RepositoryException e) {
            log.warn("Error while retrieving ACL for {}: {}", path, e.toString());
        }
        return null;
    }


    private void archiveCSVFile(final Resource csvResource, final String damFolderPath) {
        try {
            Resource parent = csvResource.getParent();
            Resource fileResource = parent.getChild("file");
            if (fileResource!= null) {
                Node fileNode = fileResource.adaptTo(Node.class);
                fileNode.remove();
            }
        } catch (Exception e) {
            log.error("Can not delete file", e);
        }
        String targetFolder = damFolderPath;
        String targetFileName = csvResource.getName(); 
        Map<String, Object> metaData = new HashMap<>();
        metaData.put("targetFolder", targetFolder );
        metaData.put("targetFileName", targetFileName);

        workflowInvoker.startWorkflow(JLPConstants.MOVE_ASSET_WORKFLOW, csvResource.getPath(), metaData);
    }
    
    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }


    @Override
    public void addCompositeFolder(String path) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource pathResource = resourceResolver.getResource(path);
            if (pathResource != null) {
                Node pathNode = pathResource.adaptTo(Node.class);                
                if (isFolder(pathNode) && !pathNode.hasNode(JLPConstants.COMPOSITE_FOLDER)) {
                    pathNode.addNode(JLPConstants.COMPOSITE_FOLDER, "sling:Folder");
                    resourceResolver.commit();
                }
            }
        } catch (Exception e) {
            log.error("Can't create composite folder", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        
    }
    
    @Override
    public String addProductToCutoutBrief(String projectPath, String productCode, String category,
            String imageRequirement, String stylingGuide, String range, String ids, String clientNotes) {
        
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            Node collectionsFolderNode = resourceResolver.getResource("/content/dam/collections").adaptTo(Node.class);
            Node dashboardNode = jcrContentNode.getNode("dashboard");
            Node gadgetsNode = dashboardNode.getNode("gadgets");
            Session session = resourceResolver.adaptTo(Session.class);
            JackrabbitSession jrSession = (JackrabbitSession) session;
            PrincipalManager principalManager = jrSession.getPrincipalManager();
            JackrabbitAccessControlManager acm = (JackrabbitAccessControlManager) session.getAccessControlManager();
            
            
            if (projectDamFolderNode.hasNode(productCode)) {
                return "ERROR - Product already exist";
            }
            
            if (isProductDuplicatedInBriefs(productCode)) {
            	return "WARNING - The product is already present in an open brief";
            }
            
            CutoutBriefRow row = new CutoutBriefRow();
            row.setCategory(category);
            row.setImageRequirements(imageRequirement);
            row.setStyleGuideProductType(stylingGuide);
            row.setStockNumber(productCode);
            row.setIDS(!ids.isEmpty() ? ids:"");
            row.setRange(!range.isEmpty() ? range:"");
            row.setClientStylingNotes(!clientNotes.isEmpty() ? clientNotes:"");

            Node productNode = createProductFolderInDAM(projectDamFolderNode, row, projectPath, resourceResolver);
            Node collectionNode = createProductCollectionNode(row.getStockNumber(), projectNode.getName(), damFolderPath, collectionsFolderNode);
            createCollectionTile(collectionNode, gadgetsNode);
            setACLsToCollection(collectionNode, projectNode, acm, principalManager);
            if (isShootStartDateConfirmed(jcrContentNode)) {
                setupProductTasks(resourceResolver, jcrContentNode, productNode);
            }
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not add product", e);
            throw new RuntimeException("Can not add product - " +e.getMessage(), e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "SUCCESS";
    }
    
    @Override
    public String addLookToLifestyleBrief(String projectPath, String lookName, String namingConvention,
            String shotDescription, String buyingOffice, String range, String ids,
            String clientNotes, int noOfAlts, List<String> products) {

        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            Node collectionsFolderNode = resourceResolver.getResource("/content/dam/collections").adaptTo(Node.class);
            Node dashboardNode = jcrContentNode.getNode("dashboard");
            Node gadgetsNode = dashboardNode.getNode("gadgets");
            Session session = resourceResolver.adaptTo(Session.class);
            JackrabbitSession jrSession = (JackrabbitSession) session;
            PrincipalManager principalManager = jrSession.getPrincipalManager();
            JackrabbitAccessControlManager acm = (JackrabbitAccessControlManager) session.getAccessControlManager();
            
            
            if (projectDamFolderNode.hasNode(namingConvention)) {
                return "ERROR - Look already exist";
            }
            
            LifestyleBriefRow row = new LifestyleBriefRow();
            
            row.setBuyingOffice(buyingOffice);
            row.setLifestyleName(projectDamFolderNode.getName());
            row.setProducts(products);
            row.setAltNumber(""+ noOfAlts);
            row.setShootname(lookName);
            row.setShotDescription(shotDescription);
            row.setIDS(!ids.isEmpty() ? ids:"");
            row.setRange(!range.isEmpty() ? range:"");
            row.setClientNotes(!clientNotes.isEmpty() ? clientNotes:"");
            row.setNamingConvention(namingConvention);

            Node productNode = createLooksFolderInDAM(projectDamFolderNode, row, projectPath, resourceResolver);
            Node collectionNode = createProductCollectionNode(row.getNamingConvention(), projectNode.getName(), damFolderPath, collectionsFolderNode);
            createCollectionTile(collectionNode, gadgetsNode);
            setACLsToCollection(collectionNode, projectNode, acm, principalManager);
            if (isShootStartDateConfirmed(jcrContentNode)) {
                setupProductTasks(resourceResolver, jcrContentNode, productNode);
            }
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not add product", e);
            throw new RuntimeException("Can not add product - " +e.getMessage(), e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "SUCCESS";
    }
    
    @Override
    public String addProductToLifestyleLook(String projectPath, String lookName,
            String productCode) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Product product = productService.getProduct(productCode);
            if (product != null) {
                String productPath = product.getProductPath();
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                Node jcrContentNode = projectNode.getNode("jcr:content");
                String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
                Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
                if (projectDamFolderNode.hasNode(lookName)) {
                    //First updates the look placeholder
                    Node lookNode = projectDamFolderNode.getNode(lookName);
                    if (lookNode == null) {
                        return "ERROR - Look does not exist";
                    }
                    List<String> products = new ArrayList<String>();
                    if (lookNode.hasProperty(JLPConstants.DAM_JLP_PRODUCTS)) {
                        Value[] values = lookNode.getProperty(JLPConstants.DAM_JLP_PRODUCTS).getValues();
                        for (Value value : values) {
                            products.add(value.getString());
                        }
                    }
                    if (products.indexOf(productPath) < 0) {
                        products.add(productPath);
                    }
                    lookNode.setProperty(JLPConstants.DAM_JLP_PRODUCTS, products.toArray(new String[products.size()]));
                    
                    // Second update each sub-asset metadata
                    List<String> assetPaths = new ArrayList<>();
                    NodeIterator altFolderIt = lookNode.getNodes();
                    while (altFolderIt.hasNext()) {
                        Node altFolderNode = altFolderIt.nextNode();
                        if (altFolderNode.getName().toLowerCase().indexOf("alt") >= 0) {
                            NodeIterator assetsIt = altFolderNode.getNodes();
                            while (assetsIt.hasNext()) {
                                Node assetNode = assetsIt.nextNode();
                                if (assetNode.hasNode("jcr:content/metadata")) {
                                    Node metadata = assetNode.getNode("jcr:content/metadata");
                                    metadata.setProperty(JLPConstants.DAM_JLP_PRODUCTS, products.toArray(new String[products.size()]));
                                    assetPaths.add(assetNode.getPath());
                                }
                            }
                        }
                    }
                    //Finally update the product info with all the new assets
                    if (assetPaths.size() > 0) {
                        Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
                        Node assets = JcrUtils.getNodeIfExists(productNode, "assets");
    
                        if (assets == null) {
                            log.info("Create product assets folder product: '" + productNode.getPath()
                                    + "'");
                            assets = productNode.addNode("assets", "nt:unstructured");
                        }
    
                        for (String assetPath : assetPaths) {
                            Node assetNode =
                                    assets.addNode("asset" + UUID.randomUUID(), "nt:unstructured");
                            assetNode.setProperty("fileReference", assetPath);
                            assetNode.setProperty("sling:resourceType",
                                    "commerce/components/product/image");
                            assetNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            assetNode.setProperty("jcr:lastModifiedBy", "JLP-service");
                        }
                    }
                }
                resourceResolver.commit();
            } else {
                return "ERROR - Product does not exist";
            }
        } catch (Exception e) {
            log.error("Fatal Error - Can not add product", e);
            throw new RuntimeException("Can not add product - " +e.getMessage(), e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "SUCCESS";
    }

    private boolean isShootStarted(Node jcrContentNode) throws RepositoryException {
        if (jcrContentNode.hasProperty("project.shootstartDate") && 
                !StringUtils.isBlank(jcrContentNode.getProperty("project.shootstartDate").getString())) {
            Calendar startDate = jcrContentNode.getProperty("project.shootstartDate").getDate();
            if (Calendar.getInstance().after(startDate) || Calendar.getInstance().equals(startDate)) {
                return true;
            }
        }
        return false;
    }

    private boolean isShootStartDateConfirmed(Node jcrContentNode) throws RepositoryException {
        if (jcrContentNode.hasNode("work")) {
            Node workNode = jcrContentNode.getNode("work");
            NodeIterator it = workNode.getNodes();
            int i = 0;
            while (it.hasNext()) {
                it.nextNode();
                i++;
                if (i>2) {
                    return true;
                }
            }
        }
        return false;
    }




    private boolean isFolder(Node pathNode) throws RepositoryException{
        return pathNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0;
    }

    @Override
    public void publishBriefToScene7(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            

            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");

            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode =
                    resourceResolver.getResource(damFolderPath).adaptTo(Node.class);

            NodeIterator projectDamChildrens = projectDamFolderNode.getNodes();
            while (projectDamChildrens.hasNext()) {
                Node productFolderNode = projectDamChildrens.nextNode();
                deliverProductToScene7(productFolderNode.getPath());
            }
        } catch (Exception e) {
            log.error("Can not publish assets for project: " + projectPath, e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    @Override
    public void deliverProductToScene7(String productFolderPath)  {
        Calendar cal = Calendar.getInstance();
        String month = new SimpleDateFormat("MMM").format(cal.getTime());
        String year = new SimpleDateFormat("YYYY").format(cal.getTime());
        
        ResourceResolver resourceResolver = getResourceResolver();
        
        try {
            Node productFolderNode = resourceResolver.getResource(productFolderPath).adaptTo(Node.class);
            String projectPath = BriefUtils.getProjectPathFromProductNode(productFolderNode);
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if (productFolderNode.getPrimaryNodeType().getName().equals("sling:Folder")) {
                String targetPath = null;
                if (productFolderNode.hasProperty("productPath")) {
                    String productPath = productFolderNode.getProperty("productPath").getString();
                    Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
       
                    targetPath = JLPConstants.JOHNLEWIS_SCENE7_FOLDER + "/" + year + "/"
                            + month + "/"
                            + JLWorkflowsUtils.encodePath(productNode.getProperty(ProductConstants.DIRECTORATE).getString())
                            + "/"
                            + JLWorkflowsUtils.encodePath(productNode.getProperty(ProductConstants.BUYING_OFFICE).getString());
                } else {
                    if (productFolderNode.hasProperty(JLPLifeStyleConstants.BUYING_OFFICE)
                            && productFolderNode.hasProperty(JLPLifeStyleConstants.DIRECTORATE)) {
                        targetPath = JLPConstants.JOHNLEWIS_SCENE7_FOLDER + "/" + year + "/"
                                + month + "/"
                                + JLWorkflowsUtils.encodePath(productFolderNode.getProperty(JLPLifeStyleConstants.DIRECTORATE).getString())
                                + "/"
                                + JLWorkflowsUtils.encodePath(productFolderNode.getProperty(JLPLifeStyleConstants.BUYING_OFFICE).getString());
                    }
                }
                if (targetPath != null) {
                    NodeIterator productFolderChildrens = productFolderNode.getNodes();
                    while (productFolderChildrens.hasNext()) {
                        Node assetFolderNode = productFolderChildrens.nextNode();
                        if (assetFolderNode.getPrimaryNodeType().getName().equals("sling:Folder")
                                && assetFolderNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)
                                && BriefAssetStatus.ASSET_RETOUCHED.toString()
                                        .equals(assetFolderNode
                                                .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)
                                                .getString())) {
       
                            NodeIterator assetFolderNodeChildrens = assetFolderNode.getNodes();
                            while (assetFolderNodeChildrens.hasNext()) {
                                Node assetNode = assetFolderNodeChildrens.nextNode();
                                if (assetNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                                    if (assetNode.getNode("jcr:content/metadata").hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)){
                                        BriefAssetStatus assetStatus = BriefAssetStatus.valueOf(
                                                assetNode
                                                    .getNode("jcr:content/metadata")
                                                    .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)
                                                    .getString()
                                        );
                                        if (assetStatus.equals(BriefAssetStatus.ASSET_RETOUCHED)) {
                                            if (BriefUtils.isCutoutBrief(jcrContentNode)) {
                                                completeProductForAsset(assetNode, resourceResolver);
                                            }
                                            createAuditLog(assetNode, "Asset Delivered");
                                            assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_DELIVERED.toString());
                                            assetNode.getParent().setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_DELIVERED.toString());
                                            resourceResolver.commit();
                                            log.info("Publishing " + assetNode.getPath() + " to scene7 on path" + targetPath);
                                            Map<String, Object> metaData = new HashMap<>();
                                            metaData.put("targetFolder", targetPath);
                                            metaData.put("targetFileName", assetNode.getName());
                                            metaData.put("conflictResolution", "replace");
                                            metaData.put("mode", "copy");
                                            // we tell to publish to scene7 once moving is done
                                            metaData.put("nextWorkflow", JLPConstants.SCENE7_WORKFLOW);
                                            workflowInvoker.startWorkflow(JLPConstants.MOVE_ASSET_WORKFLOW,
                                                    assetNode.getPath(), metaData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    productFolderNode.setProperty("dam:JLPDelivered", true);
                    resourceResolver.commit();
                }
            }
        } catch (Exception e) {
            log.error("Error delivering product", e);
        } finally {
           if (resourceResolver != null) {
               resourceResolver.close();
           } 
        }
    }


    private void createAuditLog(Node assetNode, String eventType) {
        try {
            AuditLogEntry ae = new AuditLogEntry(ASSET_AUDITLOG_PATH,
                    new Date(),
                    assetNode.getSession().getUserID(),
                    assetNode.getPath(),
                    eventType,
                    null);           
               auditLog.add(ae);
        } catch (Exception e) {
            log.error("Error creating log audit", e);
        }
    }

    private void completeProductForAsset(Node assetNode, ResourceResolver resourceResolver) throws RepositoryException, PersistenceException {
        Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
        metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_TIME, Calendar.getInstance());
        metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_USER, "system");
        if (JLWorkflowsUtils.isMainIMage(assetNode) && metadataNode != null && metadataNode.hasProperty("cq:productReference")) {
            String productPath = metadataNode.getProperty("cq:productReference").getString();
            Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            productNode.setProperty("statusId", ProductStatus.IMAGE_COMPLETED.toString());
        }
        resourceResolver.commit();
    }

    @Override
    public void completeOfflineBrief(String projectPath) {
        // TODO Auto-generated method stub

    }

    @Override
    public void triggerReleaseWorkflow(final String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node contentNode = projectNode.getNode("jcr:content");
            String workflowTitle = null;
            String workflowNodeName = null;
            String workflowModel = null;
            if (BriefUtils.isCutoutBrief(contentNode)) {
                 workflowTitle = "CutOut Brief Completion Workflow";
                 workflowNodeName = "cutout-brief-completion-workflow";
                 workflowModel = JLPConstants.CUTOUT_COMPLETION_WORKFLOW_MODEL;
            } else if (BriefUtils.isLifestyleBrief(contentNode)) {
                 workflowTitle = "Lifestyle Brief Completion Workflow";
                 workflowNodeName = "lifestyle-brief-completion-workflow";
                 workflowModel = JLPConstants.LIFESTYLE_COMPLETION_WORKFLOW_MODEL;
            } else {
                 workflowTitle = "Retouch Brief Completion Workflow";
                 workflowNodeName = "retiycg-brief-completion-workflow";
                 workflowModel = JLPConstants.RETOUCH_COMPLETION_WORKFLOW_MODEL;
            }
            
            ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle, workflowNodeName, workflowModel, contentNode, "admin");
            ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService, resourceResolver);
        } catch (Exception e) {
            log.error("Can not create workflow: " + projectPath, e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public String changeBriefStatus(String projectPath, String status, String comments, String userID) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if ("CANCELLED".equals(status) && isShootStarted(jcrContentNode)) {
                return "ERROR - Project already started, can not cancel";
            }

            jcrContentNode.setProperty(JLPConstants.PROJECT_STATUS, status);
            jcrContentNode.setProperty(JLPConstants.PROJECT_COMMENTS, comments);
            jcrContentNode.setProperty(JLPConstants.PROJECT_COMPLETE_USER, userID);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            
            jcrContentNode.setProperty(JLPConstants.PROJECT_COMPLETE_DATE, sdf.format(date));
            resourceResolver.commit();
        } catch (Exception e) {
            log.error("Fatal Error - Can not create product", e);
            throw new RuntimeException("Can not create product - " +e.getMessage(), e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "SUCCESS";
    }
    
    private void setAltNotNeededStatus(String projectPath, ResourceResolver resourceResolver)
            throws PathNotFoundException, RepositoryException, PersistenceException {
        NodeIterator assetFolderNodeIt = getAssetFolderNodeIt(projectPath, resourceResolver);
        if (assetFolderNodeIt != null) {
            while (assetFolderNodeIt.hasNext()) {
                Node assetFolderNode = assetFolderNodeIt.nextNode();
                if (assetFolderNode.getPrimaryNodeType().getName().equals("sling:Folder")
                        && assetFolderNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                    BriefAssetStatus assetStatus = BriefAssetStatus.valueOf(assetFolderNode
                            .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                    if (!BriefAssetStatus.ASSET_RETOUCHED.equals(assetStatus)) {
                        assetFolderNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS,
                                BriefAssetStatus.ALT_NOT_NEEDED.toString());
                        NodeIterator assetNodeIt = assetFolderNode.getNodes();
                        while (assetNodeIt.hasNext()) {
                            Node assetNode = assetNodeIt.nextNode();
                            if (assetNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                                if (metadataNode != null) {
                                    metadataNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS,
                                            BriefAssetStatus.ALT_NOT_NEEDED.toString());
                                }
                            }
                        }
                    }
                }
            }
            resourceResolver.commit();
        }
    }

    @Override
    public String forceCompleteProject(String projectPath, String status, String comments,
            String userID) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            WorkflowSession wfSession = resourceResolver.adaptTo(WorkflowSession.class);

            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if (jcrContentNode != null) {
                //change brief status to complete
                jcrContentNode.setProperty(JLPConstants.PROJECT_STATUS, status);
                jcrContentNode.setProperty(JLPConstants.PROJECT_COMMENTS, comments);
                jcrContentNode.setProperty(JLPConstants.PROJECT_COMPLETE_USER, userID);
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");               
                jcrContentNode.setProperty(JLPConstants.PROJECT_COMPLETE_DATE, sdf.format(date));
                resourceResolver.commit();
                
                //set AltNotNeeded Status on assets folders and assets
                setAltNotNeededStatus(projectPath, resourceResolver);

                // Complete single assets workflows with ACTIVE related task state (228-12).
                Node taskGroupNode = jcrContentNode.getNode("tasks");
                NodeIterator taskGroupChildIterator = taskGroupNode.getNodes();
                while (taskGroupChildIterator.hasNext()) {
                    Node dateNode = taskGroupChildIterator.nextNode();
                    NodeIterator taskIterator = dateNode.getNodes();

                    while (taskIterator.hasNext()) {
                        Node taskNode = taskIterator.nextNode();
                        if (JLPConstants.PROVIDE_SHOOT_CUTOUT_WORKFLOW_NAME.equals(
                                taskNode.getProperty(JLPConstants.WORKFLOW_MODEL_ID).getString())
                                && JLPConstants.ACTIVE_WORKFLOW_STATUS.equals(taskNode
                                        .getProperty(JLPConstants.WORKFLOW_STATUS).getString())) {
                            String workflowId = taskNode
                                    .getProperty(JLPConstants.WORKFLOW_INSTACE_ID).getString();
                            Workflow workflow = wfSession.getWorkflow(workflowId);
                            wfSession.terminateWorkflow(workflow);
                        }
                    }
                }
                
                //Complete cutout brief project
                if (jcrContentNode.hasProperty(JLPConstants.JLP_BRIEF_END_USE)
                        && (JLPConstants.RELEASE_ONLINE_VALUE.equals(jcrContentNode
                                .getProperty(JLPConstants.JLP_BRIEF_END_USE).getString())
                                || JLPConstants.RELEASE_MULTI_USE_ONLINE_VALUE.equals(
                                        jcrContentNode.getProperty(JLPConstants.JLP_BRIEF_END_USE)
                                                .getString()))) {
                    publishBriefToScene7(projectNode.getPath());
                } else {
                    completeOfflineBrief(projectNode.getPath());
                }
                
            }
        } catch (Exception e) {
            log.error("Fatal Error - Can not Force complete the Brief", e);
            throw new RuntimeException("Can not Force complete the Brief - " + e.getMessage(), e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "SUCCESS";
    }

    @Override
    public boolean showForceCompleteButton(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            return existsRetouchedAssets(projectPath, resourceResolver)
                    && !isACompleateBrief(projectPath, resourceResolver)
                    && !hasPendingReconcileProducts(projectPath);
        } catch (Exception e) {
            log.error("Can not Evalueate if is a valid Force complete the Brief state", e);
            return false;
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public boolean hasPendingReconcileProducts(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if (jcrContentNode != null) {
                if (jcrContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
                    String damPath =
                            jcrContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();
                    Node damNode = resourceResolver.getResource(damPath).adaptTo(Node.class);
                    NodeIterator damNodeChildIt = damNode.getNodes();
                    while (damNodeChildIt.hasNext()) {
                        Node damProductFolderNode = damNodeChildIt.nextNode();
                        if ("sling:Folder"
                                .equals(damProductFolderNode.getPrimaryNodeType().getName())
                                && !JLPConstants.REFERENCE_FOLDER
                                        .equals(damProductFolderNode.getName())
                                && damProductFolderNode
                                        .hasProperty(JLPConstants.JLP_RECONCILE_PRODUCT)
                                && isShootNeeded(damProductFolderNode)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can not Evalueate if has Pending Reconcile Products", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return false;
    }

    private boolean isShootNeeded(Node damProductFolderNode) throws RepositoryException {
        NodeIterator children = damProductFolderNode.getNodes();
        while (children.hasNext()) {
            Node altNode = children.nextNode();
            if (altNode.hasProperty("dam:JLPBriefAssetStatus")) {
                BriefAssetStatus status = BriefAssetStatus.valueOf(altNode.getProperty("dam:JLPBriefAssetStatus").getString());
                if (status != BriefAssetStatus.ALT_NOT_NEEDED) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isACompleateBrief(String projectPath, ResourceResolver resourceResolver)
            throws PathNotFoundException, RepositoryException {
        Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
        Node jcrContentNode = projectNode.getNode("jcr:content");
        if (jcrContentNode != null && jcrContentNode.hasProperty(JLPConstants.PROJECT_STATUS)
                && "COMPLETE".equals(
                        jcrContentNode.getProperty(JLPConstants.PROJECT_STATUS).getString())) {
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isBriefCompleted(String projectPath){
    	ResourceResolver resourceResolver = getResourceResolver();    	
    	try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if (jcrContentNode != null && jcrContentNode.hasProperty(JLPConstants.PROJECT_STATUS)
                    && "COMPLETE".equals(
                            jcrContentNode.getProperty(JLPConstants.PROJECT_STATUS).getString())) {
                return true;
            }
            return false;
    		
    	} catch(Exception e) {
    		log.error("Can not evaluate the current brief", e);
            return false;
    	}
    }

    private boolean existsRetouchedAssets(String projectPath, ResourceResolver resourceResolver)
            throws PathNotFoundException, RepositoryException, ValueFormatException {
        NodeIterator assetFolderNodeIt = getAssetFolderNodeIt(projectPath, resourceResolver);
        if (assetFolderNodeIt != null) {
            while (assetFolderNodeIt.hasNext()) {
                Node assetFolderNode = assetFolderNodeIt.nextNode();
                if (assetFolderNode.getPrimaryNodeType().getName().equals("sling:Folder")
                        && assetFolderNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)
                        && hasUploadedAsset(assetFolderNode)) {
                    BriefAssetStatus assetStatus = BriefAssetStatus.valueOf(assetFolderNode
                            .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                    if (BriefAssetStatus.ASSET_RETOUCHED.equals(assetStatus)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean isAValidForceCompleteState(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            NodeIterator assetFolderNodeIt = getAssetFolderNodeIt(projectPath, resourceResolver);
            if (assetFolderNodeIt != null) {
                while (assetFolderNodeIt.hasNext()) {
                    Node assetFolderNode = assetFolderNodeIt.nextNode();
                    if (assetFolderNode.getPrimaryNodeType().getName().equals("sling:Folder")
                            && assetFolderNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)
                            && hasUploadedAsset(assetFolderNode)) {
                        BriefAssetStatus assetStatus = BriefAssetStatus.valueOf(assetFolderNode
                                .getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                        if (BriefAssetStatus.ASSET_UPLOADED.equals(assetStatus) 
                                || BriefAssetStatus.ASSET_REQUESTED.equals(assetStatus)) {
                            return false;
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            log.error("Can not Evalueate if is a valid Force complete the Brief state", e);
            return false;
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    private boolean hasUploadedAsset(Node assetFolderNode) throws RepositoryException {
        NodeIterator assetNodeIt =  assetFolderNode.getNodes();
        while (assetNodeIt.hasNext()) {
            Node assetNode =  assetNodeIt.nextNode();
            if (assetNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                return true;
            }
        }
        return false;
    }
    
    
    @Override
    public boolean isProductDuplicatedInBriefs(String productCode) {
   	 ResourceResolver resourceResolver = getResourceResolver();
        if (resourceResolver != null) {
            Session session = resourceResolver.adaptTo(Session.class);
            try {
           	 Product prod = productService.getProduct(productCode);
                if (prod != null) {
                   
               	 List<String> briefList = prod.getBriefs();
               	 if (briefList.isEmpty()) {
               		 return false;
               	 }else {
                   	 for (String briefPath : briefList) {
                   		 if (isCutoutBrief(briefPath, resourceResolver) && !isBriefCompleted(briefPath)) {
                   			 return true;
                   		 }
                   	 }
				}
               }
            } catch (Exception e) {
                log.error("Error checking for briefs containing the product:  " + productCode, e);
                return false;
            } finally {
                session.logout();
                resourceResolver.close();
            }
        }
   	 return false;
    }
    
    
    private boolean isCutoutBrief(String briefPath, ResourceResolver resourceResolver) throws RepositoryException{
        Node projectNode = resourceResolver.getResource(briefPath).adaptTo(Node.class);
        Node projectContentNode = projectNode.getNode("jcr:content");
        return BriefUtils.isCutoutBrief(projectContentNode);
    }

    private NodeIterator getAssetFolderNodeIt(String projectPath,
            ResourceResolver resourceResolver) throws PathNotFoundException, RepositoryException {
        Resource projectResource = resourceResolver.getResource(projectPath);
        if (projectResource != null) {
            Node projectNode = projectResource.adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            if (jcrContentNode != null) {
                if (jcrContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
                    String damPath =
                            jcrContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();
                    Node damNode = resourceResolver.getResource(damPath).adaptTo(Node.class);
                    NodeIterator damNodeChildIt = damNode.getNodes();
                    while (damNodeChildIt.hasNext()) {
                        Node damProductFolderNode = damNodeChildIt.nextNode();
                        if (damProductFolderNode.getPrimaryNodeType().getName()
                                .equals("sling:Folder")
                                && !JLPConstants.REFERENCE_FOLDER
                                        .equals(damProductFolderNode.getName())) {
                            return damProductFolderNode.getNodes();
                        }
                    }
                }
            }
        }
        return null;
    }
    
    @Reference
    private AssetTaskCompleterService assetTaskCompleter;
   
    @Override
    public void ingest3rdpartyRetouchedAsset(String assetPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node asNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            String sourceAssetPath = getSourceAssetPath(asNode.getParent().getParent().getName(), asNode.getName(), resourceResolver);
            if (sourceAssetPath != null) {
                Node metadataNode = asNode.getNode("jcr:content/metadata");
                metadataNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_RETOUCHED.toString());
                resourceResolver.commit();
                assetTaskCompleter.complete(sourceAssetPath, AutocompleteStatus.EXTERNAL_RETOUCH_DONE, "");
            } else {
                Map<String, Object> metaData = new HashMap<>();
                String targetFolder = asNode.getParent().getPath() + "/Rejected";
                String targetFileName = asNode.getName();
                metaData.put("targetFolder", targetFolder);
                int i = 0;
                while (resourceResolver.getResource(targetFolder + "/" + targetFileName) != null) {
                    targetFileName = asNode.getName().replace(".", "_" + i + ".");
                    i++;
                }
                
                metaData.put("targetFileName", targetFileName);
                workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, asNode.getPath(), metaData);
            }
        } catch (Exception e) {
            log.error("Error ingesting cutout asset", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
    }
    
    private String getSourceAssetPath(String sharedBriefFolderName, String assetName, ResourceResolver resourceResolver) {
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query =
                    queryManager.createQuery("SELECT * FROM [dam:Asset] AS s "
                            + " WHERE ISDESCENDANTNODE(s,'" + JLPConstants.PROJECTS_DAM_PATH  + "') "
                            + " AND NAME() = \"" + assetName + "\" "
                            + " AND s.[jcr:content/metadata/" + JLPConstants.JLP_BRIEF_ASSET_STATUS + "] = '" + BriefAssetStatus.ASSET_UPLOADED.toString() + "'",
                             javax.jcr.query.Query.JCR_SQL2);
            QueryResult queryResult = query.execute();
            NodeIterator results = queryResult.getNodes();
            String briefName = sharedBriefFolderName.substring(0, sharedBriefFolderName.lastIndexOf("-"));
            while (results.hasNext()) {
                Node hit = results.nextNode();
                if (hit.getParent().getParent().getParent().getName().equals(briefName)) {
                    if (hit.getNode("jcr:content/metadata").hasProperty(JLPConstants.RETOUCH_MODE)) {
                       String retouchMode = hit.getNode("jcr:content/metadata").getProperty(JLPConstants.RETOUCH_MODE).getString();
                       if ("thirdparty".equals(retouchMode)) {
                           return hit.getPath();
                       }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can't find source asset for ", assetName );
        }
        return null;
    }

    @Override
    public String createPathForProjectFromDeliveryDate(Calendar originalDeliveryDate, String projectName) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Calendar deliveryDate = originalDeliveryDate;
            StringBuilder builder = new StringBuilder(JLPConstants.PROJECTS_ROOT_FOLDER);
            deliveryDate.add(Calendar.MONTH, -1);
            int year = deliveryDate.get(Calendar.YEAR);
            int quarter = (deliveryDate.get(Calendar.MONTH)/3) + 1;
            int week = deliveryDate.get(Calendar.WEEK_OF_YEAR);
            builder.append("/").append(year).append("/Q").append(quarter).append("/Week_").append(week);
            Session session = resourceResolver.adaptTo(Session.class);
            Node path = JcrUtils.getOrCreateByPath(builder.toString(), "sling:OrderedFolder", session);
            Node tempPath = path;
            while (!tempPath.getPath().equals(JLPConstants.PROJECTS_ROOT_FOLDER)) {
                if (!tempPath.hasProperty("sling:resourceType")) {
                    tempPath.setProperty("sling:resourceType", "cq/gui/components/projects/admin/card/foldercard");
                }
                if (!tempPath.hasProperty("jcr:title")) {
                    tempPath.setProperty("jcr:title", tempPath.getName());
                }
                tempPath = tempPath.getParent();
            }
            if (path.hasNode(projectName)) {
                return "ERROR - Path " + builder.toString() + "/" + projectName + " already exists, please try a different name";
            }
            session.save();
            return builder.toString();
        } catch (Exception e) {
            log.error("Error creating project path", e);
            return "ERROR - Can't create project subfolder";
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public void processRemovedProduct(String projectDAMPath, String productCode) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource projectDAMResource = resourceResolver.getResource(projectDAMPath);
            if (projectDAMResource!=null) {
                Node projectDAMNode = projectDAMResource.adaptTo(Node.class);
                if (projectDAMNode.hasProperty(JLPConstants.JLP_PROJECT_PATH)) {
                    Property propPP = projectDAMNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
                    String projectPath = null;
                    if (propPP.isMultiple()) {
                        projectPath = propPP.getValues()[0].getString();
                    } else {
                        projectPath = propPP.getString();
                    }
                    Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                    if (projectNode.hasNode("jcr:content/dashboard/gadgets")) {
                        Node collectionsNode = projectNode.getNode("jcr:content/dashboard/gadgets");
                        if (collectionsNode.hasNode("collection_" + productCode)) {
                           Node collectionNode = collectionsNode.getNode("collection_" + productCode);
                           Node collectionDAMNode = resourceResolver.getResource(collectionNode.getProperty("collectionPath").getString()).adaptTo(Node.class);
                           collectionDAMNode.remove();
                           collectionNode.remove();
                           resourceResolver.commit();
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            log.error("Error deleting product from project", e);
        } finally {
            if (resourceResolver!=null) {
                resourceResolver.close();
            }
        }
    }
}
