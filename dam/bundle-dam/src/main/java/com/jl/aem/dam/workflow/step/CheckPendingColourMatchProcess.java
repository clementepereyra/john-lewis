package com.jl.aem.dam.workflow.step;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Check pending colour match process"})
public class CheckPendingColourMatchProcess extends AbstractProjectWorkflowProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(SetupAssetShootsWorkflowProcess.class);
    
    @Reference 
    ResourceResolverFactory resolverFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap metadata)
            throws WorkflowException {
        try { 
            Node projectNode = getProjectFromPayload(item, session.getSession(), resolverFactory);
            ResourceResolver resourceResolver = getResourceResolver(resolverFactory, session.getSession());
            if (isPendingColourMatch(projectNode, resourceResolver)) {
                item.getWorkflow().getWorkflowData().getMetaDataMap().put("isPendingColourMatch", "YES");
            }
            session.getSession().save();
        } catch (Exception e) {
            log.error("Can not set alt no needed", e);
        }
    }

    private boolean isPendingColourMatch(Node projectNode, ResourceResolver resourceResolver) throws Exception {
        Node jcrContentNode = projectNode.getNode("jcr:content");
        if (jcrContentNode != null) {
            if (jcrContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
                String damPath =
                        jcrContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();
                Node damNode = resourceResolver.getResource(damPath).adaptTo(Node.class);
                NodeIterator damNodeChildIt = damNode.getNodes();
                while (damNodeChildIt.hasNext()) {
                    Node damProductFolderNode = damNodeChildIt.nextNode();
                    if (damProductFolderNode.getPrimaryNodeType().getName()
                            .equals("sling:Folder")
                            && !JLPConstants.REFERENCE_FOLDER
                                    .equals(damProductFolderNode.getName())) {
                        NodeIterator altNodeIt = damProductFolderNode.getNodes();
                        while (altNodeIt.hasNext()) {
                            Node altNode = altNodeIt.nextNode();
                            if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                                String statusStr = altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString();
                                BriefAssetStatus status = BriefAssetStatus.valueOf(statusStr);
                                if (status == BriefAssetStatus.PENDING_COLOUR_MATCH) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}
