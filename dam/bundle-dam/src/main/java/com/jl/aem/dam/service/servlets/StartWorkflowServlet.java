package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.impl.BriefsServiceImpl;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/startShootWorkflow.json",
        "sling.servlet.methods=POST"})
public class StartWorkflowServlet  extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    //private static final String TEMP_PATH = "/content/dam/projects/temp/retouch";
    
    private static final Logger logger = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        try {
            List<String> errors = new ArrayList<String>();
            for(String path : items) {
                Resource resource = resourceResolver.getResource(path);
                if (resource != null) {
                    Node node = resource.adaptTo(Node.class);
                    if(node.getPrimaryNodeType().getName().equals("sling:Folder") && node.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                        BriefAssetStatus status = BriefAssetStatus.valueOf(node.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                        if (status!= BriefAssetStatus.ALT_NOT_NEEDED) {
                            if (isWorkflowStartedForAlt(node, resourceResolver)){
                                errors.add(node.getName() + " must be an N/A alt folder.");
                            }
                        }
                        if (node.getParent().hasProperty("dam:JLPDelivered")) {
                            errors.add(node.getName() + " is in an already delivered product.");
                        }
                        
                    } else {
                        errors.add(node.getName() + " is not an alt folder.");
                    }
                } else {
                    errors.add(path.substring(path.lastIndexOf("/")+1) + " is not an alt folder.");
                }
            }
            if (errors.size() > 0) {
                for (String error : errors) {
                    response.getWriter().println();
                    response.getWriter().println(error);
                }
            } else {
                response.getWriter().write("OK");
            }
            response.setContentType("text/plain");
        } catch (Exception e) {
            logger.error("Can't validate product delivery", e);
        }
    }
    
    private boolean isWorkflowStartedForAlt(Node node, ResourceResolver resourceResolver) throws Exception{
        String projectPath = getProjectPath(node.getPath(), resourceResolver);
        String productName = getProductName(node.getPath(), resourceResolver);
        String altName = getAltName(node.getPath(), resourceResolver);
        Resource projectResource = resourceResolver.getResource(projectPath);
        if (projectResource != null) {
            Node projectNode = projectResource.adaptTo(Node.class);
            Node workNode = projectNode.getNode("jcr:content").getNode("work");
            NodeIterator it = workNode.getNodes();
            while (it.hasNext()) {
                Node workItemNode = it.nextNode();
                if (workItemNode.getName().equals("provide-shoot-" + productName + "-" + altName)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        for(String path : items) {
            Resource resource = resourceResolver.getResource(path);
            if (resource != null) {
                String projectPath = getProjectPath(path, resourceResolver);
                String  productName = getProductName(path, resourceResolver);
                String altName = getAltName(path, resourceResolver);
                briefsService.startWorkflowForAltNodeInProject(projectPath, productName, altName);
                Node altNode = resource.adaptTo(Node.class);
                try {
                    altNode.setProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS, BriefAssetStatus.ASSET_REQUESTED.toString());
                    resourceResolver.commit();
                } catch (Exception e) {
                    logger.error("Can't reset alt status", e);
                }
            }
        }
        response.getWriter().write("OK");
        response.setContentType("text/plain");
    }

    private String getAltName(String altNodePath, ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource(altNodePath);
        if (resource != null) {
            Node altNode = resource.adaptTo(Node.class);
            try {
                return altNode.getName();
            } catch (Exception e) {
                logger.error("Error getting project path", e);
            }
        }
        return null;
    }

    private String getProductName(String altNodePath, ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource(altNodePath);
        if (resource != null) {
            Node altNode = resource.adaptTo(Node.class);
            try {
                return altNode.getParent().getName();
            } catch (Exception e) {
                logger.error("Error getting product name", e);
            }
        }
        return null;
    }

    private String getProjectPath(String altNodePath, ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource(altNodePath);
        if (resource != null) {
            Node altNode = resource.adaptTo(Node.class);
            try {
                return BriefUtils.getProjectPathFromProductNode(altNode.getParent());
            } catch (Exception e) {
                logger.error("Error getting project path", e);
            }
        }
        return null;
    }

    

    
    
}
