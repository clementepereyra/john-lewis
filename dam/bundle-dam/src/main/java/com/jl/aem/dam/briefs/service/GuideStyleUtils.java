package com.jl.aem.dam.briefs.service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

public class GuideStyleUtils {

    private static final Logger log = LoggerFactory.getLogger(GuideStyleUtils.class);

    private static final String STYLE_GUIDE_PATH = "/etc/jl-admin/style-guide/style-guide-page";

    private static final String SEARCHED_RESOURCE_TYPE =
            "onejl-dam/components/styleguideproduct-item";

    private static final String SEARCHED_PROPERTY = "productType";

    private static final String ALTS_PROPERTY = "alts";

    private static final int DEFAULT_NUMBER_OF_ALTS = 6;

    public static int getNumberOfAlts(String prodTypeName, ResourceResolver resourceResolver) {
        try {
            if (StringUtils.isNotBlank(prodTypeName)) {
                SearchResult results = searchGuideStyle(prodTypeName, resourceResolver);

                if (results.getTotalMatches() > 0) {
                    Hit hit = results.getHits().get(0);

                    Node hitNode = hit.getNode();
                    if (hitNode.hasProperty(ALTS_PROPERTY)) {
                        return Integer.parseInt(hitNode.getProperty(ALTS_PROPERTY).getString());
                    }

                } else {
                    return DEFAULT_NUMBER_OF_ALTS;
                }
            }
        } catch (RepositoryException e) {
            log.error("Error getting number of alts from guide syle", e);

        }
        return DEFAULT_NUMBER_OF_ALTS;
    }

    private static SearchResult searchGuideStyle(String prodTypeName,
            ResourceResolver resourceResolver) {
        Session session = resourceResolver.adaptTo(Session.class);

        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", STYLE_GUIDE_PATH);

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "nt:unstructured");

        Predicate resourceTypePredicate = new Predicate("status", "property");
        resourceTypePredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "sling:resourceType");
        resourceTypePredicate.set(JcrPropertyPredicateEvaluator.VALUE, SEARCHED_RESOURCE_TYPE);
        resourceTypePredicate.set(JcrPropertyPredicateEvaluator.OPERATION,
                JcrPropertyPredicateEvaluator.OP_EQUALS);

        Predicate productTypePredicate = new Predicate("status", "property");
        productTypePredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, SEARCHED_PROPERTY);
        productTypePredicate.set(JcrPropertyPredicateEvaluator.VALUE, prodTypeName);
        productTypePredicate.set(JcrPropertyPredicateEvaluator.OPERATION,
                JcrPropertyPredicateEvaluator.OP_EQUALS);


        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.add(resourceTypePredicate);
        predicates.add(productTypePredicate);
        predicates.setAllRequired(true);

        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(predicates, session);
        query.setHitsPerPage(0);

        return query.getResult();
    }


}
