package com.jl.aem.dam.briefs.service;

import java.util.Calendar;
import java.util.List;

import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;

public interface BriefsService {

   public void importCutoutBriefCSV (final String projectPath, final String filename);
   
   public void importLifestyleBriefCSV (final String projectPath, final String filename);

   public void ingestCutoutAsset(final String assetPath);

   public void ingestLifestyleAsset(final String assetPath);

   public void setupShootTasks(final String projectPath);

   public void processNewStatus(final String projectPath, final String productCode, final String altCode, final BriefAssetStatus assetStatus);

   public void setReshootNeeded(String projectPath, String productCode, String altCode);

   public void publishBriefToScene7(String projectPath);

   public void completeOfflineBrief(String projectPath);

   public void addCompositeFolder(String path);

   public String addProductToCutoutBrief(String projectPath, String productCode, String category, String imageRequirement, String stylingGuide, String range, String ids, String clientNotes);
   
   public String addProductToLifestyleLook(String projectPath, String lookName, String productCode);

    public void triggerReleaseWorkflow(String projectPath);
    
    public String changeBriefStatus(String projectPath, String status, String comments, String userID);
    
    public String forceCompleteProject(String projectPath, String status, String comments, String userID);
    
    public boolean showForceCompleteButton(String projectPath);
    
    public boolean isAValidForceCompleteState(String projectPath);
    
    public boolean hasPendingReconcileProducts(String projectPath);
    
    public boolean isBriefCompleted(String projectPath);
    
    public boolean isProductDuplicatedInBriefs(String productCode);

    void importRetouchBriefCSV(String projectPath, String filename);

    public void ingest3rdpartyRetouchedAsset(String path);

    public String createPathForProjectFromDeliveryDate(Calendar deliveryDate, String projectName);

    public void processRemovedProduct(String projectPath, String productCode);

    void deliverProductToScene7(String productFolderPath) ;

    public String addLookToLifestyleBrief(String projectPath, String lookName, String namingConventions,
            String shotDescription, String buyingOffice, String range, String ids,
            String clientNotes,  int noOfAlts, List<String> products);

    void startWorkflowForAltNodeInProject(String projectPath, String itemId, String altName);
}


