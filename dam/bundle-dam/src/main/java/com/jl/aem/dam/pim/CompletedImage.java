package com.jl.aem.dam.pim;

import com.google.gson.annotations.SerializedName;

public class CompletedImage {
    
    @SerializedName("product_code")
    private String productCode;
    
    @SerializedName("image_category")
    private String imageCategory;
 
    @SerializedName("prev_status_id")
    private String prevStatusId;
    
    @SerializedName("new_status_id")
    private String newStatusId;
    
    @SerializedName("updated_timestamp")
    private String updatedTime;

    @SerializedName("updated_by")
    private String updatedBy;

 
    public CompletedImage(final String productCode, final String prevStatus, final String newStatus, final String updatedTime, final String updatedBy, final String imageCat) {
        super();
        this.productCode = productCode;
        this.prevStatusId = prevStatus;
        this.newStatusId = newStatus;
        this.updatedBy = updatedBy;
        this.updatedTime = updatedTime;
        this.imageCategory = imageCat.replace("jlp:image-category/", "").replace("alt/", "").replace("/", "");
    }

    public String getProductCode() {
        return productCode;
    }

    public String getPrevStatusId() {
        return prevStatusId;
    }

    public String getNewStatusId() {
        return newStatusId;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }
    
}
