package com.jl.aem.dam.workflow.step;



import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.exec.WorkItem;

public abstract class AbstractProjectWorkflowProcess {
    
    private static final Logger log = LoggerFactory.getLogger(SetupAssetShootsWorkflowProcess.class);


    public static final String TYPE_JCR_PATH = "JCR_PATH";

    protected boolean isBriefProject(Node projectNode) throws RepositoryException {
        if (projectNode.hasProperty("sling:resourceType")) {
            String type = projectNode.getProperty("sling:resourceType").getString();
            if ("cq/gui/components/projects/admin/card/projectcard".equals(type)) {
                if (projectNode.hasNode("jcr:content")) {
                    Node jcrContent = projectNode.getNode("jcr:content");
                    if (jcrContent.hasProperty("cq:template")) {
                        String template =jcrContent.getProperty("cq:template").getString();
                        if (template.indexOf("brief") >= 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    protected Node getProjectFromPayload(final WorkItem item, final Session session, ResourceResolverFactory resolverFactory) {
    
        Node project = null;
        if (item.getWorkflowData().getPayloadType().equals(TYPE_JCR_PATH)) {
            final String path = item.getWorkflowData().getPayload().toString();
            final Resource resource = getResourceResolver(resolverFactory, session).getResource(path);
            if (null != resource) {
                project = resource.adaptTo(Node.class);
            } else {
                log.error("getProjectFromPaylod: project [{}] in payload of workflow [{}] does not exist.", path,
                        item.getWorkflow().getId());
            }
        }
        return project;
    }

    protected ResourceResolver getResourceResolver(ResourceResolverFactory resolverFactory, Session session) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("user.jcr.session", session); 
            resourceResolver = resolverFactory.getResourceResolver(param);
            return resourceResolver;
        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
    }

}
