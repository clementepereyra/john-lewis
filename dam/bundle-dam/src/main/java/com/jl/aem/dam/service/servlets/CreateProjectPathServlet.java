package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.Calendar;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/setupProjectFolder",
    "sling.servlet.methods=POST"
  }
)
public class CreateProjectPathServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 6090044399598818194L;

	private static final Logger log = LoggerFactory.getLogger(CreateProjectPathServlet.class);

    @Reference
    private BriefsService briefsService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
        try {
            
            String deliveryDateParam = request.getParameter("deliveryDate");
            String projectName = request.getParameter("projectName");
            Calendar deliveryDate = null;
            if (StringUtils.isNumeric(deliveryDateParam)) {
                deliveryDate = Calendar.getInstance();
                deliveryDate.setTimeInMillis(Long.valueOf(deliveryDateParam));
            }
            if (deliveryDateParam != null && deliveryDate != null && projectName != null) {
                String path = briefsService.createPathForProjectFromDeliveryDate(deliveryDate, projectName);
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().write("{\"result\":\"" + path + "\"}");
                response.setContentType("application/json");
            } else {
                response.getWriter().write("{\"result\":\"ERROR: Can't create path for project.\"}");
                response.setStatus(HttpServletResponse.SC_OK);
                response.setContentType("application/json");
            }
            
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in Add New Alts Servlet " + e.getMessage());
            response.getWriter().write("{\"result\":\"ERROR: Can't create path for project. "+ e.getMessage() + "\"}");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType("application/json");
        }
    }

    
}