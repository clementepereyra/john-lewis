package com.jl.aem.dam.briefs.workflow.impl;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;

public class ProjectRoleHelper {
    private String activityCoordinator;
    private String stockCoordinator;
    private String shootProducer;
    private String retoucher;
    private String owner;
    private String photographer;
    private String externalRetoucher;
    private String postProductionCoordinator;
    private String artDirector;

    public String getActivityCoordinator() {
        return activityCoordinator;
    }

    public String getStockCoordinator() {
        return stockCoordinator;
    }

    public String getShootProducer() {
        return shootProducer;
    }

    public String getRetoucher() {
        return retoucher;
    }

    public String getOwner() {
        return owner;
    }

    public String getPhotographer() {
        return photographer;
    }

    public String getExternalRetoucher() {
        return externalRetoucher;
    }

    public String getPostProductionCoordinator() {
        return postProductionCoordinator;
    }   

    public String getArtDirector() {
        return artDirector;
    }

    public ProjectRoleHelper(Node projectNode) {
        try {
           PropertyIterator propIterator = projectNode.getProperties();
           while (propIterator.hasNext()) {
               Property property = propIterator.nextProperty();
               if (property.getName().startsWith("role_")) {
                   if (property.getName().equals("role_activitycoordinator")) {
                       activityCoordinator = property.getValue().getString();
                   } else if (property.getName().equals("role_stockcoordinator")) {
                       stockCoordinator = property.getValue().getString();
                   } else if (property.getName().equals("role_artdirector")) {
                       artDirector = property.getValue().getString();
                   } else if (property.getName().equals("role_shootproducer")) {
                       shootProducer = property.getValue().getString();
                   } else if (property.getName().equals("role_retoucher")) {
                       retoucher = property.getValue().getString();
                   } else if (property.getName().equals("role_owner")) {
                       owner = property.getValue().getString();
                   } else if (property.getName().equals("role_photographer")) {
                       photographer = property.getValue().getString();
                   } else if (property.getName().equals("role_externalretoucher")) {
                       externalRetoucher = property.getValue().getString();
                   } else if (property.getName().equals("role_postprodcoordinator")) {
                       postProductionCoordinator = property.getValue().getString();
                   } 
               }
           }
        } catch (Exception e) {
            ProjectWorkflowHelper.log.error("Could not map project roles to helper", e);
        }
    }

}