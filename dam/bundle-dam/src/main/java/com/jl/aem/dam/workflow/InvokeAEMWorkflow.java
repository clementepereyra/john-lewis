package com.jl.aem.dam.workflow;

import java.util.Map;

public interface InvokeAEMWorkflow {

	public String startWorkflow(String workflowName, String workflowContent, Map<String, Object> metaData);

}