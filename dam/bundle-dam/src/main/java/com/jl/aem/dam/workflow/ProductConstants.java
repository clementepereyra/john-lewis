package com.jl.aem.dam.workflow;

public interface ProductConstants {

    public static final String JCR_LAST_MODIFIED = "jcr:lastModified";
    public static final String JCR_TITLE = "jcr:title";
    public static final String JCR_CONTENT = "jcr:content";
    
    public static final String CQ_COMMERCE_TYPE = "cq:commerceType";
    public static final String CQ_TAGGABLE = "cq:Taggable";

    public static final String SLING_RESOURCE_TYPE = "sling:resourceType";

    public static final String PRODUCT_CODE ="identifier";
    public static final String BRAND_NAME =  "brandName";
    public static final String COLOUR = "colour";
    public static final String COLOUR_CODE = "colourCode";
    public static final String COLOUR_GROUP = "colourGroup";
    public static final String CONFIRMED_DATE = "confirmedDate";
    public static final String IMAGE_SOURCE = "imageSource";
    public static final String MODEL_NO = "modelNo";
    public static final String ONLINE_STATUS = "onlineStatus";
    public static final String OPTION_DESC = "optionDes";
    public static final String POS_DESC = "posDesc";
    public static final String PROD_TYPE_NAME = "prodTypeName";
    public static final String RESEARCH_DONE = "researchDone";
    public static final String SAMPLE_SOURCE = "sampleSource";
    public static final String SEL_DESC = "selDesc";
    public static final String STATUS_ID = "statusId";
    public static final String SUPPLIER_CONTACT_DETAIL = "supplierContactDetail";
    public static final String TRADED_CODE = "tradedCode";
    public static final String WSM_ITEM_ID = "wsmItemId";
    public static final String USER_ID_UPD = "userIdUpd";
    public static final String WSM_TITLE = "wsmTitle";
    public static final String DIRECTORATE = "directorate";
    public static final String SEASON_CODE = "seasonCode";
    public static final String YEAR = "year";
    public static final String BUYING_OFFICE = "buyingOffice";
    public static final String LAUNCH_DATE = "launchDate";
    public static final String DELETED = "deleted";
    public static final String STOCK_LEVEL = "stockLevel";
    public static final String PRICE = "price";
    public static final String IMAGE_REQUESTED_DATE = "imageRequestedDate";
    public static final String BRIEFS = "briefs";

}
