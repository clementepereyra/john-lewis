package com.jl.aem.dam.workflow;

import java.util.List;

import com.jl.aem.dam.pim.Product;

public interface WorkflowBusinessSettings {

    boolean isInWorkflow(String supplier, String buyingOffice);

    List<String> getPhasedRolloutBuyingOffices();

    List<String> getPhasedRolloutBrands();

    boolean is34Ratio(Product product);

    boolean isSquareRatio(Product product);
}
