package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = WorkflowProcess.class,
        property = {"process.label=Complete cutout brief project"})
public class CompleteProjectWorkflowProcess extends AbstractProjectWorkflowProcess
        implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(CompleteProjectWorkflowProcess.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefsService;

    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap map)
            throws WorkflowException {
        try {
            Node projectNode = getProjectFromPayload(item, session.getSession(), resolverFactory);

            Node projectContentNode = projectNode.getNode("jcr:content");

            if (projectContentNode.hasProperty(JLPConstants.JLP_BRIEF_END_USE)
                    && (JLPConstants.RELEASE_ONLINE_VALUE.equals(projectContentNode
                            .getProperty(JLPConstants.JLP_BRIEF_END_USE).getString())
                            || JLPConstants.RELEASE_MULTI_USE_ONLINE_VALUE.equals(projectContentNode
                                    .getProperty(JLPConstants.JLP_BRIEF_END_USE).getString()))) {
                log.info("Completing brief to scene7: " + projectNode.getPath());
                briefsService.publishBriefToScene7(projectNode.getPath());
            } else {
                log.info("Completing brief to offline: " + projectNode.getPath());
                log.info(projectContentNode.hasProperty(JLPConstants.JLP_BRIEF_END_USE)?projectContentNode
                            .getProperty(JLPConstants.JLP_BRIEF_END_USE).getString():"No end use");
                briefsService.completeOfflineBrief(projectNode.getPath());
            }

            briefsService.changeBriefStatus(projectNode.getPath(), "COMPLETE", "Automatic complete",
                    session.getUser().getID());
        } catch (Exception e) {
            log.error("Error completing project", e);
            notifyUser(e.getCause().getMessage());
        }

    }

    private void notifyUser(String string) {
        // TODO Notify user about wrong CSV file

    }

}
