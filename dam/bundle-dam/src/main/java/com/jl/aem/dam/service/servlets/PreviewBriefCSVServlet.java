package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.dam.api.Asset;
import com.google.gson.Gson;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.CSVBriefHelper;
import com.jl.aem.dam.briefs.service.CutoutBrief;
import com.jl.aem.dam.briefs.service.CutoutBrief.CutoutBriefRow;
import com.jl.aem.dam.briefs.service.LifestyleBrief;
import com.jl.aem.dam.briefs.service.LifestyleBrief.LifestyleBriefRow;
import com.jl.aem.dam.briefs.service.RetouchBrief;
import com.jl.aem.dam.briefs.service.RetouchBrief.RetouchBriefRow;
import com.jl.aem.dam.pim.ProductService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/previewbriefcsv",
    "sling.servlet.methods=GET"
  }
)
public class PreviewBriefCSVServlet extends SlingAllMethodsServlet {
    
    /**
     * 
     */
    private static final long serialVersionUID = -3741915378251088937L;
    private Gson gson = new Gson();
    
    @Reference
    private ProductService productService;
    
    @Reference
    private BriefsService briefService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        String filename = request.getParameter("filename");
        String briefID = request.getParameter("briefID");
        
        ResourceResolver resourceResolver = request.getResourceResolver();
        Resource csvResource = resourceResolver.getResource(filename);
        Asset csvAsset = csvResource.adaptTo(Asset.class);
        boolean isCutoutBrief = "CUTOUT".equalsIgnoreCase(briefID);
        boolean isLifestyleBrief = "LIFESTYLE".equalsIgnoreCase(briefID);
        PreviewResponse previewResponse = null;
        if (isCutoutBrief) {
            previewResponse = generateCutoutBriefPreview(csvAsset);
        } else if (isLifestyleBrief) { 
            previewResponse = generateLifestyleBriefPreview(csvAsset);
        } else { 
            previewResponse = generateRetouchBriefPreview(csvAsset);
        }
        
        previewResponse.setBriefID(briefID);
        previewResponse.setBriefID(briefID);
        response.getWriter().write(gson.toJson(previewResponse));
        
    }

    private PreviewResponse generateCutoutBriefPreview(Asset csvAsset) throws IOException {
        PreviewResponse preview = new PreviewResponse();
        CutoutBrief brief = CSVBriefHelper.parseCutoutBriefCSV(csvAsset);
        if (brief.isValid()) {
            preview.setValid(true);
            List<CutoutBriefRow> rows = brief.getRows();
            preview.setRows(rows.size());
            List<String> validProducts = new ArrayList<>();
            List<String> notValidProducts = new ArrayList<>();
            List<String> duplicatedProducts = new ArrayList<>();
            for (CutoutBriefRow row : rows) {
                String prodCode = row.getStockNumber();
                if (productService.exist(prodCode)) {
                	if(briefService.isProductDuplicatedInBriefs(prodCode)) {
                		duplicatedProducts.add(prodCode);              		
                	}
                	validProducts.add(prodCode);
                } else {
                    notValidProducts.add(prodCode);
                }
            }
            preview.setDuplicatedProducts(duplicatedProducts);
            preview.setNotValidProducts(notValidProducts);
            preview.setValidProducts(validProducts);
        } else {
            preview.setValid(false);
        }
        
        return preview;
    }
    
    private PreviewResponse generateLifestyleBriefPreview(Asset csvAsset) throws IOException {
        PreviewResponse preview = new PreviewResponse();
        LifestyleBrief brief = CSVBriefHelper.parseLifestyleBriefCSV(csvAsset);
        if (brief.isValid()) {
            preview.setValid(true);
            List<LifestyleBriefRow> rows = brief.getRows();
            preview.setRows(rows.size());
            List<String> validProducts = new ArrayList<>();
            List<String> notValidProducts = new ArrayList<>();
            for (LifestyleBriefRow row : rows) {
                List<String> products = row.getProducts();
                for (String prodCode : products) {
                    if (productService.exist(prodCode)) {
                        validProducts.add(prodCode);
                    } else {
                        notValidProducts.add(prodCode);
                    }
                }
            }
            preview.setNotValidProducts(notValidProducts);
            preview.setValidProducts(validProducts);
        } else {
            preview.setValid(false);
        }
        
        return preview;
    }

    private PreviewResponse generateRetouchBriefPreview(Asset csvAsset) throws IOException {
        PreviewResponse preview = new PreviewResponse();
        RetouchBrief brief = CSVBriefHelper.parseRetouchBriefCSV(csvAsset);
        if (brief.isValid()) {
            preview.setValid(true);
            List<RetouchBriefRow> rows = brief.getRows();
            preview.setRows(rows.size());
            List<String> validProducts = new ArrayList<>();
            List<String> notValidProducts = new ArrayList<>();
            for (RetouchBriefRow row : rows) {
                String prodCode = row.getStockNumber();
                if (productService.exist(prodCode)) {
                    validProducts.add(prodCode);
                } else {
                    notValidProducts.add(prodCode);
                }
            }
            preview.setNotValidProducts(notValidProducts);
            preview.setValidProducts(validProducts);
        } else {
            preview.setValid(false);
        }
        
        return preview;
    }
    
    public static class PreviewResponse {
        private int rows;
        private boolean valid;
        private List<String> validProducts;
        private List<String> notValidProducts;
        private List<String> duplicatedProducts;
        private String briefID;
        public int getRows() {
            return rows;
        }
        public void setRows(int rows) {
            this.rows = rows;
        }
        public boolean isValid() {
            return valid;
        }
        public void setValid(boolean valid) {
            this.valid = valid;
        }
        public List<String> getValidProducts() {
            return validProducts;
        }
        public void setValidProducts(List<String> validProducts) {
            this.validProducts = validProducts;
        }
        public void setDuplicatedProducts(List<String> duplicatedProducts) {
            this.duplicatedProducts = duplicatedProducts;
        }
        public List<String> getDuplicatedProducts() {
            return duplicatedProducts;
        }
        public List<String> getNotValidProducts() {
            return notValidProducts;
        }
        public void setNotValidProducts(List<String> notValidProducts) {
            this.notValidProducts = notValidProducts;
        }
        public String getBriefID() {
            return briefID;
        }
        public void setBriefID(String briefID) {
            this.briefID = briefID;
        }       
    }
}
