package com.jl.aem.dam.workflow.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

import com.jl.aem.dam.workflow.JLPWorkflowSettings;

@Component(immediate=true, name="JLP DAM Workflow Configuration", service=JLPWorkflowSettings.class)
@Designate(ocd = JLPWorkflowSettingsConfiguration.class)
public class JLPWorkflowSettingsImpl implements JLPWorkflowSettings {

    
    private String reviewerAdmin;
    private int unchangedDaysLimit;
    private String observedPath;
    private String emailLogoUrl;
    private String emailLogoAlt;
    private String emailSignature;
    private int emailLogoHeight;
    private String[] unchangedAssetRecipientList;
    private String[] assetStatusRecipientList;
    private String[] invalidProductRecipientList;
    private String[] artworkersRecipientList;
    private String[] monitoredStatus;
    private String[] artworkersBornRecipientList;
    private String artworkersBornUrl;
    private String artworkersBornSecureToken;
    private String artworkersBornJobTypeId;
    private String artworkersBornBusinessAreaId;
    private boolean enableEmailNotification;
    private int expirationDaysWarning;
    private String[] expiredAssetsRecipientList;
    private boolean enableAutoCompletePhotographersTask;
    private boolean enableAutoCompleteReTouchersTask;
    private int commitTimeout;

    @Override
    public String getReviewAdmin() {
        return reviewerAdmin;
    }

    @Override
    public String getEmailLogoUrl() {
        return emailLogoUrl;
    }

    @Override
    public String getEmailLogoAlt() {
        return emailLogoAlt;
    }

    @Override
    public int getEmailLogoHeight() {
        return emailLogoHeight;
    }
    
    @Override
    public String getEmailSignature() {
        return emailSignature;
    }

    @Override
    public int getUnchangedDaysLimit() {
        return unchangedDaysLimit;
    }

    @Override
    public String[] getUnchangedAssetRecipientList() {
        return unchangedAssetRecipientList;
    }

    @Override
    public String[] getAssetStatusRecipientList() {
        return assetStatusRecipientList;
    }

    @Override
    public String[] getMonitoredStatus() {
        return monitoredStatus;
    }

    @Override
    public String[] getInvalidProductRecipientList() {
        return invalidProductRecipientList;
    }

    @Override
    public String[] getArtworkersRecipientList() {
        return artworkersRecipientList;
    }

    @Override
    public String[] getArtworkersBornRecipientList() {
        return artworkersBornRecipientList;
    }

    @Override
    public String getObservedPath() {
        return observedPath;
    }
    
    @Override
    public String getArtworkersBornUrl() {
        return artworkersBornUrl;
    }
    
    @Override
    public String getArtworkersBornSecureToken() {
        return artworkersBornSecureToken;
    }
    
    @Override
    public String getArtworkersBornJobTypeId() {
        return artworkersBornJobTypeId;
    }
    
    @Override
    public String getArtworkersBornBusinessAreaId() {
        return artworkersBornBusinessAreaId;
    }
    
    @Override
    public boolean getEnableEmailNotification() {
        return enableEmailNotification;
    }
    
    @Override
    public int getExpirationDaysWarning() {
        return expirationDaysWarning;
    }
    @Override
    public String[] getExpiredAssetRecipientList() {
        return expiredAssetsRecipientList;
    }
    
    @Override
    public boolean getEnableAutoCompletePhotographersTask() {
        return enableAutoCompletePhotographersTask;
    }

    @Override
    public boolean getEnableAutoCompleteReTouchersTask() {
        return enableAutoCompleteReTouchersTask;
    }
    
    @Override
    public int getCommitTimeout() {
        return commitTimeout;
    }
    
    @Activate
    protected void activate(final JLPWorkflowSettingsConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPWorkflowSettingsConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPWorkflowSettingsConfiguration config) {
    }

    private synchronized void resetService(final JLPWorkflowSettingsConfiguration config) {
        reviewerAdmin = config.getReviewAdmin();
        unchangedDaysLimit = config.getUnchangedLimit();
        observedPath = config.getObservedPath();
        unchangedAssetRecipientList = config.getUnchangedAssetRecipientList();
        invalidProductRecipientList = config.getInvalidProductRecipientList();
        artworkersRecipientList = config.getArtworkersRecipientList();
        assetStatusRecipientList = config.getAssetStatusRecipientList();
        monitoredStatus = config.getMonitoredStatus();
        artworkersBornJobTypeId = config.getArtworkersBornJobTypeId();
        artworkersBornBusinessAreaId = config.getArtworkersBornBusinessAreaId();
        artworkersBornSecureToken = config.getArtworkersBornSecureToken();
        artworkersBornUrl = config.getArtworkersBornUrl();
        artworkersBornRecipientList = config.getArtworkersBornRecipientList();
        emailLogoUrl = config.getEmailLogoUrl();
        emailLogoAlt = config.getEmailLogoAlt();
        emailLogoHeight = config.getEmailLogoHeight();
        emailSignature = config.getEmailSignature();
        enableEmailNotification = config.getEnableEmailNotification();
        expirationDaysWarning = config.getExpirationDaysWarning();
        expiredAssetsRecipientList = config.getExpiredAssetsRecipientList();
        enableAutoCompletePhotographersTask = config.getEnableAutoCompletePhotographersTask();
        enableAutoCompleteReTouchersTask = config.getEnableAutoCompleteReTouchersTask();
        commitTimeout = config.getCommitTimeout();
    }


}
