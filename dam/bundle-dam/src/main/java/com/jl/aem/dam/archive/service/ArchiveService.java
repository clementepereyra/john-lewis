package com.jl.aem.dam.archive.service;

public interface ArchiveService {

    String archiveAsset(final String assetPath);

    void archiveLayeredAssets();

    void removeFlattenAssets();

    String restoreAsset(String path);
    
    void cleanThirdPartyAssets();
    
}
