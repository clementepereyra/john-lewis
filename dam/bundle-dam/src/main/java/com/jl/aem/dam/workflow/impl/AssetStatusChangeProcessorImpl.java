package com.jl.aem.dam.workflow.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;


import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamEvent;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.AssetStatusChangeProcessor;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.AssetStatusCommandFactory;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service = AssetStatusChangeProcessor.class)
public class AssetStatusChangeProcessorImpl implements AssetStatusChangeProcessor {

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private AssetStatusCommandFactory assetStatusCommandFactory;
    
    private static final Logger LOG = LoggerFactory.getLogger(AssetStatusChangeProcessorImpl.class);


    @Override
    public void process(DamEvent damEvent, String assetPath) {
        ResourceResolver resourceResolver = null;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            LOG.error("Can not login system user and get resource resolver " + e.getMessage());
        }
        if (resourceResolver != null) {
            AssetStatus status = null;
            boolean statusChanged = false;
            try {
                Resource assetResource = resourceResolver.getResource(assetPath);
                Asset asset = assetResource.adaptTo(Asset.class);
                String assetStatus = asset.getMetadataValue(JLPConstants.JLP_STATUS); 
                String assetAssigned = asset.getMetadataValue(JLPConstants.JLP_ASIGNEE); 
                String prevStatus = asset.getMetadataValue(JLPConstants.JLP_LAST_STATUS); 
                String prevAssigned = asset.getMetadataValue(JLPConstants.JLP_LAST_ASIGNEE); 
                statusChanged = valueChanged(assetStatus, prevStatus);
                boolean assignedChanged = valueChanged(assetAssigned, prevAssigned);
    
                if(statusChanged || assignedChanged) {
                  //TODO log audit
                  //damAuditLog.logAsset(damEvent, asset, auditLog);
                }
                
                if(statusChanged || assignedChanged) {
                    saveLastJLPValues(assetPath, assetStatus, assetAssigned, resourceResolver);
                }
                if (assetStatus != null) {
                    status = AssetStatus.valueOf(assetStatus);
                }
                LOG.debug("JLP status change " + assetStatus + " before " + prevStatus);
            } catch (Exception e){
                LOG.error("Error getting asset data", e);
            } finally {
                 if (resourceResolver != null) {
                     resourceResolver.close();
                 }
            }
            
            if(statusChanged){
              
              AssetStatusCommand assetStatusCommand = assetStatusCommandFactory.getAssetStatusCommand(status);
              if (assetStatusCommand != null) {
                  LOG.info("Call asset command. Status: " + status.toString() + " Asset " + assetPath);
                  assetStatusCommand.execute(assetPath, damEvent.getUserId());
              }
            }
        }

    }
    
    @Override
    public boolean shouldProcess(DamEvent damEvent) {
        if (damEvent == null) {
            return false;
        }
        String additionalInfo = "";
        if (damEvent.getAdditionalInfo() != null) {
            additionalInfo = damEvent.getAdditionalInfo();
        }
        if(additionalInfo.contains("jcr:content/metadata/" + JLPConstants.JCR_LAST_MODIFIED) || 
          additionalInfo.contains("jcr:content/metadata/" + JLPConstants.JLP_LAST_ASIGNEE) || 
          additionalInfo.contains("jcr:content/metadata/" + JLPConstants.JLP_LAST_STATUS)) {
            return false;
        }
        return true;
    }

    private boolean valueChanged(final String newValue, final String oldValue){
        if(oldValue == null && newValue != null && newValue.trim().length()>2)
            return true;

        return ((oldValue != null) && !oldValue.equals(newValue));
      }

    private void saveLastJLPValues(final String assetPath, final String assetStatus, final String assetAssigned, final ResourceResolver resourceResolver)  {
        try{    
            Session session = resourceResolver.adaptTo(Session.class);
            Resource resource = resourceResolver.getResource(assetPath);
            Node assetNode = resource.adaptTo(Node.class);
            session.refresh(true);
            Node metadataNode = assetNode.getNode("jcr:content/metadata");
                if(assetStatus != null) {
                    metadataNode.setProperty(JLPConstants.JLP_LAST_STATUS, assetStatus);
                }
                  if(assetAssigned != null) {
                      metadataNode.setProperty(JLPConstants.JLP_LAST_ASIGNEE, assetAssigned);
                  }
            session.save();
            session.logout();
          } catch(Exception e) {
            LOG.error("JLP DAM error save last status for asset " );
          }
    }
}
