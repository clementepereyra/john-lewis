package com.jl.aem.dam.service.integration;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.http.client.CookieStore;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(
        immediate = true,
        service = HTTPClientFactory.class
)
@Designate(ocd = JLPHttpClientConfiguration.class)
public class HTTPClientFactory {

    private static final Logger logger = LoggerFactory.getLogger(HTTPClientFactory.class);
    
	private HttpClientBuilder clientBuilder = HttpClientBuilder.create();
    private final CookieStore cookies = new BasicCookieStore();
	
	private String proxyHost, proxyProtocol;
	private int proxyPort;

	public CloseableHttpClient buildHttpClient() {
		clientBuilder.setProxy(null);
		return clientBuilder.build();
	}
	
	public CloseableHttpClient buildHttpProxyClient() {
		HttpHost proxy = new HttpHost(proxyHost, proxyPort, proxyProtocol);
		clientBuilder.setProxy(proxy);
		return clientBuilder.build();
	}
	
    private void configureNaiveTrustStrategy() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
        SSLContextBuilder builder = new SSLContextBuilder();
        TrustStrategy strategy = new TrustStrategy() {
            @Override
            public boolean isTrusted(X509Certificate[] xcs, String hostname) throws CertificateException {
                return true;
            }
        };
        
        builder.loadTrustMaterial(null, strategy);
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                builder.build(), NoopHostnameVerifier.INSTANCE
        );
        
        clientBuilder.setSSLSocketFactory(sslsf);
    }

    public Map<String, String> getCookiesAsMap() {
        Map<String, String> map = new HashMap<>();
        Date now = new Date();
        for (Cookie c : cookies.getCookies()) {
            if (!c.isExpired(now)) {
                map.put(c.getName(), c.getValue());
            }
        }
        return map;
    }
    
    
    @Activate
    protected void activate(final JLPHttpClientConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPHttpClientConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPHttpClientConfiguration config) {
    }

    private synchronized void resetService(final JLPHttpClientConfiguration config) {
        proxyHost = config.getProxyHost();
        proxyPort = config.getProxyPort();
        proxyProtocol = config.getProxyProtocol();

        try {
            configureNaiveTrustStrategy();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException ex) {
            logger.error(ex.getMessage(), ex);
        }

        clientBuilder.setDefaultCookieStore(cookies);
    }
}