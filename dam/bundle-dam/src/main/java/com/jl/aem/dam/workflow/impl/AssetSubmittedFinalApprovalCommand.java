package com.jl.aem.dam.workflow.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;

@Component(immediate=true, service=AssetSubmittedFinalApprovalCommand.class)
public class AssetSubmittedFinalApprovalCommand implements AssetStatusCommand {
    
    private static final Logger logger = LoggerFactory.getLogger(AssetSubmittedFinalApprovalCommand.class);

    @Reference
    private Replicator replicator;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Override
    public void execute(String assetPath, final String userID) {
 
        logger.info("AssetApprovedCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            try {
                Map<String, Object> metaData = new HashMap<>();
                String targetFolder = JLPConstants.JOHNLEWIS_REVIEW_FOLDER;
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                if (metadataNode.hasProperty("cq:productReference")) {
                    Resource productResource = resourceResolver.getResource(metadataNode.getProperty("cq:productReference").getString());
                    Node productNode = productResource.adaptTo(Node.class);     
                    String directorate = JLWorkflowsUtils.encodePath(productNode.getProperty("directorate").getString());
                    String brandName = assetNode.getNode("jcr:content/metadata").getProperty(JLPConstants.JLP_BRAND_NAME).getString();
                    targetFolder += "/" + JLWorkflowsUtils.encodePath(directorate) + "/" + JLWorkflowsUtils.encodePath(brandName);
                    if (JLWorkflowsUtils.isVideo(assetNode)) {
                        targetFolder += "/videos";
                    } else if(JLWorkflowsUtils.isManual(assetNode)) {
                        targetFolder += "/manuals";
                    } else {
                        targetFolder += "/products";
                    }
                } else if (JLWorkflowsUtils.isLogo(assetNode)) {
                    targetFolder += "/logos";
                }
                if(assetNode.getPath().contains(JLPConstants.ARTWORKERS_ROOT_FOLDER)){
                    metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_PROVIDED_BY_ARTWORKER, Calendar.getInstance());
                }
                if(assetNode.getPath().contains(JLPConstants.IC_REVIEW_PATH)){
                    metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_IC_APPROVED_TIME, Calendar.getInstance());
                }
                resourceResolver.commit();
                
                metaData.put("targetFolder", targetFolder);
                metaData.put("targetFileName", assetNode.getProperty("jcr:content/metadata/" + JLPConstants.JLP_ITEM_ID).getString() + "." + 
                        assetNode.getProperty("jcr:content/metadata/dc:format").getString().split("/")[1]);
                metaData.put("nextStatus", AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL.toString());
                workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
            } catch (Exception e) {
                logger.error("Problem while trying to move asset to review folder.", e);
            }
        } catch (Exception e){
            logger.error("Can not publish new asset: " + assetPath, e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
}
