package com.jl.aem.dam.workflow.validation;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Logo Validator Configuration", description = "Service Configuration")
public @interface JLPLogoValidatorConfiguration {

    @AttributeDefinition(name ="Max Size", description="Max valid dimension size allowed", defaultValue="1000")
    public int getMaxSize() default 1000;
    @AttributeDefinition(name ="Min Size", description="Minimum valid dimension size allowed", defaultValue="1000")
    public int getMinSize() default 1000;
    @AttributeDefinition(name ="Extensions allowed", description="List of extensions allowed (lower case, no dot)", defaultValue={"png"})
    public String[] getExtensions() default {"png"};
    
}
