package com.jl.aem.dam.reports.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.jl.aem.dam.reports.models.results.JLSNIItem;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class JLSNICellValue {
    @ValueMapValue
    private String property;

    @RequestAttribute
    private JLSNIItem result;

    private String value;
    
    public JLSNICellValue() {
    }
    
    public JLSNICellValue(JLSNIItem resource, String property) {
       this.result = resource;
       this.property= property;
       init();
    }

    public String getValue() {
        return value;
    }
    
    @PostConstruct
    private void init() {
        if (result != null && property != null) {
           if ("briefPath".equals(property)) {
               value = result.getBriefPath();
           } else if ("briefName".equals(property)) {
               value = result.getBriefName();
           } else if ("shootStartDate".equals(property)) {
               value = result.getStartShootDate();
           } else if ("shootEndDate".equals(property)) {
               value = result.getEndShootDate();
           } else if ("dueDate".equals(property)) {
               value = result.getDueDate();
           } else if ("countAssets".equals(property)) {
               value = result.getCountAssets();
           } else if ("countProducts".equals(property)) {
               value = result.getCountProducts();
           }
        }
    }
}
