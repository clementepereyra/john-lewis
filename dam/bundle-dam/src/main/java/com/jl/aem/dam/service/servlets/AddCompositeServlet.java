package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/addCompositeFolder.json",
        "sling.servlet.methods=POST"})
public class AddCompositeServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        for(String path : items) {
            briefService.addCompositeFolder(path);
        }
        response.getWriter().write("{\"result\":\"OK\"}");
        response.setContentType("application/json");
    }
    

}
