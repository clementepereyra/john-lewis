package com.jl.aem.dam.workflow.validation;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Video  Validator Configuration", description = "Service Configuration")
public @interface JLPVideoValidatorConfiguration {

    @AttributeDefinition(name ="Max Size", description="Max valid size in Mb", defaultValue="2000")
    public int getMaxSize() default 2000;
    @AttributeDefinition(name ="Min Size", description="Min valid size in Mb", defaultValue="5")
    public int getMinSize() default 5;
    @AttributeDefinition(name ="Extensions allowed", description="List of extensions allowed (lower case, no dots)", defaultValue={"mp4"})
    public String[] getExtensions() default {"mp4"};

}
