package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.version.VersionManager;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamEvent;
import com.day.cq.dam.api.Rendition;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = WorkflowProcess.class,
        property = {"process.label=Process Asset 3rd Party Retouched"})
public class BriefAssetThirdPartyRetouchedWorkflowProcess
        extends AbstractBriefAssetRetouchedWorkflowProcess implements WorkflowProcess {

    private static final Logger log =
            LoggerFactory.getLogger(BriefAssetThirdPartyRetouchedWorkflowProcess.class);


    @Reference
    ResourceResolverFactory resolverFactory;
    
    @Reference
    EventAdmin eventAdmin;

    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession wfSession, MetaDataMap metadata)
            throws WorkflowException {
        try {
            Session session = wfSession.getSession();
            ResourceResolver resourceResolver = getResourceResolver(resolverFactory, session);

            String workflowTitle = item.getWorkflow().getWorkflowData().getMetaDataMap()
                    .get("workflowTitle", String.class);
            String productCode = workflowTitle.split("-")[0];
            String altCode = workflowTitle.split("-")[1];
            
            Node projectNode = getProjectFromPayload(item, session, resolverFactory);

            Node projectContentNode = projectNode.getNode(JCR_CONTENT);
            Node damFolderNode = resourceResolver.getResource(
                    projectContentNode.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            Node productNode = damFolderNode.hasNode(productCode)?damFolderNode.getNode(productCode):null;
            if (productNode == null) {
                NodeIterator productNodes = damFolderNode.getNodes();
                while (productNodes.hasNext()) {
                    Node candidateNode = productNodes.nextNode();
                    if (candidateNode.hasProperty("ean")) {
                        if (productCode.equals(candidateNode.getProperty("ean").getString())) {
                            productCode = candidateNode.getName();
                            break;
                        }
                    }
                }
            }

            if (projectContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
                String damFolderPath =
                        projectContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();

                StringBuilder folderPathSB = new StringBuilder(damFolderPath).append(SLASH)
                        .append(productCode).append(SLASH).append(altCode);

                Resource folderResource = resourceResolver.getResource(folderPathSB.toString());
                if (folderResource != null) {
                    Node folderNode = folderResource.adaptTo(Node.class);
                    NodeIterator nodIt = folderNode.getNodes();
                    while (nodIt.hasNext()) {
                        Node assetNode = (Node) nodIt.next();
                        if (assetNode.getPrimaryNodeType().getName().equals(DAM_ASSET)) {

                            Node assetContentNode = assetNode.getNode(JCR_CONTENT);
                            String retouchedAssetPath = null;
                            if (assetContentNode != null && assetContentNode
                                    .hasProperty(JLPConstants.THIRD_PARTY_ASSET_PATH)) {
                                retouchedAssetPath = assetContentNode
                                        .getProperty(JLPConstants.THIRD_PARTY_ASSET_PATH)
                                        .getString();
                            }
                            Resource retouchedAssetRes =
                                    resourceResolver.getResource(retouchedAssetPath);

                            Asset retouchedAsset = null;
                            if (retouchedAssetRes != null) {
                                retouchedAsset = retouchedAssetRes.adaptTo(Asset.class);
                            }

                            if (retouchedAsset != null) {
                                if (assetNode.canAddMixin("mix:versionable")) {
                                    assetNode.addMixin("mix:versionable");
                                    session.save();
                                }

                                VersionManager versionManager =
                                        session.getWorkspace().getVersionManager();
                                versionManager.checkpoint(assetNode.getPath());
                                session.save();

                                Asset asset = resourceResolver.getResource(assetNode.getPath())
                                        .adaptTo(Asset.class);

                                for (Rendition retouchedRendition : retouchedAsset
                                        .getRenditions()) {
                                    asset.addRendition(retouchedRendition.getName(),
                                            retouchedRendition.getStream(),
                                            retouchedRendition.getMimeType());
                                    eventAdmin.postEvent(DamEvent.renditionUpdated(asset.getPath(), session.getUserID(), retouchedRendition.getName()).toEvent());

                                }
                                session.save();
                                
                                if (BriefUtils.isCutoutBrief(projectContentNode)) {
                                    briefsService.processNewStatus(BriefUtils.getProjectPath(assetNode), productCode, altCode, BriefAssetStatus.PENDING_COLOUR_MATCH);
                                }
                                String retouchedAssetFolder =
                                        retouchedAssetRes.getParent().getPath();
//                                removeAssetNodeFromFolder(resourceResolver, session,
//                                        retouchedAssetFolder);

                                String prendingRetouchedAssetFolder = retouchedAssetFolder
                                        .replaceAll(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER,
                                                JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER);
                                removeAssetNodeFromFolder(resourceResolver, session,
                                        prendingRetouchedAssetFolder, asset.getName());
                                eventAdmin.postEvent(DamEvent.originalUpdated(asset.getPath(), session.getUserID()).toEvent());

                                return;
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("Error processing Request Asset 3rd Party Retouch", e);
        }
    }

}


