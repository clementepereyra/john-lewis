package com.jl.aem.dam.listener;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Asset Modified Event Listener Configuration", description = "Service Configuration", pid="com.jl.aem.dam.listener.AssetModifiedEventListernerConfigurator")
public @interface AssetModifiedEventListernerConfigurator {

    
    @AttributeDefinition(name ="Observed Paths", description="Paths observed by DAM workflows", defaultValue={"/content/dam"})
    String[] getObservedPaths() default {"/content/dam"};
    

}
