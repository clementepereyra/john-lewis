package com.jl.aem.dam.workflow.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.jl.aem.dam.workflow.step.helper.JLProductAssociator;
import com.jl.aem.dam.workflow.step.helper.ProductAssociatorHelper;

@Component(immediate = true, service = AssetPendingICCommand.class)
public class AssetPendingICCommand implements AssetStatusCommand {

    private static final Logger logger = LoggerFactory.getLogger(AssetPendingICCommand.class);
   
   
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private InvokeAEMWorkflow workflowInvoker;

    private static final String MOVE_ASSET_WORKFLOW =
            "/etc/workflow/models/dam/move_asset/jcr:content/model";


    @Override
    public void execute(String assetPath, final String userID) {
        logger.info("AssetPendingICCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            JLProductAssociator associator = new JLProductAssociator();
            associator.associateAssetWithProduct(assetNode, resourceResolver, session);
            Map<String, String> productInfo = ProductAssociatorHelper.getProductFromAsset(assetNode, resourceResolver, session);

            String targetFolder = JLPConstants.IC_REVIEW_PATH;
            String targetFileName = assetNode.getName();
            if (productInfo.get("productPath") != null) {
                Resource productResource = resourceResolver.getResource(productInfo.get("productPath"));
                Node productNode = productResource.adaptTo(Node.class);
                targetFolder += "/" + JLWorkflowsUtils.encodePath(productNode.getProperty("directorate").getString()) + "/" + JLWorkflowsUtils.encodePath(productNode.getProperty("brandName").getString());
                if (JLWorkflowsUtils.isVideo(assetNode)) {
                    targetFolder += "/videos";
                } else if(JLWorkflowsUtils.isManual(assetNode)) {
                    targetFolder += "/manuals";
                } else {
                    targetFolder += "/products";
                }
                if (StringUtils.isNotBlank(productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString())) {
                    String itemId = productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString();
                    itemId = JLWorkflowsUtils.addLeftZeros(itemId);
                    String extension = FilenameUtils.getExtension(assetNode.getPath());
                    //String path = FilenameUtils.getFullPath(assetNode.getPath());
                    String imageCategory =  productInfo.get("imageCat");
                    imageCategory = imageCategory.substring(imageCategory.lastIndexOf("/")+1);
                    if ("main".equals(imageCategory.toLowerCase().trim())) {
                        imageCategory = "";
                    }
                    Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                    metadataNode.setProperty(JLPConstants.JLP_ITEM_ID, itemId + imageCategory);
                    session.save();
                    targetFileName = itemId + imageCategory + "." +extension;
                }
            } else if (JLWorkflowsUtils.isLogo(assetNode)) {
                    targetFolder += "/logos";
                
            }
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder );
            metaData.put("targetFileName", targetFileName);
            workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);

             
        } catch(Exception e) {
            logger.error("Can not rename, associate and pass to Image Coordinator: " + assetPath, e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }


}
