package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.pim.ProductService;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/AddLookToBrief",
        "sling.servlet.methods=POST"})
public class AddLookToBriefServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefService;
    
    @Reference
    private ProductService productService;
    
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        StringBuilder validationResult = new StringBuilder();
        String lookName = request.getParameter("look_name");
        String namingConventions = request.getParameter("naming_conventions");
        String noOfAlts = request.getParameter("no_of_alts");

        if (StringUtils.isEmpty(lookName) ) {
            validationResult.append("ERROR - Look Name can not be empty\n");
        }
        if (StringUtils.isEmpty(namingConventions)) {
            validationResult.append("ERROR - Naming Convention can not be empty\n");
        }
        if (StringUtils.isEmpty(noOfAlts)) {
            validationResult.append("ERROR - Number of Alts can not be empty\n");
        }
        
        String productCodes = request.getParameter("product_codes");
        List<String> codes = toList(productCodes);
        for (String code : codes) {
            if (!productService.exist(code)) {
                validationResult.append("WARNING - " + code + " does not exist in AEM cache\n");
            }
        }
        Gson gson = new Gson();
        JsonResult theResult = new JsonResult();
        theResult.setResult(validationResult.toString());
        response.getWriter().write(gson.toJson(theResult));

        response.setContentType("application/json");
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        
        String projectPath = request.getParameter("project_path");
        String lookName = request.getParameter("look_name");
        String namingConventions = request.getParameter("naming_conventions");
        String shotDescription = request.getParameter("shot_description");
        String buyingOffice = request.getParameter("buying_office");
        String noOfAlts = request.getParameter("no_of_alts");
        String range = request.getParameter("range");
        String ids = request.getParameter("ids");
        String clientNotes = request.getParameter("client_notes");
        String productCodes = request.getParameter("product_codes");
        Gson gson = new Gson();
        JsonResult theResult = new JsonResult();
        theResult.setResult(briefService.addLookToLifestyleBrief(projectPath, lookName, namingConventions, shotDescription, buyingOffice, range, ids, clientNotes, toInt(noOfAlts), toList(productCodes)));
        response.getWriter().write(gson.toJson(theResult));
        response.setContentType("application/json");
    }

    private int toInt(String noOfAlts) {
        return Integer.parseInt(noOfAlts);
    }

    private List<String> toList(String productCodes) {
        List<String> result = new ArrayList<>();
        if (!StringUtils.isEmpty(productCodes)) {
            String[] codes = productCodes.split(",");
            for (String code : codes) {
                if (!StringUtils.isEmpty(code)) {
                    result .add(code.trim());
                }
            }
        }
        return result;
    }
    
    public static class JsonResult {
        private String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
        
    }

}
