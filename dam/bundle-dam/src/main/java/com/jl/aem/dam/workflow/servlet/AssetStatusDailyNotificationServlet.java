package com.jl.aem.dam.workflow.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.workflow.AssetStatusDailyNotificationService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/admin/assetStatusDailyNotification.json",
    "sling.servlet.methods=GET"
  }
)
public class AssetStatusDailyNotificationServlet extends SlingAllMethodsServlet{


    private static final long serialVersionUID = 5918127103396049392L;

    @Reference
    private AssetStatusDailyNotificationService assetStatusDailyNotificationService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws ServletException,
            IOException {
        assetStatusDailyNotificationService.execute();;
        response.setContentType("text/plain");
        response.getWriter().print("OK");
    }
    
}