package com.jl.aem.dam.briefs.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.exec.Route;
import com.jl.aem.dam.briefs.service.AltTaskCompleterService;
import com.jl.aem.dam.briefs.service.AssetTaskCompleterService;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = AltTaskCompleterService.class)
public class AltTaskCompleterServiceImpl implements AltTaskCompleterService {
    
    private static final Logger log = LoggerFactory.getLogger(AltTaskCompleterServiceImpl.class);
    private static final String PROVIDE_SHOOT_PATH = "/jcr:content/work/provide-shoot-";

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private AssetTaskCompleterService assetCompleter;

    @Override
    public boolean canComplete(String resourcePath, AutocompleteStatus newStatus) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource altResource = resourceResolver.getResource(resourcePath);
            if (altResource != null) {
                Node altNode = altResource.adaptTo(Node.class);
                if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                    BriefAssetStatus currentStatus = BriefAssetStatus.valueOf(altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                    if (newStatus == AutocompleteStatus.COLOUR_MATCH_NOT_NEEDED) {
                        return currentStatus == BriefAssetStatus.PENDING_COLOUR_MATCH;
                    }
                    if (altNode.hasProperty(JLPConstants.RETOUCH_MODE)) {
                        return false;
                    }
                    return validateStatus(currentStatus, newStatus); 
                }
                return false;
            }
            
        } catch (Exception e) {
            log.error("Error checking asset current status", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return false;
    }

    private boolean validateStatus(BriefAssetStatus currentStatus, AutocompleteStatus newStatus) {
        if (newStatus == AutocompleteStatus.ALT_NOT_NEEDED) {
            return currentStatus.equals(BriefAssetStatus.ASSET_REQUESTED);
        } else {
            return currentStatus.equals(BriefAssetStatus.ASSET_UPLOADED);
        }
    }

    @Override
    public void complete(String resourcePath, AutocompleteStatus newStatus, String comments) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource altResource = resourceResolver.getResource(resourcePath);
            if (altResource != null) {
                Node altNode = altResource.adaptTo(Node.class);
                if (newStatus == AutocompleteStatus.ALT_NOT_NEEDED) {
                    completeAltNotNeeded(altNode, resourceResolver, comments);
                } else {
                    NodeIterator altIterator = altNode.getNodes();
                    while (altIterator.hasNext()) {
                        Node childNode = altIterator.nextNode();
                        if (childNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                            if (childNode.getName().toLowerCase().indexOf("reshoot") <=0) {
                                assetCompleter.complete(childNode.getPath(), newStatus, comments);
                                break;
                            }
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            log.error("Error completing alt", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private void completeAltNotNeeded(Node altNode, ResourceResolver resourceResolver, String comments) throws Exception{
        String projectPath = BriefUtils.getProjectPathFromProductNode(altNode.getParent());
        String projectWorkPath = null;
        String productCode = null;
        String assetFolderName = null;
        String eanCode = null;
       
        
        
        assetFolderName = altNode.getName();
        Node productNode = altNode.getParent();
        if (productNode != null) {
            productCode = productNode.getName();
            eanCode = productNode.hasProperty("ean")?productNode.getProperty("ean").getString():"-";
        }


        Node projectWorkNode = null;
        if (productCode != null && assetFolderName != null) {
            projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                    .append(productCode).append("-").append(assetFolderName).toString();
            Resource projectWorkResurce = resourceResolver.getResource(projectWorkPath);

            if (projectWorkResurce != null) {
                projectWorkNode = projectWorkResurce.adaptTo(Node.class);
            } else {
                projectWorkPath = new StringBuilder(projectPath).append(PROVIDE_SHOOT_PATH)
                        .append(eanCode).append("-").append(assetFolderName).toString();
                projectWorkResurce = resourceResolver.getResource(projectWorkPath);
                if (projectWorkResurce != null) {
                    projectWorkNode = projectWorkResurce.adaptTo(Node.class);
                }
            }
        }
        
        com.adobe.granite.workflow.WorkflowSession graniteWfSession =
                resourceResolver.adaptTo(com.adobe.granite.workflow.WorkflowSession.class);

        if (graniteWfSession != null && projectWorkNode != null) {
            String workflowId = null;
            if (projectWorkNode.hasProperty("workflow.id")) {
                workflowId = projectWorkNode.getProperty("workflow.id").getString();
            }

            if (workflowId != null) {
                
                Node mainWorkflowNode = resourceResolver.getResource(workflowId).adaptTo(Node.class);
                Node workItemNode = mainWorkflowNode.getNode("workItems");
                Node workflowMetadataNode = mainWorkflowNode.getNode("data/metaData");
                NodeIterator workItemChildIterator = workItemNode.getNodes();
                if (workItemChildIterator.hasNext()) {
                    Node workItemInstanceNode = (Node) workItemChildIterator.next();
                    com.adobe.granite.workflow.exec.WorkItem workItem =
                            graniteWfSession.getWorkItem(workItemInstanceNode.getPath());

                    Node metaDataWorkItemNode = resourceResolver
                            .getResource(workItemInstanceNode.getPath() + "/metaData")
                            .adaptTo(Node.class);

                    String taskId = null;
                    if (metaDataWorkItemNode.hasProperty("taskId")) {
                        log.debug("Set as compleate taskId {} ", taskId);
                        taskId = metaDataWorkItemNode.getProperty("taskId").getString();
                        Node taskNode =
                                resourceResolver.getResource(taskId).adaptTo(Node.class);
                        taskNode.setProperty("status", "COMPLETE");
                        resourceResolver.commit();
                    }
                    
                    if (workflowMetadataNode != null) {
                        //workflowMetadataNode.setProperty("lastTaskAction", action);
                        workItem.getWorkflow().getWorkflowData().getMetaDataMap().put("lastTaskAction", "Alt Not Needed");
                        workItem.getMetaDataMap().put("comment", comments);
                        resourceResolver.commit();
                    }
                    
                    
                    log.debug("workItem id = ", workItem.getId());
                    List<Route> routes = graniteWfSession.getRoutes(workItem, false);
                    log.debug("Number of routes for the workItem = {}", routes.size());
                    // It is a linear workflow model.
                    Route route = routes.get(0);
                    if (route != null) {
                        log.debug("routing: {}",route.getName());
                        graniteWfSession.complete(workItem, routes.get(0));
                    }
                }
            }
        }
    }
    
    
        

        


    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }

}
