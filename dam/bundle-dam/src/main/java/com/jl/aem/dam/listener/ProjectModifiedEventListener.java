package com.jl.aem.dam.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.jl.aem.dam.briefs.workflow.BriefChangeProcessor;


@Component(name = "com.jl.dam.listener.ProjectModifiedEventListener", service = EventHandler.class, immediate = true, 
    property = {"event.topics=org/apache/sling/api/resource/Resource/*"})
public class ProjectModifiedEventListener implements EventHandler {

    //private static final Logger LOG = LoggerFactory.getLogger(ProjectModifiedEventListener.class);
    
    @Reference
    private BriefChangeProcessor briefChangeProcessor;
    
    @Override
    public void handleEvent(Event event) {
        String path = getPath(event);
        String topic = getTopic(event);
        String resourceType = getResourceType(event);
        String userId = getUserId(event);
        if (shouldProcess(path, topic, resourceType)) {
            briefChangeProcessor.process(path, userId);
        }
    }

    private String getResourceType(Event event) {
        if (event.getProperty("resourceType") != null) {
            return event.getProperty("resourceType").toString();
        }
        return "N/A";
    }
    
    private String getUserId(Event event) {
        if (event.getProperty("userid") != null) {
            return event.getProperty("userid").toString();
        }
        return "N/A";
    }

    private boolean shouldProcess(String path, String topic, String resourceType) {
        if (path.startsWith("/content/project") && resourceType.equals("cq/gui/components/projects/admin/card/projectcontent")) {
            return true;
        }
        return false;
    }

    private String getTopic(Event event) { 
        return event.getTopic();
    }

    private String getPath(Event event) {
        return event.getProperty("path").toString();
    }

}
