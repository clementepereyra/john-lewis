package com.jl.aem.dam.workflow.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.step.helper.ProductAssociatorHelper;

@Component(immediate=true, service = JLPLogoValidator.class)
@Designate(ocd = JLPLogoValidatorConfiguration.class)
public class JLPLogoValidator extends JLPCommonValidator implements JLPAssetValidator {

    private static final Logger logger = LoggerFactory.getLogger(JLPSquareValidator.class);
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    private long minSize;
    private long maxSize;
    private List<String> extensions;
    
    @Override
    public boolean canValidate(String assetPath) {
        if (ProductAssociatorHelper.isBrandLogo(assetPath)) {
            return true;
        }
        return false;
    }
    
    @Override
    public String validateAsset(String assetPath) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user", e);
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                long assetHeight = getAssetHeight(metadataNode);
                long assetWidth = getAssetWidth(metadataNode);
                if (assetHeight != assetWidth) {
                    return "ERROR: Asset aspect ratio is not square";
                }
                if (assetHeight > maxSize) {
                    return "ERROR: Asset is larger than " + maxSize + " px";
                }
                if (assetHeight < minSize) {
                    return "ERROR: Asset height is below " + minSize + " px";
                }
                String assetExtension = getAssetExtension(assetNode);
                if (!extensions.contains(assetExtension)) {
                    return "ERROR: Asset extension is not allowed";
                }

            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return JLPAssetValidator.SUCCESS_RESPONSE;
    }

    @Activate
    protected void activate(final JLPLogoValidatorConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPLogoValidatorConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPLogoValidatorConfiguration config) {
    }

    private synchronized void resetService(final JLPLogoValidatorConfiguration config) {
        maxSize = config.getMaxSize();
        minSize = config.getMinSize();
        extensions = Arrays.asList(config.getExtensions());
    }
    
}
