package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.service.impl.BriefsServiceImpl;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/reopenForRetouch.json",
        "sling.servlet.methods=POST"})
public class ReopenForRetouchServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    private static final String TEMP_PATH = "/content/dam/projects/temp/retouch";
    
    private static final Logger logger = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        String tempPath = null;
        for(String path : items) {
            tempPath = reopenForRetouch(path, resourceResolver);
        }
        response.getWriter().write("{\"result\":\""+ tempPath + "\"}");
        response.setContentType("application/json");
    }

    private String reopenForRetouch(String path, ResourceResolver resourceResolver) {
        try {
            String targetPath = path.replace(JLPConstants.JOHNLEWIS_SCENE7_FOLDER, TEMP_PATH);
            String targetFolder = targetPath.substring(0, targetPath.lastIndexOf("/"));          
            BriefUtils.reopenAssetForRetouch(path, targetFolder, resourceResolver);       
            return targetFolder;
        } catch (Exception e) {
            logger.error("Can not reopen asset: " + path, e);
            return "ERROR";
        }
    }
    
    
}
