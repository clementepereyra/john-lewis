package com.jl.aem.dam.reports.models.helper;

import org.apache.sling.api.SlingHttpServletRequest;

import com.adobe.acs.commons.reports.api.ResultsPage;

public interface JLReportExecutorHelper {

   ResultsPage fetchResults(SlingHttpServletRequest request);
   String getDetails();
   String getParameters();
    
}
