package com.jl.aem.dam.workflow.jobs;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.NextToExpireNotificationService;

/**
 * Schedulable job to sent a daily notification (1:00 am) reporting asset that are next to expire
 * 
 * @author hgercek
 *
 */

@Component(immediate = true, name = "Next to Expire Notification Job", service = {Runnable.class},
        property = {"scheduler.expression=0 0 1 * * ? "})
public class NextToExpireNotificationJob implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(NextToExpireNotificationJob.class);

    @Reference
    private NextToExpireNotificationService nextToExpireNotificationService;

    @Reference
    private SlingSettingsService slingSettingsService;


    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes().contains("author");
        if (isAuthorInstance) {
            logger.debug("Running Next to Expire Notification job");
            nextToExpireNotificationService.execute();
            logger.debug("Next to Expire Notification job completed");
        }
    }

}
