package com.jl.aem.dam.workflow.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkflowData;
//Adobe CQ Workflow APIs
import com.day.cq.workflow.model.WorkflowModel;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;

//This is a component so it can provide or consume services
@Component(service=InvokeAEMWorkflow.class, immediate=true)
public class InvokeAEMWorkflowImpl implements InvokeAEMWorkflow {

	@Reference
	private WorkflowService workflowService;

	@Reference
	private ResourceResolverFactory resolverFactory;

	private static Logger logger = LoggerFactory.getLogger(InvokeAEMWorkflowImpl.class);
	
	@Override
	public String startWorkflow(String workflowName, String workflowContent, Map<String, Object> metaData) {
		
		try {
			ResourceResolver resourceResolver = null;
			try {       
	            Map<String, Object> param = new HashMap<String, Object>();
	            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
	            resourceResolver = resolverFactory.getServiceResourceResolver(param);

	        } catch (Exception e) {
	            logger.error("Can not login system user and get resource resolver " + e.getMessage());
	            return null;
	        }
			Session session = resourceResolver.adaptTo(Session.class);

			// Create a workflow session
			WorkflowSession wfSession = workflowService.getWorkflowSession(session);

			// Get the workflow model
			WorkflowModel wfModel = wfSession.getModel(workflowName);

			// Get the workflow data
			// The first param in the newWorkflowData method is the payloadType.
			// Just a fancy name to let it know what type of workflow it is
			// working with.
			WorkflowData wfData = wfSession.newWorkflowData("JCR_PATH", workflowContent);

			// Run the Workflow.
			wfSession.startWorkflow(wfModel, wfData, metaData);

			return workflowName + " has been successfully invoked on this content: " + workflowContent;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}