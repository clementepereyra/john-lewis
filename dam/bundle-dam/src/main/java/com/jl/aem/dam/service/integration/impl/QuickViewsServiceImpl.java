package com.jl.aem.dam.service.integration.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.service.integration.QuickViewsService;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = QuickViewsService.class)
public class QuickViewsServiceImpl implements QuickViewsService {

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    private static final Logger logger = LoggerFactory.getLogger(QuickViewsServiceImpl.class);

    private static final String QUICKVIEWS_ROOT_PATH = "/content/dam/collections/public/quickviews";
    private static final String NAV_ROOT_PATH = "/apps/cq/core/content/nav/quickviews";
    
    private static final String QUICKVIEWS_NODE_NAME_PREFIX = "quickview_";
    
    private static final int QUICKVIEWS_AMOUNT = 14;
    
    @Override
    public void createOrUpdateQuickViews() {
        ResourceResolver resourceResolver  = getResourceResolver();
        try {
            Node quickViewsRoot = JcrUtils.getOrCreateByPath(QUICKVIEWS_ROOT_PATH, "nt:unstructured", resourceResolver.adaptTo(Session.class));
            Node quickViewsNavRoot = JcrUtils.getOrCreateByPath(NAV_ROOT_PATH, "nt:unstructured", resourceResolver.adaptTo(Session.class));

            for (int i = 0; i < QUICKVIEWS_AMOUNT; i++) {
                Node quickViewNode = null;
                if (quickViewsRoot.hasNode(QUICKVIEWS_NODE_NAME_PREFIX + i)) {
                    quickViewNode = quickViewsRoot.getNode(QUICKVIEWS_NODE_NAME_PREFIX + i);
                } else {
                    quickViewNode = quickViewsRoot.addNode(QUICKVIEWS_NODE_NAME_PREFIX + i, "nt:unstructured");
                    quickViewNode.setProperty("sling:resourceType", "dam/smartcollection");                   
                    quickViewNode.setProperty("jcr:created", Calendar.getInstance());
                    quickViewNode.setProperty("jcr:createdBy", quickViewNode.getSession().getUserID());
                    quickViewNode.setProperty("sling:resourceSuperType", "sling/collection");
                    quickViewNode.setProperty("sling:resourceType", "dam/smartcollection");
                }
                String collectionName = getCollectionName(i);
                if (quickViewsNavRoot.hasNode("dayago" + i)) {
                    Node navNode = quickViewsNavRoot.getNode("dayago" + i);
                    navNode.setProperty("jcr:title", collectionName);
                }
                quickViewNode.setProperty("jcr:title", collectionName);

                String querytemplate = "path=" + JLPConstants.PROJECTS_DAM_PATH + "\n" 
                        + "type=dam:Asset\n" 
                        + "property=jcr:content/metadata/" + JLPConstants.JLP_BRIEF_ASSET_STATUS + "\n"
                        + "property.operation=exists\n"
                        + "1_daterange.p.or=true\n"
                        + "1_daterange.property=./jcr\\:content/jcr\\:lastModified\n"
                        + "1_daterange.lowerBound={lowerBound}\n"
                        + "1_daterange.upperBound={upperBound}\n"
                        + "p.guessTotal=true\n" 
                        + "savedSearches@Delete=\n" 
                        + "mainasset=true\n" 
                        + "createsmartcollection=true\n" 
                        + "location=asset\n"
                        + "orderby=@jcr:content/metadata/"+ JLPConstants.JLP_PRODUCT_CODES;
  
                
                quickViewNode.setProperty("dam:query", querytemplate.replace("{lowerBound}", "" + getLowerBound(i)).replace("{upperBound}", "" + getUpperBound(i)));
                resourceResolver.commit();
            }
        } catch (Exception e) {
            logger.error("Can't create or update quick views", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        
    }

    private long getLowerBound(int i) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_YEAR, i*(-1));
        
        return cal.getTimeInMillis();
    }
    
    private long getUpperBound(int i) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        cal.add(Calendar.DAY_OF_YEAR, i*(-1));
        
        return cal.getTimeInMillis();
    }


    private String getCollectionName(int i) {
        
        if (i == 0) {
            //Today
            return "Assets Modified Today";
        } else if (i == 1) {
            //Yesterday
            return "Assets Modified Yesterday";
        } else {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, i*(-1));
            // Generic
            return "Assets Modified " + normalizeDateField(cal.get(Calendar.DAY_OF_MONTH)) + "/" + normalizeDateField(cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
        }
    }
    
    private String normalizeDateField(int dateFieldValue) {
        if (dateFieldValue < 10) {
            return "0" + dateFieldValue;
        }
        return "" + dateFieldValue;
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            logger.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }

        return resourceResolver;
    }
}
