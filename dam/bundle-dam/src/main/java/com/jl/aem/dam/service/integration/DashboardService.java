package com.jl.aem.dam.service.integration;

import java.util.List;

public interface DashboardService {

    
    public PieChartView getCurrentStatus();
    public PieChartView getBriefStatus(String projectPath);
    public List<String> getProductsRequested(String projectPath);
    public List<String> getProductsWithSamples(String projectPath);
    public long getAssetRequestedCount(String projectPath);
    public long getAssetUploadedCount(String projectPath);
    public int getAllProductsRequestedCount(String projectPath);
    public int getAllProductsWithSamplesCount(String projectPath);

}
