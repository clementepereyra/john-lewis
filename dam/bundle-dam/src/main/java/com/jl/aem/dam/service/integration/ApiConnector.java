package com.jl.aem.dam.service.integration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader;

@Component( immediate = true, service=ApiConnector.class)
public class ApiConnector {

    private static final Logger logger = LoggerFactory.getLogger(ApiConnector.class);
    
    private final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    
    @Reference
    HTTPClientFactory httpClientFactory;
    
    public Map<String, Object> getJsonObject(String url) throws IOException, IllegalStateException {
        logger.info("Calling GET " + url);
        HttpGet getMethod = new HttpGet(url);
        try (CloseableHttpClient client = httpClientFactory.buildHttpClient()) {
            HttpResponse response = client.execute(getMethod);
            InputStream responseStream = response.getEntity().getContent();
            InputStreamReader reader = new InputStreamReader(responseStream);
            JsonReader jsonReader = new JsonReader(reader);
            return gson.fromJson(jsonReader, LinkedTreeMap.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public <T> T postJsonObject(final String url, final Map<String, String> headers, final Type beanType, final String body) throws IOException, IllegalStateException {
        logger.info("Calling Post " + url);
        HttpPost postMethod = new HttpPost(url);
        StringEntity postingString = new StringEntity(body);//gson.tojson() converts your pojo to json
        postMethod.setEntity(postingString);
        postMethod.setHeader("Content-type", "application/json");
        if(headers != null) {
            for (Entry<String, String> entry : headers.entrySet()) {
                postMethod.addHeader(entry.getKey(), entry.getValue());
            }
        }
            
        HttpResponse response = null;
        try (CloseableHttpClient client = httpClientFactory.buildHttpClient()) {
            response = client.execute(postMethod);
            //InputStream responseStream = response.getEntity().getContent();
            //InputStreamReader reader = new InputStreamReader(responseStream);
            //JsonReader jsonReader = new JsonReader(reader);
            String responseBody = EntityUtils.toString(response.getEntity());
            logger.debug(responseBody);
            return gson.fromJson(responseBody, beanType);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public boolean postURLEncodedForm(final String url, final Map<String, String> headers, final Map<String,String> fields) {
        int status = postURLEncodedFormAndGetStatus( url, headers, fields);
        return !(status >= 400 || status < 0);
    }
    
    public int postURLEncodedFormAndGetStatus(final String url, final Map<String, String> headers, final Map<String,String> fields) {
        HttpPost post = new HttpPost(url);
        if(headers != null) {
            for (Entry<String, String> entry : headers.entrySet()) {
                post.addHeader(entry.getKey(), entry.getValue());
            }
        }
        List<NameValuePair> nvps = new ArrayList<>();
        for (Entry<String, String> entry : fields.entrySet()) {
            nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        try {
            post.setEntity(new UrlEncodedFormEntity(nvps));
        } catch (UnsupportedEncodingException e) {
            logger.error("Can not crete post call to " + url, e);
            return -1;
        }
        HttpResponse response = null;
        try (CloseableHttpClient client = httpClientFactory.buildHttpClient()) {
            response = client.execute(post);
            //InputStream responseStream = response.getEntity().getContent();
            //InputStreamReader reader = new InputStreamReader(responseStream);
            //JsonReader jsonReader = new JsonReader(reader);
          
            return response.getStatusLine().getStatusCode();
          
        } catch (Exception e) {
            logger.error("Can not post to " + url, e);
            return -1;
        }
    }

    public <T> T getServiceResponse(String url, Type bean) throws IOException, IllegalStateException {
        logger.info("Calling GET " + url);
        HttpGet getMethod = new HttpGet(url);
        try (CloseableHttpClient client = httpClientFactory.buildHttpClient()) {
            HttpResponse response = client.execute(getMethod);
            InputStream responseStream = response.getEntity().getContent();
            InputStreamReader reader = new InputStreamReader(responseStream);
            JsonReader jsonReader = new JsonReader(reader);
            return gson.fromJson(jsonReader, bean);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }
    
    public int getResponseCode(String url, String method) throws IOException, IllegalStateException {
        logger.info("Calling GET " + url);
        HttpUriRequest httpMethod = getMethod(method, url);
        
        try (CloseableHttpClient client = httpClientFactory.buildHttpClient()) {
            HttpResponse response = client.execute(httpMethod);
            return response.getStatusLine().getStatusCode();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    private HttpUriRequest getMethod(String method, String url) {
        if ("get".equalsIgnoreCase(method)) {
            return new HttpGet(url);
        } else if ("put".equalsIgnoreCase(method)) {
            return new HttpPut(url);
        } else if ("post".equalsIgnoreCase(method)) {
            return new HttpPost(url);
        } else if ("delete".equalsIgnoreCase(method)) {
            return new HttpDelete(url);
        }
        return null;
    }
}
