package com.jl.aem.dam.briefs.service;

import javax.jcr.Node;

public interface NotifyNewAssetsService {
	
	public void notifyOnNewAsset(Node assetCreated);

}
