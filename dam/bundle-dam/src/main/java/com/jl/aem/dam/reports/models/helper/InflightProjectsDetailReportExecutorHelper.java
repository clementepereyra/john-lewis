package com.jl.aem.dam.reports.models.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.reports.api.ResultsPage;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.reports.models.JLContentReportExecutor;
import com.jl.aem.dam.reports.models.results.InflightProjectdetailItem;
import com.jl.aem.dam.reports.models.results.JLContentItem;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

public class InflightProjectsDetailReportExecutorHelper implements JLReportExecutorHelper {
    
    private static final Logger log = LoggerFactory.getLogger(JLContentReportExecutor.class);
    
    private Map<String, String> parameters  = new HashMap<String, String>();
    
    private static final String START_DATE_LOWER_BOUND_PARAM = "startDateLowerBound";
    private static final String START_DATE_UPPER_BOUND_PARAM = "startDateUpperBound";
    private static final String DUE_DATE_LOWER_BOUND_PARAM = "dueDateLowerBound";
    private static final String DUE_DATE_UPPER_BOUND_PARAM = "dueDateUpperBound";
    
    @Override
    public ResultsPage fetchResults(SlingHttpServletRequest request) {
    	log.info("Inside InflightProjectsDetailReportExecutorHelper");
        List<Object> results = new ArrayList<>();
        try {
            prepareParameters(request);
            
            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            List<Node> briefs = new ArrayList<Node>();
            
            StringBuilder queryString = new StringBuilder("SELECT s.* "
                    + " FROM [nt:unstructured] AS s "
                    + " WHERE ISDESCENDANTNODE(s, [/content/projects]) "
                    + " AND s.[sling:resourceType]='cq/gui/components/projects/admin/card/projectcard'"
                    + " AND s.[jcr:content/JLPStatus] = 'ACTIVE'"
                    ); 
            if (parameters.get(START_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(START_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(START_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(START_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]<=" + upperBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(DUE_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(DUE_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]<=" + upperBound.getTime());
                }
            }
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery(queryString.toString(), javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            Date currentDate = new Date();
            while (iterator.hasNext()) {
                Node brief = iterator.nextNode();
                Date shootStartDate = getBriefDate(brief, "project.shootstartDate");
                Date shootEndDate = getBriefDate(brief, "project.shootendDate");
                if (shootStartDate!= null&& shootEndDate != null && shootStartDate.getTime() <= currentDate.getTime() && shootEndDate.getTime() >= currentDate.getTime()) {
                    briefs.add(brief);
                }
                    
            }
            
            for (Node brief : briefs) {
                InflightProjectdetailItem item = new InflightProjectdetailItem();
                BriefSummary summary = getBriefSummary(brief, resourceResolver);
                item.setAssetsDelivered(summary.getAssetsDelivered());
                item.setBriefType(getBriefType(brief));
                item.setCreator(getBriefCreator(brief));
                item.setDateCreated(getBriefDateCreation(brief));
                item.setDeliveryDate(getBriefDeliveryDate(brief));
                item.setMarkupNeeded(summary.getMarkupNeeded());
                item.setPendingReShoot(summary.getPendingReshoot());
                item.setPendingRetouch(summary.getPendingRetouch());
                item.setPendingShot(summary.getPendingShoot());
                item.setProjectName(getProjectName(brief));
                item.setSamplesReceived(summary.getSamplesReceived());
                item.setAssetsRetouched(summary.getAssetsRetouched());
                item.setProjectPath(brief.getPath());
                item.setPendingColourMatch(summary.getPendingColourMatch());
                results.add(item);
            }

        } catch (Exception e) {
            log.error("Can't create inflight report", e);
        }
        ResultsPage page = new ResultsPage(results, results.size(), 0);
        return page;

    }

    private BriefSummary getBriefSummary(Node brief, ResourceResolver resourceResolver) {
        BriefSummary result = new BriefSummary();
        int samples = 0;
        Map<BriefAssetStatus, Integer> statusMap = new HashMap<>();
        statusMap.put(BriefAssetStatus.ASSET_DELIVERED, 0);
        statusMap.put(BriefAssetStatus.ASSET_REQUESTED, 0);
        statusMap.put(BriefAssetStatus.ASSET_RETOUCHED, 0);
        statusMap.put(BriefAssetStatus.ASSET_UPLOADED, 0);
        statusMap.put(BriefAssetStatus.MARK_UP_NEEDED, 0);
        statusMap.put(BriefAssetStatus.RE_SHOOT_NEEDED, 0);
        statusMap.put(BriefAssetStatus.READY_TO_RETOUCH, 0);
        statusMap.put(BriefAssetStatus.PENDING_COLOUR_MATCH, 0);
        List<Node> products = getItemsInBrief(brief, resourceResolver);
        for (Node prodNode : products) {
            if ( productHasSamples(resourceResolver, prodNode)) {
                samples++;
            }
            try {
                NodeIterator it = prodNode.getNodes();
                while (it.hasNext()) {
                    Node assetNode = it.nextNode();
                    if (assetNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                        BriefAssetStatus status = BriefAssetStatus.valueOf(assetNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                        statusMap.put(status, statusMap.get(status)+1);
                    }
                }
            } catch (Exception e) {
                log.error("Can't get assets summary for product.", e);
            }
        } 
        result.setAssetsDelivered(statusMap.get(BriefAssetStatus.ASSET_DELIVERED).toString());
        result.setAssetsRetouched(statusMap.get(BriefAssetStatus.ASSET_RETOUCHED).toString());
        result.setMarkupNeeded(statusMap.get(BriefAssetStatus.MARK_UP_NEEDED).toString());
        result.setPendingReshoot(statusMap.get(BriefAssetStatus.RE_SHOOT_NEEDED).toString());
        result.setPendingRetouch(statusMap.get(BriefAssetStatus.ASSET_UPLOADED).toString());
        result.setPendingShoot(statusMap.get(BriefAssetStatus.ASSET_REQUESTED).toString());
        result.setPendingColourMatch(statusMap.get(BriefAssetStatus.PENDING_COLOUR_MATCH).toString());
        if (products.size()>0) {
            result.setSamplesReceived(""+ (samples*100/products.size()));
        } else {
            result.setSamplesReceived("0");
        }

        return result;
    }

    private boolean productHasSamples(ResourceResolver resourceResolver, Node prodNode) {
        try {
            if (prodNode.hasProperty("productPath")) {
                Resource skuResource = resourceResolver.getResource("productPath");
                if (skuResource != null) {
                    Node skuNode = skuResource.adaptTo(Node.class);
                    if (skuNode.hasProperty("quantityReceived")) {
                        long quantity = skuNode.getProperty("quantityReceived").getLong();
                        if (quantity>0) {
                            return true;
                        }
                    }
                }
            } else if (prodNode.hasProperty(JLPConstants.JLP_RECONCILE_PRODUCT)) {
                if (prodNode.hasProperty("quantityReceived")) {
                    long quantity = prodNode.getProperty("quantityReceived").getLong();
                    if (quantity>0) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.error ("Can't calculate prod samples for report", e);
        }
        return false;
    }

    private String getProjectName(Node brief) {
        String projectName = "N/A";
        try {
            Node jcrContent = brief.getNode("jcr:content");
            projectName = jcrContent.getProperty("jcr:title").getString();
        } catch (Exception e) {
            log.error("Can't get project name", e);
        }
        return projectName;
    }

    private String getBriefDeliveryDate(Node brief) {
        String projectName = "N/A";
        try {
            Node jcrContent = brief.getNode("jcr:content");
            projectName = jcrContent.getProperty("project.dueDate").getString().substring(0, 10);
        } catch (Exception e) {
            log.error("Can't get project name", e);
        }
        return projectName;
    }

    private String getBriefDateCreation(Node brief) {
        String projectCreation = "N/A";
        try {
            projectCreation = brief.getProperty("jcr:created").getString().substring(0, 10);
        } catch (Exception e) {
            log.error("Can't get project creation date", e);
        }
        return projectCreation;
    }

    private String getBriefCreator(Node brief) {
        String projectCreation = "N/A";
        try {
            projectCreation = brief.getProperty("jcr:createdBy").getString();
        } catch (Exception e) {
            log.error("Can't get project creator", e);
        }
        return projectCreation;
    }

    @Override
    public String getDetails() {
        return "<dl>" + "Inflight Content Report Executor" + "</dl>";
    }

    @Override
    public String getParameters() {
        return "";
    }

    
    private void prepareParameters(SlingHttpServletRequest request) {
        parameters = new HashMap<String, String>();
        @SuppressWarnings("unchecked")
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
          String key = paramNames.nextElement();
          parameters.put(key, StringEscapeUtils.escapeSql(request.getParameter(key)));
        }
    }
    
    
    
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Date parseDate(String stringDate) {
        Date date = null;
        String sanitized = stringDate;
        if (stringDate.length() >= 10) {
            sanitized = stringDate.substring(0, 10);
            try { 
                date = sdf.parse(sanitized);
            } catch (Exception e) {
                log.error("Error parsing date", e);
            }
        }
        return date;
    }
    
    private Date getBriefDate(Node brief, String string) {
        Node jcrContent;
        try {
            jcrContent = brief.getNode("jcr:content");
            if (jcrContent.hasProperty(string) && jcrContent.getProperty(string).getLength() > 0) {
                return jcrContent.getProperty(string).getDate().getTime();
            }
        } catch (Exception e) {
            log.error("Can't parse date", e);
        }
        return null;
    }

    

    private String getBriefType(Node brief) {
        try {
            Node projectContentNode = brief.getNode("jcr:content");
            if (BriefUtils.isCutoutBrief(projectContentNode)) {
                return "cutout";
            } else if (BriefUtils.isLifestyleBrief(projectContentNode)) {
                return "lifestyle";
            } else if (BriefUtils.isRetouchBrief(projectContentNode)) {
                return "retouch";
            }
        } catch (Exception e) {
            log.error("Can't get brief type for report", e);
        }
        return null;
    }
    
    private List<Node> getItemsInBrief(Node brief, ResourceResolver resourceResolver) {
        List<Node> results = new ArrayList<>();
        try {
            Node damNode = resourceResolver.getResource(brief.getNode("jcr:content").getProperty("damFolderPath").getString()).adaptTo(Node.class);
            NodeIterator it = damNode.getNodes();
            while (it.hasNext()) {
                Node productNode = it.nextNode();
                if (productNode.hasProperty("stockNumber")
                      || productNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)
                        ) {
                    results.add(productNode);
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return results;
    }

    
    
    private static class BriefSummary {
        private String pendingShoot;
        private String pendingRetouch;
        private String assetsRetouched;
        private String assetsDelivered;
        private String markupNeeded;
        private String pendingReshoot;
        private String samplesReceived;
        private String pendingColourMatch;
        public String getPendingShoot() {
            return pendingShoot;
        }
        public void setAssetsRetouched(String string) {
            this.assetsRetouched = string;
        }
        public void setPendingShoot(String pendingShoot) {
            this.pendingShoot = pendingShoot;
        }
        public String getPendingRetouch() {
            return pendingRetouch;
        }
        public void setPendingRetouch(String pendingRetouch) {
            this.pendingRetouch = pendingRetouch;
        }
        public String getAssetsDelivered() {
            return assetsDelivered;
        }
        public void setAssetsDelivered(String assetsDelivered) {
            this.assetsDelivered = assetsDelivered;
        }
        public String getMarkupNeeded() {
            return markupNeeded;
        }
        public void setMarkupNeeded(String markupNeeded) {
            this.markupNeeded = markupNeeded;
        }
        public String getPendingReshoot() {
            return pendingReshoot;
        }
        public void setPendingReshoot(String pendingReshoot) {
            this.pendingReshoot = pendingReshoot;
        }
        public String getSamplesReceived() {
            return samplesReceived;
        }
        public void setSamplesReceived(String samplesReceived) {
            this.samplesReceived = samplesReceived;
        }
        public String getAssetsRetouched() {
            return assetsRetouched;
        }
        public String getPendingColourMatch() {
            return pendingColourMatch;
        }
        public void setPendingColourMatch(String pendingColourMatch) {
            this.pendingColourMatch = pendingColourMatch;
        }
        
        
    }
}
