package com.jl.aem.dam.service.integration;

import java.util.List;

public class PieChartView {

    private final List<String> xAxis;
    private final List<Long> yAxis;
    private final List<String> series;

    public PieChartView(List<String> xAxis, List<Long> yAxis, List<String> series) {
        super();
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.series = series;
    }

    public List<String> getxAxis() {
        return xAxis;
    }

    public List<Long> getyAxis() {
        return yAxis;
    }

    public List<String> getSeries() {
        return series;
    }


}
