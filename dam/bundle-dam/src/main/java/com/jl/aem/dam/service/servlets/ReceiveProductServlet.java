package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/ReceiveProduct",
    "sling.servlet.methods=POST"
  }
)
public class ReceiveProductServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 4904472777745815851L;

	private static final Logger log = LoggerFactory.getLogger(ReceiveProductServlet.class);

    @Reference
    private SlingRepository repository;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private QueryBuilder builder;


    public void bindRepository(SlingRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        try {
            RequestParameter[] pathParameters = request.getRequestParameters("path");
            List<String> paths = new ArrayList<>();
            for (RequestParameter pathParameter : pathParameters) {
                paths.add(pathParameter.toString());
            }
            for (String path : paths) {
                String quantity = request.getRequestParameter("quantity").toString();
                           
                Resource productResource = getProductResource(path, resourceResolver);
                Node productNode = productResource.adaptTo(Node.class);
                if (productNode != null) {
                    Node historyNode = productNode.hasNode("quantityHistory")?productNode.getNode("quantityHistory"):productNode.addNode("quantityHistory", "nt:unstructured");
                    
                    Node newEntry = historyNode.addNode("receive"+ System.currentTimeMillis());
                    newEntry.setProperty("quantityReceived", quantity);
                    newEntry.setProperty("type", "receive"); 
                    newEntry.setProperty("user", session.getUserID());
                    Calendar cal = Calendar.getInstance();
                    
                    UserManager userManager = productResource.getResourceResolver().adaptTo(UserManager.class);
                    User user = (User) userManager.getAuthorizable(session.getUserID());
                    String lastName = user.getProperty("./profile/familyName")!=null?user.getProperty("./profile/familyName")[0].getString():"";
                    String firstName = user.getProperty("./profile/givenName")!=null?user.getProperty("./profile/givenName")[0].getString():"";        
                    newEntry.setProperty("userName", firstName + " " + lastName);
                    newEntry.setProperty("date", cal);
                    
                    long currentQuantity = productNode.hasProperty("quantityReceived")?productNode.getProperty("quantityReceived").getLong():0l;
                    currentQuantity += Long.parseLong(quantity); 
                    productNode.setProperty("quantityReceived", currentQuantity);
                    productResource.getResourceResolver().commit();
                }
            }
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in DespatchProductServlet " + e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private Resource getProductResource(String path, ResourceResolver resourceResolver) {
        Resource resource = resourceResolver.getResource(path);
        if (resource != null) {
            Node n = resource.adaptTo(Node.class);
            try {
                if (n.getPath().indexOf(JLPConstants.PRODUCTS_ROOT_FOLDER) >= 0) {
                    return resource;
                } else if (n.getPath().indexOf(JLPConstants.PROJECTS_DAM_PATH)>=0 && n.hasProperty("productPath")) {
                    Resource prodResource = resourceResolver.getResource(n.getProperty("productPath").getString());
                    return prodResource;
                }
            } catch (Exception e) {
                log.error("Can't obtain product info from node", e);
            }
        }
        return null;
    }    
}