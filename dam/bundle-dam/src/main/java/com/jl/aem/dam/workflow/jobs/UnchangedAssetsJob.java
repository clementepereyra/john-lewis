package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.UnchangedAssetNotificationService;

/**
 * Schedulable job to purge dead references of assets from sku objects.
 * 
 * @author fersoube
 *
 */
@Component(immediate = true, name = "Unchanged Assets Notification Job",service= {Runnable.class},
property ={"scheduler.expression=0 30 8 * * ? "})
public class UnchangedAssetsJob implements Runnable {

	private static final Logger logger = LoggerFactory
			.getLogger(UnchangedAssetsJob.class);

	@Reference
	private UnchangedAssetNotificationService unchangedAssetsService;

	@Reference
	private SlingSettingsService slingSettingsService;
	

	public void run() {
		boolean isAuthorInstance = slingSettingsService.getRunModes()
				.contains("author");
		if (isAuthorInstance) {
			logger.trace("Running Unchanged Asset Notification job");
			unchangedAssetsService.execute();
			logger.trace("Unchanged Asset Notification job completed");
		}
	}
}
