package com.jl.aem.dam.service.integration.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.service.integration.BriefDetailsService;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;
import com.jl.aem.dam.workflow.ProductConstants;

@Component(immediate = true, service = BriefDetailsService.class)
public class BriefDetailsServiceImpl implements BriefDetailsService {
    private static final Logger log = LoggerFactory.getLogger(BriefDetailsServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;
    @Override
    public List<String> getHeaders(final String projectPath) {
        String briefType = getBriefType(projectPath);
        if ("CUTOUT".equals(briefType) || "RETOUCH".equals(briefType)) {
            List<String> result = new ArrayList<>();
            result.add("Stock Number");
            result.add("Product Name");
            result.add("Product Description");
            result.add("Sample Stock");
            result.add("Product Type");
            result.add("Category");
            result.add("Client Style Notes");
            result.add("Brand");
            result.add("Buying Office");
            result.add("Web Sku");
            result.add("Ean Code");
            result.add("Image Required");
            result.add("Number of Alts");
            return result;
        } else if ("LIFESTYLE".equals(briefType)){
            List<String> result = new ArrayList<>();
            result.add("Lifestyle name");
            result.add("Look");
            result.add("Shot Description");    
            result.add("Buying Office");   
            result.add("Style Notes"); 
            result.add("Range");   
            result.add("IDS");
            result.add("No Alts");
            result.add("Product Code");    
            result.add("Product Description"); 
            result.add("Quantity Received");    
            result.add("Web Sku"); 
            result.add("EAN Code");
            return result;
        }
        return new ArrayList<>();
    }

    @Override
    public List<Map<String, String>> getRows(String projectPath) {
        String briefType = getBriefType(projectPath);
        ResourceResolver resourceResolver = getResourceResolver();
        List<Map<String,String>> result = new ArrayList<>();
        try {    
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node jcrContentNode = projectNode.getNode("jcr:content");
            String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
            Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
            NodeIterator productNodes = projectDamFolderNode.getNodes();
            while (productNodes.hasNext()) {
                Node productNode = productNodes.nextNode();
                if (productNode.getPrimaryNodeType().getName().equals("sling:Folder")
                        && productNode.getPath().indexOf("/" + JLPConstants.COMPOSITE_FOLDER) < 0 
                        && productNode.getPath().indexOf("/" + JLPConstants.REFERENCE_FOLDER) < 0) {
                    Map<String, String> row = null;
                    if ("CUTOUT".equals(briefType) || "RETOUCH".equals(briefType)) {
                        row = processCutoutItem(productNode);
                        result.add(row);
                    } else if ("LIFESTYLE".equals(briefType)) {
                        List<Map<String,String>> newRows = processLifestyleItem(productNode);
                        result.addAll(newRows);
                    }
                    
                }
            }
        
        } catch (Exception e) {
            log.error("Can not obtain brief details.", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }

        return result;
    }

    @Override
    public String getProjectName(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            return projectNode.getNode("jcr:content").getProperty("jcr:title").getString();
        } catch (Exception e) {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "Brief Detail";
    }
    
    private String getBriefType(String projectPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
            Node projectContentNode = projectNode.getNode("jcr:content");
            if (BriefUtils.isCutoutBrief(projectContentNode)) {
                return "CUTOUT";
            } else if (BriefUtils.isLifestyleBrief(projectContentNode)) {
                return "LIFESTYLE";
            } else if (BriefUtils.isRetouchBrief(projectContentNode)) {
                return "RETOUCH";
            }
        } catch (Exception e) {
            log.error("Can not obtain brief details.", e);
            
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return "N/A";
    }

    private Map<String, String> processCutoutItem(final Node productNode) {
        Map<String, String> row = new HashMap<>();
        row.put("Stock Number", getPropertySafely(productNode, "stockNumber"));
        row.put("Product Name", getPropertySafely(productNode, "productName"));
        row.put("Product Description", getPropertySafely(productNode, "description"));
        row.put("Sample Stock", getSampleStock(productNode));
        row.put("Product Type", getPropertySafely(productNode, "productType"));
        row.put("Category", getPropertySafely(productNode, "category"));
        row.put("Client Style Notes", getPropertySafely(productNode, "clientStylingNotes"));
        row.put("Image Required", getPropertySafely(productNode, "imageRequirements"));
        row.put("Web Sku", getPropertySafely(productNode, "webSku"));
        row.put("Ean Code", getPropertySafely(productNode, "ean"));
        row.put("Brand", getPropertySafely(productNode, "brand"));
        row.put("Number of Alts", String.join(" ", listAlts(productNode)));
        row.put("Buying Office", getPropertySafely(productNode, "buyingOffice"));
        return row;
    }

    private String getSampleStock(final Node productNode) {
        String stockLevel = "0";
        try {
            String productPath = getPropertySafely(productNode, "productPath");
            if (!StringUtils.isBlank(productPath)) {
                Node rawProductNode = productNode.getSession().getNode(productPath);
                if (rawProductNode.hasProperty("quantityReceived")) {
                    stockLevel = rawProductNode.getProperty("quantityReceived").getString();
                }
            } else {
                if (productNode.hasProperty("quantityReceived")) {
                    stockLevel = productNode.getProperty("quantityReceived").getString();
                }
            }
        } catch (Exception e) {
            log.error("Can't access product node", e);
        }
        return stockLevel;
    }
    
    private List<Map<String, String>> processLifestyleItem(final Node lookNode) {
        List<Map<String, String>> items = new ArrayList<Map<String,String>>();
        String lifestyleName = getPropertySafely(lookNode, JLPLifeStyleConstants.NAME);
        String lookName = getPropertySafely(lookNode, JLPLifeStyleConstants.LOOK_SHOOT_NAME);
        //row.put("Naming Convention", getPropertySafely(productNode, JLPLifeStyleConstants.NAMING_CONVENTION));
        String shotDescription = getPropertySafely(lookNode, JLPLifeStyleConstants.SHOOT_DESCRIPTION);
        String buyingOffice =  getPropertySafely(lookNode, JLPLifeStyleConstants.BUYING_OFFICE);
        String clientNotes =  getPropertySafely(lookNode, JLPLifeStyleConstants.CLIENT_STYLING_NOTES);
        String range = getPropertySafely(lookNode, JLPLifeStyleConstants.RANGE);
        String ids = getPropertySafely(lookNode, JLPLifeStyleConstants.IDS);
        List<String> productPaths = getLifestyleProducts(lookNode); 
        String noAlts = "" + listAlts(lookNode).size();
        if (productPaths.size() > 0) {
            for (String productPath : productPaths) {
                try { 
                    Node product = lookNode.getSession().getNode(productPath);
                    if (product != null) {
                        String prodCode = getPropertySafely(product, ProductConstants.PRODUCT_CODE);
                        String prodDesc = getPropertySafely(product, ProductConstants.WSM_TITLE);
                        String wsId = getPropertySafely(product, ProductConstants.WSM_ITEM_ID);
                        String ean = getPropertySafely(product, ProductConstants.TRADED_CODE);
                        String sampleStock = getPropertySafely(product, "quantityReceived");
                        Map<String, String> row = new HashMap<>();
                        if (items.size()==0) {
                            row.put("Lifestyle name", lifestyleName);
                            row.put("Look", lookName);
                            row.put("Shot Description", shotDescription);    
                            row.put("Buying Office", buyingOffice);   
                            row.put("Style Notes", clientNotes); 
                            row.put("Range", range);   
                            row.put("IDS", ids);  
                            row.put("No Alts", noAlts);
                        }
                        row.put("Product Code", prodCode);    
                        row.put("Product Description", prodDesc); 
                        row.put("Quantity Received", sampleStock);    
                        row.put("Web Sku", wsId); 
                        row.put("EAN Code", ean);
                        items.add(row);
                    }
                } catch (Exception e) {
                    log.error("Can't get lifetyle item detail", e);
                }
            }
        } else {
            Map<String, String> row = new HashMap<>();
            row.put("Lifestyle name", lifestyleName);
            row.put("Look", lookName);
            row.put("Shot Description", shotDescription);    
            row.put("Buying Office", buyingOffice);   
            row.put("Style Notes", clientNotes); 
            row.put("Range", range);   
            row.put("IDS", ids);  
            row.put("No Alts", noAlts);
            row.put("Product Code", "N/A");    
            row.put("Product Description", "N/A"); 
            row.put("Quantity Received", "N/A");    
            row.put("Web Sku", "N/A"); 
            row.put("EAN Code", "N/A");
            items.add(row);
        }
        
        return items;
    }

    
    private List<String> getLifestyleProducts(Node productNode) {
        List<String> products = new ArrayList<>();
        try {
            if (productNode.hasProperty(JLPConstants.DAM_JLP_PRODUCTS)) {
                Property property = productNode.getProperty(JLPConstants.DAM_JLP_PRODUCTS);
                if (property.isMultiple()) {
                    Value[] values = property.getValues();
                    for (Value value : values) {
                        String product = value.getString();
                        products.add(product);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can't get lifestyle products", e);
        }
        return products;
    }

    private List<String> listAlts(Node productNode) {
        List<String> list = new ArrayList<>();
        try {
            NodeIterator children = productNode.getNodes();
            while (children.hasNext()) {
                Node child = children.nextNode();
                if (child.getPath().indexOf("/" + JLPConstants.COMPOSITE_FOLDER + "/") < 0 
                        && child.getPath().indexOf("/" + JLPConstants.REFERENCE_FOLDER + "/") < 0
                        && child.getPrimaryNodeType().getName().equals("sling:Folder")) {
                    if (child.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                        BriefAssetStatus status = BriefAssetStatus.valueOf(child.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                        if (status != BriefAssetStatus.ALT_NOT_NEEDED) {
                            list.add(child.getName());
                        }
                    }
                }
            }
        } catch (Exception e){
            log.error("Error counting number of assets for brief details" , e);
        }
        Collections.sort(list);
        return list;
    }

    private String getPropertySafely(Node productNode, String propertyName) {
        String result = "";
        try {
        if (productNode.hasProperty(propertyName)) {
            Property property = productNode.getProperty(propertyName);
            if (property.isMultiple()) {
                Value[] values = property.getValues();
                StringBuilder builder = new StringBuilder();
                for (Value value : values) {
                    builder.append(value.getString());
                    builder.append(" ");
                }
            } else {
                result = property.getString();
            }
        } 
        } catch (Exception e) {
            log.error("Can not retrieve property for brief preview: " + propertyName, e);
        }
        return result;
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
}
