package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.jl.aem.dam.briefs.service.AltTaskCompleterService;
import com.jl.aem.dam.briefs.service.AssetTaskCompleterService;
import com.jl.aem.dam.briefs.service.ProductTaskCompleterService;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/externalRetouch",
    "sling.servlet.methods=POST"
  }
)
public class ExternalRetouchServlet extends SlingAllMethodsServlet {
    
    private static final Logger log = LoggerFactory.getLogger(ExternalRetouchServlet.class);

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private AssetTaskCompleterService assetCompleterService;

    @Reference
    private AltTaskCompleterService altCompleterService;
    
    @Reference
    private ProductTaskCompleterService productCompleterService;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        try {
            
            String retoucher = request.getParameter("retoucher")!=null?request.getParameter("retoucher"):"";
            AutocompleteStatus newStatus = AutocompleteStatus.EXTERNAL_RETOUCH;

            RequestParameter[] resourcePaths = request.getRequestParameters("path");
            

            if (resourcePaths != null) {
                //List<String> assetPaths = getAssets(resourcePaths, request.getResourceResolver());
                ResourceResolver resourceResolver = getResourceResolver();
                List<String> availableAssets = new ArrayList<String>();
                try { 
                    for (RequestParameter resourcePath : resourcePaths) {
                        String path = resourcePath.toString();
                        if (isAsset(path, resourceResolver)) {
                            if (assetCompleterService.canComplete(path, newStatus)) {
                                availableAssets.add(path);
                            }
                        } else if (isAlt(path, resourceResolver)) {
                            if (altCompleterService.canComplete(path, newStatus)) {
                                availableAssets.add(path);
                            }
                        } else if (isProduct(path, resourceResolver) || isLook(path, resourceResolver)) {
                            availableAssets.addAll(productCompleterService.getAssetsToRetouch(path, newStatus));
                            
                        } else if (isProject(path, resourceResolver)) {
                            List<String> productPaths = getProductPaths(path, resourceResolver);
                            for (String productPath : productPaths) {
                                availableAssets.addAll(productCompleterService.getAssetsToRetouch(productPath, newStatus));
                            }
                        }
                    }
                    StringBuilder targetFolder = new StringBuilder();
                    if (StringUtils.isBlank(retoucher)) {
                        targetFolder.append("born");
                    } else {
                        targetFolder.append(retoucher);
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    if (availableAssets.size() > 0) {
                        String briefName = getBriefName(availableAssets.iterator().next(), resourceResolver);
                        String timestamp = sdf.format(new Date());
                        targetFolder.append("/").append(briefName).append("-").append(timestamp);
                        for (String path : availableAssets) {
                            if (isAsset(path, resourceResolver)) {
                                assetCompleterService.complete(path, newStatus, targetFolder.toString());
                            } else if (isAlt(path, resourceResolver)) {
                                altCompleterService.complete(path, newStatus, targetFolder.toString());
                            }
                        }
    
                        // Return to caller
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().write("{\"result\":\"OK\", \"folder\":\"" + JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH + "/" + targetFolder + "\"}");
                        response.setContentType("application/json");
    
                    } else {
                        response.getWriter().write("{\"result\":\"ERROR: Can't complete all tasks, please verify current status and make sure they can be set to the selected one.\"}");
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.setContentType("application/json");
                    }
                } catch (Exception e)  {
                    log.error("Error autocompleting assets " + e.getMessage());
                } finally {
                  if (resourceResolver != null) {
                      resourceResolver.close();
                  }  
                }
            }

        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        try {

            RequestParameter[] resourcePaths = request.getRequestParameters("path");
            AutocompleteStatus newStatus = AutocompleteStatus.EXTERNAL_RETOUCH;


            if (resourcePaths != null) {
                ResourceResolver resourceResolver = getResourceResolver();
                AssetSummary summary = new AssetSummary();
                List<String> availableAssets = new ArrayList<String>();
                if (resourcePaths.length > 0) {
                    collectProjectSummary(getProjectPath(resourcePaths[0].getString(), resourceResolver), resourceResolver, summary);
                }
                try { 
                    for (RequestParameter resourcePath : resourcePaths) {
                        String path = resourcePath.toString();
                        if (isAsset(path, resourceResolver)) {
                            if (assetCompleterService.canComplete(path, newStatus)) {
                                availableAssets.add(path);
                            }
                        } else if (isAlt(path, resourceResolver)) {
                            if (altCompleterService.canComplete(path, newStatus)) {
                                availableAssets.add(path);
                            }
                        } else if (isProduct(path, resourceResolver) || isLook(path, resourceResolver)) {
                            availableAssets.addAll(productCompleterService.getAssetsToRetouch(path, newStatus));
                            
                        } else if (isProject(path, resourceResolver)) {
                            List<String> productPaths = getProductPaths(path, resourceResolver);
                            for (String productPath : productPaths) {
                                availableAssets.addAll(productCompleterService.getAssetsToRetouch(productPath, newStatus));
                            }
                        }
                    }
                    summary.assetsToRetouch  = availableAssets.size();
                    summary.assetsUploaded = summary.assetsUploaded - summary.assetsToRetouch;
                    Gson gson = new Gson();
                    
                    
                    response.getWriter().write(gson.toJson(summary));
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("application/json");

                } catch (Exception e)  {
                    log.error("Error autocompleting assets " + e.getMessage());
                } finally {
                  if (resourceResolver != null) {
                      resourceResolver.close();
                  }  
                }
            }

        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private String getProjectPath(String path, ResourceResolver resourceResolver) throws Exception {
        
        Resource resource = resourceResolver.getResource(path);
        if (isAsset(path, resourceResolver)) {
            return resource.getParent().getParent().getParent().getPath();
        } else if (isAlt(path, resourceResolver)) {
            return resource.getParent().getParent().getPath();
        } else if (isProduct(path, resourceResolver) || isLook(path, resourceResolver)) {
            return resource.getParent().getPath();
        } else if (isProject(path, resourceResolver)) {
            return path;
        }
        return null;
    }

    private void collectProjectSummary(String path, ResourceResolver resourceResolver,
            AssetSummary summary) throws Exception {
        Resource resource = resourceResolver.getResource(path);
        if (resource != null) {
            Node projectNode = resource.adaptTo(Node.class);
            NodeIterator nodeIt = projectNode.getNodes();
            while (nodeIt.hasNext()) {
                Node productNode =nodeIt.nextNode();
                if (isProduct(productNode.getPath(), resourceResolver) || isLook(productNode.getPath(), resourceResolver) ) {
                    collectProductSummary(productNode.getPath(), resourceResolver, summary);
                }
            }
        }
    }

    private void collectProductSummary(String path, ResourceResolver resourceResolver,
            AssetSummary summary) throws Exception {
        Resource resource = resourceResolver.getResource(path);
        if (resource != null) {
            Node productNode = resource.adaptTo(Node.class);
            NodeIterator nodeIt = productNode.getNodes();
            while (nodeIt.hasNext()) {
                Node altNode =nodeIt.nextNode();
                if (isAlt(altNode.getPath(), resourceResolver)) {
                    collectAltSummary(altNode.getPath(), resourceResolver, summary);
                }
            }
        }
    }

    private void collectAltSummary(String path, ResourceResolver resourceResolver,
            AssetSummary summary) throws Exception {
        Resource resource = resourceResolver.getResource(path);
        if (resource != null) {
            Node altNode = resource.adaptTo(Node.class);
            if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                String statusStr = altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString();
                BriefAssetStatus status = BriefAssetStatus.valueOf(statusStr);
                if (status == BriefAssetStatus.ASSET_DELIVERED) {
                    summary.assetsDelivered++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.ASSET_REQUESTED) {
                    summary.assetsRequested++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.ASSET_RETOUCHED) {
                    summary.assetsRetouched++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.ASSET_UPLOADED) {
                    if (!altNode.hasProperty("dam:JLPRetouchMode")) {
                        summary.assetsUploaded++;
                    } else {
                        String retouchMode = altNode.getProperty("dam:JLPRetouchMode").getString();
                        if ("thirdparty".equals(retouchMode)) {
                            summary.assetsExternalRetouch++;
                        } else {
                            summary.assetsInternalRetouch++;
                        }
                    }
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.MARK_UP_NEEDED) {
                    summary.markUpNeeded++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.RE_SHOOT_NEEDED) {
                    summary.reShootNeeded++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.READY_TO_RETOUCH) {
                    summary.readyToRetouch++;
                    summary.totalAssets++;
                } else if (status == BriefAssetStatus.PENDING_COLOUR_MATCH) {
                    summary.pendingColourMatch++;
                    summary.totalAssets++;
                }
            }
        }
    }

    private void collectAssetSummary(String path, ResourceResolver resourceResolver, AssetSummary summary) throws Exception {
       Resource resource = resourceResolver.getResource(path);
       if (resource != null) {
           Node assetNode = resource.adaptTo(Node.class);
           Node assetMetadata = assetNode.getNode("jcr:content/metadata");
           if (assetMetadata.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
               String statusStr = assetMetadata.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString();
               BriefAssetStatus status = BriefAssetStatus.valueOf(statusStr);
               if (status == BriefAssetStatus.ASSET_DELIVERED) {
                   summary.assetsDelivered++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.ASSET_REQUESTED) {
                   summary.assetsRequested++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.ASSET_RETOUCHED) {
                   summary.assetsRetouched++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.ASSET_UPLOADED) {
                   if (!assetMetadata.hasProperty("dam:JLPRetouchMode")) {
                       summary.assetsUploaded++;
                   } else {
                       String retouchMode = assetMetadata.getProperty("dam:JLPRetouchMode").getString();
                       if ("thirdparty".equals(retouchMode)) {
                           summary.assetsExternalRetouch++;
                       } else {
                           summary.assetsInternalRetouch++;
                       }
                   }
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.MARK_UP_NEEDED) {
                   summary.markUpNeeded++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.RE_SHOOT_NEEDED) {
                   summary.reShootNeeded++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.READY_TO_RETOUCH) {
                   summary.readyToRetouch++;
                   summary.totalAssets++;
               } else if (status == BriefAssetStatus.PENDING_COLOUR_MATCH) {
                   summary.pendingColourMatch++;
                   summary.totalAssets++;
               }
           }
       }
    }

    private String getBriefName(String path, ResourceResolver resourceResolver) throws RepositoryException {
        if (isAlt(path, resourceResolver)) {
            Resource resource = resourceResolver.getResource(path);
            return resource.getParent().getParent().getName();
        } else {
            Resource resource = resourceResolver.getResource(path);
            return resource.getParent().getParent().getParent().getName();
        }
    }

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
    
    private boolean isProject(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty("projectPath")) {
                    return true;
                }
            }
            
        }
        return false;
    }

    private boolean isProduct(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty("stockNumber")) {
                    return true;
                }
            }
            
        }
        return false;
    }
    
    private boolean isLook(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME)) {
                    return true;
                }
            }
            
        }
        return false;
    }

    private boolean isAlt(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.getName().toLowerCase().startsWith("alt")
                        || resourceNode.getName().toLowerCase().equals("main")) {
                    return true;
                }
            }
            
        }
        return false;
    }
    
    private boolean isAsset(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            return resourceNode.getPrimaryNodeType().getName().equals("dam:Asset");
        }
        return false;
    }
    
    private List<String> getProductPaths(String projectPath, ResourceResolver resourceResolver) throws RepositoryException {
        List<String> result = new ArrayList<>();
        Node projectDamFolderNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
        NodeIterator nodeIt = projectDamFolderNode.getNodes();
        while (nodeIt.hasNext()) {
            Node prodNode = nodeIt.nextNode();
            if (isFolder(prodNode) 
                    && prodNode.getName().toLowerCase().indexOf("composites")<0 
                    && prodNode.getName().toLowerCase().indexOf("reference")<0 ) {
                result.add(prodNode.getPath());
            }
        }
        return result;
    }

    private boolean isFolder(Node resourceNode) throws RepositoryException {
        return resourceNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0;
    }

    public static class AssetSummary {
        public int pendingColourMatch = 0;
        public int assetsToRetouch = 0;
        public int assetsDelivered = 0;
        public int assetsRequested = 0;
        public int assetsRetouched = 0;
        public int assetsUploaded = 0;
        public int assetsInternalRetouch = 0;
        public int assetsExternalRetouch = 0;
        public int markUpNeeded = 0;
        public int reShootNeeded = 0;
        public int readyToRetouch = 0;
        public int totalAssets = 0;
    }
}