package com.jl.aem.dam.pim.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.jl.aem.dam.pim.CompletedImage;
import com.jl.aem.dam.pim.ProductService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/updated_images",
    "sling.servlet.methods=GET"
  }
)
public class CompletedImagesServlet extends SlingAllMethodsServlet{

    private static final String PFORMAT_FROM_DATE = "yyyyMMdd_HHmmss";

    private static final String PNAME_FROM_DATE = "fromDate";

    /**
     * 
     */
    private static final long serialVersionUID = 1946503157051545195L;

    @Reference
    private ProductService productService;
    
    private static Gson gson = new Gson();
    
    private static final Logger logger = LoggerFactory.getLogger(CompletedImagesServlet.class);
    
    private static SimpleDateFormat sdf = new SimpleDateFormat(PFORMAT_FROM_DATE);

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        String dateParameter = request.getParameter(PNAME_FROM_DATE);
        sdf.setLenient(true);
        Date date = null;
        if (dateParameter != null) {
            try {
                date = sdf.parse(dateParameter);
            } catch (Exception e) {
                logger.error("Can not parse date", e);
                response.setStatus(200);
                response.setContentType("application/json");
                CompletedImagesResponse resp = new CompletedImagesResponse(400, new ArrayList<>(),
                        "Provided date can not be parsed, please use the following format: "
                                + PFORMAT_FROM_DATE);
                response.getWriter().write(gson.toJson(resp));
                return;
            }
        }
        List<CompletedImage> completedImages = productService.getProductCompletedImages(date);
        CompletedImagesResponse resp = new CompletedImagesResponse(200, completedImages,  "");
        response.setStatus(200);
        response.setContentType("application/json");
        response.getWriter().write(gson.toJson(resp));
    }

    public static class CompletedImagesResponse {
        private int status;
        
        @SerializedName("updated_images")
        private List<CompletedImage> completedImages;
        private String error;
        
        public CompletedImagesResponse(int status, List<CompletedImage> completedImages, String error) {
            this.status = status;
            this.completedImages = completedImages;
            this.error = error;
        }
        public int getStatus() {
            return status;
        }
        public String getError() {
            return error;
        }
        public List<CompletedImage> getCompletedImages() {
            return completedImages;
        }
        
    }
    
}
