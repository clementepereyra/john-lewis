package com.jl.aem.dam.reports.models.results;

public class InflightProjectdetailItem {

    private String projectName;
    private String briefType;
    private String dateCreated;
    private String deliveryDate;
    private String creator;
    private String samplesReceived;
    private String pendingShot;
    private String pendingRetouch;
    private String pendingReShoot;
    private String assetsDelivered;
    private String markupNeeded;
    private String assetsRetouched;
    private String projectPath;
    private String pendingColourMatch;
    
    public String getProjectName() {
        return projectName;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public String getDateCreated() {
        return dateCreated;
    }
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }
    public String getDeliveryDate() {
        return deliveryDate;
    }
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    public String getCreator() {
        return creator;
    }
    public void setCreator(String creator) {
        this.creator = creator;
    }
    public String getSamplesReceived() {
        return samplesReceived;
    }
    public void setSamplesReceived(String samplesReceived) {
        this.samplesReceived = samplesReceived;
    }
    public String getPendingShot() {
        return pendingShot;
    }
    public void setPendingShot(String pendingShot) {
        this.pendingShot = pendingShot;
    }
    public String getPendingRetouch() {
        return pendingRetouch;
    }
    public void setPendingRetouch(String pendingRetouch) {
        this.pendingRetouch = pendingRetouch;
    }
    public String getPendingReShoot() {
        return pendingReShoot;
    }
    public void setPendingReShoot(String pendingReShoot) {
        this.pendingReShoot = pendingReShoot;
    }
    public String getAssetsDelivered() {
        return assetsDelivered;
    }
    public void setAssetsDelivered(String assetsDelivered) {
        this.assetsDelivered = assetsDelivered;
    }
    public String getBriefType() {
        return briefType;
    }
    public void setBriefType(String briefType) {
        this.briefType = briefType;
    }
    public String getMarkupNeeded() {
        return markupNeeded;
    }
    public void setMarkupNeeded(String markupNeeded) {
        this.markupNeeded = markupNeeded;
    }
    public void setAssetsRetouched(String assetsRetouched) {
       this.assetsRetouched = assetsRetouched;
    }
    public String getAssetsRetouched() {
        return assetsRetouched;
    }
    public String getProjectPath() {
        return projectPath;
    }
    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }
    public void setPendingColourMatch(String pendingColourMatch) {
        this.pendingColourMatch = pendingColourMatch;
        
    }
    public String getPendingColourMatch() {
        return pendingColourMatch;
    }
}
