package com.jl.aem.dam.workflow;

public interface ExpiredAssetNotificationService {

    void execute();
}
