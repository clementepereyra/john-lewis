package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Setup Asset Shoots Workflow Process"})
public class SetupAssetShootsWorkflowProcess extends AbstractProjectWorkflowProcess implements WorkflowProcess {

    static final Logger log = LoggerFactory.getLogger(SetupAssetShootsWorkflowProcess.class);
    
    @Reference 
    ResourceResolverFactory resolverFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap arg2)
            throws WorkflowException {
        try {
            Node projectNode = getProjectFromPayload(item, session.getSession(), resolverFactory);
            
            if (isBriefProject(projectNode)) {

                briefsService.setupShootTasks(projectNode.getPath());
                
            } 
        } catch (Exception e) {
            log.error("Error creating shoots", e);
        }
        
    }
    
}
