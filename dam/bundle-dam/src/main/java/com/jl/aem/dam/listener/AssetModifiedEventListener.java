package com.jl.aem.dam.listener;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.DamEvent;
import com.day.cq.dam.api.DamEvent.Type;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import com.day.cq.dam.commons.util.DamUtil;
import com.jl.aem.dam.workflow.AssetStatusChangeProcessor;
import com.day.cq.dam.api.Asset;

@Component(name = "com.jl.dam.listener.AssetModifiedEventListener", service = EventHandler.class, immediate = true, property = {
        "event.topics=" + DamEvent.EVENT_TOPIC })
@Designate(ocd = AssetModifiedEventListernerConfigurator.class)
public class AssetModifiedEventListener implements EventHandler {

    @Reference
    public AssetStatusChangeProcessor assetStatusChangeProcessor;

    private static final Logger LOG = LoggerFactory.getLogger(AssetModifiedEventListener.class);

    private String[] observedPaths;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public void handleEvent(Event event) {

        DamEvent damEvent = DamEvent.fromEvent(event);
        if (damEvent != null) {
            String nodePath = damEvent.getAssetPath();
            Type eventType = damEvent.getType();
            LOG.info("JLP DAM Listener Dam Event -> {} for node {}", eventType, nodePath);

            if (!isCurrentNodeObservable(nodePath)) {
                LOG.debug("JLP DAM Listener Current node is not observable for this service. {}", nodePath);
                return;
            }

            if (eventType == DamEvent.Type.METADATA_UPDATED && assetStatusChangeProcessor.shouldProcess(damEvent)) {
                String assetPath = getEventAsset(damEvent.getAssetPath());

                assetStatusChangeProcessor.process(damEvent, assetPath);

            }
        }
    }

    @Activate
    protected void activate(final AssetModifiedEventListernerConfigurator config) {
        resetService(config);
    }

    @Modified
    protected void modified(final AssetModifiedEventListernerConfigurator config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final AssetModifiedEventListernerConfigurator config) {
    }

    private synchronized void resetService(final AssetModifiedEventListernerConfigurator config) {
        LOG.info("Resetting default Cloud Manager Listener service using configuration: " + config);

        String[] observedPaths = config.getObservedPaths();
        for (String path : observedPaths) {
            LOG.info("Obseerving path--> " + path);
        }
        this.observedPaths = observedPaths;
    }

    private Boolean isCurrentNodeObservable(String nodePath) {
        if (StringUtils.isBlank(nodePath)) {
            return false;
        }
        for (String path : observedPaths) {
            if (nodePath.startsWith(path)) {
                return true;
            }
        }
        return false;
    }

    private String getEventAsset(String assetPath) {
        Asset asset = null;
        ResourceResolver resourceResolver = null;
        try {
            try {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
                resourceResolver = resolverFactory.getServiceResourceResolver(param);

            } catch (Exception e) {
                LOG.error("Can not login system user and get resource resolver " + e.getMessage());
                return null;
            }
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                if (null != resource) {
                    asset = DamUtil.resolveToAsset(resource);
                }
            }
            return asset.getPath();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
}
