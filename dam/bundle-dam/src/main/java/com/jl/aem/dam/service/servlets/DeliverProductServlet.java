package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.impl.BriefsServiceImpl;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/deliverProduct.json",
        "sling.servlet.methods=POST"})
public class DeliverProductServlet  extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    //private static final String TEMP_PATH = "/content/dam/projects/temp/retouch";
    
    private static final Logger logger = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        try {
            List<String> errors = new ArrayList<String>();
            for(String path : items) {
                Resource resource = resourceResolver.getResource(path);
                if (resource != null) {
                    Node node = resource.adaptTo(Node.class);
                    if(node.getPrimaryNodeType().getName().equals("sling:Folder") && (node.hasProperty("stockNumber") || node.hasProperty("dam:JLPLookName"))) {
                        NodeIterator nodeIterator = node.getNodes();
                        while (nodeIterator.hasNext()) {
                            Node altNode = nodeIterator.nextNode();
                            if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                                BriefAssetStatus status = BriefAssetStatus.valueOf(altNode.getProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS).getString());
                                if (status!= BriefAssetStatus.ALT_NOT_NEEDED && status!=BriefAssetStatus.ASSET_RETOUCHED) {
                                    errors.add(node.getName() + " - " + altNode.getName());
                                }
                            }
                        }
                    } else {
                        errors.add(node.getName());
                    }
                } else {
                    errors.add(path.substring(path.lastIndexOf("/")+1));
                }
            }
            if (errors.size() > 0) {
                for (String error : errors) {
                    response.getWriter().println();
                    response.getWriter().println(error);
                }
            } else {
                response.getWriter().write("OK");
            }
            response.setContentType("text/plain");
        } catch (Exception e) {
            logger.error("Can't validate product delivery", e);
        }
    }
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        for(String path : items) {
            Resource resource = resourceResolver.getResource(path);
            if (resource != null) {
                briefsService.deliverProductToScene7(path);
            }
        }
        response.getWriter().write("OK");
        response.setContentType("text/plain");
    }

    

    
    
}
