package com.jl.aem.dam.workflow;

public enum ProductStatus {

    IMAGE_REQUIRED ("Image Required"),
    IMAGE_REQUESTED ("Image Requested"),
    IMAGE_AVAILABLE ("Image Available"),
    IMAGE_COMPLETED ("Image Completed"),
    MISSING_METADATA ("Mising Metadata");
    
    private final String displayLabel;
    
    private ProductStatus(final String theDisplayLabel) {
        this.displayLabel = theDisplayLabel;
    }
    
    public String getDisplayLabel() {
        return displayLabel;
    }
}
