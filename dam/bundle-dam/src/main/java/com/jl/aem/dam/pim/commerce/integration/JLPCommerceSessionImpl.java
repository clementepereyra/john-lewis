package com.jl.aem.dam.pim.commerce.integration;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.AbstractJcrCommerceSession;

public class JLPCommerceSessionImpl extends AbstractJcrCommerceSession {
    
   protected SlingHttpServletRequest request;
   protected SlingHttpServletResponse response;
   protected Resource resource;
   protected ResourceResolver resolver;
   protected AbstractJcrCommerceService commerceService;
    
   protected List<CartEntry> cart = new ArrayList<CartEntry>();
   protected List<String> activePromotions = new ArrayList<String>();

   public JLPCommerceSessionImpl(
           AbstractJcrCommerceService commerceService,
           SlingHttpServletRequest request, SlingHttpServletResponse response,
           Resource resource) throws CommerceException {
       super(commerceService, request, response, resource);
    
        this.request = request;
        this.response = response;
        this.resource = resource;
        this.resolver = resource.getResourceResolver();
        this.commerceService = commerceService;
        
   }
}
