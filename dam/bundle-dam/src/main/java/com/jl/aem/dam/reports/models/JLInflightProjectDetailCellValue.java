package com.jl.aem.dam.reports.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.jl.aem.dam.reports.models.results.InflightProjectdetailItem;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class JLInflightProjectDetailCellValue {
    @ValueMapValue
    private String property;

    @RequestAttribute
    private InflightProjectdetailItem result;

    private String value;
    
    public JLInflightProjectDetailCellValue() {
    }
    
    public JLInflightProjectDetailCellValue(InflightProjectdetailItem resource, String property) {
       this.result = resource;
       this.property= property;
       init();
    }

    public String getValue() {
        return value;
    }
    
    @PostConstruct
    private void init() {
        if (result != null && property != null) {
           if ("projectPath".equals(property)) {
               value = result.getProjectPath();
           } else if ("projectName".equals(property)) {
               value = result.getProjectName();
           } else if ("deliveryDate".equals(property)) {
               value = result.getDeliveryDate();
           } else if ("assetsDelivered".equals(property)) {
               value = result.getAssetsDelivered() ;
           } else if ("assetsRetouched".equals(property)) {
               value = result.getAssetsRetouched() ;
           } else if ("briefType".equals(property)) {
               value = result.getBriefType() ;
           } else if ("creator".equals(property)) {
               value = result.getCreator() ;
           } else if ("dateCreated".equals(property)) {
               value = result.getDateCreated();
           } else if ("markupNeeded".equals(property)) {
               value = result.getMarkupNeeded() ;
           } else if ("pendingReshoot".equals(property)) {
               value = result.getPendingReShoot() ;
           } else if ("pendingRetouch".equals(property)) {
               value = result.getPendingRetouch() ;
           } else if ("pendingShot".equals(property)) {
               value = result.getPendingShot() ;
           } else if ("samplesReceived".equals(property)) {
               value = result.getSamplesReceived() ;
           } else if ("pendingColourMatch".equals(property)) {
               value = result.getPendingColourMatch() ;
           } 
        }
    }
}
