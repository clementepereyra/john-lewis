package com.jl.aem.dam.workflow;

public interface AssetStatusCommandFactory {

    public AssetStatusCommand getAssetStatusCommand(final AssetStatus status);
}
