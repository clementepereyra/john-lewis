package com.jl.aem.dam.reports.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.jl.aem.dam.reports.models.results.OverDueProjectDetailsItem;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class  JLOverDueProjectDetails{
    @ValueMapValue
    private String property;

    @RequestAttribute
    private OverDueProjectDetailsItem result;

    private String value;
    
    public JLOverDueProjectDetails() {
    }
    
    public JLOverDueProjectDetails(OverDueProjectDetailsItem resource, String property) {
       this.result = resource;
       this.property= property;
       init();
    }

    public String getValue() {
        return value;
    }
    
    @PostConstruct
    private void init() {
        if (result != null && property != null) {
           if ("actualDueDate".equals(property)) {
               value = result.getActualDueDate();
           } else if ("projectName".equals(property)) {
               value = result.getProjectName();
           } else if ("briefType".equals(property)) {
               value = result.getBriefType() ;
           } else if ("startShootDate".equals(property)) {
               value = result.getStartShootDate() ;
           } else if ("endShootDate".equals(property)) {
               value = result.getEndShootDate() ;
           } else if ("countAssets".equals(property)) {
               value = result.getCountAssets() ;
           }
        }
    }
}
