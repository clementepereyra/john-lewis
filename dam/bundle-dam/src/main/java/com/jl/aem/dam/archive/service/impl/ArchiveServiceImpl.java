package com.jl.aem.dam.archive.service.impl;

import java.io.InputStream;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.jl.aem.dam.archive.service.ArchiveService;
import com.jl.aem.dam.archive.service.JLPArchivingSettings;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=ArchiveService.class)
public class ArchiveServiceImpl implements ArchiveService{

    private static final Logger log = LoggerFactory.getLogger(ArchiveServiceImpl.class);

    private static final String DAM_FLATTEN_STATUS = "dam:flattenStatus";

    private static final String DAM_FLATTEN_DATE = "dam:flattenDate";
    
    private static final String DAM_ARCHIVE_STATUS = "dam:archiveStatus";

    private static final String DAM_ARCHIVE_DATE = "dam:archiveDate";

    private static final String DAM_ARCHIVE_BLOB_NAME = "dam:archiveBlobName";

    private static final String DAM_ARCHIVE_BUCKET_NAME = "dam:archiveBucketName";

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private JLPArchivingSettings archivingSettings;
    
    @Override
    public String archiveAsset(final String assetPath) {
        ResourceResolver resourceResolver = getResourceResolver();
        Storage storage = getStorageService(resourceResolver);

        try {
            if (storage != null) {
                Resource assetResource = resourceResolver.getResource(assetPath);
                if (assetResource != null) {
                    Asset targetAsset = assetResource.adaptTo(Asset.class);
                    Rendition layered = targetAsset.getRendition(JLPConstants.RENDITION_LAYERED);
                    if (layered != null) {
                        String bucketName = archivingSettings.getBucketName();
                        String blobName = generateBlobName(targetAsset);
                        String contentType = getContentType(targetAsset);
                        
                        BlobId blobId = BlobId.of(bucketName, blobName);
                        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build();
                        Blob blob = storage.create(blobInfo, IOUtils.toByteArray(layered.getStream()));
                
                        String blobLink = blob.getSelfLink();
                        
                        Node assetNode = assetResource.adaptTo(Node.class);
                        Node metadataNode = assetNode.getNode("jcr:content/metadata");
                        metadataNode.setProperty(DAM_ARCHIVE_STATUS, blobLink);
                        metadataNode.setProperty(DAM_ARCHIVE_DATE, Calendar.getInstance());
                        metadataNode.setProperty(DAM_ARCHIVE_BLOB_NAME, blobName);
                        metadataNode.setProperty(DAM_ARCHIVE_BUCKET_NAME, bucketName);
                        resourceResolver.commit();
                        targetAsset.removeRendition(JLPConstants.RENDITION_LAYERED);
                        resourceResolver.commit();
                    }
                }
            }
        } catch (Exception e) {
            log.error("Can't archive asset", e);
        }
        return null;
    }
    
    @Override
    public void archiveLayeredAssets() {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            QueryResult searchResult = searchLayeredAssetsToArchive(resourceResolver);
            if (searchResult != null) {
                NodeIterator it = searchResult.getNodes();
                while (it.hasNext()) {
                    Node hit = it.nextNode();
                    archiveAsset(hit.getPath());
                }
            }
        } catch (Exception e) {
            log.error("Error archiving assets", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public void removeFlattenAssets() {
        List<String> assetsExpired = findAssetsExpired();
        doRemoveFlattenAssets(assetsExpired);
    }
    
    private void doRemoveFlattenAssets(List<String> assetsExpired) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            for (String assetPath : assetsExpired) {
                Resource assetResource = resourceResolver.getResource(assetPath);
                if (assetResource != null) {
                    if (archivingSettings.getKeepAssetMetadataAndThumbnails()) {
                        Asset targetAsset = assetResource.adaptTo(Asset.class);
                        Rendition flatten = targetAsset.getRendition(JLPConstants.RENDITION_FLATTEN);
                        if (flatten != null) {
                            targetAsset.removeRendition(JLPConstants.RENDITION_FLATTEN);
                            resourceResolver.commit();
                        }
                        Rendition original = targetAsset.getRendition(JLPConstants.RENDITION_ORIGINAL);
                        if (original != null) {
                            targetAsset.removeRendition(JLPConstants.RENDITION_ORIGINAL);
                            resourceResolver.commit();
                        }
                        Node assetNode = assetResource.adaptTo(Node.class);
                        Node metadataNode = assetNode.getNode("jcr:content/metadata");
                        metadataNode.setProperty(DAM_FLATTEN_STATUS, "REMOVED");
                        metadataNode.setProperty(DAM_FLATTEN_DATE, Calendar.getInstance());
                        resourceResolver.commit();
                    } else {
                        Node assetNode = assetResource.adaptTo(Node.class);
                        assetNode.remove();
                        resourceResolver.commit();
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error removing flatten assets", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        
    }

    private List<String> findAssetsExpired() {
        long expirationTime = archivingSettings.getFlattenTiffLivingTimeInDays() * 24 * 60 * 60 * 1000;
        long referenceDate = (System.currentTimeMillis() - expirationTime);
        ResourceResolver resourceResolver = getResourceResolver();
        List<String> retVal = new ArrayList<>();
        try {
            QueryManager queryManager =
                    resourceResolver.adaptTo(Session.class).getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery("SELECT asset.* "
                    + "FROM [dam:Asset] AS asset "
                    + "INNER JOIN [nt:unstructured] AS jcrContent ON ISCHILDNODE(jcrContent,asset) "
                    + "INNER JOIN [nt:unstructured] AS metadata ON ISCHILDNODE(metadata,jcrContent) "
                    + "WHERE ISDESCENDANTNODE(asset, '" + JLPConstants.JOHNLEWIS_SCENE7_FOLDER
                    + "') " + "AND jcrContent.[jcr:lastModified] <= " + referenceDate + " "
                    + "AND metadata.[dam:MIMEtype] = 'image/tiff' " + "AND metadata.["
                    + DAM_FLATTEN_STATUS + "] IS NULL", javax.jcr.query.Query.JCR_SQL2);
            QueryResult searchResult = query.execute();
            if (searchResult != null) {
                NodeIterator it = searchResult.getNodes();
                while (it.hasNext()) {
                    Node hit = it.nextNode();
                    retVal.add(hit.getPath());                    
                }
            }
            return retVal;
        } catch (Exception e) {
            log.error("Can't retrieve assets for archiving", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return new ArrayList<>();
    }


    private String getContentType(Asset targetAsset) {
        return targetAsset.getMimeType();
    }

    private String generateBlobName(Asset targetAsset) {
        return targetAsset.getPath();
    }

    private Storage getStorageService(final ResourceResolver resourceResolver) {
        try {
            Resource resource = resourceResolver.getResource(archivingSettings.getPathToJsonKey());
            InputStream is = resource.adaptTo(InputStream.class);
            StorageOptions options = StorageOptions.newBuilder()
                    .setProjectId(archivingSettings.getProjectID())
                    .setCredentials(GoogleCredentials.fromStream(is))
                    .build();
            Storage storage = options.getService();
            return storage;
        } catch (Exception e) {
            log.error("Can't access Google Cloud to obtain storage object.", e);
            return null;
        }
    }
    
    
    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
    
    private QueryResult searchLayeredAssetsToArchive(final ResourceResolver resourceResolver) {

        long expirationTime = archivingSettings.getLayeredTiffLivingTimeInDays() * 24 * 60 * 60 * 1000;
        long referenceDate = (System.currentTimeMillis() - expirationTime);

        try {
            QueryManager queryManager =
                    resourceResolver.adaptTo(Session.class).getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery("SELECT asset.* "
                    + "FROM [dam:Asset] AS asset "
                    + "INNER JOIN [nt:unstructured] AS jcrContent ON ISCHILDNODE(jcrContent,asset) "
                    + "INNER JOIN [nt:unstructured] AS metadata ON ISCHILDNODE(metadata,jcrContent) "
                    + "WHERE ISDESCENDANTNODE(asset, '" + JLPConstants.JOHNLEWIS_SCENE7_FOLDER
                    + "') " + "AND jcrContent.[jcr:lastModified] <= " + referenceDate + " "
                    + "AND metadata.[dam:MIMEtype] = 'image/tiff' " + "AND metadata.["
                    + DAM_ARCHIVE_STATUS + "] IS NULL", javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            return result;
        } catch (Exception e) {
            log.error("Can't retrieve assets for archiving", e);
        }

        return null;
    }
    
    @Override
    public String restoreAsset(String path) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Resource resource = resourceResolver.getResource(path);
            if (resource != null) {
                Asset asset = resource.adaptTo(Asset.class);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                if (metadataNode.hasProperty(DAM_ARCHIVE_BUCKET_NAME) && metadataNode.hasProperty(DAM_ARCHIVE_BLOB_NAME)) {
                    String bucketName = metadataNode.getProperty(DAM_ARCHIVE_BUCKET_NAME).getString();
                    String blobName = metadataNode.getProperty(DAM_ARCHIVE_BLOB_NAME).getString();
                    Storage storage = getStorageService(resourceResolver);
                    if (storage != null) {
                        BlobId blobId = BlobId.of(bucketName, blobName);
                        Blob blob = storage.get(blobId);
                        
                        if (blob !=null) {               
                            InputStream input = Channels.newInputStream(blob.reader());
                            asset.addRendition(JLPConstants.RENDITION_LAYERED, input, metadataNode.getProperty("dam:MIMEtype").getString());
                            metadataNode.getParent().setProperty("jcr:lastModified", Calendar.getInstance());
                            if (metadataNode.hasProperty(DAM_ARCHIVE_STATUS)) {
                                metadataNode.getProperty(DAM_ARCHIVE_STATUS).remove();
                            }
                            resourceResolver.commit();
                            return "OK";
                        } else {
                            return "ERROR: Asset can not be found in Google Cloud Storage";
                        }
                    } else {
                        return "ERROR: Can't access Google Cloud Storage: " + path;
                    }
                } else {
                    return "ERROR: Asset is not in google cloud: " + path;
                }
            } else {
                return "ERROR: Asset not found: " + path;
            }
            
        } catch (Exception e) {
            log.error("Error restoring asset: " + path, e);
            return "ERROR: Internal error restoring asset: "+ e.getCause();
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    @Override
    public void cleanThirdPartyAssets() {
        Calendar currentDate = Calendar.getInstance();
        ResourceResolver resourceResolver = getResourceResolver();
        long allowedTime = this.archivingSettings.getThirdPartyAssetLivingTimeInHours() * 60 * 60 * 1000;
        try {
            Resource thirdPartyRootResource = resourceResolver.getResource(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH);
            if (thirdPartyRootResource != null) {
                Node thirdPartyResourceNode = thirdPartyRootResource.adaptTo(Node.class);
                NodeIterator agentNodes = thirdPartyResourceNode.getNodes();
                while (agentNodes.hasNext()) {
                    Node agentNode = agentNodes.nextNode();
                    if (!"jcr:content".equals(agentNode.getName())) {
                        NodeIterator briefNodes = agentNode.getNodes();
                        while (briefNodes.hasNext()) {
                            Node briefNode = briefNodes.nextNode();
                            if (isRetouchedCompleted(briefNode)) {
                                Node retouchedFolderNode = getRetouchedFolderNode(briefNode);
                                if (retouchedFolderNode != null) {
                                    NodeIterator assetNodes = retouchedFolderNode.getNodes();
                                    while (assetNodes.hasNext()) {
                                        Node assetNode = assetNodes.nextNode();
                                        if ("dam:Asset".equals(assetNode.getPrimaryNodeType().getName())) {
                                            Calendar lastModified = assetNode.getNode("jcr:content").getProperty("jcr:lastModified").getDate();
                                            if (currentDate.getTimeInMillis() - lastModified.getTimeInMillis() >= allowedTime) {
                                                log.info("Removing third party asset: " + assetNode.getPath());
                                                assetNode.remove();
                                                resourceResolver.commit();
                                            }
                                        } 
                                    }
                                    if (isRetouchedFolderEmpty(retouchedFolderNode)) {
                                        log.info("Removing third party folder: " + briefNode.getPath());
                                        briefNode.remove();
                                        resourceResolver.commit();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error cleaning third party assets.", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private boolean isRetouchedFolderEmpty(Node retouchedFolderNode) {
        try {
            NodeIterator retouchedNodes = retouchedFolderNode.getNodes();
            while (retouchedNodes.hasNext()) {
                Node assetNode = retouchedNodes.nextNode();
                if ("dam:Asset".equals(assetNode.getPrimaryNodeType().getName())) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.error("Can't check if brief pending retouch is empty.", e);
            return false;
        }
        return true;
    }

    private Node getRetouchedFolderNode(Node briefNode) {
        try {
            NodeIterator nodes = briefNode.getNodes();
            while (nodes.hasNext()) {
                Node childNode = nodes.nextNode();
                if (childNode.getName().indexOf(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER) >= 0) {
                    return childNode;
                } 
            }
        } catch (Exception e) {
            log.error("Can't check if brief pending retouch is empty.", e);
        }
        return null;
    }

    private boolean isRetouchedCompleted(Node briefNode) {
        try {
            NodeIterator nodes = briefNode.getNodes();
            while (nodes.hasNext()) {
                Node childNode = nodes.nextNode();
                if (childNode.getName().indexOf(JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER) >= 0) {
                    NodeIterator pendingRetouchNodes = childNode.getNodes();
                    while (pendingRetouchNodes.hasNext()) {
                        Node assetNode = pendingRetouchNodes.nextNode();
                        if ("dam:Asset".equals(assetNode.getPrimaryNodeType().getName())) {
                            return false;
                        }
                    }
                } 
            }
        } catch (Exception e) {
            log.error("Can't check if brief pending retouch is empty.", e);
        }
        return true;
    }
}
