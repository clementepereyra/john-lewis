package com.jl.aem.dam.workflow.step.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

public class ProductAssociatorHelper {
    private static final Logger log = LoggerFactory.getLogger(ProductAssociatorHelper.class); 

    public static final String IMAGE_CAT_ROOT = "jlp:image-category/";

    private final static Pattern REGEXP_STOCK_CODE = Pattern.compile("\\d+((alt|vid|mnl)\\d+)");
    private final static Pattern REGEXP_MODEL_1 = Pattern.compile(".*(\\s\\(\\d+\\))");
    private final static Pattern REGEXP_MODEL_2 = Pattern.compile(".*(_\\d+)");

    
    public static Map<String, String> getProductFromAsset(Node asNode, ResourceResolver resourceResolver, Session session) {
        try {
            String nodeName = asNode.getName();
            
            //Remove extension
            String productCode = nodeName.substring(0, nodeName.lastIndexOf("."));
            String imageCat = IMAGE_CAT_ROOT + "main";
            
            //Matches stock code
            Matcher matcher = REGEXP_STOCK_CODE.matcher(productCode);
            if (matcher.matches()) {
                if (productCode.contains("alt")) {
                    imageCat = IMAGE_CAT_ROOT + "alt/" + matcher.group(1);
                } else if (productCode.contains("mnl")) {
                    imageCat = IMAGE_CAT_ROOT + "mnl";
                } else if (productCode.contains("vid")) {
                    imageCat = IMAGE_CAT_ROOT + "vid/" + matcher.group(1);
                }
                productCode = productCode.replace(matcher.group(1), "");
            }
            
            // Matches model number v1
            matcher = REGEXP_MODEL_1.matcher(productCode);
            if (matcher.matches()) {
                imageCat = IMAGE_CAT_ROOT + "alt/alt" + matcher.group(1).replace("(", "").replace(")", "").replace(" ","");
                productCode = productCode.replace(matcher.group(1), "");
            }
            
            //Matches model number v2
            matcher = REGEXP_MODEL_2.matcher(productCode);
            if (matcher.matches()) {
                imageCat = IMAGE_CAT_ROOT + "alt/alt" + matcher.group(1).replace("_", "");
                productCode = productCode.replace(matcher.group(1), "");
            }
            
            String productNodePath = searchProductNode(productCode, resourceResolver, session);

            
            Map<String, String> result = new HashMap<>();
            result.put("productCode", productCode);
            result.put("productPath", productNodePath);
            result.put("imageCat", imageCat);
            
            return result;
            
        } catch (Exception e) {
             log.error("Can not get the product node", e);
             return null;
        }
    }
    
    public static String searchProductNode(String productCode, final ResourceResolver resourceResolver, final Session session) {
        try {
            
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery("SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(s,'/etc/commerce/products/onejl') AND (CONTAINS(s.identifier, '"+productCode+"') OR CONTAINS(s.tradedCode, '"+productCode+"') OR CONTAINS(s.modelNo, '"+productCode+"'))", javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            if (result.getNodes().hasNext()) {
                return result.getNodes().nextNode().getPath();
            }
            
        } catch (Exception e) {
          log.error("Error searching product node for asset", e);
        }
        return null;
    }
    
    public static boolean isBrandLogo(String assetPath) {
        return assetPath.substring(0, assetPath.lastIndexOf(".")).endsWith("_brl");
    }
}
