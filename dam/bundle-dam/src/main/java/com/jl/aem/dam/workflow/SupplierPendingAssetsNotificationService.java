package com.jl.aem.dam.workflow;

public interface SupplierPendingAssetsNotificationService {

    void execute();
}