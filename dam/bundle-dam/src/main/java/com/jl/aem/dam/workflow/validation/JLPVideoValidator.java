package com.jl.aem.dam.workflow.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate=true, service = JLPVideoValidator.class)
@Designate(ocd = JLPVideoValidatorConfiguration.class)
public class JLPVideoValidator extends JLPCommonValidator  implements JLPAssetValidator {

    private static final Logger logger = LoggerFactory.getLogger(JLPSquareValidator.class);
    
    private List<String> extensions;
    
    private long maxSize;
    
    private long minSize;
    
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Override
    public boolean canValidate(String assetPath) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                String dcFormat = metadataNode.getProperty("dc:format").getString();
                if (dcFormat.startsWith("video")) {
                    return true;
                }

            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return false;
    }
    
    @Override
    public String validateAsset(String assetPath) {
        logger.info("AssetReviewRejectedCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user", e);
            throw new RuntimeException(e);
        }
        try {
            if (resourceResolver != null) {
                Resource resource = resourceResolver.getResource(assetPath);
                Node assetNode = resource.adaptTo(Node.class);
                Node metadataNode = assetNode.getNode("jcr:content/metadata");
                long videoSize = getVideoSize(metadataNode);
                
                if (videoSize > maxSize) {
                    return "ERROR: Asset is larger than " + maxSize + " mb";
                }
                if (videoSize < minSize) {
                    return "ERROR: Asset is smaller " + minSize + " mb";
                }
                String assetExtension = getAssetExtension(assetNode);
                if (!extensions.contains(assetExtension)) {
                    return "ERROR: Asset extension is not allowed";
                }
                
            }
         } catch (Exception e) {
             logger.error("Can not test validator", e);
         } finally {
             resourceResolver.close();
         }
         return JLPAssetValidator.SUCCESS_RESPONSE;
    }
    

    private long getVideoSize(Node metadataNode) {
        try {
            return metadataNode.getProperty("dam:size").getLong() / 1000000;
        } catch (RepositoryException e) {
            throw new RuntimeException("Can not get height", e);
        }
    }
    
    @Activate
    protected void activate(final JLPVideoValidatorConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPVideoValidatorConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPVideoValidatorConfiguration config) {
    }

    private synchronized void resetService(final JLPVideoValidatorConfiguration config) {
        maxSize = config.getMaxSize();
        minSize = config.getMinSize();
        extensions = Arrays.asList(config.getExtensions());
    }
}
