package com.jl.aem.dam.service.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.archive.service.ArchiveService;
import com.jl.aem.dam.service.integration.QuickViewsService;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/QuickViewsUpdate",
    "sling.servlet.methods=GET"
  }
)
public class QuickViewsUpdaterServlet extends SlingSafeMethodsServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 5584606190073457984L;
    @Reference
    private QuickViewsService quickViewsArchive;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        quickViewsArchive.createOrUpdateQuickViews();
        response.getWriter().write("OK");
        response.setContentType("text/plain");
    }
}
