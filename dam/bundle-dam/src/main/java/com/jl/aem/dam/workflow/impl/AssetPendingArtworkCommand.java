package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.service.integration.ApiConnector;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.WorkflowBusinessSettings;

@Component(immediate=true, service=AssetPendingArtworkCommand.class)
public class AssetPendingArtworkCommand implements AssetStatusCommand {

    private static final Logger logger = LoggerFactory.getLogger(AssetPendingArtworkCommand.class);
    private static final String CUSTOMER_TOKEN = "customer-token";
    private static final String BUSINESS_AREA_ID = "businessareaId";
    private static final String JOB_TYPE_ID = "jobTypeId";
    private static final String JOB_NAME = "jobName";
    private static final String FILE_NAME = "filename";
    private static final String MESSAGE = "messagee";
    private static final String EMAIL_BORN_TEMPLATE_URL = "/etc/notification/email/html/jlp/bornErrorTemplate.txt";
    private static final Long SLEEP_TIME = new Long(900000);

    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private ApiConnector apiConnector;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private JLPWorkflowSettings settings;
    
    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Reference
    private WorkflowBusinessSettings businessSettings;
    
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    
    @Override
    public void execute(String assetPath, final String userID) {
        logger.info("AssetReadyForArtworkCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            updateAssetStatus(assetNode, resourceResolver, "Image Available", userID);
            resourceResolver.commit();
            String targetFolder = JLPConstants.ARTWORKERS_PENDING_RETOUCH;
            if (assetNode.getPath().indexOf(JLPConstants.ARTWORKERS_AVAILABLE) >=0) {
                targetFolder = assetNode.getParent().getPath().replaceAll(JLPConstants.ARTWORKERS_AVAILABLE, JLPConstants.ARTWORKERS_PENDING_RETOUCH);
            } else {
                String productPath = assetNode.getNode("jcr:content/metadata").getProperty("cq:productReference").getString();
                Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
                Product product = JLWorkflowsUtils.populateProductFromNode(productNode);
                if (businessSettings.is34Ratio(product)) {
                    targetFolder += "/3_4";
                } else {
                    targetFolder += "/square";
                }
            }
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder );
            metaData.put("targetFileName", assetNode.getName());
            
            workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
            notifyArtworkers(assetNode);
        } catch (Exception e) {
            logger.error("Can not move asset to Artworkers. Asset: " + assetPath, e);
        }finally{
            if(resourceResolver != null && resourceResolver.isLive()){
                resourceResolver.close();
             }
        }
    }


    private void updateAssetStatus(Node assetNode, ResourceResolver resourceResolver, String status, String userID) {
        try {
            if (assetNode.getNode("jcr:content/metadata").hasProperty("cq:productReference")) {
                String productPath = assetNode.getProperty("jcr:content/metadata/cq:productReference").getString();
                Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
                if(JLWorkflowsUtils.isMainIMage(assetNode)) {
                    productNode.setProperty("statusId", status);
                }
            }
            Node metadata = assetNode.getNode("jcr:content/metadata");
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_TIME, Calendar.getInstance());
            metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_USER, userID);
            if(assetNode.getPath().contains(JLPConstants.IC_REVIEW_PATH)){
                metadata.setProperty(JLPConstants.ASSET_STATUS_CHANGE_IC_APPROVED_TIME, Calendar.getInstance());
            }
        } catch (Exception e) {
            logger.error("Can not update product status",e);
        }
        
    }
    
    private void notifyArtworkers(Node assetNode) {
        String url = settings.getArtworkersBornUrl();
        try {
        Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
        String prod_code = metadataNode.getProperty(JLPConstants.JLP_PRODUCT_CODES).getString();
        String name = assetNode.getName();
        
        Map<String, String> headers = new HashMap<String, String>(); 
        headers.put(CUSTOMER_TOKEN, settings.getArtworkersBornSecureToken());
        
        Map<String, String> fields = new HashMap<String, String>(); 
        fields.put(BUSINESS_AREA_ID, settings.getArtworkersBornBusinessAreaId());
        fields.put(JOB_TYPE_ID, settings.getArtworkersBornJobTypeId());
        fields.put(JOB_NAME, prod_code);
        fields.put(FILE_NAME, name);
        
        
        if(apiConnector.postURLEncodedForm(url, headers, fields)){
            logger.info("Artworker notification successfully");
        } else {
            callRetryAsynchronously(url, headers, fields,prod_code, name);
        }
            
        } catch (Exception e) {
            logger.error("Artworkers can not be nofied", e);
        }
    }


    private void callRetryAsynchronously(String url, Map<String, String> headers, Map<String, String> fields, String prod_code, String name) {
        new Thread(new Runnable() {
            public void run() {
                int count = 1;
                while(count < 3) {
                    if(apiConnector.postURLEncodedForm(url, headers, fields)) {
                        count = 3;
                    }
                    else {
                        count ++;
                        if(count == 3) {
                            sentErrorEmailToBorn(url, headers, fields,prod_code,name);
                        } else {
                            try {
                                Thread.sleep(SLEEP_TIME);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                }
            }
        }).start();
    }


    private void sentErrorEmailToBorn(String url, Map<String, String> headers, Map<String, String> fields, String prod_code, String name) {
        int status = apiConnector.postURLEncodedFormAndGetStatus(url, headers, fields);
        
        if (!(status >= 400 || status < 0)) {
            ResourceResolver resourceResolver = null;
            Session session = null;
            try {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
                resourceResolver = resolverFactory.getServiceResourceResolver(param);
                session = resourceResolver.adaptTo(Session.class);
            } catch (Exception e) {
                logger.error("Can not login system user");
                throw new RuntimeException(e);
            }
            String message = "";
            if(status == -1){
                message = "Can not crete post call to url: " + url; 
            } else if(status > 400){
                message = "Post call to url: '" + url + "' fails. Status code: " + status;
            }
            
            fields.put(BUSINESS_AREA_ID, settings.getArtworkersBornBusinessAreaId());
            fields.put(JOB_TYPE_ID, settings.getArtworkersBornJobTypeId());
            fields.put(JOB_NAME, prod_code);
            fields.put(FILE_NAME, name);
            
            try {
                Resource templateRsrc = resourceResolver.getResource(EMAIL_BORN_TEMPLATE_URL);
                
                MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
                MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                
                // Creating the Email.
                HtmlEmail email = new HtmlEmail();
                Map<String, String> emailProperties = new HashMap<String,String>();
                
                emailProperties.put(BUSINESS_AREA_ID, settings.getArtworkersBornBusinessAreaId());
                emailProperties.put(JOB_TYPE_ID, settings.getArtworkersBornJobTypeId());
                emailProperties.put(JOB_NAME, prod_code);
                emailProperties.put(FILE_NAME, name);
                emailProperties.put(MESSAGE, message);
                emailProperties.put("logoUrl", settings.getEmailLogoUrl());
                emailProperties.put("logoAlt", settings.getEmailLogoAlt());
                emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
                emailProperties.put("signature", settings.getEmailSignature());
                
                email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                
                ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
                for (String recipient : settings.getInvalidProductRecipientList()) {
                    try {
                        emailRecipients.add(new InternetAddress(recipient));
                    } catch (AddressException e) {
                        logger.error(e.getMessage(), e);
                    }
                }
                email.setTo(emailRecipients);
                
                messageGateway.send(email);
            } catch (Exception e) {
                logger.error("Fatal error while sending email: ", e);
                logger.error("It was not possible to sent the email to born");
            }finally{
                if(resourceResolver != null && resourceResolver.isLive()){
                    resourceResolver.close();
                 }
            }
            
        }
    }

}
