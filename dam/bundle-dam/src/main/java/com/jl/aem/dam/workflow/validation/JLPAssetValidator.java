package com.jl.aem.dam.workflow.validation;

public interface JLPAssetValidator {
    
    public final static String SUCCESS_RESPONSE = "SUCCESS";

    public String validateAsset(final String assetPath);
    
    public boolean canValidate(final String assetPath);
}
