package com.jl.aem.dam.workflow.validation;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class JLPCommonValidator {

    public JLPCommonValidator() {
        super();
    }

    protected String getAssetExtension(Node assetNode) {
        try {
            return assetNode.getName().substring(assetNode.getName().lastIndexOf(".") + 1).toLowerCase();
        } catch (RepositoryException e) {
            throw  new RuntimeException("Can not get extension", e);
        }
    }

    protected long getAssetHeight(Node metadataNode) {
        try {
            return metadataNode.getProperty("tiff:ImageLength").getLong();
        } catch (RepositoryException e) {
            throw new RuntimeException("Can not get height", e);
        }
    }

    protected long getAssetWidth(Node metadataNode) {
        try {
            return metadataNode.getProperty("tiff:ImageWidth").getLong();
        } catch (RepositoryException e) {
            throw new RuntimeException("Can not get width", e);
        }
    }

}
