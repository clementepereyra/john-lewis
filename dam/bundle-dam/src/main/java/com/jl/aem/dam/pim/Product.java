package com.jl.aem.dam.pim;

import java.util.Calendar;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("prod_code")
    private String prodCode;
    @SerializedName("image_source")
    private String imageSource;
    @SerializedName("supp_contact_detl")
    private String supplierContactDetail;
    @SerializedName("online_status")
    private String onlineStatus;
    @SerializedName("wsm_item_id")
    private String wsmItemId;
    @SerializedName("pos_desc")
    private String posDesc;
    @SerializedName("brand_name")
    private String brandName;
    @SerializedName("traded_code")
    private String tradedCode;
    @SerializedName("colour_group")
    private String colourGroup;
    @SerializedName("colour")
    private String colour;
    @SerializedName("colour_code")
    private String colourCode;
    @SerializedName("prod_type_name")
    private String prodTypeName;
    @SerializedName("model_no")
    private String modelNo;
    @SerializedName("option_desc")
    private String optionDesc;
    @SerializedName("sample_source")
    private String sampleSource;
    @SerializedName("publication_status")
    private String publicationStatus;
    @SerializedName("research_done")
    private String researchDone;
    @SerializedName("sel_desc")
    private String selDesc;
    @SerializedName("wsm_title")
    private String wsmTitle;
    @SerializedName("confirmed_date")
    private String confirmedDate;
    @SerializedName("user_id_upd")
    private String userIdUpd;

    private String statusId;
    @SerializedName("deleted")
    private String deleted;

    @SerializedName("directorate")
    private String directorate;
    @SerializedName("season_code")
    private String seasonCode;
    @SerializedName("web_stk_qty")
    private String stockLevel;
    @SerializedName("Year")
    private String year;
    @SerializedName("Buying_Office")
    private String buyingOffice;
    @SerializedName("Launch_Date")
    private String launchDate;
    @SerializedName("image_requested_date")
    private Calendar imageRequestedDate;
    private String productPath;
    
    private List<String> briefs;
    
    public String getTradedCode() {
        return sanitize(tradedCode);
    }
    public void setTradedCode(String tradedCode) {
        this.tradedCode = tradedCode;
    }
    public String getProdCode() {
        return sanitize(prodCode);
    }
    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }
    public String getImageSource() {
        return sanitize(imageSource);
    }
    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }
    public String getSupplierContactDetail() {
        return sanitize(supplierContactDetail);
    }
    public void setSupplierContactDetail(String supplierContactDetail) {
        this.supplierContactDetail = supplierContactDetail;
    }
    public String getOnlineStatus() {
        return sanitize(onlineStatus);
    }
    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }
    public String getWsmItemId() {
        return sanitize(wsmItemId);
    }
    public void setWsmItemId(String wsmItemId) {
        this.wsmItemId = wsmItemId;
    }
    public String getPosDesc() {
        return sanitize(posDesc);
    }
    public void setPosDesc(String posDesc) {
        this.posDesc = posDesc;
    }
    public String getBrandName() {
        return sanitize(brandName);
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public String getColourGroup() {
        return sanitize(colourGroup);
    }
    public void setColourGroup(String colourGroup) {
        this.colourGroup = colourGroup;
    }
    public String getColour() {
        return sanitize(colour);
    }
    public void setColour(String colour) {
        this.colour = colour;
    }
    public String getColourCode() {
        return sanitize(colourCode);
    }
    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }
    public String getProdTypeName() {
        return sanitize(prodTypeName);
    }
    public void setProdTypeName(String prodTypeName) {
        this.prodTypeName = prodTypeName;
    }
    public String getModelNo() {
        return sanitize(modelNo);
    }
    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }
    public String getOptionDesc() {
        return sanitize(optionDesc);
    }
    public void setOptionDesc(String optionDesc) {
        this.optionDesc = optionDesc;
    }
    public String getSampleSource() {
        return sanitize(sampleSource);
    }
    public void setSampleSource(String sampleSource) {
        this.sampleSource = sampleSource;
    }
    public String getPublicationStatus() {
        return sanitize(publicationStatus);
    }
    public void setPublicationStatus(String publicationStatus) {
        this.publicationStatus = publicationStatus;
    }
    public String getResearchDone() {
        return sanitize(researchDone);
    }
    public void setResearchDone(String researchDone) {
        this.researchDone = researchDone;
    }
    public String getSelDesc() {
        return sanitize(selDesc);
    }
    public void setSelDesc(String selDesc) {
        this.selDesc = selDesc;
    }
    public String getWsmTitle() {
        return sanitize(wsmTitle);
    }
    public void setWsmTitle(String wsmTitle) {
        this.wsmTitle = wsmTitle;
    }
    public String getConfirmedDate() {
        return sanitize(confirmedDate);
    }
    public void setConfirmedDate(String confirmedDate) {
        this.confirmedDate = confirmedDate;
    }
    public String getUserIdUpd() {
        return sanitize(userIdUpd);
    }
    public void setUserIdUpd(String userIdUpd) {
        this.userIdUpd = userIdUpd;
    }
    public String getStatusId() {
        return statusId;
    }
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
    public String getDirectorate() {
        return sanitize(directorate);
    }
    public void setDirectorate(String directorate) {
        this.directorate = directorate;
    }
    public String getSeasonCode() {
        return sanitize(seasonCode);
    }
    public void setSeasonCode(String seasonCode) {
        this.seasonCode = seasonCode;
    }
    public String getYear() {
        return sanitize(year);
    }
    public void setYear(String year) {
        this.year = year;
    }
    public String getBuyingOffice() {
        return sanitize(buyingOffice);
    }
    public void setBuyingOffice(String buyingOffice) {
        this.buyingOffice = buyingOffice;
    }
    public String getLaunchDate() {
        return sanitize(launchDate);
    }
    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }
    public String getStockLevel() {
        return sanitize(stockLevel);
    }
    public void setStockLevel(String stockLevel) {
        this.stockLevel = stockLevel;
    }
    public String getDeleted() {
        return sanitize(deleted);
    }
    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }
    
    public Calendar getImageRequestedDate() {
        return imageRequestedDate;
    }
    
    public void setImageRequestedDate(Calendar imReqDate) {
        this.imageRequestedDate = imReqDate;
    }
    
    public String getProductPath() {
        return productPath;
    }
    
    public void setProductPath(String productPath) {
        this.productPath = productPath;
    }
    
    public void setBriefs(List<String> briefs) {
        this.briefs = briefs;
    }
    public List<String> getBriefs() {
        return briefs;
    }
    
    private String sanitize(String field) {
        if (field !=null) {
            return field.trim();
        }
        return "";
    }
}
