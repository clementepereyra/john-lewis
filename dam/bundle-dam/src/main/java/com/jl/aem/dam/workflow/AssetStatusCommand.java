package com.jl.aem.dam.workflow;

public interface AssetStatusCommand {

    public void execute(final String assetPath, final String userID);
}
