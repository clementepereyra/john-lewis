package com.jl.aem.dam.workflow.validation;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP 3:4 Asset Validator Configuration", description = "Service Configuration")
public @interface JLP34ValidatorConfiguration {

    @AttributeDefinition(name ="Max Size", description="Max valid dimension size allowed in its larger dimension", defaultValue="3000")
    public int getMaxSize() default 3000;
    @AttributeDefinition(name ="Min Size", description="Minimum valid dimension size allowed in its larger dimension", defaultValue="800")
    public int getMinSize() default 800;
    @AttributeDefinition(name ="Min Bits Per Pixel", description="Minimum bits per pixel allowed", defaultValue="8")
    public int getMinBitsPerPixel() default 8;
    @AttributeDefinition(name ="Extensions allowed", description="List of extensions allowed (lower case, no dot)", defaultValue={"tif", "tiff"})
    public String[] getExtensions() default {"tif", "tiff"};
    @AttributeDefinition(name ="Product categories", description="List of product categories accepted by this validator", defaultValue={"fashion"})
    public String[] getProductCategories() default {"fashion"};
    @AttributeDefinition(name ="Field Name for Product Category", description="Field Name in Product Metadata containing the category value", defaultValue="directorate")
    public String getProductField() default "directorate";
}
