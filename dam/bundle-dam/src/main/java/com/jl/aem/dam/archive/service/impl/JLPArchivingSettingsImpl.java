package com.jl.aem.dam.archive.service.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

import com.jl.aem.dam.archive.service.JLPArchivingSettings;

@Component(immediate=true, name="JLP Archiving Configuration", service=JLPArchivingSettings.class)
@Designate(ocd = JLPArchivingConfiguration.class)
public class JLPArchivingSettingsImpl implements JLPArchivingSettings {

    
    private String pathToJsonKey;
    private String projectID;
    private String bucketName;
    private int layeredTiffLivingTimeInDays;
    private int flattenTiffLivingTimeInDays;
    private boolean archiveEnabled;
    private boolean keepAssetMetadataAndThumbnails;
    private int thirdPartyAssetLivingTimeInHours;
    
    
    @Activate
    protected void activate(final JLPArchivingConfiguration config) {
        resetService(config);
    }

    @Modified
    protected void modified(final JLPArchivingConfiguration config) {
        resetService(config);
    }

    @Deactivate
    protected void deactivate(final JLPArchivingConfiguration config) {
    }
    
   
    

    private synchronized void resetService(final JLPArchivingConfiguration config) {
        pathToJsonKey = config.getPathToJsonKey();
        projectID = config.getProjectID();
        bucketName = config.getBucketName();
        layeredTiffLivingTimeInDays = config.getLayeredTiffLivingTimeInDays();
        archiveEnabled = config.getArchiveEnabled();
        flattenTiffLivingTimeInDays = config.getFlattenTiffLivingTimeInDays();
        keepAssetMetadataAndThumbnails = config.getKeepAssetMetadataAndThumbnails();
        thirdPartyAssetLivingTimeInHours =config.getThirdPartyAssetLivingTimeInHours();
    }

    public String getPathToJsonKey() {
        return pathToJsonKey;
    }

    public String getProjectID() {
        return projectID;
    }

    public String getBucketName() {
        return bucketName;
    }

    public int getLayeredTiffLivingTimeInDays() {
        return layeredTiffLivingTimeInDays;
    }
    
    @Override
    public int getFlattenTiffLivingTimeInDays() {
        return flattenTiffLivingTimeInDays;

    }
    @Override
    public boolean getArchiveEnabled() {
        return archiveEnabled;
    }
    @Override
    public boolean getKeepAssetMetadataAndThumbnails() {
        return keepAssetMetadataAndThumbnails;
    }
    
    @Override
    public int getThirdPartyAssetLivingTimeInHours() {
        return thirdPartyAssetLivingTimeInHours;
    }

}
