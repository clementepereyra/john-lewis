package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.ProductTaskCompleterService;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/AutocompleteTaskProject",
    "sling.servlet.methods=POST"
  }
)
public class AutocompleteTaskProjectServlet extends SlingAllMethodsServlet {
    
    private static final Logger log = LoggerFactory.getLogger(AutocompleteTaskProjectServlet.class);

    private static final long serialVersionUID = 2598426539166789515L;
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private ProductTaskCompleterService productCompleterService;
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        try {
            
            String comments = request.getParameter("comment")!=null?request.getParameter("comment"):"";
            String assetStatus = request.getParameter("status");
            AutocompleteStatus newStatus = null;
            if (assetStatus != null) {
                newStatus = AutocompleteStatus.valueOf(assetStatus);
            }

            String projectPath = request.getParameter("project_path");

            

            if (projectPath != null) {
                boolean validation = true;
                //List<String> assetPaths = getAssets(resourcePaths, request.getResourceResolver());
                ResourceResolver resourceResolver = getResourceResolver();
                try {
                    List<String> productPaths = getProductPaths(projectPath, resourceResolver);
                    for (String productPath : productPaths) {
                        if (!productCompleterService.canComplete(productPath, newStatus)) {
                            validation = false;
                            break;
                        }
                    }
                    if (validation) {
                        for (String productPath : productPaths) {
                            productCompleterService.complete(productPath, newStatus, comments);                        
                        }
    
                        // Return to caller
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().write("{\"result\":\"OK\"}");
                        response.setContentType("application/json");
                    } else {
                        response.getWriter().write("{\"result\":\"ERROR: Can't complete all tasks, please verify current status and make sure they can be set to the selected one.\"}");
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.setContentType("application/json");
                    }
                } catch (Exception e) {
                    log.error("Can't complete proeject", e);
                } finally {
                    if (resourceResolver != null) {
                        resourceResolver.close();
                    }
                }
            }

        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    
    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }

    private List<String> getProductPaths(String projectPath, ResourceResolver resourceResolver) throws RepositoryException {
        List<String> result = new ArrayList<>();
        Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
        Node jcrContentNode = projectNode.getNode("jcr:content");
        String damFolderPath = jcrContentNode.getProperty("damFolderPath").getString();
        Node projectDamFolderNode = resourceResolver.getResource(damFolderPath).adaptTo(Node.class);
        NodeIterator nodeIt = projectDamFolderNode.getNodes();
        while (nodeIt.hasNext()) {
            Node prodNode = nodeIt.nextNode();
            if (isFolder(prodNode) 
                    && prodNode.getName().toLowerCase().indexOf("composites")<0 
                    && prodNode.getName().toLowerCase().indexOf("reference")<0 ) {
                result.add(prodNode.getPath());
            }
        }
        return result;
    }


    private boolean isFolder(Node resourceNode) throws RepositoryException {
        return resourceNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0;
    }



}