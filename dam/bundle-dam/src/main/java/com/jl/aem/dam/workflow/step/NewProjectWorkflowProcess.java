package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.briefs.service.BriefsService;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=New Project Workflow Process"})
public class NewProjectWorkflowProcess extends AbstractProjectWorkflowProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(NewProjectWorkflowProcess.class);
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Override
    public void execute(WorkItem item, WorkflowSession session, MetaDataMap arg2)
            throws WorkflowException {
        try {
            Node projectNode = getProjectFromPayload(item, session.getSession(), resolverFactory);
            
            if (isBriefProject(projectNode)) {

                Node jcrContent = projectNode.getNode("jcr:content");
                if (jcrContent.hasProperty("filename")) {
                    String filename = jcrContent.getProperty("filename").getString();
                    
                    if (BriefUtils.isCutoutBrief(jcrContent)) {
                    	briefsService.importCutoutBriefCSV(projectNode.getPath(), filename);
                    } else if (BriefUtils.isLifestyleBrief(jcrContent)) {
                    	briefsService.importLifestyleBriefCSV(projectNode.getPath(), filename);
                    } else if (BriefUtils.isRetouchBrief(jcrContent)) {
                        briefsService.importRetouchBriefCSV(projectNode.getPath(), filename);
                    }
                } else {
                    jcrContent.setProperty("project.csverror", Boolean.TRUE);
                    session.getSession().save();
                    notifyUser("CSV file not found");
                }
            } 
        } catch (Exception e) {
            log.error("Error importing brief csv", e);
            notifyUser(e.getCause().getMessage());
        }
        
    }
    
    private void notifyUser(String string) {
        // TODO Notify user about wrong CSV file
        
    }

    
}
