package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.ProductStatus;
import com.jl.aem.dam.workflow.WorkflowBusinessSettings;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.pim.ProductService;
import com.jl.aem.dam.workflow.DailySupplierNotificationService;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;


@Component(immediate=true, name="JLP Daily supplier Notification Service", service=DailySupplierNotificationService.class)
public class DailySupplierNotificationServiceImpl implements DailySupplierNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(DailySupplierNotificationServiceImpl.class);
    private static String UPLOAD_MESSAGE = "Please upload assets to your creative cloud account <br/>";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Reference
    private ProductService productService;

    @Reference
    private JLPWorkflowSettings workflowSettings;
    
    @Reference
    private WorkflowBusinessSettings businessSettings;
    
    @Override
    public void execute() {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
             Map<String, Object> param = new HashMap<String, Object>();
             param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
             resourceResolver = resolverFactory.getServiceResourceResolver(param);
     session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
                logger.error("Can not login system user");
                throw new RuntimeException(e);
        }
        try {
            SearchResult result = searchProducts(resourceResolver, session);
            if(result.getTotalMatches() > 0) {
                Map<String,ArrayList<Product>> productsBySuplier = getProductsBySuplier(result);
                
                for (Entry<String,ArrayList<Product>> entry : productsBySuplier.entrySet()) {
                    sentSupplierNotification(entry.getKey(), entry.getValue(), resourceResolver, session);
                }
            } else {
                logger.info("DailySupplierNotificationService: There are no new products to notify.");
            }
            
          
        } catch (Exception e) {
            logger.error("Fatal error while creating daily product suppliares notifications: ", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        } 
    }
    
    
    private void changeProductStatus( Product p, ResourceResolver resourceResolver) {
        logger.info("changeProductStatus Product: " + p.getProdCode());
        p.setStatusId(ProductStatus.IMAGE_REQUESTED.toString());
        p.setImageRequestedDate(Calendar.getInstance());
        productService.updateProduct(p);
    }

    private void sentSupplierNotification(String supplier, ArrayList<Product> products, ResourceResolver resourceResolver, Session session) {
        boolean succesfull = true;
        if (workflowSettings.getEnableEmailNotification()) {
            try {
                // Getting the Email template.
                String emailTemplate = "/etc/notification/email/html/jlp/assetUploadRequestTemplate.txt";
                Resource templateRsrc = resourceResolver.getResource(emailTemplate);
                
                MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
                MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                
                // Creating the Email.
                HtmlEmail email = new HtmlEmail();
                Map<String, String> emailProperties = new HashMap<String,String>();
                String productList = "";
                for(Product p: products){
                    if(p.getDeleted().equals("N") && !p.getStatusId().equals(ProductStatus.MISSING_METADATA)) {
                        productList += "<tr>"
                            + "<td>" + p.getProdCode() + "</td>" +
                            "<td>" + p.getBrandName() + "</td>" +  
                            "<td>" + p.getStockLevel() + "</td>" +
                            "<td>" + JLWorkflowsUtils.getBuyingGrupByOffice(p.getBuyingOffice(), resourceResolver) + "</td>" +
                            "<td>" + UPLOAD_MESSAGE + "</td>" +
                            "</tr>";
                    }
                }
                emailProperties.put("logoUrl", workflowSettings.getEmailLogoUrl());
                emailProperties.put("logoAlt", workflowSettings.getEmailLogoAlt());
                emailProperties.put("logoHeight", Integer.toString(workflowSettings.getEmailLogoHeight()));
                emailProperties.put("products", productList);
                emailProperties.put("signature", workflowSettings.getEmailSignature());
                email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                ArrayList<InternetAddress> recipients = new ArrayList<InternetAddress>();
                recipients.add(new InternetAddress(supplier));
                email.setTo(recipients);
                
                messageGateway.send(email);
            } catch (Exception e) {
                logger.error("Fatal error while sending email: ", e);
                logger.error("It was not possible to sent the email to supplier: " + supplier);
                succesfull = false;
            }
        }
        //If email was sent then products should change their status to 'Image Requested'
        if(succesfull) {
            logger.info("sentSupplierNotification Change status products" );
            
            for(Product p: products) {
                logger.info("Product: " + p.getProdCode());
                if(p.getDeleted().equals("N") && !p.getStatusId().equals(ProductStatus.MISSING_METADATA)) {
                    changeProductStatus(p, resourceResolver);
                }
            }
            try {
                session.save();
            } catch (RepositoryException e) {
                logger.error("Fatal error while saving status: ", e);
            }
        }
    }


    private Map<String, ArrayList<Product>> getProductsBySuplier(SearchResult result) {
        List<Hit> hits = result.getHits();
        Map<String,ArrayList<Product>> productsBySuplier = new HashMap<String,ArrayList<Product>>();
        
        for (Hit hit : hits) {
             try {
                 Node node = hit.getNode();
                 Product p = JLWorkflowsUtils.populateProductFromNode(node);
                 if (businessSettings.isInWorkflow(p.getBrandName(), p.getBuyingOffice())) {
                     String supplier = p.getSupplierContactDetail();
                     if(!productsBySuplier.containsKey(supplier)){
                    	 productsBySuplier.put(supplier,new ArrayList<Product>());
                     }
                     productsBySuplier.get(supplier).add(p);
                 }
             } catch (RepositoryException e) {
                 logger.error("Can not process asset.", e);
             }
            }
        return productsBySuplier;
    }


    private SearchResult searchProducts(ResourceResolver resourceResolver, Session session) {
        PredicateGroup predicates = new PredicateGroup();

        logger.info("Search products for daily supplier notification");
        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", "/etc/commerce/products/onejl");
        
        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "nt:unstructured");

        Predicate statusPredicate = new Predicate("status", "property");
        statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "statusId");
        statusPredicate.set(JcrPropertyPredicateEvaluator.VALUE, ProductStatus.IMAGE_REQUIRED. toString());
        statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);
        
        Predicate sourcePredicate = new Predicate("source", "property");
        sourcePredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "imageSource");
        sourcePredicate.set(JcrPropertyPredicateEvaluator.VALUE, "supplierEmail". toString());
        sourcePredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);

        Predicate hasItemId = new Predicate("hasItemId", "property");
        hasItemId.set(JcrPropertyPredicateEvaluator.PROPERTY, ProductConstants.WSM_ITEM_ID);
        hasItemId.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EXISTS);
        hasItemId.set(JcrPropertyPredicateEvaluator.VALUE, "true");


        predicates.add(pathPredicate);
        predicates.add(statusPredicate);
        predicates.add(sourcePredicate);
        predicates.add(hasItemId);
        predicates.add(typePredicate);
        predicates.setAllRequired(true);

        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);

        return query.getResult();
    }
}