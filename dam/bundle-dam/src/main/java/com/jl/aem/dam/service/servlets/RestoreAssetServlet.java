package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.archive.service.ArchiveService;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.impl.BriefsServiceImpl;
import com.jl.aem.dam.briefs.workflow.BriefAssetStatus;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/restoreAsset.json",
        "sling.servlet.methods=POST"})
public class RestoreAssetServlet  extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    //private static final String TEMP_PATH = "/content/dam/projects/temp/retouch";
    
    private static final Logger logger = LoggerFactory.getLogger(BriefsServiceImpl.class);

    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private ArchiveService archiveService;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        doPost(request, response);
    }
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String[] items = request.getParameterValues("item");
        ResourceResolver resourceResolver = request.getResourceResolver();
        for(String path : items) {
            Resource resource = resourceResolver.getResource(path);
            if (resource != null) {
               String result = archiveService.restoreAsset(path);
               if (!"OK".equals(result)) {
                   response.getWriter().println(result);
               }
               
            }
        }
        response.getWriter().write("OK");
        response.setContentType("text/plain");
    }

    

    
    
}
