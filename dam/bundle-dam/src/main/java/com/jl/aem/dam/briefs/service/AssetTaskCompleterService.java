package com.jl.aem.dam.briefs.service;

import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;

public interface AssetTaskCompleterService {

    boolean canComplete(String assetPath, AutocompleteStatus newStatus);
    void complete(String assetPath, AutocompleteStatus newStatus, String comments);
    
}
