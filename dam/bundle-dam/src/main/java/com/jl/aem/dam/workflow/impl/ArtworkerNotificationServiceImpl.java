package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.ArtworkerNotificationService;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;


@Component(immediate=true, name="JLP Artworkers Notification Service", service=ArtworkerNotificationService.class)
public class ArtworkerNotificationServiceImpl implements ArtworkerNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(ArtworkerNotificationServiceImpl.class);
    private static String ASSET_FOLDER_MESSAGE = "You can find the image in the following folder: <br/>";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private JLPWorkflowSettings settings;
    
    @Override
    public void execute() {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
             Map<String, Object> param = new HashMap<String, Object>();
             param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
             resourceResolver = resolverFactory.getServiceResourceResolver(param);
             session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
                logger.error("Can not login system user");
                throw new RuntimeException(e);
        }
        try {
            SearchResult result = searchArtworkerAssets(resourceResolver, session);
            if( result.getTotalMatches() > 0 && settings.getEnableEmailNotification()){
                sentArtworkerNotification(result, resourceResolver, session);
            }
            session.save();
            resourceResolver.commit();
        } catch (Exception e) {
            logger.error("Fatal error while creating artworker notifications: ", e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        } 
    }

    private void sentArtworkerNotification(SearchResult result, ResourceResolver resourceResolver, Session session) {
        ArrayList<Node> metadataNodes = new ArrayList<Node>();
        try {
            // Getting the Email template.
            String emailTemplate = "/etc/notification/email/html/jlp/artworkersNotifyTemplate.txt";
            Resource templateRsrc = resourceResolver.getResource(emailTemplate);
            MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
            MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
            
            // Creating the Email.
            HtmlEmail email = new HtmlEmail();
            Map<String, String> emailProperties = new HashMap<String,String>();
            String assetList = "";
            List<Hit> hits = result.getHits();
            for (Hit hit : hits) {
                Node assetNode = hit.getNode();
                Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                assetList += "Name: <b>" + assetNode.getName() + "</b> <br/>" +
                    "Product: <b>" + metadataNode.getProperty(JLPConstants.JLP_PRODUCT_CODES).getString() + "</b> <br/>" + 
                    "Directorate: <i>" + metadataNode.getProperty(JLPConstants.JLP_DIRECTORATE).getString() + "</i> <br/>" + 
                    ASSET_FOLDER_MESSAGE +
                    "Path: " + assetNode.getPath() + "<br/><br/>";
                metadataNodes.add(metadataNode);
            }
            emailProperties.put("logoUrl", settings.getEmailLogoUrl());
            emailProperties.put("logoAlt", settings.getEmailLogoAlt());
            emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
            emailProperties.put("signature", settings.getEmailSignature());
            emailProperties.put("assets", assetList);
            email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
            ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
            for (String recipient : settings.getInvalidProductRecipientList()) {
                try {
                    emailRecipients.add(new InternetAddress(recipient));
                } catch (AddressException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            email.setTo(emailRecipients);
            
            messageGateway.send(email);
            
            
        } catch (Exception e) {
            logger.error("Fatal error while sending email: ", e);
            logger.error("It was not possible to sent the for artworkers " );
        }
        try {
            for (Node assetMetadata: metadataNodes) {
                assetMetadata.setProperty( JLPConstants.JLP_STATUS, 
                        AssetStatus.ASSET_ARTWORKING_REQUESTED.toString());
            }
        } catch (Exception e) {
            logger.error("It was not possible to set the status artworker requested to the asset", e);
        }
    }

    private SearchResult searchArtworkerAssets(ResourceResolver resourceResolver, Session session) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", JLPConstants.ARTWORKERS_PENDING_RETOUCH);
        
        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "dam:Asset");

        Predicate statusPredicate = new Predicate("status", "property");
        statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_STATUS);
        statusPredicate.set(JcrPropertyPredicateEvaluator.VALUE, AssetStatus.ASSET_PENDING_ARTWORKING.toString());
        statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);

        predicates.add(pathPredicate);
        predicates.add(statusPredicate);
        predicates.add(typePredicate);
        predicates.setAllRequired(true);

        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);

        return query.getResult();
    }
}