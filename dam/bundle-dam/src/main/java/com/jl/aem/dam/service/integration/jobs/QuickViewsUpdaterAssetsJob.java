package com.jl.aem.dam.service.integration.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.archive.service.ArchiveService;
import com.jl.aem.dam.archive.service.JLPArchivingSettings;
import com.jl.aem.dam.service.integration.QuickViewsService;

@Component(immediate = true, name = "Quick views updater assets job",service= {Runnable.class},
    property ={"scheduler.expression=0 0 0 * * ? "})
public class QuickViewsUpdaterAssetsJob implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(QuickViewsUpdaterAssetsJob.class);


    @Reference
    private QuickViewsService quickViewsService;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance ) {
            logger.trace("Running quick views updater job");
            quickViewsService.createOrUpdateQuickViews();
            logger.trace("Quick views updater completed");
        }
    }
}