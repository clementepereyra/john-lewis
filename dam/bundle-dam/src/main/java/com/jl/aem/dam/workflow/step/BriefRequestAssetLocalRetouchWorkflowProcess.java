package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = WorkflowProcess.class,
        property = {"process.label=Process Request Asset Local Retouch"})
public class BriefRequestAssetLocalRetouchWorkflowProcess
        extends AbstractBriefAssetRetouchedWorkflowProcess implements WorkflowProcess {

    private static final Logger log =
            LoggerFactory.getLogger(BriefRequestAssetLocalRetouchWorkflowProcess.class);

    @Reference
    ResourceResolverFactory resolverFactory;

    @Override
    public void execute(WorkItem item, WorkflowSession wfSession, MetaDataMap metadata)
            throws WorkflowException {
        try {
            Session session = wfSession.getSession();
            ResourceResolver resourceResolver = getResourceResolver(resolverFactory, session);

            Node projectNode = getProjectFromPayload(item, session, resolverFactory);

            String workflowTitle = item.getWorkflow().getWorkflowData().getMetaDataMap()
                    .get("workflowTitle", String.class);
            String productCode = workflowTitle.split("-")[0];
            String altCode = workflowTitle.split("-")[1];
            
            Node jcrContent = projectNode.getNode("jcr:content");
            Node damFolderNode = resourceResolver.getResource(
                    jcrContent.getProperty("damFolderPath").getString()
                    ).adaptTo(Node.class);
            Node productNode = damFolderNode.hasNode(productCode)?damFolderNode.getNode(productCode):null;
            if (productNode == null) {
                NodeIterator productNodes = damFolderNode.getNodes();
                while (productNodes.hasNext()) {
                    Node candidateNode = productNodes.nextNode();
                    if (candidateNode.hasProperty("ean")) {
                        if (productCode.equals(candidateNode.getProperty("ean").getString())) {
                            productCode = candidateNode.getName();
                            break;
                        }
                    }
                }
            }
            Node projectContentNode = projectNode.getNode(JCR_CONTENT);

            if (projectContentNode.hasProperty(JLPConstants.DAM_FOLER_PATH)) {
                String damFolderPath =
                        projectContentNode.getProperty(JLPConstants.DAM_FOLER_PATH).getString();
                
                StringBuilder assetFolderPathSB = new StringBuilder(damFolderPath).append(SLASH)
                        .append(productCode).append(SLASH).append(altCode);
                
                Resource altRes = resourceResolver.getResource(assetFolderPathSB.toString());
                if (altRes != null) {
                    Node altNode = altRes.adaptTo(Node.class);
                    altNode.setProperty(JLPConstants.JL_RETOUCHER, "true");
                    altNode.setProperty(JLPConstants.RETOUCH_MODE, "local");
                    NodeIterator nodeIt = altNode.getNodes();          
                    while (nodeIt.hasNext()) {
                        Node assetNode = (Node) nodeIt.next();
                        if (assetNode.getPrimaryNodeType().getName().equals(DAM_ASSET)) {
                            assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.RETOUCH_MODE, "local");
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error processing Request Asset local Retouch: {}", e);
        }

    }

}
