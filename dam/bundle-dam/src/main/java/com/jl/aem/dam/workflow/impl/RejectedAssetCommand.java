package com.jl.aem.dam.workflow.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;


@Component(immediate = true, service = RejectedAssetCommand.class)
public class RejectedAssetCommand implements AssetStatusCommand {

    private static final Logger logger = LoggerFactory.getLogger(RejectedAssetCommand.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private InvokeAEMWorkflow workflowInvoker;

    @Reference
    private JLPWorkflowSettings settings;

    private static final String MOVE_ASSET_WORKFLOW =
            "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Override
    public void execute(String assetPath, final String userID) {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Resource resource = resourceResolver.getResource(assetPath);
            Node assetNode = resource.adaptTo(Node.class);
            String errorMessage = "No error message specified - Please check with JL DAM admin";
            if (assetNode.getNode("jcr:content/metadata").hasProperty(JLPConstants.JLP_COMMENT)) {
                errorMessage = assetNode.getProperty("jcr:content/metadata/" + JLPConstants.JLP_COMMENT).getString();
            }
            assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.ASSET_STATUS_CHANGE_REJECTED_TIME,Calendar.getInstance());
            assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.ASSET_REJECTED_AUTO, assetNode.getPath().contains(JLPConstants.SUPPLIERS_ROOT_FOLDER));
            resourceResolver.commit();
            
            if (assetNode.getPath().contains(JLPConstants.IC_REVIEW_PATH)) {
                // These are assets rejected by the image coordinator.
                if (assetNode.getNode("jcr:content/metadata").hasProperty(JLPConstants.JLP_SUPPLIER_ID)) {
                    String supplierID = assetNode.getNode("jcr:content/metadata").getProperty(JLPConstants.JLP_SUPPLIER_ID).getString();
                    //String supplierFolder = JLWorkflowsUtils.formatSupplierID(supplierID);
                    String brandName = null;
                    if (assetNode.getNode("jcr:content/metadata").getProperty(JLPConstants.JLP_BRAND_NAME) != null) {
                        brandName = assetNode.getNode("jcr:content/metadata").getProperty(JLPConstants.JLP_BRAND_NAME).getString();
                    } else {
                        // This is for logos, assuming those are the only ones with no brandname property.
                        brandName = assetNode.getName().substring(0, assetNode.getName().indexOf("_"));
                    }
                    String targetFolder = JLPConstants.SUPPLIERS_ROOT_FOLDER + "/" +JLWorkflowsUtils.encodePath(brandName) + "/rejected";
                    String targetFileName =  assetNode.getName();
                    
                    
                    Map<String, Object> metaData = new HashMap<>();
                    metaData.put("targetFolder", targetFolder);
                    metaData.put("targetFileName", targetFileName);
                    if (supplierID != null) {
                        notifySupplier(targetFolder, targetFileName, supplierID, resourceResolver, errorMessage, session);
                    }
                    workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
                }
            } else if (assetNode.getPath().contains(JLPConstants.SUPPLIERS_ROOT_FOLDER)) {
                // These are assets not passing the technical review (might be wrongly named or not low quality assets).
                if (assetNode.getParent().getParent().hasNode("rejected")) {
                    String supplierID = JLWorkflowsUtils.getSupplierIDForNode(assetNode);
                    String targetFolder = assetNode.getParent().getParent().getNode("rejected").getPath();
                    String targetFileName = assetNode.getName();
                    
                    Map<String, Object> metaData = new HashMap<>();
                    metaData.put("targetFolder", targetFolder);
                    int i = 0;
                    while (resourceResolver.getResource(targetFolder + "/" + targetFileName) != null) {
                        targetFileName = assetNode.getName().replace(".", "_" + i + ".");
                        i++;
                    }
                    
                    metaData.put("targetFileName", targetFileName);
                    if (supplierID != null) {
                        notifySupplier(targetFolder, targetFileName, supplierID, resourceResolver, errorMessage, session);
                    }
                    workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
                }   
            }
            

        } catch (Exception e){
            logger.error("Can not validate new asset: " + assetPath, e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }

    private void notifySupplier(final String targetFolder, final String targetFileName,
            final String supplierID, final ResourceResolver resourceResolver,
            final String errorMessage, final Session session) {
        if(settings.getEnableEmailNotification()){
            String emailTemplate = "/etc/notification/email/html/jlp/rejectedUploadedAsset.txt";
            Resource templateRsrc = resourceResolver.getResource(emailTemplate);
    
            MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
    
            try {
                MessageGateway<HtmlEmail> messageGateway =
                        messageGatewayService.getGateway(HtmlEmail.class);
                // Creating the Email.
                HtmlEmail email = new HtmlEmail();
                Map<String, String> emailProperties = new HashMap<String, String>();
                emailProperties.put("errorMessage", errorMessage);
                emailProperties.put("assetPath", targetFolder + "/" + targetFileName);
                emailProperties.put("logoUrl", settings.getEmailLogoUrl());
                emailProperties.put("logoAlt", settings.getEmailLogoAlt());
                emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
                emailProperties.put("signature", settings.getEmailSignature());
                email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                email.setTo(Arrays.asList(new InternetAddress(supplierID)));
                messageGateway.send(email);
            } catch (Exception e) {
                logger.error("Fatal error while sending email: ", e);
            }
        }
    }

}
