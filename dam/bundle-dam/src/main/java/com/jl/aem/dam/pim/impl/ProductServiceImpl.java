package com.jl.aem.dam.pim.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.version.VersionException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.oak.plugins.nodetype.NodeTypeConstants;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.dam.api.Asset;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.workflow.WorkflowService;
import com.jl.aem.dam.briefs.service.ReconcileProductUtils;
import com.jl.aem.dam.briefs.workflow.ProjectWorkflowInfo;
import com.jl.aem.dam.briefs.workflow.impl.ProjectWorkflowHelper;
import com.jl.aem.dam.pim.CompletedImage;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.pim.ProductService;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.AssetStatusCommandFactory;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.jl.aem.dam.workflow.ProductStatus;
import com.jl.aem.dam.workflow.step.helper.JLProductAssociator;

@Component(immediate=true, service=ProductService.class)
public class ProductServiceImpl implements ProductService {
    
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);
    
    private String rootProductsPath = "/etc/commerce/products/onejl";
    
    private static final String INVALID_PRODUCT_MAIL_TEMPLATE = "/etc/notification/email/html/jlp/invalidProductDataTemplate.txt";
    private static final String EMPTY_FIELDS_PRODUCT_MAIL_TEMPLATE = "/etc/notification/email/html/jlp/productEmptyFieldsEmailTemplate.txt";
    public static String UPLOAD_MESSAGE = "Please upload assets into the JL shared folder of your creative cloud account: <br/>";
    
    private static final String MOVE_ASSET_WF = "/etc/workflow/models/dam/move_asset/jcr:content/model";
    private static final String CUTOUT_COMPLETION_WORKFLOW_MODEL = "/etc/workflow/models/jl-cutout-workflowcompletion/jcr:content/model";
    private static final Pattern IMAGE_CAT_PATTERN = Pattern.compile("(main|alt\\d*)");
    private static final String IMAGE_CAT_ROOT = "jlp:image-category/";
    
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private AssetStatusCommandFactory assetStatusCommandFactory;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private JLPWorkflowSettings settings;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private WorkflowService workflowService;
    
    private String getReconcileProductPath(String tradedCode) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            return ReconcileProductUtils.getProductBriefToReconcile(tradedCode, resourceResolver);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    private void updateProductReceivedInfo(String productPath, String briefProductPath,
            ResourceResolver resourceResolver) {
        try {
            Node prodNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            Node briefProdNode =
                    resourceResolver.getResource(briefProductPath).adaptTo(Node.class);
            String receivedBy = "N/A";
            if (briefProdNode.hasProperty("receivedBy")) {
                receivedBy = briefProdNode.getProperty("receivedBy").getString();
            }
            String quantity = "0";
            if (briefProdNode.hasProperty("quantityReceived")) {
                quantity = briefProdNode.getProperty("quantityReceived").getString();
            }
            Calendar dateReceived = Calendar.getInstance();
            if (briefProdNode.hasProperty("dateReceived")) {
               dateReceived = briefProdNode.getProperty("dateReceived").getDate();
            }
            if (!"0".equals(quantity)) {
                Node historyNode = prodNode.hasNode("quantityHistory")?prodNode.getNode("quantityHistory"):prodNode.addNode("quantityHistory", "nt:unstructured");
                
                Node newEntry = historyNode.addNode("receive"+ System.currentTimeMillis());
                newEntry.setProperty("quantityReceived", quantity);
                newEntry.setProperty("type", "receive"); 
                newEntry.setProperty("user", receivedBy);
                newEntry.setProperty("date", dateReceived);
                
                UserManager userManager = resourceResolver.adaptTo(UserManager.class);
                User user = (User) userManager.getAuthorizable(receivedBy);
                String lastName = user.getProperty("./profile/familyName")!=null?user.getProperty("./profile/familyName")[0].getString():"";
                String firstName = user.getProperty("./profile/givenName")!=null?user.getProperty("./profile/givenName")[0].getString():"";        
                newEntry.setProperty("userName", firstName + " " + lastName);
                
                long currentQuantity = prodNode.hasProperty("quantityReceived")?prodNode.getProperty("quantityReceived").getLong():0l;
                currentQuantity += Long.parseLong(quantity); 
                prodNode.setProperty("quantityReceived", currentQuantity);
            }
        } catch (Exception e) {
            logger.error("error while trying to reconcile Reveived Info data: {}", e);
        }

    }
    
    private String reconcileBriefProductName(String productCode, String briefProductPath,
            ResourceResolver resourceResolver, Session session) {
        try {
            Node briefProdNode = resourceResolver.getResource(briefProductPath).adaptTo(Node.class);

            String targetFolder = briefProdNode.getParent().getPath();
            String targetFullPath =
                    targetFolder + (targetFolder.endsWith("/") ? "" : "/") + productCode;

            session.move(briefProdNode.getPath(), targetFullPath);
            
            return targetFullPath;

        } catch (Exception e) {
            logger.error("error while trying to reconcile Product Name: {}", e);
        }
        return null;
    }
    
    private void reconcileBriefThirdPartyFolder(String productCode, String briefProductPath,
            ResourceResolver resourceResolver, Session session) {
        try {
            
            
            Node briefProdNode =
                    resourceResolver.getResource(briefProductPath).adaptTo(Node.class);
            
            String briefName = briefProdNode.getParent().getName();
            String eanCode = briefProdNode.getName();
            Resource thirdPartyFolder = resourceResolver.getResource(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH);
            if (thirdPartyFolder != null) {
                Node thirdPartyFolderNode =  thirdPartyFolder.adaptTo(Node.class);
                NodeIterator thirdParties = thirdPartyFolderNode.getNodes();
                while (thirdParties.hasNext()) {
                    Node thirdParty = thirdParties.nextNode();
                    Node pendingRetouch = thirdParty.hasNode(JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER)?thirdParty.getNode(JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER):null;
                    Node retouched = thirdParty.hasNode(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER)?thirdParty.getNode(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER):null;
     
                    if (pendingRetouch != null) {
                        NodeIterator pendingBriefsIt = pendingRetouch.getNodes();
                        while (pendingBriefsIt.hasNext()) {
                            Node pendingBriefNode = pendingBriefsIt.nextNode();
                            if (pendingBriefNode.getName().equals(briefName)) {
                                NodeIterator pendingProdIt = pendingBriefNode.getNodes();
                                while (pendingProdIt.hasNext()) {
                                    Node pendingProdNode = pendingProdIt.nextNode();
                                    if (pendingProdNode.getName().equals(eanCode)) {
                                        String targetFolder = pendingProdNode.getParent().getPath();
                                        String targetFullPath =
                                                targetFolder + (targetFolder.endsWith("/") ? "" : "/") + productCode;

                                        session.move(pendingProdNode.getPath(), targetFullPath);
                                    }
                                }
                            }
                        }
                    }
                    
                    if (retouched != null) {
                        NodeIterator pendingBriefsIt = retouched.getNodes();
                        while (pendingBriefsIt.hasNext()) {
                            Node pendingBriefNode = pendingBriefsIt.nextNode();
                            if (pendingBriefNode.getName().equals(briefName)) {
                                NodeIterator pendingProdIt = pendingBriefNode.getNodes();
                                while (pendingProdIt.hasNext()) {
                                    Node pendingProdNode = pendingProdIt.nextNode();
                                    if (pendingProdNode.getName().equals(eanCode)) {
                                        String targetFolder = pendingProdNode.getParent().getPath();
                                        String targetFullPath =
                                                targetFolder + (targetFolder.endsWith("/") ? "" : "/") + productCode;

                                        session.move(pendingProdNode.getPath(), targetFullPath);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
           

        } catch (Exception e) {
            logger.error("error while trying to reconcile Product Name: {}", e);
        }
    }
    
    private void setBriefListInProduct(String productPath, String briefProductPath,
            ResourceResolver resourceResolver) {
        try {
            Node briefProdNode = resourceResolver.getResource(briefProductPath).adaptTo(Node.class);
            Node prodNode = resourceResolver.getResource(productPath).adaptTo(Node.class);

            Node projectDamNode = briefProdNode.getParent();
            if (projectDamNode.hasProperty(JLPConstants.JLP_PROJECT_PATH)) {
                List<String> pathList = new LinkedList<String>();
                Property pathProp = projectDamNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
                if (pathProp.isMultiple()) {
                    for (Value value : pathProp.getValues()) {
                        pathList.add(value.getString());
                    }
                } else {
                    pathList.add(pathProp.getString());
                }
                String[] briefs = pathList.toArray(new String[pathList.size()]);
                prodNode.setProperty(ProductConstants.BRIEFS, briefs);
            }
        } catch (Exception e) {
            logger.error("error while trying to set BriefList in product: {}", e);
        }
    }
 
    private void reconcileBriefProductProperties(String productPath, String briefProductPath,
            ResourceResolver resourceResolver) {
        try {
            Node briefProdNode = resourceResolver.getResource(briefProductPath).adaptTo(Node.class);
            Node prodNode = resourceResolver.getResource(productPath).adaptTo(Node.class);

            briefProdNode.setProperty("stockNumber",
                        prodNode.getProperty(ProductConstants.PRODUCT_CODE).getString());

            if (prodNode.hasProperty(ProductConstants.WSM_TITLE)) {
                briefProdNode.setProperty("productName",
                        prodNode.getProperty(ProductConstants.WSM_TITLE).getString());
            }
            if (prodNode.hasProperty(ProductConstants.WSM_ITEM_ID)) {
                briefProdNode.setProperty("webSku",
                        prodNode.getProperty(ProductConstants.WSM_ITEM_ID).getString());
            }
            if (prodNode.hasProperty(ProductConstants.SEL_DESC)) {
                briefProdNode.setProperty("description",
                        prodNode.getProperty(ProductConstants.SEL_DESC).getString());
            }
            if (prodNode.hasProperty(ProductConstants.TRADED_CODE)) {
                briefProdNode.setProperty("ean",
                        prodNode.getProperty(ProductConstants.TRADED_CODE).getString());
            }
            if (prodNode.hasProperty(ProductConstants.SUPPLIER_CONTACT_DETAIL)) {
                briefProdNode.setProperty("supplier",
                        prodNode.getProperty(ProductConstants.SUPPLIER_CONTACT_DETAIL).getString());
            }
            if (prodNode.hasProperty(ProductConstants.COLOUR)) {
                briefProdNode.setProperty("colour",
                        prodNode.getProperty(ProductConstants.COLOUR).getString());
            }
            if (prodNode.hasProperty(ProductConstants.BRAND_NAME)) {
                briefProdNode.setProperty("brand",
                        prodNode.getProperty(ProductConstants.BRAND_NAME).getString());
            }
            if (prodNode.hasProperty(ProductConstants.BRAND_NAME)) {
                briefProdNode.setProperty("productPath", productPath);
            }
            briefProdNode.setProperty("status", "unavailable");
            briefProdNode.getProperty(JLPConstants.JLP_RECONCILE_PRODUCT).remove();

        } catch (Exception e) {
            logger.error("error while trying to reconcile Product properties: {}", e);
        }
    }

    private void reconcileAssets(String productPath, String briefProductPath,
            ResourceResolver resourceResolver, Session session) {
        try {
            List<String> assetPaths = ReconcileProductUtils
                    .getAssetPathsToReconcile(briefProductPath, resourceResolver);
            for (String assetPath : assetPaths) {
                if (!(assetPath.indexOf("/" + JLPConstants.COMPOSITE_FOLDER + "/") > 0)
                        && !(assetPath.indexOf("/" + JLPConstants.REFERENCE_FOLDER + "/") > 0)) {
                    reconcileAsset(assetPath, productPath, resourceResolver, session);
                }
            }
            
            assetPaths = ReconcileProductUtils
                    .getThirdPartyAssetPathsToReconcile(briefProductPath, resourceResolver);
            for (String assetPath : assetPaths) {
                if (!(assetPath.indexOf("/" + JLPConstants.COMPOSITE_FOLDER + "/") > 0)
                        && !(assetPath.indexOf("/" + JLPConstants.REFERENCE_FOLDER + "/") > 0)) {
                    reconcileAsset(assetPath, productPath, resourceResolver, session);
                }
            }

        } catch (Exception e) {
            logger.error("error while trying to reconcile Assets: {}", e);
        }
    }
    
    private void reconcileAsset(String assetPath, String productPath,
            ResourceResolver resourceResolver, Session session) {
        try {
            Node prodNode = resourceResolver.getResource(productPath).adaptTo(Node.class);

            Node asNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
           
            String altCode = getAltCode(asNode);
            Map<String, String> productInfo = new HashMap<>();
            productInfo.put("productCode", prodNode.getName());
            productInfo.put("productPath", productPath);
            productInfo.put("imageCat", getImageCategory(altCode));
            JLProductAssociator associator = new JLProductAssociator();
            associator.associateAssetWithProduct(asNode, resourceResolver, session, productInfo);

            if ("main".equals(altCode)) {
                altCode = "";
            }

            String wsmItemId = StringUtils.EMPTY;
            String ean = prodNode.getProperty(ProductConstants.TRADED_CODE).getString();
            if (prodNode.hasProperty(ProductConstants.WSM_ITEM_ID)) {
                wsmItemId = prodNode.getProperty(ProductConstants.WSM_ITEM_ID).getString();
            }
            
            if (asNode.getNode("jcr:content").hasProperty(JLPConstants.THIRD_PARTY_ASSET_PATH)) {
                String thirdPartyRef = asNode.getNode("jcr:content").getProperty(JLPConstants.THIRD_PARTY_ASSET_PATH).getString();
                thirdPartyRef = thirdPartyRef.replace("/"+ ean + "/", "/" + prodNode.getName() + "/")
                                             .replace("/" + ean , "/" + JLWorkflowsUtils.addLeftZeros(wsmItemId));
                asNode.getNode("jcr:content").setProperty(JLPConstants.THIRD_PARTY_ASSET_PATH, thirdPartyRef);
                
            }

            String targetFolder = asNode.getParent().getPath();
            String extension = asNode.getName().substring(asNode.getName().lastIndexOf("."));
            String targetFileName = JLWorkflowsUtils.addLeftZeros(wsmItemId) + altCode + extension;
            
            session.save();
            resourceResolver.commit();

            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder);
            metaData.put("targetFileName", targetFileName);
            metaData.put("conflictResolution", "keepboth");
            workflowInvoker.startWorkflow(MOVE_ASSET_WF, asNode.getPath(), metaData);

        } catch (Exception e) {
            logger.error("error while trying to reconcile Asset: {}", e);
        }

    }
  
    private String getAltCode(final Node asNode) throws RepositoryException {
        boolean  limit = false;
        Node temp = asNode.getParent();
        while(!limit) {
            String path = temp.getName().toLowerCase();
            Matcher matcher = IMAGE_CAT_PATTERN.matcher(path);
            if (matcher.matches()) {
                String altCode = matcher.group(1);
                return altCode;
            } else {
                temp = temp.getParent();
                if (temp == null) {
                    limit = true;
                }
            }
        }
        
        return "main"; //default to main if parent is not found
    }
    
    private String getImageCategory(final String altCode) {
        StringBuilder imageCategory = new StringBuilder(IMAGE_CAT_ROOT);
        if (altCode.contains("alt")) {
           imageCategory.append("alt/").append(altCode);
        } else if (altCode.contains("mnl")) {
            imageCategory.append("mnl");
        } else if (altCode.contains("vid")) {
            imageCategory.append("vid/").append(altCode);
        } else {
            imageCategory.append("main/");
        }
        return imageCategory.toString();
    }
    
    private List<String> getPendingTaskForProject(final Node projectNode) throws RepositoryException {
        List<String> results = new ArrayList<>();
        Node tasksNode = projectNode.getNode("jcr:content/tasks");
        NodeIterator tasksIterator = tasksNode.getNodes();
        while (tasksIterator.hasNext()) {
            Node tasksGroupNode = tasksIterator.nextNode();
            NodeIterator taskItems = tasksGroupNode.getNodes();
            while (taskItems.hasNext()) {
                Node taskItem = taskItems.nextNode();
                if (JLPConstants.PROVIDE_SHOOT_CUTOUT_WORKFLOW_NAME
                        .equals(taskItem.getProperty("wfModelId").getString())
                        && "ACTIVE".equalsIgnoreCase(taskItem.getProperty("status").getString())) {
                    results.add(taskItem.getProperty("wfInstanceId").getString());
                }
            }
        }
        return results;
    }

    private void triggerReleaseWorkflow(String briefProductPath,
            ResourceResolver resourceResolver) {
        try {
            Node briefProdNode = resourceResolver.getResource(briefProductPath).adaptTo(Node.class);
            String projectPath = null;
            Node projectDamNode = briefProdNode.getParent();
            if (projectDamNode.hasProperty(JLPConstants.JLP_PROJECT_PATH)) {
                Property pathProp = projectDamNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
                if (pathProp.isMultiple() && pathProp.getValues().length > 0) {
                    projectPath = pathProp.getValues()[0].getString();
                } else {
                    projectPath = pathProp.getString();
                }
            }
            if (projectPath != null) {
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
                List<String> tasks = getPendingTaskForProject(projectNode);
                if (tasks.size() <= 0) {
                    String workflowTitle = "CutOut Brief Completion Workflow";
                    String workflowNodeName = "cutout-brief-completion-workflow";
                    String workflowModel = CUTOUT_COMPLETION_WORKFLOW_MODEL;
                    Node contentNode = projectNode.getNode("jcr:content");
                    ProjectWorkflowInfo workflowInfo = new ProjectWorkflowInfo(workflowTitle,
                            workflowNodeName, workflowModel, contentNode, "admin");
                    ProjectWorkflowHelper.startProjectWorkflow(workflowInfo, workflowService,
                            getResourceResolver());
                }
            }
        } catch (Exception e) {
            logger.error("Can not create workflow {} ", e);
        }
    }

    @Override
    public synchronized void addProduct(Product product) {
        ResourceResolver resourceResolver = getResourceResolver();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            Node rootProductsNode = null;
            Node suppliersRootNode = null;
            logger.info("Adding product" + product.getProdCode());
            try {
                rootProductsNode = JcrUtils.getOrCreateByPath(rootProductsPath, "sling:Folder", session);
                suppliersRootNode = JcrUtils.getOrCreateByPath(JLPConstants.SUPPLIERS_ROOT_FOLDER, "sling:Folder", session);
                session.save();
            } catch (RepositoryException e) {
                logger.error("Can not create or get root paths - aborting");
                throw new RuntimeException(e);
            }
            product.setStatusId( ProductStatus.IMAGE_REQUIRED.toString());
            String productPath = createOrUpdateNodeForProduct(rootProductsNode, product, resourceResolver, session);
            session.save();
            if ("supplierEmail".equalsIgnoreCase(product.getImageSource())) {
                createNodeForAssets(suppliersRootNode, product);
            }
            session.save();
            String briefProductPath = getReconcileProductPath(product.getTradedCode());
            if (briefProductPath != null) {
                
                reconcileBriefThirdPartyFolder(product.getProdCode(), briefProductPath, resourceResolver, session);

                String newBriefProductPath = reconcileBriefProductName(product.getProdCode(), briefProductPath, resourceResolver, session);
                reconcileBriefProductProperties(productPath, newBriefProductPath, resourceResolver);
                session.save();
                
                
                updateProductReceivedInfo(productPath, newBriefProductPath, resourceResolver);
                setBriefListInProduct(productPath, newBriefProductPath, resourceResolver);
                session.save();
                resourceResolver.commit();
                
                reconcileAssets(productPath, newBriefProductPath, resourceResolver, session);
                session.save();
                triggerReleaseWorkflow(newBriefProductPath, resourceResolver);
            }
            session.logout();
        } catch (Exception e) {
            logger.error("Error creating product", e);
            throw new RuntimeException("Error creating product", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
     }
     

    private void notifySupplier(Product product) {
         String emailTemplate = "/etc/notification/email/html/jlp/assetUploadRequestTemplate.txt";
         ResourceResolver resourceResolver = getResourceResolver();
         Resource templateRsrc = resourceResolver.getResource(emailTemplate);
         Session session = resourceResolver.adaptTo(Session.class);

         MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);

         try {
             MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
             HtmlEmail email = new HtmlEmail();
             Map<String, String> emailProperties = new HashMap<String,String>();
             String productList = "<tr>" +
                     "<td>" + product.getProdCode() + "</td>" +
                     "<td>" + product.getBrandName() + "</td>" +  
                     "<td>" + product.getStockLevel() + "</td>" +
                     "<td>" + JLWorkflowsUtils.getBuyingGrupByOffice(product.getBuyingOffice(), resourceResolver) + "</td>" +
                     "<td>" + UPLOAD_MESSAGE + "</td>" +
                     "</tr>";
             //"Folder: " + getAssetPathByProduct(product) + "<br/><br/>";
             emailProperties.put("products", productList);
             emailProperties.put("logoUrl", settings.getEmailLogoUrl());
             emailProperties.put("logoAlt", settings.getEmailLogoAlt());
             emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
             emailProperties.put("signature", settings.getEmailSignature());
             email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
             
             List<InternetAddress> recipients = new ArrayList<InternetAddress>();
             recipients.add(new InternetAddress(product.getSupplierContactDetail()));
             email.setTo(recipients);
             messageGateway.send(email);
         } catch (Exception e) {
             logger.error("Fatal error while sending email: ", e);
         } finally {
             session.logout();
             resourceResolver.close();
         }
     }

     private String createOrUpdateNodeForProduct(Node rootProductsNode, Product product, ResourceResolver resourceResolver, Session session) throws RepositoryException, PersistenceException {        
         String itemId = "";
         String status = "";
         boolean isUpdate = false;
         boolean isvalidProduct = true;
         if( !validateProduct(product)) {
             isvalidProduct = false;
             status = ProductStatus.MISSING_METADATA.toString();
             if(StringUtils.isBlank(product.getYear())) {
                 return "";
             }
         } else if(product.getStatusId() != null && !product.getStatusId().equals("")) {
             status = product.getStatusId();
         }
         
         product.setDeleted("Y".equals(product.getDeleted())?"Y":"N");
         
         String[] newProductPath = getPathForProduct(product);
         Node productNode = getOrCreatePrettyPath(rootProductsNode, newProductPath, true);
         String oldWSMItemId = null;
         if (productNode.hasProperty(ProductConstants.WSM_ITEM_ID)) {
             oldWSMItemId = productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString();
         }
        
         if (productNode.hasNode(JLWorkflowsUtils.encodePath(newProductPath[newProductPath.length-1]))) {
             isUpdate = true;
             productNode = productNode.getNode(JLWorkflowsUtils.encodePath(newProductPath[newProductPath.length-1]));
         } else {
             productNode = productNode.addNode(JLWorkflowsUtils.encodePath(newProductPath[newProductPath.length-1]), NodeTypeConstants.NT_UNSTRUCTURED);
             productNode.addNode(ProductConstants.JCR_CONTENT, NodeTypeConstants.NT_UNSTRUCTURED);
             productNode.setProperty(ProductConstants.CQ_COMMERCE_TYPE, "product");
             productNode.addMixin(ProductConstants.CQ_TAGGABLE);
             productNode.setProperty(ProductConstants.PRICE, 0);
             productNode.setProperty(ProductConstants.SLING_RESOURCE_TYPE, "commerce/components/product");
         }
         
         if(isvalidProduct && productNode.hasProperty(ProductConstants.STATUS_ID) && ProductStatus.MISSING_METADATA.toString().equals(productNode.getProperty(ProductConstants.STATUS_ID).getString())) {
             status = ProductStatus.IMAGE_REQUIRED.toString();
         }
         
         activateProductEmail(product, productNode);
         
         productNode.setProperty(ProductConstants.JCR_LAST_MODIFIED, Calendar.getInstance());
         productNode.setProperty(ProductConstants.PRODUCT_CODE, product.getProdCode());
         productNode.setProperty(ProductConstants.JCR_TITLE, product.getProdCode());
         productNode.setProperty(ProductConstants.BRAND_NAME, product.getBrandName());
         productNode.setProperty(ProductConstants.COLOUR, product.getColour());
         productNode.setProperty(ProductConstants.COLOUR_CODE, product.getColourCode());
         productNode.setProperty(ProductConstants.COLOUR_GROUP, product.getColourGroup());
         productNode.setProperty(ProductConstants.CONFIRMED_DATE, product.getConfirmedDate());
         productNode.setProperty(ProductConstants.IMAGE_SOURCE, product.getImageSource());
         productNode.setProperty(ProductConstants.MODEL_NO, product.getModelNo());
         productNode.setProperty(ProductConstants.ONLINE_STATUS, product.getOnlineStatus());
         productNode.setProperty(ProductConstants.OPTION_DESC, product.getOptionDesc());
         productNode.setProperty(ProductConstants.POS_DESC, product.getPosDesc());
         productNode.setProperty(ProductConstants.PROD_TYPE_NAME, product.getProdTypeName());
         productNode.setProperty(ProductConstants.RESEARCH_DONE, product.getResearchDone());
         productNode.setProperty(ProductConstants.SAMPLE_SOURCE, product.getSampleSource());
         productNode.setProperty(ProductConstants.SEL_DESC, product.getSelDesc());
         if(!StringUtils.isBlank(status)) {
             productNode.setProperty(ProductConstants.STATUS_ID, status);
         }
         productNode.setProperty(ProductConstants.IMAGE_REQUESTED_DATE, product.getImageRequestedDate());
         productNode.setProperty(ProductConstants.SUPPLIER_CONTACT_DETAIL, product.getSupplierContactDetail());
         productNode.setProperty(ProductConstants.TRADED_CODE, product.getTradedCode());
         if(!StringUtils.isBlank(product.getWsmItemId())) {
             productNode.setProperty(ProductConstants.WSM_ITEM_ID, product.getWsmItemId());
             itemId = product.getWsmItemId();
         }
         productNode.setProperty(ProductConstants.USER_ID_UPD, product.getUserIdUpd());
         productNode.setProperty(ProductConstants.WSM_TITLE, product.getWsmTitle());
         productNode.setProperty(ProductConstants.DIRECTORATE, product.getDirectorate());
         productNode.setProperty(ProductConstants.SEASON_CODE, product.getSeasonCode());
         productNode.setProperty(ProductConstants.YEAR, product.getYear());
         productNode.setProperty(ProductConstants.BUYING_OFFICE, product.getBuyingOffice());
         productNode.setProperty(ProductConstants.LAUNCH_DATE, product.getLaunchDate());
         productNode.setProperty(ProductConstants.DELETED, product.getDeleted());
         productNode.setProperty(ProductConstants.STOCK_LEVEL, product.getStockLevel());
         if (product.getBriefs() != null) {
             String[] briefs = product.getBriefs().toArray(new String[product.getBriefs().size()]);
             productNode.setProperty(ProductConstants.BRIEFS,  briefs);
         }
         
         logger.info("Get path prodct:"+ productNode.getPath());
         resourceResolver.commit();
         session.save();
         if (isUpdate) {
             updateRelatedAssetsMetadata(productNode, product, resourceResolver, session);
         }
         //If an item id was assigned and the product is in image completed status, then
         //it calls appoved commad in order to publish the assets in scene 7
         if (isUpdate && !itemId.equals(oldWSMItemId) && !"".equals(itemId)) {
             renameAssetsWithNewItemId(rootProductsNode, productNode, resourceResolver);
         }
         if (!itemId.equals("") && oldWSMItemId == null && status.equals(ProductStatus.IMAGE_COMPLETED.toString())){
             callAssetApprovedCommand(rootProductsNode, productNode);
         }
         return productNode.getPath();
     }

     private void renameAssetsWithNewItemId(Node rootProductsNode, Node productNode, ResourceResolver resourceResolver) {
         try {
             Node assets = JcrUtils.getOrAddNode(productNode, "assets");
             NodeIterator it = assets.getNodes();
             String wsmItemId = StringUtils.EMPTY;
             if (productNode.hasProperty(ProductConstants.WSM_ITEM_ID)) {
                 wsmItemId = productNode.getProperty(ProductConstants.WSM_ITEM_ID).getString();
             }
             while (it.hasNext() ) {
                 Node assetNodeItem = it.nextNode();
                 if (assetNodeItem.hasProperty("fileReference")) {
                     String assetPath = assetNodeItem.getProperty("fileReference").getString();
                     Resource assetResource = resourceResolver.getResource(assetPath);
                     if (assetResource != null) {
                         Node assetNode = assetResource.adaptTo(Node.class);
                         if (!productNode.hasProperty("dam:JLPProducts")) { // Ignore lifestyle assets.
                             String altCode = getAltCode(assetNode);                         
                             if ("main".equals(altCode)) {
                                 altCode = "";
                             }
                             String targetFolder = assetNode.getParent().getPath();
                             String extension = assetNode.getName().substring(assetNode.getName().lastIndexOf("."));
                             String targetFileName = JLWorkflowsUtils.addLeftZeros(wsmItemId) + altCode + extension;
                             Map<String, Object> metaData = new HashMap<>();
                             metaData.put("targetFolder", targetFolder);
                             metaData.put("targetFileName", targetFileName);
                             metaData.put("conflictResolution", "keepboth");
                             workflowInvoker.startWorkflow(MOVE_ASSET_WF, assetNode.getPath(), metaData);
                         }
                         
                     }
                     
                 }
             }
         } catch (Exception e) {
             logger.error("Can't rename assets after product update", e);
         }
     }

    /**
     * When a product is deleted or invalid, then during the update it could be activated, 
     * then we have to notify the supplier that the product is active
     * @param product (new)
     * @param productNode (old)
     * @throws RepositoryException
     * @throws PathNotFoundException
     */
    private void activateProductEmail(Product product, Node productNode)
            throws RepositoryException, PathNotFoundException {
        boolean sendEmail = false; 
        if(productNode.hasProperty(ProductConstants.DELETED) &&
            (productNode.getProperty(ProductConstants.DELETED).getString().equals("Y") &&
                    product.getDeleted().equals("N"))) {
                 sendEmail = true;
         } else
             if(productNode.hasProperty(ProductConstants.STATUS_ID.toString()) &&
                     productNode.getProperty(ProductConstants.STATUS_ID).getString().equals(ProductStatus.MISSING_METADATA.toString())
                     && ProductStatus.IMAGE_REQUESTED.toString().equals(product.getStatusId())) {
                 sendEmail = true;
             }
        if(sendEmail && settings.getEnableEmailNotification()){
            notifySupplier(product);
        }
    }

    private void callAssetApprovedCommand(Node rootProductsNode, Node productNode) throws RepositoryException {
         Node assets = JcrUtils.getOrAddNode(productNode, "assets");
         NodeIterator it = assets.getNodes();
         while (it.hasNext() ) {
             Node assetNodeItem = it.nextNode();
             AssetStatusCommand assetStatusCommand = assetStatusCommandFactory.getAssetStatusCommand(AssetStatus.FINAL_ASSET_APPROVED);
             if (assetStatusCommand != null) {
                 if (assetNodeItem.hasProperty("fileReference")) {
                     String assetPath = assetNodeItem.getProperty("fileReference").getString();
                     Node assetNode = assetNodeItem.getSession().getNode(assetPath);
                     Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                     if(metadataNode.getProperty("dam:JLPStatus").getString().equals(AssetStatus.FINAL_ASSET_APPROVED)) {
                         assetStatusCommand.execute(assetPath, "system");
                     }
                 }
             }
         }
    }

    @Override
     public String getAssetPathByProduct(Product product) {
    	 ResourceResolver resourceResolver = getResourceResolver();
         Session session = null;
    	 try {
             session = resourceResolver.adaptTo(Session.class);
             Node rootAssetsNode = JcrUtils.getOrCreateByPath(JLPConstants.SUPPLIERS_ROOT_FOLDER, "sling:Folder", session);
             String[] paths = getPathForProduct(product);
             Node node = getOrCreatePrettyPath(rootAssetsNode,paths,false);
             return node.getPath();
         } catch (Exception e) {
             logger.error("Error getting asset path", e);
         } finally {
             if (session != null) {
                 session.logout();
             }
             if (resourceResolver != null) {
                 resourceResolver.close();
             }
         }
    	 return "";
     }
    
     private String[] getPathForProduct(Product product) {
         return new String[] {product.getDirectorate(), product.getYear(), product.getBrandName(), product.getProdCode()};
     }

     private void createNodeForAssets(Node rootAssetsNode, Product product) throws RepositoryException {
         try {
             //Node supplierFolder = getOrCreateSupplierFolder(rootAssetsNode, product);
             getOrCreateBrandFolder(rootAssetsNode, product);
         } catch (Exception e) {
             logger.error("Can not create supplier folder", e);
         }
     }

     private void getOrCreateBrandFolder(Node supplierFolder, Product product) {
         try {
             Node brandedFolder = null;
             if (supplierFolder.hasNode(JLWorkflowsUtils.encodePath(product.getBrandName()))) {
                 brandedFolder = supplierFolder.getNode(JLWorkflowsUtils.encodePath(product.getBrandName())); 
             } else {
                 brandedFolder = supplierFolder.addNode(JLWorkflowsUtils.encodePath(product.getBrandName()), "sling:Folder");
                 Node jcrContent = brandedFolder.addNode("jcr:content", "nt:unstructured");
                 jcrContent.setProperty("jcr:title", product.getBrandName());
                 jcrContent.getSession().save();
                 Thread.sleep(2000);
             }
             createFolderIfNotExist(brandedFolder, "product", "Product Image");
             createFolderIfNotExist(brandedFolder, "video", "Video");
             createFolderIfNotExist(brandedFolder, "pdf", "PDFs");
             createFolderIfNotExist(brandedFolder, "logo", "Logo");
             createFolderIfNotExist(brandedFolder, "rejected", "Rejected");
         } catch (Exception e) {
             logger.error("Can not create folder for supplier", e);
         }
    }
     
    private Node createFolderIfNotExist(Node parent, String nodeName, String title) throws RepositoryException {
        if (parent.hasNode(nodeName)) {
            return parent.getNode(nodeName);
        }
        Node newFolder = parent.addNode(nodeName, "sling:Folder");
        Node jcrContent = newFolder.addNode("jcr:content", "nt:unstructured");
        jcrContent.setProperty("jcr:title", title);
        return newFolder;
    }


//    private Node getOrCreateSupplierFolder(Node rootAssetsNode, Product product) throws RepositoryException {
//        String supplierContactDetail = product.getSupplierContactDetail();
//        String supplierFolderID = JLWorkflowsUtils.formatSupplierID(supplierContactDetail);
//        Node supplierFolder = null;
//        if (rootAssetsNode.hasNode(supplierFolderID)) {
//            supplierFolder = rootAssetsNode.getNode(supplierFolderID);
//        } else {
//            supplierFolder = rootAssetsNode.addNode(supplierFolderID, "sling:Folder");
//            Node jcrContent = supplierFolder.addNode("jcr:content", "nt:unstructured");
//            jcrContent.setProperty("jcr:title", supplierContactDetail);
//        }
//        if (!supplierFolder.hasProperty("supplierID")) {
//            supplierFolder.setProperty("supplierID", supplierContactDetail);
//        }
//        return supplierFolder;
//
//    }


    private Node getOrCreatePrettyPath(Node rootNode, String[] newPath, boolean ommitLastFolder) throws RepositoryException{
         Node parent = rootNode;
         int length = newPath.length;
         if (ommitLastFolder) {
             length--;
         }
         for (int i = 0; i< length; i++) {
             if (!parent.hasNode(JLWorkflowsUtils.encodePath(newPath[i]))) {
                 parent = parent.addNode(JLWorkflowsUtils.encodePath(newPath[i]), "sling:Folder");
                 Node jcrContent = parent.addNode("jcr:content", NodeTypeConstants.NT_UNSTRUCTURED);
                 jcrContent.setProperty("jcr:title",newPath[i]);
             } else {
                 parent = parent.getNode(JLWorkflowsUtils.encodePath(newPath[i]));
             }
         }
         return parent;
     }
     
     

//     private String[] getPathForAsset(Product product) {
//         return new String[] {product.getDirectorate(), product.getYear(), product.getSeasonCode(), product.getBrandName()};
//     }
//     
//     private String[] getPathForSupplierAsset(Product product) {
//         return new String[] {product.getBrandName(), product.getYear(), product.getSeasonCode()};
//     }

     @Override
     public synchronized void updateProduct(Product product) {
         ResourceResolver resourceResolver = getResourceResolver();
         try {
             Session session = resourceResolver.adaptTo(Session.class);
             Node rootProductsNode = null;
             Node rootAssetsNode = null;
             try {
                 rootProductsNode = JcrUtils.getOrCreateByPath(rootProductsPath, "sling:Folder", session);
                 rootAssetsNode = JcrUtils.getOrCreateByPath(JLPConstants.SUPPLIERS_ROOT_FOLDER
                         , "sling:Folder", session);
                 session.save();
             } catch (RepositoryException e) {
                 logger.error("Can not create or get root paths - aborting");
                 throw new RuntimeException(e);
             }
             if ("Y".equals(product.getDeleted())) {
                 deleteProduct(resourceResolver, session, product);
             } else {
                 String productPath = calculateProductPath(product);
                 if (resourceResolver.getResource(productPath) == null) {
                     String oldPath = searchProductNode(product.getProdCode(), resourceResolver, session);
                     JcrUtils.getOrCreateByPath(productPath.replace("/" + product.getProdCode(), ""), "sling:Folder", session);
                     session.move(oldPath, productPath);
                     session.save();
                 }
                 createOrUpdateNodeForProduct(rootProductsNode, product, resourceResolver, session);
                 if ("supplierEmail".equals(product.getImageSource())) {
                     createNodeForAssets(rootAssetsNode, product);
                     session.save();
                 }
             }
         } catch (Exception e) {
             logger.error("Error uploading product", e);
             throw new RuntimeException("Error uploading product", e);
         } finally {
             if (resourceResolver != null) {
                 resourceResolver.close();
             }
         }
     }

     private void deleteProduct(ResourceResolver resourceResolver, Session session, Product product) {
         try {
             String path = searchProductNode(product.getProdCode(), resourceResolver, session);
             Resource productResource = resourceResolver.getResource(path);
             if (productResource != null) {
                 Node productNode = productResource.adaptTo(Node.class);
                 if (productNode.hasNode("assets")) {
                     NodeIterator it = productNode.getNode("assets").getNodes();
                     while (it.hasNext()) {
                         Node assetRefNode = it.nextNode();
                         String assetPath = assetRefNode.getProperty("fileReference").getString();
                         Resource assetResource = resourceResolver.getResource(assetPath);
                         if (assetResource != null) {
                             Node assetNode = assetResource.adaptTo(Node.class);
                             Node metadata = assetNode.getNode("jcr:content/metadata");
                             if (metadata.hasProperty(JLPConstants.DAM_JLP_PRODUCTS)) {
                                 removeProductFromLifestyle(path, assetNode, metadata);       
                             } else {
                                 assetNode.remove();
                             }
                             resourceResolver.commit();
                         }
                     }
                 }
                 productNode.remove();
                 resourceResolver.commit();
             }
         } catch (Exception e) {
             logger.error("Error deleting product: " + product.getProdCode(), e);
         }
     }

    private void removeProductFromLifestyle(String path, Node assetNode, Node metadata)
            throws PathNotFoundException, RepositoryException, ValueFormatException,
            VersionException, LockException, ConstraintViolationException, AccessDeniedException {
        Property propJLPProducts = metadata.getProperty(JLPConstants.DAM_JLP_PRODUCTS);
         if (propJLPProducts.isMultiple()) {
             Value[] values = propJLPProducts.getValues();
             if (values.length > 1) {
                 String[] newValues = new String[values.length-1];
                 int i = 0;
                 for (Value value : values) {
                     if (value.getString().indexOf(path)<=0) {
                         newValues[i] = value.getString();
                         i++;   
                     }
                 }
                 metadata.setProperty(JLPConstants.DAM_JLP_PRODUCTS, newValues);
             } else {
                 metadata.setProperty(JLPConstants.DAM_JLP_PRODUCTS, new String[0]);
                 // Should remove lifestyle asset associated to no longer existent product??
                 //assetNode.remove(); 
             }
         }
    }

    private String calculateProductPath(Product product) {
        StringBuilder builder = new StringBuilder();
        builder.append(rootProductsPath);
        String[] productPath = getPathForProduct(product);
        for(String folder : productPath) {
            builder.append("/");
            builder.append(JLWorkflowsUtils.encodePath(folder));
        }
        return builder.toString();
     }


    @Override
     public void sentProductEmptyFieldsEmail(String prodCode,String fieldName) {
         //if(settings.getEnableEmailNotification()) {
             try {
                 ResourceResolver resourceResolver = getResourceResolver();
                 Session session = resourceResolver.adaptTo(Session.class);
                 
                 // Getting the Email template.
                 Resource templateRsrc = resourceResolver.getResource(EMPTY_FIELDS_PRODUCT_MAIL_TEMPLATE);
                 MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
                 MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                 
                 // Creating the Email.
                 HtmlEmail email = new HtmlEmail();
                 Map<String, String> emailProperties = new HashMap<String,String>();
                 
                 emailProperties.put("prodCode", prodCode);
                 emailProperties.put("fieldName", fieldName);
                 emailProperties.put("logoUrl", settings.getEmailLogoUrl());
                 emailProperties.put("logoAlt", settings.getEmailLogoAlt());
                 emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
                 emailProperties.put("signature", settings.getEmailSignature());
                 email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                 ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
                 for (String recipient : settings.getInvalidProductRecipientList()) {
                     try {
                         emailRecipients.add(new InternetAddress(recipient));
                     } catch (AddressException e) {
                         logger.error(e.getMessage(), e);
                     }
                 }
                 email.setTo(emailRecipients);
                 
                 messageGateway.send(email);
             } catch (Exception e) {
                 logger.error("Fatal error while sending email: ", e);
                 logger.error("It was not possible to sent the invalid product email to admin");
             }
        //}
     }
     
     private void updateRelatedAssetsMetadata(Node productNode, Product product, ResourceResolver resourceResolver, Session session) throws RepositoryException {
         
         Node assets = JcrUtils.getOrAddNode(productNode, "assets");
         
         NodeIterator it = assets.getNodes();
         while (it.hasNext() ) {
             Node assetNodeItem = it.nextNode();
             if (assetNodeItem.hasProperty("fileReference")) {
                 try {
                     String assetPath = assetNodeItem.getProperty("fileReference").getString();
                     Resource assetResource = resourceResolver.getResource(assetPath);
                     if (assetResource != null) {
                         Node assetNode = assetResource.adaptTo(Node.class);
                         JLWorkflowsUtils.setAssetPropertiesFromProduct(assetNode,productNode,resourceResolver);
                     } else {
                         assetNodeItem.remove();
                     }
                     resourceResolver.commit();
                     session.save();
                 } catch (Exception e) {
                     logger.error("Can't update asset node with product info", e);
                 }
             }
         }
         
     }

     @Override
     public void deleteProduct(Product product) {
         // TODO Auto-generated method stub
         
     }

     @Override
     public List<CompletedImage> getProductCompletedImages(final Date fromDate) {
         //TODO CHANGE LOGIC OF COMPLETED ASSETS 
         // Search assets instead of products
         // Search assets where available or completed asset are > fromDate
         // For each asset which status changed in the given range, look for productReference 
         // and build CompletedImage object following existent rules (similar)
         List<CompletedImage> completedImages = new ArrayList<>();
         ResourceResolver resourceResolver = getResourceResolver();
         long lowerBound = System.currentTimeMillis() - (365 * 24 * 60 * 60 * 1000);
         
         try {
             Resource resource = resourceResolver.getResource(JLPConstants.CREATIVE_ASSETS_ROOT_FOLDER);
             Node rootNode = resource.adaptTo(Node.class);
             if (fromDate != null) {
                 lowerBound = fromDate.getTime();
             } else {
                 if (rootNode.hasProperty(JLPConstants.IMAGE_COMPLETE_LAST_QUERY_UPDATE)) {
                     Calendar lastQuery = rootNode.getProperty(JLPConstants.IMAGE_COMPLETE_LAST_QUERY_UPDATE).getDate();
                     lowerBound = lastQuery.getTimeInMillis();
                 }
                 
             }
             Calendar newQueryDate = Calendar.getInstance();
             Session session = resourceResolver.adaptTo(Session.class);
             
             StringBuilder s = new StringBuilder();
             s.append("select * from [dam:Asset] as a");
             s.append(" where isdescendantnode(a, '/content/dam/Creative')");
             s.append(" and ([jcr:content/metadata/dateAvailable] >= " + lowerBound);
             s.append(" OR [jcr:content/metadata/dateCompleted] >= " + lowerBound + ")");
             
             QueryManager queryManager = session.getWorkspace().getQueryManager();
             Query query = queryManager.createQuery(s.toString(), javax.jcr.query.Query.JCR_SQL2);
             QueryResult result = query.execute();
             NodeIterator iterator = result.getNodes();
             
             Calendar lowerBoundCalendar = Calendar.getInstance();
             lowerBoundCalendar.setTimeInMillis(lowerBound);
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss:SSS z");
             
             while (iterator.hasNext()) {
                 Node hitNode = iterator.nextNode();
                 Node metadataNode = JcrUtils.getNodeIfExists(hitNode, "jcr:content/metadata");
                 String prodCode = metadataNode.getProperty(JLPConstants.JLP_PRODUCT_CODES).getString();
                 String imageCat = metadataNode.getProperty(JLPConstants.JLP_IMAGE_CATEGORY).getString();
                 
                 
                 if (metadataNode.hasProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_TIME)) {
                     Calendar availableTime = metadataNode.getProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_TIME).getDate();
                     if (availableTime.after(lowerBoundCalendar)) {
                         String prevStatus = ProductStatus.IMAGE_REQUESTED.getDisplayLabel();
                         String newStatus =  ProductStatus.IMAGE_AVAILABLE.getDisplayLabel();
                         String updatedTime = sdf.format(availableTime.getTime());
                         String updatedBy = metadataNode.getProperty(JLPConstants.ASSET_STATUS_CHANGE_AVAILABLE_USER).getString();
                         completedImages.add(new CompletedImage(prodCode, prevStatus, newStatus, updatedTime, updatedBy,imageCat));
                     }
                 }
                 
                 if (metadataNode.hasProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_TIME)) {
                     Calendar availableTime = metadataNode.getProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_TIME).getDate();
                     if (availableTime.after(lowerBoundCalendar)) {
                         String prevStatus = ProductStatus.IMAGE_AVAILABLE.getDisplayLabel();
                         String newStatus =  ProductStatus.IMAGE_COMPLETED.getDisplayLabel();
                         String updatedTime = sdf.format(availableTime.getTime());
                         String updatedBy = metadataNode.getProperty(JLPConstants.ASSET_STATUS_CHANGE_COMPLETED_USER).getString();
                         completedImages.add(new CompletedImage(prodCode, prevStatus, newStatus, updatedTime, updatedBy,imageCat));
                     }
                 }
             }
             
             rootNode.setProperty(JLPConstants.IMAGE_COMPLETE_LAST_QUERY_UPDATE, newQueryDate);
             resourceResolver.commit();
         } catch (Exception e) {
             logger.error("Eror collecting updated products", e);
         } finally {
             resourceResolver.close();
         }
         
         return completedImages;
     }

     @Override
     public String importProducts(String path) {
         logger.info("Importing products from: "+ path);
         ResourceResolver resourceResolver = getResourceResolver();
         Resource resource = resourceResolver.getResource(path);
         Asset csvAsset = resource.adaptTo(Asset.class);
         Session session = resourceResolver.adaptTo(Session.class);
         InputStream in = csvAsset.getOriginal().adaptTo(InputStream.class);
         try {
             InputStreamReader isr = new InputStreamReader(in,"UTF-8");
             BufferedReader br = new BufferedReader(isr);
             logger.info("PRODUCT encoding: "+isr.getEncoding());
             logger.info("Trying to parse CSV: "+ path);
             
             String currentLine = null;
             long lineNumber=1;
             List<String> errors = new ArrayList<>();
             if ((currentLine = br.readLine()) != null) {
                 ArrayList<String> indexes = getArraylistFromLine(currentLine);  
                 while ((currentLine = br.readLine()) != null) {
                     Product p = new Product();
                     try {
                         ArrayList<String> currentLineArray = getArraylistFromLine(currentLine);
                         String prodCode = getPropertyByIndex(currentLineArray, indexes, "prodCode");
                         if(prodCode != null) {
                             String prodPath = searchProductNode(prodCode,resourceResolver, session);
                             if(prodPath != null &&  !"".equals(prodPath)){
                                 Node productNode = session.getNode(prodPath);
                                 p = JLWorkflowsUtils.populateProductFromNode(productNode);
                             }
                         }
                         p.setProdCode(getPropertyByIndex(currentLineArray, indexes, "prodCode"));
                         
                         if(indexes.contains("brandName")){
                             p.setBrandName(getPropertyByIndex(currentLineArray, indexes, "brandName"));
                         }
                         if(indexes.contains("colour")){
                             p.setColour(getPropertyByIndex(currentLineArray, indexes, "colour"));
                         }
                         if(indexes.contains("colourCode")){
                             p.setColourCode(getPropertyByIndex(currentLineArray, indexes, "colourCode"));
                         }
                         if(indexes.contains("colourGroup")){
                             p.setColourGroup(getPropertyByIndex(currentLineArray, indexes, "colourGroup"));
                         }
                         if(indexes.contains("confirmedDate")){
                             p.setConfirmedDate(getPropertyByIndex(currentLineArray, indexes, "confirmedDate"));
                         }
                         if(indexes.contains("imageSource")){
                             p.setImageSource(getPropertyByIndex(currentLineArray, indexes, "imageSource"));
                         }
                         String modelNo = getPropertyByIndex(currentLineArray, indexes, "modelNo");
                         if(indexes.contains("modelNo")){
                             p.setModelNo(modelNo);
                         }
                         String onlineStatus = getPropertyByIndex(currentLineArray, indexes, "onlineStatus");
                         if(indexes.contains("onlineStatus")){
                             p.setOnlineStatus(onlineStatus);
                         }
                         if(indexes.contains("optionDesc")){
                             p.setOptionDesc(getPropertyByIndex(currentLineArray, indexes, "optionDesc"));
                         }
                         if(indexes.contains("posDesc")){
                             p.setPosDesc(getPropertyByIndex(currentLineArray, indexes, "posDesc"));
                         }
                         if(indexes.contains("prodTypeName")){
                             p.setProdTypeName(getPropertyByIndex(currentLineArray, indexes, "prodTypeName"));
                         }
                         if(indexes.contains("researchDone")){
                             p.setResearchDone(getPropertyByIndex(currentLineArray, indexes, "researchDone"));
                         }
                         if(indexes.contains("sampleSource")){
                             p.setSampleSource(getPropertyByIndex(currentLineArray, indexes, "sampleSource"));
                         }
                         if(indexes.contains("selDesc")){
                             p.setSelDesc(getPropertyByIndex(currentLineArray, indexes, "selDesc"));
                         }
                         if(indexes.contains("statusId")){
                             p.setStatusId(getPropertyByIndex(currentLineArray, indexes, "statusId"));
                         }
                         if(indexes.contains("deleted")){
                             p.setDeleted(getPropertyByIndex(currentLineArray, indexes, "deleted"));
                         }
                         if(indexes.contains("supplierContactDetail")){
                         p.setSupplierContactDetail(getPropertyByIndex(currentLineArray, indexes, "supplierContactDetail"));
                         }
                         if(indexes.contains("tradedCode")){
                             p.setTradedCode(getPropertyByIndex(currentLineArray, indexes, "tradedCode"));
                         }
                         if(indexes.contains("wsmItemId")){
                             p.setWsmItemId(getPropertyByIndex(currentLineArray, indexes, "wsmItemId"));
                         }
                         if(indexes.contains("userIdUpd")){
                             p.setUserIdUpd(getPropertyByIndex(currentLineArray, indexes, "userIdUpd"));
                         }
                         if(indexes.contains("wsmTitle")){
                             p.setWsmTitle(getPropertyByIndex(currentLineArray, indexes, "wsmTitle"));
                         }
                         if(indexes.contains("directorate")){
                             p.setDirectorate(getPropertyByIndex(currentLineArray, indexes, "directorate"));
                         }
                         if(indexes.contains("seasonCode")){
                             p.setSeasonCode(getPropertyByIndex(currentLineArray, indexes, "seasonCode"));
                         }
                         if(indexes.contains("year")){
                             p.setYear(getPropertyByIndex(currentLineArray, indexes, "year"));
                         }
                         if(indexes.contains("buyingOffice")){
                             p.setBuyingOffice(getPropertyByIndex(currentLineArray, indexes, "buyingOffice"));
                         }
                         if(indexes.contains("launchDate")){
                             p.setLaunchDate(getPropertyByIndex(currentLineArray, indexes, "launchDate"));
                         }
                         if(indexes.contains("stockLevel")){
                             p.setStockLevel(getPropertyByIndex(currentLineArray, indexes, "stockLevel"));
                         }
                         if(indexes.contains("web_stk_qty")){
                             p.setStockLevel(getPropertyByIndex(currentLineArray, indexes, "web_stk_qty"));
                         }
                         addProduct(p);
                         lineNumber++;
                     } catch (Exception e) {
                         errors.add(lineNumber + " - " + e.getCause());
                         e.printStackTrace();
                         logger.error("Error importing product " + p.getProdCode()!= null? "code: " + p.getProdCode(): "on Row: "+lineNumber);
                     }
                 }
             }
             logger.info("Finish import");
             br.close();
             StringBuilder builder = new StringBuilder("Imported : " + (lineNumber - errors.size()) + " products\n");
             builder.append("Errors: \n");
             for (String error : errors) {
                 builder.append(error).append("\n");
             }
             return  builder.toString();
         } catch (Exception e) {
             logger.error("Fatal error importing products",e);
             return "Fatal Error - aborting: " + e.getCause(); 
         } finally {
             if (resourceResolver != null) {
                 try {
                    resourceResolver.commit();
                    resourceResolver.close();
                } catch (PersistenceException e) {
                    logger.error("Fatal error closing session",e);
                }
             }
         }
     }

     private String getPropertyByIndex(ArrayList<String> currentLineArray, ArrayList<String> indexes, String propertyName) {
         for( int i=0; i< indexes.size(); i++){
             if(indexes.get(i).toString().equals(propertyName.toString())){
                 return currentLineArray.get(i);
             }
         }
         return "";
     }

     private ArrayList<String> getArraylistFromLine (String line) {
         if(line.charAt(line.length()-1) == (',')){
             line+= " ";
         }
         if(line.startsWith("\"")){
             line = line.substring(1);
         }
         line = line.replace("\"\"", "#DOUBLEQUOTE#");
         line = replaceComma(line,"\"");
         line = line.replace("\"", "");
         
         line = line.replace(",", "#SEPARATOR#");
         line = line.replace("#COMMA#",",");
         line = line.replace("#DOUBLEQUOTE#","\"");
         return new ArrayList<String>(Arrays.asList(line.split("#SEPARATOR#")));
     }

     private String replaceComma(String line, String replaceString) {
         if(line.contains(replaceString)){
             String[] tokens = line.split(replaceString);
             line = "";
             for (int i = 0; i<tokens.length; i++)  {
                 String token = tokens[i];
                 if((i%2)!=0){
                     token = token.replace(",","#COMMA#");
                 }
                 line += token;
             } 
         }
         return line;
     }

     private ResourceResolver getResourceResolver() {
         ResourceResolver resourceResolver;
         try {       
             Map<String, Object> param = new HashMap<String, Object>();
             param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
             resourceResolver = resolverFactory.getServiceResourceResolver(param);

         } catch (Exception e) {
             logger.error("Can not login system user and get resource resolver " + e.getMessage());
             return null;
         }
        
         return resourceResolver;
     }

     @Override
     public boolean exist(String prodCode) {
         ResourceResolver resourceResolver = getResourceResolver();
         if (resourceResolver != null) {
             Session session = resourceResolver.adaptTo(Session.class);
             try {
                 if (searchProductNode(prodCode, resourceResolver, session) != null) {
                     return true;
                 }
             } catch (Exception e) {
                 logger.error("Can't search for product " + prodCode, e);
             } finally {
                 session.logout();
                 resourceResolver.close();
             }
         }

         return false;
     }
     
     @Override
     public Product getProduct(String prodCode) {
         ResourceResolver resourceResolver = getResourceResolver();
         if (resourceResolver != null) {
             Session session = resourceResolver.adaptTo(Session.class);
             try {
                 String productPath = searchProductNode(prodCode, resourceResolver, session);
                 if (productPath != null) {
                     Node productNode = JcrUtils.getNodeIfExists(productPath, session);
                     return JLWorkflowsUtils.populateProductFromNode(productNode);
                 }
             } catch (Exception e) {
                 logger.error("Can't search for product " + prodCode, e);
             } finally {
                 session.logout();
                 resourceResolver.close();
             }
         }

         return null;
     }
     

     
     private String searchProductNode(String productCode, final ResourceResolver resourceResolver, final Session session) {
         try {
             QueryManager queryManager = session.getWorkspace().getQueryManager();
             javax.jcr.query.Query query = queryManager.createQuery("SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(s,'/etc/commerce/products/onejl') AND (CONTAINS(s.identifier, '"+productCode+"') OR s.tradedCode = '"+productCode+"')" , javax.jcr.query.Query.JCR_SQL2);
             QueryResult result = query.execute();
             if (result.getNodes().hasNext()) {
                 return result.getNodes().nextNode().getPath();
             }
         } catch (Exception e) {
           logger.error("Error searching product node", e);  
         }
         return null;
     }
   
    
     private boolean validateProduct(Product product) {
         boolean isValid = true;
         String invalidFields = "";
         removeEmailExtraData(product);
         if ("supplierEmail".equals(product.getImageSource()) && !validateEmailAddress(product.getSupplierContactDetail())){
             invalidFields = ProductConstants.SUPPLIER_CONTACT_DETAIL.toString();
             isValid = false;
         }
         
         if (StringUtils.isBlank(product.getYear())) {
             String year = ProductConstants.YEAR.toString();
             invalidFields += invalidFields.equals("")? year: ", " + year ;
             isValid = false;
         }
         
         if(!isValid) {
             sentInvalidProductCreatedEmail(product.getProdCode(), invalidFields);
         }
         
         return isValid;
     }

     private void sentInvalidProductCreatedEmail(String prodCode, String fieldName) {
         if(settings.getEnableEmailNotification()) {
             try {
                 ResourceResolver resourceResolver = getResourceResolver();
                 Session session = resourceResolver.adaptTo(Session.class);
                 
                 // Getting the Email template.
                 Resource templateRsrc = resourceResolver.getResource(INVALID_PRODUCT_MAIL_TEMPLATE);
                 MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
                 MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                 
                 // Creating the Email.
                 HtmlEmail email = new HtmlEmail();
                 Map<String, String> emailProperties = new HashMap<String,String>();
                 
                 emailProperties.put("logoUrl", settings.getEmailLogoUrl());
                 emailProperties.put("logoAlt", settings.getEmailLogoAlt());
                 emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
                 emailProperties.put("signature", settings.getEmailSignature());
                 emailProperties.put("prodCode", prodCode);
                 emailProperties.put("fieldName", fieldName);
                 email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                 ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
                 for (String recipient : settings.getInvalidProductRecipientList()) {
                     try {
                         emailRecipients.add(new InternetAddress(recipient));
                     } catch (AddressException e) {
                         logger.error(e.getMessage(), e);
                     }
                 }
                 email.setTo(emailRecipients);
                 
                 messageGateway.send(email);
             } catch (Exception e) {
                 logger.error("Fatal error while sending email: ", e);
                 logger.error("It was not possible to sent the invalid product email to admin");
             }
         }
    }

    private boolean validateEmailAddress(String emailStr) {
         Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
         return matcher.find();
     }

     private void removeEmailExtraData(Product product) {
         String mail = product.getSupplierContactDetail();
         if (mail.contains("<") && mail.contains(">")) {
             product.setSupplierContactDetail(mail.substring(mail.indexOf("<") + 1, mail.lastIndexOf(">")));
         } else if (mail.contains("<")){
             product.setSupplierContactDetail(mail.substring(mail.indexOf("<") + 1));
         }
     }
}
