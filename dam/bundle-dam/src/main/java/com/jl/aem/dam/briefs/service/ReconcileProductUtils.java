package com.jl.aem.dam.briefs.service;

import java.util.LinkedList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.JLPConstants;

public class ReconcileProductUtils {

    private static final Logger log = LoggerFactory.getLogger(ReconcileProductUtils.class);

    public static String getProductBriefToReconcile(String productCode,
            ResourceResolver resourceResolver) {
        try {
            if (StringUtils.isNotBlank(productCode)) {
                SearchResult results = searchReconcileProductNodes(resourceResolver);

                if (results.getTotalMatches() > 0) {
                    List<Hit> hits = results.getHits();
                    for (Hit hit : hits) {
                        Node node = hit.getNode();
                        if (productCode.equals(node.getName())) {
                            return node.getPath();
                        }
                    }

                }
            }
        } catch (RepositoryException e) {
            log.error("Error getting number of alts from guide syle", e);

        }
        return null;
    }


    private static SearchResult searchReconcileProductNodes(ResourceResolver resourceResolver) {
        Session session = resourceResolver.adaptTo(Session.class);

        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", JLPConstants.PROJECTS_DAM_PATH);

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "sling:Folder");

        Predicate productTypePredicate = new Predicate("reconcileProduct", "property");
        productTypePredicate.set(JcrPropertyPredicateEvaluator.PROPERTY,
                JLPConstants.JLP_RECONCILE_PRODUCT);
        productTypePredicate.set(JcrPropertyPredicateEvaluator.OPERATION,
                JcrPropertyPredicateEvaluator.OP_EXISTS);


        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.add(productTypePredicate);
        predicates.setAllRequired(true);

        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(predicates, session);
        query.setHitsPerPage(0);

        return query.getResult();
    }

    public static List<String> getAssetPathsToReconcile(String briefProdPath,
            ResourceResolver resourceResolver) {
        log.debug("*** briefProdPath = {}", briefProdPath);
        List<String> assetPaths = new LinkedList<String>();
        try {

            if (StringUtils.isNotBlank(briefProdPath)) {

                Node briefProdNode =
                        resourceResolver.getResource(briefProdPath).adaptTo(Node.class);
                NodeIterator briefProdNodeIter = briefProdNode.getNodes();
                while (briefProdNodeIter.hasNext()) {
                    Node altNode = (Node) briefProdNodeIter.next();
                    if (altNode.getPrimaryNodeType().getName().equals("sling:Folder")) {
                        NodeIterator altNodeiterator = altNode.getNodes();
                        while (altNodeiterator.hasNext()) {
                            Node asNode = (Node) altNodeiterator.next();
                            if (asNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                                assetPaths.add(asNode.getPath());
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            log.error("Error getting number of alts from guide syle", e);

        }
        return assetPaths;
    }
    
    public static List<String> getThirdPartyAssetPathsToReconcile(String briefProdPath,
            ResourceResolver resourceResolver) {
        log.debug("*** briefProdPath = {}", briefProdPath);
        List<String> assetPaths = new LinkedList<String>();
        try {

            if (StringUtils.isNotBlank(briefProdPath)) {

                Node briefProdNode =
                        resourceResolver.getResource(briefProdPath).adaptTo(Node.class);
                
                String prodID = briefProdNode.getName();
                String briefName = briefProdNode.getParent().getName();
                
                Resource thirdPartyFolder = resourceResolver.getResource(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH);
                if (thirdPartyFolder != null) {
                    Node thirdPartyFolderNode =  thirdPartyFolder.adaptTo(Node.class);
                    NodeIterator thirdParties = thirdPartyFolderNode.getNodes();
                    while (thirdParties.hasNext()) {
                        Node thirdParty = thirdParties.nextNode();
                        if (thirdParty.hasNode(JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER)) {
                            Node pendingRetouch = thirdParty.getNode(JLPConstants.THIRD_PARTY_PENDING_RETOUCH_FOLDER);
                            collectThirdPartyAssets(assetPaths, prodID, briefName, pendingRetouch);

                        }
                        if (thirdParty.hasNode(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER)) {
                            Node retouched = thirdParty.getNode(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER);
                            collectThirdPartyAssets(assetPaths, prodID, briefName, retouched);
                        }
                    }
                }
                
            }
        } catch (RepositoryException e) {
            log.error("Error getting number of alts from guide syle", e);

        }
        return assetPaths;
    }


    private static void collectThirdPartyAssets(List<String> assetPaths, String prodID,
            String briefName, Node collectableFolder) throws RepositoryException {
        if (collectableFolder != null) {
            NodeIterator pendingBriefsIt = collectableFolder.getNodes();
            while (pendingBriefsIt.hasNext()) {
                Node pendingBriefNode = pendingBriefsIt.nextNode();
                if (pendingBriefNode.getName().equals(briefName)) {
                    NodeIterator pendingProdIt = pendingBriefNode.getNodes();
                    while (pendingProdIt.hasNext()) {
                        Node pendingProdNode = pendingProdIt.nextNode();
                        if (pendingProdNode.getName().equals(prodID)) {
                            
                            NodeIterator altNodeIt = pendingProdNode.getNodes();
                            while (altNodeIt.hasNext()) {
                                Node altNode = altNodeIt.nextNode();
                                if (altNode.getPrimaryNodeType().getName().equals("sling:Folder")) {
                                    NodeIterator assetIt = altNode.getNodes();
                                    while (assetIt.hasNext()) {
                                        Node asNode = assetIt.nextNode();
                                        if (asNode.getPrimaryNodeType().getName().equals("dam:Asset")) {
                                            assetPaths.add(asNode.getPath());
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
}
