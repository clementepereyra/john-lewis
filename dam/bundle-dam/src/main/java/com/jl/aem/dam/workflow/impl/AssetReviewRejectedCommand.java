package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.replication.Replicator;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.WorkflowBusinessSettings;

@Component(immediate=true, service=AssetReviewRejectedCommand.class)
public class AssetReviewRejectedCommand implements AssetStatusCommand {
    
    private static final Logger logger = LoggerFactory.getLogger(AssetReviewRejectedCommand.class);

    private static final String EMAIL_TEMPLATE = "/etc/notification/email/html/jlp/assetReviewerRejectedTemplate.txt";

    @Reference
    private Replicator replicator;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private MessageGatewayService messageGatewayService;
    
    @Reference
    private JLPWorkflowSettings settings;
    
    @Reference
    private WorkflowBusinessSettings businessSettings;
    
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Override
    public void execute(String assetPath, final String userID) {
 
        logger.info("AssetReviewRejectedCommand executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
            metadataNode.setProperty(JLPConstants.ASSET_REJECTED_AUTO,false);
            metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_REJECTED_TIME,Calendar.getInstance());
            int valilationFailsCount = 1;
            String imageCat = metadataNode.getProperty(JLPConstants.JLP_IMAGE_CATEGORY).getString().replace("jlp:image-category/alt/", "_").replace("jlp:image-category/", "_");
            if(metadataNode.hasProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS + imageCat)){
                valilationFailsCount = Integer.parseInt(metadataNode.getProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS + imageCat).getString()) + 1;
            }
            metadataNode.setProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS, valilationFailsCount);
            resourceResolver.commit();
            
            String targetFolder = JLPConstants.ARTWORKERS_PENDING_RETOUCH;
            
            String productPath = assetNode.getNode("jcr:content/metadata").getProperty("cq:productReference").getString();
            Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            Product product = JLWorkflowsUtils.populateProductFromNode(productNode);
            if (businessSettings.is34Ratio(product)) {
                targetFolder += "/3_4";
            } else {
                targetFolder += "/square";
            }
            
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetFolder);
            metaData.put("targetFileName", assetNode.getName());
            
            workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
            
            if(settings.getEnableEmailNotification()) {
                notifyArtworker(assetNode, resourceResolver);
            }
            
        } catch (Exception e){
            logger.error("Can not reject the asset: " + assetPath, e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
    
    private void notifyArtworker(final Node assetNode, final ResourceResolver resourceResolver) {
        Resource templateRsrc = resourceResolver.getResource(EMAIL_TEMPLATE);
        Session session = resourceResolver.adaptTo(Session.class);
        MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);

        try {
            MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
            // Creating the Email.
            HtmlEmail email = new HtmlEmail();
            
            Map<String, String> emailProperties = new HashMap<String,String>();
            Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
            emailProperties.put("reviewerComment", metadataNode.getProperty(JLPConstants.JLP_COMMENT).getString());
            emailProperties.put("assetPath", assetNode.getPath());
            emailProperties.put("logoUrl", settings.getEmailLogoUrl());
            emailProperties.put("logoAlt", settings.getEmailLogoAlt());
            emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
            emailProperties.put("signature", settings.getEmailSignature());
            
            email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
            ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
            for (String recipient : settings.getInvalidProductRecipientList()) {
                try {
                    emailRecipients.add(new InternetAddress(recipient));
                } catch (AddressException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            email.setTo(emailRecipients);
            messageGateway.send(email);
        } catch (Exception e) {
            logger.error("Fatal error while sending email: ", e);
        } finally {
            session.logout();
        }
    }
}
