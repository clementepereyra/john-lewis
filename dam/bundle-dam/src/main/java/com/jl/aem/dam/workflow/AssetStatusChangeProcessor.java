package com.jl.aem.dam.workflow;

import com.day.cq.dam.api.DamEvent;

public interface AssetStatusChangeProcessor {
    
    boolean shouldProcess(final DamEvent damEvent);

    void process(final DamEvent damEvent, final String assetPath);
}
