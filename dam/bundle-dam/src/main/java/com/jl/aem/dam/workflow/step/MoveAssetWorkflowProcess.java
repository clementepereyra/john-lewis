package com.jl.aem.dam.workflow.step;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.Workspace;
import javax.jcr.version.VersionManager;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.audit.AuditLog;
import com.day.cq.audit.AuditLogEntry;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;

@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Move Asset Workflow Process"})
public class MoveAssetWorkflowProcess extends AbstractDAMWorkflowProcess {
   
    private static final Logger log = LoggerFactory.getLogger(MoveAssetWorkflowProcess.class);
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    @Reference
    private JLPWorkflowSettings workflowSettings;
    
    @Reference 
    private AuditLog auditLog;
    
    @Override
    public void execute(WorkItem item, WorkflowSession wfSession, MetaDataMap args)
            throws WorkflowException {
        final Asset asset = getAssetFromPayload(item, wfSession.getSession());
        Session session = wfSession.getSession();
        ResourceResolver resourceResolver = getResourceResolver(session);
        String oldPath = asset.getPath();
        try {
            // First move the asset
            String targetFolder= item.getWorkflowData().getMetaDataMap().get("targetFolder").toString();
            String targetFileName = item.getWorkflowData().getMetaDataMap().get("targetFileName").toString();
            String targetFullPath = targetFolder + (targetFolder.endsWith("/")?"":"/") + targetFileName;
            boolean rename = false;
            if (oldPath.startsWith(targetFolder)) {
                rename = true;
            }
            if (!targetFullPath.equals(asset.getPath())) {

                getOrCreateFolder(session, targetFolder);
                String conflictResolution = item.getWorkflowData().getMetaDataMap().get("conflictResolution", String.class);
                int index = 0;
                if ("keepboth".equals(conflictResolution)) {
                    while (session.nodeExists(targetFullPath)) {
                        targetFullPath = targetFullPath.substring(0, targetFullPath.lastIndexOf(".")) + "_" + index + targetFullPath.substring(targetFullPath.lastIndexOf("."));
                        index++;
                    }
                } else if ("replace".equals(conflictResolution)) {
                   if (session.nodeExists(targetFullPath)) {
                       session.removeItem(targetFullPath);
                       session.save();
                   } 
                }
                com.adobe.granite.asset.api.AssetManager assetManager = resourceResolver.adaptTo(com.adobe.granite.asset.api.AssetManager.class);
                String mode = item.getWorkflowData().getMetaDataMap().get("mode", String.class);
                if ("copy".equals(mode)) {
                    Workspace workspace = session.getWorkspace();
                    workspace.copy(oldPath, targetFullPath);
                    assetManager.copyAsset(oldPath, targetFullPath);
                    if (asset.getRendition(JLPConstants.RENDITION_FLATTEN) != null) {
                        asset.removeRendition(JLPConstants.RENDITION_FLATTEN);
                    }
                    if (asset.getRendition(JLPConstants.RENDITION_LAYERED) != null) {
                        asset.removeRendition(JLPConstants.RENDITION_LAYERED);
                    }
                    if (asset.getRendition(JLPConstants.RENDITION_ORIGINAL) != null) {
                        asset.removeRendition(JLPConstants.RENDITION_ORIGINAL);
                    }
                } else {
                    assetManager.moveAsset(oldPath, targetFullPath);
                    //session.move(oldPath, targetFullPath);
                }
                session.save();

                
                switchToFlattened(targetFullPath, oldPath, resourceResolver);

                updateProductReferencesToAsset(session, resourceResolver, oldPath, targetFullPath);
                
                updateTimelineEvents(session, resourceResolver, oldPath, targetFolder, targetFullPath, mode);
                if (!"copy".equals(mode)) {
                    createAuditLog(targetFullPath, rename, session);
                }
            }
            
                // Finally check if we have to set a new status
            Node assetNode = resourceResolver.getResource(targetFullPath).adaptTo(Node.class);

            Object nextStatusObj = item.getWorkflowData().getMetaDataMap().get("nextStatus");
            if (nextStatusObj != null) {
                String nextStatus = nextStatusObj.toString();
                assetNode.getNode("jcr:content/metadata").setProperty(JLPConstants.JLP_STATUS, nextStatus);
                session.save();
            }
            
            Object nextWorkflowObj = item.getWorkflowData().getMetaDataMap().get("nextWorkflow");
            if (nextWorkflowObj != null) {
                String nextWorkflow = nextWorkflowObj.toString();
                workflowInvoker.startWorkflow(nextWorkflow, targetFullPath, new HashMap<String,Object>());
            }
              
        } catch (Exception e) {
            log.error("Error moving asset " + item.getWorkflowData().getPayload().toString(), e);
        }
    }

    private static String ASSET_AUDITLOG_PATH = "com/day/cq/dam";
    private void createAuditLog(String targetFullPath, boolean isRename, Session session) {
        AuditLogEntry ae = new AuditLogEntry(ASSET_AUDITLOG_PATH,
                new Date(),
                session.getUserID(),
                targetFullPath,
                isRename?"Asset Renamed":"Asset Moved",
                null);           
        auditLog.add(ae);
    }

    private void getOrCreateFolder(Session session, String targetFolder)
            throws RepositoryException {
        if (targetFolder.startsWith("/content/dam/")
                && (targetFolder.indexOf(JLPConstants.ARTWORKERS_PENDING_RETOUCH)>=0
                    || targetFolder.indexOf(JLPConstants.ARTWORKERS_RETOUCHED)>=0
                    || targetFolder.indexOf(JLPConstants.SUPPLIERS_ROOT_FOLDER)>=0)) {
            String[] folders = targetFolder.substring("/content/dam/".length()).split("/");
            Node currentNode = session.getNode("/content/dam");
            for (String folder : folders) {
                if (currentNode.hasNode(folder)) {
                    currentNode = currentNode.getNode(folder);
                } else {
                    currentNode = currentNode.addNode(folder, "sling:Folder");
                    session.save();
                    try { 
                        Thread.sleep(workflowSettings.getCommitTimeout());
                    } catch (Exception e) {
                        //do nothing
                    }
                }
            }
        } else {
           JcrUtils.getOrCreateByPath(targetFolder, "sling:Folder", session);
           session.save();
        }
    }

    public final static String AUDIT_PAGES_ROOT_PATH = "/var/audit/com.day.cq.wcm.core.page";
    public final static String AUDIT_DAM_ROOT_PATH = "/var/audit/com.day.cq.dam";
    
    private void updateTimelineEvents(Session session, ResourceResolver resourceResolver,
            String oldPath, String targetFolder, String targetFullPath, String mode) {
        moveAuditPath(AUDIT_PAGES_ROOT_PATH, session, resourceResolver, oldPath, targetFolder, targetFullPath, mode);
        moveAuditPath(AUDIT_DAM_ROOT_PATH, session, resourceResolver, oldPath, targetFolder, targetFullPath, mode);

    }

    private void moveAuditPath(String auditRootPath, Session session,
            ResourceResolver resourceResolver, String oldPath, String targetFolder,
            String targetFullPath, String mode) {
        boolean moved = false;
        int attempts = 0;
        while (!moved && attempts <5) {
            try {
                String auditOldPath = auditRootPath + oldPath;
                Resource auditOldResource = resourceResolver.getResource(auditOldPath);
                if (auditOldResource != null) {
                    String targetFolderNewAuditPath= auditRootPath + targetFolder;
                    Resource targetFolderNewAuditPathResource = resourceResolver.getResource(targetFolderNewAuditPath);
                    if (targetFolderNewAuditPathResource == null) {
                        getOrCreateFolder(session, targetFolderNewAuditPath);
                    }
                    String targetAssetNewAuditFullPath = auditRootPath + targetFullPath;
                    if (session.nodeExists(targetAssetNewAuditFullPath)) {
                        session.removeItem(targetAssetNewAuditFullPath);
                        session.save();
                    }
                    if ("copy".equals(mode)) {
                        session.getWorkspace().copy(auditOldPath, targetAssetNewAuditFullPath);
                    } else {
                        session.move(auditOldPath, targetAssetNewAuditFullPath);
                    }
                    session.save();
                }
                moved = true;
            } catch (Exception e) {
                log.error("Can't move audit records " + auditRootPath + oldPath, e);
                attempts++;
                try {
                    Thread.sleep(1000);
                    session.refresh(false);
                } catch (Exception e1) {
                    log.error("Can't refresh session " + auditRootPath + oldPath, e);
                }
            }
        } 
    }

    private void updateProductReferencesToAsset(Session session, ResourceResolver resourceResolver,
            String oldPath, String targetFullPath) throws Exception {

        Node assetNode = resourceResolver.getResource(targetFullPath).adaptTo(Node.class);
        if (assetNode.getNode("jcr:content/metadata").hasProperty("cq:productReference")) {
            String productPath = assetNode.getProperty("jcr:content/metadata/cq:productReference").getString();
            Node productNode = resourceResolver.getResource(productPath).adaptTo(Node.class);
            Node prodAssetsNode = productNode.getNode("assets");
            NodeIterator nodeIt = prodAssetsNode.getNodes();
            while (nodeIt.hasNext()) {
                Node prodChildAssetNode = nodeIt.nextNode();
                if (prodChildAssetNode.getProperty("fileReference").getString().equals(oldPath)) {
                   prodChildAssetNode.setProperty("fileReference", targetFullPath);
                }
            }
            session.save();
        }
        
        if (assetNode.getNode("jcr:content/metadata")
                .hasProperty(JLPLifeStyleConstants.JLP_PRODUCTS)) {
            List<String> productsPath = new ArrayList<String>();
            Property productsProperty = assetNode
                    .getProperty("jcr:content/metadata/" + JLPLifeStyleConstants.JLP_PRODUCTS);
            if (productsProperty.isMultiple()) {
                for (Value value : productsProperty.getValues()) {
                    productsPath.add(value.getString());
                }
            } else {
                productsPath.add(productsProperty.getString());
            }
   
            for (String productPath : productsPath) {
                Node productNode =
                        resourceResolver.getResource(productPath).adaptTo(Node.class);
                Node prodAssetsNode = productNode.getNode("assets");
                NodeIterator nodeIt = prodAssetsNode.getNodes();
                while (nodeIt.hasNext()) {
                    Node prodChildAssetNode = nodeIt.nextNode();
                    if (prodChildAssetNode.getProperty("fileReference").getString()
                            .equals(oldPath)) {
                        prodChildAssetNode.setProperty("fileReference", targetFullPath);
                    }
                }
            }
            session.save();
        }
        resourceResolver.commit();
    }
    
    private void switchToFlattened(String targetFullPath, String oldPath, ResourceResolver resourceResolver) throws RepositoryException, PersistenceException {
        if (targetFullPath.indexOf(JLPConstants.JOHNLEWIS_SCENE7_FOLDER) >= 0 
                && oldPath.indexOf("/content/dam/projects") >= 0
                && (oldPath.indexOf(".tif") >=0 || oldPath.indexOf(".tiff") >=0) ) {
            Asset finalAsset = resourceResolver.getResource(targetFullPath).adaptTo(Asset.class);
            Rendition flatten = finalAsset.getRendition(JLPConstants.RENDITION_FLATTEN);
            if (flatten != null) {
                Session session = resourceResolver.adaptTo(Session.class);
                
                Node n = session.getNode(targetFullPath);
                if (n.canAddMixin("mix:versionable")) {
                    n.addMixin("mix:versionable");
                    session.save();
                }
                
                VersionManager versionManager = session.getWorkspace().getVersionManager();
                versionManager.checkpoint(targetFullPath);
                session.save();
                Rendition layered = finalAsset.getOriginal();
                finalAsset.addRendition(JLPConstants.RENDITION_LAYERED, layered.getStream(), layered.getMimeType());
                finalAsset.addRendition(JLPConstants.RENDITION_ORIGINAL, flatten.getStream(), flatten.getMimeType());
                resourceResolver.commit();
                session.save();
                
            }
        }
    }

    @Override
    protected ResourceResolverFactory getResolverFactory() {
        return resolverFactory;
    }

}
