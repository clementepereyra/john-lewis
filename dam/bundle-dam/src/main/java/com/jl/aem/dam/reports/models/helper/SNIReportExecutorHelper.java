package com.jl.aem.dam.reports.models.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.reports.api.ResultsPage;
import com.jl.aem.dam.reports.models.results.JLSNIItem;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

public class SNIReportExecutorHelper implements JLReportExecutorHelper {
    private static final Logger log = LoggerFactory.getLogger(SNIReportExecutorHelper.class);

    private Map<String, String> parameters  = new HashMap<String, String>();
    
    private static final String START_DATE_LOWER_BOUND_PARAM = "startDateLowerBound";
    private static final String START_DATE_UPPER_BOUND_PARAM = "startDateUpperBound";
    private static final String DUE_DATE_LOWER_BOUND_PARAM = "dueDateLowerBound";
    private static final String DUE_DATE_UPPER_BOUND_PARAM = "dueDateUpperBound";
    
    @Override
    public ResultsPage fetchResults(SlingHttpServletRequest request) {
        List<Object> results = new ArrayList<>();
        try {
            prepareParameters(request);
            
            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            Map<String, List<Node>> briefs = new HashMap<>();
            briefs.put("cutout", new ArrayList<>());
            briefs.put("lifestyle", new ArrayList<>());
            briefs.put("retouch", new ArrayList<>());
            
            StringBuilder queryString = new StringBuilder("SELECT s.* "
                    + " FROM [nt:unstructured] AS s "
                    + " WHERE ISDESCENDANTNODE(s, [/content/projects]) "
                    + " AND s.[sling:resourceType]='cq/gui/components/projects/admin/card/projectcard'"
                    + " AND s.[jcr:content/jcr:title] LIKE '%_SNI_%'"
                    ); 
            if (parameters.get(START_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(START_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(START_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(START_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.startDate]<=" + upperBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_LOWER_BOUND_PARAM) != null) {
                Date lowerBound = parseDate(parameters.get(DUE_DATE_LOWER_BOUND_PARAM));
                if (lowerBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]>=" + lowerBound.getTime());
                }
            }
            if (parameters.get(DUE_DATE_UPPER_BOUND_PARAM) != null) {
                Date upperBound = parseDate(parameters.get(DUE_DATE_UPPER_BOUND_PARAM));
                if (upperBound != null) {
                    queryString.append(" AND s.[jcr:content/project.dueDate]<=" + upperBound.getTime());
                }
            }
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            javax.jcr.query.Query query = queryManager.createQuery(queryString.toString(), javax.jcr.query.Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator iterator = result.getNodes();
            while (iterator.hasNext()) {
                Node brief = iterator.nextNode();
                Node jcrContent = brief.getNode("jcr:content");
                int countProducts = getProductCount(brief, resourceResolver);
                int countAssets =  getAssetCountInBrief(brief, resourceResolver);

                JLSNIItem item = new JLSNIItem();
                item.setBriefName(jcrContent.getProperty("jcr:title").getString());
                item.setBriefPath(brief.getPath());
                item.setCountAssets(Integer.toString(countAssets));
                item.setCountProducts(Integer.toString(countProducts));
                
                if (jcrContent.hasProperty("project.dueDate")){
                    String dueDate = jcrContent.getProperty("project.dueDate").getString().substring(0,10);
                    item.setDueDate(dueDate);
                }
                if (jcrContent.hasProperty("project.shootstartDate")){
                    String endShootDate = jcrContent.getProperty("project.shootstartDate").getString().substring(0,10);
                    item.setEndShootDate(endShootDate);
                }
                if (jcrContent.hasProperty("project.shootendDate")){
                    String startShootDate = jcrContent.getProperty("project.shootendDate").getString().substring(0,10);
                    item.setStartShootDate(startShootDate);
                }
                results.add(item);
            }
            
        } catch (Exception e) {
            log.error("Can't create inflight report", e);
        }
        ResultsPage page = new ResultsPage(results, results.size(), 0);
        return page;
    }
    
    private int getAssetCountInBrief(Node brief, ResourceResolver resourceResolver) {
        int count = 0;
        try {
            Node damNode = resourceResolver.getResource(brief.getNode("jcr:content").getProperty("damFolderPath").getString()).adaptTo(Node.class);
            NodeIterator it = damNode.getNodes();
            while (it.hasNext()) {
                Node productNode = it.nextNode();
                if (productNode.hasProperty("stockNumber")
                      || productNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)
                        ) {
                    count+=getAssetCountInProduct(productNode);
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return count;
    }
    
    private int getAssetCountInProduct(Node product) {
        int count = 0;
        try {
            NodeIterator it = product.getNodes();
            while (it.hasNext()) {
                Node altNode = it.nextNode();
                if (altNode.hasProperty(JLPConstants.JLP_BRIEF_ASSET_STATUS)) {
                    count++;
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return count;
    }

    private int getProductCount(Node brief, ResourceResolver resourceResolver) {
        int count = 0;
        try {
            Node damNode = resourceResolver.getResource(brief.getNode("jcr:content").getProperty("damFolderPath").getString()).adaptTo(Node.class);
            NodeIterator it = damNode.getNodes();
            while (it.hasNext()) {
                Node productNode = it.nextNode();
                if (productNode.hasProperty("stockNumber")
                      || productNode.hasProperty(JLPLifeStyleConstants.NAMING_CONVENTION)
                        ) {
                    count++;
                }
            }
        } catch (Exception e) {
            log.error("Can't retrieve items in breif for report", e);
        }
        return count;
    }

    private void prepareParameters(SlingHttpServletRequest request) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public String getParameters() {
        // TODO Auto-generated method stub
        return null;
    }
    
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Date parseDate(String stringDate) {
        Date date = null;
        String sanitized = stringDate;
        if (stringDate.length() >= 10) {
            sanitized = stringDate.substring(0, 10);
            try { 
                date = sdf.parse(sanitized);
            } catch (Exception e) {
                log.error("Error parsing date", e);
            }
        }
        return date;
    }
    
}
