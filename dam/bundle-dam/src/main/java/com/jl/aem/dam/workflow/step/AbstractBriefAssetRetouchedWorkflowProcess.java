package com.jl.aem.dam.workflow.step;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

public abstract class AbstractBriefAssetRetouchedWorkflowProcess
        extends AbstractProjectWorkflowProcess {

    protected static final String DAM_ASSET = "dam:Asset";

    protected static final String JCR_CONTENT = "jcr:content";

    protected static final String SLING_FOLDER = "sling:Folder";
    protected static final String SLING_ORDERED_FOLDER = "sling:OrderedFolder";

    protected static final String SLASH = "/";

    protected void removeAssetNodeFromFolder(ResourceResolver resourceResolver, Session session,
            String folderNodePath, String assetName) throws RepositoryException, VersionException, LockException,
            ConstraintViolationException, AccessDeniedException, ItemExistsException,
            ReferentialIntegrityException, InvalidItemStateException, NoSuchNodeTypeException {
        Resource folderResource = resourceResolver.getResource(folderNodePath);
        if (folderResource != null) {
            Node folderNode = folderResource.adaptTo(Node.class);
            NodeIterator nodIt = folderNode.getNodes();
            while (nodIt.hasNext()) {
                Node assetNode = (Node) nodIt.next();
                if (assetNode.getPrimaryNodeType().getName().equals(DAM_ASSET) && assetNode.getName().indexOf(assetName.substring(0, assetName.indexOf(".")))>=0) {
                    session.removeItem(assetNode.getPath());
                    session.save();
                }
            }
        }
    }

}
