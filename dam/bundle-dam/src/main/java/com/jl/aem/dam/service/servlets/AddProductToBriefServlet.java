package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.briefs.service.BriefsService;

@Component(service = {Servlet.class},
property = {"sling.servlet.paths=/bin/jlp/public/AddProductToBrief",
        "sling.servlet.methods=POST"})
public class AddProductToBriefServlet  extends SlingAllMethodsServlet {
    
    
    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private BriefsService briefService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        String projectPath = request.getParameter("project_path");
        String productCode = request.getParameter("product_code");
        String category = request.getParameter("category");
        String imageRequirement = request.getParameter("image_requirements");
        String stylingGuide = request.getParameter("styling_guide");
        String range = request.getParameter("range");
        String ids = request.getParameter("ids");
        String clientNotes = request.getParameter("client_notes");
        
        String result = briefService.addProductToCutoutBrief(projectPath, productCode, category, imageRequirement, stylingGuide, range, ids, clientNotes);
        response.getWriter().write("{\"result\":\""+result+"\"}");
        response.setContentType("application/json");
    }
    

}
