package com.jl.aem.dam.workflow.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.AssetStatusDailyNotificationService;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;


@Component(immediate=true, name="Asset Status Daily Notification Service", service=AssetStatusDailyNotificationService.class)
public class AssetStatusDailyNotificationServiceImpl implements AssetStatusDailyNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(AssetStatusDailyNotificationServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private JLPWorkflowSettings settings;
    
    @Override
    public void execute() {
        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
                logger.error("Can not login system user");
                throw new RuntimeException(e);
        }
        try {
            SearchResult result = searchAssets(resourceResolver, session);
            if (result.getTotalMatches() > 0) {
                
                String csvFileName = "required_action_assets_"+ System.currentTimeMillis() + ".csv";
                File tempFile = null;
                InputStream tempFiles = null;
                FileOutputStream tempout = null;
                tempFile = File.createTempFile(csvFileName, null);
                tempout = new FileOutputStream(tempFile);
                final String commaSeparator = ",";
                tempout.write("AssetPath,Status,LastModified,CurrentOwner\n".getBytes());   

                List<Hit> hits = result.getHits();
                
                for (Hit hit : hits) {
                    try {
                        
                        Node node = hit.getNode();
                        if (node.hasNode("jcr:content/metadata") 
                                && node.getNode("jcr:content/metadata").hasProperty(JLPConstants.JLP_STATUS)
                            && node.getNode("jcr:content/metadata").hasProperty(JLPConstants.JLP_ASIGNEE)) {
                            StringBuilder csvLine = new StringBuilder();
                            csvLine.append(node.getPath());
                            csvLine.append(commaSeparator);
                            csvLine.append(node.getNode("jcr:content/metadata")
                                    .getProperty(JLPConstants.JLP_STATUS).getString());
                            csvLine.append(commaSeparator);
                            csvLine.append(node.getNode("jcr:content")
                                    .getProperty(JLPConstants.JCR_LAST_MODIFIED).getString());
                            csvLine.append(commaSeparator);
                            csvLine.append(node.getNode("jcr:content/metadata")
                                    .getProperty(JLPConstants.JLP_ASIGNEE).getString());
                            
                            csvLine.append("\n");
                            tempout.write(csvLine.toString().getBytes());
                        }
                        
                    } catch (RepositoryException e) {
                        logger.warn("Can not process asset " + hit.getPath() + " - " + e.getMessage());
                    }
                }
                
                tempout.flush();
                tempFiles = new FileInputStream(tempFile);
                if(settings.getEnableEmailNotification()){
                    // Getting the Email template.
                    String emailTemplate = "/etc/notification/email/html/jlp/needsActionTemplate.txt";
                    Resource templateRsrc = resourceResolver.getResource(emailTemplate);
    
                    MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
    
                    try {
                        MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
                        // Creating the Email.
                        HtmlEmail email = new HtmlEmail();
                        Map<String, String> emailProperties = new HashMap<String,String>();
                        emailProperties.put("logoUrl", settings.getEmailLogoUrl());
                        emailProperties.put("logoAlt", settings.getEmailLogoAlt());
                        emailProperties.put("logoHeight", Integer.toString(settings.getEmailLogoHeight()));
                        emailProperties.put("signature", settings.getEmailSignature());
                        email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);
                        ByteArrayDataSource fileDS = new ByteArrayDataSource(tempFiles, "text/csv");
                        email.attach(fileDS, "text/csv", "This is your attached file.");
                        email.setTo(getEmailRecipients(settings.getAssetStatusRecipientList()));
                        messageGateway.send(email);
                    } catch (Exception e) {
                        logger.error("Fatal error while sending email: ", e);
                    }
                }
                tempout.close();
                tempFiles.close();

            }
        } catch (Exception e) {
            logger.error("Fatal error while creating daily assets require action report: ", e);
        } finally {
            session.logout();
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        } 
    }
    
    
    private SearchResult searchAssets(ResourceResolver resourceResolver, Session session) {
        PredicateGroup predicates = new PredicateGroup();

        Predicate pathPredicate = new Predicate("path", "path");
        pathPredicate.set("path", settings.getObservedPath());

        Predicate typePredicate = new Predicate("type", "type");
        typePredicate.set("type", "dam:Asset");
 
        int i = 1;
        Predicate statusPredicate = new Predicate("status", "n_value");
        statusPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "jcr:content/metadata/" + JLPConstants.JLP_STATUS);
        for (String status :  settings.getMonitoredStatus()) {
            statusPredicate.set( i + "_" + JcrPropertyPredicateEvaluator.VALUE, status);
            i++;
        }
        statusPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EQUALS);
        predicates.add(statusPredicate);
        
       
        predicates.add(pathPredicate);
        predicates.add(typePredicate);
        predicates.setAllRequired(true);
                
        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                predicates, session);
        query.setHitsPerPage(0);
        
        return query.getResult();
    }

    public List<InternetAddress> getEmailRecipients(String[] recipients) {

        ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
        for (String recipient : recipients) {
            try {
                emailRecipients.add(new InternetAddress(recipient));
            } catch (AddressException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return emailRecipients;
    }
    
    
  

}