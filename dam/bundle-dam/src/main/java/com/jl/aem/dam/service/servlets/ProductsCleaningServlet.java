package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.jl.aem.dam.briefs.service.BriefUtils;
import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLPWorkflowSettings;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.ProductConstants;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

@Component(service = {Servlet.class},
        property = {"sling.servlet.paths=/bin/jlp/public/productsCleaning",
                "sling.servlet.methods=GET"})
public class ProductsCleaningServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private ResourceResolverFactory resolverFactory;
    private static final Logger logger = LoggerFactory.getLogger(ProductsCleaningServlet.class);

    @Reference
    private JLPWorkflowSettings workflowSettings;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {

        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            if("cleanProducts".equals(request.getParameter("action"))) {
                String productsListPath = request.getParameter("productsListPath");
                if (productsListPath != null) {
                    cleanProducts(productsListPath, response, resourceResolver);
                } else {
                    response.getWriter().println("Wrong Request: Specify valid path for productsListPath.");
                }
            } else if("logProducts".equals(request.getParameter("action"))) {
                logProducts(response, resourceResolver);
            } else if("associateBriefs".equals(request.getParameter("action"))) {
                associateBriefs(response, resourceResolver);
            }else {
                response.getWriter().println("Wrong Request: Specify action.");
            }
        } catch (Exception e) {
            logger.error("Internal error logging/cleaning products: ", e);
            response.getWriter().println("Internal error logging/cleaning products." + e.getCause());
            e.printStackTrace(response.getWriter());
        } finally {
            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }


    }
    
    
    private void cleanProducts(String productsListPath, SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        String processID = java.util.UUID.randomUUID().toString();
        try {
            logger.info("Starting clean process: " + processID);
            response.getWriter().println("Products Clean process" + processID);
            response.getWriter().println("");
            Resource resource = resourceResolver.getResource(productsListPath);
            if (resource != null) {
                Asset csvAsset = resource.adaptTo(Asset.class);
                InputStream in = csvAsset.getOriginal().adaptTo(InputStream.class);
                final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
                final CSVReader reader = new CSVReaderBuilder(new InputStreamReader(in, "UTF-8"))
                        .withSkipLines(1).withCSVParser(parser).build();
                List<String[]> myEntries = reader.readAll();
                List<String> toDelete = new ArrayList<>();
                for (String[] rowContent : myEntries) {
                    toDelete.add(rowContent[0]);
                }
                reader.close();
                deleteAll(toDelete, resourceResolver, response, processID);
            } else {
                response.getWriter().println("Products Clean process" + processID + " - File with products to be deleted can't be found at " + productsListPath);
            }
        } catch (Exception e) {
            logger.error("Products Clean process" + processID + " - Can't clean products" , e);
        }
    }


    private void deleteAll(List<String> toDelete, ResourceResolver resourceResolver,
            SlingHttpServletResponse response, String processID) {
        try {
            Resource resource = resourceResolver.getResource(JLPConstants.PRODUCTS_ROOT_FOLDER);
            Node node = resource.adaptTo(Node.class);
            NodeIterator directorateIt = node.getNodes();
            boolean excludeServletLog = false;
            int origAmount = toDelete.size();
            if (origAmount>5000) {
                excludeServletLog = true;
            }
            while (directorateIt.hasNext()) {
    
                Node directorate = directorateIt.nextNode();
                NodeIterator yearIt = directorate.getNodes();
                while (yearIt.hasNext()) {
                    Node year = yearIt.nextNode();
                    NodeIterator brandsIt = year.getNodes();
                    while (brandsIt.hasNext()) {
                        Node brand = brandsIt.nextNode();
                        NodeIterator productIt = brand.getNodes();
                        while (productIt.hasNext()) {
                            Node product = productIt.nextNode();
                            logger.debug("Products Clean process" + processID + "- Produt Analyzed: Product Code: " + product.getName() + " - " + " Product Path: " + product.getPath());
                            if (product.hasProperty(ProductConstants.BRAND_NAME)) {
                                String code = product.getName();
                                try {
                                    if (toDelete.contains(code)) {
                                        product.remove();
                                        resourceResolver.commit();
                                        if (!excludeServletLog) {
                                            response.getWriter().println("Deleted product: " + code);
                                        }
                                        logger.info("Products Clean process" + processID + " - Deleted product: " + code);
                                        toDelete.remove(code);
                                    }
                                } catch (Exception e) {
                                    response.getWriter().println("Can't delete product: " + code + " (" + e.getMessage() + ")" );
                                    logger.error("Products Clean process" + processID +" - Can't delete product " + code);
                                }
                            }
                        }
                    }
                }
            }
            if (!excludeServletLog) {
              for (String code : toDelete) {
                  response.getWriter().println("Product Not Found in AEM: " + code);
              }
            }
            response.getWriter().println("Total deleted products:" + ( origAmount - toDelete.size()));
            response.getWriter().println("Total products not found:" + (toDelete.size()) );

        } catch (Exception e) {
            logger.error("Can't clean products", e);
        }
        
    }


    private void logProducts(SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        CSVWriter writer = null;
        try {
            response.setContentType("text/csv");

            // Make sure to show the download dialog
            response.setHeader("Content-disposition","attachment; filename=export.csv");

            OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), 
                    StandardCharsets.UTF_8);
            writer = new CSVWriter(osw);
            String[] entries = { "Product Code", "Product Path", "Is Deleted", "Modified" };
            writer.writeNext(entries);
            Resource resource = resourceResolver.getResource(JLPConstants.PRODUCTS_ROOT_FOLDER);
            Node node = resource.adaptTo(Node.class);
            NodeIterator directorateIt = node.getNodes();
            while (directorateIt.hasNext()) {

                Node directorate = directorateIt.nextNode();
                NodeIterator yearIt = directorate.getNodes();
                while (yearIt.hasNext()) {
                    Node year = yearIt.nextNode();
                    NodeIterator brandsIt = year.getNodes();
                    while (brandsIt.hasNext()) {
                        Node brand = brandsIt.nextNode();
                        NodeIterator productIt = brand.getNodes();
                        while (productIt.hasNext()) {
                            Node product = productIt.nextNode();
                            logger.debug("Product Code: " + product.getName() + " - " + " Product Path: " + product.getPath());
                            if (product.hasProperty(ProductConstants.BRAND_NAME)) {
                                String path = product.getPath();
                                String code = product.getName();
                                String modified = product.getProperty("jcr:lastModified").getString();
                                String isDeleted = "N";
                                if (product.hasProperty("deleted")) {
                                    isDeleted = product.getProperty("deleted").getString();
                                }
 
                                
                                entries = new String[]{ code, path, isDeleted, modified }; 
                                writer.writeNext(entries);
                                 
                                
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Can not setup brands", e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    logger.error("Can't close file", e);
                }
            }
            try {
                response.getWriter().flush();
            } catch (Exception e) {
                logger.error("Can't close file", e);
            }
        }
    }
    
    private void associateBriefs(SlingHttpServletResponse response,
            ResourceResolver resourceResolver) {
        String processID = java.util.UUID.randomUUID().toString();
        try {
            logger.info("Starting brief association process: " + processID);
            response.getWriter().println("Brief Association Process" + processID);
            response.getWriter().println("");
            Resource resource = resourceResolver.getResource(JLPConstants.PROJECTS_DAM_PATH);
            if (resource != null) {
                Node rootNode = resource.adaptTo(Node.class);
                NodeIterator rootIt = rootNode.getNodes();
                while (rootIt.hasNext()) {
                    Node yearNode = rootIt.nextNode();
                    if (yearNode.hasProperty("projectPath")) {
                        associateProject(yearNode, resourceResolver, response);
                    } else {
                        NodeIterator quarterIt = yearNode.getNodes();
                        while (quarterIt.hasNext()) {
                            Node quarterNode = quarterIt.nextNode();
                            NodeIterator weekIt = quarterNode.getNodes();
                            while (weekIt.hasNext()) {
                                Node weekNode = weekIt.nextNode();
                                NodeIterator projectIt = weekNode.getNodes();
                                while (projectIt.hasNext()) {
                                    Node projectNode = projectIt.nextNode();
                                    if (projectNode.hasProperty("projectPath")) {
                                        associateProject(projectNode, resourceResolver, response);
                                    }
                                            
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Brief Association Process" + processID + " - Can't associate briefs" , e);
        }
    }


    private void associateProject(Node projectNode, ResourceResolver resourceResolver, SlingHttpServletResponse response) {
        try {
            String projectPath = null;
            Property prop = projectNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
            if (prop.isMultiple()) {
                projectPath = prop.getValues()[0].getString();
            } else {
                projectPath = prop.getString();
            }
            
            NodeIterator productIt = projectNode.getNodes();
            response.getWriter().print("Associating project " + projectPath + " to products:");
            while (productIt.hasNext()) {
                Node productProjectNode = productIt.nextNode();
                if (productProjectNode.hasProperty("productPath")) {
                    String productPath = productProjectNode.getProperty("productPath").getString();
                    Resource productResource = resourceResolver.getResource(productPath);
                    if (productResource != null) {
                        try {
                            Node productNode = productResource.adaptTo(Node.class);
                            Product product = JLWorkflowsUtils.populateProductFromNode(productNode);
                            if (!product.getBriefs().contains(projectPath)) {
                                response.getWriter().print(" " + product.getProdCode());
                                product.getBriefs().add(projectPath);
                                String[] briefs = product.getBriefs().toArray(new String[product.getBriefs().size()]);
                                productNode.setProperty(ProductConstants.BRIEFS,  briefs);
                                resourceResolver.commit();
                            }
                        } catch (Exception e1) {
                            logger.error("Can't associate product " + productProjectNode.getPath() + " to brief " + projectPath, e1 );
                        }
                    }
                }
                
            }
            response.getWriter().println("");
        } catch (Exception e) {
            logger.error("Can't associate project", e);
        }
        
    }
}
