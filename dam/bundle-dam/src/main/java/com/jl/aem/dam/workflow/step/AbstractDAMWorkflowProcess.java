package com.jl.aem.dam.workflow.step;



import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;

public abstract class AbstractDAMWorkflowProcess implements WorkflowProcess{

    /**
     * Logger instance for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractDAMWorkflowProcess.class);
    public static final String TYPE_JCR_PATH = "JCR_PATH";
    
    protected Asset getAssetFromPayload(final WorkItem item, final Session session) {
    
        Asset asset = null;
    
        if (item.getWorkflowData().getPayloadType().equals(TYPE_JCR_PATH)) {
            final String path = item.getWorkflowData().getPayload().toString();
            final Resource resource = getResourceResolver(session).getResource(path);
            if (null != resource) {
                asset = DamUtil.resolveToAsset(resource);
            } else {
                log.error("getAssetFromPaylod: asset [{}] in payload of workflow [{}] does not exist.", path,
                        item.getWorkflow().getId());
            }
        }
        return asset;
    }
    
    protected Node getAssetNodeFromPayload(final WorkItem item, final Session session) {     
        Node asset = null;
    
        if (item.getWorkflowData().getPayloadType().equals(TYPE_JCR_PATH)) {
            final String path = item.getWorkflowData().getPayload().toString();
            final Resource resource = getResourceResolver(session).getResource(path);
            if (null != resource) {
                asset = resource.adaptTo(Node.class);
            } else {
                log.error("getAssetNodeFromPayload: node [{}] in payload of workflow [{}] does not exist.", path,
                        item.getWorkflow().getId());
            }
        }
        return asset;
    }

    protected ResourceResolver getResourceResolver(Session session) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("user.jcr.session", session); 
            resourceResolver = getResolverFactory().getResourceResolver(param);
            return resourceResolver;
        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
    }

    protected abstract ResourceResolverFactory getResolverFactory();

}
