package com.jl.aem.dam.workflow.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.AssetStatusCommandFactory;


@Component(immediate=true, service=AssetStatusCommandFactory.class)
public class AssetStatusCommandFactoryImpl implements AssetStatusCommandFactory {

    @Reference
    AssetStatusGenericCommand genericCommand;
    
    @Reference
    UploadedAssetValidationCommand validationCommand;
    
    @Reference
    RejectedAssetCommand rejectedAssetCommand;
    
    @Reference
    AssetApprovedCommand assetApprovedCommand;
    
    @Reference
    AssetPendingArtworkCommand assetPendingArtworkCommand;
    
    @Reference
    AssetReviewRejectedCommand assetReviewRejectedCommand;
    
    @Reference
    AssetPendingICCommand assetPendingICCommand;
    
    @Reference
    AssetSubmittedFinalApprovalCommand assetSubmitedFinalApprovalCommand;
    
    @Reference
    AssetAvailableForArtworkCommand assetReadyForArtworkCommand;
    
    @Reference
    AssetArtworkRejectCommand assetArtworkRejectCommand;
    
    @Override
    public AssetStatusCommand getAssetStatusCommand(final AssetStatus status) {
        if (status ==  AssetStatus.ASSET_NEW) {
            return validationCommand;
        } else if (status ==  AssetStatus.ASSET_RE_REQUESTED) {
            return rejectedAssetCommand;
        } else if (status ==  AssetStatus.FINAL_ASSET_APPROVED) {
            return assetApprovedCommand;
        } else if (status ==  AssetStatus.ASSET_PENDING_ARTWORKING) {
            return assetPendingArtworkCommand;
        } else if (status ==  AssetStatus.FINAL_ASSET_REJECTED) {
            return assetReviewRejectedCommand;
        } else if (status == AssetStatus.ASSET_PENDING_IC) {
          return assetPendingICCommand;  
        }  else if (status ==  AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL) {
            return assetSubmitedFinalApprovalCommand;
        } else if (status ==  AssetStatus.ASSET_AVAILABLE_FOR_ARTWORKING) {
            return assetReadyForArtworkCommand;
        } else if (status ==  AssetStatus.ARTWORK_REJECTED) {
            return assetArtworkRejectCommand;
        }
        return genericCommand;
    }

}
