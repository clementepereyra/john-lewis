package com.jl.aem.dam.workflow.validation;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
@Component (immediate=true, service = JLPAssetValidatorFactory.class)
public class JLPAssetValidatorFactory {
    @Reference
    private JLPSquareValidator jlsquareValidator;
    
    @Reference
    private JLP34Validator jl34Validator;

    @Reference
    private JLPVideoValidator jlVideoValidator;
    
    @Reference
    private JLPManualValidator jlManualValidator;
    
    @Reference
    private JLPLogoValidator jlLogoValidator;
    
    @Reference
    private JLPSupplierImageValidator jlSupplierValidator;
    
    private List<JLPAssetValidator> assetValidators;
    
    public JLPAssetValidator getAssetValidator(final String assetPath) {
        for (JLPAssetValidator validator : assetValidators) {
            if (validator.canValidate(assetPath)) {
                return validator;
            }
        }
        return null;
    }
    
    @Activate
    @Modified
    public void activate() {
        assetValidators = new ArrayList<JLPAssetValidator>();
        //assetValidators.add(jlsquareValidator);
        //assetValidators.add(jl34Validator);
        assetValidators.add(jlSupplierValidator);
        assetValidators.add(jlVideoValidator);
        assetValidators.add(jlManualValidator);
        assetValidators.add(jlLogoValidator);
    }

    public JLPAssetValidator getArtworkerValidator(Node originalAssetNode) throws RepositoryException {
        if (originalAssetNode.getPath().indexOf("/square/") >= 0) {
            return jlsquareValidator;
        }
        return jl34Validator;
    }
}
