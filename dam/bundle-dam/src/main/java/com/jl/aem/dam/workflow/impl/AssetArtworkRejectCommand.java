package com.jl.aem.dam.workflow.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Replicator;
import com.jl.aem.dam.workflow.AssetStatusCommand;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate=true, service=AssetArtworkRejectCommand.class)
public class AssetArtworkRejectCommand implements AssetStatusCommand {
    
    private static final Logger logger = LoggerFactory.getLogger(AssetArtworkRejectCommand.class);

    @Reference
    private Replicator replicator;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;
    
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Override
    public void execute(String assetPath, final String userID) {
 
        logger.info("Artworker reject executing for asset path: " + assetPath);
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            Node assetNode = resourceResolver.getResource(assetPath).adaptTo(Node.class);
            try {
                Map<String, Object> metaData = new HashMap<>();
                String targetFolder = JLPConstants.ARTWORKERS_RETOUCHED + "/Rejected";
                String targetFileName = assetNode.getName();
                metaData.put("targetFolder", targetFolder);
                int i = 0;
                while (resourceResolver.getResource(targetFolder + "/" + targetFileName) != null) {
                    targetFileName = assetNode.getName().replace(".", "_" + i + ".");
                    i++;
                }
                
                metaData.put("targetFileName", targetFileName);
                workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, assetNode.getPath(), metaData);
            } catch (Exception e) {
                logger.error("Problem while trying to move asset to review folder.", e);
            }
        } catch (Exception e){
            logger.error("Can not publish new asset: " + assetPath, e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
    }
}
