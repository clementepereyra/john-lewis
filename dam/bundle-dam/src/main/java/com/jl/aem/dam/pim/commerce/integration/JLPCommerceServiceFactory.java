package com.jl.aem.dam.pim.commerce.integration;


import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;

import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.CommerceServiceFactory;
import com.adobe.cq.commerce.common.AbstractJcrCommerceServiceFactory;


/**
 * A simple new implementation for the {@link CommerceServiceFactory} interface.
 */
@Component(immediate = true, name = "Adobe CQ Commerce Factory for JLP DAM", service=CommerceServiceFactory.class,
property={"service.descriptionservice.description=Factory for JLP DAM commerce service","commerceProvider=onejl"})
 public class JLPCommerceServiceFactory 
extends AbstractJcrCommerceServiceFactory implements CommerceServiceFactory {
 
    /**
     * Create a new <code>CommerceServiceImpl</code>.
     */
    public CommerceService getCommerceService(Resource res) {
        return new JLPCommerceServiceImpl(getServiceContext(), res);
    }
 
}