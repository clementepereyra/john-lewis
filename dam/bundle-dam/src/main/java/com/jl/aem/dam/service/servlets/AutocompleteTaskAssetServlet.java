package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.briefs.service.AltTaskCompleterService;
import com.jl.aem.dam.briefs.service.AssetTaskCompleterService;
import com.jl.aem.dam.briefs.service.ProductTaskCompleterService;
import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;
import com.jl.aem.dam.workflow.JLPLifeStyleConstants;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/AutocompleteTaskAsset",
    "sling.servlet.methods=POST"
  }
)
public class AutocompleteTaskAssetServlet extends SlingAllMethodsServlet {
    
    private static final Logger log = LoggerFactory.getLogger(AutocompleteTaskAssetServlet.class);

    private static final long serialVersionUID = 2598426539166789515L;

    @Reference
    private AssetTaskCompleterService assetCompleterService;

    @Reference
    private AltTaskCompleterService altCompleterService;
    
    @Reference
    private ProductTaskCompleterService productCompleterService;
    
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        try {
            
            String comments = request.getParameter("comment")!=null?request.getParameter("comment"):"";
            String assetStatus = request.getParameter("status");
            AutocompleteStatus newStatus = null;
            if (assetStatus != null) {
                newStatus = AutocompleteStatus.valueOf(assetStatus);
            }

            RequestParameter[] resourcePaths = request.getRequestParameters("path");
            

            if (resourcePaths != null) {
                boolean validation = true;
                //List<String> assetPaths = getAssets(resourcePaths, request.getResourceResolver());
                ResourceResolver resourceResolver = getResourceResolver();
                try { 
                    for (RequestParameter resourcePath : resourcePaths) {
                        String path = resourcePath.toString();
                        if (isAsset(path, resourceResolver)) {
                            if (!assetCompleterService.canComplete(path, newStatus)) {
                                validation = false;
                                break;
                            }
                        } else if (isAlt(path, resourceResolver)) {
                            if (!altCompleterService.canComplete(path, newStatus)) {
                                validation = false;
                                break;
                            }
                        } else if (isProduct(path, resourceResolver) || isLook(path, resourceResolver)) {
                            if (!productCompleterService.canComplete(path, newStatus)) {
                                validation = false;
                                break;
                            }
                        } else if (isProject(path, resourceResolver)) {
                            List<String> productPaths = getProductPaths(path, resourceResolver);
                            for (String productPath : productPaths) {
                                if (!productCompleterService.canComplete(productPath, newStatus)) {
                                    validation = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (validation) {
                        for (RequestParameter resourcePath : resourcePaths) {
                            String path = resourcePath.toString();
                            if (isAsset(path, resourceResolver)) {
                                assetCompleterService.complete(path, newStatus, comments);
                            } else if (isAlt(path, resourceResolver)) {
                                altCompleterService.complete(path, newStatus, comments);
                            } else if (isProduct(path, resourceResolver) || isLook(path, resourceResolver)) {
                                productCompleterService.complete(path, newStatus, comments);
                            } else if (isProject(path, resourceResolver)) {
                                List<String> productPaths = getProductPaths(path, resourceResolver);
                                for (String productPath : productPaths) {
                                    productCompleterService.complete(productPath, newStatus, comments);                        

                                }
                                productCompleterService.complete(path, newStatus, comments);
                            }
                        }
    
                        // Return to caller
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().write("{\"result\":\"OK\"}");
                        response.setContentType("application/json");
    
                    } else {
                        response.getWriter().write("{\"result\":\"ERROR: Can't complete all tasks, please verify current status and make sure they can be set to the selected one.\"}");
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.setContentType("application/json");
                    }
                } catch (Exception e)  {
                    log.error("Error autocompleting assets " + e.getMessage());
                } finally {
                  if (resourceResolver != null) {
                      resourceResolver.close();
                  }  
                }
            }

        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver;
        try {       
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);

        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
       
        return resourceResolver;
    }
    
    private boolean isProject(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty("projectPath")) {
                    return true;
                }
            }
            
        }
        return false;
    }

    private boolean isProduct(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty("stockNumber")) {
                    return true;
                }
            }
            
        }
        return false;
    }
    
    private boolean isLook(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.hasProperty(JLPLifeStyleConstants.LOOK_SHOOT_NAME)) {
                    return true;
                }
            }
            
        }
        return false;
    }

    private boolean isAlt(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            if (isFolder(resourceNode)) {
                if (resourceNode.getName().toLowerCase().startsWith("alt")
                        || resourceNode.getName().toLowerCase().equals("main")) {
                    return true;
                }
            }
            
        }
        return false;
    }
    
    private boolean isAsset(String resourcePath, ResourceResolver resourceResolver) throws RepositoryException {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            Node resourceNode = resource.adaptTo(Node.class);
            return resourceNode.getPrimaryNodeType().getName().equals("dam:Asset");
        }
        return false;
    }
    
    private List<String> getProductPaths(String projectPath, ResourceResolver resourceResolver) throws RepositoryException {
        List<String> result = new ArrayList<>();
        Node projectDamFolderNode = resourceResolver.getResource(projectPath).adaptTo(Node.class);
        NodeIterator nodeIt = projectDamFolderNode.getNodes();
        while (nodeIt.hasNext()) {
            Node prodNode = nodeIt.nextNode();
            if (isFolder(prodNode) 
                    && prodNode.getName().toLowerCase().indexOf("composites")<0 
                    && prodNode.getName().toLowerCase().indexOf("reference")<0 ) {
                result.add(prodNode.getPath());
            }
        }
        return result;
    }

    private boolean isFolder(Node resourceNode) throws RepositoryException {
        return resourceNode.getPrimaryNodeType().getName().toLowerCase().indexOf("folder")>=0;
    }

}