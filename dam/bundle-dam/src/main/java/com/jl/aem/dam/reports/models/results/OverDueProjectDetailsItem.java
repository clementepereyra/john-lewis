package com.jl.aem.dam.reports.models.results;

public class OverDueProjectDetailsItem {
	
	 	private String projectName;
	 	private String briefType;
	    private String actualDueDate;
	    private String startShootDate;
	    private String endShootDate;
	    private String countAssets;
	    
	 	public String getProjectName() {
			return projectName;
		}
		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}
		public String getBriefType() {
			return briefType;
		}
		public void setBriefType(String briefType) {
			this.briefType = briefType;
		}
		public String getActualDueDate() {
			return actualDueDate;
		}
		public void setActualDueDate(String actualDueDate) {
			this.actualDueDate = actualDueDate;
		}
		public String getStartShootDate() {
			return startShootDate;
		}
		public void setStartShootDate(String startShootDate) {
			this.startShootDate = startShootDate;
		}
		public String getEndShootDate() {
			return endShootDate;
		}
		public void setEndShootDate(String endShootDate) {
			this.endShootDate = endShootDate;
		}
		public String getCountAssets() {
			return countAssets;
		}
		public void setCountAssets(String countAssets) {
			this.countAssets = countAssets;
		}
		
	    
	 	
}
