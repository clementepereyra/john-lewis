package com.jl.aem.dam.briefs.service;

import java.util.List;

public class LifestyleBrief {
    
    private List<LifestyleBriefRow> rows;
    private boolean valid = true;

    public static class LifestyleBriefRow {
        private String lifestyleName;
        private String shootname;
        private String namingConvention;
        private String shotDescription;
        private String buyingOffice;
        private String clientNotes;
        private String range;
        private String IDS;
        private String altNumber;
        private List<String> products;
        
        public String getLifestyleName() {
            return lifestyleName;
        }
        public void setLifestyleName(String lifestyleName) {
            this.lifestyleName = lifestyleName;
        }
        public String getShootname() {
            return shootname;
        }
        public void setShootname(String shootname) {
            this.shootname = shootname;
        }
        public String getNamingConvention() {
            return namingConvention;
        }
        public void setNamingConvention(String namingConvention) {
            this.namingConvention = namingConvention;
        }
        public String getShotDescription() {
            return shotDescription;
        }
        public void setShotDescription(String shotDescription) {
            this.shotDescription = shotDescription;
        }
        public String getBuyingOffice() {
            return buyingOffice;
        }
        public void setBuyingOffice(String buyingOffice) {
            this.buyingOffice = buyingOffice;
        }
        public String getClientNotes() {
            return clientNotes;
        }
        public void setClientNotes(String clientNotes) {
            this.clientNotes = clientNotes;
        }
        public String getRange() {
            return range;
        }
        public void setRange(String range) {
            this.range = range;
        }
        public String getIDS() {
            return IDS;
        }
        public void setIDS(String iDS) {
            IDS = iDS;
        }
        public String getAltNumber() {
            return altNumber;
        }
        public void setAltNumber(String altNumber) {
            this.altNumber = altNumber;
        }
        public List<String> getProducts() {
            return products;
        }
        public void setProducts(List<String> products) {
            this.products = products;
        }
        
        
        
    }
    
    public void setNotValid() {
        valid = false;
    }
    
    public boolean isValid() {
        return valid;
    }

    public List<LifestyleBriefRow> getRows() {
        return rows;
    }

    public void setRows(List<LifestyleBriefRow> rows) {
        this.rows = rows;
    }
}
