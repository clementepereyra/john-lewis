package com.jl.aem.dam.workflow.jobs;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.workflow.ArtworkerNotificationService;

/**
 * Schedulable job to sent asset notifications to artworkers
 * 
 * @author Bruno Szumpich
 *
 */
@Component(immediate = true, name = "Artworker notification job",service= {Runnable.class},
    property ={"scheduler.expression=0 0 8 * * ? "})
public class ArtworkerNotificationJob implements Runnable {

    private static final Logger logger = LoggerFactory
            .getLogger(ArtworkerNotificationJob.class);

    @Reference
    private ArtworkerNotificationService service;

    @Reference
    private SlingSettingsService slingSettingsService;
    

    public void run() {
        boolean isAuthorInstance = slingSettingsService.getRunModes()
                .contains("author");
        if (isAuthorInstance) {
            logger.trace("Running artworker notification job");
            service.execute();
            logger.trace("Asset artworker notification completed");
        }
    }
}