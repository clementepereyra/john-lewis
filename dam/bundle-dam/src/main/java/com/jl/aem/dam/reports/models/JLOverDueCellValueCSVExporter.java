package com.jl.aem.dam.reports.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.adobe.acs.commons.reports.api.ReportCellCSVExporter;
import com.jl.aem.dam.reports.models.results.OverDueProjectDetailsItem;


@Model(adaptables = Resource.class)
public class JLOverDueCellValueCSVExporter implements ReportCellCSVExporter {

    @Inject
    private String property;

    @Inject
    @Optional
    private String format;

    @Override
    public String getValue(Object result) {
    	OverDueProjectDetailsItem resource = (OverDueProjectDetailsItem) result;
    	JLOverDueProjectDetails val = new JLOverDueProjectDetails(resource, property);
      return val.getValue();
    }
}
