package com.jl.aem.dam.briefs.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.day.cq.dam.api.Asset;
import com.jl.aem.dam.briefs.service.CutoutBrief.CutoutBriefRow;
import com.jl.aem.dam.briefs.service.LifestyleBrief.LifestyleBriefRow;
import com.jl.aem.dam.briefs.service.RetouchBrief.RetouchBriefRow;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

public class CSVBriefHelper {
    
    public static final int Stock_Number_IDX = 0;
    public static final int Style_Guide_Product_Type_IDX = 1;
    public static final int Image_requirements_IDX = 2;
    public static final int Category_IDX = 3;
    public static final int Client_Styling_Notes_IDX = 4;
    public static final int Range_IDX = 5;
    public static final int IDS_IDX = 6;
    public static final int CSV_ROWS_LENGTH = 7;
    
    public static final String Stock_Number_Label = "Stock Number";
    public static final String Style_Guide_Product_Type_Label = "Style Guide Product Type";
    public static final String Image_requirements_Label = "Image Requirements";
    public static final String Category_Label = "Category";
    public static final String Client_Styling_Notes_Label = "Client/Styling Notes";
    public static final String Range_Label = "Range";
    public static final String IDS_Label = "IDS";
    
    //Lifestyle
    public static final int Lifestyle_Name = 0;
    public static final int Lifestyle_Shootname = 1;
    public static final int Lifestyle_Naming_convention = 2;
    public static final int Lifestyle_Shot_Description = 3;
    public static final int Lifestyle_Buying_Office = 4;
    public static final int Lifestyle_Client_Notes = 5;
    public static final int Lifestyle_Range = 6;
    public static final int Lifestyle_IDS = 7;
    public static final int Lifestyle_Alt_Number = 8;
    
    public static final String Lifestyle_Name_Label = "Lifestyle Name";
    public static final String Lifestyle_Shootname_Label = "Look/Shoot name";
    public static final String Lifestyle_Naming_convention_Label = "Naming Convention";
    public static final String Lifestyle_Shot_Description_Label = "Shot Descpription";
    public static final String Lifestyle_Buying_Office_Label = "Buying Office";
    public static final String Lifestyle_Client_Notes_Label = "Client/Styling Notes";
    public static final String Lifestyle_Range_Label = "Range";
    public static final String Lifestyle_IDS_Label = "IDS";
    
  //Lifestyle
    public static final int Retouch_Stock_Number_IDX = 0;
    public static final int Retouch_ProdType_IDX = 1;
    public static final int Retouch_Comments_IDX = 2;

    
    public static final String Retouch_Stock_Number_Label = "Stock Number";
    public static final String Retouch_ProdType_Label = "ProdType";
    public static final String Retouch_Comments_Label = "Comments";
    
    public static CutoutBrief parseCutoutBriefCSV(final Asset csvAsset) throws IOException {
        InputStream in = csvAsset.getOriginal().adaptTo(InputStream.class);
        
        final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        final CSVReader reader = new CSVReaderBuilder(new InputStreamReader(in, "UTF-8"))
                .withSkipLines(1).withCSVParser(parser).build();
        List<String[]> myEntries = reader.readAll();
        CutoutBrief cutoutBrief =new CutoutBrief();
        List<CutoutBriefRow> rows = new ArrayList<>();
        if (isValidCutoutCSV(myEntries)) {
            for (String[] rowContent : myEntries) {
                CutoutBriefRow cbRow = new CutoutBriefRow();
                cbRow.setStockNumber(rowContent[CSVBriefHelper.Stock_Number_IDX]);
                cbRow.setCategory(rowContent[CSVBriefHelper.Category_IDX]);
                cbRow.setClientStylingNotes(rowContent[CSVBriefHelper.Client_Styling_Notes_IDX]);
                cbRow.setIDS(rowContent[CSVBriefHelper.IDS_IDX]);
                cbRow.setImageRequirements(rowContent[CSVBriefHelper.Image_requirements_IDX]);
                cbRow.setStyleGuideProductType(rowContent[CSVBriefHelper.Style_Guide_Product_Type_IDX]);
                cbRow.setRange(rowContent[CSVBriefHelper.Range_IDX]);
                rows.add(cbRow);
            }
            cutoutBrief.setRows(rows);
        } else {
            cutoutBrief.setNotValid();
        }
        reader.close();
        return cutoutBrief;
        
    }
    
    public static LifestyleBrief parseLifestyleBriefCSV(final Asset csvAsset) throws IOException {
        InputStream in = csvAsset.getOriginal().adaptTo(InputStream.class);
        
        final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        final CSVReader reader = new CSVReaderBuilder(new InputStreamReader(in, "UTF-8"))
                .withSkipLines(1).withCSVParser(parser).build();
        List<String[]> myEntries = reader.readAll();
        LifestyleBrief lifestyleBrief = new LifestyleBrief();
        List<LifestyleBriefRow> rows = new ArrayList<>();
        if (isValidLifestyleCSV(myEntries)) {
            for (String[] row : myEntries) {
                LifestyleBriefRow lbRow = new LifestyleBriefRow();
                lbRow.setLifestyleName(row[CSVBriefHelper.Lifestyle_Name]);
                lbRow.setShootname(row[CSVBriefHelper.Lifestyle_Shootname]);
                lbRow.setNamingConvention(row[CSVBriefHelper.Lifestyle_Naming_convention]);
                lbRow.setShotDescription(row[CSVBriefHelper.Lifestyle_Shot_Description]);
                lbRow.setBuyingOffice(row[CSVBriefHelper.Lifestyle_Buying_Office]);
                lbRow.setClientNotes(row[CSVBriefHelper.Lifestyle_Client_Notes]);
                lbRow.setRange(row[CSVBriefHelper.Lifestyle_Range]);
                lbRow.setIDS(row[CSVBriefHelper.Lifestyle_IDS]);
                lbRow.setAltNumber(row[CSVBriefHelper.Lifestyle_Alt_Number]);
                int i = CSVBriefHelper.Lifestyle_Alt_Number+1;
                
                String currentContent = row[i];
                List<String> products = new ArrayList<>();
                
                while(currentContent != null && !currentContent.isEmpty()) {
                    products.add(currentContent);
                    i++;
                    currentContent = row[i];
                }
                lbRow.setProducts(products);
                rows.add(lbRow);
            }
            lifestyleBrief.setRows(rows);
        } else {
            lifestyleBrief.setNotValid();
        }
        reader.close();
        return lifestyleBrief;
    }
    
    
    public static RetouchBrief parseRetouchBriefCSV(final Asset csvAsset) throws IOException {
        InputStream in = csvAsset.getOriginal().adaptTo(InputStream.class);
        
        final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        final CSVReader reader = new CSVReaderBuilder(new InputStreamReader(in, "UTF-8"))
                .withSkipLines(1).withCSVParser(parser).build();
        List<String[]> myEntries = reader.readAll();
        RetouchBrief retouchBrief = new RetouchBrief();
        List<RetouchBriefRow> rows = new ArrayList<>();
        if (isValidRetouchCSV(myEntries)) {
            for (String[] rowContent : myEntries) {
                RetouchBriefRow cbRow = new RetouchBriefRow();
                cbRow.setStockNumber(rowContent[CSVBriefHelper.Retouch_Stock_Number_IDX]);
                cbRow.setComments(rowContent[CSVBriefHelper.Retouch_Comments_IDX]);
                cbRow.setProdType(rowContent[CSVBriefHelper.Retouch_ProdType_IDX]);
                rows.add(cbRow);
            }
            retouchBrief.setRows(rows);
        } else {
            retouchBrief.setNotValid();
        }
        reader.close();
        return retouchBrief;
        
    }

    private static boolean isValidLifestyleCSV(List<String[]> myEntries) {
        for (String[] row : myEntries) {
            if (StringUtils.isEmpty(row[CSVBriefHelper.Lifestyle_Naming_convention])
                    || StringUtils.isEmpty(row[CSVBriefHelper.Lifestyle_Shootname])
                    || StringUtils.isEmpty(row[CSVBriefHelper.Lifestyle_Name]) ) {
                return false;
            }
        }
        return true;
    }

    private static boolean isValidCutoutCSV(final List<String[]> myEntries) {
        for (String[] row : myEntries) {
            if (StringUtils.isEmpty(row[CSVBriefHelper.Image_requirements_IDX]) 
            		|| StringUtils.isEmpty(row[CSVBriefHelper.Style_Guide_Product_Type_IDX]) ) {
                return false;
            }
        }
        return true;
    }
    
    private static boolean isValidRetouchCSV(final List<String[]> myEntries) {
        for (String[] row : myEntries) {
            if (StringUtils.isEmpty(row[CSVBriefHelper.Retouch_Stock_Number_IDX])
                  ||  StringUtils.isEmpty(row[CSVBriefHelper.Retouch_ProdType_IDX])) {
                return false;
            }
        }
        return true;
    }

    public static CutoutBriefRow convertToCutoutRow(RetouchBriefRow row) {
        CutoutBriefRow cutoutRow = new CutoutBriefRow();
        cutoutRow.setStockNumber(row.getStockNumber());
        return cutoutRow;
    }
}
