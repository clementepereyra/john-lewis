package com.jl.aem.dam.workflow.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jl.aem.dam.pim.Product;
import com.jl.aem.dam.workflow.WorkflowBusinessSettings;

@Component(immediate = true, name = "JLP DAM Workflow Business Settings Provider",
        service = WorkflowBusinessSettings.class)
public class WorkflowBussinesSettingsImpl implements WorkflowBusinessSettings {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowBussinesSettingsImpl.class);

    private static final String BUYING_OFFICES_ROOT = "/etc/jl-admin/roll-out/buyingOffices/jcr:content/list/item";
    private static final String BRANDS_ROOT = "/etc/jl-admin/roll-out/brands/jcr:content/list/item";
    private static final String ITEMS_PROPERTY_NAME = "constants";

    private static final String BUYING_OFFICES_3_4 = "/etc/jl-admin/artworking/3_4/jcr:content/list/item";
    private static final String BUYING_OFFICES_SQUARE = "/etc/jl-admin/artworking/square/jcr:content/list/item";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public boolean isInWorkflow(String brand, String buyingOffice) {
        boolean brandIsAllowed = false;
        boolean buyingOfficeIsAllowed = false;
        List<String> allowedBrands = getPhasedRolloutBrands();
        for (String allowedBrand : allowedBrands) {
            if (allowedBrand.trim().equalsIgnoreCase(brand.trim())) {
                brandIsAllowed = true;
            }
        }
        List<String> allowedbuyingOffices = getPhasedRolloutBuyingOffices();
        for (String allowedbuyingOffice : allowedbuyingOffices) {
            if (allowedbuyingOffice.trim().equalsIgnoreCase(buyingOffice.trim())) {
                buyingOfficeIsAllowed = true;
            }
        }
        return (brandIsAllowed && buyingOfficeIsAllowed);
    }
    
    @Override
    public List<String> getPhasedRolloutBuyingOffices() {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            return getConstantsAsList(resourceResolver, BUYING_OFFICES_ROOT);

        } catch (Exception e) {
            logger.error("Fatal error while creating daily product suppliares notifications: ", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return Collections.emptyList();
    }



    @Override
    public List<String> getPhasedRolloutBrands() {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            return getConstantsAsList(resourceResolver, BRANDS_ROOT);

        } catch (Exception e) {
            logger.error("Fatal error while creating daily product suppliares notifications: ", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return Collections.emptyList();
    }
    
    public List<String> get3_4BuyingOffices() {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            return getConstantsAsList(resourceResolver, BUYING_OFFICES_3_4);

        } catch (Exception e) {
            logger.error("Fatal error while creating daily product suppliares notifications: ", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return Collections.emptyList();
    }
    
    public List<String> getSquareBuyingOffices() {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
        } catch (Exception e) {
            logger.error("Can not login system user");
            throw new RuntimeException(e);
        }
        try {
            return getConstantsAsList(resourceResolver, BUYING_OFFICES_SQUARE);

        } catch (Exception e) {
            logger.error("Fatal error while creating daily product suppliares notifications: ", e);
        } finally {
            if (resourceResolver != null) {
                resourceResolver.close();
            }
        }
        return Collections.emptyList();
    }
    
    private List<String> getConstantsAsList(ResourceResolver resourceResolver, String nodePath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(nodePath);
        Node buyingOfficesNode = resource.adaptTo(Node.class);
        List<String> result = new ArrayList<>();
        Value[] values = buyingOfficesNode.getProperty(ITEMS_PROPERTY_NAME).getValues();
        for (Value value : values) {
            result.add(value.getString());
        }
        return result;
    }
    
    @Override
    public boolean is34Ratio(Product product) {
        List<String> bo34 = get3_4BuyingOffices();
        for (String bo : bo34) {
            if (product.getBuyingOffice().trim().equalsIgnoreCase(bo.trim())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isSquareRatio(Product product) {
        List<String> sqaurebo = getSquareBuyingOffices();
        for (String bo : sqaurebo) {
            if (product.getBuyingOffice().trim().equalsIgnoreCase(bo.trim())) {
                return true;
            }
        }
        return false;
    }
}
