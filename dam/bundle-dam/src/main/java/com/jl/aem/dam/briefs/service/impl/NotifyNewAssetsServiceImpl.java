package com.jl.aem.dam.briefs.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.jl.aem.dam.briefs.service.NotifyNewAssetsService;

@Component(immediate=true, service=NotifyNewAssetsService.class)
public class NotifyNewAssetsServiceImpl implements NotifyNewAssetsService{

    private static final Logger log = LoggerFactory.getLogger(NotifyNewAssetsServiceImpl.class);
    
    private static String emailTemplate = "/etc/notification/email/html/jlp/newAssetNotification.txt";
    
    private static final String NOTIFICATION_ITEM_ROOT = "/etc/jl-admin/roll-out/new-asset-notification/jcr:content/list/item";
    
    private static final String DISABLE_JOB = "disableJob";
    private static final boolean DEFAULT_DISABLE_JOB = false;

    private static final String DEFAULT_DAM_PATH = "/content/dam/";

    private static final String LOGO_URL = "logoUrl";
    private static final String DEFAULT_LOGO_URL = "https://www.johnlewis.com/assets/header/john-lewis-logo.gif";

    private static final String LOGO_ALT = "logoAlt";
    private static final String DEFAULT_LOGO_ALT = "John Lewis";

    private static final String LOGO_HEIGHT = "logoHeight";
    private static final int DEFAULT_LOGO_HEIGHT = 30;

    private static final String SIGNATURE = "signature";
    private static final String DEFAULT_SIGNATURE = "John Lewis";

    private static final String OPEN_TR = "<tr>";
    private static final String CLOSE_TR = "</tr>";

    private static final String OPEN_TD = "<td>";
    private static final String CLOSE_TD = "</td>";


    
    @org.osgi.service.component.annotations.Reference
    private ResourceResolverFactory resolverFactory;
    
    @org.osgi.service.component.annotations.Reference
    private MessageGatewayService messageGatewayService;

    @Override
    public void notifyOnNewAsset(Node assetCreated) {
    	
    	ResourceResolver resourceResolver = null;
        Session session = null;
        boolean notificationAlreadySent = false;
        
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, this.getClass().getName());
            resourceResolver = resolverFactory.getServiceResourceResolver(param);
            session = resourceResolver.adaptTo(Session.class);
        } catch (Exception e) {
        	log.error("Can not login system user");
            throw new RuntimeException(e);
        }
    	
    	Node currentNode = assetCreated;
    	Set<String> emailNotificationDestinators = new HashSet<>();
    	try {
    		emailNotificationDestinators = getEmailRecipients(currentNode.getParent());
    		if (emailNotificationDestinators.size()>0) {
    			Calendar calendarToday = Calendar.getInstance();
    			Calendar lastNotificationSent  = Calendar.getInstance();
    			if(currentNode.getParent().hasProperty("notificationSent")) {
    				lastNotificationSent = currentNode.getParent().getProperty("notificationSent").getDate();
    				notificationAlreadySent = true;
    			}
    			if((!isSameDay(lastNotificationSent, calendarToday) && notificationAlreadySent) || !notificationAlreadySent) {
    				currentNode.getParent().setProperty("notificationSent", calendarToday);
    				session.save();
    				resourceResolver.commit();
    				sendEmails(resourceResolver, session, emailNotificationDestinators, assetCreated.getPath());
    			}
    		}
		} catch (Exception e) {
			log.error("There was an error while trying to notificate users of a new created asset" , e);
		}    	
    }
    
    public Set<String> getEmailRecipients(Node searchNode){
    	Set<String> emailNotificationDestinators = new HashSet<>();
    	try {
    		while(searchNode.getPath().contains(DEFAULT_DAM_PATH)) {
    		    if (searchNode.hasNode("jcr:content")) {
        			Node jcrNode = searchNode.getNode("jcr:content");
        			if(jcrNode.hasProperty("usersToNotify")){
        				Property usersToNotify = jcrNode.getProperty("usersToNotify");
        				if(usersToNotify.isMultiple()){
        					Value[] notifyTo = jcrNode.getProperty("usersToNotify").getValues();
        					for(Value v : notifyTo) {
        						emailNotificationDestinators.add(v.getString());
        					}
        				}else {
        					String notifyTo = jcrNode.getProperty("usersToNotify").getValue().toString();
        					emailNotificationDestinators.add(notifyTo);
        				}
        			}
    		    }
    			searchNode = searchNode.getParent();
    		}			
		} catch (Exception e) {
			log.error("There was an error while trying to retrieve recipient emails" , e);
		}
    	return emailNotificationDestinators;
    }
    
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    
    public void sendEmails(ResourceResolver resourceResolver, Session session, Set<String> recipients, String newAssetsPath) {
        Resource templateRsrc = resourceResolver.getResource(emailTemplate);
        MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), session);
        MessageGateway<HtmlEmail> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
        HtmlEmail email = new HtmlEmail();
        Map<String, String> emailProperties = new HashMap<String, String>();
        
        try {
            boolean disableNotification = getDisableNotification(resourceResolver, NOTIFICATION_ITEM_ROOT);

            if (!disableNotification) {

                String logoUrl = getLogoUrl(resourceResolver, NOTIFICATION_ITEM_ROOT);
                String logoAltText = getLogoAltText(resourceResolver, NOTIFICATION_ITEM_ROOT);
                int logoHeight = getLogoHeight(resourceResolver, NOTIFICATION_ITEM_ROOT);
                String signature = getSignature(resourceResolver, NOTIFICATION_ITEM_ROOT);
                
                emailProperties.put("logoUrl", logoUrl);
                emailProperties.put("logoAlt", logoAltText);
                emailProperties.put("logoHeight", Integer.toString(logoHeight));
                emailProperties.put("pathList", newAssetsPath);
                emailProperties.put("signature", signature);
                
                email = mailTemplate.getEmail(StrLookup.mapLookup(emailProperties), HtmlEmail.class);

                email.setTo(getEmailRecipients(recipients));

                messageGateway.send(email);

            }
        } catch (Exception e) {
            log.error("Error while trying of notificate Assets next to expire: {}", e);
        }       
    }
    
    private List<InternetAddress> getEmailRecipients(Set<String> recipients) {

        ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
        for (String recipient : recipients) {
            try {
                emailRecipients.add(new InternetAddress(recipient));
            } catch (AddressException e) {
                log.error(e.getMessage(), e);
            }
        }
        return emailRecipients;
    }
    
    private boolean getDisableNotification(ResourceResolver resourceResolver,
            String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(DISABLE_JOB)) {
                return Boolean.parseBoolean(node.getProperty(DISABLE_JOB).getString());
            }
        }
        return DEFAULT_DISABLE_JOB;
    }

    private String getLogoUrl(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_URL)) {
                return node.getProperty(LOGO_URL).getString();
            }
        }
        return DEFAULT_LOGO_URL;
    }

    private String getLogoAltText(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_ALT)) {
                return node.getProperty(LOGO_ALT).getString();
            }
        }
        return DEFAULT_LOGO_ALT;
    }

    private int getLogoHeight(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(LOGO_HEIGHT)) {
                String result = node.getProperty(LOGO_HEIGHT).getString();
                try {
                    return Integer.parseInt(result);
                } catch (NumberFormatException e) {
                    return DEFAULT_LOGO_HEIGHT;
                }
            }
        }
        return DEFAULT_LOGO_HEIGHT;
    }

    private String getSignature(ResourceResolver resourceResolver, String notificationItemPath)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        Resource resource = resourceResolver.getResource(notificationItemPath);

        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.hasProperty(SIGNATURE)) {
                return node.getProperty(SIGNATURE).getString();
            }
        }
        return DEFAULT_SIGNATURE;
    }
}
