package com.jl.aem.dam.service.integration;

import java.util.List;
import java.util.Map;

public interface BriefDetailsService {

    List<String> getHeaders(final String projectPath);
    List<Map<String,String>> getRows(final String projectPath);
    String getProjectName(String projectPath);
    
}
