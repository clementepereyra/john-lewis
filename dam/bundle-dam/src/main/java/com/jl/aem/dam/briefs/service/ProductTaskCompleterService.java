package com.jl.aem.dam.briefs.service;

import java.util.Collection;

import com.jl.aem.dam.briefs.workflow.AutocompleteStatus;

public interface ProductTaskCompleterService {

    boolean canComplete(String assetPath, AutocompleteStatus newStatus);
    void complete(String assetPath, AutocompleteStatus newStatus, String comments);
    Collection<? extends String> getAssetsToRetouch(String path, AutocompleteStatus newStatus);
    
}
