package com.jl.aem.dam.workflow.step.helper;

import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.version.VersionManager;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.validation.JLPAssetValidator;
import com.jl.aem.dam.workflow.validation.JLPAssetValidatorFactory;


public class JLAssetMerger {

    private static final Logger log = LoggerFactory.getLogger(JLAssetMerger.class);

    public void mergeAsset(Node assetNode, Session session, ResourceResolver resourceResolver, JLPAssetValidatorFactory assetValidatorFactory) {
        Node originalAssetNode = searchOriginalAsset(assetNode, session, resourceResolver);
        if (originalAssetNode != null) {
            String validationResult = null;
            try {
                JLPAssetValidator validator = assetValidatorFactory.getArtworkerValidator(originalAssetNode);
                validationResult = validator.validateAsset(assetNode.getPath());
            } catch (Exception e) {
                log.error("Can not validate retouched asset" , e);
            }
            if (JLPAssetValidator.SUCCESS_RESPONSE.equals(validationResult)) {
                String newExtension = getNewExtension(assetNode);
                addVersion(originalAssetNode, session, resourceResolver);
                copyProperties(originalAssetNode, assetNode);
                replaceRenditions(originalAssetNode, assetNode, session, resourceResolver);
                triggerMove(originalAssetNode, session, resourceResolver, newExtension);
            } else {
                try {
                    Node metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                    metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ARTWORK_REJECTED.toString());
                    metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_REJECTED_TIME, Calendar.getInstance());
                    metadataNode.setProperty(JLPConstants.JLP_COMMENT, "Retouched asset does not meet minimum technical requirements: " + validationResult);
                    
                    int valilationFailsCount = 1;
                    
                    Node metadataNodeOriginal = JcrUtils.getNodeIfExists(originalAssetNode, "jcr:content/metadata");
                    if(metadataNodeOriginal.hasProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS)){
                        valilationFailsCount = Integer.parseInt(metadataNodeOriginal.getProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS ).getString()) + 1;
                    }
                    metadataNodeOriginal.setProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS, valilationFailsCount);
                    
                    metadataNode.setProperty(JLPConstants.JLP_ASIGNEE, metadataNodeOriginal.getProperty(JLPConstants.JLP_ASIGNEE).getString());
                    metadataNode.setProperty(JLPConstants.JLP_LAST_STATUS, metadataNodeOriginal.getProperty(JLPConstants.JLP_LAST_STATUS).getString());
                    metadataNode.setProperty(JLPConstants.JLP_LAST_ASIGNEE, metadataNodeOriginal.getProperty(JLPConstants.JLP_LAST_ASIGNEE).getString());
                    
                } catch (Exception e) {
                    log.error("Can not validate retouched asset" , e);
                }
            }
        } else {
            Node metadataNode;
            try {
                metadataNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/metadata");
                metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ARTWORK_REJECTED.toString());
                metadataNode.setProperty(JLPConstants.ASSET_STATUS_CHANGE_REJECTED_TIME, Calendar.getInstance());
                metadataNode.setProperty(JLPConstants.JLP_COMMENT, "Can not find original asset for new retouched asset");
                
                int valilationFailsCount = 1;
                if(metadataNode.hasProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS)){
                    valilationFailsCount = Integer.parseInt(metadataNode.getProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS).getString()) + 1;
                }
                metadataNode.setProperty(JLPConstants.ASSET_ART_VALIDATION_FAILS, valilationFailsCount);
            } catch (RepositoryException e) {
               log.error("Can not update status after rejecting retouched node." , e);
            }
        }
        try {
            session.save();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private String getNewExtension(Node assetNode) {
        String newExtension = null;
        try {
            newExtension = assetNode.getName().substring(assetNode.getName().lastIndexOf("."));
        } catch (RepositoryException e) {
            log.error("Can't get new extension");
        }
        return newExtension;
    }


    private void copyProperties(Node originalAssetNode, Node assetNode) {
        try {
            Node metadataOriginal = originalAssetNode.getNode("jcr:content/metadata");
            PropertyIterator it = assetNode.getNode("jcr:content/metadata").getProperties();
            while (it.hasNext()) {
                try {
                    Property property = it.nextProperty();
                    if (property.getName().equalsIgnoreCase("jcr:primaryType")) {
                        continue;
                    }
                    if (property.isMultiple()) {
                        metadataOriginal.setProperty(property.getName(), property.getValues());
                    } else {
                        metadataOriginal.setProperty(property.getName(), property.getValue());
                    }
                } catch (Exception e) {
                    log.error("It was not able to copy the property.", e);
                }
            }
            originalAssetNode.getSession().save();
        } catch (Exception e) {
            log.error("Cant save the session coping the properties in asset merge.", e);
        }
    }

    private Node searchOriginalAsset(Node assetNode, Session session,
            ResourceResolver resourceResolver) {
        try {
            
            String name = assetNode.getName().substring(0, assetNode.getName().lastIndexOf("."));
            log.info("Searching for a Original asset: " + name);
            Map<String, String> map = new HashMap<String, String>();
            map.put("path", JLPConstants.ARTWORKERS_PENDING_RETOUCH);
            map.put("type", "dam:Asset");
            map.put("group.1_property", "jcr:content/metadata/" + JLPConstants.JLP_ITEM_ID);
            map.put("group.1_property.value", name );
            map.put("group.1_property.operation", JcrPropertyPredicateEvaluator.OP_EQUALS);

            Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(
                    PredicateGroup.create(map), session);
            query.setHitsPerPage(0);
                    
            SearchResult searchResult =  query.getResult();
            if (searchResult.getTotalMatches() > 0) {     
                if (searchResult.getTotalMatches() > 1) {
                    // This means that there could be more than one match, we try to return the one done by product code.
                    for (Hit hit : searchResult.getHits()) {
                        if (name.equals(hit.getNode().getProperty("jcr:content/metadata/" + JLPConstants.JLP_ITEM_ID).getString())) {
                            return hit.getNode();
                        }
                    }
                }
                return searchResult.getHits().get(0).getNode();
            }
        } catch (Exception e) {
          log.error("Error searching product node for asset", e);
        }
        return null;
    }
    
    private void addVersion(Node originalAssetNode, Session session,
            ResourceResolver resourceResolver) {
        VersionManager mgr;
        
        String assetPath = "";
        try {
            assetPath = originalAssetNode.getPath();
            mgr = session.getWorkspace().getVersionManager();
            session.save();
            mgr.checkpoint(assetPath);
            
        } catch (Exception e) {
            log.error("It was not possible to create a checkpoint for the asset: " + assetPath,e);
        }
        
    }
    
    private void replaceRenditions(Node originalAssetNode, Node assetNode, Session session,
            ResourceResolver resourceResolver) {
        try{
            //1- get assets to merge
            Resource toBeReplacedResource = resourceResolver.getResource(originalAssetNode.getPath());
            Resource replacingResource = resourceResolver.getResource(assetNode.getPath());

            Asset toBeReplacedAsset = toBeReplacedResource.adaptTo(Asset.class);
            Asset replacingAsset = replacingResource.adaptTo(Asset.class);

            //2- Delete the original renditions from node to be replaced
            NodeIterator renditionsToBeReplaced = JcrUtils.getNodeIfExists(originalAssetNode, "jcr:content/renditions").getNodes();
            while(renditionsToBeReplaced.hasNext()) {
                renditionsToBeReplaced.nextNode().remove();
            }
            
            //3- Replace the original rendition
            String mimeType = toBeReplacedAsset.getMimeType();

            Resource original = replacingAsset.getOriginal();
            InputStream stream = original.adaptTo(InputStream.class);

            toBeReplacedAsset.addRendition("original", stream, mimeType);

            //4- replace renditions
            Node renditionsNode = JcrUtils.getNodeIfExists(assetNode, "jcr:content/renditions");
            NodeIterator renditions = renditionsNode.getNodes();
            log.info("Renditions ammount: " + renditions.getSize());
            while(renditions.hasNext()) {
                Node rendition = renditions.nextNode();
                if(rendition.getProperty("jcr:primaryType").getString().equals("nt:file")) {
                    String renditionName = rendition.getName();
                    log.info("rendition Name: " + renditionName);
                    Resource r = replacingAsset.getRendition(renditionName);
                    stream = r.adaptTo(InputStream.class);
                    toBeReplacedAsset.addRendition(renditionName, stream, mimeType);
                }
            }

            //5- Remove the uploaded node
            session.removeItem(replacingResource.getPath());

            session.save();
        }catch(Exception e){
            log.error("Error while binary was replacing" , e);
        }
    }
    
    private void triggerMove(Node originalAssetNode ,Session session,
            ResourceResolver resourceResolver, final String newExtension) {
            try{
                Node metadataNode = JcrUtils.getNodeIfExists(originalAssetNode, "jcr:content/metadata");
                metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ASSET_SUBMITTED_FOR_FINAL_APPROVAL.toString());
                session.save();
            } catch (Exception e) {
                log.error("Problem while trying to move asset to review folder.", e);
            }
        
    }

}
