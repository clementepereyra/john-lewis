package com.jl.aem.dam.workflow.step;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(immediate = true, service = WorkflowProcess.class,
property = {"process.label=Delete Product from brief project"})
public class DeleteProductFromBriefProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(DeleteProductFromBriefProcess.class);

    public static final String TYPE_JCR_PATH = "JCR_PATH";

    @Reference
    private BriefsService briefService;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public void execute(WorkItem wItem, WorkflowSession wfSession, MetaDataMap metadataMap)
            throws WorkflowException {
        Session session = wfSession.getSession();
        String path = getPathFromPayload(wItem, session);
        ResourceResolver resourceResolver = getResourceResolver(session);
        if (isProductFolder(path, resourceResolver)) {
            String productCode = path.substring(path.lastIndexOf("/")+1);
            String projectPath = path.substring(0, path.lastIndexOf("/"));
            briefService.processRemovedProduct(projectPath, productCode);
        }
    }
    
    
    private boolean isProductFolder(String path, ResourceResolver resourceResolver) {
        if (path!=null) {
            if (path.startsWith(JLPConstants.PROJECTS_DAM_PATH)) {
                String[] folders = path.replace(JLPConstants.PROJECTS_DAM_PATH, "").split("/");
                if (folders.length == 6) {
                    String productCode = folders[5];
                    if (!productCode.toLowerCase().equals("reference") 
                            && !productCode.toLowerCase().equals("composites")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private String getPathFromPayload(final WorkItem item, final Session session) {
        if (item.getWorkflowData().getPayloadType().equals(TYPE_JCR_PATH)) {
            return item.getWorkflowData().getPayload().toString();    
        }
        return null;
    }

    private ResourceResolver getResourceResolver(Session session) {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("user.jcr.session", session); 
            resourceResolver = resolverFactory.getResourceResolver(param);
            return resourceResolver;
        } catch (Exception e) {
            log.error("Can not login system user and get resource resolver " + e.getMessage());
            return null;
        }
    }
}
