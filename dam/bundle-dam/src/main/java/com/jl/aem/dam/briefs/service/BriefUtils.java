package com.jl.aem.dam.briefs.service;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.granite.asset.api.AssetManager;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.jl.aem.dam.workflow.JLPConstants;

public class BriefUtils {
    
    public static String getSingleAssetWorkflowModel(Node jcrContent) throws RepositoryException {
        boolean isCutoutWorkflow = isCutoutBrief(jcrContent);
        boolean isLifestyleWorkflow = isLifestyleBrief(jcrContent);
        if (isCutoutWorkflow) {
            return JLPConstants.PROVIDE_SHOOT_CUTOUT_WORKFLOW_NAME;
        } else if (isLifestyleWorkflow)  {
            return JLPConstants.PROVIDE_SHOOT_LIFESTYLE_WORKFLOW_NAME;
        } else {
            return JLPConstants.PROVIDE_SHOOT_RETOUCH_WORKFLOW_NAME;
        }
    }

    public static boolean isCutoutBrief(Node projectContentNode)
            throws ValueFormatException, RepositoryException, PathNotFoundException {
        return projectContentNode.getProperty("cq:template").getString().indexOf("cutout") >= 0;
    }
    
    public static boolean isLifestyleBrief(Node projectContentNode) throws RepositoryException {
        return projectContentNode.getProperty("cq:template").getString().indexOf("lifestyle") >= 0;
    }

    public static boolean isRetouchBrief(Node projectContentNode) throws RepositoryException {
        return projectContentNode.getProperty("cq:template").getString().indexOf("retouch") >= 0;
    }

    public static void reopenAssetForRetouch(String liveAssetPath, String targetFolder,
            ResourceResolver resourceResolver) throws RepositoryException, PersistenceException {
        String fileName = liveAssetPath.substring(liveAssetPath.lastIndexOf("/") + 1);
        String targetPath = targetFolder + "/" + fileName;
        Session session = resourceResolver.adaptTo(Session.class);
        JcrUtils.getOrCreateByPath(targetFolder, "sling:Folder", session);
        session.save();
        AssetManager manager = resourceResolver.adaptTo(AssetManager.class);
        manager.copyAsset(liveAssetPath, targetPath);
        Resource resource = resourceResolver.getResource(targetPath);
        if (resource != null) {
            Asset targetAsset = resource.adaptTo(Asset.class);
            Rendition layered = targetAsset.getRendition(JLPConstants.RENDITION_LAYERED);
            targetAsset.addRendition(JLPConstants.RENDITION_ORIGINAL, layered.getStream(),
                    layered.getMimeType());
        }
        resourceResolver.commit();

    }
    
    public static String getProjectNodeName(final Node assetNode) throws RepositoryException {
        Node altNode = assetNode.getParent();
        Node prodNode = altNode.getParent();
        Node briefNode = prodNode.getParent();
        return briefNode.getName();
    }
    
    public static String getProjectPath(final Node assetNode) throws RepositoryException {
        Node altNode = assetNode.getParent();
        Node prodNode = altNode.getParent();
        Node briefNode = prodNode.getParent();
        Property prop = briefNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
        if (prop.isMultiple()) {
            return prop.getValues()[0].getString();
        } else {
            return prop.getString();
        }
    }
    
    
    public static String getProjectPathFromProductNode(final Node productNode) throws RepositoryException {
        Node briefNode = productNode.getParent();
        Property prop = briefNode.getProperty(JLPConstants.JLP_PROJECT_PATH);
        if (prop.isMultiple()) {
            return prop.getValues()[0].getString();
        } else {
            return prop.getString();
        }
    }


}
