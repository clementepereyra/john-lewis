package com.jl.aem.dam.service.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetManager;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;

@Component(service = { Servlet.class },
property = { 
    "sling.servlet.paths=/bin/jlp/public/ChangeSequenceOrder",
    "sling.servlet.methods=POST"
  }
)
public class ChangeSequenceOrderServlet extends SlingAllMethodsServlet {
    private static final Logger log = LoggerFactory.getLogger(ChangeSequenceOrderServlet.class);

    private static final long serialVersionUID = 2598426539166789515L;
    private static final String MOVE_ASSET_WORKFLOW = "/etc/workflow/models/dam/move_asset/jcr:content/model";

    @Reference
    private SlingRepository repository;
    
    @Reference
    private InvokeAEMWorkflow workflowInvoker;


    public void bindRepository(SlingRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        try {
            // Get the submitted form data

            String newSequence = request.getParameter("new_sequence_order");

            RequestParameter assetPaths[] = request.getRequestParameters("path");
            List<Asset> assetList = new ArrayList<Asset>();

            if (assetPaths != null) {
                Resource tagResource = resourceResolver.getResource(newSequence);
                Tag tag = tagResource.adaptTo(Tag.class);
                for (RequestParameter path : assetPaths) {
                    String filename = path.toString();
                    Resource resource = resourceResolver.getResource(filename);

                    if (null != resource) {
                        Asset asset = resource.adaptTo(Asset.class);
                        if (null != asset)
                            assetList.add(asset);
                    }
                }

                updateMetadataProp(assetList, tag.getTagID());
                updateAssetNames(assetList, tag.getName());
            }

            // Return to caller
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.error("Error in DamMetadataUpdateServlet " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void updateAssetNames(List<Asset> assetList, String suffix) {
        

        for (Asset asset : assetList) {
            String targetPath = asset.getParent().getPath();
            Resource metadataResource = asset.getChild("jcr:content/metadata");
            ModifiableValueMap map = metadataResource.adaptTo(ModifiableValueMap.class);
            
            Object targetNameObj = map.get(JLPConstants.JLP_ITEM_ID);
            String targetName = asset.getName().substring(0, asset.getName().lastIndexOf("."));
            
            if (targetNameObj != null) {
                targetName = targetNameObj.toString(); 
            }
            targetName += suffix.trim().replace("main", "") + asset.getName().substring(asset.getName().lastIndexOf("."));
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("targetFolder", targetPath );
            metaData.put("targetFileName", targetName);
            workflowInvoker.startWorkflow(MOVE_ASSET_WORKFLOW, asset.getPath(), metaData);
        }
    }

    private void updateMetadataProp(List<Asset> assetList, String tagID) throws PersistenceException {

        // just change asset metadata
        for (Asset asset : assetList) {
            Resource metadataResource = asset.getChild("jcr:content/metadata");
            ModifiableValueMap map = metadataResource.adaptTo(ModifiableValueMap.class);

            if (tagID != null && !tagID.trim().isEmpty()
                    && tagID.trim().length() > 1) {
                map.put(JLPConstants.JLP_IMAGE_CATEGORY, tagID);
            }
            asset.getResourceResolver().commit();
        }
    }

}