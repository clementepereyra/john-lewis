package com.jl.aem.dam.service.integration;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "JLP Http Client Configuration", description = "Service Configuration")
public @interface JLPHttpClientConfiguration {
    

    @AttributeDefinition(name ="Proxy Host", description="Proxy host", defaultValue="proxy.jl.com")
    String getProxyHost() default "";
    
    @AttributeDefinition(name ="Proxy port", description="Proxy port'", defaultValue="80")
    int getProxyPort() default 80;
    
    @AttributeDefinition(name ="Proxy protocol", description="The protocol to communicate with proxy", defaultValue="http")
    String getProxyProtocol() default "http" ;
    
    
}
