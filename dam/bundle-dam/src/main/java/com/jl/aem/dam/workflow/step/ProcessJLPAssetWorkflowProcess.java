package com.jl.aem.dam.workflow.step;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.jl.aem.dam.briefs.service.BriefsService;
import com.jl.aem.dam.briefs.service.NotifyNewAssetsService;
import com.jl.aem.dam.workflow.AssetStatus;
import com.jl.aem.dam.workflow.InvokeAEMWorkflow;
import com.jl.aem.dam.workflow.JLPConstants;
import com.jl.aem.dam.workflow.JLWorkflowsUtils;
import com.jl.aem.dam.workflow.step.helper.JLAssetMerger;
import com.jl.aem.dam.workflow.validation.JLPAssetValidatorFactory;

/**
 * Workflow step that catches assets related to JLP workflow and starts associated actions.
 * 
 * Arguments:
 * <ul>
 * <li>renditionName - The name of the rendition to modify.</li>
 * </ul>
 *
 */
@Component(immediate=true, service=WorkflowProcess.class, property={"process.label=Identy JLP Assets and put them into correct workflow"})
public class ProcessJLPAssetWorkflowProcess extends AbstractDAMWorkflowProcess  implements WorkflowProcess {

    /**
     * Logger instance for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(ProcessJLPAssetWorkflowProcess.class);

    public static final String TYPE_JCR_PATH = "JCR_PATH";
    
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private InvokeAEMWorkflow workflowInvoker;

    @Reference
    private JLPAssetValidatorFactory assetValidatorFactory;
    
    @Reference
    private BriefsService briefsService;
    
    @Reference
    private NotifyNewAssetsService newAssetNotification;

    @Override
    public final void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaData)
            throws WorkflowException {
        log.info("Associate asset worflow process is running");
        final Asset asset = getAssetFromPayload(workItem, workflowSession.getSession());
       
        if (asset != null) {
            try {
                if (asset.getPath().indexOf(JLPConstants.CREATIVE_ASSETS_ROOT_FOLDER) < 0
                        && asset.getPath().indexOf(JLPConstants.PROJECTS_DAM_PATH) < 0
                        && asset.getPath().indexOf(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH) <0) {
                    log.info("JL Product Association not needed: asset is not on creative folder.");
                    return;
                }
                Resource resource = asset.adaptTo(Resource.class);
                ResourceResolver resolver = resource.getResourceResolver();
                Session session = workflowSession.getSession();
                Resource assetResource = resolver.getResource(asset.getPath());
                Node asNode = assetResource.adaptTo(Node.class);
                newAssetNotification.notifyOnNewAsset(asNode);
                if (isArtworkerAsset(asNode)  && asNode.getPath().toLowerCase().indexOf("rejected") < 0) {
                    JLAssetMerger merger = new JLAssetMerger();
                    merger.mergeAsset(asNode, session, resolver, assetValidatorFactory);
                    session.save();
                    resolver.commit();
                } else if (isSupplierAsset(asNode) && asNode.getPath().toLowerCase().indexOf("rejected") < 0 ) {
                    //JLProductAssociator associator = new JLProductAssociator();
                    //associator.associateAssetWithProduct(asNode, resolver, session, asset.getModifier());
                     Node metadataNode = JcrUtils.getNodeIfExists(asNode, "jcr:content/metadata");
                     String supplierInfo = JLWorkflowsUtils.getSupplierIDForNode(asNode);
                     if (supplierInfo != null) {
                         metadataNode.setProperty(JLPConstants.JLP_SUPPLIER_ID, supplierInfo);
                     } else {
                         metadataNode.setProperty(JLPConstants.JLP_SUPPLIER_ID, "local_user");
                     }
                     asNode.addMixin("mix:versionable");                     
                     metadataNode.setProperty(JLPConstants.JLP_STATUS, AssetStatus.ASSET_NEW.toString());
                     metadataNode.setProperty(JLPConstants.JLP_ASIGNEE, asset.getModifier());
                     session.save();
                     resolver.commit();
                } else if (isCutoutBriefAsset(asNode, resolver) || isRetouchBriefAsset(asNode, resolver)) {
                    briefsService.ingestCutoutAsset(asNode.getPath());
                } else if (isLifestyleBriefAsset(asNode, resolver)) {
                    briefsService.ingestLifestyleAsset(asNode.getPath());
                } else if (is3rdPartyRetouchedAsset(asNode) && asNode.getPath().toLowerCase().indexOf("rejected") < 0 ) {
                    briefsService.ingest3rdpartyRetouchedAsset(asNode.getPath());
                }
            } catch (Exception e) {
                log.error("Can not associate asset " +  asset.getPath(), e);
            }
        } else {
            String wfPayload = workItem.getWorkflowData().getPayload().toString();
            String message = "execute: cannot add required field to asset [{" + wfPayload
                    + "}] in payload doesn't exist for workflow [{" + workItem.getId() + "}].";
            throw new WorkflowException(message);
        }
        log.info("Associate asset worflow process is finished");
    }

    private boolean is3rdPartyRetouchedAsset(Node assetNode) throws RepositoryException{
        if (assetNode.getPath().toLowerCase().indexOf(JLPConstants.THIRD_PARTIES_RETOUCHERS_DAM_PATH.toLowerCase())>=0 
            && assetNode.getPath().toLowerCase().indexOf(JLPConstants.THIRD_PARTY_RETOUCHED_FOLDER.toLowerCase()) >= 0) {
                return true; 
            }
        return false;
    }

    private boolean isLifestyleBriefAsset(Node asNode, ResourceResolver resourceResolver) {
        return isBriefAsset(asNode, resourceResolver, "lifestyle-brief");
    }

    private boolean isCutoutBriefAsset(final Node asNode, final ResourceResolver resourceResolver) {
        return isBriefAsset(asNode, resourceResolver, "cutout-brief");
    }

    private boolean isRetouchBriefAsset(final Node asNode, final ResourceResolver resourceResolver) {
        return isBriefAsset(asNode, resourceResolver, "retouch-brief");
    }
    
    private boolean isBriefAsset(final Node asNode, final ResourceResolver resourceResolver, String briefPattern) {
        try {
            String path = asNode.getPath();
            String name = asNode.getName();
            if (path.indexOf(JLPConstants.PROJECTS_DAM_PATH) >=0 
                    && name.toLowerCase().indexOf(".csv")<0
                    && name.toLowerCase().indexOf("cover")<0) {
                Node parent = asNode.getParent();
                while (!parent.hasProperty("projectPath")) {
                    parent = parent.getParent();
                }
                Property pathProperty = parent.getProperty("projectPath");
                String projectPath = null;
                if (pathProperty.isMultiple()) {
                    projectPath = pathProperty.getValues()[0].getString();
                } else {
                    projectPath = pathProperty.getString();
                }
                Node projectNode = resourceResolver.getResource(projectPath).adaptTo(Node.class).getNode("jcr:content");
                if (projectNode.getProperty("cq:template").getString().indexOf(briefPattern)>=0) {
                    return true;
                }
                
            }
        } catch (Exception e) {
            log.error("Error processing asset", e);
        }
        return false;
    }
    
    @Override
    protected ResourceResolverFactory getResolverFactory() {
        return resolverFactory;
    }
    
    private boolean isSupplierAsset(Node asNode) throws RepositoryException {
        return asNode.getPath().contains(JLPConstants.SUPPLIERS_ROOT_FOLDER);
    }

    private boolean isArtworkerAsset(Node asNode) throws RepositoryException {
        return asNode.getPath().contains(JLPConstants.ARTWORKERS_RETOUCHED);
    }

    
}
