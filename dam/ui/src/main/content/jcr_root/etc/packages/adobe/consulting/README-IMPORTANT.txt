The package for acs-aem-commons-content contained in this directory was customized for JL in order
to extend the report builder feature.
 
This is the details of changes in acs-aem-commons-content project:

1) Change in bundle/pom.xml

maven-bundle-plugin configuration updated to include JL packages

<Import-Package>
    com.adobe.cq.dialogconversion;resolution:=optional,
    sun.misc.*;resolution:=optional,
    org.apache.sling.models.annotations.*;resolution:=optional, <!-- the used class InjectionStrategy is only since Sling Models API 1.2 -->
    org.apache.sling.models.spi.injectorspecific.*;resolution:=optional, <!-- that package was added with Sling Models API 1.1 -->
    com.amazonaws.*;resolution:=optional, <!-- only required for MCP S3 Ingestor -->
    com.github.jknack.handlebars;resolution:=optional, <!-- only required for Report Builder -->
    com.day.cq.wcm.workflow.process;version="[6.0,8)", <!-- using a wider version range for forward compatibility -->
    com.day.cq.mailer;version="[5.9,7)", <!-- using a wider version range for forward compatibility -->
    com.adobe.cq.sightly;version="[2.5,4)",
    org.apache.poi.ss.usermodel;version="[2.0,4)",  <!-- using a wider version range for forward compatibility -->
    org.apache.poi.ss.util;version="[2.0,4)",
    org.apache.poi.xssf.usermodel;version="[2.0,4)",
    com.jl.aem.dam.reports.models;version="[2.6,8)",
    *
</Import-Package>

The reason of this change is to allow the classloader in ACS commons package to load the classes provided
by JL report executor (the classloader only knows the classes specificied in the import-package configuration)

2) Change in com.adobe.acs.commons.reports.internal.ReportCSVExportServlet

public static final String PN_EXECUTOR = "reportExecutor";
  private void updateCSV(Resource config, SlingHttpServletRequest request, List<ReportCellCSVExporter> exporters,
      Csv csv, Writer writer) throws ReportException {
      String reportExecutorClass = config.getValueMap().get(PN_EXECUTOR, String.class);  
    Class<?> exClass;
    try {
        exClass = getClass().getClassLoader().loadClass(reportExecutorClass);
    
        Object model = request.adaptTo(exClass);
        
        ReportExecutor executor = (ReportExecutor) model;
    
        executor.setConfiguration(config);
        log.debug("Retrieved executor {}", executor);
    
        ResultsPage queryResult = executor.getAllResults();
        List<? extends Object> results = queryResult.getResults();
        log.debug("Retrieved {} results", results.size());
    
        for (Object result : results) {
          List<String> row = new ArrayList<String>();
          try {
            for (ReportCellCSVExporter exporter : exporters) {
              row.add(exporter.getValue(result));
            }
            csv.writeRow(row.toArray(new String[row.size()]));
            writer.flush();
          } catch (Exception e) {
            log.warn("Exception writing row: " + row, e);
          }
        }
    
        log.debug("Results written successfully");
    } catch (ClassNotFoundException e1) {
        log.error("Class not found", e1);
    }
  }
  
  The change was done to allow the csv exporter to export reports different than query report executor, just
  like the report runner does.
 