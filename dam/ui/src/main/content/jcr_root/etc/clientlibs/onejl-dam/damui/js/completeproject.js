/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#completeprojectform", submit = "#completeprojectform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
        //e.preventDefault();
        
        //var $submit = $(e.target);
        var dataPath, i,
        servletPath = '/bin/jlp/public/changeProjectStatus',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        projectPath = window.location.href.substring(window.location.href.indexOf('.html/') + 5);
 		data += '&project_path=' + projectPath;
 		data += '&status=COMPLETE';
        //var numAssetsShared = $(".foundation-selections-item").length;
         
        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content"),
            location = "";
            
            if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
            		alert("Can't Force Complete the Brief because exists upload Assets in Pending Shot or Pending Retouch state");
            } 

            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error Force Compleating Brief");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
$(document).ready(function() {
	if (window.location.href.indexOf("/projects/details.html")>=0) {
		$.ajax({
			  url: "/bin/jlp/public/showForceCompleteButton",
			  type: "GET",
			  data: { 
				  project_path: window.location.href.substring(window.location.href.indexOf('.html/') + 5)
			  },
			  success: function(response) {
				  var showButton = (response.result === 'true');
				  if (!showButton) {
					  $('.foundation-collection-action.coral-Link.coral-BasicList-item.coral-AnchorList-item').each(function(idx, element){
						  var text = $(element).find('.coral-BasicList-item-content').text();
						  if ("Force Complete" === text) {
							  $(element).attr('disabled', 'disabled');
						  }
					  });
				  }			  
			  },
			  error: function(xhr) {
				  alert("Error while trying to evaluete if show Force Complete Button");
			  }
		});
	}
}
);
