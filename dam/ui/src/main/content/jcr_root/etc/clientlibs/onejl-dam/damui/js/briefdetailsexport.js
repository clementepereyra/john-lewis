/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    		//$('#briefdetails').find('coral-dialog-header')[0].innerHTML='fer';
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                // For IE (tested 10+)
                if (window.navigator.msSaveOrOpenBlob) {
                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                        type: "text/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, filename);
                } else {
                	downloadFile(filename, csvData);


                	
//                    $(this)
//                        .attr({
//                            'download': filename
//                            ,'href': csvData
//                            //,'target' : '_blank' //if you want it to open in a new window
//                    });
                }
                
                function downloadFile(fileName, urlData) {

                	var aLink = document.createElement('a');
                    aLink.download = fileName;
                    aLink.href = urlData;

                    var event = new MouseEvent('click');
                    aLink.dispatchEvent(event);
            	}

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $(document).on("click", "#briefdetailsform-submit", function (e) {
                var projectName = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);

                exportTableToCSV.apply(this, [$('#briefdetailsform > table'), projectName + '.csv']);

            });


}(document, Granite.$));