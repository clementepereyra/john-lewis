/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#addlookform", submit = "#addlookform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }

    $(document).on("click", submit, function (e) {
        //e.preventDefault();

        //var $submit = $(e.target);

        var look_name = $(".coral-Form-fieldwrapper #look_name").val();
        var style_gui = $(".coral-Form-fieldwrapper #styling_guide").val();
        var naming_conventions = $(".coral-Form-fieldwrapper #naming_conventions").val();
        var no_of_alts = $(".coral-Form-fieldwrapper #no_of_alts").val();

        if((look_name == "") || (style_gui == "") || (naming_conventions == "") || (no_of_alts == "")) {

            alert("Please fill the required values");

        } else {

            var isnum = (/^\d+$/).test(no_of_alts);

            if (!isnum){
                alert("Number of Alts must be a number");
            } else {
    			var dataPath, i,
                url = '/bin/jlp/public/AddLookToBrief',
                $form = $(form),
                formData = $form.serialize(),
                //concat the assets paths to parameters
                projectPath = window.location.href.substring(window.location.href.indexOf('.html/') + 5);
    			formData += '&project_path=' + projectPath;
                

                $.ajax({
                    type: 'GET',
                    url: url + "?" + formData,
                    cache: false
                }).done(function (data, textStatus, jqXHR) {
                    
                    	var confirmResult = true;
                    	if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
                    		confirmResult = false;
                            alert(jqXHR.responseJSON.result);
                        } else if (jqXHR.responseJSON.result.indexOf("WARNING")>=0) {
                        	confirmResult =  confirm(jqXHR.responseJSON.result);
                        }
                    	if (confirmResult) {
                        	
                            post(url, formData);
                        }
                    
                  }).fail(function (jqXHR, textStatus, errorThrown) {
                    //show the error
                    alert("Error validating look");
                });
        
                 $(this).closest(".coral-Dialog").modal("hide");
                 $(this).closest(".coral-Dialog").hide();
            }
        }
    });

    function post(url, data) {
    	$.ajax({
            type: 'POST',
            url: url,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content"),
            location = "";

            if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
                alert(jqXHR.responseJSON.result);
              } 

             if (jqXHR.responseJSON.result.indexOf("WARNING")>=0) {
                alert(jqXHR.responseJSON.result);
              }  

            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error adding product");
        });
    }
    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
