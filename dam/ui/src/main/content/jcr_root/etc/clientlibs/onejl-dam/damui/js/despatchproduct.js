/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#despatchproductform", submit = "#despatchproductform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }

    $(document).on("click", submit, function (e) {
        var productId, i, productPath,
        servletPath = '/bin/jlp/public/DespatchProduct',
        $form = $(form),
        postData = $form.serialize();
        var getData = "?"+$form.serialize();
        //Concat the path
        var items = $(".foundation-collection").find(".foundation-selections-item");
        for (i = 0; i < items.length; i++) {
            productPath = items[i].dataset.foundationCollectionItemId;
            if (productPath) {
            	postData += '&path=' + encodeURIComponent(productPath, "UTF-8");
                getData+='&path=' + encodeURIComponent(productPath, "UTF-8");
            }
        }

        $.ajax({
            type: 'GET',
            url: servletPath + getData,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            if(jqXHR.responseText){
            	var text = "";
            	if (jqXHR.responseText.indexOf("Quantity is 0")>=0) {
            		text = "WARNING: Current Quantity is 0";
            	} else if (jqXHR.responseText.indexOf("Current quantity is")>=0) {
            		text = "WARNING: " + jqXHR.responseText;
            	} else {
            		text= "WARNING: This product is still needed for:\n\n" + data + "\nBrief(s).";
            	}
                var confirmResult = confirm(text);
                if (confirmResult) {
                	postDespatch(servletPath, postData);
                }
            } else {
            	postDespatch(servletPath, postData);
            }
            
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error despatching product");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });
    function reset($form) {
        $form[0].reset();
    }
    
    function postDespatch(servletPath, postData) {
    	$.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: postData,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            setTimeout(refreshPage(contentApi), 2000);
            
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error despatching product");
        });
    }

}(document, Granite.$));
