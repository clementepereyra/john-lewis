/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#cancelprojectform", submit = "#cancelprojectform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
        //e.preventDefault();
        
        //var $submit = $(e.target);
        var dataPath, i,
        servletPath = '/bin/jlp/public/changeProjectStatus',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        projectPath = window.location.href.substring(window.location.href.indexOf('.html/') + 5);
 		data += '&project_path=' + projectPath;
 		data += '&status=CANCELLED';
        //var numAssetsShared = $(".foundation-selections-item").length;
         
        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content"),
            location = "";
            
            if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
            	alert(jqXHR.responseJSON.result);
              } 

            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error cancelling brief");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
