/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#addproducttolookform", submit = "#addproducttolookform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }

    $(document).on("click", submit, function (e) {
        //e.preventDefault();

        //var $submit = $(e.target);

        var prod_code = $(".coral-Form-fieldwrapper #product_code").val();


        if(prod_code == ""){

            alert("Please fill the required values");

        } else {

            var isnum = (/^\d+$/).test(prod_code);

            if (prod_code.length != 8 || !isnum) {
                alert("Product id must be an 8 digit number");
            } else {

    			var dataPath, i,
                servletPath = '/bin/jlp/public/AddProductToLook',
                $form = $(form),
                data = $form.serialize(),
                //concat the assets paths to parameters
                projectPath = window.location.href.substring(window.location.href.indexOf('.html/') + 5);
                data += '&project_path=' + projectPath;
                var items = $(".foundation-collection").find(".foundation-selections-item");
                if (items.length) {
                    for (i = 0; i < items.length; i++) {
                        dataPath = items[i].dataset.foundationCollectionItemId;
                        if (dataPath) {
                            data += '&path=' + dataPath;
                        }
                    }
                }
        
                $.ajax({
                    type: 'POST',
                    url: servletPath,
                    contentType: "application/x-www-form-urlencoded",
                    data: data,
                    cache: false
                }).done(function (data, textStatus, jqXHR) {
                    var contentApi = $(".foundation-content").adaptTo("foundation-content"),
                    location = "";
        
                    if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
                        alert(jqXHR.responseJSON.result);
                      } 
        
                    setTimeout(refreshPage(contentApi), 2000);
                  }).fail(function (jqXHR, textStatus, errorThrown) {
                    //show the error
                    alert("Error adding product");
                });
        
                 $(this).closest(".coral-Dialog").modal("hide");
                 $(this).closest(".coral-Dialog").hide();
            }
        }
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
