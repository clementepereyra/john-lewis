/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#autocompletetaskprojectform", submit = "#autocompletetaskprojectform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
        //e.preventDefault();
        
    	var dataPath, i,
        servletPath = '/bin/jlp/public/AutocompleteTaskProject',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        projectPath = window.location.href.substring(window.location.href.indexOf('.html/') + 5);
        data += '&project_path=' + projectPath;
        //concat the assets paths to parameters
        

        //var numAssetsShared = $(".foundation-selections-item").length;
         
        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
                alert(jqXHR.responseJSON.result);
            } 

            if (jqXHR.responseJSON.result.indexOf("WARNING")>=0) {
                alert(jqXHR.responseJSON.result);
            }  
            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Tasks can not be completed. ");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
