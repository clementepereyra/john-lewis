/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#autocompletetaskform", submit = "#autocompletetaskform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
        //e.preventDefault();
        
        //var $submit = $(e.target);
        var dataPath, i,
        servletPath = '/bin/jlp/public/AutocompleteTaskAsset',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        items = $(".foundation-collection").find(".foundation-selections-item");
        if (items.length) {
            for (i = 0; i < items.length; i++) {
                dataPath = items[i].dataset.foundationCollectionItemId;
                if (dataPath) {
                    data += '&path=' + dataPath;
                }
            }
        }

        //var numAssetsShared = $(".foundation-selections-item").length;
         
        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
                alert(jqXHR.responseJSON.result);
              } 

            if (jqXHR.responseJSON.result.indexOf("WARNING")>=0) {
                alert(jqXHR.responseJSON.result);
            }  
            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Tasks can not be completed. ");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
