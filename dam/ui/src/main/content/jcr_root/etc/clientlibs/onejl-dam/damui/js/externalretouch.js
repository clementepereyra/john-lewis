/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#externalretouchform", submit = "#externalretouchform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
    	var retoucher = $(".coral-Form-fieldwrapper #retoucher").val();

        if (retoucher == "") {
            retoucher = "born";
            $(".coral-Form-fieldwrapper #retoucher").val("born");
        } else if (retoucher.indexOf(":")>=0 || retoucher.indexOf("/")>=0 || retoucher.indexOf(" ") >=0 ) {
            alert("The retoucher can not contain blank spaces or any of the following characters: / : ");
        }
        var paths = "";
        var pathsObject = {};
        var dataPath, i,
        servletPath = '/bin/jlp/public/externalRetouch',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        items = $(".foundation-collection").find(".foundation-selections-item");
        var patharr = [];

        
        /* Only checks assets selected in assets view */
        if (items.length) {
            items.each(function(i) {
                var item = $(this);
                var itemPath = item.data("foundation-collection-item-id");
                patharr.push(itemPath);
                paths += "path=" + encodeURIComponent(itemPath).replace(/%2F/g, "/") + "&";
                pathsObject[itemPath] = true;
            });

            //remove the last '&'
            paths = paths.substr(0, paths.length - 1);
        }
        data = data + "&" + paths;
        
        
        $.ajax({
            type: 'GET',
            url: servletPath + "?" + data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            if(jqXHR.responseJSON){
            		var bodyHtml = "<span>Assets to be Retouched</span>&nbsp;<span>" + jqXHR.responseJSON.assetsToRetouch + "</span><br/>"
            						+ "<span>Pending Shots</span>&nbsp;<span>" + jqXHR.responseJSON.assetsRequested + "</span><br/>"
            						+ "<span>Assets Pending Retouch not in selection</span>&nbsp;<span>" + jqXHR.responseJSON.assetsUploaded + "</span><br/>"
            						+ "<span>Assets in External Retouch</span>&nbsp;<span>" + jqXHR.responseJSON.assetsExternalRetouch + "</span><br/>"
            						+ "<span>Assets in Internal Retouch</span>&nbsp;<span>" + jqXHR.responseJSON.assetsInternalRetouch + "</span><br/>"
            						+ "<span>Assets Retouched</span>&nbsp;<span>" + jqXHR.responseJSON.assetsRetouched + "</span><br/>"
            						+ "<span>Assets Pending Colour Match</span>&nbsp;<span>" + jqXHR.responseJSON.pendingColourMatch + "</span><br/>"
            						+ "<span>Assets Delivered</span>&nbsp;<span>" + jqXHR.responseJSON.assetsDelivered + "</span><br/>"
            						+ "<span>Total Assets in Brief</span>&nbsp;<span>" + jqXHR.responseJSON.totalAssets + "</span><br/>";
            		var dialog = new Coral.Dialog().set({
            		    id: 'buyDialog',
            		    header: {
            		      innerHTML: 'Status Summary'
            		    },
            		    content: {
            		      innerHTML: bodyHtml
            		    },
            		    footer: {
            		      innerHTML: '<button id="cancelButton" is="coral-button" variant="default" coral-close>Cancel</button><button id="acceptButton" is="coral-button" variant="primary">Accept</button>'
            		    }
            		  });
            		  document.body.appendChild(dialog);
            		  dialog.on('coral-overlay:close', function (event) {
            			  });
        			  dialog.on('click', '#acceptButton', function() {
        			    dialog.hide();
        			  //POST Request
                        var formdata = {
                        	"retoucher" : retoucher,
                            "path": patharr,
                            "_charset_": "utf-8"
                        };
                        post(servletPath, formdata);
        			  });
            		  dialog.show();
            } else {
            	alert("Error obtaining summary");
            }
            
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error obtaining summary");
        });

        

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    function post (url, data) {
	  $.ajax({
	      type: 'POST',
	      url: url,
	      contentType: "application/x-www-form-urlencoded",
	      data: data,
	      cache: false
	  }).done(function (data, textStatus, jqXHR) {
	      var contentApi = $(".foundation-content").adaptTo("foundation-content");
	      if (jqXHR.responseJSON.result.indexOf("ERROR")>=0) {
	          alert(jqXHR.responseJSON.result);
	      } else {
		        var bodyHtml ="<span>Share Folder " + jqXHR.responseJSON.folder + " created. Please share with external retouchers</span>";
				var dialog = new Coral.Dialog().set({
				  id: 'buyDialog',
				  header: {
				    innerHTML: 'Brief set for external retouch'
				  },
				  content: {
				    innerHTML: bodyHtml
				  },
				  footer: {
				    innerHTML: '<button id="acceptButton" is="coral-button" variant="primary">Accept</button>'
				  }
				});
				document.body.appendChild(dialog);
				dialog.on('coral-overlay:close', function (event) {
					  });
				dialog.on('click', '#acceptButton', function() {
				  dialog.hide();
			      setTimeout(refreshPage(contentApi), 2000);

				});
				dialog.show();
	      }
	    }).fail(function (jqXHR, textStatus, errorThrown) {
	      //show the error
	      alert("Tasks can not be completed. ");
	  });
    }

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
