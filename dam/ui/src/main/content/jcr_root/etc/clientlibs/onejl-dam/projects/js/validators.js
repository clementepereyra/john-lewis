/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2015 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
(function(window, $, Granite) {
    "use strict";
    
    function getTargetProjectPath() {

    }

    var projectSubmit = '#project-submit';
    $(document).on("click", projectSubmit, function (e) {
    	e.preventDefault()
    	var deliveryDate = new Date($('#deliveryDate').val()).getTime();
    	var projectName = $('#projectName').val();

        var data = 'deliveryDate='+ deliveryDate + "&projectName="+ projectName;
		$.ajax({
            type: 'POST',
            url: '/bin/jlp/public/setupProjectFolder',
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
        	var finalPath = jqXHR.responseJSON.result;
        	if (finalPath.indexOf('ERROR')<0) {
                var parentPathInput = $('#parentPath');
        		parentPathInput.val(finalPath);
                var form = $(projectSubmit);
        		form.submit();
        	} else {
        		alert(finalPath);
        	}
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("ERROR: Can't find a path for the current brief");
        });

    });
    
   JLDAM.validcsv = false;
  
  $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
      selector: "[data-foundation-validation~='jldam.valid.csv'],[data-validation~='jldam.valid.csv']",
      validate: function(el) {
          if (!JLDAM.validcsv) {
              return Granite.I18n.get('CSV File is not valid');
          }
      }
  });
    
//    var existentProjects = new Array();
//    
//    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
//        selector: "[data-foundation-validation~='foundation.unique.name'],[data-validation~='foundation.unique.name']",
//        validate: function(el) {
//            var v = el.value.toLowerCase().replace(" ", "-");
//
//            if ($.inArray(v, existentProjects) >=0) {
//                return Granite.I18n.get('Duplicated name - There is an existent project with the same name');
//            }
//        }
//    });
//    
//    function getWeekNumber(d) {
//        // Copy date so don't modify original
//        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
//        // Set to nearest Thursday: current date + 4 - current day number
//        // Make Sunday's day number 7
//        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
//        // Get first day of year
//        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
//        // Calculate full weeks to nearest Thursday
//        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
//        // Return array of year and week number
//        return [d.getUTCFullYear(), weekNo];
//    }
//    
//    function reloadExistentProjects(deliveryDate) {
//    	existentProjects = new Array();
//    	var year = deliveryDate.getFullYear();
//    	var week = getWeekNumber(deliveryDate);
//    	var quarter = Math.floor((deliveryDate.getMonth() + 3) / 3);
//    	var path = '/content/projects/' + year + "/Q" + quarter + "/Week_" + week;
//    	$.get(path + ".1.json", function( data ) {
//            var i =0;
//            for (var key in data) {
//                if (data.hasOwnProperty(key)) {
//                    existentProjects[i] = key.toLowerCase();
//                    i++;
//                }
//            }
//        });
//    }
//    
//    
//    $(window).on("change", "#deliveryDate", function (e) {
//    	reloadExistentProjects(new Date($('#deliveryDate').val()));
//    });
//
//    var path = window.location.pathname;
//    if (path.indexOf('/newproject.html')>=0) {
//        path = path.substring(path.indexOf(".html")+5);
//        $.get(path + ".1.json", function( data ) {
//            var i =0;
//            for (var key in data) {
//                if (data.hasOwnProperty(key)) {
//                    existentProjects[i] = key.toLowerCase();
//                    i++;
//                }
//            }
//        });
//    } else if (path.indexOf('/mnt/overlay/cq/core/content/projects/properties.html')>=0) {
//    	var url_string = window.location.href
//    	var url = new URL(url_string);
//    	var item = url.searchParams.get("item");
//    	var currentResource = item.substring(item.lastIndexOf("/"));
//    	path = item.replace(currentResource, "");
//    	currentResource = currentResource.replace("/", "");
//        $.get(path + ".1.json", function( data ) {
//            var i =0;
//            for (var key in data) {
//                if (data.hasOwnProperty(key) && key.toLowerCase() != currentResource.toLowerCase()) {
//                    existentProjects[i] = key.toLowerCase();
//                    i++;
//                }
//            }
//        });
//    }

})(window, Granite.$, Granite);


(function ($, Granite, undefined) {
    "use strict";

    var requiredString;

    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-duedate.coral-InputGroup',
        validate: validateDeliveryDate
    });

    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-shootenddate.coral-InputGroup',
        validate: validateShootDate
    });

    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-shootstartdate.coral-InputGroup',
        validate: validateShootDate
    });
    
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-reduedate.coral-InputGroup',
        validate: validateReDeliveryDate
    });
    
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-reshootenddate.coral-InputGroup',
        validate: validateReShootDate
    });

    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: '.project-reshootstartdate.coral-InputGroup',
        validate: validateReShootDate
    });
       
    function validateReDeliveryDate(el) {
        var setDateTime = el.valueAsDate;
        
        var setReShootStartDate = getDateFromCollection($('.project-reshootstartdate.coral-InputGroup'));
        var setReShootDueDate = getDateFromCollection($('.project-reshootenddate.coral-InputGroup'));

        //date is not mandatory so return no error if it is null
        if(setDateTime == null){
            if (setReShootStartDate != null)
                setTimeout($('input[name="project.reshootstartDate"]').change(), 250);
            if (setReShootDueDate != null) 
                setTimeout($('input[name="project.reshootendDate"]').change(), 250);   
            return "";	
        }
        
        var setDueDate = getDateFromCollection($('.project-duedate.coral-InputGroup'));
        var setDateObj = new Date(setDateTime);
        
        if (setDueDate == null) {
	    		errorDisableSubmit();
	        return Granite.I18n.get("Delivery Date should be set");
	    } else if (setDueDate > setDateObj) {
	        errorDisableSubmit();
	        return Granite.I18n.get("Rescheduled Delivery date must be newer than Delivery Date");
	    }

        var yesterdayDateObj = new Date();
        yesterdayDateObj.setDate(yesterdayDateObj.getDate()-1);       

        // Validate Date is greater than today
        if(setDateObj <= yesterdayDateObj) {
            errorDisableSubmit();
            return Granite.I18n.get("Rescheduled Delivery Date cannot be in the past");
        }
        
        if (isweekendday(setDateObj)) {
            errorDisableSubmit();
            return Granite.I18n.get("Rescheduled Delivery Date cannot be in weekend");
        }

        if (setReShootStartDate != null)
            setTimeout($('input[name="project.reshootstartDate"]').change(), 250);
        if (setReShootDueDate != null) 
            setTimeout($('input[name="project.reshootendDate"]').change(), 250);      
        // wait for the ui to remove error icons before checking if there are any still displaying
        setTimeout(dueDateValidEnableSubmit, 250);
        return "";
    }
    
    function validateReShootDate(el) {
    		var setDateTime = el.valueAsDate;

        //date is not mandatory so return no error if it is null
        if(setDateTime == null)
            return "";

        var setDateObj = new Date(setDateTime);

        var yesterdayDateObj = new Date();
        yesterdayDateObj.setDate(yesterdayDateObj.getDate()-1);
        
        var setStartProjectDate = getDateFromCollection($('.project-startdate.coral-InputGroup'));
        var setReDeliveryDate = getDateFromCollection($('.project-reduedate.coral-InputGroup'));

        if (el.name == 'project.reshootstartDate') {  // Do validation against End Date
            var setReDueDate = getDateFromCollection($('.project-reshootenddate.coral-InputGroup'));
            
            if (setReDeliveryDate == null) {
            		errorDisableSubmit();
            	    return Granite.I18n.get("Re Shoot Start Date cannot be entered if Rescheduled Delivery date is not filled in");
            } else {
            		if (setDateObj > setReDeliveryDate) {
                    errorDisableSubmit();
                    return Granite.I18n.get("Re Shoot Start Date cannot be after the Rescheduled Delivery Date");
                }         	
            }
            
            if (setStartProjectDate != null && setDateObj < setStartProjectDate) {
	        		errorDisableSubmit();
	        	    return Granite.I18n.get("Re Shoot Start Date cannot be before the Project Start Date");
            }
            
            // Validate Date is greater than today
            if(setDateObj <= yesterdayDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot Start Date cannot be in the past");
            }
            
            if (isweekendday(setDateObj)) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot Start Date cannot be in weekend");
            }
            
            if (setReDueDate == null) {
                // due date is not mandatory so return no error if it is null
                return "";
            }            

            var setReDueDateObj = new Date(setReDueDate);
            if (setDateObj > setReDueDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot Start Date must precede the Re Shoot End Date");
            }
            // wait for the ui to remove error icons before checking if there are any still displaying
            setTimeout(reShootStartDateValidEnableSubmit, 250);
        }
        else { // it's Re the due date
            
            if (setReDeliveryDate == null) {
                // due date is not mandatory so return no error if it is null
            		errorDisableSubmit();
            	    return Granite.I18n.get("Re Shoot End Date cannot be entered if Rescheduled Delivery date is not filled in");
            } else {
	            	if (setDateObj > setReDeliveryDate) {
	            		errorDisableSubmit();
	            		return Granite.I18n.get("Re Shoot End Date cannot be after the Rescheduled Delivery Date");
	            	}

	        		if (islaboraldaydifflessthantwo(setDateObj,setReDeliveryDate)) {
	                errorDisableSubmit();
	                return Granite.I18n.get("Re Shoot End Date must be less than two working days before the Rescheduled Delivery Date");
	            }         	
	        }
        	
            if (setStartProjectDate != null && setDateObj < setStartProjectDate) {
        			errorDisableSubmit();
        			return Granite.I18n.get("Re Shoot End Date cannot be before the Project Start Date");
            }
        
            // Validate Date is greater than today
            if(setDateObj <= yesterdayDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot End Date cannot be in the past");
            }
            
            if (isweekendday(setDateObj)) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot End Date cannot be in weekend");
            }

            var setReStartDate = getDateFromCollection($('.project-reshootstartdate.coral-InputGroup'));
            if (setReStartDate == null) {
                // start date is not mandatory so return no error if it is null
                return "";
            }

            var setReStartDateObj = new Date(setReStartDate);
            if (setDateObj < setReStartDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Re Shoot End Date must be newer than Re Shoot Start Date");
            }

            // wait for the ui to remove error icons before checking if there are any still displaying
            setTimeout(reShootDueDateValidEnableSubmit, 250);
        }
        return "";
    }
    
    function validateDeliveryDate(el) {
        var setDateTime = el.valueAsDate;
        
        var setShootStartDate = getDateFromCollection($('.project-shootstartdate.coral-InputGroup'));
        var setShootDueDate = getDateFromCollection($('.project-shootenddate.coral-InputGroup'));
        
        var setReShootDeliveryDate = getDateFromCollection($('.project-reduedate.coral-InputGroup'));

        //date is not mandatory so return no error if it is null
        if(setDateTime == null){
            if (setShootStartDate != null)
                setTimeout($('input[name="project.shootstartDate"]').change(), 250);
            if (setShootDueDate != null) 
                setTimeout($('input[name="project.shootendDate"]').change(), 250);   
            return "";	
        }

        var setDateObj = new Date(setDateTime);

        var yesterdayDateObj = new Date();
        yesterdayDateObj.setDate(yesterdayDateObj.getDate()-1);       

        // Validate Date is greater than today
        if(setDateObj <= yesterdayDateObj) {
            errorDisableSubmit();
            return Granite.I18n.get("Delivery Date cannot be in the past");
        }
        
        if (isweekendday(setDateObj)) {
            errorDisableSubmit();
            return Granite.I18n.get("Delivery Date cannot be in weekend");
        }

        if (setShootStartDate != null)
            setTimeout($('input[name="project.shootstartDate"]').change(), 250);
        if (setShootDueDate != null) 
            setTimeout($('input[name="project.shootendDate"]').change(), 250);    
        if (setReShootDeliveryDate != null) 
            setTimeout($('input[name="project.redueDate"]').change(), 250);         
        
        // wait for the ui to remove error icons before checking if there are any still displaying
        setTimeout(dueDateValidEnableSubmit, 250);
        return "";
    }

    function validateShootDate(el) {
        var setDateTime = el.valueAsDate;

        //date is not mandatory so return no error if it is null
        if(setDateTime == null)
            return "";

        var setDateObj = new Date(setDateTime);

        var yesterdayDateObj = new Date();
        yesterdayDateObj.setDate(yesterdayDateObj.getDate()-1);
        
        var setStartProjectDate = getDateFromCollection($('.project-startdate.coral-InputGroup'));
        var setDeliveryDate = getDateFromCollection($('.project-duedate.coral-InputGroup'));

        if (el.name == 'project.shootstartDate' || el.name == 'taskShootStartDate') {  // Do validation against End Date
            var setDueDate = getDateFromCollection($('.project-shootenddate.coral-InputGroup'));
            
            if (setDeliveryDate == null) {
            		errorDisableSubmit();
            	    return Granite.I18n.get("Shoot Start Date cannot be entered if Delivery date is not filled in");
            } else {
            		if (setDateObj > setDeliveryDate) {
                    errorDisableSubmit();
                    return Granite.I18n.get("Shoot Start Date cannot be after the Project Delivery Date");
                }         	
            }
            
            if (setStartProjectDate != null && setDateObj < setStartProjectDate) {
	        		errorDisableSubmit();
	        	    return Granite.I18n.get("Shoot Start Date cannot be before the Project Start Date");
            }
            
            // Validate Date is greater than today
            if(setDateObj <= yesterdayDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot Start Date cannot be in the past");
            }
            
            if (isweekendday(setDateObj)) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot Start Date cannot be in weekend");
            }
            
            if (setDueDate == null) {
                // due date is not mandatory so return no error if it is null
                return "";
            }            

            var setDueDateObj = new Date(setDueDate);
            if (setDateObj > setDueDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot Start Date must precede the Shoot End Date");
            }
            // wait for the ui to remove error icons before checking if there are any still displaying
            setTimeout(shootStartDateValidEnableSubmit, 250);
        }
        else { // it's the due date
            
            if (setDeliveryDate == null) {
                // due date is not mandatory so return no error if it is null
            		errorDisableSubmit();
            	    return Granite.I18n.get("Shoot End Date cannot be entered if Delivery date is not filled in");
            } else {
	            	if (setDateObj > setDeliveryDate) {
	            		errorDisableSubmit();
	            		return Granite.I18n.get("Shoot End Date cannot be after the Project Delivery Date");
	            	}

	        		if (islaboraldaydifflessthantwo(setDateObj,setDeliveryDate)) {
	                errorDisableSubmit();
	                return Granite.I18n.get("Shoot End Date must be less than two working days before the Project Delivery Date");
	            }         	
	        }
        	
            if (setStartProjectDate != null && setDateObj < setStartProjectDate) {
        			errorDisableSubmit();
        			return Granite.I18n.get("Shoot End cannot be before the Project Start Date");
            }
        
            // Validate Date is greater than today
            if(setDateObj <= yesterdayDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot End Date cannot be in the past");
            }
            
            if (isweekendday(setDateObj)) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot End Date cannot be in weekend");
            }

            var setStartDate = getDateFromCollection($('.project-shootstartdate.coral-InputGroup'));
            if (setStartDate == null) {
                // start date is not mandatory so return no error if it is null
                return "";
            }

            var setStartDateObj = new Date(setStartDate);
            if (setDateObj < setStartDateObj) {
                errorDisableSubmit();
                return Granite.I18n.get("Shoot End Date must be newer than Shoot Start Date");
            }

            // wait for the ui to remove error icons before checking if there are any still displaying
            setTimeout(shootDueDateValidEnableSubmit, 250);
        }
        return "";
    }
    
    function isweekendday(evDay) {
	    	if (evDay.getDay() == 0 ||evDay.getDay() == 6) 
	    		return true;
	    	else
	    		false;
    }
       
    //get number of weekday between second and first
    function islaboraldaydifflessthantwo(first, second) {
    		var daydiff = Math.round((second-first)/(1000*60*60*24));
    		if (daydiff >= 4) {
    			return false;
    		} else {
    			return (daydiff - countweekenddates(first, second)) < 2;
    		}
    }
    
    function countweekenddates(first, second) {
    		var count = 0;
    	   	var loopDate = new Date(first);
    		while (loopDate < second) {          
    			if (loopDate.getDay() == 0 ||loopDate.getDay() == 6) {
    				count++;
    			}
    	       var newDate = loopDate.setDate(loopDate.getDate() + 1);
    	       loopDate = new Date(newDate);
    	    }
    		return count;
    }
    
    function getDateFromCollection(collection) {
        if (collection.length > 0) {
            return collection[0].valueAsDate;
        } else {
            return null;
        }
    }

    function errorDisableSubmit() {
        $('#shell-propertiespage-saveactivator').prop('disabled', true);
        $('.betty-ActionBar-item.granite-ActionGroup').prop('disabled', true)
    }

    function noErrorEnableSubmit() {
        if ($('.coral-Icon--alert').length == 0) {
            $('#shell-propertiespage-saveactivator').prop('disabled', false);
            $('.betty-ActionBar-item.granite-ActionGroup').prop('disabled', false);
            return true;
        } else {
            return false;
        }
    }
    
    function reShootStartDateValidEnableSubmit() {
        if (!noErrorEnableSubmit()) {
            if ($('.project-reshootenddate + .coral-Icon--alert').length > 0) {
                $('input[name="project.reshootendDate"]').change();
            }          
        }
   }

    function reShootDueDateValidEnableSubmit() {
        if (!noErrorEnableSubmit()) {
            if ($('.project-reshootstartdate + .coral-Icon--alert').length > 0) {
                $('input[name="project.reshootstartDate"]').change();
            }           
        }
    }
    
    function dueDateValidEnableSubmit() {
        if (!noErrorEnableSubmit()) {
            if ($('.project-startdate + .coral-Icon--alert').length > 0) {
                // there is currently an alert icon next to the start date input box, and the current
                // due date change might resolve that error, but we need to trigger another change
                // on the start date in order to invoke the validator that will remove the error status
                $('input[name="project.startDate"]').change();
            }          
        }
    }

    function shootStartDateValidEnableSubmit() {
        if (!noErrorEnableSubmit()) {
            if ($('.project-shootenddate + .coral-Icon--alert').length > 0) {
                // there is currently an alert icon next to the due date input box, and the current
                // start date change might resolve that error, but we need to trigger another change
                // on the due date in order to invoke the validator that will remove the error status
                $('input[name="project.shootendDate"]').change();
            }          
        }
   }

    function shootDueDateValidEnableSubmit() {
        if (!noErrorEnableSubmit()) {
            if ($('.project-shootstartdate + .coral-Icon--alert').length > 0) {
                // there is currently an alert icon next to the start date input box, and the current
                // due date change might resolve that error, but we need to trigger another change
                // on the start date in order to invoke the validator that will remove the error status
                $('input[name="project.shootstartDate"]').change();
            }           
        }
    }


})(Granite.$, Granite);
$(document).ready(function() {

    function getDateFromCollection(collection) {
        if (collection.length > 0) {
            return collection[0].valueAsDate;
        } else {
            return null;
        }
    }
    
    var setDueDate = getDateFromCollection($('.project-duedate.coral-InputGroup'));
    
    if (setDueDate != null) {
    	 	setTimeout(function(){
    	 		if ($('.project-reduedate.coral-InputGroup')[0].valueAsDate == null) {
    	 			$('.project-reduedate.coral-InputGroup')[0].valueAsDate = setDueDate;
    	 		}
 		}
    	 	, 250);
    }
	
}
);
