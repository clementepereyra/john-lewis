/* 
 * CCD-176
 * Activator for add composite folder action. Copied from view properties ootb activator with some custom
 * modifications.
 */
(function(document, $) {
    "use strict";
    
    var addcompositeServletURL = "/bin/jlp/public/reopenForRetouch.json";

    $(document).on("foundation-contentloaded", function(e) {
        var addcompositeActivator = ".foundation-damadmin-reopen-activator";
        if (window.location.href.indexOf('/content/dam/Creative/JohnLewis/Scene7/') > 0) {
        	$(addcompositeActivator).show();
        } else {
        	$(addcompositeActivator).hide();
        }
        $(document).off("click", addcompositeActivator).on("click", addcompositeActivator, function(e) {
            var activator = $(this);
            var paths = "";
            var pathsObject = {};
            var postParamLimit = this.dataset.foundationCollectionAction ? $.parseJSON(this.dataset.foundationCollectionAction).data.postRequestLimit : 1;
            //selections
            var items = $(".foundation-selections-item");
            var ui = $(window).adaptTo("foundation-ui");
            var patharr = [];
            if (ui != undefined) {
                ui.wait();
            }
            
            /* Only checks assets selected in assets view */
            if (items.length) {
                items.each(function(i) {
                    var item = $(this);
                    var itemPath = item.data("foundation-collection-item-id");
                    patharr.push(itemPath);
                    paths += "item=" + encodeURIComponent(itemPath).replace(/%2F/g, "/") + "&";
                    pathsObject[itemPath] = true;
                });

                //remove the last '&'
                paths = paths.substr(0, paths.length - 1);
            }

            var url = Granite.HTTP.externalize(addcompositeServletURL);

            if(url && patharr) {

                    //POST Request
                    var data = {
                        "item": patharr,
                        "_charset_": "utf-8"
                    };
                    post(url, data);
                }


        });
        
        function refreshPage(contentApi){
            if(contentApi){
                contentApi.refresh();
            }else{
                location.reload(true);
            }
        }

        // Post to the provided URL with the specified parameters.
        function post(path, parameters) {
            var form = $('<form></form>');
            $.each(parameters, function(key, value) {
                if ($.isArray(value)){
                    $.each(value, function(keyArray, valueArray){
                            var field = $('<input></input>');
                            field.attr("type", "hidden");
                            field.attr("name", key);
                            field.attr("value", valueArray);
                            form.append(field);
                        }
                    );
                }
                else {
                    var field = $('<input></input>');
                    field.attr("type", "hidden");
                    field.attr("name", key);
                    field.attr("value", value);
                    form.append(field);
                }
            });

            /* Using ajax post instead of regular post as we need to refresh the current page*/
            
            $.ajax({
                type: 'POST',
                url: path,
                contentType: "application/x-www-form-urlencoded",
                data: form.serialize(),
                cache: false
            }).done(function (data, textStatus, jqXHR) {
            	var contentApi = $(".foundation-content").adaptTo("foundation-content");
            	window.open('/assets.html' + encodeURI(jqXHR.responseJSON.result));
                setTimeout(refreshPage(contentApi), 500);
              }).fail(function (jqXHR, textStatus, errorThrown) {
                //show the error
                alert("Error openening asset for retouch");
            });
        }

    });

})(document, Granite.$);
