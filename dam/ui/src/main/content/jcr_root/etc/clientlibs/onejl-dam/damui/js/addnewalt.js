/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#addnewaltform", submit = "#addnewaltform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }
    
    $(document).on("click", submit, function (e) {
        //e.preventDefault();
        
        //var $submit = $(e.target);
        var dataPath, i,
        servletPath = '/bin/jlp/public/AddNewAlt',
        $form = $(form),
        data = $form.serialize(),
        //concat the assets paths to parameters
        items = $(".foundation-collection").find(".foundation-selections-item");
        if (items.length) {
            for (i = 0; i < items.length; i++) {
                dataPath = items[i].dataset.foundationCollectionItemId;
                if (dataPath) {
                    data += '&path=' + dataPath;
                }
            }
        }

        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var contentApi = $(".foundation-content").adaptTo("foundation-content"),
            location = "";
            if ($(jqXHR.responseText).find("#Path").length === 1) {
              location = $(jqXHR.responseText).find("#Path").text();
            }

            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("error adding alts");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });

    

    function reset($form) {
        //reset the form
        $form[0].reset();
    }

}(document, Granite.$));
