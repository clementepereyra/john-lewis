/*jslint devel: true */
/*global Granite*/
(function (document, $) {
    "use strict";

    var activator = ".cq-damadmin-admin-actions-download-activator", form = "#receiveproductform", submit = "#receiveproductform-submit";

    $(document).on("click", activator, function (e) {
        var $form = $(form);
        $form[0].reset();
    });

    $(document).on("click", activator, function (e) {
       $('.coral-Popover').popover("hide");
    });

    function refreshPage(contentApi){
        if(contentApi){
            contentApi.refresh();
        }else{
            location.reload(true);
        }
    }

    $(document).on("click", submit, function (e) {
        var productId, i, productPath,
        servletPath = '/bin/jlp/public/ReceiveProduct',
        $form = $(form),
        data = $form.serialize();
        //Concat the path
        var items = $(".foundation-collection").find(".foundation-selections-item");
        for (i = 0; i < items.length; i++) {
            productPath = items[i].dataset.foundationCollectionItemId;
            if (productPath) {
                data += '&path=' + encodeURIComponent(productPath, "UTF-8");
            }
        }

        $.ajax({
            type: 'POST',
            url: servletPath,
            contentType: "application/x-www-form-urlencoded",
            data: data,
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            setTimeout(refreshPage(contentApi), 2000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            //show the error
            alert("Error receiving product");
        });

         $(this).closest(".coral-Dialog").modal("hide");
         $(this).closest(".coral-Dialog").hide();
    });
    function reset($form) {
        $form[0].reset();
    }

}(document, Granite.$));
