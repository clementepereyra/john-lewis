/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2015 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
var JLPUtil = {};
JLPUtil.Observable = {

    observers: {},

    addObserver: function(event, observer) {
        if(this.observers[event]){
            this.observers[event].push(observer);
        }else{
            this.observers[event] = [observer];
        }
    },

    removeObserver: function(event, observer) {
        var observersArray = this.observers[event],
            index = observersArray.indexOf(observer);

        if (index !== -1) {
            observersArray = observersArray.splice(index, 1);
        }
    },

    /** Notifies observers in the same order they registered */
    notifyObservers: function(event) {
        var observersArray = this.observers[event];
        if(observersArray) {
            var observersArrayLength = observersArray.length;
            for (var iterator = 0; iterator < observersArrayLength; iterator++) {
                observersArray[iterator]();
            }
        }
    }
};

$(document).ready(function() {

	var handleUserPicked = function() {
        var val = $('.user-predicate .js-coral-Autocomplete-hidden')[0].value;
            $('.user-predicate .property-input')[0].value = val;
            if (val.length == 0) {
            	$('.user-predicate .property-input')[0].disabled = true;
                $('.user-predicate .property-predicate-name')[0].disabled = true;
                $('.user-predicate .search-predicate-property')[0].disabled = true;
            } else {
            	$('.user-predicate .property-input')[0].disabled = false;
                $('.user-predicate .property-predicate-name')[0].disabled = false;
                $('.user-predicate .search-predicate-property')[0].disabled = false;
            }

            if (val.trim().length) {
                var $form = $(".granite-omnisearch-form");
            	$form.submit();
            }
    }

	JLPUtil.Observable.addObserver('UserPicked', handleUserPicked);

}
);
(function (document, $) {
    "use strict";

    var $document = $(document);

    
    $document.on('selected', '.user-predicate .js-userpicker-item', function (e) {
        var $this = $(this);
        var val = $this.find('.foundation-layout-util-subtletext').text()
		JLPUtil.Observable.notifyObservers('UserPicked');

    });

})(document, Granite.$);