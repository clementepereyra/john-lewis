 <%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%@page import="org.apache.sling.api.resource.Resource,
                  java.util.HashSet,
                  java.util.Date,
                  java.util.Map,
                  java.util.List,
                  java.util.ArrayList,
                  com.jl.aem.dam.workflow.JLWorkflowsUtils,
                  java.util.HashMap,
                  java.util.concurrent.TimeUnit,
                  com.adobe.granite.comments.CommentCollection,
                  com.adobe.granite.comments.CommentManager"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/init/assetBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/row/common/common.jsp"%><%

Map<String,Integer> freqMap = (HashMap<String,Integer>)request.getAttribute("com.adobe.cq.datasource.freqMap");
int count = 0;
if (freqMap != null) {
    count  = freqMap.containsKey(resource.getPath()) ? freqMap.get(resource.getPath()) : 0;
}

Resource reportNode = resourceResolver.getResource(slingRequest.getRequestPathInfo().getSuffix());
ValueMap reportVM = ResourceUtil.getValueMap(reportNode);
String [] columnsArray = reportVM.get("columns", String [].class);
List<String> columns = new ArrayList<String>();
for(String col:columnsArray){
    columns.add(col.substring(col.lastIndexOf("/")+1));
}

boolean showOriginalIfNoRenditionAvailable = (request!=null && request.getAttribute("showOriginalIfNoRenditionAvailable")!=null) ? (Boolean)request.getAttribute("showOriginalIfNoRenditionAvailable") : false;
boolean showOriginalForGifImages = (request!=null && request.getAttribute("showOriginalForGifImages")!=null) ? (Boolean)request.getAttribute("showOriginalForGifImages") : false;

String thumbnailUrl = request.getContextPath() + requestPrefix 
+ getThumbnailUrl(asset, 48, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
//Override default thumbnail for set when there is manual thumbnail defined
if (dmSetManualThumbnailAsset != null) {
    thumbnailUrl = request.getContextPath() + requestPrefix 
    		+ getThumbnailUrl(asset, 1280, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
}

String assetActionRels = StringUtils.join(getAssetActionRels(hasJcrRead, hasJcrWrite, hasAddChild, canEdit, canAnnotate, isDownloadAllowedForAdmins, isAssetExpired, isSubAssetExpired, isContentFragment, isArchive), " ");

request.setAttribute("actionRels", actionRels.concat(" " + assetActionRels));

attrs.addClass("foundation-collection-navigator");
navigationHref = request.getContextPath() + "/assetdetails.html" + Text.escapePath(resource.getPath());
attrs.add("data-foundation-collection-navigator-href", xssAPI.getValidHref(navigationHref));

attrs.add("is", "coral-table-row");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);
attrs.addClass("foundation-collection-item");
attrs.add("data-foundation-collection-item-id", resource.getPath());

ValueMap resourceProperties = resource.getValueMap();

%><div <%= metaAttrs %>></div>
<tr <%= attrs %>>
    <td is="coral-table-cell" coral-table-rowselect><%
        if (isArchive) {
            %><coral-icon class="foundation-collection-item-thumbnail" icon="fileZip" size="S"></coral-icon><%
        } else {
            %><img class="foundation-collection-item-thumbnail" src="<%= xssAPI.getValidHref(thumbnailUrl) %>" alt=""><%
        }%>
    </td>
    <%for (String columnName : columns) {%>
    <% if( columnName.equals("supplier")) {
        String supplier = resourceProperties.get("jcr:content/metadata/dam:supplierContactDetail", String.class);
        if(supplier == null)
            supplier="";%>
            <td is="coral-table-cell" value="<%= supplier %>"><%= supplier %></td>
    <%} if (columnName.equals("imageid")) {
        String formattedResourceTitle = resourceTitle.indexOf(".") >= 0 ? resourceTitle.substring(0,resourceTitle.indexOf(".")): resourceTitle;
        %>
        
        <td class="foundation-collection-item-title" is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(formattedResourceTitle) %>"><%= xssAPI.encodeForHTML(formattedResourceTitle) %></td>
    <%} if (columnName.equals("producttitle")) {
        String prodTitle = resourceProperties.get("jcr:content/metadata/dam:JLPProductTitle", String.class);
        if (prodTitle == null) {
            prodTitle = "";
        } 
        %>
        
        <td class="foundation-collection-item-title" is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(prodTitle) %>"><%= xssAPI.encodeForHTML(prodTitle) %></td>
    <%} if( columnName.equals("brand")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:brandName", String.class);
        if (propValue == null) {
            propValue ="";
    }%>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("modelnumber")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:ModelNumber", String.class);
        if (propValue == null) {
            propValue ="";
    }%>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("jlstocknumber")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:JLPProductCodes", String.class);
        if (propValue == null) {
            propValue ="";
    }%>
     <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("stocklevel")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:JLPStockLevel", String.class);
        if (propValue == null) {
            propValue ="";
    }%>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("products")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:JLPProductCodes", String.class);
        if (propValue == null) {
            propValue ="";
    }%>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("deleted")) {
        String propValue = resourceProperties.get("jcr:content/metadata/dam:ProductDeleted", String.class);
        if (propValue == null) {
            propValue ="No";
            } else if (propValue.toUpperCase( ).equals("Y")){
                propValue = "Yes";
            } else {
                propValue = "No";
            }%>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(propValue) %>"><%= xssAPI.encodeForHTMLAttr(propValue) %></td>
    <%} if( columnName.equals("activation")) { 
        Calendar dateCo = resourceProperties.get("jcr:content/metadata/dateCompleted", Calendar.class);
        Long dateCoL = (null != dateCo) ? dateCo.getTimeInMillis() : 0;
        String completedDate = outVar(xssAPI, i18n, rtf.format(dateCoL,true));
        if(completedDate == null || dateCoL.equals(new Long(0))) {
            completedDate="";
        }%>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateCoL)) %>"><%
            if (lastModified != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateCoL)) %>"><%= xssAPI.encodeForHTML(completedDate) %></time><%
            }
        %></td>
    <%} if( columnName.equals("type")) { %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(displayMimeType) %>"><%= xssAPI.encodeForHTML(displayMimeType) %></td>
    <%} if( columnName.equals("size")) { %>
        <td is="coral-table-cell" value="<%= bytes %>"><%= size %></td>
    <%} if( columnName.equals("added")) { 
        Calendar date = resourceProperties.get("jcr:created", Calendar.class);
        String createdBy = resourceProperties.get("jcr:createdBy", String.class);
        createdBy = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), createdBy);
        Long addedDate = (null != date) ? date.getTimeInMillis() : 0;
        String created = outVar(xssAPI, i18n, rtf.format(addedDate,true));
        if(created==null) {
            created="";
        }%>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(addedDate)) %>"><%
            if (lastModified != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(addedDate)) %>"><%= xssAPI.encodeForHTML(created) %></time><%

                %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(createdBy) %></div><%
            }
        %></td>
    <%} if( columnName.equals("modified")) { %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%
            if (lastModified != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%= xssAPI.encodeForHTML(lastModified) %></time><%

                %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(lastModifiedBy) %></div><%
            }
        %></td>
	<%} if( columnName.equals("expired")) { 
        Calendar expDate = resourceProperties.get("jcr:content/metadata/prism:expirationDate", Calendar.class);
        String date="";
        Long expireDate=0L,currentDate=new Date().getTime(); 
        if(expDate!=null) {
            expireDate = expDate.getTimeInMillis();
            if(currentDate>expireDate){
                date = outVar(xssAPI, i18n, rtf.format(expireDate,true));
            }
            else {
                date = outVar(xssAPI, i18n, rtf.format(currentDate,expireDate));
                if(date!=""){
                    date=date.concat(" later");
                }
            }
        }%>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(expireDate)) %>"><%
            if (date != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(expireDate)) %>"><%= xssAPI.encodeForHTML(date) %></time><%
            }
        %></td>
	<%} if( columnName.equals("published")) { %>
         <td is="coral-table-cell" value="<%= (!isDeactivated && publishedDate != null) ? xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) : "0" %>"><%
            // Published date and status
            if (!isDeactivated && publishedDate != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) %>"><%= xssAPI.encodeForHTML(publishedDate) %></time><%
            } else {
                %><span><%= xssAPI.encodeForHTML(i18n.get("Not published")) %></span><%
            }

            // Published by
            if (!isDeactivated && publishedBy != null) {
                %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(publishedBy) %></div><%
            }%>

        </td>
	<%} if( columnName.equals("frequency")) { %>
            <td is="coral-table-cell" value="<%= count %>"><%= count %></td>
    <%} if( columnName.equals("path")) { %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resource.getPath()) %>"><%= xssAPI.encodeForHTML(resource.getPath()) %></td>
    <%} if( columnName.equals("supplierissues")) {
        String supplierIssues = resourceProperties.get("jcr:content/metadata/assetValidationFailsCount", String.class);
        if(supplierIssues == null)
            supplierIssues="0";%>
            <td is="coral-table-cell" value="<%= supplierIssues %>"><%= supplierIssues %></td>
    <%} if( columnName.equals("artworkingissues")) {
        String artIssues = resourceProperties.get("jcr:content/metadata/artworkingValidationFailsCount", String.class);
        if(artIssues == null)
            artIssues="0";%>
            <td is="coral-table-cell" value="<%= artIssues %>"><%= artIssues %></td>
    <%} if( columnName.equals("rejected")) { 
            Calendar date = resourceProperties.get("jcr:content/metadata/dateRejected", Calendar.class);
            Long rejectedDate = (null != date) ? date.getTimeInMillis() : 0;
            String rejected = outVar(xssAPI, i18n, rtf.format(rejectedDate,true));
            if(rejected == null || rejectedDate == 0) {
                rejected="";
            }%>
            <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(rejectedDate)) %>">
                <time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(rejectedDate)) %>"><%= xssAPI.encodeForHTML(rejected) %></time>
            </td>
    <%} if( columnName.equals("status")) {
        String statusId = resourceProperties.get("jcr:content/metadata/dam:JLPStatus", String.class);%>
        <td is="coral-table-cell"><%= xssAPI.encodeForHTMLAttr(statusId) %></td>
    <%} if( columnName.equals("comments")) {
        String comment = resourceProperties.get("jcr:content/metadata/dam:JLPComment", String.class);
        if (comment == null) {
            comment ="";
        }%>
        <td is="coral-table-cell" value="<%= comment %>"><%= comment %></td>
    <%} if( columnName.equals("autorejected")) {
        String auto = resourceProperties.get("jcr:content/metadata/rejectedAuto", String.class);
        if (auto == null) {
            auto ="";
        }%>
        <td is="coral-table-cell" value="<%= auto %>"><%= auto %></td>
    <%} if( columnName.equals("outsourcedto")) {
        String outsourcedTo = resourceProperties.get("jcr:content/metadata/dam:JLPOutsourcedTo", String.class);
        if (outsourcedTo == null) {
            outsourcedTo ="";
        }%>
        <td is="coral-table-cell" value="<%= outsourcedTo %>"><%= outsourcedTo %></td>
    <%} if( columnName.equals("directorate")) {
        String directorate = resourceProperties.get("jcr:content/metadata/dam:JLPDirectorate", String.class);
        if (directorate == null) {
            directorate ="";
        }%>
        <td is="coral-table-cell" value="<%= directorate %>"><%= directorate %></td>
    <%} if( columnName.equals("altnumber")) {
        String altNumber = resourceProperties.get("jcr:content/metadata/dam:JLPImageCategory", String.class);
        if (altNumber == null) {
            altNumber ="";
        }
        if (altNumber.endsWith("/")) {
            altNumber = altNumber.substring(0, altNumber.length()-1);
        }
        if (altNumber.contains("/")) {
        	altNumber = altNumber.substring(altNumber.lastIndexOf("/")+1);
        }
        %>
        <td is="coral-table-cell" value="<%= altNumber %>"><%= altNumber %></td>
    <%} if( columnName.equals("source")) {
    String source = resourceProperties.get("jcr:content/metadata/dam:JLPSource", String.class);
    if (source == null) {
        source ="";
    }%>
    <td is="coral-table-cell"><%= xssAPI.encodeForHTMLAttr(source) %></td>
    <%} if( columnName.equals("assetrequested")) { 
            Calendar date = resourceProperties.get("jcr:content/metadata/dateSupplierRequested", Calendar.class);
            Long requestedDate = (null != date) ? date.getTimeInMillis() : 0;
            String requested = outVar(xssAPI, i18n, rtf.format(requestedDate,true));
            if(requested == null || requestedDate == 0) {
                requested="";
            }%>
            <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(requestedDate)) %>">
                <time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(requestedDate)) %>"><%= xssAPI.encodeForHTML(requested) %></time><%
            %></td>
    <%} if( columnName.equals("uploaded")) { 
            Calendar date = resourceProperties.get("jcr:content/metadata/dateSupplierUploaded", Calendar.class);
            Long uploadedDate = (null != date) ? date.getTimeInMillis() : 0;
            String uploaded = outVar(xssAPI, i18n, rtf.format(uploadedDate,true));
            if(uploaded == null || uploadedDate == 0) {
                uploaded="";
            }%>
            <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(uploadedDate)) %>">
                <time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(uploadedDate)) %>"><%= xssAPI.encodeForHTML(uploaded) %></time><%
            %></td>
   <%} if( columnName.equals("available")) { 
        Calendar dateAv = resourceProperties.get("jcr:content/metadata/dateAvailable", Calendar.class);
        String userAv = resourceProperties.get("jcr:content/metadata/userAvailable", String.class);
        userAv = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), userAv);
        Long dateAvL = (null != dateAv) ? dateAv.getTimeInMillis() : 0;
        String availableDate = outVar(xssAPI, i18n, rtf.format(dateAvL,true));
        if(availableDate == null|| userAv == null) {
            availableDate = "";
            userAv = "";
        }%>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateAvL)) %>"><%
            if (lastModified != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateAvL)) %>"><%= xssAPI.encodeForHTML(availableDate) %></time><%

                %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(userAv) %></div><%
            }
        %></td>
     
    
     <%} if( columnName.equals("approvedbyic")) { 
            Calendar date = resourceProperties.get("jcr:content/metadata/dateICApproved", Calendar.class);
            Long icApprovedDate = (null != date) ? date.getTimeInMillis() : 0;
            String icApproved = outVar(xssAPI, i18n, rtf.format(icApprovedDate,true));
            if(icApproved == null || icApprovedDate == 0) {
                icApproved="";
            }%>
            <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(icApprovedDate)) %>">
                <time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(icApprovedDate)) %>"><%= xssAPI.encodeForHTML(icApproved) %></time><%
            %></td>
     <%} if( columnName.equals("providedbyartworker")) { 
            Calendar date = resourceProperties.get("jcr:content/metadata/dateProvidedByArtworker", Calendar.class);
            Long providedDate = (null != date) ? date.getTimeInMillis() : 0;
            String provided = outVar(xssAPI, i18n, rtf.format(providedDate,true));
            if(provided == null || providedDate == 0) {
                provided="";
            }%>
            <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(providedDate)) %>">
                <time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(providedDate)) %>"><%= xssAPI.encodeForHTML(provided) %></time><%
            %></td>
     <%} if( columnName.equals("completed")) { 
        Calendar dateCo = resourceProperties.get("jcr:content/metadata/dateCompleted", Calendar.class);
        String userCo = resourceProperties.get("jcr:content/metadata/userCompleted", String.class);
        userCo = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), userCo);
        Long dateCoL = (null != dateCo) ? dateCo.getTimeInMillis() : 0;
        String completedDate = outVar(xssAPI, i18n, rtf.format(dateCoL,true));
        if(completedDate == null || userCo == null) {
            completedDate="";
            userCo="";
        }%>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateCoL)) %>"><%
            if (lastModified != null) {
                %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(dateCoL)) %>"><%= xssAPI.encodeForHTML(completedDate) %></time><%

                %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(userCo) %></div><%
            }
        %></td>
        
        <%} if( columnName.equals("suppliertime")) { 
        Calendar dateReq = resourceProperties.get("jcr:content/metadata/dateSupplierRequested", Calendar.class);
        Calendar dateUp = resourceProperties.get("jcr:content/metadata/dateSupplierUploaded", Calendar.class);
        
        Long dateReqLong = (null != dateReq) ? dateReq.getTimeInMillis() : 0;
        Long dateUpLong = (null != dateUp) ? dateUp.getTimeInMillis() : 0;
        
        String displayDate = JLWorkflowsUtils.getTimeDifference(dateReqLong, dateUpLong);
        
        Long diff = dateUpLong - dateReqLong;
        %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(diff)) %>">
            <time datetime="<%= xssAPI.encodeForHTMLAttr(displayDate) %>"><%= xssAPI.encodeForHTML(displayDate) %></time>
        </td>
        <%}%>
        <% if( columnName.equals("ictime")) { 
        Calendar dateUp = resourceProperties.get("jcr:content/metadata/dateSupplierUploaded", Calendar.class);
        Calendar dateApp = resourceProperties.get("jcr:content/metadata/dateICApproved", Calendar.class);
        
        Long dateUpLong = (null != dateUp) ? dateUp.getTimeInMillis() : 0;
        Long dateAppLong = (null != dateApp) ? dateApp.getTimeInMillis() : 0;
        
        String displayDate = JLWorkflowsUtils.getTimeDifference(dateUpLong, dateAppLong);
        
        Long diff = dateAppLong - dateUpLong;
        %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(diff)) %>">
            <time datetime="<%= xssAPI.encodeForHTMLAttr(displayDate) %>"><%= xssAPI.encodeForHTML(displayDate) %></time>
        </td>
        <%}%>
        <% if( columnName.equals("artworktime")) { 
        Calendar dateReady = resourceProperties.get("jcr:content/metadata/dateAvailable", Calendar.class);
        Calendar dateUp = resourceProperties.get("jcr:content/metadata/dateProvidedByArtworker", Calendar.class);
        
        Long dateReadyLong = (null != dateReady) ? dateReady.getTimeInMillis() : 0;
        Long dateUpLong = (null != dateUp) ? dateUp.getTimeInMillis() : 0;
        
        String displayDate = JLWorkflowsUtils.getTimeDifference(dateReadyLong, dateUpLong);
        
        Long diff = dateUpLong - dateReadyLong;
        %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(diff)) %>">
            <time datetime="<%= xssAPI.encodeForHTMLAttr(displayDate) %>"><%= xssAPI.encodeForHTML(displayDate) %></time>
        </td>
        <%}%>
        <% if( columnName.equals("finalreviewertime")) { 
        Calendar dateReady = resourceProperties.get("jcr:content/metadata/dateProvidedByArtworker", Calendar.class);
        Calendar dateComp = resourceProperties.get("jcr:content/metadata/dateCompleted", Calendar.class);
        
        Long dateReadyLong = (null != dateReady) ? dateReady.getTimeInMillis() : 0;
        Long dateCompLong = (null != dateComp) ? dateComp.getTimeInMillis() : 0;
        
        String displayDate = JLWorkflowsUtils.getTimeDifference(dateReadyLong, dateCompLong);
        
        Long diff = dateCompLong - dateReadyLong;
        %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(diff)) %>">
            <time datetime="<%= xssAPI.encodeForHTMLAttr(displayDate) %>"><%= xssAPI.encodeForHTML(displayDate) %></time>
        </td>
        <%}%>
        <% if( columnName.equals("completeworkflowtime")) { 
        Calendar dateReq = resourceProperties.get("jcr:content/metadata/dateSupplierRequested", Calendar.class);
        Calendar dateComp = resourceProperties.get("jcr:content/metadata/dateCompleted", Calendar.class);
        
        Long dateReqLong = (null != dateReq) ? dateReq.getTimeInMillis() : 0;
        Long dateCompLong = (null != dateComp) ? dateComp.getTimeInMillis() : 0;
        
        String displayDate = JLWorkflowsUtils.getTimeDifference(dateReqLong, dateCompLong);
        
        Long diff = dateCompLong - dateReqLong;
        %>
        <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(diff)) %>">
            <time datetime="<%= xssAPI.encodeForHTMLAttr(displayDate) %>"><%= xssAPI.encodeForHTML(displayDate) %></time>
        </td>
        <%}%>
        <%} //for %>
</tr>
