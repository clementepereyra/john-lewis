<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%>
<%@page import="org.apache.jackrabbit.util.Text,com.jl.aem.dam.briefs.workflow.BriefAssetStatus,
java.util.Iterator,com.jl.aem.dam.workflow.JLPConstants"%><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/directoryBase.jsp"%><%

String thumbnailUrl = "";

String directoryActionRels = StringUtils.join(getDirectoryActionRels(hasJcrRead, hasModifyAccessControl, hasJcrWrite, hasReplicate, isMACShared, isCCShared, isRootMACShared, isMPShared, isRootMPShared), " ");

request.setAttribute("actionRels", actionRels.concat(" " + directoryActionRels));

     boolean manualThumnailExists = false;
    // Path at which the manual thumbnail(if present) exists
    String manualThumbnailPath = resource.getPath() + "/jcr:content/manualThumbnail.jpg";
    Resource manualThumbnail = resourceResolver.getResource(manualThumbnailPath);
    if (null != manualThumbnail) {
        manualThumnailExists = true;
    }
    
    String escapedResourcePath = Text.escapePath(resourcePath);
    
    if (manualThumnailExists) {
        //UIHelper.getCacheKiller() generates the cache killer based on default thumbnail's last modified time.
        int cck = 600000 + (int)(Math.random() * (600001));
        	thumbnailUrl = requestPrefix + escapedResourcePath + "/jcr:content/manualThumbnail.jpg?ch_ck=" + cck + requestSuffix;
    } else {
        thumbnailUrl = requestPrefix + escapedResourcePath + ".folderthumbnail.jpg?width=280&height=240&ch_ck=" + ck + requestSuffix;
    }

attrs.add("variant", "inverted");
attrs.addClass("foundation-collection-navigator");
%>
<cq:include script="link.jsp"/>
<%
    if (request.getAttribute("com.adobe.directory.card.nav")!=null){
        navigationHref =  (String) request.getAttribute("com.adobe.directory.card.nav");
    //navigationHref = Text.escapePath(navigationHref);
    navigationHref = navigationHref;
    attrs.add("data-foundation-collection-navigator-href", xssAPI.getValidHref(navigationHref));
    }


request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);
request.setAttribute("com.adobe.cq.assets.contentrenderer.directory.profileTitleList", profileTitleList);

%>
<cq:include script = "meta.jsp"/>
<coral-card <%= attrs %>>
    <coral-card-asset class="coral-Card-asset">
       <img src="<%= xssAPI.getValidHref(thumbnailUrl) %>">
    </coral-card-asset>
	<coral-card-content>
        <coral-card-context class="coral-Card-context"><%= xssAPI.encodeForHTML(context) %></coral-card-context>
        <coral-card-title class="foundation-collection-item-title" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>"><%= xssAPI.encodeForHTML(resourceTitle) %></coral-card-title>
        <cq:include script = "propertyList.jsp"/>
    </coral-card-content>
    <%    
    String colorClass = "";
    String xxStatusLabel = null;
    Object statusLabel = resource.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS);
    if(statusLabel != null) {
        BriefAssetStatus briefAssetStatus = BriefAssetStatus.valueOf(statusLabel.toString());
        xxStatusLabel = BriefAssetStatus.valueOf(statusLabel.toString()).getDisplayLabel();
        if (briefAssetStatus == BriefAssetStatus.ASSET_REQUESTED) {
			colorClass = "pendingShoot";
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_UPLOADED) {
            if (resource.getValueMap().get(JLPConstants.RETOUCH_MODE) == null) {
                colorClass = "pendingRetouch";
            } else {
				colorClass = "readyToRetouch";
            }
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_RETOUCHED) {
			colorClass = "assetRetouched";
        } else if (briefAssetStatus == BriefAssetStatus.ALT_NOT_NEEDED) {
			colorClass = "altNotNeeded";
        } else if (briefAssetStatus == BriefAssetStatus.RE_SHOOT_NEEDED) {
			colorClass = "pendingReShoot";
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_DELIVERED) {
			colorClass = "productDelivered";
        } else if (briefAssetStatus == BriefAssetStatus.PENDING_COLOUR_MATCH) {
			colorClass = "pendingColourMatch";
        }
    } else {
        if (resource.getValueMap().get("stockNumber") != null || resource.getValueMap().get("dam:JLPLookName") != null) {
            Iterator<Resource> children = resource.getChildren().iterator();
            boolean allDone = true;
            boolean allDelivered =  true;
            boolean allReadyForRetouch = true;
            boolean allPendingRetouch = true;
            boolean allPendingColourMatch = true;
            
            while (children.hasNext()) {
                Resource child = children.next();
                Object statusLabelChild = child.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS);
                if (statusLabelChild != null) {
                    BriefAssetStatus briefAssetStatus = BriefAssetStatus.valueOf(statusLabelChild.toString());
                    if (briefAssetStatus != BriefAssetStatus.ASSET_DELIVERED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allDelivered = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allDone = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.ASSET_UPLOADED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allReadyForRetouch = false;
                        allPendingRetouch = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.PENDING_COLOUR_MATCH && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allPendingColourMatch = false;
                    }
                    if (briefAssetStatus == BriefAssetStatus.ASSET_UPLOADED) {
                        if (child.getValueMap().get(JLPConstants.RETOUCH_MODE) == null) {
                            allReadyForRetouch = false;
                        }
                    }
                }
            }
            if (allDelivered || resource.getValueMap().get("dam:JLPDelivered") != null) {
                xxStatusLabel = "Product Delivered";
                colorClass = "productDelivered";
            } else if (allDone) {
    			colorClass = "assetRetouched";
                xxStatusLabel =  "Product Retouched";
            } else if (allReadyForRetouch) {
                colorClass = "readyToRetouch";
                xxStatusLabel = "Product Ready To Retouch";
            } else if (allPendingRetouch) {
                colorClass = "pendingRetouch";
                xxStatusLabel = "Product Pending Retouch";
            } else if (allPendingColourMatch) {
                colorClass = "pendingColourMatch";
                xxStatusLabel = "Product Pending Colour Match";
            }
        } else if (resource.getValueMap().get("projectPath") != null) {
            boolean allDelivered =  true;
            Iterator<Resource> children = resource.getChildren().iterator();
            int length = 0;
            while (children.hasNext()) {
                Resource child = children.next();
                if (child.getValueMap().get("stockNumber") != null || child.getValueMap().get("dam:JLPLookName") != null ) {
                    length++;
                    if (child.getValueMap().get("dam:JLPDelivered") == null) {
                        allDelivered = false;
                    }
                } 
                
            }
            if (allDelivered && length >=1) {
                colorClass = "productDelivered";
            }
        }
    }
	if(xxStatusLabel != null) {      
    %> <coral-card-info>
			<coral-alert class="<%=colorClass%>">
    			<coral-alert-content ><%= xxStatusLabel%></coral-alert-content>
			</coral-alert>
    	</coral-card-info>
    <%}%>
    <cq:include script = "applicableRelationships.jsp"/>
</coral-card>
<cq:include script = "quickActions.jsp"/>
<%!

%>