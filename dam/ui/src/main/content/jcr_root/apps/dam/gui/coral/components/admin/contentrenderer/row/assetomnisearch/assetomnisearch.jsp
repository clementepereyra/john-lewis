 <%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%@page import="org.apache.sling.api.resource.Resource"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/assetBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/insightBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/row/common/common.jsp"%><%

boolean showOriginalIfNoRenditionAvailable = (request!=null && request.getAttribute("showOriginalIfNoRenditionAvailable")!=null) ? (Boolean)request.getAttribute("showOriginalIfNoRenditionAvailable") : false;
boolean showOriginalForGifImages = (request!=null && request.getAttribute("showOriginalForGifImages")!=null) ? (Boolean)request.getAttribute("showOriginalForGifImages") : false;

String thumbnailUrl = request.getContextPath() + requestPrefix 
+ getThumbnailUrl(asset, 48, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
//Override default thumbnail for set when there is manual thumbnail defined
if (dmSetManualThumbnailAsset != null) {
    thumbnailUrl = request.getContextPath() + requestPrefix 
    		+ getThumbnailUrl(asset, 1280, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
} else if (isContentFragment && (asset != null)) {
    thumbnailUrl = getFragmentThumbnailUrl(asset);
    resourceTitle = xssAPI.encodeForHTML(getFragmentTitle(asset.adaptTo(Resource.class), resourceTitle));
}

String assetActionRels = StringUtils.join(getAssetActionRels(hasJcrRead, hasJcrWrite, hasAddChild, canEdit, canAnnotate, isDownloadAllowedForAdmins, isAssetExpired, isSubAssetExpired, isContentFragment, isArchive), " ");
String rels = actionRels.concat(" " + assetActionRels);
if (isContentFragment) {
    // hacks to get the correct actionbar for Content Fragments
    rels = rels.replaceAll("aem-assets-admin-actions-edit-activator", "");
    rels = rels.replaceAll("foundation-damadmin-properties-activator", "");
    rels = rels.replaceAll("cq-damadmin-admin-actions-share-activator", "");
    rels = rels.replaceAll("cq-damadmin-admin-actions-mpshare-activator", "");
    rels = rels.replaceAll("cq-damadmin-admin-actions-mpunshare-activator", "");
    rels = rels.replaceAll("cq-damadmin-admin-actions-adhocassetshare-activator", "");
    rels += " aem-assets-admin-actions-edit-fragment-activator";
    rels += " foundation-damadmin-fragmentprops-activator";
}
request.setAttribute("actionRels", rels);

attrs.addClass("foundation-collection-navigator");
%>
<cq:include script="link.jsp"/>
<%
    if (request.getAttribute("com.adobe.assets.card.nav")!=null){
        navigationHref =  (String) request.getAttribute("com.adobe.assets.card.nav");
    }
attrs.add("data-foundation-collection-navigator-href", xssAPI.getValidHref(navigationHref));

attrs.add("is", "coral-table-row");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);

ValueMap resourceProperties = resource.getValueMap();
WorkflowStatus workflowStatus = resource.adaptTo(WorkflowStatus.class);

request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);

%>
<tr <%= attrs %>>
    <td is="coral-table-cell" coral-table-rowselect>
        <img class="foundation-collection-item-thumbnail" src="<%= xssAPI.getValidHref(thumbnailUrl) %>" alt="">
    </td>
    <td class="foundation-collection-item-title" is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>"><%= xssAPI.encodeForHTML(resourceTitle) %>
        <cq:include script = "status.jsp"/>
    </td>

    <cq:include script = "status-lister.jsp"/>
    <td is="coral-table-cell" value="0"></td> <!--Adding a placeholder column for encodingStatus -->
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(displayMimeType) %>"><%= xssAPI.encodeForHTML(displayMimeType) %></td>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resolution) %>"><%= xssAPI.encodeForHTML(resolution)%></td>
    <td is="coral-table-cell" value="<%= bytes %>"><%= size %></td>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%
        if (lastModified != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%= xssAPI.encodeForHTML(lastModified) %></time><%

            // Modified-after-publish indicator
                if (publishDateInMillis > 0 && publishDateInMillis < assetLastModification) {
                String modifiedAfterPublishStatus = i18n.get("Modified since last publication");
                %><coral-icon icon="alert" size="XS" title="<%= xssAPI.encodeForHTMLAttr(modifiedAfterPublishStatus) %>"></coral-icon><%
            }

            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(lastModifiedBy) %></div><%
        }
    %></td>
     <td is="coral-table-cell" value="<%= (!isDeactivated && publishedDate != null) ? xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) : "0" %>"><%
        // Published date and status
        if (!isDeactivated && publishedDate != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) %>"><%= xssAPI.encodeForHTML(publishedDate) %></time><%
        } else {
            %><span><%= xssAPI.encodeForHTML(i18n.get("Not published")) %></span><%
        }

        // Publication/un-publication pending indicator
        String publicationPendingStatus = getPublicationPendingStatus(replicationStatus, i18n);
        if (publicationPendingStatus.length() > 0) {
            %><coral-icon icon="pending" size="XS" title="<%= xssAPI.encodeForHTMLAttr(publicationPendingStatus) %>"></coral-icon><%
        }

        // On/Off time indicator
        String onOffTimeStatus = getOnOffTimeStatus(resourceProperties, i18n, request.getLocale());
        if (onOffTimeStatus.length() > 0) {
            %><coral-icon icon="clock" size="XS" title="<%= xssAPI.encodeForHTMLAttr(onOffTimeStatus) %>"></coral-icon><%
        }

        // Publication/un-publication scheduled indicator
        List<Workflow> scheduledWorkflows = getScheduledWorkflows(workflowStatus);
        if (!isDeactivated && scheduledWorkflows.size() > 0) {
            String scheduledStatus = getScheduledStatus(scheduledWorkflows, i18n, request.getLocale(), resourceResolver);
            if (scheduledStatus.length() > 0) {
                %><coral-icon icon="calendar" size="XS" title="<%= xssAPI.encodeForHTMLAttr(scheduledStatus) %>"></coral-icon><%
            }
        }

        // Published by
        if (!isDeactivated && publishedBy != null) {
            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(publishedBy) %></div><%
        }

        %><cq:include script = "applicableRelationships.jsp"/>
    </td>
    <td is="coral-table-cell" value="<%= isCheckedOut ? "1" : "0" %>">
        <%
            // Checkout Status
            if (isCheckedOut) {
                %><coral-icon icon="lockOn" size="XS" title="<%= xssAPI.encodeForHTMLAttr(i18n.get("Checked Out By {0}", "", checkedOutByFormatted)) %>"><%
            }
        %>
    </td>
    <td is="coral-table-cell" value="<%= commentsCount %>"><%= commentsCount %></td>
     <td is="coral-table-cell" value=""></td>
    <cq:include script = "meta.jsp"/>
</tr><%!

     private String getFragmentThumbnailUrl(Asset asset) {
         String thumbnailUrl;
         Resource resource = asset.adaptTo(Resource.class);
         Resource thumbnailRsc = resource.getChild("jcr:content/thumbnail.png");
         if (thumbnailRsc != null) {
             // use the existing thumbnail
             Calendar createdCal = thumbnailRsc.getValueMap()
                     .get(JcrConstants.JCR_CREATED, Calendar.class);
             Resource contentResource = thumbnailRsc.getChild(JcrConstants.JCR_CONTENT);
             Calendar lastModifiedCal = createdCal;
             if (contentResource != null) {
                 lastModifiedCal = contentResource.getValueMap()
                         .get(JcrConstants.JCR_LASTMODIFIED, createdCal);
             }
             long lastModified =
                     (lastModifiedCal != null ? lastModifiedCal.getTimeInMillis() : -1);
             thumbnailUrl = Text.escapePath(thumbnailRsc.getPath()) + "?_ck=" + lastModified;
         } else {
             // default thumbnail
             thumbnailUrl = Text.escapePath("/libs/dam/cfm/admin/content/static/thumbnail_fragment.png");
         }
         return thumbnailUrl;
     }

     private String getFragmentTitle(Resource resource, String defaultTitle) {
         Resource contentResource = resource.getChild(JcrConstants.JCR_CONTENT);
         ValueMap props = contentResource.getValueMap();
         return props.get("jcr:title", defaultTitle);
     }

 %>
