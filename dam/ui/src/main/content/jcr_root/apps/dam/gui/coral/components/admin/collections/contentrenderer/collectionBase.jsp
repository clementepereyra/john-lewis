<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%@page import="java.awt.CardLayout"%>
<%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page import="org.apache.sling.api.resource.Resource,
				  javax.jcr.Node,
                  javax.jcr.Session,
				  javax.jcr.RepositoryException,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  java.util.Calendar,
                  java.util.ResourceBundle,
				  com.day.cq.search.Query,
				  com.day.cq.search.QueryBuilder,
				  com.day.cq.search.PredicateGroup,
				  com.day.cq.search.result.SearchResult,
                  org.apache.commons.lang.StringUtils,
                  org.apache.jackrabbit.api.security.user.Authorizable,
                  org.apache.jackrabbit.util.Text,
                  org.apache.sling.resource.collection.ResourceCollection,
                  com.adobe.granite.security.user.UserManagementService,
                  com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.security.user.util.AuthorizableUtil,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  com.day.cq.dam.commons.util.DamConfigurationConstants,
                  com.day.cq.dam.commons.util.DamUtil,
                  com.day.cq.dam.commons.util.UIHelper"%><%
    ExpressionHelper ex = cmp.getExpressionHelper();
    AccessControlManager acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();

    String actionRels =
        "cq-damadmin-admin-actions-viewproperties-activator" +
        " cq-damadmin-admin-actions-download-activator" +
        " cq-damadmin-admin-actions-createworkflow" +
        " cq-damadmin-admin-actions-removefromcollection ";

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();

    final String COLLECTION_DETAILS_VANITY = "/mnt/overlay/dam/gui/content/collections/collectiondetails.html";

    boolean showQuickActions = true;
    Object quickActionsAttr = request.getAttribute("com.adobe.cq.item.quickActions");
    if (quickActionsAttr != null) {
        if (quickActionsAttr.getClass().getName().equals("java.lang.String")) {
            showQuickActions =  ((String)quickActionsAttr).equals("true");
        } else {
            showQuickActions = ((Boolean)quickActionsAttr).booleanValue();
        }
    }

    boolean isLinkSharePage = false;
    Object isLinkSharePageAttr = request.getAttribute("isLinkSharePage");
    if (isLinkSharePageAttr != null) {
        if (isLinkSharePageAttr.getClass().getName().equals("java.lang.String")) {
            isLinkSharePage =  ((String)isLinkSharePageAttr).equals("true");
        } else {
            isLinkSharePage = ((Boolean)isLinkSharePageAttr).booleanValue();
        }
    }

    // calculate request suffx and prefix for link share page
    String requestSuffix = "";
    String requestPrefix = "";

    if (isLinkSharePage) {
        Object requestSuffixAttr = request.getAttribute("com.adobe.cq.item.requestSuffix");
        if (requestSuffixAttr != null) {
            if (requestSuffixAttr.getClass().getName().equals("java.lang.String")) {
                requestSuffix =  (String)requestSuffixAttr;
                requestSuffix = ex.get(requestSuffix, String.class);
            }
        }

        Object requestPrefixAttr = request.getAttribute("com.adobe.cq.item.requestPrefix");
        if (requestPrefixAttr != null) {
            if (requestPrefixAttr.getClass().getName().equals("java.lang.String")) {
                requestPrefix =  (String)requestPrefixAttr;
                requestPrefix = ex.get(requestPrefix, String.class);
            }
        }
    }

    Node childCollectionNode = resource.adaptTo(Node.class);
    String childCollectionPath = childCollectionNode.getPath();

    // Url of the child collection referred by the card.
    String childCollectionUrl = request.getContextPath() + COLLECTION_DETAILS_VANITY + resource.getPath();

    String title = UIHelper.getTitle(resource);

    ResourceCollection collection = resource.adaptTo(ResourceCollection.class);
    // assuming that the resource is a collection
    long ck = getCollectionCK(resource);
    String description = "";
    String lightboxCollectionName = "lightbox";
    boolean canEditCollectionSettings = false;
    boolean canDeleteCollection = false;
    boolean canAddChildNodes = false;
    boolean showCollectionSettings = true;
    boolean isLightboxCollection = false;

    String currentCollectionName = childCollectionNode.getName();
    if (lightboxCollectionName.equals(currentCollectionName)) {
        String currentCollectionPath = childCollectionNode.getPath();
        Authorizable user = resourceResolver.adaptTo(Authorizable.class);
        if (null != user) {
            Resource suffixRes = resourceResolver.getResource(slingRequest.getRequestPathInfo().getSuffix());
            UserManagementService userManagementService = sling.getService(UserManagementService.class);
            String tenantUserHome = DamUtil.getInheritedProperty(DamConfigurationConstants.DAM_TENANT_USER_HOME, suffixRes,
                    userManagementService.getUserRootPath());
            String userHome = user.getPath();
            userHome = userHome.indexOf(tenantUserHome)== 0 ? StringUtils.substringAfter(userHome, tenantUserHome): userHome;
            String damCollsHome = DamUtil.getInheritedProperty(DamConfigurationConstants.DAM_COLLECTION_HOME, suffixRes, "/content/dam/collections");
            String userCollsHome = damCollsHome + userHome;
            String lightboxCollectionPath = userCollsHome + "/" + lightboxCollectionName;
            if(!currentCollectionPath.endsWith(lightboxCollectionPath)) {
                return;
            }
        }
    }

    if (childCollectionNode.hasProperty("jcr:description")) {
        description = childCollectionNode.getProperty("jcr:description").getString();
    }
    String href = request.getContextPath() + COLLECTION_DETAILS_VANITY + Text.escapePath(childCollectionPath);
    if (isLinkSharePage) {
        href = request.getContextPath() + "/linkshare.html?path=" + Text.escapePath(childCollectionPath) + requestSuffix;
    }
    actionRels += " cq-assets-create-collection-from-existing-activator ";
    String collectionName = resource.getName();
    //move activator is used for add to collection, which is wrong should be changed
    if ( !"lightbox".equals(collectionName) ) {
        actionRels += " cq-damadmin-admin-actions-add-to-collection-activator";
    } else {
        // CQ-17654: Add to collection should be available for light box.
        isLightboxCollection = true;
    }
    if (UIHelper.hasPermission(acm, resource, Privilege.JCR_WRITE) && UIHelper.hasPermission(acm, resource, Privilege.JCR_MODIFY_ACCESS_CONTROL)) {
        canEditCollectionSettings = true;
        // CQ-14615: Share collection/settings should not be available for lightbox collection
        if (!isLightboxCollection) {
            actionRels += " cq-damadmin-admin-actions-collectionsettings dam-asset-createtask-action-activator cq-damadmin-admin-actions-adhocassetshare-activator";
        } else {
            showCollectionSettings = false;
        }
    }
    if (!isLightboxCollection && UIHelper.hasPermission(acm, resource, "crx:replicate")) {
        actionRels += " cq-damadmin-admin-actions-mpshare-activator";
        actionRels += " cq-damadmin-admin-actions-mpunshare-activator";
    }
    // CQ-17654: Delete should not be available for light box as even after deleting it a new lightbox collection is anyway created.
    if (UIHelper.hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE) && !isLightboxCollection) {
        canDeleteCollection = true;

        actionRels += " cq-damadmin-admin-actions-delete-activator";
    }

    if (UIHelper.hasPermission(acm, resource, Privilege.JCR_ADD_CHILD_NODES)) {
        canAddChildNodes = true;
    }
    String collectionClass = " card-collection ";
    String query = "";
    // Changing resource to resourceResolver.resolve(resource.getPath()), due to issue CQ-30010
    // Revert when resource.getResourceType(), fetches correct value
    boolean isSmartCollection = DamUtil.isSmartCollection(resourceResolver.resolve(resource.getPath()));
	long resultCount = 0;
    if (isSmartCollection) {
        collectionClass = collectionClass + " smart ";
        QueryBuilder queryBuilder = sling.getService(QueryBuilder.class);
        PredicateGroup rootPredicate = new PredicateGroup();
        Query myquery = queryBuilder.loadQuery(childCollectionNode.getPath()
                    + "/dam:query", resourceResolver.adaptTo(Session.class));
        rootPredicate.add(myquery.getPredicates());
        rootPredicate.setAllRequired(true);
        myquery = queryBuilder.createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
        myquery.setHitsPerPage(1L);
        SearchResult result = myquery.getResult();
        resultCount = result.getTotalMatches();
    } 

    String thumbnailPath = request.getContextPath() + requestPrefix + Text.escapePath(childCollectionPath) + ".folderthumbnail.jpg?width=240&height=240&ch_ck=" + ck + requestSuffix;
    // Path at which manual thumbnail(if present) exists
    String manualThumbnailPath = childCollectionNode.getPath() + "/manualThumbnail.jpg";
    Resource manualThumbnail = resourceResolver.getResource(manualThumbnailPath);
    if (manualThumbnail != null) {
        thumbnailPath = request.getContextPath() + requestPrefix + manualThumbnailPath  + "?ch_ck=" + ck + requestSuffix;
    }

    String role = i18n.get("Viewer");
    if (canEditCollectionSettings) {
        role = i18n.get("Owner");
    } else if (canAddChildNodes) {
        role = i18n.get("Editor");
    }

    String type = i18n.get("COLLECTION", "collection of resources/assets");
    if (isSmartCollection) {
        type = i18n.get("SMART COLLECTION", "i.e. query based collection of assets");
    }
	
    //last modified and modified by
    ResourceBundle resourceBundle = slingRequest.getResourceBundle(slingRequest.getLocale());
    RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", resourceBundle);
    String lastModified = null;
    String lastModifiedBy = null;
    long collectionLastModification = -1;
    if (childCollectionNode.hasProperty("jcr:lastModified")) {
        Calendar mod = childCollectionNode.getProperty("jcr:lastModified").getDate();
        collectionLastModification = (null != mod) ? mod.getTimeInMillis() : 0;
        if (collectionLastModification == 0) {
            Calendar created = childCollectionNode.getProperty("jcr:created").getDate();
            collectionLastModification = (null != created) ? created.getTimeInMillis() : 0;
        }
    }
    lastModified = outVar(xssAPI, i18n, rtf.format(collectionLastModification, true));

    if (childCollectionNode.hasProperty("jcr:lastModifiedBy")) {
        String collectionModifier = childCollectionNode.getProperty("jcr:lastModifiedBy").getString();
        lastModifiedBy = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), collectionModifier);
    }
	
    AttrBuilder metaAttrs = new AttrBuilder(request, xssAPI);
    metaAttrs.addBoolean("hidden", true);
    metaAttrs.addClass("foundation-collection-assets-meta");
    metaAttrs.add("data-foundation-collection-meta-title", title);
    metaAttrs.add("data-foundation-collection-meta-type", "collection");
    metaAttrs.add("data-foundation-collection-meta-path", childCollectionPath);
    metaAttrs.add("data-foundation-collection-meta-rel", actionRels);

%>
<%!
/**
cache killer for the collection is, most recently modified asset last modification time.
*/
long getCollectionCK(Resource coll) {
    return UIHelper.getCacheKiller( coll.adaptTo(Node.class));
}
%>
