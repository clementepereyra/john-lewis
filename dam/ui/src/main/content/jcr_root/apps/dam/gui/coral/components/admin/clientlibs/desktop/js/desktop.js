/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
(function (document, $) {
    "use strict";

    var DAM_ROOT_EXACT = "/content/dam";
    var DAM_ROOT = DAM_ROOT_EXACT + "/";

    var rel = ".dam-asset-desktop-action";
    $(document).fipo("tap" + rel, "click" + rel, rel, function (e) {
        e.preventDefault();
        var activator = $(this);

        var selectionCount = Dam.Util.getSelectionCount(Dam.Util.getSelection());

        for (var i = 0; i < selectionCount; i++) {
            var path = Dam.Util.getSelectionPathAt(Dam.Util.getSelection(),i);
            if (!path) {
                path = activator.data("path");
            }
            openAssetLink(activator, path, i);
        }
        return true;
    });

    function openAssetLink(activator, path, i) {
        if (activator.data("use-current-path")) {
			path = $(".foundation-collection").data("foundationCollectionId");
        }
        if (!path) {
            return;
        }
        if (path.indexOf(DAM_ROOT) != 0 && path != DAM_ROOT_EXACT) {
            console.error("unable to reveal non-dam item: " + path);
            return;
        }
        var query = activator.data("href-query") || "";
        var withoutRoot = '';
        if (path != DAM_ROOT_EXACT) {
            withoutRoot = path.substr(DAM_ROOT.length);
        }
        var href = "aem-asset:/" + encodeURI(withoutRoot) + query;

        var ifra = document.createElement('iframe');
        ifra.style.display = 'none';
        ifra.onload = function() { ifra.parentNode.removeChild(ifra); };
        ifra.src = href;
        document.body.appendChild(ifra);

    }

})(document, Granite.$);