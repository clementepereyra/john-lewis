<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%@include file="/libs/granite/ui/global.jsp" %>
<%@page session="false"
        import="com.day.cq.i18n.I18n,
                com.day.cq.wcm.api.Page,
                org.apache.sling.api.resource.Resource,
				org.apache.sling.api.resource.ValueMap,
                com.adobe.granite.ui.components.AttrBuilder,
                com.adobe.granite.ui.components.ComponentHelper,
                com.adobe.granite.ui.components.Config,
                com.adobe.granite.ui.components.Tag,
                com.adobe.granite.ui.components.Value,
                com.adobe.granite.ui.components.LayoutBuilder,             
                com.adobe.granite.xss.XSSAPI,
                javax.jcr.Node,
                com.day.cq.dam.api.Asset,
			    org.apache.jackrabbit.util.Text,
				javax.jcr.Property" %><%
%>
<div>

<%

    Config cfg = new Config(resource);
    
    String contentPath = request.getContextPath() + ((String[])request.getAttribute("aem.assets.ui.properties.content"))[0];

	String productPath = null;
    String productID = null;
	String[] jlProducts = null;

    if (contentPath!=null) {
        Resource res = resource.getResourceResolver().getResource(contentPath);       
        if (res != null) {
            Asset contentAsset = res.adaptTo(Asset.class);
            productPath = contentAsset.getMetadataValue("cq:productReference");
            productID = contentAsset.getMetadataValue("dam:productID");
            String jlProductsProp = contentAsset.getMetadataValue("dam:JLPProducts");
            if (jlProductsProp != null) {
            	jlProducts = (String[]) jlProductsProp.split(",");
            }

        }      
    }
    if (jlProducts == null || jlProducts.length==0) {
        if (productPath == null || productPath.isEmpty()) {
        %>
            <div>
            <%=i18n.get("This digital asset is not associated with any product, currently. Hence, no Product Data is available.")%>
            </div>
        </div>
        <%
            return;
        }
    
    
        if(productID == null) {
            Resource productRes = resource.getResourceResolver().getResource(productPath);
            productID = productRes.adaptTo(ValueMap.class).get("identifier", String.class);
        }
    
        String fieldLabel = cfg.get("fieldLabel", String.class);
    
        Config productCfg = new Config(resource.getResourceResolver().getResource(productPath));
    
        Tag tag = cmp.consumeTag();
        tag.setName("div");
    
        AttrBuilder attrs = tag.getAttrs();
    
        attrs.add("id", productCfg.get("id", String.class));
        attrs.addRel(productCfg.get("rel", String.class));
        attrs.addClass(productCfg.get("class", String.class));
        attrs.add("title", i18n.getVar(productCfg.get("title", String.class)));
    
        attrs.addOthers(productCfg.getProperties(), "id", "rel", "class", "title");
    
        tag.printlnStart(out);
    
        Object formValueAttribute = null;
        Object formContentPathAttribute = null;
        try{
            formContentPathAttribute = request.getAttribute(Value.CONTENTPATH_ATTRIBUTE);
            formValueAttribute = request.getAttribute(Value.FORM_VALUESS_ATTRIBUTE);
            request.setAttribute(Value.CONTENTPATH_ATTRIBUTE, productPath);
            request.setAttribute(Value.FORM_VALUESS_ATTRIBUTE, null);
        %>
            <sling:include path="/mnt/overlay/dam/gui/content/pim/productmetadata/jcr:content/cq:dialog/content" resourceType="granite/ui/components/foundation/contsys" />
        <%
        }
        finally {
            if( formValueAttribute !=null) {
                request.setAttribute(Value.FORM_VALUESS_ATTRIBUTE, formValueAttribute);
            }
            if ( formContentPathAttribute != null) {
                request.setAttribute(Value.CONTENTPATH_ATTRIBUTE, formContentPathAttribute);
            }
        }
        tag.printlnEnd(out);
    
        String pageUrl = "/libs/commerce/gui/content/products/properties.html?item=" + productPath;
    
    %>
        <div data-metatype="reference">
            <div><%=i18n.get("Product Reference")%></div>
            <a title="<%= xssAPI.encodeForHTMLAttr(productID) %>" class="coral-Icon coral-Icon--link withLabel" href="<%= xssAPI.getValidHref(pageUrl) %>" target="_blank"> <%= xssAPI.encodeForHTML(productID) %></a>
        </div>
	<%} else {
    %><div data-metatype="reference">
    	<div><%=i18n.get("Product References")%></div><%

        for (String jlProdref : jlProducts) {
        	String pageUrl = "/libs/commerce/gui/content/products/properties.html?item=" + jlProdref.trim();
        	Resource productRes = resource.getResourceResolver().getResource(jlProdref.trim());
            Node map = productRes.adaptTo(Node.class);
            productID = map.getProperty("identifier").getString();
            Property productNameProperty = map.getProperty("wsmTitle");
            String productName = null;
            if (productNameProperty != null) {
                productName = productNameProperty.getString();
            }
            if (productName != null) {
	%>
            <div><a title="<%= xssAPI.encodeForHTMLAttr(productID) %>" class="coral-Icon coral-Icon--link withLabel" href="<%= xssAPI.getValidHref(pageUrl) %>" target="_blank"> <%= xssAPI.encodeForHTML(productID) %> - <%= xssAPI.encodeForHTML(productName) %></a></div>
	<% 
			} else {
	%>
	            <div><a title="<%= xssAPI.encodeForHTMLAttr(productID) %>" class="coral-Icon coral-Icon--link withLabel" href="<%= xssAPI.getValidHref(pageUrl) %>" target="_blank"> <%= xssAPI.encodeForHTML(productID) %> </a></div>
	<% 			    
			}
        }
    }
    %>
</div>