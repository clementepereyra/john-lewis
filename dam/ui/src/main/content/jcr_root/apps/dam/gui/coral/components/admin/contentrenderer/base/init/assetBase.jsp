<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page import="org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ResourceUtil,
                  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.api.resource.ResourceResolver,
      			  org.apache.jackrabbit.api.security.user.User,
                  org.apache.jackrabbit.api.security.user.Group,
                  org.apache.jackrabbit.api.security.user.Authorizable,
                  org.apache.commons.lang.StringUtils,
                  org.apache.sling.api.resource.ValueMap,
      			  java.util.Iterator,
				  java.util.Arrays,
				  javax.jcr.lock.LockManager,
                  javax.jcr.Node,
				  com.adobe.granite.comments.CommentCollection,
				  com.adobe.granite.comments.CommentManager,
				  com.adobe.granite.ui.components.Config,
				  com.day.cq.dam.api.DamConstants,
                  com.day.cq.dam.api.s7dam.constants.S7damConstants,
                  com.day.cq.dam.api.s7dam.constants.S7damConstants,
				  com.day.cq.dam.commons.util.DamUtil,
                  com.day.cq.dam.commons.util.S7SetHelper,
                  com.day.cq.dam.commons.util.S73DHelper,
				  com.day.cq.commons.LabeledResource,
				  com.day.cq.commons.jcr.JcrConstants,
				  com.day.cq.dam.commons.util.UIHelper,
				  com.day.cq.dam.api.checkout.AssetCheckoutService"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/base.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/assetBase.jsp"%><%
%><%--###
Asset Base Initializer
=========

This JSP is initializing all the attaribues expected by ../assetBase.jsp, it is expected to evaluate asset properties & perform performance incentive tasks at this JSP, hence it should not be triggered more than once per resource.

###--%><%

 boolean is3D = S73DHelper.isS73D(resource);
 request.setAttribute(IS_3D_ASSET, is3D);

 boolean isMissingDependencies = false;
 if(is3D)
	 isMissingDependencies = S73DHelper.isUnResolved(resource);
 request.setAttribute(IS_MISSING_DEPENDENCIES, isMissingDependencies);


 String metadataPath = resource.getPath() + "/jcr:content/metadata/";
 Resource metadataResc = resourceResolver.getResource(metadataPath);
 Node metadataNode = metadataResc != null ? metadataResc.adaptTo(Node.class) : null;
 Authorizable authUser = resourceResolver.adaptTo(Authorizable.class);

 String mimeType = "";
 // display mime type is shown on card (in grid view) and in list view
    String displayMimeType="";
    Resource lookupResource = resourceResolver.getResource("/mnt/overlay/dam/gui/content/assets/jcr:content/mimeTypeLookup");

    boolean canCompare = false;
    String[] comparableMimeTypes = {
            "image/jpeg",
            "image/jpg",
            "image/png",
            "image/gif",
            "image/bmp",
            "image/tiff",
            "image/vnd.adobe.photoshop",
            "application/x-photoshop",
            "application/photoshop",
            "application/psd",
            "image/psd"
    };

    attrs.add("data-timeline", true);

    //mimeType
	if (is3D) {
            Resource assetContent = resource.getChild("jcr:content");
            displayMimeType = (assetContent != null) ? assetContent.getValueMap().get("dam:s7damType", "").toUpperCase() : "";
            String extension = FilenameUtils.getExtension(resource.getName());
            if (!extension.isEmpty()) {
                displayMimeType += " (" + extension + ")";
            }
    } else if(isIdsTemplateAsset(asset.adaptTo(Node.class))) {
	    // for indesign templates, display the dam:templateType
        displayMimeType = getTemplateType(asset).toUpperCase();
        if(asset.getMimeType() != null) {
            mimeType = asset.getMimeType();
        }
    } else if (asset.getMimeType() != null) {
        mimeType = asset.getMimeType();
        String ext = mimeType.substring(mimeType.lastIndexOf('/') + 1, mimeType.length());
        if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
            displayMimeType = "";
        }
        if (displayMimeType.length() == 0 && mimeType.startsWith("image")) {
            displayMimeType = "IMAGE";
        } else if (displayMimeType.length() == 0 && mimeType.startsWith("text")) {
            displayMimeType = "DOCUMENT";
        } else if (displayMimeType.length() == 0 && (mimeType.startsWith("video") || mimeType.startsWith("audio"))) {
            displayMimeType = "MULTIMEDIA";
        } else if (displayMimeType.length() == 0 && mimeType.startsWith("application")) {
            int idx_1 = ext.lastIndexOf('.');
            int idx_2 = ext.lastIndexOf('-');
            int lastWordIdx = (idx_1 > idx_2)?idx_1:idx_2;
            displayMimeType = ext.substring(lastWordIdx+1).toUpperCase();
        }
    }

    if (contains(comparableMimeTypes, mimeType)) {
        canCompare = true;
    }
    attrs.addOther("can-compare", String.valueOf(canCompare));

   request.setAttribute(MIMETYPE, mimeType);

    if (displayMimeType.length() == 0 && asset.getName() != null) {
        String filename = asset.getName();
        String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
            displayMimeType = "";
        }
    }

//Check if Annotate operation is applicable for Asset
    boolean canAnnotate = false;
    String[] annotatableMimeTypes = { "image/jpeg", "image/jpg", "image/png", "image/gif" };
    if (hasAddChild &&  hasModifyProperties) {
        if (contains(annotatableMimeTypes, mimeType) || UIHelper.getBestfitRendition(asset, 319) != null) {
            canAnnotate = true;
        }
    }

 request.setAttribute(CAN_ANNOTATE, canAnnotate);


//Check if Edit operation is applicable for Asset
    boolean canEdit = false;
    boolean canEditMetadata = false;
    boolean isLocked = isLocked(resourceResolver, resourceNode);
    boolean isEditablePrintAsset = false;
    if (hasModifyProperties && hasVersionMgmt && !isLocked) {
         isEditablePrintAsset = isEditablePrintAsset(resourceNode);
    }
    // attempt to find a editor that can handle this resource if has the
    // permission to edit
    if (hasModifyProperties && hasVersionMgmt) {
        canEditMetadata = true;
        if (!isLocked) {
            if (mimeType.startsWith("video")) {
                // DMVideo is availabe for edit
                final String DAM_S7DAMTYPE = "dam:s7damType";
                Resource jcrContent = resourceResolver.getResource(resource.getPath() + "/" + JcrConstants.JCR_CONTENT);
                ValueMap properties = jcrContent.getValueMap();
                canEdit = properties.containsKey(DAM_S7DAMTYPE);

            } else if(isEditablePrintAsset){
                canEdit = true;
            } else {
                Resource editors = resource.getResourceResolver().getResource("/libs/dam/gui/content/assets/editors");
                for (Iterator<Resource> it = editors.listChildren(); it.hasNext();) {
                    Resource child = it.next();
                    Config editorPropCfg = new Config(child);
                    String[] mimeTypes = editorPropCfg.get("mimetypes", String[].class);
                    if (mimeTypes != null && contains(mimeTypes, mimeType)) {
                        canEdit = true;
                        break;
                    }
                }
            }
        }
    } else if (hasModifyProperties) {
        canEditMetadata = true;
    }

 request.setAttribute(IS_EDITABLE_PRINT_ASSET, isEditablePrintAsset);
 request.setAttribute(CAN_EDIT, canEdit);

boolean isAssetExpired = false;
boolean isSubAssetExpired = false;
boolean isAssetSubAsset = false;
Calendar assetExpiryTime = null;
boolean isDownloadAllowedForAdmins = false;

if (null != resourceNode) {
	Calendar now = Calendar.getInstance();
	assetExpiryTime = DamUtil.getExpiryTime(asset);
    request.setAttribute(ASSET_EXPIRY_TIME, assetExpiryTime);
	if (null != assetExpiryTime) {
		isAssetExpired = DamUtil.isExpiredAsset(asset);
	}

	if (!isAssetExpired) {
		isSubAssetExpired = DamUtil.isExpiredSubAsset(resource);
	}

	if (isDAMAdmin) {
		isDownloadAllowedForAdmins = true;
	}
	request.setAttribute(IS_ASSETEXPIRED, isAssetExpired);
	request.setAttribute(IS_SUBASSET_EXPIRED, isSubAssetExpired);
	request.setAttribute(IS_DOWNLOAD_ALLOWED_FOR_ADMINS, isDownloadAllowedForAdmins);
}

    isAssetSubAsset = isSubAsset(resource);
	type = isAsset && isAssetSubAsset ? "subasset" : "asset";
    request.setAttribute(TYPE, type);

    Asset parentAsset = null;
    if (isAssetSubAsset) {
        Resource parentResource = resource.getParent().getParent(); //since immediate parent is subaasets
        parentAsset = parentResource.adaptTo(Asset.class);
    }

    //DM Set Manual Thumbnail
    Asset dmSetManualThumbnailAsset = null;

//product, size, width & height
	long width = 0;
    long height = 0;
    String size = "0.0 B";
    long bytes=0;
    boolean isProduct = false;
	String dmSetManualThumbnail = "";

    if (metadataNode != null) {
        try {
            width = Long.valueOf(DamUtil.getValue(metadataNode, "tiff:ImageWidth", DamUtil.getValue(metadataNode, "exif:PixelXDimension", "")));
            height = Long.valueOf(DamUtil.getValue(metadataNode, "tiff:ImageLength", DamUtil.getValue(metadataNode, "exif:PixelYDimension", "")));
        } catch (Exception e) {

        }

        try {
            bytes =  Long.valueOf(DamUtil.getValue(metadataNode, "dam:size", "0"));
            //size
            if (bytes == 0 && asset.getOriginal() != null) {
                bytes = asset.getOriginal().getSize();
            }
            size = UIHelper.getSizeLabel(bytes, slingRequest);

        } catch(Exception e) {}
        try {
            dmSetManualThumbnail = DamUtil.getValue(metadataNode, "manualThumbnail", "");
            if (!dmSetManualThumbnail.isEmpty()) {
                Resource manualThumbResource = resourceResolver.getResource(dmSetManualThumbnail);
                dmSetManualThumbnailAsset = manualThumbResource.adaptTo(Asset.class);

            }
        } catch(Exception e) {}

       try {
            isProduct = !DamUtil.getValue(metadataNode,"cq:productReference", "").isEmpty();
        }
        catch(Exception e) {}
    }
	request.setAttribute(WIDTH, width);
	request.setAttribute(HEIGHT, height);
	request.setAttribute(BYTES, bytes);
	request.setAttribute(SIZE, size);
	request.setAttribute(MANUAL_THUMBNAIL_ASSET, dmSetManualThumbnailAsset);

    metaAttrs.add("data-asset-size", size);
//resolution
    String resolution = "";
    if (width != 0 && height != 0) {
        resolution = UIHelper.getResolutionLabel(width, height, slingRequest);
    }
    resolution = resolution.replaceAll(",", "");
    request.setAttribute(RESOLUTION, resolution);

	// Check subassets
	boolean hasSubassets = false;
    if (resource.getChild("subassets") != null){
        hasSubassets = true;
    }

	// asset last modification
	String lastModifiedBy = "";
 	long assetLastModification = asset.getLastModified();
    ValueMap vm = resource.adaptTo(ValueMap.class);
        if (assetLastModification == 0) {
            Calendar created = vm.get("jcr:created", Calendar.class);
            assetLastModification = (null != created) ? created.getTimeInMillis() : 0;
        }
	request.setAttribute(ASSET_LAST_MODIFICATION, assetLastModification);
    String lastModified = outVar(xssAPI, i18n, rtf.format(assetLastModification, true));
    request.setAttribute(LAST_MODIFIED, lastModified);
    String assetModifier = asset.getModifier();
    if (StringUtils.isNotBlank(assetModifier)) {
        String storedFormattedName = (String) request.getAttribute(assetModifier);
        if (StringUtils.isBlank(storedFormattedName)) {
            lastModifiedBy = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), assetModifier);
            request.setAttribute(assetModifier, lastModifiedBy);
        } else {
            lastModifiedBy = storedFormattedName;
        }
    } else {
        assetModifier = "";
    }
     // Also check asset modifier should not be empty. see CQ-39542
     if (!"".equals(assetModifier) && upm.getUserProperties(assetModifier, "profile") == null) {
            lastModifiedBy = i18n.get("External User");
      }

 //comments
        CommentManager cm = sling.getService(CommentManager.class);
        final CommentCollection collection = cm.getCollection(resource, CommentCollection.class);
        int commentsCount = 0;
        if (null != collection) {
            commentsCount = collection.getCommentList().size();
        }
		request.setAttribute(COMMENTS_COUNT, commentsCount);

//Content Fragment
  boolean isContentFragment = this.isContentFragment(resource);
  request.setAttribute(IS_CONTENT_FRAGMENT, isContentFragment);

    //Dynamic Media
    //Check DM Set
    boolean isDMSet = S7SetHelper.isS7Set(resource);
    String s7damType = "";
    String dmAssetType = "";
    boolean dynamicVideo = false;
	request.setAttribute(IS_DM_SET, isDMSet);
    if (resourceNode.hasNode(JcrConstants.JCR_CONTENT) &&
            resourceNode.getNode(JcrConstants.JCR_CONTENT).hasProperty("dam:s7damType")) {
        s7damType = resourceNode.getNode(JcrConstants.JCR_CONTENT).getProperty("dam:s7damType").getString();
    }
	request.setAttribute(S7_DAM_TYPE, s7damType);

    //Get set type as displayMimeType for DM Set
    if (isDMSet) {
        //Override displayMimeType for set
        displayMimeType = s7damType;
        // set editing requires remove nodes
        canEdit = hasJcrWrite;
        if (s7damType.equalsIgnoreCase("CarouselSet")) {
            //carousel set editor key
            attrs.add("data-editorkey", "carouselset");
        }
        else {
            //generic set editor key
            attrs.add("data-editorkey", "set");
        }
        dmAssetType = "set";
    }
    else {
        //default editor
        attrs.add("data-editorkey", "");
        if (mimeType.contains("video") && enabledDynamicMedia &&
                (s7damType.equals(S7damConstants.S7_VIDEO_AVS) || s7damType.equals(S7damConstants.S7_VIDEO))) {
            dmAssetType = "video";
            dynamicVideo = true;
            // set editing requires remove nodes
            canEdit = hasJcrWrite;
            //video editor key
            attrs.add("data-editorkey", "video");
        }
        else if (!s7damType.isEmpty()) {
            dmAssetType = s7damType;
        }
    }
	request.setAttribute(DM_ASSET_TYPE, dmAssetType);
	request.setAttribute(DYNAMIC_VIDEO, dynamicVideo);
    //flag for publish to indicate that the asset is DM asset + type of DM Asset
    metaAttrs.add("data-dm-asset-type", dmAssetType);
    metaAttrs.add("data-asset-mimetype", mimeType);

//Checkout Status
boolean isCheckedOut = false;
boolean canCheckOut = false;
boolean canCheckIn = false;
boolean parentIsCheckedOut = false;

AssetCheckoutService assetCheckoutService = sling.getService(AssetCheckoutService.class);
if (assetCheckoutService != null) {
	isCheckedOut = assetCheckoutService.isCheckedOut(asset);
	canCheckOut = assetCheckoutService.canCheckOut(asset) && !asset.isSubAsset();
	canCheckIn = assetCheckoutService.canCheckIn(asset) && !asset.isSubAsset();

    if (parentAsset != null) {
        parentIsCheckedOut = assetCheckoutService.isCheckedOut(parentAsset);
    }
}

String checkedOutBy = "";
String checkedOutByFormatted = "";
boolean checkedOutByCurrentUser = false;
boolean isParentAssetCheckedOutByCurrentUser = false;

if (isCheckedOut) {
    checkedOutBy = assetCheckoutService.getCheckedOutBy(asset);
    checkedOutByFormatted = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), checkedOutBy);
    checkedOutByCurrentUser = authUser.getID().equals(checkedOutBy);
}

if (parentIsCheckedOut) {
    String parentAssetCheckedOutBy = assetCheckoutService.getCheckedOutBy(parentAsset);
    isParentAssetCheckedOutByCurrentUser = authUser.getID().equals(parentAssetCheckedOutBy);
}

request.setAttribute(IS_CHECKED_OUT, isCheckedOut);
request.setAttribute(CAN_CHECK_OUT, canCheckOut);
request.setAttribute(CAN_CHECK_IN, canCheckIn);
request.setAttribute(CHECKED_OUT_BY, checkedOutBy);
request.setAttribute(CHECKED_OUT_BY_FORMATTED, checkedOutByFormatted);
request.setAttribute(CHECKED_OUT_BY_CURRENT_USER, checkedOutByCurrentUser);
request.setAttribute(IS_PARENT_ASSET_CHECKED_OUT, parentIsCheckedOut);
request.setAttribute(IS_PARENT_ASSET_CHECKED_OUT_BY_CURRENT_USER, isParentAssetCheckedOutByCurrentUser);

metaAttrs.addOther("can-compare", String.valueOf(canCompare));
metaAttrs.add("data-asset-is-sub-asset", isAssetSubAsset);
metaAttrs.add("data-asset-is-checked-out", isCheckedOut);
metaAttrs.add("data-asset-can-check-out", canCheckOut);
metaAttrs.add("data-asset-can-check-in", canCheckIn);
metaAttrs.add("data-asset-checked-out-by", checkedOutBy);
metaAttrs.add("data-asset-checked-out-by-formatted", checkedOutByFormatted);
metaAttrs.add("data-asset-checked-out-by-current-user", checkedOutByCurrentUser);
metaAttrs.add("data-parent-asset-is-checked-out", parentIsCheckedOut);
metaAttrs.add("data-parent-asset-checked-out-by-current-user", isParentAssetCheckedOutByCurrentUser);

boolean isWorkflowActive = false;
String assetProcessingState = "";
if (resourceNode.hasNode(JcrConstants.JCR_CONTENT) &&
    resourceNode.getNode(JcrConstants.JCR_CONTENT).hasProperty(DamConstants.DAM_ASSET_STATE)) {
    assetProcessingState = resourceNode.getNode(JcrConstants.JCR_CONTENT).getProperty(DamConstants.DAM_ASSET_STATE).getString();
    if (assetProcessingState.equals(DamConstants.DAM_ASSET_STATE_PROCESSING)) {
        isWorkflowActive = true;
    }
}
metaAttrs.add("data-workflow-active", isWorkflowActive);

//Navigation Href
final String ASSET_DETAILS_VANITY = "/assetdetails.html";
final String CONTENT_FRAGMENT_EDITOR_VANITY = "/editor.html";
final String DM_SET_PREVIEW_VANITY = "/mnt/overlay/dam/gui/content/s7dam/sets/setpreview.html";
String navigationHref = request.getContextPath() + ASSET_DETAILS_VANITY + Text.escapePath(resource.getPath());

if (isContentFragment) {
    navigationHref = request.getContextPath() + CONTENT_FRAGMENT_EDITOR_VANITY + Text.escapePath(resource.getPath());
    canEdit = hasJcrWrite;
 } else if (S7SetHelper.isS7Set(resource)) {
     navigationHref = request.getContextPath() + DM_SET_PREVIEW_VANITY + Text.escapePath(resource.getPath());
 }

// Asset Status Property {Approved/Rejected}
 String status = ResourceUtil.getValueMap(metadataResc).get("dam:status", "");
 request.setAttribute(STATUS, status);
 metaAttrs.addOther("status", status);

//Asset Rating
double averageRating = 0.0;
if (resource.getChild("jcr:content")!=null){
    averageRating = resource.getChild("jcr:content").getValueMap().get("averageRating", 0.0);
}
request.setAttribute(AVERAGE_RATING, averageRating);

// Zip Archive
boolean isArchive = this.isArchive(metadataResc);
request.setAttribute(IS_ARCHIVE, isArchive);

//Creative Rating
double creativeRating = 0.0;
if (resource.getChild("jcr:content")!=null){
    creativeRating = resource.getChild("jcr:content/metadata").getValueMap().get("xmp:Rating", 0.0);
}
request.setAttribute(CREATIVE_RATING, creativeRating);
%>
