<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page import="org.apache.sling.api.resource.Resource,
				  com.adobe.granite.security.user.util.AuthorizableUtil,
				  com.day.cq.dam.commons.util.UIHelper"%><%
// Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/assetBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/column/common/common.jsp"%><%
String name = resource.getName();


boolean showOriginalIfNoRenditionAvailable = (request!=null && request.getAttribute("showOriginalIfNoRenditionAvailable")!=null) ? (Boolean)request.getAttribute("showOriginalIfNoRenditionAvailable") : false;
boolean showOriginalForGifImages = (request!=null && request.getAttribute("showOriginalForGifImages")!=null) ? (Boolean)request.getAttribute("showOriginalForGifImages") : false;

boolean lowResolution = false;
String assetLength = asset.getMetadataValue("tiff:ImageLength");
String assetWidth = asset.getMetadataValue("tiff:ImageWidth");

if(assetLength!=null && assetWidth!=null){
    try{
        int len = Integer.parseInt(assetLength);
        int wid =  Integer.parseInt(assetWidth);
        if(len < 48 || wid < 48) {
            lowResolution = true;
        }
    }catch (NumberFormatException nfe){
        log.error("Exception:" + nfe);
    }
}

String thumbnailUrl = request.getContextPath() + requestPrefix
+ getThumbnailUrl(asset, 48, showOriginalIfNoRenditionAvailable, showOriginalForGifImages)
+ "?ch_ck=" + ck + requestSuffix;
//Override default thumbnail for set when there is manual thumbnail defined
if (dmSetManualThumbnailAsset != null) {
    thumbnailUrl = request.getContextPath() + requestPrefix
    		+ getThumbnailUrl(asset, 1280, showOriginalIfNoRenditionAvailable, showOriginalForGifImages)
    		+ "?ch_ck=" + ck + requestSuffix;
}

String assetActionRels = StringUtils.join(getAssetActionRels(hasJcrRead, hasJcrWrite, hasAddChild, canEdit, canAnnotate, isDownloadAllowedForAdmins, isAssetExpired, isSubAssetExpired, isContentFragment, isArchive), " ");

request.setAttribute("actionRels", actionRels.concat(" " + assetActionRels));

attrs.add("itemscope", "itemscope");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);

if (resource.getChild("subassets")!=null){
     attrs.add("variant", "drilldown");
}
request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);

%><coral-columnview-item <%= attrs %>>
<cq:include script = "meta.jsp"/>
    <coral-columnview-item-thumbnail><%
        if (isArchive) {
            %><coral-icon icon="fileZip" size="S"></coral-icon><%
        } else {
            if(lowResolution){
                %><img src="<%= xssAPI.getValidHref(thumbnailUrl) %>" alt="" vertical-align: middle; itemprop="thumbnail" style="height: auto; width: auto;"><%
            } else {
                %><img src="<%= xssAPI.getValidHref(thumbnailUrl) %>" alt="" vertical-align: middle; itemprop="thumbnail" ><%
            }

        }%>
    </coral-columnview-item-thumbnail>
    <coral-columnview-item-content>
        <div class="foundation-collection-item-title" itemprop="title" title="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>">
            <%= xssAPI.encodeForHTML(resourceTitle) %>
        </div><%
        if (name != null && !name.equals(resourceTitle)) {
            %><div class="foundation-layout-util-subtletext">
                <%= xssAPI.encodeForHTML(name) %>
            </div><%
        }%>
    </coral-columnview-item-content>
    <cq:include script = "applicableRelationships.jsp"/>
    <cq:include script = "link.jsp"/>
	<meta itemprop="lastmodified" content="<%= lastModified %>">
    <meta itemprop="lastmodifiedby" content="<%= xssAPI.encodeForHTMLAttr(lastModifiedBy) %>">
</coral-columnview-item><%!
//Add private methods here
%>
