<%--
  ADOBE CONFIDENTIAL

  Copyright 2016 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false" contentType="text/html" pageEncoding="utf-8"
          import="com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ds.ValueMapResource,
                  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  java.util.HashMap"%><%
%><%@taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0"%><%

    Config cfg = new Config(resource);

    String title = cfg.get("text", String.class);
String metaPropName = cfg.get("name", "jcr:content/metadata/dam:JLPAssigned");
    long predicateIndex = cfg.get("listOrder", 5000L);
    String indexGroup = predicateIndex + "_group";
    String predicateName = indexGroup + ".property";
    String propVal = predicateName + ".value";
    String breadcrumbs = i18n.get(title);

    ValueMap userPickerProperties = new ValueMapDecorator(new HashMap<String, Object>());
    userPickerProperties.put("emptyText", cfg.get("emptyText", i18n.get("Owner")));
    userPickerProperties.put("name", propVal);
    userPickerProperties.put("hideServiceUsers", true);
    userPickerProperties.put("granite:class", "search-predicate-owner-userpicker");
    ValueMapResource userPickerResource = new ValueMapResource(resourceResolver, resource.getPath(), "granite/ui/components/coral/foundation/form/userpicker", userPickerProperties);
%><div class="owner-predicate"
			data-list-order="<%=predicateIndex%>"
			data-type="owner"
      data-name="<%= predicateName %>">
    <sling:include resource="<%= userPickerResource %>"/>
    <input type="hidden" name="<%=predicateName%>" value="<%= xssAPI.encodeForHTMLAttr(metaPropName) %>" class="search-predicate-owner-option">
    <input type="hidden" name="<%= predicateName %>.breadcrumbs" value="<%= xssAPI.encodeForHTMLAttr(breadcrumbs) %>">
</div>
<!-- note: the script is being included every time select list is opened / autocomplete loaded -->
<ui:includeClientLib categories="dam.admin.searchpanel.ownerpredicate" />
