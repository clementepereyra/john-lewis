<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%@page session="false" %>
<%@include file="/apps/dam/gui/coral/components/admin/collections/contentrenderer/collectionBase.jsp"%><%
    String downloadHref = xssAPI.getValidHref(request.getContextPath() + "/mnt/overlay/dam/gui/content/assets/downloadasset.html");
    String downloadHasLicenseHref = xssAPI.getValidHref(request.getContextPath() + "/mnt/overlay/dam/gui/content/assets/haslicense.html");
    String downloadLicenseHref = xssAPI.getValidHref(request.getContextPath() + "/mnt/overlay/dam/gui/content/assets/licensecheck.html");
    String collectionContext = i18n.get("Collection");
    if (isSmartCollection) {
        collectionContext = i18n.get("Smart Collection");
    }
    String collectionIcon = "visit";
    if(role.equals("Owner")){
        collectionIcon = "login";
    } else if (role.equals("Editor")) {
        collectionIcon = "userEdit";
    }
%>
<div <%= metaAttrs %>></div>
<coral-card variant="condensed" data-timeline="true" stacked>
    <coral-card-asset>
        <img src = "<%= xssAPI.getValidHref(thumbnailPath) %>">
    </coral-card-asset>
    <coral-card-content>
        <coral-card-context class="coral-Card-context"><%= xssAPI.encodeForHTML(collectionContext) %></coral-card-context>
        <coral-card-title class="foundation-collection-item-title" value="<%= xssAPI.encodeForHTMLAttr(title) %>"><%= xssAPI.encodeForHTML(title)%></coral-card-title>
        <%if (isSmartCollection) {%>
          <coral-card-description class="coral-Card-description" style="display:block"><%=resultCount%> assets </coral-card-description>
        <%}%>
        <coral-icon icon="<%= collectionIcon %>" size="xS"></coral-icon>
    </coral-card-content>

    <meta class="foundation-collection-quickactions" data-foundation-collection-quickactions-rel="<%= xssAPI.encodeForHTMLAttr(actionRels) %>"></meta>

	<coral-card-info>
        <!-- todo : could be in a different jsp file? -->
		<a style="width:100%; height:100%; position:absolute;" href="<%= xssAPI.getValidHref(childCollectionUrl) %>" data-foundation-content-title="<%= xssAPI.encodeForHTMLAttr(title) + resultCount%> "></a>
	</coral-card-info>

</coral-card>

<coral-quickactions target="_prev" alignmy="left top" alignat="left top">
    <coral-quickactions-item icon="check" class="foundation-collection-item-activator"><%= xssAPI.encodeForHTML(i18n.get("Select")) %></coral-quickactions-item>
    <% 
   	if (showQuickActions) {
    %>
    <coral-quickactions-item icon="infoCircle" class="foundation-anchor" data-foundation-anchor-href="<%= xssAPI.getValidHref(request.getContextPath() + "/mnt/overlay/dam/gui/content/assets/metadataeditorcollection.html" + Text.escapePath(childCollectionPath)) %>"><%= xssAPI.encodeForHTML(i18n.get("Properties")) %></coral-quickactions-item>
    <% if (canEditCollectionSettings && showCollectionSettings) { %>
        <coral-quickactions-item icon="gear" class="foundation-anchor" data-foundation-anchor-href="<%= xssAPI.getValidHref(request.getContextPath() + "/mnt/overlay/dam/gui/content/collections/collectionsettingswizard.html" + Text.escapePath(childCollectionPath)) %>"><%= xssAPI.encodeForHTML(i18n.get("Settings")) %></coral-quickactions-item>
    <%}%>
    <coral-quickactions-item icon="download" class="cq-damadmin-admin-actions-download-activator" data-href="<%=downloadHref%>" data-haslicense-href="<%=downloadHasLicenseHref%>" data-license-href="<%=downloadLicenseHref%>" data-itemPath="<%=xssAPI.encodeForHTMLAttr(childCollectionPath)%>"><%= xssAPI.encodeForHTML(i18n.get("Download")) %></coral-quickactions-item>
    <% } %>
</coral-quickactions>