<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page import="org.apache.sling.api.resource.Resource,
				  javax.jcr.Node,
				  javax.jcr.RepositoryException,
				  javax.jcr.Session,
				  javax.jcr.security.AccessControlManager,
				  javax.jcr.security.Privilege,
				  javax.jcr.PathNotFoundException,
				  javax.jcr.ValueFormatException,
				  java.util.List,
                  java.util.Locale,
				  java.util.ArrayList,
                  java.util.Calendar,
				  java.util.ResourceBundle,
				  org.apache.commons.lang.StringUtils,
				  org.apache.jackrabbit.util.Text,
				  org.apache.jackrabbit.api.security.user.Authorizable,
				  org.apache.sling.api.resource.ResourceResolver,
                  org.apache.sling.api.resource.ValueMap,
				  com.adobe.granite.ui.components.AttrBuilder,
				  com.adobe.granite.security.user.UserProperties,
                  com.adobe.granite.security.user.UserPropertiesManager,
				  com.adobe.granite.security.user.util.AuthorizableUtil,
				  com.adobe.granite.ui.components.Tag,
				  com.adobe.granite.ui.components.ExpressionHelper,
				  com.day.cq.dam.commons.util.DamUtil,
				  com.day.cq.dam.commons.util.DamLanguageUtil,
				  com.day.cq.dam.api.Rendition,
				  com.day.cq.dam.api.Asset,
                  com.day.cq.commons.LanguageUtil,
				  com.day.cq.commons.date.RelativeTimeFormat,
				  com.day.cq.dam.commons.util.UIHelper,
				  com.day.cq.dam.entitlement.api.EntitlementConstants,
                  org.apache.sling.featureflags.Features"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/base.jsp"%><%
%><%--###
Asset Base Initializer
=========

This JSP is initializing all the attaribues expected by ../base.jsp, it is expected to evaluate asset properties & perform performance incentive tasks at this JSP, hence it should not be triggered more than once per resource.

###--%><%

ExpressionHelper ex = cmp.getExpressionHelper();

AccessControlManager acm = null;
Session session = resourceResolver.adaptTo(Session.class);
try {
    acm = session.getAccessControlManager();
} catch (RepositoryException e) {
    log.error("Unable to get access manager", e);
}

Node resourceNode = resource.adaptTo(Node.class);
request.setAttribute(RESOURCE_NODE, resourceNode);
Asset asset = resource.adaptTo(Asset.class);
request.setAttribute(ASSET, asset);
String resourcePath = resourceNode.getPath();
request.setAttribute(RESOURCE_PATH, resourcePath);

//applicable relationships
boolean hasJcrWrite = false, hasReplicate = false, hasJcrRead = false, hasRemoveNode = false, hasModifyProperties = false, hasAddChild = false, hasModifyAccessControl = false, hasVersionMgmt = false, isDAMAdmin = false;
    Privilege p[] = UIHelper.getAllPermission(acm, resource);

    if(p[0].equals(acm.privilegeFromName(Privilege.JCR_ALL))){
        hasJcrWrite = hasReplicate = hasJcrRead = hasRemoveNode = hasModifyProperties = hasAddChild = hasModifyAccessControl = hasVersionMgmt= true;
    }
    else{
        for(int i=0;i<p.length;i++){
            if(p[i].getName().equals("rep:write") || p[i].getName().equals("jcr:write")){
                hasJcrWrite=true;
                hasRemoveNode=true;
                hasAddChild=true;
                hasModifyProperties=true;
            }
            if(p[i].getName().equals("crx:replicate")){
                hasReplicate=true;
            }
            if(p[i].getName().equals("jcr:modifyAccessControl")){
                hasModifyAccessControl=true;
            }
            if(p[i].equals(acm.privilegeFromName(Privilege.JCR_READ))){
                hasJcrRead=true;
            }
            if(p[i].equals(acm.privilegeFromName(Privilege.JCR_REMOVE_NODE))){
                hasRemoveNode=true;
            }
            if(p[i].equals(acm.privilegeFromName(Privilege.JCR_MODIFY_PROPERTIES))){
                hasModifyProperties=true;
            }
            if(p[i].equals(acm.privilegeFromName(Privilege.JCR_ADD_CHILD_NODES))){
                hasAddChild=true;
            }
            if(p[i].equals(acm.privilegeFromName(Privilege.JCR_VERSION_MANAGEMENT))){
                hasVersionMgmt =true;
            }
        }
    }
if (acm.hasPrivileges(DamUtil.getTenantAssetsRoot(resourceResolver, resourcePath), new Privilege[] {acm.privilegeFromName(Privilege.JCR_ALL)})) {
    isDAMAdmin = true;
}

//set permissions in HttpRequest
request.setAttribute(HAS_REPLICATE_ACCESS, hasReplicate);

//Flag for resource type
boolean isFolder = false;
boolean isAsset = false;
boolean isExpiredAsset = false;
boolean isExpiredSubAsset = false;
boolean isMergedPrintTemplate = isMergedPrintTemplate(resourceNode);
boolean hasIndesignTags = hasIndesignTags(resourceNode);
String type = "asset";

if (resourceNode.isNodeType("dam:Asset")) {
    isAsset = true;
    isExpiredAsset = DamUtil.isExpiredAsset(asset);
	if (!isExpiredAsset) {
		isExpiredSubAsset = DamUtil.isExpiredSubAsset(resource);
	}
}
String actionRels = StringUtils.join(getActionRels(resourceNode, hasReplicate, hasRemoveNode, hasModifyAccessControl, isExpiredAsset, isExpiredSubAsset), " ");
request.setAttribute(ACTION_RELS, actionRels);

if (resourceNode.isNodeType("nt:folder")) {
    isFolder = true;
    type = "directory";
}
request.setAttribute(IS_FOLDER, isFolder);
request.setAttribute(TYPE, type);

Tag tag = cmp.consumeTag();
request.setAttribute("tag", tag);
AttrBuilder attrs = tag.getAttrs();
     String requestSuffix = "";
    Object requestSuffixAttr = request.getAttribute("com.adobe.cq.item.requestSuffix");
    if (requestSuffixAttr != null) {
        if (requestSuffixAttr.getClass().getName().equals("java.lang.String")) {
            requestSuffix =  (String)requestSuffixAttr;
            requestSuffix = ex.get(requestSuffix, String.class);
        }
    }
   request.setAttribute(REQUEST_SUFFIX, requestSuffix);
    String requestPrefix = "";
    Object requestPrefixAttr = request.getAttribute("com.adobe.cq.item.requestPrefix");
    if (requestPrefixAttr != null) {
        if (requestPrefixAttr.getClass().getName().equals("java.lang.String")) {
            requestPrefix =  (String)requestPrefixAttr;
            requestPrefix = ex.get(requestPrefix, String.class);
        }
    }
	request.setAttribute(REQUEST_PREFIX, requestPrefix);

long ck = UIHelper.getCacheKiller(resourceNode);
request.setAttribute(CACHE_KILLER, ck);

String resourceTitle = UIHelper.getTitle(resource);
request.setAttribute(RESOURCE_TITLE, resourceTitle);

UserPropertiesManager upm = resourceResolver.adaptTo(UserPropertiesManager.class);
//Shop Desktop Link
UserProperties userProps = upm.getUserProperties(resourceResolver.adaptTo(Authorizable.class), "");
boolean showDesktopLinks = "true".equals(userProps.getProperty("preferences/showAssetDesktopLinks"));
request.setAttribute(SHOW_DESKTOP_LINKS, showDesktopLinks);

ResourceBundle resourceBundle = slingRequest.getResourceBundle(slingRequest.getLocale());
//Changing date format to satisfy CCD-385
RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", resourceBundle);


//Publish info
    String publishedDate = null ;
    long publishDateInMillis = -1;
    String publishedBy = "";
    Boolean isDeactivated = false;
    boolean enabledDynamicMedia = false;
    final Features featureManager = sling.getService(Features.class);
    if (featureManager.getFeature(EntitlementConstants.ASSETS_DYNAMICMEDIA_FEATURE_FLAG_PID)!=null &&
            featureManager.isEnabled(EntitlementConstants.ASSETS_DYNAMICMEDIA_FEATURE_FLAG_PID)) {
        enabledDynamicMedia = true;
    }
    request.setAttribute(ENABLED_DYNAMIC_MEDIA, enabledDynamicMedia);
    Resource contentRes = resource.getChild("jcr:content");

    // For type nt:folder, the properties related to resource node is present at the folder level
    if (contentRes == null) {
        contentRes = resource;
    }

    if(contentRes != null){
        ValueMap contentVm = contentRes.adaptTo(ValueMap.class);
        Calendar cal = contentVm.get("cq:lastReplicated", Calendar.class);
		//Assumption: Fetching publishedBy, isDeactivated fields only in case when cq:lastReplicated property is set 
        if (cal != null) {
            publishedDate = formatDate(cal, null, resourceBundle);

            publishDateInMillis = contentVm.get("cq:lastReplicated", Calendar.class).getTimeInMillis();


        String publishedByPrincipal = contentVm.get("cq:lastReplicatedBy", String.class);
        if (StringUtils.isNotBlank(publishedByPrincipal)) {
            String storedFormattedName = (String) request.getAttribute(publishedByPrincipal);
            if (StringUtils.isBlank(storedFormattedName)) {
                publishedBy = AuthorizableUtil.getFormattedName(resource.getResourceResolver(), publishedByPrincipal);
                request.setAttribute(publishedByPrincipal, publishedBy);
            } else {
                publishedBy = storedFormattedName;
            }
        }
        request.setAttribute(PUBLISHED_BY, publishedBy);

        String lastReplicationAction = contentVm.get("cq:lastReplicationAction", String.class);
        isDeactivated = "Deactivate".equals(lastReplicationAction) ? true : false;
        }    

    }
 request.setAttribute(PUBLISHED_DATE, publishedDate);
 request.setAttribute(PUBLISH_DATE_IN_MILLIS, publishDateInMillis);
 request.setAttribute(IS_DEACTIVATED, isDeactivated);
 request.setAttribute(IS_MERGED_PRINT_TEMPLATE, isMergedPrintTemplate);

//Meta resource

AttrBuilder metaAttrs = new AttrBuilder(request, xssAPI);

boolean timeLine = false;

if (isAsset) {
    timeLine = true;
}
request.setAttribute(TIMELINE, timeLine);
metaAttrs.addBoolean("hidden", true);
metaAttrs.add("data-timeline", timeLine);
metaAttrs.addClass("foundation-collection-assets-meta");
metaAttrs.add("data-foundation-collection-meta-title", resourceTitle);
metaAttrs.add("data-foundation-collection-meta-folder", isFolder);
metaAttrs.add("data-foundation-collection-meta-type", type);
metaAttrs.add("data-foundation-collection-meta-path", resourcePath);
metaAttrs.add("data-foundation-collection-meta-rel", actionRels);
if(hasIndesignTags) {
    metaAttrs.add("data-has-indesign-tags", hasIndesignTags);
}

//display language
String displayLanguage = getDisplayLanguage(resource.getPath(), false);

//allow navigation
 boolean allowNavigation = true;
    Object allowNavigationAttr = request.getAttribute(NAVIGATION_ALLOWED);
    if (allowNavigationAttr != null) {
        if (allowNavigationAttr.getClass().getName().equals("java.lang.String")) {
            allowNavigation =  ((String)allowNavigationAttr).equals("true");
        } else {
            allowNavigation = ((Boolean)allowNavigationAttr).booleanValue();
        }
    }

%>