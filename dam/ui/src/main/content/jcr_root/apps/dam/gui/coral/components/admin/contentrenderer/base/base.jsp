<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page import="org.apache.sling.api.resource.Resource,
				  javax.jcr.Node,
				  javax.jcr.RepositoryException,
				  javax.jcr.Session,
				  javax.jcr.security.AccessControlManager,
				  javax.jcr.security.Privilege,
				  javax.jcr.PathNotFoundException,
				  javax.jcr.ValueFormatException,
				  java.util.List,
                  java.util.Locale,
				  java.util.ArrayList,
                  java.util.Calendar,
				  java.util.ResourceBundle,
				  org.apache.commons.lang.StringUtils,
				  org.apache.jackrabbit.util.Text,
				  org.apache.jackrabbit.api.security.user.Authorizable,
				  org.apache.sling.api.resource.ResourceResolver,
                  org.apache.sling.api.resource.ValueMap,
				  com.adobe.granite.ui.components.AttrBuilder,
				  com.adobe.granite.security.user.UserProperties,
                  com.adobe.granite.security.user.UserPropertiesManager,
				  com.adobe.granite.security.user.util.AuthorizableUtil,
				  com.adobe.granite.ui.components.Tag,
				  com.adobe.granite.ui.components.ExpressionHelper,
				  com.day.cq.dam.commons.util.DamUtil,
				  com.day.cq.dam.commons.util.DamLanguageUtil,
				  com.day.cq.dam.api.Rendition,
				  com.day.cq.dam.api.Asset,
				  com.day.cq.dam.api.DamConstants,
                  com.day.cq.commons.LanguageUtil,
                  com.adobe.dam.print.ids.StringConstants,
				  com.day.cq.commons.date.RelativeTimeFormat,
				  com.day.cq.dam.commons.util.UIHelper"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><%--###
Base API
=========
This JSP is included by spawned pages, hence it may gets called multiple time. This JSP should not evaluate or perform any execution task,
rather it should only contain methods & configs to avoid performance overhead.
###--%><%
//Attributes set by \contentrenderer\base\init\base.jsp should be accessed through following constants

final String ASSET = "com.adobe.cq.assets.contentrenderer.asset";

final String RESOURCE_NODE = "com.adobe.cq.assets.contentrenderer.resourceNode";                      

final String REQUEST_PREFIX = "com.adobe.cq.assets.contentrenderer.requestPrefix";

final String REQUEST_SUFFIX = "com.adobe.cq.assets.contentrenderer.requestSuffix";

final String CACHE_KILLER = "com.adobe.cq.assets.contentrenderer.ck";

final String ENABLED_DYNAMIC_MEDIA = "com.adobe.cq.assets.contentrenderer.enabledDynamicMedia";     

final String PUBLISH_DATE_IN_MILLIS = "com.adobe.cq.assets.contentrenderer.publishDateInMillis";  

final String PUBLISHED_DATE = "com.adobe.cq.assets.contentrenderer.publishedDate"; 

final String IS_DEACTIVATED = "com.adobe.cq.assets.contentrenderer.isDeactivated"; 

final String PUBLISHED_BY = "com.adobe.cq.assets.contentrenderer.publishedBy"; 

final String SHOW_DESKTOP_LINKS = "com.adobe.cq.assets.contentrenderer.showDesktopLinks"; 

final String RESOURCE_PATH = "com.adobe.cq.assets.contentrenderer.resourcePath";

final String TYPE = "com.adobe.cq.assets.contentrenderer.type";

final String RESOURCE_TITLE = "com.adobe.cq.assets.contentrenderer.resourceTitle";

final String TIMELINE = "com.adobe.cq.assets.contentrenderer.timeLine";

final String IS_FOLDER = "com.adobe.cq.assets.contentrenderer.isFolder";

final String ACTION_RELS = "com.adobe.cq.assets.contentrenderer.actionRels"; 

final String SHOW_QUICK_ACTIONS = "com.adobe.cq.assets.contentrenderer.showQuickActions"; 

final String NAVIGATION_ALLOWED = "com.adobe.cq.assets.contentrenderer.allowNavigation";

final String IS_MERGED_PRINT_TEMPLATE = "com.adobe.cq.assets.contentrenderer.asset.isMergedPrintAsset";

final String HAS_REPLICATE_ACCESS = "com.adobe.cq.assets.contentrenderer.replicateAccess";

%><%!

private List<String> getActionRels(Resource resource, Asset asset, AccessControlManager acm) {
    List<String> actionRels = new ArrayList<String>();
    return actionRels;
}

private String getThumbnailUrl(Asset asset, int width, 
        boolean showOriginalIfNoRenditionAvailable, boolean showOriginalForGifImages) {
    String thumbnailUrl="";
    int thumbnailWidth =0;
    String mimeType = asset.getMimeType();
    Rendition thumbnailRendition = UIHelper.getBestfitRendition(asset, width);
        if (thumbnailRendition != null) {

            if ("image/gif".equals(mimeType) && showOriginalForGifImages) {
                Rendition originalRendition = asset.getRendition(DamConstants.ORIGINAL_FILE);
                return Text.escapePath(originalRendition.getPath());
            }

            if (DamConstants.ORIGINAL_FILE.equals(thumbnailRendition.getName())
                    && !showOriginalIfNoRenditionAvailable) {
                return Text.escapePath(asset.getPath() + ".thumb.319.319.png");
            }

            try {
                //use the existing thumbnail
                thumbnailWidth = (int) width;
                thumbnailUrl = asset.getPath() + "/jcr:content/renditions/" + thumbnailRendition.getName();
            } catch (Exception e) {
            }
        } else {
            //default thumbnail
            thumbnailUrl = asset.getPath() + ".thumb.319.319.png";
        }

        thumbnailUrl = Text.escapePath(thumbnailUrl);
        return thumbnailUrl;
}

private List<String> getActionRels(boolean hasReplicate,boolean hasRemoveNode, boolean hasModifyAccessControl, boolean isExpiredAsset, boolean isExpiredSubAsset) {
	return getActionRels(null, hasReplicate, hasRemoveNode, hasModifyAccessControl, isExpiredAsset, isExpiredSubAsset);
}

private List<String> getActionRels(Node node, boolean hasReplicate,boolean hasRemoveNode, boolean hasModifyAccessControl, boolean isExpiredAsset, boolean isExpiredSubAsset) {
    List<String> actionRels = new ArrayList<String>();
    boolean isDMSet = false;
    if(node != null){
    	try{
    	    if (node.hasProperty("jcr:content/dam:s7damType")) {
                String dmType = node.getProperty("jcr:content/dam:s7damType").getString();
                if (dmType.endsWith("Set")){
            	    isDMSet = true;
                }
            }
    	} catch(Exception ex){
    	}
    }
    actionRels.add("cq-damadmin-admin-actions-removefromcollection");
    if(!isExpiredAsset && !isExpiredSubAsset) {
        actionRels.add("cq-siteadmin-admin-actions-copy-activator");

        if (hasReplicate) {
            actionRels.add("cq-damadmin-admin-actions-publish-activator");
            actionRels.add("cq-siteadmin-admin-actions-publish-activator");
            if (!isDMSet) {
                actionRels.add("cq-damadmin-admin-actions-mpshare-activator");
            }
        }

        if (hasRemoveNode) {
            actionRels.add("cq-damadmin-admin-actions-move-activator");
        }

        if (hasModifyAccessControl) {
            actionRels.add("cq-damadmin-admin-actions-adhocassetshare-activator");
            actionRels.add("cq-damadmin-admin-actions-share-activator");
        }
    }

    if (hasReplicate) {
            actionRels.add("cq-damadmin-admin-actions-unpublish-activator");
            actionRels.add("cq-siteadmin-admin-actions-unpublish-activator");
            if (!isDMSet) {
                actionRels.add("cq-damadmin-admin-actions-mpunshare-activator");
            }
    }
    if (hasRemoveNode) {
            actionRels.add("cq-damadmin-admin-actions-delete-activator");
    }
    return actionRels;
}
//Changing date format to satisfy CCD-385
private String formatDate(Calendar cal, String defaultValue, ResourceBundle rb) {
        if (cal == null) return defaultValue;

        RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", rb);
        return rtf.format(cal.getTimeInMillis(), true);
    }

private String getDisplayLanguage(String sourcePath, Boolean asset) {
		String root = DamLanguageUtil.getLanguageRoot(sourcePath);
		String locale = null;
		Locale localeObj = null;
		String langValue = "";
		if (root == null) {
			root = LanguageUtil.getLanguageRoot(sourcePath);
			if (root != null && !(asset && root.equals(sourcePath))) {
				locale = root.substring(root.lastIndexOf("/") + 1);
			}
		} else {
			if (!(asset && root.equals(sourcePath))) {
				locale = root.substring(root.lastIndexOf("-") + 1);
			}
		}

		if (locale != null) {
			localeObj = LanguageUtil.getLocale(locale);
			if (localeObj != null) {
				langValue = localeObj.getDisplayLanguage();
			}
			langValue += " (" + locale + ")";
		}
		return langValue;
	}

    private boolean isMergedPrintTemplate(Node resourceNode) {
        try {
            return (resourceNode.hasProperty("jcr:content/metadata/"+StringConstants.MERGED_TYPE) &&
                    resourceNode.getProperty("jcr:content/metadata/"+StringConstants.MERGED_TYPE).getString().equals(StringConstants.MERGED_TYPE_TEMPLATE));
        } catch (Exception ex) {
            log("Exception occurred while checking whether the asset is Merged print Template "
                    + ex.getMessage());
        }
        return false;
    }

    private boolean isMergedPrintAsset(Node resourceNode) {
        try {
            return (resourceNode.hasNode("jcr:content/related/"+StringConstants.CATALOG_RELATION));
        } catch (Exception ex) {
            log("Exception occurred while checking whether the asset is Merged print asset "
                    + ex.getMessage());
        }
        return false;
    }

    private boolean hasIndesignTags(Node resourceNode){
        try {
            return (resourceNode.hasNode("jcr:content/renditions/"+getResourceName(resourceNode)+".xml"));
        } catch (Exception ex) {
            log("Exception occurred while checking whether the asset has indesign tag's xml exported "
                    + ex.getMessage());
        }
        return false;
    }

    private static String getResourceName(Node resNode) throws RepositoryException {
        //get the name of the ID print template
        String name = resNode.getName();
        int idx = name.lastIndexOf('.');
        if(idx > 0) {
            name = name.substring(0, idx);
        }
        return name;
    }

%>