 <%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%@page import="org.apache.sling.api.resource.Resource"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/assetBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/insightBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/row/common/common.jsp"%><%

boolean showOriginalIfNoRenditionAvailable = (request!=null && request.getAttribute("showOriginalIfNoRenditionAvailable")!=null) ? (Boolean)request.getAttribute("showOriginalIfNoRenditionAvailable") : false;
boolean showOriginalForGifImages = (request!=null && request.getAttribute("showOriginalForGifImages")!=null) ? (Boolean)request.getAttribute("showOriginalForGifImages") : false;

boolean lowResolution = false;
String assetLength = asset.getMetadataValue("tiff:ImageLength");
String assetWidth = asset.getMetadataValue("tiff:ImageWidth");

Resource metadataResource = resource.getChild("jcr:content").getChild("metadata");

String stockNumber = metadataResource.getValueMap().get("dam:JLPProductCodes","");
String category = metadataResource.getValueMap().get("category","");
String clientStylingNotes = metadataResource.getValueMap().get("clientStylingNotes","");
String ids = metadataResource.getValueMap().get("ids","");
String imageRequirements = metadataResource.getValueMap().get("imageRequirements","");
String productType = metadataResource.getValueMap().get("productType","");
String range = metadataResource.getValueMap().get("range","");
String productName = metadataResource.getValueMap().get("dam:JLPProductTitle","");
String webSKU = metadataResource.getValueMap().get("dam:JLPItemId","");
String description = metadataResource.getValueMap().get("dam:wsmTitle","");
String ean = metadataResource.getValueMap().get("dam:eanTradedCode","");
String jlstatus = metadataResource.getValueMap().get("status","");
String supplier = metadataResource.getValueMap().get("dam:supplierContactDetail","");
String colour = metadataResource.getValueMap().get("dam:colour","");
String brand = metadataResource.getValueMap().get("dam:brandName","");
String briefname = metadataResource.getValueMap().get("dam:JLPBriefName","");
Calendar dDate = metadataResource.getValueMap().get("dam:JLPBriefDueDate", Calendar.class);
String deliverydate = null;   
    
if (dDate == null) {
    deliverydate = "";
} else {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    deliverydate = sdf.format(dDate.getTime());
}

if(assetLength!=null && assetWidth!=null){
    try{
        int len = Integer.parseInt(assetLength);
        int wid =  Integer.parseInt(assetWidth);
        if(len < 48 || wid < 48) {
            lowResolution = true;
        }
    }catch (NumberFormatException nfe){
        log.error("Exception:" + nfe);
    }
}

String thumbnailUrl = request.getContextPath() + requestPrefix 
+ getThumbnailUrl(asset, 48, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
//Override default thumbnail for set when there is manual thumbnail defined
if (dmSetManualThumbnailAsset != null) {
    thumbnailUrl = request.getContextPath() + requestPrefix 
    		+ getThumbnailUrl(asset, 1280, showOriginalIfNoRenditionAvailable, showOriginalForGifImages) + "?ch_ck=" + ck + requestSuffix;
}

String assetActionRels = StringUtils.join(getAssetActionRels(hasJcrRead, hasJcrWrite, hasAddChild, canEdit, canAnnotate, isDownloadAllowedForAdmins, isAssetExpired, isSubAssetExpired, isContentFragment, isArchive), " ");

request.setAttribute("actionRels", actionRels.concat(" " + assetActionRels));
if (allowNavigation) {
attrs.addClass("foundation-collection-navigator");
}
%>
<cq:include script="link.jsp"/>
<%

    if (request.getAttribute("com.adobe.assets.card.nav")!=null){
        navigationHref =  (String) request.getAttribute("com.adobe.assets.card.nav");
    }
attrs.add("data-foundation-collection-navigator-href", xssAPI.getValidHref(navigationHref));

attrs.add("is", "coral-table-row");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);

ValueMap resourceProperties = resource.getValueMap();
WorkflowStatus workflowStatus = resource.adaptTo(WorkflowStatus.class);

request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);

%>
<tr <%= attrs %>>
    <td is="coral-table-cell" coral-table-rowselect><%
        if (isArchive) {
            %><coral-icon class="foundation-collection-item-thumbnail" icon="fileZip" size="S"></coral-icon><%
        } else {
            if(lowResolution){
                %><img class="foundation-collection-item-thumbnail" src="<%= xssAPI.getValidHref(thumbnailUrl)%>" alt="" style="height: auto; width: auto;"><%
            } else {
                %><img class="foundation-collection-item-thumbnail" src="<%= xssAPI.getValidHref(thumbnailUrl)%>" alt="" ><%
            }
        }%>
    </td>
    <td class="foundation-collection-item-title" is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>"><%= xssAPI.encodeForHTML(resourceTitle) %>
        <cq:include script = "status.jsp"/>
    </td>

    <td is="coral-table-cell" value="<%= displayLanguage %>"><%= displayLanguage %></td>
    <cq:include script = "status-lister.jsp"/>

    <td class="encodingstatus foundation-collection-item-title" is="coral-table-cell">
        <cq:include script = "encodingstatus.jsp"/>
    </td>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(displayMimeType) %>"><%= xssAPI.encodeForHTML(displayMimeType) %></td>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resolution) %>"><%= xssAPI.encodeForHTML(resolution)%></td>
    <td is="coral-table-cell" value="<%= bytes %>"><%= size %></td>
    <cq:include script = "rating.jsp"/>
    <td is="coral-table-cell" value="<%= assetUsageScore %>"><%= assetUsageScore %></td>
    <td is="coral-table-cell" value="<%= assetImpressionScore %>"><%= assetImpressionScore %></td>
    <td is="coral-table-cell" value="<%= assetClickScore %>"><%= assetClickScore %></td>
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%
        if (lastModified != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(assetLastModification)) %>"><%= xssAPI.encodeForHTML(lastModified) %></time><%

            // Modified-after-publish indicator
                if (publishDateInMillis > 0 && publishDateInMillis < assetLastModification) {
                String modifiedAfterPublishStatus = i18n.get("Modified since last publication");
                %><coral-icon icon="alert" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(modifiedAfterPublishStatus) %>"></coral-icon><%
            }

            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(lastModifiedBy) %></div><%
        }
    %></td>
     <td is="coral-table-cell" value="<%= (!isDeactivated && publishedDate != null) ? xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) : "0" %>"><%
        // Published date and status
        if (!isDeactivated && publishedDate != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) %>"><%= xssAPI.encodeForHTML(publishedDate) %></time><%
        } else {
            %><span><%= xssAPI.encodeForHTML(i18n.get("Not published")) %></span><%
        }

        // Publication/un-publication pending indicator
        String publicationPendingStatus = getPublicationPendingStatus(replicationStatus, i18n);
        if (publicationPendingStatus.length() > 0) {
            %><coral-icon icon="pending" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(publicationPendingStatus) %>"></coral-icon><%
        }

        // On/Off time indicator
        String onOffTimeStatus = getOnOffTimeStatus(resourceProperties, i18n, request.getLocale());
        if (onOffTimeStatus.length() > 0) {
            %><coral-icon icon="clock" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(onOffTimeStatus) %>"></coral-icon><%
        }

        // Publication/un-publication scheduled indicator
        List<Workflow> scheduledWorkflows = getScheduledWorkflows(workflowStatus);
        if (!isDeactivated && scheduledWorkflows.size() > 0) {
            String scheduledStatus = getScheduledStatus(scheduledWorkflows, i18n, request.getLocale(), resourceResolver);
            if (scheduledStatus.length() > 0) {
                %><coral-icon icon="calendar" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(scheduledStatus) %>"></coral-icon><%
            }
        }

        // Published by
        if (!isDeactivated && publishedBy != null) {
            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(publishedBy) %></div><%
        }

        %><cq:include script = "applicableRelationships.jsp"/>
    </td>
    <td is="coral-table-cell" value="<%= isCheckedOut ? xssAPI.encodeForHTMLAttr(checkedOutByFormatted) : "0" %>">
        <%
            // Checkout Status
            if (isCheckedOut) {
                String titleDisplay = i18n.get("Checked Out By {0}", "name inserted to variable", checkedOutByFormatted);
                %><coral-icon icon="lockOn" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(titleDisplay) %>"><%
            }
        %>
    </td>
    <%    String comment = DamUtil.getValue(metadataNode, "dam:JLPComment", ""); %>
    <td is="coral-table-cell" value="<%= comment %>"><%= comment %></td>
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for metadata profile-->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for image profile-->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for video profile-->
    <td is="coral-table-cell" value="<%=stockNumber%>"><%=stockNumber%></td>
    <td is="coral-table-cell" value="<%=category%>"><%=category%></td>
    <td is="coral-table-cell" value="<%=clientStylingNotes%>"><%=clientStylingNotes%></td>
	<td is="coral-table-cell" value="<%=ids%>"><%=ids%></td>
    <td is="coral-table-cell" value="<%=imageRequirements%>"><%=imageRequirements%></td>
    <td is="coral-table-cell" value="<%=productType%>"><%=productType%></td>
    <td is="coral-table-cell" value="<%=range%>"><%=range%></td>
    <td is="coral-table-cell" value="<%=productName%>"><%=productName%></td>
    <td is="coral-table-cell" value="<%=webSKU%>"><%=webSKU%></td>
    <td is="coral-table-cell" value="<%=description%>"><%=description%></td>
    <td is="coral-table-cell" value="<%=ean%>"><%=ean%></td>
    <td is="coral-table-cell" value="<%=jlstatus%>"><%=jlstatus%></td>
    <td is="coral-table-cell" value="<%=supplier%>"><%=supplier%></td>
    <td is="coral-table-cell" value="<%=colour%>"><%=colour%></td>
    <td is="coral-table-cell" value="<%=brand%>"><%=brand%></td>
    <td is="coral-table-cell" value="<%= briefname %>"><%=briefname%></td>
	<td is="coral-table-cell" value="<%= deliverydate %>"><%=deliverydate%></td>

    <cq:include script = "reorder.jsp"/>
    <cq:include script = "meta.jsp"/>
</tr><%!

%>
