/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2016 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */

(function (document, $) {
    "use strict";

    var $document = $(document);
    var $userPicker = $document.find(".search-predicate-owner-userpicker");
    var $userPickerList = $document.find(".search-predicate-owner-userpicker .coral-SelectList");
    var authorizablesCache = {};

    function resolveUserNameById(userId) {
        var userString = "";
        var servletUrl = Granite.HTTP.externalize('/libs/granite/security/search/authorizables.json?query={"condition":[{"named":"' + userId + '"}]}');
        var userName = userId;
        if (!authorizablesCache.hasOwnProperty(userId)) {
            authorizablesCache[userId] = "";
            $.ajax({
                url: servletUrl,
                type: "GET",
                dataType: "json",
                async: false,
                success: function(responseJson) {
                    authorizablesCache[userId] = userName;
                    var userData = {};
                    if (responseJson.hasOwnProperty("authorizables") && responseJson.authorizables.length) {
                        userData = responseJson.authorizables[0];
                        if (userData.hasOwnProperty("name")) {
                            userName = userData.name;
                            authorizablesCache[userId] = userName;
                        }
                    }
                }
            });
        }
        else {
            userName = authorizablesCache[userId];
        }
        return userName;
    };

    function getValueElement($ancestor, name) {
        return $ancestor.find("input[type=hidden][name='" + name + ".value" +
            "'].js-coral-Autocomplete-hidden");
    }

    function updateTag(tagList, conf, tag, name) {
        if (tag === null) {
            tag = new Coral.Tag();
            // used to search for it afterwards
            tag.setAttribute("name", name);
            tagList.items.add(tag);
        }

        tag.set({
            label: {
                innerHTML: $("<span class='u-coral-text-capitalize u-coral-text-italic u-coral-text-secondary'>")
                    .text(conf["graniteOmnisearchTypeaheadSuggestionTag"].toString() + ":").prop("outerHTML")
                    + $("<span/>").text(conf["graniteOmnisearchTypeaheadSuggestionValue"].toString()).prop("outerHTML")
            }
        });

        return tag;
    }

    $document.off("granite-omnisearch-predicate-clear.owner")
        .on("granite-omnisearch-predicate-clear.owner", function(event) {

            if (event.detail.reset) {
                return;
            }
            var $form = $(event.target);
            var name = event.detail.item.getAttribute("name");
            var autocomplete;

            var $value = getValueElement($form, name);

            if ($value.length && $value.attr("value") !== "") {
                autocomplete = $userPicker.data("autocomplete");
                if (autocomplete) {
                    autocomplete.clear();
                } else {
                    $value.attr("value", "");
                }
                $form.submit();
            }
    });

    $document.off("granite-omnisearch-predicate-update.owner")
        .on("granite-omnisearch-predicate-update.owner", function(event) {
            var queryParameters = event.detail.queryParameters;
            if (!queryParameters) {
                return false;
            }
            var tagList = event.detail.tagList;
            var $predicate = $('.owner-predicate');
            var name = $predicate.data("name");
            var userName = "";

            var $value = getValueElement($predicate, name);
            var $breadcrumb = $predicate.find("input[type=hidden][name='" + name + ".breadcrumbs" + "']");

            var value = queryParameters[name + ".value"];
            if (!value) {
                $value.attr("value", "");
                return false;
            }
            userName = resolveUserNameById(value);
            var conf = {
                name: name,
                graniteOmnisearchTypeaheadSuggestionTag: $breadcrumb.val(),
                // TODO: this is the username, not the display name
                graniteOmnisearchTypeaheadSuggestionValue: userName
            }
            var tag = tagList.querySelector("coral-tag[name='" + name + "']");

            updateTag(tagList, conf, tag, name);
            $value.attr("value", value);

            // makes sure the Coral internals are not modified
            $predicate.children("input[type=hidden]").each(function() {
                this.disabled = !($(this).val());
            });
    });

    $userPickerList.off("selected.owner").on("selected.owner", function(event) {

        var $this = $(this);
        var $form = $this.closest(".granite-omnisearch-form");
        var $predicate = $this.closest(".owner-predicate");
        var tagList = document.querySelector(".granite-omnisearch-typeahead-tags");

        var displayedValue = event.displayedValue;

        var name = $predicate.data("name");
        var tag = tagList.querySelector("coral-tag[name='" + name + "']");

        if (displayedValue) {
            // enable hidden inputs
            $this.parent().find(".search-predicate-owner-option").prop("disabled", false);

            // display predicate tag
            var $breadcrumb = $predicate.find("input[type=hidden][name='" + name + ".breadcrumbs" + "']");
            var conf = {
                graniteOmnisearchTypeaheadSuggestionTag: $breadcrumb.val(),
                graniteOmnisearchTypeaheadSuggestionValue: displayedValue
            };
            updateTag(tagList, conf, tag, name);
        }

        // makes sure the Coral internals are not modified
        $predicate.children("input[type=hidden]").each(function() {
            this.disabled = !($(this).val());
        });

        $form.submit();

    });


})(document, Granite.$);
