<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%>
<%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/base.jsp"%><%
%><%@page session="false"%><%
%><%@page import="java.util.Calendar,
				  java.util.Locale,
				  com.day.cq.dam.api.Rendition,
				  com.day.cq.dam.commons.util.UIHelper,
				  com.day.cq.dam.commons.util.DamUtil,
				  javax.jcr.Node,
				  com.day.cq.dam.api.DamConstants,
                  org.apache.commons.lang.StringUtils,
				  org.apache.jackrabbit.JcrConstants,
				  javax.jcr.RepositoryException,
				  com.day.cq.dam.commons.util.S73DHelper,
				  org.apache.commons.io.FilenameUtils,
				  org.apache.jackrabbit.util.Text,
				  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.api.resource.ResourceUtil,
				  com.adobe.granite.security.user.util.AuthorizableUtil,
                  com.adobe.granite.security.user.UserPropertiesManager,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  com.day.cq.dam.api.Asset,
                  org.apache.sling.api.resource.Resource, org.apache.sling.api.resource.ResourceResolver,
                  com.day.cq.dam.api.checkout.AssetCheckoutService" %><%

Config cfg = cmp.getConfig();
String path = cmp.getExpressionHelper().getString(cfg.get("path", String.class));

if (path == null) return;

Resource contentResource = resourceResolver.getResource(path);

if (contentResource == null) return;

String thumbnailUrl = "";
Asset asset = contentResource.adaptTo(Asset.class);
String title = UIHelper.getTitle(contentResource);
if (asset != null && isContentFragment(asset)) {
    title = getFragmentTitle(contentResource, title);
}
String name = contentResource.getName();

String mimeType = "";
String displayMimeType="";

boolean isDirectory = false;

boolean showOriginalIfNoRenditionAvailable = cfg.get("showOriginalIfNoRenditionAvailable", false);
boolean showOriginalForGifImages = cfg.get("showOriginalForGifImages", false);
request.setAttribute("showOriginalIfNoRenditionAvailable", showOriginalIfNoRenditionAvailable);
request.setAttribute("showOriginalForGifImages", showOriginalForGifImages);

if (asset != null) {

    thumbnailUrl = request.getContextPath() + getThumbnailUrl(asset, 480, showOriginalIfNoRenditionAvailable, showOriginalForGifImages);
    // display mime type is shown on card (in grid view) and in list view
    boolean is3D = S73DHelper.isS73D(resource);

    Resource lookupResource = resourceResolver.getResource("/mnt/overlay/dam/gui/content/assets/jcr:content/mimeTypeLookup");

    //mimeType
    if (is3D) {
        Resource assetContent = resource.getChild("jcr:content");
        displayMimeType = (assetContent != null) ? assetContent.getValueMap().get("dam:s7damType", "").toUpperCase() : "";
        String extension = FilenameUtils.getExtension(resource.getName());
        if (!extension.isEmpty()) {
            displayMimeType += " (" + extension + ")";
        }
    }
    else if (asset.getMimeType() != null) {
        mimeType = asset.getMimeType();
        String ext = mimeType.substring(mimeType.lastIndexOf('/') + 1, mimeType.length());
        if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
            displayMimeType = "";
        }
        if (displayMimeType.length() == 0 && mimeType.startsWith("image")) {
            displayMimeType = "IMAGE";
        } else if (displayMimeType.length() == 0 && mimeType.startsWith("text")) {
            displayMimeType = "DOCUMENT";
        } else if (displayMimeType.length() == 0 && (mimeType.startsWith("video") || mimeType.startsWith("audio"))) {
            displayMimeType = "MULTIMEDIA";
        } else if (displayMimeType.length() == 0 && mimeType.startsWith("application")) {
            int idx_1 = ext.lastIndexOf('.');
            int idx_2 = ext.lastIndexOf('-');
            int lastWordIdx = (idx_1 > idx_2)?idx_1:idx_2;
            displayMimeType = ext.substring(lastWordIdx+1).toUpperCase();
        }
    }

    if (displayMimeType.length() == 0 && asset.getName() != null) {
        String filename = asset.getName();
        String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
            displayMimeType = "";
        }
    }
} else {
    // Path at which the manual thumbnail(if present) exists
    String manualThumbnailPath = contentResource.getPath() + "/jcr:content/manualThumbnail.jpg";
    Resource manualThumbnail = resourceResolver.getResource(manualThumbnailPath);
    if (null != manualThumbnail) {
        thumbnailUrl = Text.escapePath(contentResource.getPath()) + "/jcr:content/manualThumbnail.jpg";
    } else {
        thumbnailUrl = Text.escapePath(contentResource.getPath()) + ".folderthumbnail.jpg";
    }
    isDirectory = true;
}

Locale locale = request.getLocale();
String assetDetailsVanity = "/assetdetails.html";
// Changing date format to satisfy CCD-385
RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", slingRequest.getResourceBundle(locale));
UserPropertiesManager upm = resourceResolver.adaptTo(UserPropertiesManager.class);

long width = 0;
long height = 0;
String size = "";
String modified = null, modifiedBy = "";

boolean isCheckedOut = false;
String checkedOutBy = "";
String formattedCheckedOutBy = "";

String[] profileTitleList = new String[]{"","",""};
String[] profilePropertyList = new String[]{"jcr:content/metadataProfile", "jcr:content/imageProfile", "jcr:content/videoProfile"};
String[] profileNamePropertyList = new String[]{"jcr:content/jcr:title", "name", "jcr:title"};
String metadataSchemaDisplayName = "";

Node contentRscNode = contentResource.adaptTo(Node.class);
if (!isDirectory) {
Node metadataNode = contentRscNode.getNode(JcrConstants.JCR_CONTENT + "/" + DamConstants.METADATA_FOLDER);

try {
	width = Long.valueOf(DamUtil.getValue(metadataNode, DamConstants.TIFF_IMAGEWIDTH, DamUtil.getValue(metadataNode, DamConstants.EXIF_PIXELXDIMENSION, "")));
	height = Long.valueOf(DamUtil.getValue(metadataNode, DamConstants.TIFF_IMAGELENGTH, DamUtil.getValue(metadataNode, DamConstants.EXIF_PIXELYDIMENSION, "")));
} catch(NumberFormatException nfe) {
	//eat it as this is non-image asset
}

long bytes = Long.valueOf(DamUtil.getValue(metadataNode, "dam:size", "0"));
if (bytes == 0 && asset.getOriginal() != null) {
    bytes = asset.getOriginal().getSize();
}
if (bytes != 0) {
size = UIHelper.getSizeLabel(bytes, slingRequest);
}
    //lastModified & lastModifiedBy
        long lastModification = asset.getLastModified();
        if (lastModification == 0) {
            ValueMap contentResourceVM = contentResource.adaptTo(ValueMap.class);
            Calendar created = contentResourceVM.get("jcr:created", Calendar.class);
            lastModification = (null != created) ? created.getTimeInMillis() : 0;
        }
        modified = outVar(xssAPI, i18n, rtf.format(lastModification, true));
        String modifier = asset.getModifier();
        if (StringUtils.isNotBlank(modifier)) {
            String storedFormattedName = (String) request.getAttribute(modifier);
            if (StringUtils.isBlank(storedFormattedName)) {
                modifiedBy = AuthorizableUtil.getFormattedName(contentResource.getResourceResolver(), modifier);
                request.setAttribute(modifier, modifiedBy);
            } else {
                modifiedBy = storedFormattedName;
            }

        } else {
            modifier = "";
        }
        // Also check asset modifier should not be empty. see CQ-39542
        if (!"".equals(modifier) && upm.getUserProperties(modifier, "profile") == null) {
            modifiedBy = i18n.get("External User");
        }

    AssetCheckoutService assetCheckoutService = sling.getService(AssetCheckoutService.class);
    if (assetCheckoutService != null && assetCheckoutService.isCheckedOut(asset)) {
        isCheckedOut = true;
        checkedOutBy = assetCheckoutService.getCheckedOutBy(asset);
        formattedCheckedOutBy = AuthorizableUtil.getFormattedName(contentResource.getResourceResolver(), checkedOutBy);
    }
} else {
     // Processing profile details
    for(int i=0; i<3; i++) {
        if (contentRscNode.hasProperty(profilePropertyList[i])) {
            String profilePath = contentRscNode.getProperty(profilePropertyList[i]).getValue().getString();
            if(profilePath.trim().isEmpty()){
                continue;
            }
            Resource res = resourceResolver.getResource(profilePath);
            if (res != null) {
                Node node = res.adaptTo(Node.class);
                if (node != null) {
                    profileTitleList[i] = node.getName();
                    if (node.hasProperty(profileNamePropertyList[i])) {
                        String jcrTitle = node.getProperty(profileNamePropertyList[i]).getValue().getString();
                        if (jcrTitle != null && !jcrTitle.trim().isEmpty()) {
                            profileTitleList [i] = jcrTitle;
                        }
                    }
                }
            }
        }
    }

    // Metadata Schema details
    String metadataSchema = "";
    if (contentRscNode.hasProperty("jcr:content/metadataSchema")) {
        metadataSchema = contentRscNode.getProperty("jcr:content/metadataSchema").getValue().getString();
        if(!metadataSchema.isEmpty() && metadataSchema.indexOf("/metadataschema/") != -1) {
            metadataSchemaDisplayName = metadataSchema.substring(metadataSchema.indexOf("/metadataschema/") + ("/metadataschema/").length());
        }
    }

}




%><coral-columnview-preview><coral-columnview-preview-content>
    <coral-columnview-preview-asset>
        <img src="<%= xssAPI.getValidHref(thumbnailUrl) %>" alt="">
    </coral-columnview-preview-asset>
    <coral-columnView-preview-label><%= i18n.get("Title") %></coral-columnView-preview-label>
    <coral-columnview-preview-value><%= xssAPI.encodeForHTML(title) %></coral-columnview-preview-value><%
    if (name != null && !name.equals(title)) {%>
        <coral-columnView-preview-label><%= i18n.get("Name") %></coral-columnView-preview-label>
        <coral-columnview-preview-value><%= xssAPI.encodeForHTML(name) %></coral-columnview-preview-value><%
    }
    if (modified != null && modifiedBy!= null && modifiedBy.trim().length() > 0) {
             %><coral-columnView-preview-label><%= i18n.get("Modified") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(modified) %></coral-columnview-preview-value>

            <coral-columnView-preview-label><%= i18n.get("Modified By") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(modifiedBy) %></coral-columnview-preview-value><%
    } %>

    <%if (!isDirectory && isCheckedOut) {
            %><coral-columnView-preview-label><%= i18n.get("Checked Out By") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(formattedCheckedOutBy) %></coral-columnview-preview-value><%
    }%>

	<%if (width != 0 && height != 0) {
        %><coral-columnView-preview-label><%= i18n.get("Dimensions") %></coral-columnView-preview-label>
          <coral-columnview-preview-value><%= xssAPI.encodeForHTML(width + " x " + height+" px") %></coral-columnview-preview-value><%
    }%>

	<%if (size.length() > 0) {    %>
          <coral-columnView-preview-label><%= i18n.get("Size") %></coral-columnView-preview-label>
    	  <coral-columnview-preview-value><%= size %></coral-columnview-preview-value><%
	 }%>

    <%if (!isDirectory && displayMimeType != null) {
            %><coral-columnView-preview-label><%= i18n.get("Type") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(displayMimeType) %></coral-columnview-preview-value><%
    }%>

	<%if (isDirectory && !profileTitleList[0].trim().isEmpty()) {
            %><coral-columnView-preview-label><%= i18n.get("Metadata Profile") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(profileTitleList[0].trim()) %></coral-columnview-preview-value><%
    }%>

	<%if (isDirectory && !profileTitleList[1].trim().isEmpty()) {
            %><coral-columnView-preview-label><%= i18n.get("Image Profile") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(profileTitleList[1].trim()) %></coral-columnview-preview-value><%
    }%>

	<%if (isDirectory && !profileTitleList[2].trim().isEmpty()) {
            %><coral-columnView-preview-label><%= i18n.get("Video Profile") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(profileTitleList[2].trim()) %></coral-columnview-preview-value><%
    }%>

	<%if (isDirectory && !metadataSchemaDisplayName.isEmpty()) {
            %><coral-columnView-preview-label><%= i18n.get("Metadata Schema") %></coral-columnView-preview-label>
            <coral-columnview-preview-value><%= xssAPI.encodeForHTML(metadataSchemaDisplayName) %></coral-columnview-preview-value><%
    }

	Tag tag = cmp.consumeTag();
    AttrBuilder metaAttrs = tag.getAttrs();
    metaAttrs.addBoolean("hidden", true);
    metaAttrs.addClass("foundation-collection-assets-meta");
    metaAttrs.add("data-foundation-collection-meta-title", title);
    metaAttrs.add("data-foundation-collection-meta-folder", isDirectory);

    AttrBuilder imgAttrs = new AttrBuilder(request, xssAPI);
    imgAttrs.addClass("foundation-collection-meta-thumbnail");
    imgAttrs.addHref("src", thumbnailUrl);

    %><div <%= metaAttrs %>>
        <img <%= imgAttrs %>>
    </div>
</coral-columnview-preview-content></coral-columnview-preview><%!

private boolean isContentFragment(Asset asset) {
    Resource resource = asset.adaptTo(Resource.class);
    Resource contentResource = resource.getChild(JcrConstants.JCR_CONTENT);
    boolean isFragment = false;
    if (contentResource != null) {
        ValueMap contentProps = contentResource.adaptTo(ValueMap.class);
        isFragment = contentProps.get("contentFragment", false);
    }
    if (isFragment) {
        // check if editor is available - otherwise, handle as normal asset
        ResourceResolver resolver = resource.getResourceResolver();
        Resource editorRsc =
                resolver.getResource("/libs/dam/cfm/admin/content/fragment-editor");
        isFragment = (editorRsc != null);
    }
    return isFragment;
}

private String getFragmentThumbnailUrl(Asset asset) {
    String thumbnailUrl;
    Resource resource = asset.adaptTo(Resource.class);
    Resource thumbnailRsc = resource.getChild("jcr:content/thumbnail.png");
    if (thumbnailRsc != null) {
        // use the existing thumbnail
        Calendar createdCal = thumbnailRsc.getValueMap()
                .get(JcrConstants.JCR_CREATED, Calendar.class);
        Resource contentResource = thumbnailRsc.getChild(JcrConstants.JCR_CONTENT);
        Calendar lastModifiedCal = createdCal;
        if (contentResource != null) {
            lastModifiedCal = contentResource.getValueMap()
                    .get(JcrConstants.JCR_LASTMODIFIED, createdCal);
        }
        long lastModified =
                (lastModifiedCal != null ? lastModifiedCal.getTimeInMillis() : -1);
        thumbnailUrl = Text.escapePath(thumbnailRsc.getPath()) + "?_ck=" + lastModified;
    } else {
        // default thumbnail
        thumbnailUrl = Text.escapePath("/libs/dam/cfm/admin/content/static/thumbnail_fragment.png");
    }
    return thumbnailUrl;
}

private String getFragmentTitle(Resource resource, String defaultTitle) {
    Resource contentResource = resource.getChild(JcrConstants.JCR_CONTENT);
    ValueMap props = contentResource.getValueMap();
    return props.get("jcr:title", defaultTitle);
}

%>
