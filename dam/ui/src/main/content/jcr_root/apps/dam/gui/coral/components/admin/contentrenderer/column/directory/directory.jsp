<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%
%><%@page import="org.apache.sling.api.resource.Resource,
				  java.util.Iterator,
					org.apache.jackrabbit.util.Text,
					com.jl.aem.dam.briefs.workflow.BriefAssetStatus,
					com.jl.aem.dam.workflow.JLPConstants"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><%@taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0" %><%
//Changing date format to satisfy CCD-385
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/directoryBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/column/common/common.jsp"%><%
%><%
    String colorClass = "";
	String productClass = "";
    Object statusLabel = resource.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS);
    if(statusLabel != null) {
        BriefAssetStatus briefAssetStatus = BriefAssetStatus.valueOf(statusLabel.toString());
        if (briefAssetStatus == BriefAssetStatus.ASSET_REQUESTED) {
			colorClass = "pendingShoot";
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_UPLOADED) {
            if (resource.getValueMap().get(JLPConstants.RETOUCH_MODE) == null) {
                colorClass = "pendingRetouch";
            } else {
				colorClass = "readyToRetouch";
            }
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_RETOUCHED) {
			colorClass = "assetRetouched";
        } else if (briefAssetStatus == BriefAssetStatus.ALT_NOT_NEEDED) {
			colorClass = "altNotNeeded";
        } else if (briefAssetStatus == BriefAssetStatus.RE_SHOOT_NEEDED) {
			colorClass = "pendingReShoot";
        } else if (briefAssetStatus == BriefAssetStatus.ASSET_DELIVERED) {
			colorClass = "productDelivered";
        } else if (briefAssetStatus == BriefAssetStatus.PENDING_COLOUR_MATCH) {
			colorClass = "pendingColourMatch";
        }
    } else {
        if (resource.getValueMap().get("stockNumber") != null || resource.getValueMap().get("dam:JLPLookName") != null) {
            Iterator<Resource> children = resource.getChildren().iterator();
            boolean allDone = true;
            boolean allDelivered = true;
            boolean allReadyForRetouch = true;
            boolean allPendingRetouch = true;
            boolean allPendingColourMatch = true;

            while (children.hasNext()) {
                Resource child = children.next();
                Object statusLabelChild = child.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS);
                if (statusLabelChild != null) {
                    BriefAssetStatus briefAssetStatus = BriefAssetStatus.valueOf(statusLabelChild.toString());
                    if (briefAssetStatus != BriefAssetStatus.ASSET_DELIVERED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allDelivered = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allDone = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.ASSET_UPLOADED && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allReadyForRetouch = false;
                        allPendingRetouch = false;
                    }
                    if (briefAssetStatus != BriefAssetStatus.ASSET_RETOUCHED && briefAssetStatus != BriefAssetStatus.PENDING_COLOUR_MATCH && briefAssetStatus != BriefAssetStatus.ALT_NOT_NEEDED) {
                        allPendingColourMatch = false;
                    }
                    if (briefAssetStatus == BriefAssetStatus.ASSET_UPLOADED) {
                        if (child.getValueMap().get(JLPConstants.RETOUCH_MODE) == null) {
                            allReadyForRetouch = false;
                        }
                    }
                }
            }
            if (allDelivered || resource.getValueMap().get("dam:JLPDelivered") != null) {
                colorClass = "productDelivered";
            } else if (allDone) {
    			colorClass = "assetRetouched";
            } else if (allReadyForRetouch) {
                colorClass = "readyToRetouch";
            } else if (allPendingRetouch) {
                colorClass = "pendingRetouch";
            } else if (allPendingColourMatch) {
                colorClass = "pendingColourMatch";
            }
        } else if (resource.getValueMap().get("projectPath") != null) {
            Iterator<Resource> children = resource.getChildren().iterator();
            boolean allDelivered = true;
            int length = 0;
            while (children.hasNext()) {
                Resource child = children.next();
                if (child.getValueMap().get("stockNumber") != null || child.getValueMap().get("dam:JLPLookName") != null ) {
                    length++;
                    if (child.getValueMap().get("dam:JLPDelivered") == null) {
                        allDelivered = false;
                    }
                } 
                
            }
            if (allDelivered && length >=1) {
                productClass = "productDelivered";
            }
        }
    }
%><%

String directoryActionRels = StringUtils.join(getDirectoryActionRels(hasJcrRead, hasModifyAccessControl, hasJcrWrite, hasReplicate, isMACShared, isCCShared, isRootMACShared, isMPShared, isRootMPShared), " ");
String name = resource.getName(); 
request.setAttribute("actionRels", actionRels.concat(" " + directoryActionRels));

attrs.add("itemscope", "itemscope");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);
if (!"".equals(colorClass)) {
	attrs.add("class", colorClass);
}

if (hasChildren(resource)) {
    attrs.add("variant", "drilldown");
}

request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);

%><ui:includeClientLib categories="com" />
<coral-columnview-item <%= attrs %>>
<cq:include script = "meta.jsp"/>

    <coral-columnview-item-thumbnail class="<%=productClass%>">
        <coral-icon icon="folder"></coral-icon>
        </coral-columnview-item-thumbnail>
		<coral-columnview-item-content >
        <div class="foundation-collection-item-title" itemprop="title" title="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>">
            <%= xssAPI.encodeForHTML(resourceTitle) %>
        </div><%
        if (name != null && !name.equals(resourceTitle)) {
            %><div class="foundation-layout-util-subtletext">
                <%= xssAPI.encodeForHTML(name) %>
            </div><%
        }%>
    </coral-columnview-item-content>
<cq:include script = "applicableRelationships.jsp"/>
<cq:include script = "link.jsp"/>
</coral-columnview-item><%!
//Add private methods here
%>
