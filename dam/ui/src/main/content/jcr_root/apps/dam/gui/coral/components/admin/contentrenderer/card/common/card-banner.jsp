<%--
  ADOBE CONFIDENTIAL

  Copyright 2016 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%       
%><%@page import="static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT,
                  static com.day.cq.dam.api.DamConstants.RENDITIONS_FOLDER,
                  static com.day.cq.dam.api.DamConstants.ORIGINAL_FILE,
                  com.day.cq.workflow.status.WorkflowStatus,
                  java.util.Iterator,
                  com.day.cq.dam.api.ui.editor.metadata.MetadataEditorHelper,
                  com.day.cq.dam.commons.util.DamUtil,
				  com.jl.aem.dam.workflow.JLPConstants,
                  com.jl.aem.dam.briefs.workflow.BriefAssetStatus,
				  com.jl.aem.dam.workflow.AssetStatus"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/base.jsp"%><%                      
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/base/assetBase.jsp"%><%
Node resourceNode = request.getAttribute(RESOURCE_NODE) != null ? (Node) request.getAttribute(RESOURCE_NODE) : null;

    boolean isNew = isNew(resourceNode);
    //running workflow status
    WorkflowStatus wfState = resource.adaptTo(WorkflowStatus.class);
    boolean inWorkflow = false;
    try {
        inWorkflow = (wfState != null && wfState.isInRunningWorkflow(true));
        if (wfState == null || !inWorkflow) {
            Resource original = resource.getResourceResolver().getResource(resource.getPath() + "/" + JCR_CONTENT + "/" + RENDITIONS_FOLDER + "/" + ORIGINAL_FILE);
            wfState = (original == null) ? wfState : original.adaptTo(WorkflowStatus.class);
            inWorkflow = (wfState != null && wfState.isInRunningWorkflow(true));
        }
        inWorkflow = (inWorkflow || DamUtil.isInRunningWorkflow(resource));
    } catch(NullPointerException e){
        // CQ-77820 [Upgrade] Workflow service user wiped out on upgrade
        log.error("error determining the workflow status for asset", e);
    }
    ResourceResolver resolver = slingRequest.getResourceResolver();
    MetadataEditorHelper meh = sling.getService(MetadataEditorHelper.class);
    boolean missingMeta = !hasValidMetadata(resource, meh);

    Resource metadataResource = resolver.getResource(resource, "jcr:content/metadata");
    boolean archived = false;
    if (metadataResource.getValueMap().get("dam:archiveStatus") != null) {
        archived = true;
    }
    String jlpStatus = getStatusFor(metadataResource);
    String colorBkg = "";
    String statusLabel = getStatusLabel(jlpStatus);
    String styling = "style='background-color: #b0b7bf'";
    String colorClass = getClass(metadataResource);
    if (colorClass != null && !"".equals(colorClass)) {
        styling = colorClass;
    } 
    
    if (null != jlpStatus) {
        // colorBkg = getBackgroundColorFor(jlpStatus);
        try {
        	statusLabel = AssetStatus.valueOf(jlpStatus).getDisplayLabel();
        } catch (Exception e) {
            //do nothing - prevent UI to break
        }
    }
    
%><coral-card-info><%
		if (archived) {
		    %>
			<coral-alert class="archived">
    			<coral-alert-content ><%= i18n.get("Archived")%></coral-alert-content>
			</coral-alert>
		<%    
		} else if(jlpStatus != null) {
        %>
			<coral-alert <%=styling %>>
    			<coral-alert-content ><%= i18n.get(statusLabel)%></coral-alert-content>
			</coral-alert>
		<% }else if (inWorkflow && (!(resource.isResourceType("nt:folder") || resource.isResourceType("sling:Folder") ||
                resource.isResourceType("sling:OrderedFolder")))) {%>
                <coral-alert >
                    <coral-alert-content><%= i18n.get("Processing ...")%></coral-alert-content>
                </coral-alert>
        <% }else if(missingMeta) { %>
                <coral-alert variant="error">
                    <coral-alert-content><%= i18n.get("REQUIRED METADATA MISSING")%></coral-alert-content>
                </coral-alert>
       <%} else if(isNew) { %>
                <coral-tag color="blue" class="u-coral-pullRight"><%= xssAPI.encodeForHTML(i18n.get("New")) %></coral-tag>
       <%}%>
</coral-card-info>
<%!
  private String getStatusFor(Resource metadataResource) {
      String status = metadataResource.getValueMap().get(JLPConstants.JLP_STATUS, String.class);
      if (status == null) {
          status = metadataResource.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS, String.class);
      }
      return status;
  }
  
  public String getStatusLabel(String srcStatus) {
      for (AssetStatus status : AssetStatus.values()) {
          if (status.toString().equals(srcStatus)) {
              return status.getDisplayLabel();
          }
      }
      for (BriefAssetStatus status : BriefAssetStatus.values()) {
          if (status.toString().equals(srcStatus)) {
              return status.getDisplayLabel();
          }
      }
      return srcStatus;
  }
  
  public String getClass(Resource metadataResource) {    
      String colorClass = "";
      Object statusLabel = metadataResource.getValueMap().get(JLPConstants.JLP_BRIEF_ASSET_STATUS);
      if(statusLabel != null) {
          BriefAssetStatus briefAssetStatus = BriefAssetStatus.valueOf(statusLabel.toString());
          if (briefAssetStatus == BriefAssetStatus.ASSET_REQUESTED) {
  			colorClass = "pendingShoot";
          } else if (briefAssetStatus == BriefAssetStatus.ASSET_UPLOADED) {
              if (metadataResource.getValueMap().get(JLPConstants.RETOUCH_MODE) == null) {
                  colorClass = "pendingRetouch";
              } else {
  				colorClass = "readyToRetouch";
              }
          } else if (briefAssetStatus == BriefAssetStatus.ASSET_RETOUCHED) {
  			colorClass = "assetRetouched";
          } else if (briefAssetStatus == BriefAssetStatus.ALT_NOT_NEEDED) {
  			colorClass = "altNotNeeded";
          } else if (briefAssetStatus == BriefAssetStatus.RE_SHOOT_NEEDED) {
  			colorClass = "pendingReShoot";
          } else if (briefAssetStatus == BriefAssetStatus.ASSET_DELIVERED) {
  			colorClass = "productDelivered";
          } else if (briefAssetStatus == BriefAssetStatus.PENDING_COLOUR_MATCH) {
  			colorClass = "pendingColourMatch";
          }
      }
      return "class='"+ colorClass + "'";
  }
%>
<%!
	private boolean hasValidMetadata (Resource res, MetadataEditorHelper meh) {
    	List<Resource> invalidItems = meh.getInvalidFormItems(res);
    	if (null == invalidItems || invalidItems.size() == 0) {
    	    return true;
    	} else {
    	    return false;
    	}
	}%>