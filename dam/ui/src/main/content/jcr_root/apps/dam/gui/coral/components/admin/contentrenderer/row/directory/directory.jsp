<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%>
<%
%><%@page import="org.apache.sling.api.resource.Resource"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><%@include file="/apps/dam/gui/coral/components/admin/contentrenderer/base/init/directoryBase.jsp"%><%
%><%@include file="/libs/dam/gui/coral/components/admin/contentrenderer/row/common/common.jsp"%><%

String directoryActionRels = StringUtils.join(getDirectoryActionRels(hasJcrRead, hasModifyAccessControl, hasJcrWrite, hasReplicate, isMACShared, isCCShared, isRootMACShared, isMPShared, isRootMPShared), " ");

request.setAttribute("actionRels", actionRels.concat(" " + directoryActionRels));

attrs.addClass("foundation-collection-navigator");

attrs.add("is", "coral-table-row");
attrs.add("data-item-title", resourceTitle);
attrs.add("data-item-type", type);

String stockNumber = resource.getValueMap().get("stockNumber","");
String category = resource.getValueMap().get("category","");
String clientStylingNotes = resource.getValueMap().get("clientStylingNotes","");
String ids = resource.getValueMap().get("ids","");
String imageRequirements = resource.getValueMap().get("imageRequirements","");
String productType = resource.getValueMap().get("productType","");
String range = resource.getValueMap().get("range","");
String productName = resource.getValueMap().get("productName","");
String webSKU = resource.getValueMap().get("webSKU","");
String description = resource.getValueMap().get("description","");
String ean = resource.getValueMap().get("ean","");
String jlstatus = resource.getValueMap().get("status","");
String supplier = resource.getValueMap().get("supplier","");
String colour = resource.getValueMap().get("colour","");
String brand = resource.getValueMap().get("brand","");

request.setAttribute("com.adobe.assets.meta.attributes", metaAttrs);

%><cq:include script = "meta.jsp"/>
<tr <%= attrs %>>
    <td is="coral-table-cell" coral-table-rowselect>
        <coral-icon class="foundation-collection-item-thumbnail" icon="folder"></coral-icon>
    </td>
    <td class="foundation-collection-item-title" is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(resourceTitle) %>"><%= xssAPI.encodeForHTML(resourceTitle) %></td>

    <td is="coral-table-cell" value="<%= displayLanguage %>"><%= displayLanguage %></td>
    <td is="coral-table-cell" value="0"></td> <!--Adding a placeholder column for expiryStatus -->
    <td is="coral-table-cell" value="0"></td> <!--Adding a placeholder column for encodingStatus -->
    <td is="coral-table-cell" value="type"><%= "FOLDER" %></td>
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for dimensions -->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for size -->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for rating -->
	<td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for usagescore-->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for impression score-->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for click score-->
    <td is="coral-table-cell" value="<%= xssAPI.encodeForHTMLAttr(Long.toString(directoryLastModification)) %>"><%
        if (lastModified != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(directoryLastModification)) %>"><%= xssAPI.encodeForHTML(lastModified) %></time><%

            // Modified-after-publish indicator
                if (publishDateInMillis > 0 && publishDateInMillis < directoryLastModification) {
                String modifiedAfterPublishStatus = i18n.get("Modified since last publication");
                %><coral-icon icon="alert" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(modifiedAfterPublishStatus) %>"></coral-icon><%
            }

            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(lastModifiedBy) %></div><%
        }
    %>
    </td>
    <td is="coral-table-cell" value="<%= (!isDeactivated && publishedDate != null) ? xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) : "0" %>"><%
        // Published date and status
        if (!isDeactivated && publishedDate != null) {
            %><time datetime="<%= xssAPI.encodeForHTMLAttr(Long.toString(publishDateInMillis)) %>"><%= xssAPI.encodeForHTML(publishedDate) %></time><%
        } else {
            %><span><%= xssAPI.encodeForHTML(i18n.get("Not published")) %></span><%
        }

        // Publication/un-publication pending indicator
        String publicationPendingStatus = getPublicationPendingStatus(replicationStatus, i18n);
        if (publicationPendingStatus.length() > 0) {
            %><coral-icon icon="pending" style = "margin-left: 5px;" size="XS" title="<%= xssAPI.encodeForHTMLAttr(publicationPendingStatus) %>"></coral-icon><%
        }

        // Published by
        if (!isDeactivated && publishedBy != null) {
            %><div class="foundation-layout-util-subtletext"><%= xssAPI.encodeForHTML(publishedBy) %></div><%
        }

        %><cq:include script = "applicableRelationships.jsp"/>
    </td>
    <td is="coral-table-cell"></td> <!--Adding a placeholder column for checkout status-->
    <td is="coral-table-cell" value="0"></td>   <!--Adding a placeholder column for comments-->

    <% if (isProcessingProfileEntitled && !profileTitleList[0].trim().isEmpty()) { %>
        <td is="coral-table-cell" value="0"><%= xssAPI.encodeForHTML(profileTitleList[0].trim()) %></td>
    <% } else{ %>
        <td is="coral-table-cell" value="0"></td>
    <% } %>

    <% if (isProcessingProfileEntitled && !profileTitleList[1].trim().isEmpty()) { %>
        <td is="coral-table-cell" value="0"><%= xssAPI.encodeForHTML(profileTitleList[1].trim()) %></td>
    <% } else{ %>
        <td is="coral-table-cell" value="0"></td>
    <% } %>

    <% if (isProcessingProfileEntitled && !profileTitleList[2].trim().isEmpty()) { %>
        <td is="coral-table-cell" value="0"><%= xssAPI.encodeForHTML(profileTitleList[2].trim()) %></td>
    <% } else{ %>
        <td is="coral-table-cell" value="0"></td>
    <% } %>
    <td is="coral-table-cell" value="<%=stockNumber%>"><%=stockNumber%></td>
    <td is="coral-table-cell" value="<%=category%>"><%=category%></td>
    <td is="coral-table-cell" value="<%=clientStylingNotes%>"><%=clientStylingNotes%></td>
	<td is="coral-table-cell" value="<%=ids%>"><%=ids%></td>
    <td is="coral-table-cell" value="<%=imageRequirements%>"><%=imageRequirements%></td>
    <td is="coral-table-cell" value="<%=productType%><"><%=productType%></td>
    <td is="coral-table-cell" value="<%=range%>"><%=range%></td>
    <td is="coral-table-cell" value="<%=productName%>"><%=productName%></td>
    <td is="coral-table-cell" value="<%=webSKU%>"><%=webSKU%></td>
    <td is="coral-table-cell" value="<%=description%>"><%=description%></td>
    <td is="coral-table-cell" value="<%=ean%>"><%=ean%></td>
    <td is="coral-table-cell" value="<%=jlstatus%>"><%=jlstatus%></td>
    <td is="coral-table-cell" value="<%=supplier%>"><%=supplier%></td>
    <td is="coral-table-cell" value="<%=colour%>"><%=colour%></td>
    <td is="coral-table-cell" value="<%=brand%>"><%=brand%></td>
    <td is="coral-table-cell" value="0"></td>
	<td is="coral-table-cell" value="0"></td>
    <cq:include script = "reorder.jsp"/>
</tr><%!

%>
