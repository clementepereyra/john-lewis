<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page session="false" contentType="text/html; charset=utf-8"%><%
%><%@page import="org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ResourceUtil,
                  java.util.Iterator,
                  javax.jcr.Session,
                  java.io.PrintWriter,
                  com.day.cq.dam.commons.util.S7SetHelper,
                  com.adobe.granite.security.user.util.AuthorizableUtil,
                  javax.jcr.Node,
                  org.apache.sling.api.resource.ValueMap,
                  com.day.cq.search.Query,
                  com.day.cq.i18n.I18n,
                  com.adobe.granite.xss.XSSAPI,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  org.apache.commons.lang3.StringEscapeUtils,
                  java.util.Map,
                  java.util.HashMap,
                  java.util.List,
                  java.util.Calendar,
                  java.util.ArrayList,
                  com.day.cq.dam.api.Asset,
                  com.adobe.granite.comments.CommentCollection,
                  com.adobe.granite.comments.CommentManager,
                  com.day.cq.dam.commons.util.UIHelper,
                  com.day.cq.search.result.SearchResult,
                  com.day.cq.search.QueryBuilder,
                  com.day.cq.search.PredicateGroup,
                  com.jl.aem.dam.workflow.JLWorkflowsUtils,
                  com.adobe.granite.ui.components.Config"%>
<%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.2"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%><%
%><cq:defineObjects /><%
    Config cfg = new Config(resource);

	//get the search results
    QueryBuilder queryBuilder = sling.getService(QueryBuilder.class);
    Map resM = request.getParameterMap();
	Query query=null;
    PredicateGroup predicates = PredicateGroup.create(resM);
    query = queryBuilder.createQuery(predicates, resourceResolver.adaptTo(Session.class));
    SearchResult result = query.getResult ();
    Map<String,Integer> freqMap = new HashMap<String,Integer>();
    List<Resource> resSet = new ArrayList<Resource>();
    Iterator<Resource> itr = result.getResources();
	Iterator<Resource> assetItr = result.getResources();
	while (itr.hasNext()) {
		Resource hitNode = itr.next();
		if (hitNode.adaptTo(Asset.class) == null) {
			for (Iterator<Resource> resultItr = hitNode.listChildren(); resultItr.hasNext();) {
				ValueMap vm = resultItr.next().adaptTo(ValueMap.class);
				if (vm.containsKey("link")) {
					Resource asset = resourceResolver.getResource(vm.get("link").toString());
					if (asset != null && asset.adaptTo(Asset.class) != null) {
						if (freqMap.containsKey(asset.getPath())) {
							freqMap.put(asset.getPath(),freqMap.get(asset.getPath())+1);
						} else {
							freqMap.put(asset.getPath(), 1);
							resSet.add(asset);
						}
					}
				}
			}
		} else {
			resSet.add(hitNode);
		}
	}
	assetItr = resSet.iterator();

    //Write the column names 
    String reportSelected = slingRequest.getRequestPathInfo().getSuffix();
    Resource reportNode = resourceResolver.getResource(reportSelected);
    StringBuilder csvContent = new StringBuilder();
    I18n i18n = new I18n(slingRequest);
    long assetCounts = 0;
    double totalAssetSize = 0;

    response.setHeader("Content-Disposition", "attachment; filename=\""+reportSelected.substring(reportSelected.lastIndexOf("/")+1)+".csv\"");
    response.setHeader("Content-Type","text/csv");
    response.setHeader("Set-Cookie", "fileDownload=true; path=/");
    PrintWriter o = response.getWriter();
    //Adding a UTF-8 BOM for proper encoding
    o.write("\uFEFF");
    o.flush();

    if (reportNode != null) {
        ValueMap reportVM = ResourceUtil.getValueMap(reportNode);
        String [] columnsArray = reportVM.get("columns", String [].class);
        request.setAttribute("com.adobe.cq.item.columns", columnsArray);
        for (int i = 0; i < columnsArray.length; i++) { 
            String title = columnsArray[i].substring(columnsArray[i].lastIndexOf("/")+1);
            if(title.equals("imageid")){
                csvContent.append("\""+i18n.get("IMAGE ID")+"\",");
            }
            else if(title.equals("frequency")){
                csvContent.append("\""+i18n.get("DOWNLOAD COUNT")+"\",");
            }
            else if(title.equals("approvedbyic")){
                csvContent.append("\""+i18n.get("APPROVED BY IC")+"\",");
            }
            else if(title.equals("artworkingissues")){
                csvContent.append("\""+i18n.get("ARTWORKING ISSUES")+"\",");
            }
            else if(title.equals("artworktime")){
                csvContent.append("\""+i18n.get("ARTWORKING TIME")+"\",");
            }
            else if(title.equals("assetrequeted")){
                csvContent.append("\""+i18n.get("ASSET REQUESTED")+"\",");
            }
            else if(title.equals("autorejected")){
                csvContent.append("\""+i18n.get("AUTO REJECTED")+"\",");
            }
            else if(title.equals("completeworkflowtime")){
                csvContent.append("\""+i18n.get("COMPLETED WORKFLOW TIME")+"\",");
            }
            else if(title.equals("finalreviewertime")){
                csvContent.append("\""+i18n.get("FINAL REVIEWER TIME")+"\",");
            }
            else if(title.equals("producttitle")){
                csvContent.append("\""+i18n.get("PRODUCT TITLE")+"\",");
            }
            else if(title.equals("ictime")){
                csvContent.append("\""+i18n.get("IC TIME")+"\",");
            }
            else if(title.equals("jlstocknumber")){
                csvContent.append("\""+i18n.get("JL STOCK NUMBER")+"\",");
            }
            else if(title.equals("modelnumber")){
                csvContent.append("\""+i18n.get("MODEL NO")+"\",");
            }
            else if(title.equals("providedbyartworker")){
                csvContent.append("\""+i18n.get("PROVIDED BY ARTWORKER")+"\",");
            }
            else if(title.equals("stocklevel")){
                csvContent.append("\""+i18n.get("STOCK LEVEL")+"\",");
            }
            else if(title.equals("supplierissues")){
                csvContent.append("\""+i18n.get("SUPPLIER ISSUES")+"\",");
            }
            else if(title.equals("suppliertime")){
                csvContent.append("\""+i18n.get("SUPPLIER TIME")+"\",");
            }
            else if(title.equals("products")){
                csvContent.append("\""+i18n.get("ASSOCIATED PRODUCTS")+"\",");
            }
            else if(title.equals("activation")){
                csvContent.append("\""+i18n.get("ACTIVATION DATE")+"\",");
            }
            else if(title.equals("published")){
                csvContent.append("\""+i18n.get("PUBLISH STATUS")+"\",");
                csvContent.append("\""+i18n.getVar(title.toUpperCase())+"\",");
                csvContent.append("\""+i18n.get("PUBLISHED BY")+"\",");
            }
            else if(title.equals("added")){
                csvContent.append("\""+i18n.getVar(title.toUpperCase())+"\",");
                csvContent.append("\""+i18n.get("ADDED BY")+"\",");
            }
            else if(title.equals("modified")){
                csvContent.append("\""+i18n.getVar("LAST MODIFIED")+"\",");
                csvContent.append("\""+i18n.get("MODIFIED BY")+"\",");
            }
            else{
                csvContent.append("\""+i18n.getVar(title.toUpperCase())+"\",");
            }
        }
        csvContent.deleteCharAt(csvContent.length()-1);
        csvContent.append("\r\n");
        o.write(csvContent.toString());
        o.flush();
        csvContent.setLength(0); // set length of buffer to 0
        csvContent.trimToSize();
    }

	//write the data
    if (assetItr != null) {
        Resource p = null;
        Asset asset = null;
        while (assetItr.hasNext()) {
            p = assetItr.next();
            asset = p.adaptTo(Asset.class);
            String date="";
            ValueMap vm = p.adaptTo(ValueMap.class);
            String type = vm.get("sling:resourceType", String.class);
            Node childAsset = p.adaptTo(Node.class);
            if (childAsset.isNodeType("dam:Asset")) {
                assetCounts++;
                String [] columnsArray = (String [])request.getAttribute("com.adobe.cq.item.columns");
                for (int i = 0; i< columnsArray.length; i++) {
                    String title = columnsArray[i].substring(columnsArray[i].lastIndexOf("/")+1);

                    //add data for title column
                    if (title.equals("imageid")) {
                        String titleP = UIHelper.getTitle(p);
                        titleP = titleP.indexOf(".")>=0?titleP.substring(0, titleP.indexOf(".")):titleP;
                        csvContent.append(StringEscapeUtils.escapeCsv(titleP) + ",");
                    }

                    if (title.equals("imageid")) {
                        String prodTitle =vm.get("jcr:content/metadata/dam:JLPProductTitle", String.class);
                        if (prodTitle == null) {
                            prodTitle = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(prodTitle) + ",");
                    }

                    //add data for type column
                    if (title.equals("type")) {
                        String mimeType = "";
                        String displayMimeType="";
                        Resource lookupResource = resourceResolver.getResource("/mnt/overlay/dam/gui/content/assets/jcr:content/mimeTypeLookup");

                        //mimeType
                        if (asset.getMimeType() != null) {
                            mimeType = asset.getMimeType();
                            String ext = mimeType.substring(mimeType.lastIndexOf('/') + 1, mimeType.length());
                            if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
                                displayMimeType = "";
                            }
                            if (displayMimeType.length() == 0 && mimeType.startsWith("image")) {
                                displayMimeType = "IMAGE";
                            } else if (displayMimeType.length() == 0 && mimeType.startsWith("text")) {
                                displayMimeType = "DOCUMENT";
                            } else if (displayMimeType.length() == 0 && (mimeType.startsWith("video") || mimeType.startsWith("audio"))) {
                                displayMimeType = "MULTIMEDIA";
                            } else if (displayMimeType.length() == 0 && mimeType.startsWith("application")) {
                                int idx_1 = ext.lastIndexOf('.');
                                int idx_2 = ext.lastIndexOf('-');
                                int lastWordIdx = (idx_1 > idx_2)?idx_1:idx_2;
                                displayMimeType = ext.substring(lastWordIdx+1).toUpperCase();
                            }
                        }

                        if (displayMimeType.length() == 0 && asset.getName() != null) {
                            String filename = asset.getName();
                            String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                            if((displayMimeType = UIHelper.lookupMimeType(ext,lookupResource,true)) == null) {
                                displayMimeType = "";
                            }
                        }

                        csvContent.append(i18n.getVar(displayMimeType)+",");
                    }

                    //add data for size column
                    if (title.equals("size")) {
                        String size = "0.0 B";
                        long bytes=0;
                        if (asset.getOriginal() != null) {
                            bytes = asset.getOriginal().getSize();
                            totalAssetSize  = totalAssetSize + bytes;
                            size = UIHelper.getSizeLabel(bytes, slingRequest);
                        }
                        csvContent.append("\""+size.replaceAll("\"", "\"\"")+"\",");
                    }

                    //add data for created column
                    if (title.equals("added")) {
                        date="";
                        String createdBy="";
                        if (p != null) { 
                            date = vm.get("jcr:created", String.class);
                            createdBy = vm.get("jcr:createdBy", String.class);
                            createdBy = AuthorizableUtil.getFormattedName(p.getResourceResolver(), createdBy);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(date+","+createdBy+",");
                    }

                    //add data for expired column
                    if (title.equals("expired")) {
                        date="";
                        date = vm.get("jcr:content/metadata/prism:expirationDate", String.class);
                        if(date==null)
                            date="";
                        csvContent.append(date+",");
                    }

                    //add data for modified column
                    if (title.equals("modified")) {
                        date="";
                        String modifiedBy="";
                        date = vm.get("jcr:content/jcr:lastModified", String.class);
                        modifiedBy = vm.get("jcr:content/jcr:lastModifiedBy", String.class);
                        modifiedBy = AuthorizableUtil.getFormattedName(p.getResourceResolver(), modifiedBy);
                        if(date==null) {
                            date="";
                        }
                        if (date.equals("")) {
                            date = vm.get("jcr:created", String.class);
                            modifiedBy = vm.get("jcr:createdBy", String.class);
                            modifiedBy = AuthorizableUtil.getFormattedName(p.getResourceResolver(), modifiedBy);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(date+","+modifiedBy+",");
                    }

                    //add data for comments column
                    if (title.equals("comments")) {
                        String comments = vm.get("jcr:content/metadata/dam:JLPComment", String.class);
                        if (null == comments) {
                            comments = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(comments)+",");
                    }

                    //add data for frequency column
                    if (title.equals("frequency")) {
                        int count = 0;
                        if (freqMap != null) {
                            count  = freqMap.containsKey(p.getPath()) ? freqMap.get(p.getPath()) : 0;
                        }
                        csvContent.append(count+",");
                    }

                    //add data for published column
                    if (title.equals("published")) {
                        date="";
                        String publishStatus = "Unpublished",publishedBy="";
                        date = vm.get("jcr:content/cq:lastReplicated", String.class);
                        publishedBy = vm.get("jcr:content/cq:lastReplicatedBy", String.class);
                        if(publishedBy!=null) {
                            publishedBy = AuthorizableUtil.getFormattedName(p.getResourceResolver(), publishedBy);
                        }
                        else {
                            publishedBy = "";
                        }
                        if(date==null) {
                            date="";
                        }
                        String lastReplicationAction = vm.get("jcr:content/cq:lastReplicationAction", String.class);
                        if(lastReplicationAction != null) {
                            publishStatus = "Deactivate".equals(lastReplicationAction) ? "Unpublished" : "Published";;
                        }
                        csvContent.append(i18n.getVar(publishStatus)+","+date+","+publishedBy+",");
                    }

                    //add data for path column
                    if (title.equals("path")) {
                        csvContent.append("\""+p.getPath().replaceAll("\"", "\"\"")+"\""+ ",");
                    }

                     //add data for Status column
                    if (title.equals("status")) {
                        String statusId = vm.get("jcr:content/metadata/dam:JLPStatus", String.class);
                        if(statusId == null)
                            statusId = "";
                        csvContent.append(StringEscapeUtils.escapeCsv(statusId) +",");
                    }
                    
                    if (title.equals("approvedbyic")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateICApproved", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date) + ",");
                    }
                    
                    if (title.equals("providedbyartworker")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateProvidedByArtworker", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date) + ",");
                    }
                    
                    if (title.equals("uploaded")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateSupplierUploaded", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date) + ",");
                    }
                    
                    if (title.equals("assetrequested")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateSupplierRequested", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date) + ",");
                    }
                    
                    if (title.equals("available")) {
                        date="";
                        String by="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateAvailable", String.class);
                            by = vm.get("jcr:content/metadata/userAvailable", String.class);
                            by = AuthorizableUtil.getFormattedName(p.getResourceResolver(), by);
                            if(by==null){
                                by = "";
                            }
                            if(date==null)
                                date="";
                            
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date)+" "+StringEscapeUtils.escapeCsv(by)+",");
                    }
                    if (title.equals("autorejected")) {
                        String value = "";
                        value = vm.get("jcr:content/metadata/rejectedAuto", String.class);
                        if(value==null)
                            value="";
                        csvContent.append(StringEscapeUtils.escapeCsv(value)+",");
                    }
                    if (title.equals("supplier")) {
                        String value = "";
                        value = vm.get("jcr:content/metadata/dam:supplierContactDetail", String.class);
                        if(value==null)
                            value="";
                        csvContent.append(StringEscapeUtils.escapeCsv(value)+",");
                    }
                    if (title.equals("rejected")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateRejected", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date)+",");
                    }
                    
                    if (title.equals("supplierissues")) {
                        String value="";
                        if (p != null) { 
                            value = vm.get("jcr:content/metadata/assetValidationFailsCount", String.class);
                            if(value==null)
                                value="0";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(value)+",");
                    }
                    if (title.equals("artworkingissues")) {
                        String value="";
                        if (p != null) { 
                            value = vm.get("jcr:content/metadata/artworkingValidationFailsCount", String.class);
                            if(value==null)
                                value="0";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(value)+",");
                    }
                    
                    if (title.equals("completed")) {
                        date="";
                        String by="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateCompleted", String.class);
                            by = vm.get("jcr:content/metadata/userCompleted", String.class);
                            by = AuthorizableUtil.getFormattedName(p.getResourceResolver(), by);
                            if(by==null){
                                by = "";
                            }
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date+" "+by) + ",");
                    }
                    
                    if (title.equals("suppliertime")) {
                        date="";
                        String displayDate = "";
                        Calendar dateTo;
                        Calendar dateFrom;
                        if (p != null) { 
                            dateFrom = vm.get("jcr:content/metadata/dateSupplierRequested", Calendar.class);
                            dateTo = vm.get("jcr:content/metadata/dateSupplierUploaded", Calendar.class);
                            Long dateFromLong = (null != dateFrom) ? dateFrom.getTimeInMillis() : 0;
                            Long dateToLong = (null != dateTo) ? dateTo.getTimeInMillis() : 0;
                            
                            displayDate = JLWorkflowsUtils.getTimeDifference(dateFromLong, dateToLong);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(displayDate) + ",");
                    }
                    
                    if (title.equals("ictime")) {
                        date="";
                        String displayDate = "";
                        Calendar dateTo;
                        Calendar dateFrom;
                        if (p != null) { 
                            dateFrom = vm.get("jcr:content/metadata/dateSupplierUploaded", Calendar.class);
                            dateTo = vm.get("jcr:content/metadata/dateICApproved", Calendar.class);
                            Long dateFromLong = (null != dateFrom) ? dateFrom.getTimeInMillis() : 0;
                            Long dateToLong = (null != dateTo) ? dateTo.getTimeInMillis() : 0;
                            
                            displayDate = JLWorkflowsUtils.getTimeDifference(dateFromLong, dateToLong);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(displayDate) + ",");
                    }
                    
                    if (title.equals("artworktime")) {
                        date="";
                        String displayDate = "";
                        Calendar dateTo;
                        Calendar dateFrom;
                        if (p != null) { 
                            dateFrom = vm.get("jcr:content/metadata/dateAvailable", Calendar.class);
                            dateTo = vm.get("jcr:content/metadata/dateProvidedByArtworker", Calendar.class);
                            Long dateFromLong = (null != dateFrom) ? dateFrom.getTimeInMillis() : 0;
                            Long dateToLong = (null != dateTo) ? dateTo.getTimeInMillis() : 0;
                            
                            displayDate = JLWorkflowsUtils.getTimeDifference(dateFromLong, dateToLong);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(displayDate) + ",");
                    }
                    
                    if (title.equals("finalreviewertime")) {
                        date="";
                        String displayDate = "";
                        Calendar dateTo;
                        Calendar dateFrom;
                        if (p != null) { 
                            dateFrom = vm.get("jcr:content/metadata/dateProvidedByArtworker", Calendar.class);
                            dateTo = vm.get("jcr:content/metadata/dateCompleted", Calendar.class);
                            Long dateFromLong = (null != dateFrom) ? dateFrom.getTimeInMillis() : 0;
                            Long dateToLong = (null != dateTo) ? dateTo.getTimeInMillis() : 0;
                            
                            displayDate = JLWorkflowsUtils.getTimeDifference(dateFromLong, dateToLong);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(displayDate) + ",");
                    }
                    
                    if (title.equals("completeworkflowtime")) {
                        date="";
                        String displayDate = "";
                        Calendar dateTo;
                        Calendar dateFrom;
                        if (p != null) { 
                            dateFrom = vm.get("jcr:content/metadata/dateSupplierRequested", Calendar.class);
                            dateTo = vm.get("jcr:content/metadata/dateCompleted", Calendar.class);
                            Long dateFromLong = (null != dateFrom) ? dateFrom.getTimeInMillis() : 0;
                            Long dateToLong = (null != dateTo) ? dateTo.getTimeInMillis() : 0;
                            
                            displayDate = JLWorkflowsUtils.getTimeDifference(dateFromLong, dateToLong);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(displayDate) + ",");
                    }
                    
                    if (title.equals("source")) {
                        String source = vm.get("jcr:content/metadata/dam:JLPSource", String.class);
                        if(source == null){
                            source = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(source) +",");
                    }
                    if (title.equals("brand")) {
                        String propValue = vm.get("jcr:content/metadata/dam:brandName", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("outsourcedto")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPOutsourcedTo", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("directorate")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPDirectorate", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("altnumber")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPImageCategory", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        if (propValue.endsWith("/")) {
                            propValue = propValue.substring(0, propValue.length()-1);
                        }
                        if (propValue.contains("/")) {
                            propValue = propValue.substring(propValue.lastIndexOf("/")+1);
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("modelnumber")) {
                        String propValue = vm.get("jcr:content/metadata/dam:ModelNumber", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("jlstocknumber")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPProductCodes", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("stocklevel")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPStockLevel", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("products")) {
                        String propValue = vm.get("jcr:content/metadata/dam:JLPProductCodes", String.class);
                        if(propValue == null){
                            propValue = "";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("deleted")) {
                        String propValue = vm.get("jcr:content/metadata/dam:ProductDeleted", String.class);
                        if("Y".equals(propValue)){
                            propValue = "Yes";
                        } else {
                            propValue = "No";
                        }
                            
                        csvContent.append(StringEscapeUtils.escapeCsv(propValue) +",");
                    }
                    if (title.equals("activation")) {
                        date="";
                        if (p != null) { 
                            date = vm.get("jcr:content/metadata/dateCompleted", String.class);
                            if(date==null)
                                date="";
                        }
                        csvContent.append(StringEscapeUtils.escapeCsv(date)+",");
                    }
                }
                csvContent.append("\r\n");
                o.write(csvContent.toString());
                o.flush();
                csvContent.setLength(0); // set length of buffer to 0
                csvContent.trimToSize();
            }
        }
    }
    totalAssetSize = totalAssetSize/1073741824.0; //converting the total asset size from Bytes to GB. 
    csvContent.append("\""+i18n.get("Assets Count")+":\",\"" + String.valueOf(assetCounts) + "\",\""+i18n.get("Assets Total Size")+":\",\"" + String.format("%.9f", totalAssetSize) + " GB\"");
    csvContent.append("\r\n");
    o.write(csvContent.toString());
    o.flush();
    csvContent.setLength(0); // set length of buffer to 0
    csvContent.trimToSize();
    o.close();

%>

