<%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="java.lang.reflect.Array,
                  java.util.HashMap,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.ui.components.Config,
                  java.util.Map,
                  java.util.List,
                  com.jl.aem.dam.service.integration.BriefDetailsService" %><%
%><%
	BriefDetailsService briefDetailsService = sling.getService(BriefDetailsService.class);
	String projectPath = request.getParameter("path");
	String projectName = briefDetailsService.getProjectName(projectPath);
	List<String> headers = briefDetailsService.getHeaders(projectPath);
	List<Map<String,String>> rows = briefDetailsService.getRows(projectPath); 
%>
<script>
    $('#briefdetails').find('coral-dialog-header')[0].innerHTML='<%=projectName%>';
</script>
<table class="briefdetailstable">
	<tr>
	<% for (String header : headers) {%>
		<th><%=header %></th>
	<%} %>
	</tr>
	<% for (Map<String,String> row : rows) { %>
		<tr>
			<% for (String header : headers) {
				String style = "";
				if (header.equals ("Ean Code") || header.equals("Stock Number") || header.equals("Web Sku") || header.equals("Quantity Received") || header.equals("No Alts")) {
				    style = "class='centeraligned'";
				}
			%>
				<td <%=style %>><%=row.get(header)!=null?row.get(header):"" %></td>
			<%} %>
		</tr>
	<% } %>
</table>
