<%@page session="false"
          import="javax.jcr.Node, 
    			  javax.jcr.NodeIterator,
				  com.day.cq.wcm.api.Page,
				  java.util.Iterator,
				  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
				  java.util.List,
				  java.util.LinkedList"
%><%@include file="/libs/foundation/global.jsp"%>

<%!
    class ProductVO {
        private String productType = null;
        private String[] buyingOffice = null;
        private String alts = null;
        private String url = null;
        private String pageTitle = null;
        private String pageUrl = null;
    
    
    
        public ProductVO(String productType, String[] buyingOffice, String alts, String url,
                String pageTitle, String pageUrl) {
            this.productType = productType;
            this.buyingOffice = buyingOffice;
            this.alts = alts;
            this.url = url;
            this.pageTitle = pageTitle;
            this.pageUrl = pageUrl;
        }
    
        public String getProductType() {
            return productType;
        }
    
        public String[] getBuyingOffice() {
            return buyingOffice;
        }
    
        public String getAlts() {
            return alts;
        }
    
        public String getUrl() {
            return url;
        }
    
        public String getPageTitle() {
            return pageTitle;
        }
    
        public String getPageUrl() {
            return pageUrl;
        }   
    
    }
	public List<ProductVO> traverseSubtree(Page pageChild) {
        List<ProductVO> prodList = new LinkedList<ProductVO>();
     	if (pageChild != null 
           	&& pageChild.getTemplate() != null 
            && "/apps/onejl-dam/templates/styleguideproduct".equals(pageChild.getTemplate().getPath())) {
            Resource productItemRes = pageChild.getContentResource("styleguideproduct_item");
            if (productItemRes != null) {
                ValueMap productVM = productItemRes.getValueMap();
                String productType = productVM.get("productType", "");
                String[] buyingOffice = productVM.get("buyingOffice", String[].class);
                String alts = productVM.get("alts", "");
                String url = productVM.get("url", "");
                ProductVO prodVO = new ProductVO(productType, buyingOffice, alts, url,
                                                 pageChild.getTitle(), pageChild.getPath());
                prodList.add(prodVO);

            }
        }
        Iterator<Page> pageIt = pageChild.listChildren();
		while (pageIt.hasNext()) {
            prodList.addAll(traverseSubtree(pageIt.next()));
        }
        return prodList;
	}
%>
<%
    List<ProductVO> prodList = traverseSubtree(currentPage);

	String productType = null;
    String[] buyingOffice = null;
    String alts = null;
    String url = null;
    String pageTitle = null; 
    String pageUrl = null; 
    for(ProductVO  prodVO: prodList){
        productType = prodVO.getProductType();
        buyingOffice = prodVO.getBuyingOffice();
        alts = prodVO.getAlts();
        url = prodVO.getUrl();
        pageTitle = prodVO.getPageTitle(); 
    	pageUrl = prodVO.getPageUrl();

%>
		<c:set var="productType" value="<%=productType%>" />
        <c:set var="buyingOffice" value="<%=buyingOffice%>" />
        <c:set var="alts" value="<%=alts%>" />
        <c:set var="url" value="<%=url%>" />

		<h2><%=pageTitle%> - <%=pageUrl%></h2>
        <c:choose>
            <c:when test="${not empty productType}">
                <c:out value="Product Type: ${productType}" />			
            </c:when>
            <c:otherwise>
                <span style="color: red;"> Please Configure Product Type</span>
            </c:otherwise>
        </c:choose>
		<br />
        <c:choose>
            <c:when test="${not empty buyingOffice}">	
				<c:forEach var="buyingOfficeItem" items="${buyingOffice}">
					<c:out value="Buying Office: ${buyingOfficeItem}" />
				</c:forEach>				
            </c:when>
            <c:otherwise>
                <span style="color: red;"> Please Configure Buying Office</span>
            </c:otherwise>
        </c:choose>
        <br />
        <c:choose>
            <c:when test="${not empty alts}">
                <c:out value="Number of Alts: ${alts}" />				
            </c:when>
            <c:otherwise>
                <span style="color: red;"> Please Configure Number of Alts</span>
            </c:otherwise>
        </c:choose>
        <br />
        <c:choose>
            <c:when test="${not empty url}">
                <c:out value="Url: ${url}" />				
            </c:when>
        </c:choose>
<%

    }
%>
