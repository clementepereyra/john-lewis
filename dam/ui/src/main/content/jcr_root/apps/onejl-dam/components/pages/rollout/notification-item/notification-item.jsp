<%@include file="/libs/foundation/global.jsp"%>

<%
    String[] emails = properties.get("emails",
            String[].class);

	String disableJob = properties.get("disableJob",
        String.class)!= null ? properties.get("disableJob",
    	        String.class) : "false";

	String ratio = properties.get("ratio",
        String.class);
	
	String observedPath = properties.get("observedPath",
	        String.class);
	
	String logoUrl = properties.get("logoUrl",
	        String.class);
	
	String logoAlt = properties.get("logoAlt",
	        String.class);
	
	String logoHeight = properties.get("logoHeight",
	        String.class);
	
	String signature = properties.get("signature",
	        String.class);
	

%>

<c:set var="disableJob" value="<%=disableJob%>" />
<c:set var="emails" value="<%=emails%>" />
<c:set var="ratio" value="<%=ratio%>" />
<c:set var="observedPath" value="<%=observedPath%>" />
<c:set var="logoUrl" value="<%=logoUrl%>" />
<c:set var="logoAlt" value="<%=logoAlt%>" />
<c:set var="logoHeight" value="<%=logoHeight%>" />
<c:set var="signature" value="<%=signature%>" />


<c:choose>
	<c:when test="${not empty emails}">
		<c:forEach var="email" items="${emails}">
			<li><c:out value="${email}" /><br /></li>
		</c:forEach>					
	</c:when>
	<c:otherwise>
		<span style="color: red;"> Please Configure Emails in Notification Items</span>
	</c:otherwise>
</c:choose>
<br /><br />	
<c:out value="Disable Notification: ${disableJob}" />
<br />
<c:out value="Expiration Date Ratio: ${ratio}" />
<br />
<c:out value="Observed Path: ${observedPath}" />
<br />
<c:out value="Email Logo Url: ${logoUrl}" />
<br />
<c:out value="Email Logo Alt Text: ${logoAlt}" />
<br />
<c:out value="Email Logo Height: ${logoHeight}" />
<br />
<c:out value="Email Signature: ${signature}" />
<br />

