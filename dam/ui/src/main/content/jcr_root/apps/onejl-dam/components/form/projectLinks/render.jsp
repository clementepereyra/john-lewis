<%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field,
                  com.adobe.granite.ui.components.Tag, javax.jcr.Node" %>
<label class="coral-Form-fieldlabel">Briefs</label>
<%
ValueMap vm = (ValueMap) request.getAttribute(Field.class.getName());
Object[] values = vm.get("value", Object[].class);
if (values.length == 0) {
	%><div class="projectLink">Not Linked to Any Brief</div><%
}
for (Object value : values) {
    String link = "/projects/details.html" + value.toString();
    String linkTitle = value.toString();

    Resource targetResource = resourceResolver.getResource(value.toString());
    if (targetResource != null) {
		linkTitle = targetResource.getChild("jcr:content").adaptTo(Node.class).getProperty("jcr:title").getString();
    }
    %><div class="projectLink"><a title="<%=linkTitle%>" class="coral-Icon coral-Icon--link withLabel" href="<%=link%>" target="_blank"> <%=linkTitle%></a></div><%
}
%>

