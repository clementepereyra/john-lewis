<%@include file="/libs/foundation/global.jsp"%>

<%
	String disableJob = properties.get("disableJob", String.class)!= null ? properties.get("disableJob", String.class) : "false";

	String logoUrl = properties.get("logoUrl", String.class);
	
	String logoAlt = properties.get("logoAlt", String.class);
	
	String logoHeight = properties.get("logoHeight", String.class);
	
	String signature = properties.get("signature", String.class);
	
%>

<c:set var="disableJob" value="<%=disableJob%>" />
<c:set var="logoUrl" value="<%=logoUrl%>" />
<c:set var="logoAlt" value="<%=logoAlt%>" />
<c:set var="logoHeight" value="<%=logoHeight%>" />
<c:set var="signature" value="<%=signature%>" />


<br />	
<c:out value="Disable Notification: ${disableJob}" />
<br />
<c:out value="Email Logo Url: ${logoUrl}" />
<br />
<c:out value="Email Logo Alt Text: ${logoAlt}" />
<br />
<c:out value="Email Logo Height: ${logoHeight}" />
<br />
<c:out value="Email Signature: ${signature}" />
<br />