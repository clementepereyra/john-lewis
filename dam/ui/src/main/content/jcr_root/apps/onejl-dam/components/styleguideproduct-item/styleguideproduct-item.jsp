<%@include file="/libs/foundation/global.jsp"%>

<%
	String productType = properties.get("productType",
	        String.class);
	
	String[] buyingOffice = properties.get("buyingOffice",
	        String[].class);
	
	String alts = properties.get("alts",
        String.class);

	
	String url = properties.get("url",
	        String.class);
		
%>

<c:set var="productType" value="<%=productType%>" />
<c:set var="buyingOffice" value="<%=buyingOffice%>" />
<c:set var="alts" value="<%=alts%>" />
<c:set var="url" value="<%=url%>" />

<c:choose>
	<c:when test="${not empty productType}">
		<c:out value="Product Type: ${productType}" />			
	</c:when>
	<c:otherwise>
		<span style="color: red;"> Please Configure Product Type</span>
	</c:otherwise>
</c:choose>
<br />
<c:choose>
	<c:when test="${not empty buyingOffice}">
		<c:forEach var="buyingOfficeItem" items="${buyingOffice}">
			<c:out value="Buying Office: ${buyingOfficeItem}" />
		</c:forEach>					
	</c:when>
	<c:otherwise>
		<span style="color: red;"> Please Configure Buying Office</span>
	</c:otherwise>
</c:choose>
<br />
<c:choose>
	<c:when test="${not empty alts}">
		<c:out value="Number of Alts: ${alts}" />				
	</c:when>
	<c:otherwise>
		<span style="color: red;"> Please Configure Number of Alts</span>
	</c:otherwise>
</c:choose>
<br />
<c:choose>
	<c:when test="${not empty url}">
		<c:out value="Url: ${url}" />				
	</c:when>
</c:choose>
<br />

