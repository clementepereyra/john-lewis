<%@include file="/libs/foundation/global.jsp"%>

<%
    String[] rolloutConstants = properties.get("constants",
            String[].class);
%>

<c:set var="rolloutConstants" value="<%=rolloutConstants%>" />

<c:choose>
	<c:when test="${not empty rolloutConstants}">
		<c:forEach var="constant" items="${rolloutConstants}">
			<li><c:out value="${constant}" /><br /></li>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<span style="color: red;"> Please Configure the Constants</span>
	</c:otherwise>
</c:choose>

