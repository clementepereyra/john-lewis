<%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field,
                  com.adobe.granite.ui.components.Tag, javax.jcr.Node,javax.jcr.NodeIterator,
                  java.util.List,
                  java.text.SimpleDateFormat,
                  java.util.Calendar,
                  java.util.Date,
                  org.apache.sling.api.resource.Resource" %><%
%><%
    // get the product resource from the URL
    Resource itemRes = resourceResolver.getResource(slingRequest.getRequestPathInfo().getSuffix());
    if (itemRes == null) {
        itemRes = resourceResolver.getResource(request.getParameter("item"));
    }
    if (itemRes == null) {
        return;
    }
    Node productNode = itemRes.adaptTo(Node.class);
    Node historyNode = productNode.hasNode("quantityHistory")?productNode.getNode("quantityHistory"):productNode.addNode("quantityHistory", "nt:unstructured");
	if (historyNode != null) {
	    NodeIterator it = historyNode.getNodes();
		while (it.hasNext()) {
		    Node item = it.nextNode();
		    String type = item.getProperty("type").getString();
		    String user = item.getProperty("user").getString();
		    String userName = item.getProperty("userName").getString();
		    Calendar cal = item.getProperty("date").getDate();
		    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, YYYY");
		    Date date = cal.getTime();
		    String quantity = item.getProperty("quantityReceived").getString();
		   	%><div class="projectLink">
		   		<%=sdf.format(date)%> <%=type.equals("receive")?" received":"despatched" %> <%=quantity.replace("-","") %> units by <%= userName%> (<%=user%>)
		   	  </div><%
		}
	}

%>

