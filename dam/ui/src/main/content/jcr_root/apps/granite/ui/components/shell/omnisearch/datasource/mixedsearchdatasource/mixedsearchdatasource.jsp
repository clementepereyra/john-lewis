<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page import="java.util.ArrayList,
                  java.util.Iterator,
                  java.util.Map,
                  java.util.Map.Entry,
                  java.util.HashMap,
                  java.util.Set,
                  org.apache.commons.collections.Transformer,
                  org.apache.commons.collections.iterators.TransformIterator,
                  org.apache.sling.api.resource.ResourceResolver,
                  org.apache.sling.api.resource.ResourceWrapper,
                  org.apache.sling.api.resource.SyntheticResource,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.omnisearch.api.core.OmniSearchService,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.ui.components.ds.AbstractDataSource,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.day.cq.search.result.SearchResult" %><%
%><%
    final OmniSearchService searchService = sling.getService(OmniSearchService.class);
    final ExpressionHelper ex = cmp.getExpressionHelper();
    final Config dsCfg = new Config(resource.getChild(Config.DATASOURCE));
    
    // Clone the paramMap since it returns an UnmodifiableMap
    @SuppressWarnings("unchecked")
    final Map<String, String[]> predicateParams = new HashMap<String, String[]>(request.getParameterMap());

    // Remove the location since we want mixed results
    predicateParams.remove("location");

    final long limit = ex.get(dsCfg.get("limit", "20"), long.class);
    final String moduleRT = dsCfg.get("moduleResourceType", String.class);

    final Map<String, SearchResult> result = searchService.getSearchResults(resourceResolver, predicateParams, limit, 0);
    
    final ArrayList<Resource> items = new ArrayList<Resource>();

    for (Entry<String, SearchResult> entry : result.entrySet()) {
        SearchResult searchResult = entry.getValue();
        
        if (searchResult.getTotalMatches() > 0) {
            String moduleID = entry.getKey();
            
            Resource configRes = searchService.getModuleConfiguration(resourceResolver, moduleID);

            String itemRT = "";
            String moduleName = moduleID;
            
            if (configRes != null) {
                ValueMap props = configRes.getValueMap();
                
                itemRT = props.get("cardPath", "granite/ui/components/shell/omnisearch/defaultcard");

                moduleName = i18n.getVar(props.get("jcr:title", String.class));
                if (moduleName == null) {
                    moduleName = moduleID;
                }
            }
            if (itemRT.equals("/libs/commerce/gui/components/admin/products/childcard")) {
				itemRT = "/apps/commerce/gui/components/admin/products/childcard";
            }
            
            items.add(new SearchModuleResource(resourceResolver, resource.getPath(), moduleID, moduleName, moduleRT, itemRT, searchResult));
        }
    }

    request.setAttribute(DataSource.class.getName(), new AbstractDataSource() {
        public Iterator<Resource> iterator() {
            return items.iterator();
        }
    });
%><%!
private class SearchModuleResource extends SyntheticResource {
    private SearchResult result;
    private String itemRT;
    private Map<String, Object> values;

    public SearchModuleResource(ResourceResolver resolver, String path, String moduleID, String moduleName, String moduleRT, String itemRT, SearchResult result) {
        super(resolver, path, moduleRT);
        this.result = result;
        this.itemRT = itemRT;

        values = new HashMap<String, Object>();
        values.put("granite-omnisearch-location-display", moduleName);
        values.put("granite-omnisearch-location-value", moduleID);
        values.put("granite-omnisearch-count", result.getTotalMatches());
        values.put("granite-omnisearch-count-hasmore", result.hasMore());
    }

    @SuppressWarnings("unchecked")
    public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {
        if (type.equals(ValueMap.class)) {
            return (AdapterType) new ValueMapDecorator(values);
        } else {
            return super.adaptTo(type);
        }
    }

    @SuppressWarnings("unchecked")
    public Iterator<Resource> listChildren() {
        return new TransformIterator(result.getResources(), new Transformer() {
            public Object transform(Object o) {
                Resource r = ((Resource) o);

                return new ResourceWrapper(r) {
                    public String getResourceType() {
                        return itemRT;
                    }
                };
            }
        });
    }
}
%>