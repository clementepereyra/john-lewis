/*
 ADOBE CONFIDENTIAL

 Copyright 2012 Adobe Systems Incorporated
 All Rights Reserved.

 NOTICE:  All information contained herein is, and remains
 the property of Adobe Systems Incorporated and its suppliers,
 if any.  The intellectual and technical concepts contained
 herein are proprietary to Adobe Systems Incorporated and its
 suppliers and may be covered by U.S. and Foreign Patents,
 patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material
 is strictly forbidden unless prior written permission is obtained
 from Adobe Systems Incorporated.
 */
(function($, undefined) {

    var ns = ".project-brief-upload";
    
    function previewFile(filename,briefID) {
    	jQuery.ajax({
            url: "/bin/jlp/public/previewbriefcsv?filename="+filename+"&briefID="+briefID,
            cache: false,
            contentType: false,
            processData: false,
            method: 'GET',
            type: 'GET', // For jQuery < 1.9
            success: function(data){
            	var preview = "";
            
            	if (data.valid) {
            		JLDAM.validcsv = true;
            		preview = preview + "<span style='color:green'>CSV is Valid</span><br/>";
            		if(data.briefID==='CUTOUT' || data.briefID==='RETOUCH') {
            			preview = preview + "<span style='font-weight:bold'>Products in Brief: </span><span>" + data.rows + "</span><br/>";
            		} else {
            			preview = preview + "<span style='font-weight:bold'>Looks in Brief: </span><span>" + data.rows + "</span><br/>";
                		
            		}
            		preview = preview + "<span style='font-weight:bold;'>Products in AEM cache</span><span style='word-break:break-all;'>: " + data.validProducts.length + "</span><br/>";
            		if (data.notValidProducts.length>0) {
            			preview = preview + "<span style='font-weight:bold;'>Products NOT in Product Hub: </span><span style='word-break:break-all;'>" + data.notValidProducts + "</span><br/>";
            		}
                    if (data.duplicatedProducts && data.duplicatedProducts.length>0) {
                        preview = preview + "<span style='font-weight:bold; color:red;'>The following products are already present in other briefs: </span><span style='word-break:break-all;'>" + data.duplicatedProducts + "</span><br/>";
                    }
            	} else {
            		JLDAM.validcsv = false;
            		preview = preview + "<span style='color:red'>CSV is NOT Valid</span><br/>";
            	}
            	$('.briefupload').adaptTo("foundation-validation").checkValidity();
            	$('.briefupload').adaptTo("foundation-validation").updateUI();

            	$("#fileinfo")[0].innerHTML = $("#fileinfo")[0].innerHTML.replace("<p>Processing file...</p>", "");
  				$("#fileinfo").append(preview);
            },
            failure: function() {
               // alert( "Could not preview file." );
            }
		});
    }

    function uploadCSV(name, src, hasChanged) {

        var uuid = generateUUID();
        var data = new FormData();
            data.append('_charset_', 'utf-8');
            data.append('file', src);

		jQuery.ajax({
            url: "/content/dam/projects/temp.createasset.html",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function(data){
            	if ($("#fileinfo").length>0) {
            		$('#fileinfo')[0].innerHTML = "";
            	} else {
            		$(".briefupload").append("<div id='fileinfo'></div>")
            	}
            	$('#fileinfo').append("<p class='.briefFileName'>"+name+"</p><p>Processing file...</p></div>");
                $("#filename")[0].value= name;
                var briefID = $('[name=briefID]')[0].value;
                previewFile("/content/dam/projects/temp/" + name, briefID);
            },
            failure: function() {
                alert( "Could not upload file." );
            }
		});


    }

    function generateUUID () { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    }

    $(document).on("foundation-contentloaded", function(e) {
        // Thumbnail upload
        $("coral-fileupload").off("change" + ns)
            .on("change" + ns, function (e) {
                var fileName = CQ.projects.utils.getFileName(e.target);
                if (fileName) {
                    if (fileName.match(/\.(csv)/i)) {
                        if (e.target.uploadQueue && window.FileReader) {
                            var file = e.target.uploadQueue[0];
                            var reader = new FileReader();
                                uploadCSV(fileName, file._originalFile, true);
                           // reader.readAsDataURL(file._originalFile);
                        }
                    }
                }
        });


        // update the image if there has been project data provided
        var path = $(".foundation-layout-thumbnail").data("projectcoverpath");
        if (path) {
            path += "?ck=" + new Date().getTime();
            var thumbnail = $(".foundation-layout-thumbnail .foundation-layout-thumbnail-image img");
            var newImg = $("<img class='image'>").attr({src:path});
            thumbnail.replaceWith(newImg);
        }
    });

    $(document).on("foundation-wizard-stepchange" + ns, ".foundation-wizard", function(e, to, from) {
        if ($(to).hasClass("cq-projects-experience-props")) {
            var thumbnail = $(".cq-projects-admin-createproject-thumbnail");
            if (thumbnail && thumbnail.length > 0) {
                var selection = $(".foundation-selections-item");
                if (selection && selection.length > 0) {
                    var title = selection.find(".project-link-default-name").data("value");
                    var desc = selection.find(".project-link-default-description").data("value");
                    thumbnail.find("coral-card-title").html(title);
                    thumbnail.find(".cq-projects-template-description coral-card-property-content").html(desc);

                    var oldImg = $(".cq-projects-admin-createproject-thumbnail img");
                    if (oldImg && oldImg.length == 0) {
                        var src = selection.find(".project-link-default-image").data("value");
                        var newImg = $("<img>").attr({src:src, title:title});
                        $(".cq-projects-admin-createproject-thumbnail coral-card-asset").append(newImg);
                    }
                }
            }
        }
    });

})(Granite.$);
