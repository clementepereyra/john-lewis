<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page import="java.util.Iterator,
                  java.util.Map,
                  org.apache.sling.api.resource.ResourceWrapper,
                  org.apache.commons.collections.Transformer,
                  org.apache.commons.collections.iterators.TransformIterator,
                  org.apache.commons.lang.StringUtils,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.ui.components.ds.AbstractDataSource,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
                  com.adobe.granite.omnisearch.api.core.OmniSearchService,
                  com.day.cq.search.result.SearchResult" %><%
    
    DataSource ds;
    
    @SuppressWarnings("unchecked")
    Map<String, String[]> predicateParameters = request.getParameterMap();

    final String location = request.getParameter("location");
    
    if (StringUtils.isBlank(location)) {
        // if the location is not set, return an empty collection because we are not able to mix the types
        ds = EmptyDataSource.instance();
    } else {
        ExpressionHelper ex = cmp.getExpressionHelper();
        Config dsCfg = new Config(resource.getChild(Config.DATASOURCE));

        final long offset = ex.get(dsCfg.get("offset", String.class), Long.class);
        final long limit = ex.get(dsCfg.get("limit", String.class), Long.class);

        OmniSearchService searchService = sling.getService(OmniSearchService.class);
        Map<String, SearchResult> result = searchService.getSearchResults(resourceResolver, predicateParameters, limit, offset);
        final SearchResult searchResult = result.get(location);

        if (searchResult == null) {
            ds = EmptyDataSource.instance();
        } else {
            String itemResourceType = "";
            Resource configRes = searchService.getModuleConfiguration(resourceResolver, location);

            if (configRes != null) {
                final String resourceType = dsCfg.get("itemType", "card");
                final String targetResourceType = "list".equals(resourceType) ? "listItemPath" : "cardPath";
                
                ValueMap vm = configRes.getValueMap();
                String defaultResourceType = "";
                if (targetResourceType.equals("cardPath")) {
                    defaultResourceType = "granite/ui/components/shell/omnisearch/defaultcard";
                }

                itemResourceType = vm.get(targetResourceType, defaultResourceType);
                if (itemResourceType.equals("/libs/commerce/gui/components/admin/products/childrow")) {
					itemResourceType = "/apps/commerce/gui/components/admin/products/childrow";
                } else if (itemResourceType.equals("/libs/cq/gui/components/projects/admin/card/projectcard")) {
					itemResourceType = "/apps/cq/gui/components/projects/admin/card/projectcard";
                } 
            }

            final String itemRT = itemResourceType;

            ds = new AbstractDataSource() {
                @SuppressWarnings("unchecked")
                public Iterator<Resource> iterator() {
                    return new TransformIterator(searchResult.getResources(), new Transformer() {
                        public Object transform(Object o) {
                            Resource r = (Resource) o;

                            return new ResourceWrapper(r) {
                                public String getResourceType() {
                                    return itemRT;
                                }
                            };
                        }
                    });
                }
            };
        }
    }

    request.setAttribute(DataSource.class.getName(), ds);
%>