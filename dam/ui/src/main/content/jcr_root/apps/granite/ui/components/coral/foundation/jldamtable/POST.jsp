<%--
  ADOBE CONFIDENTIAL

  Copyright 2016 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"
          import="javax.servlet.http.HttpServletRequestWrapper"%><%

// This POST handler is just a way to handle a scenario where the request URL is very long, such that it cannot be handled by GET.
// So we wrap the POST request into a GET request.
// It can be even implemented generically as servlet filter, using predefined param to do this POST tunnelling (e.g. `_method_`).
slingRequest.getRequestDispatcher(resource).forward(new GetRequestWrapper(request), response);

%><%!

private class GetRequestWrapper extends HttpServletRequestWrapper {
    public GetRequestWrapper(HttpServletRequest req) {
        super(req);
    } 

    @Override
    public String getMethod() {
        return "GET";
    }
} 
%>