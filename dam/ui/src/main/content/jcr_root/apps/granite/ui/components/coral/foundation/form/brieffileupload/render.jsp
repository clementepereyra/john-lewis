<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag" %><%--###
FileUpload
==========

.. granite:servercomponent:: /libs/granite/ui/components/coral/foundation/form/fileupload
   :supertype: /libs/granite/ui/components/coral/foundation/form/field
   
   A field component to upload file.
   
   It extends :granite:servercomponent:`Field </libs/granite/ui/components/coral/foundation/form/field>` component.
   
   It has the following content structure:

   .. gnd:gnd::

      [granite:FormFileUpload] > granite:FormField
      
      /**
       * The name that identifies the field when submitting the form.
       */
      - name (String)
      
      /**
       * A hint to the user of what can be entered in the field.
       */
      - emptyText (String) i18n
      
      /**
       * Indicates if the field is in disabled state.
       */
      - disabled (Boolean)
      
      /**
       * Indicates if the field is mandatory to be filled.
       */
      - required (Boolean)

       /**
       * ``true`` to upload the files asynchronously.
       */
      - async (Boolean)

      /**
       * The name of the validator to be applied. E.g. ``foundation.jcr.name``.
       * See :doc:`validation </jcr_root/libs/granite/ui/components/coral/foundation/clientlibs/foundation/js/validation/index>` in Granite UI.
       */
      - validation (String) multiple
      
      /**
       * Indicates if multiple files can be uploaded.
       */
      - multiple (Boolean)
      
      /**
       * ``true`` to make the upload starts automatically once the file is selected.
       */ 
      - autoStart (Boolean)
      
      /**
       * The URL to upload the file.
       * This is only required when ``autoStart`` is ``true``.
       */
      - uploadUrl (StringEL)
      
      /**
       * The file size limit.
       */
      - sizeLimit (Long)
      
      /**
       * The browse and selection filter for file selection. E.g: [".png",".jpg"] or ["image/\*"].
       */
      - mimeTypes (String) multiple

      /**
       * The text of the button.
       */
      - text (String) i18n
      
      /**
       * Visually hide the text.
       * It is RECOMMENDED that every button has a text for a11y purpose. Use this property to hide it visually, while still making it available for a11y.
       */
      - hideText (Boolean)
      
      /**
       * The icon of the button.
       */
      - icon (String)
      
      /**
       * The size of the icon.
       */
      - iconSize (String) = 'S' < 'XS', 'S', 'M', 'L'
      
      /**
       * The size of the button.
       */
      - size (String) = 'M' < 'M', 'L'
      
      /**
       * The variant of the button.
       */
      - variant (String) < 'primary', 'warning', 'quiet', 'minimal', 'actionBar'
###--%><%
%><ui:includeClientLib categories="cq.projects.admin.wizard.brieffileupload" /><%
    Config cfg = cmp.getConfig();
    
    boolean hideText = cfg.get("hideText", false);
    String[] mimeTypes = cfg.get("mimeTypes", new String[0]);
    
    Tag tag = cmp.consumeTag();
    
    AttrBuilder attrs = tag.getAttrs();
    cmp.populateCommonAttrs(attrs);
    attrs.add("name", cfg.get("name", String.class));
    attrs.add("placeholder", i18n.getVar(cfg.get("emptyText", String.class)));
    attrs.addDisabled(cfg.get("disabled", false));
    attrs.addMultiple(cfg.get("multiple", false));
    attrs.add("sizelimit", cfg.get("sizeLimit", String.class));
    attrs.addBoolean("autostart", cfg.get("autoStart", false));
    attrs.addHref("action", cmp.getExpressionHelper().getString(cfg.get("uploadUrl", String.class)));
    
    if (mimeTypes.length > 0) {
        attrs.add("accept", StringUtils.join(mimeTypes, ","));
    }
    
    attrs.addBoolean("required", cfg.get("required", false));
    attrs.addBoolean("async", cfg.get("async", false));
    
    String validation = StringUtils.join(cfg.get("validation", new String[0]), " ");
    attrs.add("data-foundation-validation", validation);
    attrs.add("data-validation", validation); // Compatibility
    
    %><coral-fileupload class="briefupload" <%= attrs.build() %>><%
        AttrBuilder buttonAttrs = new AttrBuilder(request, xssAPI);
        buttonAttrs.add("type", "button");
        buttonAttrs.add("is", "coral-button");
        buttonAttrs.add("icon", cfg.get("icon", String.class));
        buttonAttrs.add("iconsize", cfg.get("iconSize", String.class));
        buttonAttrs.add("coral-fileupload-select", "");
        buttonAttrs.add("coral-fileupload-dropzone", "");
        
        if (hideText) {
        	buttonAttrs.add("aria-label", i18n.getVar(cfg.get("text", String.class)));
        }
        
        String variant = cfg.get("variant", String.class);
        if ("actionBar".equals(variant)) {
        	buttonAttrs.addClass("coral-Button--graniteActionBar");
        	buttonAttrs.add("variant", "quiet");
        } else {
        	buttonAttrs.add("variant", variant);
        }
        
        %><button <%= buttonAttrs.build() %>><%
            if (!hideText) {
                out.print(outVar(xssAPI, i18n, cfg.get("text", "")));
            }
        %></button>
    </coral-fileupload>