<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/foundation/global.jsp" %><%
%><%@page session="false" contentType="text/html; charset=utf-8"%><%
%><%@page import="java.util.ArrayList,
                  java.util.Calendar,
                  java.util.List,
                  java.text.SimpleDateFormat,
                  javax.jcr.RepositoryException,
                  javax.jcr.Session,
                  javax.jcr.Node,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  org.apache.commons.lang.StringUtils,
                  org.apache.jackrabbit.util.Text,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  com.adobe.granite.security.user.util.AuthorizableUtil,
                  com.adobe.granite.ui.components.ComponentHelper,
                  com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  com.day.cq.commons.jcr.JcrConstants,
                  com.day.cq.i18n.I18n,
                  com.day.cq.wcm.core.utils.ScaffoldingUtils,
                  com.adobe.cq.commerce.api.Product,
                  com.adobe.cq.commerce.common.CommerceHelper,
                  java.util.Iterator,
                  org.apache.sling.api.resource.ResourceUtil"%><%

    ComponentHelper cmp = new ComponentHelper(pageContext);
    I18n i18n = cmp.getI18n();
    RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", slingRequest.getResourceBundle(request.getLocale()));

    AccessControlManager acm = null;
    try {
        acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }

    Product product = resource.adaptTo(Product.class);

    String scaffoldPath = properties.get("cq:scaffolding", "");
    if (scaffoldPath.length() == 0) {
        // search all scaffolds for a path match
        Resource scRoot = resourceResolver.getResource("/etc/scaffolding");
        Node root = scRoot == null ? null : scRoot.adaptTo(Node.class);
        if (root != null) {
            scaffoldPath = ScaffoldingUtils.findScaffoldByPath(root, resource.getPath());
        }
    }
    boolean hasTouchScaffold = false;
    if (scaffoldPath != null && scaffoldPath.length() > 0) {
        Resource scaffold = resourceResolver.getResource(scaffoldPath);
        hasTouchScaffold = scaffold != null && scaffold.getChild("jcr:content/cq:dialog") != null;
    }

    Calendar modifiedDateRaw = properties.get(JcrConstants.JCR_LASTMODIFIED, Calendar.class);
    String modifiedDate = formatDate(modifiedDateRaw, i18n.get("never"), rtf);
    String modifiedBy = AuthorizableUtil.getFormattedName(resourceResolver, properties.get(JcrConstants.JCR_LAST_MODIFIED_BY, String.class));
    if (modifiedBy == null) {
        modifiedBy = "";
    }

    Calendar publishedDateRaw = properties.get("cq:lastReplicated", Calendar.class);
    String publishedDate = formatDate(publishedDateRaw, null, rtf);
    String publishedBy = AuthorizableUtil.getFormattedName(resourceResolver, properties.get("cq:lastReplicatedBy", String.class));
    if (publishedBy == null) {
        publishedBy = "";
    }

    Calendar createdDateRaw = properties.get(JcrConstants.JCR_CREATED, Calendar.class);
    Calendar twentyFourHoursAgo = Calendar.getInstance();
    twentyFourHoursAgo.add(Calendar.DATE, -1);
    if ((createdDateRaw == null) || (modifiedDateRaw != null && modifiedDateRaw.before(createdDateRaw))) {
        createdDateRaw = modifiedDateRaw;
    }
    boolean isNew = createdDateRaw != null && twentyFourHoursAgo.before(createdDateRaw);

    String lastReplicationAction = properties.get("cq:lastReplicationAction", String.class);
    boolean deactivated = "Deactivate".equals(lastReplicationAction);
    String title = CommerceHelper.getCardTitle(resource, pageManager);

    List<String> applicableRelationships = new ArrayList<String>();
    if (product != null) {
        applicableRelationships.add("cq-commerce-products-createvariation-activator");
        applicableRelationships.add("cq-commerce-collections-addcollectiontoproduct-activator");
    } else {
        applicableRelationships.add("cq-commerce-products-createproduct-activator");
        applicableRelationships.add("cq-commerce-products-createfolder-activator");
        applicableRelationships.add("cq-commerce-products-import-activator");
    }

    if (hasPermission(acm, resource, Privilege.JCR_READ)) {
        if (product != null) {
            if (hasTouchScaffold) {
                applicableRelationships.add("foundation-admin-properties-activator");
            } else {
                applicableRelationships.add("cq-commerce-products-edit-activator");
            }
        }
        else {
            applicableRelationships.add("cq-commerce-products-folderproperties-activator");
        }
    }

    if (hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE)) {
        if (product != null) {
            applicableRelationships.add("cq-commerce-products-move-activator");
        } else {
            applicableRelationships.add("cq-commerce-products-movefolder-activator");
        }
        applicableRelationships.add("cq-commerce-products-delete-activator");
        applicableRelationships.add("cq-commerce-collections-removefromcollection-activator");
        // enable collection folders to be removed
        applicableRelationships.add("cq-commerce-collections-removecollection-activator");
    }

    if (hasPermission(acm, resource, "crx:replicate")) {
        applicableRelationships.add("cq-commerce-products-publish-activator");
        applicableRelationships.add("cq-commerce-products-unpublish-activator");
    }

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();
    String imageUrl;
    boolean isFolder = false;
    if (product != null) {
        imageUrl = (product != null) ? CommerceHelper.getProductCardThumbnail(request.getContextPath(), product) : "";
        attrs.addClass("card-asset");
        if (hasVariantChildren(resource)) {
            attrs.addClass("stack");
        }
    } else {
        imageUrl = request.getContextPath() + xssAPI.getValidHref(resource.getPath()) + ".folderthumbnail.jpg?width=240&height=160";
        isFolder = true;
    }
    attrs.add("itemprop", "item");
    attrs.add("itemscope", "itemscope");
    attrs.add("data-timeline", true);
    attrs.add("data-gridlayout-sortkey", isNew ? 10 : 0);
    attrs.add("data-path", resource.getPath()); // for compatibility

	attrs.addClass("foundation-collection-navigator");
    //attrs passed to component may contain this tag, avoid setting it twice, tag already set is not our usecase.
    if (!attrs.build().contains("data-foundation-collection-navigator-href=")) {
        attrs.add("data-foundation-collection-navigator-href",
                  isFolder?xssAPI.getValidHref(request.getContextPath() + getAdminUrl(resource, currentPage)):
        xssAPI.getValidHref(getPropertiesHref(request, resource, product)));
    }

    if (request.getAttribute("cq.6.0.legacy.semantics.foundation-collection") != null) {
        attrs.addClass("foundation-collection-item");
        attrs.add("data-foundation-collection-item-id", resource.getPath());
    }

    boolean showQuickActions = true;
    Object quickActionsAttr = request.getAttribute("com.adobe.cq.item.quickActions");
    if (quickActionsAttr != null) {
        if (quickActionsAttr.getClass().getName().equals("java.lang.String")) {
            showQuickActions =  quickActionsAttr.equals("true");
        } else {
            showQuickActions = (Boolean) quickActionsAttr;
        }
    }

    if (isFolder) {
        attrs.add("variant", "inverted");
    }

    %><coral-card <%= attrs.build() %>><%
        if(StringUtils.isNotBlank(imageUrl)){%>
        <coral-card-asset>
            <img src="<%=xssAPI.getValidHref(imageUrl)%>"/>
        </coral-card-asset>
        <%
        }

        if (product != null) { %>
        	<%String imageStatus = product.getProperty("statusId", String.class); %>
             <coral-card-info>
             	<coral-tag color="blue" class="u-coral-pullRight"><%=imageStatus %></coral-tag>
             </coral-card-info>

            <coral-card-propertylist>
                <coral-card-property title="<%= xssAPI.encodeForHTMLAttr(i18n.get("SKU")) %>"><%= xssAPI.encodeForHTML(product.getSKU()) %></coral-card-property>
            </coral-card-propertylist>
	        <coral-card-propertylist>
            	<coral-card-property icon="edit" title="<%= xssAPI.encodeForHTMLAttr(i18n.get("Modified")) %>">
                	<time datetime="<%= modifiedDateRaw != null ? modifiedDateRaw.getTimeInMillis() : 0 %>"><%= xssAPI.encodeForHTML(modifiedDate) %></time>
            	</coral-card-property><%
                if (!deactivated && publishedDate != null) {
                    %><coral-card-property icon="globe" title="<%= xssAPI.encodeForHTMLAttr(i18n.get("Published")) %>"><time datetime="<%= xssAPI.encodeForHTMLAttr(String.valueOf(publishedDateRaw.getTimeInMillis())) %>"><%= xssAPI.encodeForHTML(publishedDate) %></time></coral-card-property><%
                } else {
                    %><coral-card-property icon="globeRemove"><%= xssAPI.encodeForHTML(i18n.get("Not published")) %></coral-card-property><%
                }%>
			</coral-card-propertylist> <%
        }%>
        <coral-card-content><%
        	String context = isFolder ? i18n.get("Folder") : null;
        	if (context != null) {
            	%><coral-card-context class="coral-Card-context"><%= xssAPI.encodeForHTML(context) %></coral-card-context><%
        	}
        	%><coral-card-title class="foundation-collection-item-title"><%= xssAPI.encodeForHTML(title) %></coral-card-title>
        </coral-card-content>
    	<meta class="foundation-collection-quickactions" data-foundation-collection-quickactions-rel="<%= xssAPI.encodeForHTMLAttr(StringUtils.join(applicableRelationships, " ")) %>">
        <link rel="properties" href="<%=xssAPI.getValidHref(getPropertiesHref(request, resource, product))%>">
    </coral-card>
    <% if(showQuickActions){%>
        <coral-quickactions target="_prev" alignmy="left top" alignat="left top">
            <coral-quickactions-item icon="check" class="foundation-collection-item-activator"><%= xssAPI.encodeForHTML(i18n.get("Select")) %></coral-quickactions-item><%
                if (hasPermission(acm, resource, Privilege.JCR_READ)) {
                    // show touch-optimized scaffold in properties view: %>
                <coral-quickactions-item icon="infoCircle" class="foundation-anchor" data-foundation-anchor-href="<%= xssAPI.getValidHref(getPropertiesHref(request, resource, product)) %>"><%= xssAPI.encodeForHTML(getPropertiesTitle(i18n, product)) %></coral-quickactions-item><%
                }
            if (hasPermission(acm, resource, "crx:replicate")) { %>
                <coral-quickactions-item icon="globe" class="foundation-collection-action"
                data-foundation-collection-action='{"action": "cq.wcm.publish", "data": {"referenceSrc": "<%= request.getContextPath() %>/libs/wcm/core/content/reference.json?_charset_=utf-8{&path*}", "wizardSrc": "<%= request.getContextPath() %>/libs/wcm/core/content/sites/publishpagewizard.html?_charset_=utf-8{&item*}"}}'><%= xssAPI.encodeForHTML(i18n.get("Publish")) %></coral-quickactions-item>
                <%
            } %>
        </coral-quickactions>
    <%}%>
<%!

    private String getPropertiesHref(HttpServletRequest request, Resource resource, Product product) {
        if (product != null) {
            return request.getContextPath() + "/libs/commerce/gui/content/products/properties.html?item=" + Text.escapePath(resource.getPath());
        } else {
            return request.getContextPath() + "/libs/commerce/gui/content/products/folderproperties.html" + Text.escapePath(resource.getPath());
        }
    }

    private String getPropertiesTitle(I18n i18n, Product product) {
        if (product != null) {
            return i18n.get("View Product Data");
        } else {
            return i18n.get("View Properties");
        }
    }

    private boolean hasPermission(AccessControlManager acm, Resource child, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(child.getPath(), new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // ignore
        }
        return false;
    }

    private boolean hasVariantChildren(Resource resource) {
        for (Iterator<Resource> it = resource.listChildren(); it.hasNext();) {
            Resource r = it.next();
            if (ResourceUtil.getValueMap(r).get("cq:commerceType", "").equals("variant")) {
                return true;
            }
        }
        return false;
    }

    private String getAdminUrl(Resource pageResource, Page requestPage) {
        String base = requestPage != null ? requestPage.getVanityUrl() : "/aem/products";

        if (base == null) {
            base = requestPage.getProperties().get("sling:vanityPath", base);
        }

        if (base == null) {
            base = Text.escapePath(requestPage.getPath());
        }

        // when viewing the collection members, clicking a product card opens the product properties page
        if (requestPage != null) {
            String productPropertiesAdminUrl = requestPage.getProperties().get("productPropertiesAdminUrl", String.class);
            if (StringUtils.isNotEmpty(productPropertiesAdminUrl)) {
                base = productPropertiesAdminUrl + ".html?item=";
            } else {
                base = base + ".html";
            }
        } else {
            base = base + ".html";
        }

        return base + Text.escapePath(pageResource.getPath());
    }

    private String formatDate(Calendar cal, String defaultValue, RelativeTimeFormat rtf) {
        if (cal == null) {
            return defaultValue;
        }
        try {
            return rtf.format(cal.getTimeInMillis(), true);
        } catch (IllegalArgumentException e) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(cal.getTime());
        }
    }

%>