<%--
  ADOBE CONFIDENTIAL

  Copyright 2014 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"
          import="com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag,
                  org.apache.sling.api.resource.Resource,
                  org.apache.commons.lang3.StringUtils,
                  com.jl.aem.dam.service.integration.DashboardService,
                  java.lang.Math,
				  com.day.cq.search.eval.JcrPropertyPredicateEvaluator,
				  org.apache.sling.api.resource.ResourceResolver"%>
<%@ page import="javax.jcr.Session" %>
<%@ page import="javax.jcr.RepositoryException" %>
<%@ page import="java.util.List" %>
<%@ page import="com.adobe.cq.projects.api.Project" %>
<%@ page import="org.apache.sling.api.resource.ValueMap" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.adobe.granite.security.user.UserPropertiesService" %>
<%@ page import="com.adobe.granite.security.user.UserPropertiesManager" %>
<%@ page import="org.apache.jackrabbit.api.security.user.UserManager" %>
<%@ page import="org.apache.jackrabbit.api.security.user.Authorizable" %>
<%@ page import="org.apache.sling.jcr.base.util.AccessControlUtil" %>
<%@ page import="com.adobe.granite.security.user.UserProperties" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="javax.jcr.security.AccessControlManager" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.security.Privilege" %>
<%@ page import="org.apache.jackrabbit.JcrConstants" %>
<%@ page import="org.apache.commons.lang3.time.DateUtils" %><%

    String suffix = slingRequest.getRequestPathInfo().getSuffix();
    Resource projectResource = resourceResolver.getResource(suffix);

    AccessControlManager acm = null;

    Session userSession = resourceResolver.adaptTo(Session.class);
    try {
        acm = userSession.getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }
    List<String> permissions = new ArrayList<String>();
    permissions.add("cq-project-admin-actions-open-activator");
    if (hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE)) {
        permissions.add("cq-projects-admin-actions-delete-link-activator");
    }

    Project project = projectResource.adaptTo(Project.class);
    Resource projectContent = projectResource.getChild(JcrConstants.JCR_CONTENT);
    ValueMap projectValueMap = projectResource.adaptTo(ValueMap.class);
    ValueMap projectContentVM = projectContent.adaptTo(ValueMap.class);
    Calendar dueDate = projectContentVM.get("project.dueDate", Calendar.class);
    Calendar startDate = projectContentVM.get("project.startDate", Calendar.class);
    Calendar endShootDate = projectContentVM.get("project.shootendDate", Calendar.class);
    Calendar startShootDate = projectContentVM.get("project.shootstartDate", Calendar.class);
    Calendar reDueDate = projectContentVM.get("project.redueDate", Calendar.class);
    Calendar reStartShootDate = projectContentVM.get("project.reshootstartDate", Calendar.class);
    Calendar reEndShootDate = projectContentVM.get("project.reshootendDate", Calendar.class);
    String endUse = projectContentVM.get("endUse", String.class);
    String assetsPath = projectContentVM.get("damFolderPath", String.class);
    String status = projectContentVM.get("JLPStatus", String.class);
    boolean isCompleted = "COMPLETE".equals(status);
    boolean isCancelled = "CANCELLED".equals(status);
    String completedOn = projectContentVM.get("JLPCompletedOn", String.class);
    boolean isBriefProject = projectContentVM.get("cq:template", String.class).indexOf("brief")>=0;
    String creatorImagePath = null;
    String xssEmail = null;
    String nameHtml = "";
    String creatorId = projectValueMap.get("jcr:createdBy", String.class);
    UserPropertiesService upService = sling.getService(UserPropertiesService.class);
    UserPropertiesManager upm = upService.createUserPropertiesManager(userSession, resourceResolver);
    UserManager um = AccessControlUtil.getUserManager(userSession);
    Authorizable authorizable = null;
    if (um != null) {
        authorizable = um.getAuthorizable(creatorId);
    }
    UserProperties up = authorizable != null ? upm.getUserProperties(authorizable, "profile") : null;
    String image = (up == null) ? "" : up.getResourcePath(UserProperties.PHOTOS, "/primary/image.prof.thumbnail.48.png", "");
    if (StringUtils.isBlank(image)) {
        if (authorizable != null && authorizable.isGroup()) {
            image = "/libs/granite/security/clientlib/themes/default/resources/sample-group-thumbnail.36.png";
        } else {
            image = "/libs/granite/security/clientlib/themes/default/resources/sample-user-thumbnail.100.png";
        }
    }
    if (authorizable != null) {
        String name = getFullName(authorizable,  up);
        if (name != null) {
            nameHtml = xssAPI.encodeForHTML(name);
        }
    }
    String email = (up == null) ? "" : up.getProperty(UserProperties.EMAIL);
    if (email != null) {
        xssEmail = xssAPI.encodeForHTML(email);
    }
    creatorImagePath = request.getContextPath() + image;
    String urlEncodedProjectPath = URLEncoder.encode(projectResource.getPath(), "UTF-8");
    String projectPropertiesHref = request.getContextPath() + "/libs/cq/core/content/projects/properties.html?item=" + urlEncodedProjectPath;
    String xssTitle = xssAPI.encodeForHTML(project.getTitle());
    String xssDescription = "";
    DashboardService dashboardService = sling.getService(DashboardService.class);
    String projectPath = projectResource.getPath();
    long assetCount = dashboardService.getAssetUploadedCount(projectPath);
    long assetsRequested = dashboardService.getAssetRequestedCount(projectPath);
	String xssAssetCount = xssAPI.encodeForHTML(((assetCount*100)/(assetsRequested>0?assetsRequested:1)) + "% Assets Shot: " + assetCount + "/"+ assetsRequested);
    
	//String xssAssetsRequested = xssAPI.encodeForHTML("Total Shoots Requested: " + assetsRequested);
    long productsWithSamples = dashboardService.getAllProductsWithSamplesCount(projectPath);
    long productsInBrief = dashboardService.getAllProductsRequestedCount(projectPath);
    String xssAssetSamples = xssAPI.encodeForHTML(((productsWithSamples*100)/(productsInBrief>0?productsInBrief:1))+ "% Samples Received: " + productsWithSamples + "/"+ productsInBrief);
    String description = project.getDescription();
    boolean isProjectActive = project.isActive() && !isCancelled && !isCompleted;
    boolean isOverdue = false;
    if (description != null) {
        xssDescription = xssAPI.encodeForHTML(description);
    }
	boolean isCSVParseFail = projectContentVM.get("project.csverror", Boolean.FALSE);
    SimpleDateFormat fmt_start = new SimpleDateFormat(i18n.get("MMM dd","Project info card"), request.getLocale());
    SimpleDateFormat fmt_due = new SimpleDateFormat(i18n.get("MMM dd","Project info card"), request.getLocale());
    String xssDueDate = null;
    String xssStartDate = null;
    String xssStartDatePretext = null;
    String xssEndShootDate = null;
    String xssStartShootDate = null;
    String xssStartShootDatePretext = null;
    String xssReDueDate = null;
    String xssReStartShootDate = null;
    String xssReEndShootDate = null;
    if(Calendar.getInstance().after(startDate))  {
        xssStartDatePretext = i18n.get("Started on");
    } else {
        xssStartDatePretext = i18n.get("Starts on");
    }
	if(Calendar.getInstance().after(startShootDate))  {
        xssStartShootDatePretext = i18n.get("Shoot Started on");
    } else {
        xssStartShootDatePretext = i18n.get("Shoot Starts on");
    }

    if (dueDate != null) {
        xssDueDate = xssAPI.filterHTML(fmt_due.format(dueDate.getTime()));
        isOverdue = DateUtils.truncatedCompareTo(Calendar.getInstance(), dueDate, Calendar.MILLISECOND) > 0;
    }
    if (startDate != null) {
        xssStartDate = xssAPI.filterHTML(fmt_start.format(startDate.getTime()));
    }
	if (endShootDate != null) {
        xssEndShootDate = xssAPI.filterHTML(fmt_due.format(endShootDate.getTime()));
        //isOverdue = DateUtils.truncatedCompareTo(Calendar.getInstance(), endShootDate, Calendar.MILLISECOND) > 0;
    }
    if (startShootDate != null) {
        xssStartShootDate = xssAPI.filterHTML(fmt_start.format(startShootDate.getTime()));
    }
    
    if (reDueDate != null) {
        xssReDueDate = xssAPI.filterHTML(fmt_due.format(reDueDate.getTime()));
        isOverdue = DateUtils.truncatedCompareTo(Calendar.getInstance(), reDueDate, Calendar.MILLISECOND) > 0;
    }
    if (reStartShootDate != null) {
        xssReStartShootDate = xssAPI.filterHTML(fmt_start.format(reStartShootDate.getTime()));
    }
	if (reEndShootDate != null) {
        xssReEndShootDate = xssAPI.filterHTML(fmt_due.format(reEndShootDate.getTime()));
    }
    
    String xssEndUse = xssAPI.encodeForHTML(endUse);
    
    ValueMap cardMap = resource.adaptTo(ValueMap.class);
    int cardWeight = cardMap.get("cardWeight", 0);


    Config cfg = new Config(projectResource);
    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();
    attrs.addOthers(cfg.getProperties(), "class");

    tag.printlnStart(out);

%>

<div class="cq-projects-AppDetails cq-projects-Pod-content">
    <div class="foundation-layout-media"><%
        if (creatorImagePath != null && creatorImagePath.length() > 0) {
    %><img src="<%=xssAPI.getValidHref(creatorImagePath)%>" class="cq-projects-Pod-projectInfo-img foundation-layout-media-img"><%
        } %>
        <div class="foundation-layout-media-bd"><h4><%= nameHtml %></h4><%
            if ( xssEmail != null ) {
        %><span class="email"><%= xssEmail%></span><%
            }
        %></div>
    </div>
    <div class="cq-projects-Pod-projectInfo-title"><span><%= xssTitle %></span></div>
    <div><span><%= i18n.get("End use: {0}", "Show the end use", xssEndUse) %></span></div><%
        if ( isProjectActive ) {
            if (isOverdue) {
                %><coral-icon icon="alert" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active-overdue"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Active (Overdue)")) %></span><%
            }
            else {
                %><coral-icon icon="clock" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Active")) %></span><%
                }
        } else if (isCompleted) {
    %><coral-icon icon="checkCircle" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-inactive"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Complete on " + completedOn)) %></span><%
        }  else if (isCancelled){
    %><coral-icon icon="checkCircle" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active-overdue"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Cancelled on " + completedOn)) %></span><%
        } else {
            %><coral-icon icon="checkCircle" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-inactive"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Not Active")) %></span><%
        }
    	if(isCSVParseFail) {
	%>
    <coral-icon icon="alert" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active-overdue"></coral-icon><span class="cq-projects-AppDetails-status"><%= xssAPI.encodeForHTML(i18n.get("Wrong CSV File")) %></span>
    <%
    	}
    	if(isBriefProject) {
     %>
    <div class="cq-projects-Pod-projectInfo-description">
		<span><%= xssAssetSamples %></span>
		<span><%= xssAssetCount %></span>
    </div>
    <%
    	} else {
    %>
    <div class="cq-projects-Pod-projectInfo-description">
    	<span><%= xssDescription %></span>
    </div>
    <%
    }
    %>
    <div class="cq-projects-Pod-projectInfo-bottom">
    <%
        if (xssStartDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("{0} {1}", "Show the start date of the project", xssStartDatePretext, xssStartDate) %></span><br/><%
            } %>
    <%
        if (xssDueDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("Due on {0}", "Show the due date of the project", xssDueDate) %></span><br/><%
            } 
        if (xssStartShootDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("{0} {1}", "Show the start shoot date of the project", xssStartShootDatePretext, xssStartShootDate) %></span><br/><%
            } %>
    <%
        if (xssEndShootDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("Shoot End Date on {0}", "Show the end shoot date of the project", xssEndShootDate) %></span><br/><%
            } %>
    
    <%
        if (xssReDueDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("Rescheduled Delivery Date on {0}", "Show the Rescheduled Delivery date of the project", xssReDueDate) %></span><br/><%
            } %>
    <%
        if (xssReStartShootDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("Re-Shoot Start Date on {0}", "Show the Re-Start shoot date of the project", xssReStartShootDate) %></span><br/><%
            } %> 
    <%
        if (xssReEndShootDate != null) {
    %><coral-icon icon="clock" size="XS" ></coral-icon>
        <span><%= i18n.get("Re-Shoot End Date on {0}", "Show the Re-End shoot date of the project", xssReEndShootDate) %></span><br/><%
            } %>                               
    </div>
</div>
<%
    tag.printlnEnd(out);
%>

<%--<div class="cq-projects-AppDetails"><%
    if (StringUtils.isNotBlank(description)) { %>
        <h2 class="cq-projects-AppDetails-sectionTitle"><%= i18n.get("Description") %></h2>
        <span class="cq-projects-AppDetails-description"><%= xssAPI.encodeForHTML(description) %></span><%
    }
%>--%>
<%!
    boolean hasPermission(AccessControlManager acm, Resource resource, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(resource.getPath(), new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // if we have a error then we will return false.
        }
        return false;
    }
    private String getFullName(Authorizable authorizable, UserProperties up) {
        try {
            // if we have no user profile, we return the ID
            if (up == null) return authorizable.getID();
            String givenName = up.getProperty(UserProperties.GIVEN_NAME);
            String familyName = up.getProperty(UserProperties.FAMILY_NAME);
            String name = "";
            if (givenName != null) name += givenName;
            if (givenName != null && familyName != null) name += " ";
            if (familyName != null) name += familyName;
            if (name.length() == 0) name = authorizable.getID();
            return name;
        } catch (RepositoryException e) {
            return "";
        }
    }
    
    
%>

