<%--
  ADOBE CONFIDENTIAL

  Copyright 2014 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
          javax.jcr.security.AccessControlManager,
          javax.jcr.RepositoryException,
          javax.jcr.Session,
		  javax.jcr.Node,
          java.util.List,
          java.util.ArrayList,
          javax.jcr.security.Privilege,
          com.day.cq.search.Query,
				  com.day.cq.search.QueryBuilder,
				  com.day.cq.search.PredicateGroup,
				  com.day.cq.search.result.SearchResult,
          org.apache.sling.api.resource.Resource,
          com.adobe.granite.ui.components.AttrBuilder,
          com.adobe.granite.ui.components.Config,
          org.apache.sling.resource.collection.ResourceCollection,
          com.day.cq.dam.commons.util.DamUtil" %><%
    ResourceCollection collection = (ResourceCollection) request.getAttribute("collection");
    Resource collectionResource = (Resource) request.getAttribute("collectionResource");
    String xssHref = (String) request.getAttribute("collectionPath");
    if (xssHref != null) {
    	xssHref = xssHref.replace("/libs/","/mnt/overlay/");
    }
%>
<div class="cq-projects-CardDashboard-footer"><%
    boolean isSmartCollection;
    if (collection != null) {
        Config collCfg = new Config(collectionResource);
        isSmartCollection = DamUtil.isSmartCollection(collectionResource);
     	long resultCount = 0;
        if (isSmartCollection) {
            Node childCollectionNode = collectionResource.adaptTo(Node.class);
            QueryBuilder queryBuilder = sling.getService(QueryBuilder.class);
            PredicateGroup rootPredicate = new PredicateGroup();
            Query myquery = queryBuilder.loadQuery(childCollectionNode.getPath()
                        + "/dam:query", resourceResolver.adaptTo(Session.class));
            rootPredicate.add(myquery.getPredicates());
            rootPredicate.setAllRequired(true);
            myquery = queryBuilder.createQuery(rootPredicate, resourceResolver.adaptTo(Session.class));
            myquery.setHitsPerPage(1L);
            SearchResult result = myquery.getResult();
            resultCount = result.getTotalMatches();
        }
        String xssTitle = xssAPI.filterHTML(collCfg.get("jcr:title", "")); %>
    <div class="cq-projects-Pod-collection-footer-text">
        <h4><%=xssTitle%></h4>
        <div class="info-collection">
            <p class="type collection"><%= isSmartCollection?i18n.get("SMART COLLECTION", "i.e. query based collection of assets")+ " (" + resultCount + " assets)":i18n.get("COLLECTION", "collection of resources/assets")%></p>
        </div>
    </div>
    <a class="coral-Button coral-Button--square coral-Button--quiet" href="<%= xssHref %>">
        <i class="coral-Icon coral-Icon--more"></i>
    </a><%
        }%>
</div>

