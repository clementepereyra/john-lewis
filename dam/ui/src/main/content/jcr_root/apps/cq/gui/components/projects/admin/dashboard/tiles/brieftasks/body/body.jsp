<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"
          import="org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  javax.jcr.RepositoryException,
                  javax.jcr.Session,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  org.apache.jackrabbit.JcrConstants,
                  org.apache.sling.commons.json.JSONObject,
                  org.apache.sling.commons.json.JSONArray,
                  com.jl.aem.dam.service.integration.DashboardService,
                  com.jl.aem.dam.service.integration.PieChartView" %>
<%@ taglib prefix="cq" uri="http://www.adobe.com/taglibs/granite/ui/1.0" %>
<%
    AccessControlManager acm = null;
    try {
        acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }

    String projectPath = slingRequest.getRequestPathInfo().getSuffix();
    Resource projectResource = resourceResolver.getResource(projectPath);
    if (     projectResource == null
            || !projectResource.isResourceType("cq/gui/components/projects/admin/card/projectcard") ) {
        return;
    }

    boolean canCreateTasks = hasPermission(acm, projectResource.getPath() + "/jcr:content", Privilege.JCR_WRITE);

    String ctx = request.getContextPath();
    final String xssHref = xssAPI.getValidHref(ctx + "/libs/cq/core/content/projects/showtasks.html?item=" + projectResource.getPath());
    final String wizardHref = xssAPI.getValidHref(ctx + "/libs/cq/inbox/content/inbox/addtask.html?projectPath=" + projectPath);

    Resource tasks = projectResource.getChild(JcrConstants.JCR_CONTENT + "/tasks");
    long xssTaskCount = 0;
    long xssActiveTasks = 0;
    long xssTotalOverdue = 0;
    long xssTaskCompleted = 0;
    long xssTaskPercentCompleted = 100;

    if (tasks != null) {
        ValueMap props = tasks.adaptTo(ValueMap.class);
        if (props.containsKey("totalTasks")) {
            xssTaskCount = props.get("totalTasks", Long.class);
        }

        if (props.containsKey("activeTasks")) {
            xssActiveTasks = props.get("activeTasks", Long.class);
        }

        if (props.containsKey("overdueTasks")) {
            xssTotalOverdue = props.get("overdueTasks", Long.class);
        }

        // completed are the total - (active + overdue)
        xssTaskCompleted = xssTaskCount - (xssActiveTasks + xssTotalOverdue);


        if (xssTaskCount != 0) {
            xssTaskPercentCompleted = (long) (100.0*(double)xssTaskCompleted/xssTaskCount);
        }
    }

    ValueMap cardMap = resource.adaptTo(ValueMap.class);

	DashboardService dservice = sling.getService(DashboardService.class);
	PieChartView pcview = dservice.getBriefStatus(projectPath);

    JSONObject data = new JSONObject();

    JSONArray yArray = new JSONArray();
    for (Long y : pcview.getyAxis()) {
        yArray.put(y);
    }


    JSONArray xArray = new JSONArray();
    for (String x : pcview.getxAxis()) {
        xArray.put(x);
    }


    JSONArray sArray = new JSONArray();
    for (String s : pcview.getSeries()) {
        sArray.put(s);
    }

    data.put("y", yArray);
    data.put("x", xArray);
    data.put("series", sArray);

    String xssTileid = xssAPI.encodeForHTML((String) request.getAttribute("cq-projects-tile-id"));

%>
<div class="cq-projects-Pod-content js-cq-projects-Pod-tasksPod">
    <div class="u-coral-clearFix task-data">
            <project-brieftaskstats stats='<%=data.toString()%>' size="L" legendVisible="true" outerRadius="25%" innerRadius="62%"></project-brieftaskstats>
        
    </div>
</div>
<script>
    var cnt = <%=xssActiveTasks+xssTotalOverdue%>;
    var curHeader = $('#<%=xssTileid%> h1');
    var newHeaderText = curHeader.text() + " (" + cnt + ")";
    //CCD-390 - Disabled due to wrong behavior of count.
    //curHeader.text(newHeaderText);
</script>
<%!
    boolean hasPermission(AccessControlManager acm, String path, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(path, new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // if we have a error then we will return false.
        }
        return false;
    }
%>

