<%@page session="false"
          import="com.adobe.cq.projects.api.Project,
                  org.apache.sling.api.resource.Resource,
                  org.apache.jackrabbit.util.Text,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.Session,
                  javax.jcr.RepositoryException,
                  javax.jcr.security.Privilege,
                  org.apache.commons.lang3.StringUtils,
                  org.apache.sling.api.resource.ValueMap,
                  com.day.cq.wcm.api.NameConstants,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Tag,
                  com.day.cq.commons.jcr.JcrConstants,
                  java.text.SimpleDateFormat,
                  org.apache.sling.commons.json.JSONObject,
                  org.apache.sling.commons.json.JSONArray,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  com.adobe.cq.projects.ui.ProjectHelper,
                  java.util.ArrayList,
                  java.util.List,
                  java.util.Date,
                  java.util.Calendar,
                  java.util.ResourceBundle,
                  java.net.URLEncoder"
%><%@include file="/libs/granite/ui/global.jsp"%><%
    final Project project = resource.adaptTo(Project.class);

    if (project == null) {
        log.error("Unable to render null project for resource at path: {}", resource.getPath());
        return;
    }

    AccessControlManager acm =  null;
    try {
        acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager.", e);
    }
    boolean isProjectMaster = false;
    Resource jcrContent = resource.getChild("jcr:content");
	String status = null;
    if (jcrContent!=null && jcrContent.getValueMap()!=null) {
        isProjectMaster = "true".equals(jcrContent.getValueMap().get("ismaster", String.class));
        status = jcrContent.getValueMap().get("JLPStatus", "ACTIVE");
    }

    List<String> actionRels = new ArrayList<String>();
    actionRels.add("foundation-collection-item-activator");
    if (isProjectMaster) {
        actionRels.add("cq-projects-admin-actions-masterproperties-activator");
    }
    else {
        actionRels.add("cq-projects-admin-actions-properties-activator");
        // only allow delete for regular projects, not master projects
        if (hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE)) {
            actionRels.add("cq-projects-admin-actions-delete-activator");
        }
    }

    if(isTranslationProject(jcrContent)){
        actionRels.add("cq-projects-admin-actions-translationproject-activator");
        if (canStartTranslation(jcrContent)) {
            actionRels.add("cq-projects-admin-actions-starttranslation-activator");
        }
    }

    boolean isActive = project.isActive();
    boolean isOverdue = isActive && !isProjectMaster && ProjectHelper.isOverdue(resource);

    Resource coverImage = project.getProjectCover();
    String thumbnailUrl = null;
    if (coverImage != null) {
        thumbnailUrl = request.getContextPath() + coverImage.getPath();
    } else {
        thumbnailUrl = request.getContextPath() + "/libs/cq/ui/widgets/themes/default/icons/240x180/page.png";
    }

    String title = project.getTitle() != null ? project.getTitle() : "";
    String xssTitle = xssAPI.encodeForHTML(title);
    String description = project.getDescription() != null ? project.getDescription() : "";
    String xssDescription = xssAPI.encodeForHTML(description);

    String xssPageUrl;
    if (isProjectMaster) {
        xssPageUrl = xssAPI.getValidHref(request.getContextPath() + "/libs/cq/core/content/projects/masterproperties.html?item=" + URLEncoder.encode(resource.getPath(), "UTF-8"));
    } else {
        String detailsHref = getProjectDetailsUrl(resource);
        xssPageUrl = xssAPI.getValidHref(request.getContextPath() + detailsHref + Text.escapePath(resource.getPath()));
    }

    Resource projectContent = resource.getChild(JcrConstants.JCR_CONTENT);
    ValueMap projectContentVM = projectContent.adaptTo(ValueMap.class);

    ResourceBundle resourceBundle = slingRequest.getResourceBundle(slingRequest.getLocale());

    Calendar startDate = projectContentVM.get("project.startDate", Calendar.class);

    String xssStartDate = "";

    if (!isProjectMaster && startDate != null) {
        xssStartDate = formatDate(startDate.getTime(),resourceBundle);
    }
	
    Calendar dueDate = projectContentVM.get("project.dueDate", Calendar.class);

    String xssDueDate = "";

    if (!isProjectMaster && dueDate != null) {
        xssDueDate = formatDate(dueDate.getTime(),resourceBundle);
    }

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();
    attrs.addClass("foundation-collection-navigator");
    attrs.addOther("foundation-collection-navigator-href", xssPageUrl);
    attrs.add("is", "coral-table-row");

    int totalTaskCnt = 0;
    int activeTasks = 0;
    int totalOverdue = 0;
    int completedTasks = 0;
    float percentComplete = 0.0f;

    Resource tasks = jcrContent.getChild("tasks");

    if (tasks != null) {
        ValueMap props = tasks.adaptTo(ValueMap.class);

        if (props.containsKey("totalTasks")) {
            totalTaskCnt = props.get("totalTasks", 0);
        }

        if (props.containsKey("activeTasks")) {
            activeTasks = props.get("activeTasks", 0);
        }

        if (props.containsKey("overdueTasks")) {
            totalOverdue = props.get("overdueTasks", 0);
        }

        completedTasks = totalTaskCnt - (activeTasks + totalOverdue);
        percentComplete = ((float) completedTasks / (float) totalTaskCnt) *  100.0f;
    }

    JSONObject data = new JSONObject();

    JSONArray yArray = new JSONArray();
    yArray.put(completedTasks); //Completed
    yArray.put(activeTasks); //Active
    yArray.put(totalOverdue); //Overdue

    JSONArray xArray = new JSONArray();
    xArray.put(i18n.get("Completed"));
    xArray.put(i18n.get("Active"));
    xArray.put(i18n.get("Overdue"));

    JSONArray sArray = new JSONArray();
    sArray.put("");
    sArray.put("");
    sArray.put("");

    data.put("y", yArray);
    data.put("x", xArray);
    data.put("series", sArray);
%><tr <%= attrs %>>
    <td is="coral-table-cell" coral-table-rowselect><%
        if (thumbnailUrl != null) {
    %><img class="foundation-collection-item-thumbnail" src="<%= thumbnailUrl %>" alt=""><%
    } else {
    %><coral-icon class="foundation-collection-item-thumbnail" icon="folder"></coral-icon><%
        }
    %></td>
    <td class="foundation-collection-item-title" is="coral-table-cell"><%= xssTitle %></td>
    <td is="coral-table-cell"><%= xssDescription %>
        <meta class="foundation-collection-quickactions" data-foundation-collection-quickactions-rel="<%= StringUtils.join(actionRels, " ") %>">
    </td>
    <td is="coral-table-cell"><%=xssStartDate%></td>
    <td is="coral-table-cell">
        <% if (!xssDueDate.isEmpty() && isOverdue && !"COMPLETE".equals(status) && !"CANCELLED".equals(status)) { %>
            <coral-tag color="red" size="M" quiet>
                <%=xssDueDate%>
            </coral-tag>
        <% } else { %>
            <%=xssDueDate%>
        <% } %>
        </td>
    <td is="coral-table-cell">
        <% if (!isProjectMaster) {
            if (isActive  && !"COMPLETE".equals(status) && !"CANCELLED".equals(status)) {
                if (isOverdue) { %>
                    <coral-icon icon="alert" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active-overdue"></coral-icon><span class="cq-projects-AppDetails-status"><%= capitalize(status)%></span>
                <% } else if ("ACTIVE".equals(status)) { %>
                    <coral-icon icon="clock" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-active"></coral-icon><span class="cq-projects-AppDetails-status"><%= capitalize(status)%></span>
        <% }} else { %>
            <coral-icon icon="checkCircle" size="S" class="cq-projects-AppDetails-status-icon cq-projects-AppDetails-status-icon-inactive" ></coral-icon><span class="cq-projects-AppDetails-status"><%= capitalize(status)%></span>
        <% } } %>
    </td>
    <td is="coral-table-cell">
        <% if (!isProjectMaster && tasks!=null) { %>
        <div class="project-stats-wrapper">
            <div><project-taskstats stats='<%=data.toString()%>' size="XS" outerRadius="0"></project-taskstats></div>
            <div class="project-stats-label"><%=Math.round(percentComplete)%>%</div>
        </div>
        <% } %>
    </td>
</tr><%!
    boolean hasPermission(AccessControlManager acm, Resource resource, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(resource.getPath(), new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // if we have a error then we will return false.
        }
        return false;
    }

    private String getProjectDetailsUrl(Resource resource) {
        // if the template specified a details href then we can use that
        // otherwise default to projects/details.html
        Resource content = resource.getChild(NameConstants.NN_CONTENT);
        if (content != null) {
            ValueMap map = content.adaptTo(ValueMap.class);
            String detailsHref = map.get("detailsHref", String.class);
            if (StringUtils.isNotEmpty(detailsHref)) {
                return detailsHref;
            }
        }

        //TODO - osgi this value
        return "/projects/details.html";
    }

    boolean isTranslationProject(Resource jcrContent)throws Exception {
        boolean bRetVal = false;
        if (jcrContent != null) {
            if(StringUtils.isNotBlank(jcrContent.getValueMap().get("cq:template", String.class))){
                String strTemplate = jcrContent.getValueMap().get("cq:template", String.class);
                if("/libs/cq/core/content/projects/templates/create-translation-project".equals(strTemplate) 
                    || "/libs/cq/core/content/projects/templates/translation-project".equals(strTemplate)){
                        bRetVal = true;
                }
            }
        }
        return bRetVal;
    }

    boolean canStartTranslation(Resource jcrContent) throws Exception {
        boolean bRetVal = false;
        if (jcrContent != null) {
            if(StringUtils.isNotBlank(jcrContent.getValueMap().get("translationProvider", String.class))){
                String strProvider = jcrContent.getValueMap().get("translationProvider", String.class);
                if(strProvider!=null && strProvider.length()>0){
                    Resource gadgetResource = jcrContent.getChild("dashboard/gadgets");
                    if (gadgetResource != null) {
                        // traverse all child nodes now
                        Iterable<Resource> childList = gadgetResource.getChildren();
                        for (Resource child : childList) {
                            if (canStartTranslationOnChild(child)) {
                                bRetVal = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return bRetVal;
    }

    boolean canStartTranslationOnChild(Resource child) throws Exception {
        boolean bRetVal = false;
        if (child != null) {
            if ("cq/gui/components/projects/admin/pod/translationjobpod".equals(child.getResourceType())) {
                // now check the status
                if (StringUtils.isNotBlank(child.getValueMap().get("translationStatus", String.class))) {
                    String strStatus = "|" + child.getValueMap().get("translationStatus", String.class) + "|";
                    if ("|DRAFT|SUBMITTED|SCOPE_REQUESTED|SCOPE_COMPLETED|COMMITTED_FOR_TRANSLATION|"
                        .indexOf(strStatus) != -1) {
                        //now check if there is any node or not
                        Resource childPages = child.getChild("child_pages");
                        if(childPages!=null){
                            bRetVal = childPages.hasChildren();
                        }
                    }
                }
            }
        }
        return bRetVal;
    }

    private String formatDate(Date date, ResourceBundle resourceBundle) {
        SimpleDateFormat sdfshort = new SimpleDateFormat("MMM dd",resourceBundle.getLocale());
        SimpleDateFormat sdflong = new SimpleDateFormat("dd/MM/yyyy",resourceBundle.getLocale());
        String formattedDate = null;
        if (date != null) {
            try {
                RelativeTimeFormat rtf = new RelativeTimeFormat("Y","HH:mm", "MMM dd", "dd/MM/yyyy", resourceBundle);
                long now = System.currentTimeMillis();
                if (date.getTime() < now) {
                    formattedDate = rtf.format(date.getTime(), true);
                } else {
                    if (date.getTime() - now > 1000 * 60 * 60 * 24 *120) {
                    	formattedDate = sdflong.format(date);
                    } else {
                        formattedDate = sdfshort.format(date);
                    }
                }
            } catch (IllegalArgumentException e) {
                formattedDate = sdflong.format(date.getTime());
            }
        }
        return formattedDate;
    }
    
    String capitalize(String data) {
        String firstLetter = data.substring(0,1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }
%>

