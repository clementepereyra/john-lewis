#!/bin/bash

################################
# Build script for GoCD builds #
################################

help() {
  echo -e "Usage: go <command>"
  echo -e
  echo -e "    help              Print this help"
  echo -e "    deploy-local      Upload and install sites and content locally (does not run migration scripts)"
  echo -e "    pagechecker       Run migration scripts, and validate page content against schemas"
  echo -e "    linkchecker       Run migration scripts, and validate content link urls - WARNING results in traffic to prod site, avoid running during peak times"
  echo -e "    upload-and-tag    Upload (and NOT install) sites and content bundles to dev AEM author and tag source"
  exit 0
}

init() {
  # Trying to avoid out-of-memory error on GoCD by stopping the Gradle daemon
  ./gradlew --stop

  # Set default version if needed
  if [ -z "$GO_PIPELINE_COUNTER" ]; then
  GO_PIPELINE_COUNTER="1.0-SNAPSHOT"
  fi
  echo "Version: $GO_PIPELINE_COUNTER"
}

deploy-local() {
  # Trying to avoid out-of-memory error on GoCD by completing content and sites builds before deploying
  ./gradlew --no-daemon -PincludeContent -PversionOverride=$GO_PIPELINE_COUNTER clean sites:content:aemCompose
  ./gradlew --no-daemon -PversionOverride=$GO_PIPELINE_COUNTER sites:aemCompose
  ./gradlew --no-daemon -PincludeContent -PversionOverride=$GO_PIPELINE_COUNTER sites:content:aemDeploy
  ./gradlew --no-daemon -PversionOverride=$GO_PIPELINE_COUNTER sites:aemDeploy
}

pagechecker() {
  ./gradlew --no-daemon sites:ui:generateSchemas
  cd contentTests
  ./gradlew --no-daemon clean runMigrationScripts pagechecker:test
}

linkchecker() {
  cd contentTests
  ./gradlew --no-daemon clean runMigrationScripts linkchecker:test
}

upload-and-tag() {
  ./gradlew --no-daemon -PincludeContent -Penv=dev -PversionOverride=$GO_PIPELINE_COUNTER sites:content:aemUpload
  ./gradlew --no-daemon -Penv=dev -PversionOverride=$GO_PIPELINE_COUNTER sites:aemUpload
  git -c user.name="GoCD" -c user.email="gocd@build.jlcp.uk" tag $GO_PIPELINE_COUNTER && git -c user.name="GoCD" -c user.email="gocd@build.jlcp.uk" push origin $GO_PIPELINE_COUNTER
}

if [[ $1 =~ ^(deploy-local|pagechecker|linkchecker|upload-and-tag)$ ]]; then
  init
  COMMAND=$1
  shift
  $COMMAND "$@"
else
  help
  exit 1
fi
