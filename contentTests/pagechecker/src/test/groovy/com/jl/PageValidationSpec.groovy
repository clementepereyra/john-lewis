package com.jl

import groovy.json.JsonSlurper
import okhttp3.*
import org.everit.json.schema.Schema
import org.everit.json.schema.ValidationException
import org.everit.json.schema.loader.SchemaLoader
import org.json.JSONObject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.util.regex.Pattern

import static java.util.concurrent.TimeUnit.MINUTES

class PageValidationSpec extends Specification {

    @Shared
    Map<String, Schema> schemas

    @Shared
    List<ValidationMessage> warningsReport = []

    @Shared
    List<ValidationMessage> slingResourceTypeMissingReport = []

    @Shared
    OkHttpClient httpClient = new OkHttpClient.Builder().connectionPool(new ConnectionPool(1, 10, MINUTES)).build()

    def setupSpec() {
        schemas = new File(System.properties.getOrDefault("schemaDir", "../../sites/ui/build/component-schemas") as String)
                .listFiles()
                .collectEntries {[
                        (it.name): SchemaLoader.builder()
                                                .draftV7Support()
                                                .resolutionScope(it.parentFile.absoluteFile.toURI())
                                                .schemaJson(new JSONObject(it.text))
                                                .build().load().build() as Schema
                ]}
    }

    def cleanupSpec() {
        def warningsFile = new File("build/reports/tests/test/warnings.txt")
        warningsFile.parentFile.mkdirs()
        warningsFile.createNewFile()
        warningsFile.text = warningsReport.collect { it.asTsv }.join("\n")

        def slingResourceTypeMissingFile = new File("build/reports/tests/test/slingResourceTypeMissing.txt")
        slingResourceTypeMissingFile.parentFile.mkdirs()
        slingResourceTypeMissingFile.createNewFile()
        slingResourceTypeMissingFile.text = slingResourceTypeMissingReport.collect { it.asTsv }.join("\n")
    }

    @Unroll
    def "#pageContentPath"() {
        given:
        def page = fetchPage(pageContentPath)
        def schema = schemaFor(page)

        when:
        def errorMessage = validate(schema, page, pageContentPath)

        then:
        !errorMessage

        where:
        pageContentPath << pageContentPaths
    }

    private List<String> getPageContentPaths() {
        def response = httpClient.newCall(
                new Request.Builder()
                        .addHeader("Authorization", Credentials.basic("admin", "admin"))
                        .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), "scriptPath=/etc/groovyconsole/scripts/onejl/findPages.groovy"))
                        .url("http://localhost:4502/bin/groovyconsole/post.json")
                        .build()
        ).execute()
        def scriptOutput = new JsonSlurper().parse(response.body().byteStream()).output as String
        return scriptOutput.split("\\n").sort()
    }

    private JSONObject fetchPage(String path) {
        def page = new JSONObject(fetchCmsContent("${path}.0.json"))
        def pageContent = new JSONObject(fetchCmsContent("${path}/jcr:content.infinity.json"))
        page.put("jcr:content", pageContent)
        return page
    }

    private String fetchCmsContent(String path) {
        return httpClient.newCall(
                new Request.Builder()
                        .addHeader("Authorization", Credentials.basic("admin", "admin"))
                        .url("http://localhost:4502/$path")
                        .get()
                        .build()
        ).execute().body().string()
    }

    private Schema schemaFor(JSONObject page) {
        def slingResourceType = page.getJSONObject("jcr:content").getString("sling:resourceType")
        def componentNameMatcher = slingResourceType =~ ".*/components/pages/(?<componentName>.*)"
        assert componentNameMatcher.matches()

        def componentName = componentNameMatcher.group("componentName")
        def schemaName = schemas.keySet().find { it.endsWith("$componentName-schema.json") }
        assert schemaName

        return schemas.get(schemaName)
    }

    private String validate(Schema schema, JSONObject page, String path) {
        try {
            schema.validate(page)
        } catch (ValidationException exception) {
            def messages = exception.allMessages.collect { new ValidationMessage(pagePath: path, message: it) }
            def warnings = messages.findAll { message ->
                def hasWarningOnChildNode = messages.any { it.type.warning && it.path.contains(message.parentPath) }
                def causedByWarningOnChildNode = message.type == MessageType.SLING_RESOURCE_TYPE_NOT_VALID && hasWarningOnChildNode
                return message.type.warning || causedByWarningOnChildNode
            }

            slingResourceTypeMissingReport += messages.findAll { it.type == MessageType.SLING_RESOURCE_TYPE_NOT_FOUND }
            warningsReport += warnings

            def errors = messages - warnings
            return errors.collect { it.message }.join("\n")
        }

        return null
    }

    static class ValidationMessage {

        private static final Pattern MESSAGE_PATTERN = Pattern.compile("#(?<path>(?<parentPath>.*)(?<nodeName>/[^/]+)): (?<reason>.*)")

        String pagePath
        String message

        MessageType getType() {
            return MessageType.from(pagePath, message)
        }

        String getPath() {
            def matcher = MESSAGE_PATTERN.matcher(message)
            return matcher.matches() ? matcher.group("path") : "/"
        }

        String getParentPath() {
            def matcher = MESSAGE_PATTERN.matcher(message)
            return matcher.matches() ? matcher.group("parentPath") : "/"
        }

        String getAsTsv() {
            "$type\t$pagePath\t$message"
        }
    }

    static enum MessageType {
        CATEGORY_PAGE_600001639_CUSTOM_DESIGN("/content/1jl/en/content/browse/category/600001639", ".*sling:resourceType: .* is not a valid enum value", true),
        CATEGORY_PAGE_6000074_CUSTOM_DESIGN("/content/1jl/en/content/browse/category/6000074", ".*sling:resourceType: .* is not a valid enum value", true),
        CATEGORY_PAGE_500001_50000113_CUSTOM_DESIGN("/content/1jl/en/content/browse/category/500001/50000113", ".*sling:resourceType: onejl/components/container is not a valid enum value", true),
        PRICE_MATCH_PAGE_CUSTOM_DESIGN("/content/1jl/en/content/browse/price-match-assets/price-match-universal-app", ".*sling:resourceType: .* is not a valid enum value", true),
        BUTTON_LINK_DEPRECATED(".*", ".*onejl/components/buttonLink is not a valid enum value", true),
        QUICK_LINK_DEPRECATED(".*", ".*1jl/components/quickLinks is not a valid enum value", true),
        ICEBERG_DEPRECATED(".*", ".*onejl/components/iceberg is not a valid enum value", true),
        TABLE_DEPRECATED(".*", ".*1jl/components/table is not a valid enum value", true),
        SLING_RESOURCE_TYPE_NOT_FOUND(".*", ".*required key \\[sling:resourceType\\] not found", false),
        SLING_RESOURCE_TYPE_NOT_VALID(".*", ".*sling:resourceType: (.*) is not a valid enum value", false),
        NOT_ALLOWED_BY_SCHEMA(".*", ".*", false)

        final String pagePattern
        final String messagePattern
        final boolean warning

        MessageType(String pagePattern, String messagePattern, boolean warning) {
            this.pagePattern = pagePattern
            this.messagePattern = messagePattern
            this.warning = warning
        }

        static MessageType from(String page, String message) {
            return values().find { page ==~ it.pagePattern && message ==~ it.messagePattern }
        }
    }
}
