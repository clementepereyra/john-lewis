package com.jl

import groovy.json.JsonSlurper
import okhttp3.*
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.AsyncConditions

import java.util.regex.Pattern

import static java.util.concurrent.TimeUnit.MINUTES
import static java.util.concurrent.TimeUnit.SECONDS
import static java.util.regex.Pattern.CASE_INSENSITIVE

class LinkCheckerSpec extends Specification {

    @Shared
    def linkChecker = new LinkChecker(
            new OkHttpClient.Builder()
                    .readTimeout(10, SECONDS)
                    .connectTimeout(10, SECONDS)
                    .connectionPool(new ConnectionPool(10, 60, SECONDS))
                    .cache(new Cache(new File("build/linkCheckerCache"), Long.MAX_VALUE))
                    .build()
    )

    @Shared
    List<LinkCheck> linkChecks = []

    def setupSpec() {
        linkChecks = findLinks()
        linkChecker.check(linkChecks)
    }

    @Unroll
    def "#linkCheck.link"() {
        expect:
            linkCheck.conditions.await(20)
            !linkCheck.isScene7Image()
            !linkCheck.isMDot()
            !linkCheck.isHandover()

        where:
            linkCheck << linkChecks
    }

    static class LinkChecker {

        private final OkHttpClient client

        LinkChecker(OkHttpClient client) {
            this.client = client
        }

        void check(List<LinkCheck> linkChecks) {
            linkChecks.each { linkCheck ->
                try {
                    def request = new Request.Builder()
                            .get()
                            .url(linkCheck.normalisedLinkUrl)
                            .cacheControl(new CacheControl.Builder().maxStale(Integer.MAX_VALUE, SECONDS).build())
                            .header("User-Agent", "Crayon link checker")
                            .build()

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        void onFailure(Call call, IOException exception) {
                            linkCheck.errorMessage = exception.toString()
                            linkCheck.conditions.evaluate { assert !linkCheck.errorMessage }
                        }

                        @Override
                        void onResponse(Call call, Response response) throws IOException {
                            linkCheck.responseCode = response.code()
                            response.body().string() // always get the body so the response can be cached

                            linkCheck.conditions.evaluate {
                                assert linkCheck.responseCode == 200
                            }
                        }
                    })
                } catch (Exception exception) {
                    linkCheck.errorMessage = exception.toString()
                    linkCheck.conditions.evaluate { assert !linkCheck.errorMessage }
                }
            }
        }
    }

    static class LinkCheck {

        private static final String BASE_URL = "https://www.johnlewis.com"

        String path
        String link
        String text
        String image
        AsyncConditions conditions = new AsyncConditions()
        String errorMessage
        int responseCode

        String getAemPage() {
            return path.substring(0, path.lastIndexOf("/jcr:content"))
        }

        String getAemPageRelativeToBrowse() {
            return aemPage - "/content/1jl/en/content/browse"
        }

        String getPathRelativeToAemPage() {
            return path - aemPage
        }

        String getOneJLPage() {
            if (aemPageWithSamePathOnBrowse) {
                return "$BASE_URL$aemPageRelativeToBrowse"
            }
            if (aemPageRelativeToBrowse.matches("/category/[\\d]+")) {
                return "$BASE_URL/c${aemPageRelativeToBrowse - "/category/"}"
            }

            return ""
        }

        String getNormalisedLinkUrl() {
            if (link.startsWith("http")) {
                return link
            }
            if (link.startsWith("//")) {
                return "https:$link"
            }
            if (link.startsWith("/")) {
                return "$BASE_URL$link"
            }
            if (!oneJLPage) {
                return link
            }
            return "$oneJLPage${(link.startsWith("#") || link.startsWith("?")) ? "" : "/"}$link"
        }

        boolean isScene7Image() {
            return link.contains("johnlewis.scene7.com") && !link.contains("pdf")
        }

        boolean isMDot() {
            return link.contains("m.johnlewis.com")
        }

        boolean isArchived() {
            return path.contains("archive")
        }

        boolean isHandover() {
            return link.contains("/handover/")
        }

        boolean isRequiresSignIn() {
            return ["/sign-in", "/register", "account.johnlewis.com", "/my-account", "/appointments/book", "athomeappointments.johnlewis.com"].any {
                link.contains(it)
            }
        }

        boolean isProductPage() {
            return link.matches('^.*/p/?\\d+(\\?[^#]*)?(#.*)?$')
        }

        boolean isTelephoneNumber() {
            return link.matches(Pattern.compile('^tel:.*$', CASE_INSENSITIVE))
        }

        boolean isMailTo() {
            return link.matches(Pattern.compile('^mailto:.*$', CASE_INSENSITIVE))
        }

        private boolean isAemPageWithSamePathOnBrowse() {
            return [
                    "brands",
                    "browse",
                    "business",
                    "buying-guides",
                    "content",
                    "christmas-advert",
                    "cms",
                    "customer-services",
                    "inspiration-and-advice",
                    "my-account",
                    "our-services",
                    "contextual-help",
                    "my-john-lewis-insider-news"
            ].any { aemPageRelativeToBrowse.startsWith("/$it") }
        }
    }

    private static List<LinkCheck> findLinks() {
        def response = new OkHttpClient.Builder().readTimeout(10, MINUTES).build().newCall(
                new Request.Builder()
                        .addHeader("Authorization", Credentials.basic("admin", "admin"))
                        .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), "scriptPath=/etc/groovyconsole/scripts/onejl/findLinks.groovy"))
                        .url("http://localhost:4502/bin/groovyconsole/post.json")
                        .build()
        ).execute()

        def scriptOutput = new JsonSlurper().parse(response.body().byteStream()).output as String
        return scriptOutput.replaceAll('\\"', '"')
                           .split('\\n')
                           .collect { it.split("\\t") }
                           .collect {
            new LinkCheck(path: it[0], link: it[1].trim(), text: it.length > 2 ? it[2] : "", image: it.length > 3 ? it[3] : "")
        }
                           .findAll {
            it.link != '#' && !it.archived && !it.productPage && !it.requiresSignIn && !it.telephoneNumber && !it.mailTo
        }
    }
}
